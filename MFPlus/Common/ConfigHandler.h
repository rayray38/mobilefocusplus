//
//  ConfigHandler.h
//  EFSideKick
//
//  Created by Nobel on 2014/3/6.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommandSender.h"

#define PSIA_CUSTOM     @"PSIA/Custom/Everfocus"
#define PSIA_IPADDRESS  @"PSIA/System/Network/interfaces/0/ipAddress"
#define PSIA_INFO       @"PSIA/System/DeviceInfo"
#define PSIA_DYNAMIC    @"dynamic"
#define PSIA_STATIC     @"static"

#define AFREEY_NETWORK  @"cgi-bin/config_network.cgi?Act"
#define AFREEY_SYSINFO  @"cgi-bin/config_sysinfo.cgi?Act"        //20141105 added by Ray Lin 
#define AFREEY_OK       @"[200 Successfully]"


@interface NetSeed : NSObject

@property(nonatomic)        BOOL      blnStatic;
@property(nonatomic,retain) NSString  *name;                     //20141103 added by Ray Lin
@property(nonatomic,retain) NSString  *ipv4;
@property(nonatomic,retain) NSString  *ipv6;
@property(nonatomic,retain) NSString  *subMask;
@property(nonatomic)        NSInteger httpPort;
@property(nonatomic)        NSInteger rtspPort;

@end

@interface ConfigHandler : NSObject<NSXMLParserDelegate, NSURLConnectionDelegate>
{
    CommandSender		*cmdSender;
    NSString			*currentXmlElement;
	NSMutableData		*responseData;
	NSURLConnection		*requestConnection;
    NSString            *errorDesc;
    
    NSString            *host;
    NSString            *user;
    NSString            *passwd;
    
    NSMutableDictionary *xmlArray;
    
    BOOL                blnGetName;
    BOOL                blnGetIPAddress;
    BOOL                blnGetCustom;
}

- (id)initWithHost:(NSString *)theHost;
- (NSString *)getDeviceNameByProtocol:(NSInteger)type;
- (BOOL)getNetworkConfigByProtocol:(NSInteger)type;
- (BOOL)setNetworkConfigByProtocol:(NSInteger)type Seed:(NetSeed *)seed;

- (NSString *)getStringFromLine:(NSString *)data targetString:(NSString *)target;

@property(nonatomic,retain) NSString    *host;
@property(nonatomic,retain) NSString    *user;
@property(nonatomic,retain) NSString    *passwd;

@end
