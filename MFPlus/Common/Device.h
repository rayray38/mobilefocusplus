//
//  Device.h
//  EFViewer
//
//  Created by James Lee on 2010/5/19.
//  Copyright 2010 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>

// Product type
#define DEV_DVR				0
#define DEV_IPCAM			1
#define EVENT               2
#define SETTING             9

//=====================================================================//
//                                                                     //
//                             STREAM Type                             //
//                                                                     //
//=====================================================================//
enum {
    STREAM_P2 = 0,
    STREAM_JPEG,
    STREAM_NEVIO,
    STREAM_HDIP,
    STREAM_DYNA,        // add since HDv1.0.3
    STREAM_GEMTEK,
    STREAM_AFREEY,      // add since HDv1.0.5
    STREAM_ICATCH,      // add since Plusv1.2.4
    STREAM_XMS,         // add since Plusv1.2.8
    STREAM_ONVIF,       // 20150630 added by Ray Lin
    STREAM_NVR265,      // 20161207 added by Ray Lin
} EStreamType;

//=====================================================================//
//                                                                     //
//                               DVR Index                             //
//                                                                     //
//=====================================================================//
#define IS_UNICODE(X)   ((X==EDR_HD_4H4)||(X==EDR_HD_2H14) \
                        ||(X==EPHD_04)||(X==ENDEAVOR264L4) \
                        ||(X==ENDEAVOR264X4)||(X==EPARA264_32) \
                        ||(X==PARAGON960_16X1)||(X==ECOR960_16X1))
enum {
    /* P2 Codebase */
    ECOR264_4D1	= 0,
#define STR_ECOR264_4D1			@"ECOR264-4D1/4F1"
    ECOR264_4D2,
#define STR_ECOR264_4D2			@"ECOR264-4D2/4F2"
    ECOR264_8D1,    // no mobile stream
#define STR_ECOR264_8D1			@"ECOR264-8D1/8F1"
    ECOR264_8D2,
#define STR_ECOR264_8D2			@"ECOR264-8D2/8F2"
    ECOR264_4X1,
    ECOR264_9X1,
    ECOR264_16X1,
#define STR_ECOR264_4_9_16X1    @"ECOR264-4/9/16x1"
    EDR_16D1_16F1,
#define STR_EDR_16D1_16F1		@"EDR-16D1/16F1"
    ELR_4D_4F,
#define STR_ELR_4D_4F			@"ELR-4D/4F"
    ELR_8D_8F,      // no mobile stream
#define STR_ELR_8D_8F			@"ELR-8D/8F"
    ERS_4,                      // [#10]
#define STR_ERS_4				@"ERS-4"
    PARAGON2,       // unicode
#define STR_PARAGON2			@"Paragon2"
// v2.0.3
    EDR_HD_4H4,     // unicode
#define STR_EDR_HD_4H4			@"EDR HD-4H4"
    EDR_HD_2H14,    // unicode
#define STR_EDR_HD_2H14			@"EDR HD-2H14"
    EMV_200,
#define STR_EMV_200				@"EMV200"
    EMV_400_800_1200,
#define STR_EMV_400_800_1200	@"EMV400/800/1200"
    ENDEAVOR264X4,  // unicode
    ENDEAVOR264L4,  // unicode
#define STR_ENDEAVOR264X4L4		@"Endeavor264X4/L4"
    PARAGON_HD08,
#define STR_PARAGON_HD08		@"EPHD08"
// v2.2.3 add by robert hsu 20111102
    EPARA264_32,    // unicode
#define STR_EPARA264_32         @"Paragon32X4"
// v2.2.7 add by James Lee 20120525
    PARAGON264_16X1,            // [#20]
    PARAGON264_16X2,
    PARAGON264_16X4,
#define STR_PARAGON264_16X1X2X4 @"Paragon264X1/X2/X4"
    EMV_200S_400S,
#define STR_EMV_200S_400S       @"EMV200S/400S"
// v2.3.3
    EPHD_04,
#define STR_EPHD_04             @"EPHD04+"
    ECOR960_16X1,
#define STR_ECOR960_16X1        @"ECOR960-16X1"
    PARAGON960_16X1,// unicode
#define STR_PARAGON960_16X1     @"Paragon960X1"
    EMV1201,
#define STR_EMV_1201            @"EMV1201"

    /* P3 Codebase */
    PARAGON960_16X4 = 50,       // [#50]
#define STR_PARAGON960_16X4     @"Paragon960X4"
    NVR8004X,
#define STR_NVR8004x            @"NVR8004x"
    ENVR8304D_E_X,
#define STR_ENVR8304D_E_X       @"ENVR8304D/E/X"
// HDv1.1.0 add by James Lee 20140117
    ECOR960_4_8_16F2,
#define STR_ECOR960_4_8_16F2    @"ECOR960-4/8/16F2"
    TUTIS_4_8_16F3,
#define STR_TUTIS_4_8_16F3      @"TUTIS+ 4/8/16F3"
// HDv1.1.4
    EPHD08_PLUS,
#define STR_EPHD08_PLUS         @"EPHD08+"
    EMV_401_801_1601,
#define STR_EMV401_801_1601     @"EMV401/801/1601"
    EMV_400_800_1200_HD,
#define STR_EMV400_800_1200_HD  @"EMV HD Series"
    EMV_FHD_SERIES,
#define STR_EMV_FHD_SERIES      @"EMV FHD Series"
    ECORHD_16X1,
#define STR_ECORHD_16X1         @"ECOR HD 16X1"
    ECORHD_4_8_16F,             // [#60]
#define STR_ECORHD_4_8_16F      @"ECOR HD 4/8/16F"
    EMX32,
#define STR_EMX_32              @"EMX32"
    ENVR8316E,
#define STR_ENVR8316E           @"ENVR8316E"
    ECOR_FHD_SERIES,
#define STR_ECOR_FHD_SERIES     @"ECOR FHD Series"
    ELUX_SERIES,
#define STR_ELUX_SERIES         @"ELUX Series"
    PARAGON_FHD,
#define STR_PARAGON_FHD         @"Paragon FHD Series"
    ESN41_41E_FHD,
#define STR_ESN_FHD_SERIES      @"ESN FHD Series"
    EVS410_FHD,
#define STR_EVS_FHD_SERIES      @"EVS FHD Series"
    EMV400S_FHD,
    EMV400_SSD,
#define STR_EMV_400S_SERIES     @"EMV400S Series"
    
    /* ICATCH Series */
    EPHD16U = 101,
#define STR_EPHD16U             @"EPHD16+U"
    
    /* NVR Pro */
    NVR_Pro = 111,
#define STR_NVR_PRO             @"NVR Pro"   //20161207 added by Ray Lin
    
    /* XMS Series */
    XMS_SERIES = 151,
#define STR_XMS_SERIES          @"XMS/Commander2/Elite2"
} EDvrModel;

//=====================================================================//
//                                                                     //
//                             IPCAM Index                             //
//                                                                     //
//=====================================================================//
enum {
    /* NEVIO Series */
    EAN800A_AW = 0,
    EAN850A,
#define STR_EAN850A_800A_AW     @"EAN850A/800A/AW"
    EDN800,
    EDN850H,
#define STR_EDN800_850H			@"EDN800/850H"
    EPN3100,
    EPN3600,
#define STR_EPN3100_3600		@"EPN3100/3600"
    EVS200A_AW,
#define STR_EVS200A_AW			@"EVS200A/AW"
    EZN850,
#define STR_EZN850				@"EZN850"
// v2.2.0
    EAN900,
#define STR_EAN900				@"EAN900"
    
    /* NEVIO HD Series */
    EQN2200_2201,
#define STR_EQN2200_2201		@"EQN2200/2201"
    EAN3200,                    // [#10]
#define STR_EAN3200				@"EAN3200"
// v2.2.2 add by robert hsu for add new IP cam model 20110923
    EHN3240,
#define STR_EHN3240             @"EHN3240"
    EZN3240,
#define STR_EZN3240             @"EZN3240"
    EDN3240,
#define STR_EDN3240             @"EDN3240"
    EVS410,
#define STR_EVS410              @"EVS410"
// v2.2.8 add by James Lee 20120705 for new IP cam model
    EAN3120,
    EAN3220,
    EAN3300,
#define STR_EAN3120_3220_3300   @"EAN3120/3220/3300"
    EAN3520,
    EZN3160,
    EZN3260,                    // [#20]
    EZN3340,
#define STR_EZN3160_3260_3340   @"EZN3160/3260/3340"
    EZN3560,
    EDN3160,
    EDN3260,
    EDN3340,
#define STR_EDN3160_3260_3340   @"EDN3160/3260/3340"
    EDN3560,
    EHN3160,
    EHN3260,
    EHN3340,
#define STR_EHN3160_3260_3340   @"EHN3160/3260/3340"
    EHN3560,                    // [#30]
    EPN4122_4220,
    EPN4122i_4220i,
#define STR_EPN4122_4220_i      @"EPN4122/4220(i)"
//2.3.5 add by James Lee 20130513
    EMN2120_2220_2320,
#define STR_EMN2120_2220_2320   @"EMN2120/2220/2320"
    EDN1120_1220_1320,
#define STR_EDN1120_1220_1320   @"EDN1120/1220/1320"
//HDv1.1.0 add by James Lee 20140118
    EZN1160_1260_1360,
#define STR_EZN1160_1260_1360   @"EZN1160/1260/1360"
    EZN3261_3361,
#define STR_EZN3261_3361        @"EZN3261/3361"
    EHN3261_3361,
#define STR_EHN3261_3361        @"EHN3261/3361"
    EDN2210,
#define STR_EDN2210             @"EDN2210"
    EHN1120_1220_1320,
#define STR_EHN1120_1220_1320   @"EHN1120/1220/1320"
    EPN4230_P,                  // [#40]
#define STR_EPN4230_P           @"EPN4230/4230p"
    EFN3320_3321,
#define STR_EFN3320_3321        @"EFN3320/3321"
    EFN3320c_3321c,
#define STR_EFN3320c_3321c      @"EFN3320c/3321c"
//2.4.6 add by James Lee 20141128
    EPN4220_4230_D,
#define STR_EPN4220_4230_D      @"EPN4220d/4230d"
    EBN268_V,
    EZN268_V,
#define STR_EXN268_SERIES       @"ExN268 Series"
    EDN228,
#define STR_EDN228              @"EDN228"
    EAN7200,
//20160315 added by Ray Lin
    EAN7221_7260_7360,
    EHN7221_7260_7360,
    EZN7221_7260_7360,
#define STR_POLESTAR_7_SERIES    @"Polestar 7 Series"
    
    /* DYNA Series */
//2.3.0 add by James Lee 20120723
    EAN2150 = 80,               // [#80]
#define STR_EAN2150             @"EAN2150"
    EAN2218,
#define STR_EAN2218             @"EAN2218"
    EAN2350,
#define STR_EAN2350             @"EAN2350"
    EDN2245,
#define STR_EDN2245             @"EDN2245"
    EPN2218,
#define STR_EPN2218             @"EPN2218"
    EAN7220,
#define STR_EAN7220             @"EAN7220"
    
    /* GEMTEK Series */
//2.3.4 add by James Lee 20130318
    EQN2101_2171 = 90,
    EQN2110,
#define STR_EQN2101_2110_2171   @"EQN2101/2110/2171"
    
    /* AFREEY Series */
//HDv1.0.5 add by James Lee 20130819
    ETN2160_2260_2560 = 100,
#define STR_ETN2160_2260_2560   @"ETN2160/2260/2560"
//HDv1.1.0 add by James Lee 20140117
    EDN2160_2260_2560,
#define STR_EDN2160_2260_2560   @"EDN2160/2260/2560"
    
    /* ONVIF Series */
//20150622 added by Ray Lin
    EQN100 = 105,
#define STR_EQN100              @"EQN100"
    
    /* IR Speed Dome Series */
//20160115 added by Ray Lin
    EPN5210,
    EPN5230,
#define STR_EPN5210_5230        @"EPN5210/5230"
    
//20150708 added by Ray Lin
    ONVIF,
#define STR_ONVIF               @"ONVIF"
    
    /* RTSP Series*/
//20151110 added by Ray Lin for rtsp connection
    RTSP,
#define STR_RTSP                @"RTSP"
    
//20161118 added by Ray Lin
    EBN368_V,                   // [#110]
    EZN368_V_M,
    EDN368M,
#define STR_EXN368_SERIES       @"ExN368 Series"
} EIPCamModel;

//define playback action by robert hsu 20120208
#define PLAYBACK_FORWARD        1
#define PLAYBACK_FAST_FORWARD   2
#define PLAYBACK_BACKWARD       3
#define PLAYBACK_FAST_BACKWARD  4
#define PLAYBACK_PAUSE          5
#define PLAYBACK_EXIT           10

//=====================================================================//
//                                                                     //
//                         Device Type In XMS                          //
//                                                                     //
//=====================================================================//
enum {
    XMS_IPCAM = 0,
    XMS_DVR,
    XMS_NVR,
    XMS_CHANNEL,
    XMS_RTSP,
    XMS_VEHICLE,
    XMS_VEHICLE_DVR,
    XMS_VEHICLE_CH,
} EFXmsDevType;    //20151216 added by Ray Lin


@interface Device : NSObject

@property (nonatomic)			NSInteger	product;
@property (nonatomic)			NSInteger	type;
@property (nonatomic,retain)	NSString	*name;
@property (nonatomic,retain)	NSString	*ip;
@property (nonatomic,retain)	NSString	*user;
@property (nonatomic,retain)	NSString	*password;
@property (nonatomic)			NSInteger	port;
@property (nonatomic,retain)    NSString    *mac;
@property (nonatomic)			NSInteger	streamType;
@property (nonatomic)			NSInteger	dualStream;
@property (nonatomic)			NSInteger	rtspPort;
@property (nonatomic)           NSInteger   group;  //add by James Lee 20140605 for group
@property (nonatomic)           BOOL        blnIsPlaying;
@property (nonatomic)           BOOL        blnStatic;
@property (nonatomic)           NSInteger   rowID;
@property (nonatomic,retain)    NSString    *maskAddr;
@property (nonatomic,retain)    NSString    *gateway;
@property (nonatomic,retain)    NSString    *dns;

@property (nonatomic,retain)    NSString    *device_service; // Added by Steven Chang
@property (nonatomic,retain)    NSString    *media_uri;      // Added by Steven Chang
@property (nonatomic,retain)    NSString    *device_uri;     // Added by Steven Chang
@property (nonatomic,retain)    NSString    *main_profile_token;  // Added by Steven Chang
@property (nonatomic,retain)    NSString    *sub_profile_token;   // Added by Steven Chang
@property (nonatomic,retain)    NSString    *main_stream_uri;     // Added by Steven Chang
@property (nonatomic,retain)    NSString    *sub_stream_uri;     // Added by Steven Chang
@property (nonatomic,retain)    NSString    *network_interface;    // Added by Steven Chang

@property (nonatomic)           NSInteger   blnCloseNotify;  // 20150831 added by Ray Lin
@property (nonatomic,retain)	NSString	*uuid;  //20150907 added by Ray Lin
@property (nonatomic,retain)    NSString    *devTimeZone; //20151026 ++ by Ray Lin
@property (nonatomic)           BOOL        blnEvNotify;  //20160805 ++ by Ray Lin

- (id)initWithProduct:(NSInteger)ptype;
- (id)initWithDevice:(Device *)device;

- (void)setValueWithDevice:(Device *)device;

+ (NSArray *)createDvrModelArray;
+ (NSArray *)createIpcamModelArray;
+ (NSInteger)modelStringToIndex:(NSString *)string product:(NSInteger)product;
+ (NSString *)modelIndexToString:(NSInteger)index product:(NSInteger)product;
+ (NSInteger)getStreamType:(NSInteger)model product:(NSInteger)product;
+ (BOOL)getPTZSupport:(NSInteger)model product:(NSInteger)product;

+ (NSInteger)getNewIDfromArray:(NSMutableArray *)_Array; //得到DeviceArray現在最新的rowID
+ (Device *)getDeviceByID:(NSInteger)_ID from:(NSMutableArray *)_Array; //用rowID在DeviceArray中找出Device

@end

@interface DeviceGroup : NSObject

@property (nonatomic, retain)   NSString    *name;
@property (nonatomic)           NSInteger   type;
@property (nonatomic)           NSInteger   group_id;
@property (nonatomic)           BOOL        expand;

- (id)initWithProduct:(NSInteger)ptype;
+ (DeviceGroup *)getGroupByID:(NSInteger)_ID from:(NSMutableArray *)_Array; //用GroupID在GroupArray中找出DeviceGroup
+ (NSMutableArray *)getArrayByGroupID:(NSInteger)_ID from:(NSMutableArray *)_Array; //用GroupID在DeviceArray中找出對應的sub-array
+ (NSInteger)getNewIDFromArray:(NSMutableArray *)_Array; //得到GroupArray現在最新的group_id

@end

@interface EventTable : NSObject

@property (nonatomic, retain)   NSString    *source_name;
@property (nonatomic, retain)   NSString    *source_ip;
@property (nonatomic)           NSInteger   source_port;
@property (nonatomic, retain)   NSString    *source_uuid;
@property (nonatomic, retain)   NSString    *dev_name;
@property (nonatomic)           NSInteger   dev_type;
@property (nonatomic, retain)   NSString    *dev_ip;
@property (nonatomic)           NSInteger   dev_eid;
@property (nonatomic)           NSInteger   channel;
@property (nonatomic, retain)   NSString    *event_type;
@property (nonatomic)           NSUInteger  event_time;
@property (nonatomic)           NSInteger   event_seq;
@property (nonatomic)           NSInteger   rowID;
@property (nonatomic)           NSInteger   blnRead;   //0:NO(未讀) ; 1:YES(已讀)

- (id)initWithEvent;

@end
