//
//  ShareViewController.h
//  EFViewerHD
//
//  Created by Nobel on 13/10/1.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFViewerAppDelegate.h"
#import "QRViewController.h"

#import "QREncoder.h"
#import "DataMatrix.h"

@interface ExportViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView		*devTableView;
    IBOutlet UIBarButtonItem    *btnSelectAll;
    IBOutlet UIBarButtonItem    *btnDeselect;
    
    NSMutableArray              *devList;
    NSMutableArray              *checkList;
}

- (IBAction)selectAll;
- (IBAction)deselectAll;

@property(nonatomic,retain) id<contentDelegate>     delegate;
@property(nonatomic,retain) QRViewController        *qrView;

@end
