//
//  HelpViewController.h
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/7.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFViewerAppDelegate.h"

#define HELPPDF @"MobileFocusHD_UM_V1.1.7.1_O_EN.pdf"
#define HELPURL @"http://old.everfocus.com.tw/Storage/HQ/Manual/Mobile%20Focus/MobileFocusHD_UM.pdf"
#define HELPFRAME CGRectMake(0,0,525,743)


@interface HelpViewController : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView       *helpView;
    IBOutlet UIBarButtonItem *closeBtn;
    IBOutlet UIView          *noteView;
    IBOutlet UITextView      *textView;
}

- (IBAction)closeView;

@property(nonatomic,assign) id<contentDelegate> delegate;
@property(nonatomic)        NSInteger           uType;

@end
