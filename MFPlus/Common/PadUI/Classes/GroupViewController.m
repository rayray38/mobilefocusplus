//
//  GroupViewController.m
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/3.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import "GroupViewController.h"

@interface GroupViewController ()
{
    NSInteger       popType;
}
- (void)actionSave;
- (void)actionEditName;
- (void)saveGroup;
- (void)closeView;
- (void)showAlarm:(NSString *)message;
- (void)showInputWindow:(NSString *)message with:(NSString *)defaultText;

@end

@implementation GroupViewController

@synthesize delegate,group,devArray,curGArray,devType,blnNewGroup;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MMLog(@"[GV] Init %@",self);
    UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(actionSave)];
    [self.navigationItem setRightBarButtonItem:saveBtn];
    [saveBtn release];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(closeView)];
    [self.navigationItem setLeftBarButtonItem:cancelBtn];
    [cancelBtn release];
    
    selectArray = [[NSMutableArray alloc] init];
    
    UIButton *titleBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width/2, TOOLBAR_HEIGHT_V)];
    [titleBtn setBackgroundColor:[UIColor clearColor]];
    [titleBtn addTarget:self action:@selector(actionEditName) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleBtn;
    [titleBtn release];
    
    //Initial search bar
    searchBar = [[UISearchBar alloc] init];
    searchBar.delegate = self;
    searchBar.placeholder = NSLocalizedString(@"SearchDevice", nil);
    [searchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [searchBar sizeToFit];
    devTableView.tableHeaderView = searchBar;
    
    searchDisplay = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    searchDisplay.delegate = self;
    searchDisplay.searchResultsDataSource = self;
    searchDisplay.searchResultsDelegate = self;
    
    searchResult = [[NSMutableArray alloc] init];
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.os_version >= 7.0) {
        [searchBar setBarTintColor:[UIColor blackColor]];
    }else {
        [searchBar setBarStyle:UIBarStyleBlackTranslucent];
    }
    
    popType = epvt_group;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.preferredContentSize = POPVIEW_FRAME.size;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    UIButton *titleBtn = (UIButton *)self.navigationItem.titleView;
    NSString *title = self.blnNewGroup ? NSLocalizedString(@"AddGroup",nil) : group.name;
    [titleBtn setTitle:title forState:UIControlStateNormal];
    
    [[self.navigationItem rightBarButtonItem] setEnabled:NO];
    
    if (!self.blnNewGroup) {
        for (NSInteger i=0; i<self.curGArray.count; i++)
        {
            NSIndexPath *curPath = [NSIndexPath indexPathForRow:i inSection:0];
            [[devTableView delegate] tableView:devTableView didSelectRowAtIndexPath:curPath];
        }
    }
    
    [devTableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [selectArray removeAllObjects];
    //MMLog(@"[GV] devArray %@ rCount:%d",self.devArray,self.devArray.retainCount);
}

- (void)dealloc
{
    MMLog(@"[GV] dealloc %@",self);
    [selectArray removeAllObjects];
    [selectArray release];
    selectArray = nil;
    
    [group release];
    group = nil;
    
    [searchResult removeAllObjects];
    [searchResult release];
    searchResult = nil;
    
    if (searchDisplay!=nil) {
        [searchDisplay release];
        searchDisplay = nil;
    }
    
    if (searchBar!=nil) {
        [searchBar release];
        searchBar = nil;
    }
    
    delegate = nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action

- (void)actionSave
{
    if (self.blnNewGroup) {
        //input group name
        [self showInputWindow:NSLocalizedString(@"MsgInputGName", nil) with:nil];
    }else
        [self saveGroup];
}

- (void)actionEditName
{
    if (!self.blnNewGroup)
        [self showInputWindow:NSLocalizedString(@"MsgInputGName", nil) with:group.name];
}

- (void)saveGroup
{
    if ([group.name isEqualToString:@""]) //validation
        return;
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (self.blnNewGroup) { //new group : create a new array and put all selected device into it
        
        NSMutableArray *newArray = [[NSMutableArray alloc] init];
        for (Device *dev in selectArray)
        {
            dev.group = group.group_id;
            [self.devArray removeObject:dev];
            [newArray addObject:dev];
        }
        
        BOOL ret = YES;
        if (devType == DEV_DVR) {
            [appDelegate.dvrs insertObject:newArray atIndex:appDelegate.dvrs.count-1];
            ret |= [appDelegate refreshDVRsIntoDatabase];
        }else {
            [appDelegate.ipcams insertObject:newArray atIndex:appDelegate.ipcams.count-1];
            ret |= [appDelegate refreshIPCamsIntoDatabase];
        }
        [newArray release];
        
        NSMutableArray *groupArray = [appDelegate.groups objectAtIndex:devType];
        [groupArray insertObject:group atIndex:groupArray.count];
        ret |= [appDelegate refreshGroupIntoDatabase];
        
        if (!ret)   //Alert
            [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
    }else { // existed group : put all selected device into curGArray
        
        while (self.curGArray.count > 0) //先全部移到devArray再做selectArray的mapping
        {
            Device *dev = [self.curGArray lastObject];
            NSLog(@"[GV] Dev:%@ change from [%d] to [0]",dev.name,dev.group);
            dev.group = 0;
            [self.devArray addObject:dev];
            [self.curGArray removeObject:dev];
        }
        
        for (Device *dev in selectArray)
        {
            NSLog(@"[GV] Dev:%@ change from [%d] to [%d]",dev.name,dev.group,group.group_id);
            dev.group = group.group_id;
            [self.curGArray addObject:dev];
            [self.devArray removeObject:dev];
        }
        
        BOOL ret = YES;
        ret |= devType == DEV_DVR ? [appDelegate refreshDVRsIntoDatabase] : [appDelegate refreshIPCamsIntoDatabase];
        if (!ret)   //Alert
            [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
    }
    
    [self closeView];
}

- (void)closeView
{
    [delegate dismissPopView:popType];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == searchDisplay.searchResultsTableView)
        return 1;
    else
        return (self.blnNewGroup ? 1 : 2);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
	
    if (tableView == searchDisplay.searchResultsTableView)
        rows = searchResult.count;
    else {
        switch (section) {
            case 0:
                rows = self.blnNewGroup ? self.devArray.count : self.curGArray.count;
                break;
                
            case 1:
                rows = self.devArray.count;
                break;
        }
    }
    
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 47;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // set background
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor lightTextColor];
    cell.detailTextLabel.textColor = [UIColor lightTextColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
		cell.showsReorderControl = YES;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		// set background
		cell.textLabel.backgroundColor = [UIColor clearColor];
		cell.detailTextLabel.backgroundColor = [UIColor clearColor];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
        
        UIImageView *checkBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addDev1.png"]];
        [cell setAccessoryView:checkBtn];
        [checkBtn release];
        [cell.accessoryView setHidden:YES];
    }
    
	// Configure the cell.
    Device *dev = NULL;
	if (tableView == searchDisplay.searchResultsTableView)
        dev = [searchResult objectAtIndex:indexPath.row];
    else {
        if (self.blnNewGroup)
            dev = [self.devArray objectAtIndex:indexPath.row];
        else
            dev = [(indexPath.section==0 ? self.curGArray : self.devArray) objectAtIndex:indexPath.row];
    }
    
	cell.textLabel.text = dev.name;
    
    BOOL bFind = NO;
    for (Device *tmpDev in selectArray)
    {
        if (dev.rowID == tmpDev.rowID) {
            bFind = YES;
            break;
        }
    }
    
    [cell.accessoryView setHidden:!bFind];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Device *dev;
    if (tableView == searchDisplay.searchResultsTableView)
        dev = [searchResult objectAtIndex:indexPath.row];
    else {
        if (self.blnNewGroup)
            dev = [self.devArray objectAtIndex:indexPath.row];
        else
            dev = [(indexPath.section == 0 ? self.curGArray : self.devArray) objectAtIndex:indexPath.row];
    }
    
    if (selectArray.count == 0) {
        
        [selectArray addObject:dev];
    }else {
        
        BOOL bFind = NO;
        for (Device *tmpDev in selectArray) {
            if (tmpDev.rowID == dev.rowID) {
                
                [selectArray removeObject:dev];
                bFind = YES;
                break;
            }
        }
        
        if (!bFind)
            [selectArray addObject:dev];
    }
    
    [[self.navigationItem rightBarButtonItem] setEnabled:(selectArray.count>0)];
    [tableView reloadData];
}

#pragma mark - UISearhDisplay Delegate

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    tableView.backgroundColor = [UIColor BGCOLOR];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                                         objectAtIndex:[self.searchDisplayController.searchBar
                                                                        selectedScopeButtonIndex]]];
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller  shouldReloadTableForSearchScope:(NSInteger)searchOption {
    
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text]
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:searchOption]];
    
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willHideSearchResultsTableView:(UITableView *)tableView
{
    [devTableView reloadData];
}

-(void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    searchBar.showsCancelButton = YES;
    if (appDelegate.os_version >= 7.0) {
        
        UIBarButtonItem *cancelButton;
        UIView *topView = searchBar.subviews[0];
        for (UIView *subView in topView.subviews) {
            if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
                cancelButton = (UIBarButtonItem*)subView;
            }
        }
        
        if (cancelButton)
            [cancelButton setTintColor:[UIColor darkGrayColor]];
    }
    
    
    if (devTableView.editing)
        [self setEditing:NO animated:YES];
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF.name contains[cd] %@",
                                    searchText];
    
    [searchResult removeAllObjects];
    
    if (!self.blnNewGroup)
        [searchResult addObjectsFromArray:[self.curGArray filteredArrayUsingPredicate:resultPredicate]];
    
    [searchResult addObjectsFromArray:[self.devArray filteredArrayUsingPredicate:resultPredicate]];
}

#pragma mark - UIAlertView Delegate

- (void)showAlarm:(NSString *)message
{
	if ( message!=nil ) {
		
		// open an alert with just an OK button, touch "OK" will do nothing
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"BtnOK", nil) otherButtonTitles:nil];
        
        // For update message alignment
        NSArray *subViewArray = alert.subviews;
        
        for(int x=0;x<[subViewArray count];x++)
        {
            if([[[subViewArray objectAtIndex:x] class] isSubclassOfClass:[UILabel class]] && x > 0)
            {
                UILabel *label = [subViewArray objectAtIndex:x];
                label.textAlignment = NSTextAlignmentLeft;
            }
        }
        
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
	}
}

- (void)showInputWindow:(NSString *)message with:(NSString *)defaultText
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus"
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"BtnCancel", nil)
                                          otherButtonTitles:NSLocalizedString(@"BtnOK", nil), nil];
    
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[alert textFieldAtIndex:0] setKeyboardAppearance:UIKeyboardAppearanceDark];
    [[alert textFieldAtIndex:0] setPlaceholder:NSLocalizedString(@"GroupName", nil)];
    if (defaultText)
        [[alert textFieldAtIndex:0] setText:defaultText];
    
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
    [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UITextField *input = [alertView textFieldAtIndex:0];
    
	switch (buttonIndex) {
		case 1:
            group.name = [NSString stringWithFormat:@"%@",input.text];
            if (self.blnNewGroup) {
                
                [self saveGroup];
            }else {
                
                UIButton *titleBtn = (UIButton *)self.navigationItem.titleView;
                [titleBtn setTitle:group.name forState:UIControlStateNormal];
                
                EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
                BOOL ret = [appDelegate refreshGroupIntoDatabase];
                if (!ret)
                    NSLog(@"[GV] Change Group name to %@ failed",group.name);
            }
			break;
		default:
			break;
	}
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    return [[alertView textFieldAtIndex:0].text length] > 0;
}

@end
