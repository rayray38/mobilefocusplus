//
//  SearchViewController.h
//  EFViewerHD
//
//  Created by James Lee on 13/3/7.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreamReceiver.h"
#import "EFViewerAppDelegate.h"
#import "EventViewController.h"

#define SEARCH_TIME  0
#define SEARCH_EVENT 1

#define INDEX_DATE   0
#define INDEX_TIME   1

#define IMG_CHECK       [UIImage imageNamed:@"check_on.png"]
#define IMG_UNCHECK     [UIImage imageNamed:@"check_off.png"]
#define IMG_COLLAPSE    [UIImage imageNamed:@"collapse.png"]
#define IMG_EXPAND      [UIImage imageNamed:@"expand.png"]

@class SearchViewController;

@protocol searchDelegate

@required
- (void)playbackByMask:(NSUInteger)channel;

@end

@interface SearchViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,eventDelegate>
{
    IBOutlet UITableView    *mainTable;
    IBOutlet UIView         *pickerMenu;
    IBOutlet UIDatePicker   *datePicker;
    IBOutlet UIToolbar      *pickerBar;
    
    NSDateFormatter         *dateFormatter;
    NSInteger               pickerIdx;
    
    NSUInteger              uStartTime;
    NSUInteger              uEndTime;
    NSUInteger              uSelectST;
    NSUInteger              uSelectET;
    
    NSString                *strHddST;
    NSString                *strHddET;
    
    NSUInteger              eventFilter;
}

@property(nonatomic, retain) StreamReceiver *streamReceiver;
@property(nonatomic, assign) id<searchDelegate> searchDelegate;
@property(nonatomic, assign) id<contentDelegate> delegate;
@property(nonatomic)         NSInteger searchType;
@property(nonatomic)         NSInteger playingChannel;
@property(nonatomic, retain) EventViewController *eventView;

- (IBAction)TimeSelected;

@end
