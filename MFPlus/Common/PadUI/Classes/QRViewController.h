//
//  QRViewController.h
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/9.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFViewerAppDelegate.h"

@interface QRViewController : UIViewController
{
    IBOutlet UIImageView    *imgView;
}

@property(nonatomic,retain) id<contentDelegate> delegate;
@property(nonatomic,retain) UIImage             *qrImage;

@end
