//
//  IPViewController.h
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/8/5.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChannelViewController.h"
#import "OnvifDefine.h"

@interface IPViewController : ChannelViewController
{
    NSMutableArray       *streamArray; //save the streamReceivers
}
@end
