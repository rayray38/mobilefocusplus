//
//  DeviceEditViewController.h
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/2.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"

@interface DeviceEditViewController : UIViewController<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    IBOutlet UITextField		*textField;
	IBOutlet UIPickerView       *pickerView;
}

@property (nonatomic,retain) NSArray *pickerViewArray;
@property (nonatomic,retain) NSString *editedPropertyKey;
@property (nonatomic,retain) NSString *editedPropertyDisplayName;
@property (nonatomic,retain) Device *device;

- (IBAction)editDone;

@end
