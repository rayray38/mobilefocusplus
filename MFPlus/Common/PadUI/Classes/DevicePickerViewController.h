//
//  DevicePickerViewController.h
//  EFViewerHD
//
//  Created by James Lee on 13/1/22.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFViewerAppDelegate.h"

@class DevicePickerViewController;

@protocol pickerDelegate

@required
- (void)pickerController:(DevicePickerViewController *)pickerView didSelectValue:(NSString *)value;

@end

@interface DevicePickerViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>
{
    IBOutlet UIPickerView *modelpicker;
    
    NSString              *devId;
    NSMutableArray        *devList;
    
    BOOL                  blnIOS7;
    id<pickerDelegate> delegate;
}

@property(nonatomic,retain) NSString        *devId;
@property(nonatomic,retain) NSMutableArray  *devList;
@property(nonatomic,assign) id<pickerDelegate> delegate;

@end