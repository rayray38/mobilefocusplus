//
//  QRViewController.m
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/9.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import "QRViewController.h"

@interface QRViewController ()
{
    NSInteger       popType;
}

- (void)closeView;
- (void)saveToAlbum;
- (void)showAlarm:(NSString *)message;

@end

@implementation QRViewController

@synthesize delegate;//,qrImage;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MMLog(@"[QRV] Init %@",self);
    
    UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveToAlbum)];
    [self.navigationItem setRightBarButtonItem:saveBtn];
    [saveBtn release];
    
    popType = epvt_export;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.preferredContentSize = POPVIEW_FRAME.size;
    [imgView setImage:self.qrImage];
}

- (void)dealloc
{
    MMLog(@"[QRV] dealloc %@",self);
    
    [imgView release];
    imgView = nil;
    
    self.qrImage = nil;
    delegate = nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)closeView
{
    [delegate dismissPopView:popType];
}

- (void)saveToAlbum
{
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *albumName = [NSString stringWithFormat:@"%@",appDelegate._AppName];
    
    [appDelegate.mediaLibrary saveImage:self.qrImage toAlbum:albumName withCompletionBlock:^(NSError *error){
        if (error!=nil) {
            [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
        }else {
            [self showAlarm:NSLocalizedString(@"MsgSaveOK", nil)];
            [self closeView];
        }
    }];
}

#pragma mark - Alarm Delegate

- (void)showAlarm:(NSString *)message
{
    if ( message!=nil ) {
		
		// open an alert with just an OK button, touch "OK" will do nothing
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];[alert show];
	}
}

@end
