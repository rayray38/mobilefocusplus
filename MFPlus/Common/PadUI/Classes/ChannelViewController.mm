//  
//  ChannelViewController.m
//  EFViewerHD
//
//  Created by James Lee on 12/10/8.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "ChannelViewController.h"
#import "P2StreamReceiver.h"
#import "iCStreamReceiver.h"
#import "XMSStreamReceiver.h"
#import "NVRStreamReceiver.h"
#import "EzAccessoryView.h"

@interface ChannelViewController()

- (void)getDeviceInformation;
- (void)addStreaming:(NSInteger)chIdx viewIdx:(NSInteger)vIdx;
- (void)deleteStreaming:(NSInteger)chIdx viewIdx:(NSInteger)vIdx;

- (void)openSearchView:(NSInteger)type;
- (void)popAlarm:(NSString *)message with:(NSInteger)vIdx ;
- (void)updateChannelStatus;

- (void)openSound:(NSInteger)chIdx with:(NSInteger)vIdx;
- (void)closeSound;
- (void)openSpeak:(NSInteger)chIdx with:(NSInteger)vIdx;
- (void)closeSpeak;
- (void)sendingData:(NSData *)data;

- (void)openPTZView:(NSInteger)chIdx;
- (void)startStreaming;
- (void)closeView:(NSInteger)chIdx with:(NSInteger)vIdx;

- (void)changeAudioCH:(NSInteger)chIdx with:(NSInteger)vIdx;
- (void)changeResolution:(NSInteger)chIdx;
- (void)changePlaybackMode:(NSInteger)btnId;

- (void)autoFillMask;

- (void)openPopViewWith:(NSDictionary *)info;

@end

@implementation ChannelViewController

@synthesize device,devArray,ptzView,event;
@synthesize blnChSelector,blnEventMode,blnEventMode_pb,blnOpenSearch,blnWillOpenSearch,blnPlaybackMode;

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    MMLog(@"[CV] Init %@",self);
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navView = [appDelegate.splitViewController.viewControllers objectAtIndex:1];
    detailView = [navView.viewControllers  objectAtIndex:0];
    
    playingIndexList = [[NSMutableArray alloc] init];
    devArray = [[NSMutableArray alloc] init];
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicator setHidesWhenStopped:YES];
    [self.view addSubview:activityIndicator];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    detailView.delegate = self;
    
    if (blnOpenPtz) {
        blnOpenPtz = NO;
        return;
    }
    
    blnIsSoundOn = NO;
    blnOpenPtz = NO;
    blnFirstConnect = YES;
    statusTimer = nil;
    
    //Initial UI
    [searchBtn setEnabled:YES];
    
    [mainTable reloadData];
    
    // Get Device Info
    if (device != nil && streamReceiver == nil) {
        
        switch (device.streamType) {
            case STREAM_P2:
                streamReceiver = [[P2StreamReceiver alloc] initWithDevice:device];
                NSLog(@"[IV] Initiate P2 streaming receiver %p",streamReceiver);
                break;
                
            case STREAM_ICATCH:
                streamReceiver = [[iCStreamReceiver alloc] initWithDevice:device];
                NSLog(@"[IV] Initiate iC streaming receiver %p",streamReceiver);
                break;
                
            case STREAM_NVR265:
                streamReceiver = [[NVRStreamReceiver alloc] initWithDevice:device];
                NSLog(@"[LV] Initiate NVR streaming receiver %p",streamReceiver);
                break;
        }
        [streamReceiver setDelegate:self];
        
        if (device.streamType == STREAM_NVR265) {
            currentPlayingMask = 0;
            [NSThread detachNewThreadSelector:@selector(startStreaming)
                                     toTarget:self
                                   withObject:nil];
        }
        else {
            [NSThread detachNewThreadSelector:@selector(getDeviceInfomation)
                                     toTarget:self
                                   withObject:nil];
        }
    }
    
    currentPlayingMask = 0;
    
    CGRect frame = self.view.frame;
    [activityIndicator setFrame:CGRectMake((frame.size.width-INDICATOR_HEIGHT)/2,(frame.size.height-INDICATOR_HEIGHT)/2, INDICATOR_HEIGHT, INDICATOR_HEIGHT)];
    
    //get popalarm
    blnPopEnable = NO;
    BOOL bShowPop = [EZAccessoryView getDetailSettingBy:KEY_ALARMPOP];
    [alarmBtn setHidden:!bShowPop];
    
    if (bShowPop)
        [self switchAlarmPop];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (blnOpenPtz)
        return;
    
    if (blnOpenSearch)
        [self dismissPopView:epvt_search];
    
    //stop status timer
    if (statusTimer)
        [statusTimer invalidate];
    
    if (blnIsSoundOn) 
        [detailView changeSoundMode];
    
    if (blnIsSpeakOn)
        [detailView changeSpeakMode];
    
    if (streamReceiver.blnReceivingStream) {

        [detailView disconnectAll];
        [streamReceiver stopStreaming];
    }
    
    //make sure the screen is clean
    [streamReceiver.videoControl refresh];
    
    device = detailView.device = nil;
    speakControl = nil;
    
    //change back to live control
    [detailView changeControlMode:ecm_live with:0];
    
    [playingIndexList removeAllObjects];
    [devArray removeAllObjects];
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (blnOpenPtz)
        return;

    while (blnStreamThread) {
        [NSThread sleepForTimeInterval:0.01f];
    }
    
    SAVE_FREE(streamReceiver);
    
    [self clearCredentialsCache];
}

//20151218 added by Ray Lin, set activityIndicator appear in view center
- (void)viewDidLayoutSubviews
{
    [activityIndicator setCenter:self.view.center];
}

- (void)dealloc
{
    MMLog(@"[CV] dealloc %@",self);
    
    SAVE_FREE(mainTable);
    SAVE_FREE(streamReceiver);
    SAVE_FREE(device);
    SAVE_FREE(devArray);
    SAVE_FREE(playingIndexList);
    SAVE_FREE(activityIndicator);
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"[CV] Receive Memory Warning");
}
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

#pragma mark - Streaming Control

- (void)getDeviceInformation
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    NSLog(@"[CV] GetDeviceInfo thread start");
    [activityIndicator performSelectorOnMainThread:@selector(startAnimating) withObject:nil waitUntilDone:NO];
    if (![streamReceiver getDeviceInfo])
    {
        if (!streamReceiver.errorDesc) {
            streamReceiver.errorDesc = NSLocalizedString(@"MsgInfoErr", nil);
        }
        [self showAlarmAndBack:streamReceiver.errorDesc];
        
        [pool release];
        return;
    }
    
    [mainTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    [activityIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:NO];
    
    [self performSelectorOnMainThread:@selector(autoFillMask) withObject:nil waitUntilDone:NO];
    
    //20160111 added by Ray Lin, for DVR/NVR management control
    detailView.streamReceiver = streamReceiver;
    
    [pool release];
    
    NSLog(@"[CV] GetDeviceInfo thread stop");
}

- (void)updateChannelStatus
{
    if (blnPopEnable) {
        for (CameraInfo *tmpInfo in streamReceiver.cameraList)
        {
            if (FH_IS_ALARM(tmpInfo.status) || FH_IS_MOTION(tmpInfo.status)) {
                
                NSString *status = nil;
                if (FH_IS_ALARM(tmpInfo.status))
                    status = NSLocalizedString(@"Alarm", nil);
                else if (FH_IS_MOTION(tmpInfo.status))
                    status = NSLocalizedString(@"Motion", nil);
                
                NSString *message = [NSString stringWithFormat:@"%@ - %@",tmpInfo.title,status];
                NSInteger vIdx = [[streamReceiver.outputList objectAtIndex:tmpInfo.index-1] integerValue];
                [self popAlarm:message with:vIdx];
                break;
            }
        }
    }
    [mainTable reloadData];
}

- (void)startStreaming { // thread
	
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
    blnStreamThread = YES;
	NSLog(@"[CV] Streaming thread start");
    
    if (streamReceiver.m_blnPlaybackMode) {
        
        if ( ![streamReceiver startPlaybackStreaming:currentPlayingMask] ) {
            [self showAlarmAndBack:[streamReceiver errorDesc]];
        }
    }else {
        
        if ( ![streamReceiver startLiveStreaming:currentPlayingMask] ) {
            [self showAlarmAndBack:[streamReceiver errorDesc]];
        }
    }
	
    NSLog(@"[CV] Streaming thread stop");
    blnStreamThread = NO;
	[pool release];
}

- (void)addStreaming:(NSInteger)chIdx viewIdx:(NSInteger)vIdx
{
    [streamReceiver.outputList replaceObjectAtIndex:chIdx withObject:[NSString stringWithFormat:@"%ld",(long)vIdx]];
    streamReceiver.currentPlayCH = 0x1<<chIdx;
    NSLog(@"[CV] Add CH:%ld View:%ld",(long)chIdx,(long)vIdx);
    
    if (blnFirstConnect) {
        
        currentPlayingMask |= 0x1<<(chIdx);
        if (detailView.videoControl != nil ) {
        
            streamReceiver.videoControl = detailView.videoControl;
            [streamReceiver.videoControl startByCH:vIdx];
            [streamReceiver.videoControl setBlnPSLimit:NO];
        }else {
        
            NSLog(@"[CV] VideoControl loss");
            return;
        }
        
        blnFirstConnect = NO;
        [NSThread detachNewThreadSelector:@selector(startStreaming)
                                 toTarget:self
                               withObject:nil];
    } else {
        while (!streamReceiver.blnReceivingStream) {
            if (streamReceiver.blnConnectionClear) {
                return;
            }
            [NSThread sleepForTimeInterval:0.001];
        }
        
        currentPlayingMask |= 0x1<<chIdx;
        [streamReceiver.videoControl startByCH:vIdx];
        [streamReceiver changeVideoMask:currentPlayingMask];
    }
    [streamReceiver.videoControl setPlayingChannelByCH:vIdx Playing:chIdx];
    
    //20160112 added by Ray Lin, for management control
    detailView.device = device;
    [detailView checkValidButtonItems:chIdx];
}

- (void)deleteStreaming:(NSInteger)chIdx viewIdx:(NSInteger)vIdx
{
    [streamReceiver.outputList replaceObjectAtIndex:chIdx withObject:[NSString stringWithFormat:@"99"]];
    [streamReceiver.videoControl stopByCH:vIdx];
    currentPlayingMask ^= 0x1<<chIdx;
    [streamReceiver changeVideoMask:currentPlayingMask];
}

- (void)closeView:(NSInteger)chIdx with:(NSInteger)vIdx
{
    NSLog(@"[CV] Disconnect CH:%ld View:%ld",(long)chIdx,(long)vIdx);
    for (CameraInfo *tmpEntry in playingIndexList)
    {
        //NSLog(@"[CV] PlayingIndex[%d] CH:%d",i,currentEntry.index-1);
        if (tmpEntry.index-1 == chIdx) {
            
            //NSLog(@"[CV] RemoveFromPlayList");
            tmpEntry.status = 0;    //reset status when delete streaming
            [playingIndexList removeObject:tmpEntry];
            [self deleteStreaming:chIdx viewIdx:vIdx];
            break;
        }
    }
    
    if (blnIsSoundOn) {
        [self closeSound];
    }
    
    [mainTable reloadData];
}

- (void)openSound:(NSInteger)chIdx with:(NSInteger)vIdx
{
    if (detailView.audioControl && !streamReceiver.audioControl) {
        streamReceiver.audioControl = detailView.audioControl;
    }
    [streamReceiver setCurrentPlayCH:(0x1<<chIdx)];
    
    [streamReceiver openSound];
    
    blnIsSoundOn = YES;
}

- (void)closeSound
{
    [streamReceiver closeSound];
    blnIsSoundOn = NO;
}

- (void)openSpeak:(NSInteger)chIdx with:(NSInteger)vIdx
{
    if (detailView.speakControl!=nil) {
    
        speakControl = detailView.speakControl;
        [speakControl close];
        switch (streamReceiver.audioCodec) {
            case P2_AUDIO_TYPE_G711:
            case P3_AUDIO_TYPE_G711:
                speakControl = [speakControl initWithCodecId:CODEC_ID_PCM_MULAW srate:16000 bps:4 balign:2560 fsize:2560];
                break;
            case P2_AUDIO_TYPE_ADPCM:
            case P3_AUDIO_TYPE_ADPCM:
                speakControl = [speakControl initWithCodecId:CODEC_ID_ADPCM_IMA_WAV srate:8000 bps:4 balign:164 fsize:321];
                break;
                
            case P3_AUDIO_TYPE_NVRG711:
                speakControl = [speakControl initWithCodecId:CODEC_ID_PCM_MULAW srate:8000 bps:8 balign:320 fsize:320];
                break;
        }
        
        streamReceiver.audioSender.auFlag = streamReceiver.audioCodec;
        streamReceiver.audioSender.auChannel = log2(streamReceiver.currentPlayCH);
        [speakControl setPostProcessAction:self action:@selector(sendingData:)];
        [speakControl start];
        [streamReceiver openSpeak];
        blnIsSpeakOn = YES;
    }
}

- (void)sendingData:(NSData *)data
{
    [streamReceiver.audioSender sendData:data];
}

- (void)closeSpeak
{   
    blnIsSpeakOn = NO;
    [speakControl stop];
    [streamReceiver closeSpeak];
    [speakControl close];  
}

- (void)changeAudioCH:(NSInteger)chIdx with:(NSInteger)vIdx
{
    if (blnIsSoundOn) {
        streamReceiver.currentPlayCH = 0x1<<chIdx;
        [streamReceiver changeAudioChannel];
    }
    if (blnIsSpeakOn) 
        streamReceiver.currentPlayCH = streamReceiver.audioSender.auChannel = 0x1<<chIdx;
}

- (void)changeResolution:(NSInteger)chIdx
{
    //[streamReceiver changeVideoResolution:chIdx video:streamReceiver.dualStream]; //disable for alarm pop
}

- (void)changePlaybackMode:(NSInteger)btnId;
{
    NSInteger mode;
    
    if (btnId == PLAYBACK_EXIT) {
        
        NSLog(@"[CV] Playback -> Live");
        //disconnect all streaming
        if (blnXmsIPcam) {
            [detailView disconnectAll];
        }
        else
            [streamReceiver stopStreaming];
        
        //connect live streaming
        [detailView changeControlMode:ecm_live with:0];
        streamReceiver.m_blnPlaybackMode = NO;
        blnFirstConnect = YES;
        [playingIndexList removeAllObjects];
//        [self autoFillMask];
        NSIndexPath *targetPath = [NSIndexPath indexPathForRow:log2(streamReceiver.currentPlayCH) inSection:0];
        [[mainTable delegate] tableView:mainTable didSelectRowAtIndexPath:targetPath];
        return;
    }
    
    switch (btnId) {

        case PLAYBACK_PAUSE:
            if (blnPlaybackPause) {
                mode = PLAYBACK_FORWARD;
                [streamReceiver.videoControl resume];
                pbSpeed = 1;
            }else {
                mode = PLAYBACK_PAUSE;
                [streamReceiver.videoControl pause];
                pbSpeed = 0;
            }
            blnPlaybackPause = !blnPlaybackPause;
            break;
            
        case PLAYBACK_FAST_FORWARD:
        case PLAYBACK_FAST_BACKWARD:
            if (blnPlaybackPause) {
                [streamReceiver.videoControl resume];
                blnPlaybackPause = NO;
            }
            
            if (btnId == PLAYBACK_FAST_FORWARD) {
                pbSpeed = (pbSpeed > 0 ? pbSpeed*2 : 1);
                if (pbSpeed > 32)   pbSpeed = 32;
            }else {
                pbSpeed = (pbSpeed < 0 ? pbSpeed*2 : -1);
                if (pbSpeed < -32)  pbSpeed = -32;
            }
            mode = btnId;
            break;
    }
    
    [streamReceiver changePlaybackMode:mode withValue:abs(pbSpeed)];
    [detailView changeControlMode:mode with:pbSpeed];
}

#pragma mark - PTZ Menu Control

- (void)openPTZView:(NSInteger)chIdx
{
    if (self.ptzView == nil) {
        
        PTZMenuController *viewController = [[PTZMenuController alloc] initWithNibName:@"PTZMenuController" bundle:[NSBundle mainBundle]];
        self.ptzView = viewController;
        SAVE_FREE(viewController);
    }
    
    blnOpenPtz = YES;
    self.ptzView.ptzStreamReceiver = streamReceiver;
    self.ptzView.liveChannel = chIdx;

    [self.navigationController pushViewController:self.ptzView animated:YES];
}

#pragma mark - Action

- (void)openSearchView:(NSInteger)type
{
    SearchViewController *searchView = [[SearchViewController alloc] initWithNibName:@"SearchViewController"
                                                                              bundle:[NSBundle mainBundle]];
    searchView.delegate = self;
    searchView.searchDelegate = self;
    searchView.streamReceiver = streamReceiver;
    searchView.searchType = type;
    
    VideoDisplayView *tmpView = [streamReceiver.videoControl.aryDisplayViews objectAtIndex:detailView.selectWindow];
    searchView.playingChannel = tmpView.playingChannel;
    
    UINavigationController *navSearchView = [[UINavigationController alloc] initWithRootViewController:searchView];
    SAVE_FREE(searchView);
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.splitViewController showPopViewWith:navSearchView];
    SAVE_FREE(navSearchView);
}

- (IBAction)openSearchMenu
{
    @synchronized(self)
    {
        if (self.blnWillOpenSearch) {
            blnOpenSearch = YES;
            [self openSearchView:0];
            [searchBtn setImage:[UIImage imageNamed:@"searchover.png"] forState:UIControlStateNormal];
            return;
        }
        else {
            blnOpenSearch = YES;
            if (device.streamType == STREAM_ICATCH || device.streamType == STREAM_XMS) {
                [self openSearchView:0];
                [searchBtn setImage:[UIImage imageNamed:@"searchover.png"] forState:UIControlStateNormal];
                return;
            }
            
            [searchBtn setImage:[UIImage imageNamed:@"searchover.png"] forState:UIControlStateNormal];
            
            EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
            if (appDelegate.os_version >= 8.0) {    //iOS8 only
                
                UIAlertController *searchMenu = [UIAlertController alertControllerWithTitle:nil
                                                                                    message:nil
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *actSearchTime = [UIAlertAction actionWithTitle:NSLocalizedString(@"TimePlayback", nil)
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction *action) {
                                                                          [self openSearchView:0];
                                                                      }];
                
                UIAlertAction *actSearchEvent = [UIAlertAction actionWithTitle:NSLocalizedString(@"EventPlayback", nil)
                                                                         style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction *action) {
                                                                           [self openSearchView:1];
                                                                       }];
                UIAlertAction *actCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"BtnCancel", nil)
                                                                    style:UIAlertActionStyleCancel
                                                                  handler:^(UIAlertAction *action) {
                                                                      [searchBtn setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
                                                                      blnOpenSearch = NO;
                                                                  }];
                [searchMenu addAction:actSearchTime];
                [searchMenu addAction:actSearchEvent];
                [searchMenu addAction:actCancel];
                [searchMenu.view setTintColor:[UIColor darkTextColor]];
                
                [self presentViewController:searchMenu animated:YES completion:^{}];
            }else {
                
                UIActionSheet *searchMenu = [[[UIActionSheet alloc] initWithTitle:nil
                                                                         delegate:self
                                                                cancelButtonTitle:nil
                                                           destructiveButtonTitle:nil
                                                                otherButtonTitles:NSLocalizedString(@"TimePlayback", nil),NSLocalizedString(@"EventPlayback", nil),nil]
                                             autorelease];
                [searchMenu showInView:self.view];
            }
        }
    }
}

- (void)openPopViewWith:(NSDictionary *)info
{
    @synchronized(self)
    {
        if (blnShowPop || blnOpenSearch)     return;
        
        blnShowPop = YES;
        PopViewController *popView = [[PopViewController alloc] init];
        popView.delegate = self;
        popView.streamer = streamReceiver;
        popView.title = [info objectForKey:@"MESSAGE"];
        popView.viewIdx = [[info objectForKey:@"VIDX"] integerValue];
        UINavigationController *navPopView = [[UINavigationController alloc] initWithRootViewController:popView];
        SAVE_FREE(popView);
        
        EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.splitViewController showPopAlarmWith:navPopView];
        SAVE_FREE(navPopView);
    }
}

- (void)popAlarm:(NSString *)message with:(NSInteger)vIdx
{
    if (blnPopEnable && !blnShowPop) {
        
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:message,@"MESSAGE",
                                                [NSNumber numberWithInteger:vIdx],@"VIDX",
                                                                            nil];
        [self performSelectorOnMainThread:@selector(openPopViewWith:)
                               withObject:dict
                            waitUntilDone:YES];
    }
}

- (void)autoFillMask
{
    //Initial status timer
    if (!statusTimer) {
        
        statusTimer = [NSTimer scheduledTimerWithTimeInterval:2.0f
                                                       target:self
                                                     selector:@selector(updateChannelStatus)
                                                     userInfo:Nil
                                                      repeats:YES];
    }
    
    for (CameraInfo *tmpInfo in streamReceiver.cameraList)
    {
        if ([playingIndexList indexOfObject:tmpInfo] == NSNotFound) {  //沒有在播放的camera
            
            if (playingIndexList.count+1 > pow([detailView getViewMode],2)) //現有view已點滿了
                return;
            
            //找個空的view來add
            for (VideoDisplayView *tmpView in detailView.videoDispViews)
            {
                if (!tmpView.blnIsPlay) {
                    [detailView changeSelectWindowTo:[detailView.videoDispViews indexOfObject:tmpView]];
                    NSIndexPath *targetPath = [NSIndexPath indexPathForRow:[streamReceiver.cameraList indexOfObject:tmpInfo] inSection:0];
                    [NSThread sleepForTimeInterval:0.1f];
                    [[mainTable delegate] tableView:mainTable didSelectRowAtIndexPath:targetPath];
                    break;
                }
            }
        }
    }
}

- (void)switchAlarmPop
{
    if (blnPopEnable)
        [alarmBtn setImage:[UIImage imageNamed:@"alarmpop.png"]
                  forState:UIControlStateNormal];
    else {
        EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.splitViewController closeTips];
        [alarmBtn setImage:[UIImage imageNamed:@"alarmpopover.png"]
                  forState:UIControlStateNormal];
    }
    
    blnPopEnable = !blnPopEnable;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return streamReceiver.cameraList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 30)] autorelease];
    [headerView setBackgroundColor:[UIColor BACOLOR]];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:headerView.frame] autorelease];
    NSString *title = NSLocalizedString(@"CameraList", nil);
    
    [titleLabel setText:title];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setTextColor:[UIColor lightTextColor]];
        [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
        cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
        
        UIImageView *checkBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addDev1.png"]];
        [checkBtn setFrame:CGRectMake(0, 0, 27, 27)];
        [cell setAccessoryView:checkBtn];
        [checkBtn release];
        [cell.accessoryView setHidden:YES];
    }
    
    // Configure the cell...
    CameraInfo *currentEntry = [streamReceiver.cameraList objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[currentEntry.title stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if (0x1 << (currentEntry.index-1) & currentPlayingMask) {
        [cell.accessoryView setHidden:NO];
    }else {
        [cell.accessoryView setHidden:YES];
    }
    
    UIImageView *checkBtn = (UIImageView *)cell.accessoryView;
    [checkBtn setImage:[UIImage imageNamed:(currentEntry.status ? @"warning.png" : @"addDev1.png")]];
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CameraInfo *currentEntry = [streamReceiver.cameraList objectAtIndex:indexPath.row];
    
    if (playingIndexList.count == 0) {
        
        [playingIndexList addObject:currentEntry];
        [self addStreaming:currentEntry.index-1 viewIdx:detailView.selectWindow];
    }
    else {
        
        for(CameraInfo *tmpEntry in playingIndexList)
        {
            if ([tmpEntry isEqual:currentEntry]) {

                NSLog(@"[CV] CH:%ld is Playing",currentEntry.index-1);
                return;
            }
        }

        if (0x1<<detailView.selectWindow & streamReceiver.videoControl.outputMask)
        {
            VideoDisplayView *tmpView = [streamReceiver.videoControl.aryDisplayViews objectAtIndex:detailView.selectWindow];
            if (tmpView.playingChannel == EMPTY_CHANNEL) {
                NSLog(@"[CV] View:%ld is empty",(long)detailView.selectWindow);
                return;
            }
            [self closeView:tmpView.playingChannel with:detailView.selectWindow];
        }
        
        if (playingIndexList.count<pow([detailView getViewMode],2) || [detailView getViewMode]==1)
        {
            [playingIndexList addObject:currentEntry];
            [self addStreaming:currentEntry.index-1 viewIdx:detailView.selectWindow];
        }
    }
    
    [mainTable reloadData];
}

#pragma mark - Content Delegate

- (void)dismissPopView:(NSInteger)_type
{
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.splitViewController closePopView];
    
    if (epvt_search == _type) {
        
        [searchBtn setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
        blnOpenSearch = NO;
    } else if (epvt_alarm == _type) {
        
        blnShowPop = NO;
    }
}

- (void)statusCallback:(NSString *)_message
{
    NSArray *msgArray = [_message componentsSeparatedByString:@":"];
    if ([[msgArray firstObject] isEqualToString:@"ALARM_POP"]) {
        
        if ([[msgArray objectAtIndex:1] isEqualToString:@"OFF"]) {
            [self switchAlarmPop];
            EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate.splitViewController showTips:NSLocalizedString(@"MsgAlarmDisable", nil) with:7.0f];
        }
    }
}

#pragma mark - Search Delegate

- (void)playbackByMask:(NSUInteger)channel
{
    NSIndexPath *tmpPath;
    
    NSLog(@"[CV] Live -> Playback");
    if (0x1<<channel & streamReceiver.validChannel) {
        
        //disconnect all streaming
        [detailView disconnectAll];
        [streamReceiver stopStreaming];
        
        //connect playback streaming
        blnFirstConnect = YES;
        blnPlaybackPause = NO;
        streamReceiver.m_blnPlaybackMode = YES;
        pbSpeed = 1;
        [detailView changeControlMode:ecm_playback with:0];
        tmpPath = [NSIndexPath indexPathForRow:channel inSection:0];
        [[mainTable delegate] tableView:mainTable didSelectRowAtIndexPath:tmpPath];
    }
}

#pragma mark - Stream Delegate

- (void)EventCallback:(NSString *)message
{
    NSLog(@"[CV](evMsg) %@",message);
    NSArray *msgArray = [message componentsSeparatedByString:@":"];
    if ([[msgArray firstObject] isEqualToString:@"PLAYBACK_NO"]) {
        [searchBtn setEnabled:NO];
    } else if ([[msgArray firstObject] isEqualToString:@"PLAYBACK_YES"]) {
        [searchBtn setEnabled:YES];
    } else if ([[msgArray firstObject] isEqualToString:@"RECORD_NO"]) {
        [detailView changeControlMode:ecm_noRecord with:0];
    } else if ([[msgArray firstObject] isEqualToString:@"EVENT"]) {
    } else if ([[msgArray firstObject] isEqualToString:@"BPS"]) {
#ifdef BPS_DISPLAY
        [self performSelectorOnMainThread:@selector(setTitle:)
                               withObject:[msgArray objectAtIndex:1]
                            waitUntilDone:YES];
#endif
    }
}

- (void)xmsEventStatusCallback:(NSDictionary *)data
{
    
}

#pragma mark - mainControl Delegate

- (void)controlStatusCallback:(NSString *)message
{
    CMLog(@"[CV](ctrlMsg) %@",message);
    NSArray *cmdArray = [message componentsSeparatedByString:@":"];
    if ([[cmdArray firstObject] isEqualToString:@"OPENPTZ"]) {
        //OPENPTZ:CH
        [self openPTZView:[[cmdArray objectAtIndex:1] integerValue]];
    }
    else if ([[cmdArray firstObject] isEqualToString:@"CHANGECH"]) {
        //CHANGECH:CH
        [self changeAudioCH:[[cmdArray objectAtIndex:1] integerValue]
                    with:[[cmdArray objectAtIndex:2] integerValue]];
    }
    else if ([[cmdArray firstObject] isEqualToString:@"CLOSEVIEW"]) {
        //CLOSEVIEW:CH:VID
        [self closeView:[[cmdArray objectAtIndex:1] integerValue]
                   with:[[cmdArray objectAtIndex:2] integerValue]];
    }
    else if ([[cmdArray firstObject] isEqualToString:@"SOUND"]) {
        //SOUND:ON/OFF:CH
        if ([[cmdArray objectAtIndex:1] isEqualToString:@"ON"])
            [self openSound:[[cmdArray objectAtIndex:2] integerValue]
                       with:[[cmdArray objectAtIndex:3] integerValue]];
        else
            [self closeSound];
    }
    else if ([[cmdArray firstObject] isEqualToString:@"SPEAK"]) {
        //SPEAK:ON/OFF:CH
        if ([[cmdArray objectAtIndex:1] isEqualToString:@"ON"])
            [self openSpeak:[[cmdArray objectAtIndex:2] integerValue]
                       with:[[cmdArray objectAtIndex:3] integerValue]];
        else
            [self closeSpeak];
    }
    else if ([[cmdArray firstObject] isEqualToString:@"RESOLUTION"]) {
        //RESOLUTION:MAIN/SUB:CH
        [self changeResolution:[[cmdArray objectAtIndex:2] integerValue]];
    }
    else if ([[cmdArray firstObject] isEqualToString:@"PLAYBACK"]) {
        //PLAYBACK:BUTTONID
        [self changePlaybackMode:[[cmdArray objectAtIndex:1] integerValue]];
    }
    else if ([[cmdArray firstObject] isEqualToString:@"CHANGEVIEW"]) {
        //CHANGEVIEW:VIEW_COUNT
        [self autoFillMask];
    }
    else if ([[cmdArray firstObject] isEqualToString:@"SEARCHBTN"]) {
        //SEARCHBTN:ON/OFF
        if ([[cmdArray objectAtIndex:1] isEqualToString:@"ON"])
            [searchBtn setEnabled:YES];
        else
            [searchBtn setEnabled:NO];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	// use "buttonIndex" to decide your action
	switch (buttonIndex) {
		case 0:
			[self.navigationController popViewControllerAnimated:YES];
			break;
		default:
			break;
	}
}

- (void)showAlarmAndBack:(NSString *)message
{
	if ( message!=nil ) {
		
		// open an alert with just an OK button
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                          delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"BtnOK", nil),nil];
        
		
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
	}
}

- (void)showAlarm:(NSString *)message
{
	if ( message!=nil ) {
		
		// open an alert with just an OK button, touch "OK" will do nothing
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"BtnOK", nil) otherButtonTitles:nil];
		
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];;
	}
}

#pragma mark - UIActionSheet Delegate

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    [actionSheet.layer setBorderWidth:3.0f];
    [actionSheet.layer setBorderColor:[UIColor darkGrayColor].CGColor];
    [actionSheet.subviews enumerateObjectsUsingBlock:^(id _currentView, NSUInteger idx, BOOL *stop) {
        if ([_currentView isKindOfClass:[UIButton class]]) {
            [((UIButton *)_currentView).titleLabel setTextColor:[UIColor darkTextColor]];
        }
    }];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        blnOpenSearch = NO;
        return;
    }
    
    [self openSearchView:buttonIndex];
}

#pragma mark - Credential Clear

- (void)clearCredentialsCache
{
    // reset the credentials cache...
    NSDictionary *credentialsDict = [[NSURLCredentialStorage sharedCredentialStorage] allCredentials];
    
    if ([credentialsDict count] > 0)
    {
        // the credentialsDict has NSURLProtectionSpace objs as keys and dicts of userName => NSURLCredential
        NSEnumerator *protectionSpaceEnumerator = [credentialsDict keyEnumerator];
        
        // iterate over all NSURLProtectionSpaces
        for(id urlProtectionSpace in protectionSpaceEnumerator) {
            NSEnumerator *userNameEnumerator = [[credentialsDict objectForKey:urlProtectionSpace] keyEnumerator];
            for(id userName in userNameEnumerator)
            {
                NSURLCredential *cred = [[credentialsDict objectForKey:urlProtectionSpace] objectForKey:userName];
                NSLog(@"[CV] Cred be removed: %@", cred);
                [[NSURLCredentialStorage sharedCredentialStorage] removeCredential:cred forProtectionSpace:urlProtectionSpace];
            }
        }
    }
}

#pragma mark - Network Detector

- (BOOL)checkNetworkStatus:(Device *)_device
{
    BOOL ret = NO;
    
    Reachability *deviceReach = [Reachability reachabilityWithHostName:_device.ip];
    NetworkStatus netStatus = [deviceReach currentReachabilityStatus];
    
    if (netStatus != NotReachable)
        ret = YES;
    
    return ret;
}

@end
