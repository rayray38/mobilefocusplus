//
//  IPToolViewController.m
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/3.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import "IPToolViewController.h"
#import "EFViewerAppDelegate.h"

@implementation IPToolViewController

@synthesize device,devListFilter;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MMLog(@"[IPV] Init %@",self);
    UIBarButtonItem *scanBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(scanStart)];
    [self.navigationItem setRightBarButtonItem:scanBtn];
    [scanBtn release];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];    //scanBtn    
    self.title = NSLocalizedString(@"ScanDevice", nil);
    self.preferredContentSize = POPVIEW_FRAME.size;
    [maskView setFrame:mainTable.frame];
    
    udpSender = [[UDPSender alloc] init];
    ws_discovery = [[WS_Discovery alloc] init];
    udp_deviceList = udpSender.deviceList;
    deviceList = ws_discovery.deviceList;
    
    decLabel.text = NSLocalizedString(@"MsgNoDevice", nil);
    [decLabel setHidden:YES];
    
    udpSender.productFilter = self.devListFilter;
    [mainTable reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self scanStart];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (!udpSender.blnStop) {
        [self scanStop];
    }
    if (!ws_discovery.blnStop) {
        [self scanStop];
    }
    [deviceList removeAllObjects];
    [udp_deviceList removeAllObjects];
}

- (void)dealloc
{
    SAVE_FREE(udpSender);
    SAVE_FREE(ws_discovery);
    SAVE_FREE(mainTable);
    SAVE_FREE(deviceList);
    SAVE_FREE(udp_deviceList);
    
    if (refreshTimer) {
        [refreshTimer invalidate];
        SAVE_FREE(refreshTimer);
    }
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - action

- (void)scanStart
{
    repeatTime = 0;
    [maskView setHidden:NO];
    [loadAct performSelectorOnMainThread:@selector(startAnimating) withObject:nil waitUntilDone:NO];
    [deviceList removeAllObjects];
    [udp_deviceList removeAllObjects];
    [mainTable reloadData];
    
    [udpSender start];
    if (self.devListFilter == DEV_IPCAM) {
        [ws_discovery start];
    }
    
    refreshTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                    target:self
                                                  selector:@selector(refreshList)
                                                  userInfo:nil
                                                   repeats:YES];
    
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
}

- (void)scanStop
{
    if (udpSender.blnStop)        return;
    if (self.devListFilter == DEV_IPCAM) {
        if (ws_discovery.blnStop)        return;
        [ws_discovery stop];
    }
    
    [udpSender stop];
    [refreshTimer invalidate];
    [maskView setHidden:YES];
    [loadAct stopAnimating];
    
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
}

- (void)refreshList
{
    //檢查udp跟ws-discovery搜尋到的device有沒有重複，有的話以udp的為主
    BOOL blnExist = NO;
    for (int i=0; i<udp_deviceList.count; i++) {
        Device *curDev = [udp_deviceList objectAtIndex:i];
        for (int j=0; j<deviceList.count; j++) {
            Device *refInfo = [deviceList objectAtIndex:j];
            blnExist = [refInfo.ip isEqualToString:curDev.ip];
            
            if (blnExist)
                [deviceList removeObject:refInfo];
        }
    }
    [deviceList addObjectsFromArray:udp_deviceList];
    
    if (deviceList.count>50 || repeatTime>4) {
        [self scanStop];
        if (deviceList.count == 0) {
            //NSLog(@"There is no device in devicelist");
        }
    }
    
    repeatTime++;
    [mainTable reloadData];
}

#pragma mark - TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Hide the tableView when list is empty
    if (deviceList.count == 0) {
        [mainTable setHidden:YES];
        [decLabel setHidden:NO];
    }else {
        [mainTable setHidden:NO];
        [decLabel setHidden:YES];
    }
    // Return the number of rows in the section.
    return deviceList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.showsReorderControl = YES;
		cell.selectionStyle = UITableViewCellSelectionStyleGray;
		
		// set background
		cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor EFCOLOR];
		cell.detailTextLabel.backgroundColor = [UIColor clearColor];
        cell.detailTextLabel.textColor = [UIColor lightTextColor];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
    }
    
    if (indexPath.row < deviceList.count) {
        
        Device *curInfo = [deviceList objectAtIndex:indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[curInfo.name stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",curInfo.ip];
    }else {
        
        cell.textLabel.text = nil;
        cell.detailTextLabel.text = nil;
    }
    
    return cell;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < deviceList.count) {
        
        Device *curInfo = [deviceList objectAtIndex:indexPath.row];
        self.device.name = curInfo.name;
        self.device.ip = curInfo.ip;
        if (curInfo.streamType != STREAM_ONVIF) {
            self.device.port = curInfo.port;
            self.device.product = curInfo.product;
            self.device.type = curInfo.type;
            self.device.streamType = curInfo.streamType;
        }
        else
        {
            self.device.port = 80;
            self.device.product = 1;
            self.device.type = curInfo.type;
            self.device.streamType = curInfo.streamType;
            self.device.device_service = curInfo.device_service;
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    [mainTable reloadData];
}

@end
