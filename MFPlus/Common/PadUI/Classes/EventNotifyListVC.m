//
//  EventNotifyListVC.m
//  EFViewerHD
//
//  Created by EFRD on 2016/8/8.
//  Copyright © 2016年 EF. All rights reserved.
//

#import "EventNotifyListVC.h"

@interface EventNotifyListVC ()

@end

@implementation EventNotifyListVC
{
    BOOL            blnStop;
    NSInteger       lastQueryEventSeq;
    NSMutableArray  *tmpQueryList;
}

@synthesize m_Device;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    eventList = [NSMutableArray new];
    tmpQueryList = [NSMutableArray new];
    [self initialRightBarButton];
    [self initialDBControl];
    [self initialSearchControl];
    [self initialRefreshControl];
    [self initialIndicatorFooter];
    [self initialToolBarButtonItems];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initialTitleLabelButton];
    [self buttonAction:generalBtn];
    [self getEventSeqInUserDefault];
    [appDelegate resetBadgeNumber];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        [self limitEventCount];
    });
    
    blnStop = YES;
    
    [self setSearchBarHidden];
    [self setTableViewCellHeightAuto];
    
    [mainTable reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    DBGLog(@"\nx:%f,width:%f\nx:%f,width:%f\ntoolBar's width:%f",
//           generalBtn.frame.origin.x,
//           generalBtn.frame.size.width,
//           xFleetBtn.frame.origin.x,
//           xFleetBtn.frame.size.width,
//           self.view.frame.size.width);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    if (!(appDelegate.os_version >= 7.0) && searchDisplay.isActive)
        [searchDisplay setActive:NO animated:YES];
    
    if (!blnStop)
        [self stopQuerry];
    
    if (streamReceiver)
        [streamReceiver stopStreaming];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    [eventList removeAllObjects];
    SAVE_FREE(streamReceiver);
}

- (void)dealloc
{
    [super dealloc];
    SAVE_FREE(eventList);
    SAVE_FREE(refreshCtrl);
    
    if (refreshTimer)
        [refreshTimer invalidate];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == (UIInterfaceOrientationLandscapeRight|UIInterfaceOrientationLandscapeLeft));
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Property Initialize

- (void)initialRightBarButton
{
    UIBarButtonItem *EVMenuBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
                                                                               target:self
                                                                               action:@selector(openEventMenu)];
    [self.navigationItem setRightBarButtonItem:EVMenuBtn];
    SAVE_FREE(EVMenuBtn);
}

- (void)initialDBControl
{
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:SQL_FILE_NAME];
    dbControl = [[DBControl alloc] initByPath:path];
}

- (void)initialSearchControl
{
    searchBar = [[UISearchBar alloc] init];
    searchBar.delegate = self;
    searchBar.placeholder = NSLocalizedString(@"SearchDevice", nil);
    [searchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [searchBar sizeToFit];;
    mainTable.tableHeaderView = searchBar;
    
    searchDisplay = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    searchDisplay.delegate = self;
    searchDisplay.searchResultsDataSource = self;
    searchDisplay.searchResultsDelegate = self;
    
    if (appDelegate.os_version >= 7.0) {
        [searchBar setBarTintColor:[UIColor blackColor]];
    }else {
        CGRect tmpFrame = self.view.frame;
        tmpFrame.origin.y += TOOLBAR_HEIGHT_V;
        tmpFrame.size.height -= 2*TOOLBAR_HEIGHT_V;
        [mainTable setFrame:tmpFrame];
        [searchBar setBarStyle:UIBarStyleBlackTranslucent];
    }
    
    searchResult = [[NSMutableArray alloc] init];
}

- (void)initialRefreshControl
{
    refreshCtrl = [[UIRefreshControl alloc] init];
    [refreshCtrl addTarget:self action:@selector(startQuerryForNewData) forControlEvents:UIControlEventValueChanged];
    [refreshCtrl setBackgroundColor:[UIColor BGCOLOR]];
    //[mainTable setContentOffset:CGPointMake(0, -refreshCtrl.frame.size.height-44) animated:NO];
    [mainTable addSubview:refreshCtrl];
    refreshCtrl.tintColor = [UIColor whiteColor];
}

- (void)initialIndicatorFooter
{
    indicatorFooter = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(mainTable.frame), 44)];
    [indicatorFooter setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [mainTable setTableFooterView:indicatorFooter];
}

- (void)initialTitleLabelButton
{
    UIButton *titleLabelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleLabelButton setTitle:[NSString stringWithFormat:@"%@",m_Device.name] forState:UIControlStateNormal];
    [titleLabelButton setFrame:CGRectMake(0, 0, 70, 44)];
    [titleLabelButton addTarget:self action:@selector(scrollToTop) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleLabelButton;
}

- (void)initialToolBarButtonItems
{
    [generalBtn.layer setCornerRadius:10];
    [generalBtn setClipsToBounds:YES];
    [generalBtn setBackgroundImage:[self imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
    [generalBtn setBackgroundImage:[self imageWithColor:[UIColor EFCOLOR]] forState:UIControlStateSelected];
    [generalBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [generalBtn setTitleColor:[UIColor darkTextColor] forState:UIControlStateSelected];
    
    [xFleetBtn.layer setCornerRadius:10];
    [xFleetBtn setClipsToBounds:YES];
    [xFleetBtn setBackgroundImage:[self imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
    [xFleetBtn setBackgroundImage:[self imageWithColor:[UIColor EFCOLOR]] forState:UIControlStateSelected];
    [xFleetBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [xFleetBtn setTitleColor:[UIColor darkTextColor] forState:UIControlStateSelected];
}

- (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark - Event Actions

- (void)openEventMenu
{
    if (eventList.count == 0) {
        DBGLog(@"[ENVC] No Events to delete");
        return;
    }
    
    if (appDelegate.os_version >= 8.0) {
        UIAlertController *evMenu = [UIAlertController alertControllerWithTitle:nil
                                                                          message:[NSString stringWithFormat:@"Are you sure to delete all events?"]
                                                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actDelete = [UIAlertAction actionWithTitle:NSLocalizedString(@"BtnDeleteAll", nil)
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           [self deleteAll];
                                                       }];
        
        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"BtnCancel", nil)
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction *action) {
                                                          }];
        [evMenu addAction:actDelete];
        [evMenu addAction:actCancel];
        [evMenu.view setTintColor:[UIColor blueColor]];
        [self presentViewController:evMenu animated:YES completion:^{}];
    }
    else
    {
        eventMenu= [[UIActionSheet alloc] initWithTitle:nil
                                               delegate:self
                                      cancelButtonTitle:NSLocalizedString(@"BtnCancel", nil)
                                 destructiveButtonTitle:NSLocalizedString(@"BtnDeleteAll", nil)
                                      otherButtonTitles:nil];
        
        [eventMenu showInView:self.view];
    }
}

- (void)getEventsFromTableArray
{
    NSMutableArray *tmpEventsAry = [[NSMutableArray alloc] init];
    if ([dbControl getEventsToArray:tmpEventsAry]) {
        for (EventTable *tmpEV in tmpEventsAry) {
            if ([tmpEV.source_uuid isEqualToString:m_Device.uuid]) {
                if (generalBtn.selected) {
                    if ([tmpEV.event_type rangeOfString:@":"].location == NSNotFound) {
                        [eventList addObject:tmpEV];
                    }
                }
                if (xFleetBtn.selected) {
                    if ([tmpEV.event_type rangeOfString:@":"].location != NSNotFound) {
                        [eventList addObject:tmpEV];
                    }
                }
            }
        }
    }
    [appDelegate.events removeAllObjects];
    [appDelegate.events addObject:[tmpEventsAry retain]];
    [tmpEventsAry release];
    [self resortEventList];
}

- (void)deleteEvent:(EventTable *)tmp2Delete
{
    BOOL result = YES;
    result |= [dbControl deleteEventsFromTable:tmp2Delete table:@"event"];
    
    if (!result) {
        return;
    }
    
    @try {
        [[appDelegate.events firstObject] removeObject:tmp2Delete];
        [eventList removeObject:tmp2Delete];
    } @catch (NSException *exception) {
        DBGLog(@"[ENVC] Delete event failed :%@",exception.reason);
    }
}

- (void)deleteAll
{
    __block BOOL ret = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        ret = [dbControl deleteAllEventsFromTable:m_Device.uuid table:@"event"];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            [NSThread sleepForTimeInterval:1.0f];
            if (!ret) {
                [self showAlarm:@"Delete Failed"];
                return;
            }
            [eventList removeAllObjects];
            [self getEventsFromTableArray];
            [mainTable reloadData];
        });
    });
}

- (void)startQuerryForNewData
{
    [refreshCtrl beginRefreshing];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    [generalBtn setEnabled:NO];
    [xFleetBtn setEnabled:NO];
    blnStop = NO;
    repeatTime = 0;
    
    if (![self checkNetworkStatus])
    {
        [self showAlarm:NSLocalizedString(@"MsgNoInternet", nil)];
        [refreshCtrl endRefreshing];
        [mainTable reloadData];
        return;
    }
    
    streamReceiver = [[StreamReceiver alloc] initWithDevice:m_Device];
    [streamReceiver setDelegate:self];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        if (generalBtn.selected) {
            [streamReceiver getXmsEventFromLocal:DEFAULT_QUERY_EVENT_SEQ byType:STR_XMS_EVTYPE];
        }
        else {
            [streamReceiver getXmsEventFromLocal:DEFAULT_QUERY_EVENT_SEQ byType:STR_XFLEET_EVTYPE];
        }
    });
    
    refreshTimer = [NSTimer scheduledTimerWithTimeInterval:0.2f
                                                    target:self
                                                  selector:@selector(refreshList)
                                                  userInfo:nil
                                                   repeats:YES];
}

- (void)startQuerryForOldData
{
    [indicatorFooter startAnimating];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    [generalBtn setEnabled:NO];
    [xFleetBtn setEnabled:NO];
    blnStop = NO;
    repeatTime = 0;
    
    if (![self checkNetworkStatus])
    {
        [self showAlarm:NSLocalizedString(@"MsgNoInternet", nil)];
        [indicatorFooter stopAnimating];
        [mainTable reloadData];
        return;
    }
    
    streamReceiver = [[StreamReceiver alloc] initWithDevice:m_Device];
    [streamReceiver setDelegate:self];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        if (generalBtn.selected) {
            [streamReceiver getXmsEventFromLocal:DEFAULT_QUERY_EVENT_SEQ byType:STR_XMS_EVTYPE];
        }
        else {
            [streamReceiver getXmsEventFromLocal:DEFAULT_QUERY_EVENT_SEQ byType:STR_XFLEET_EVTYPE];
        }
    });
    
    refreshTimer = [NSTimer scheduledTimerWithTimeInterval:0.2f
                                                    target:self
                                                  selector:@selector(refreshList)
                                                  userInfo:nil
                                                   repeats:YES];
}

- (void)stopQuerry
{
    if (blnStop) {
        return;
    }
    blnStop = YES;
    [refreshTimer invalidate];
    [refreshCtrl endRefreshing];
    [indicatorFooter stopAnimating];
    [tmpQueryList removeAllObjects];
    [generalBtn setEnabled:YES];
    [xFleetBtn setEnabled:YES];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
}

- (void)refreshList
{
    if (!blnStop && repeatTime > 50) {
        [self stopQuerry];
    }
    
    repeatTime++;
//    NSLog(@"%ld",(long)repeatTime);
    [mainTable reloadData];
}

- (void)resortEventList
{
    NSSortDescriptor *sort;
    sort = [NSSortDescriptor sortDescriptorWithKey:@"event_time" ascending:NO];
    
    if (eventList.count) {
        NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:eventList];
        [tmpArray sortUsingDescriptors:[NSArray arrayWithObjects:sort, nil]];
    }
}

- (void)limitEventCount
{
//    while (eventList.count > MAX_SUPPORT_EVENTCOUNT) {
//        EventTable *lastEV = [eventList lastObject];
//        [self deleteEvent:lastEV];
//        [eventList removeObject:lastEV];
//    }
//    [mainTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

- (void)getEventSeqInUserDefault
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *KEY_USERDEFAULT = [NSString stringWithFormat:@"%@-%@", m_Device.uuid, KEY_EVENT_SEQ];
    
    if ([userDefault objectForKey:KEY_USERDEFAULT]) {
        lastQueryEventSeq = [userDefault integerForKey:KEY_USERDEFAULT];
    }
    else
        lastQueryEventSeq = DEFAULT_QUERY_EVENT_SEQ;
}

- (void)updateEventSeqInUserDefaultIfNeeded
{
    if (tmpQueryList.count > 0) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *KEY_USERDEFAULT = [NSString stringWithFormat:@"%@-%@", m_Device.uuid, KEY_EVENT_SEQ];
        EventTable *lastEV = [tmpQueryList lastObject];
        
        if (![userDefault objectForKey:KEY_USERDEFAULT]) {
            [userDefault setInteger:lastEV.event_seq forKey:KEY_USERDEFAULT];
            lastQueryEventSeq = lastEV.event_seq;
            [userDefault synchronize];
            return;
        }
        else
            lastQueryEventSeq = [userDefault integerForKey:KEY_USERDEFAULT];
        
        if (lastQueryEventSeq > lastEV.event_seq) {
            [userDefault setInteger:lastEV.event_seq forKey:KEY_USERDEFAULT];
            lastQueryEventSeq = lastEV.event_seq;
            [userDefault synchronize];
        }
    }
}

- (void)setSearchBarHidden
{
    CGRect newBounds = mainTable.bounds;
    if (mainTable.bounds.origin.y < TOOLBAR_HEIGHT_V) {
        newBounds.origin.y += searchBar.bounds.size.height;
        mainTable.bounds = newBounds;
    }
}

- (void)setTableViewCellHeightAuto
{
    mainTable.estimatedRowHeight = 60.0; // Set your average height
    mainTable.rowHeight = UITableViewAutomaticDimension;
}

- (void)changeEventTextColor:(id)senderCell
{
    // called when the accessory view (disclosure button) is touched
    NSIndexPath *idxPath = [mainTable indexPathForCell:senderCell];
    DBGLog(@"[ENVC] Tap on row %ld", (long)idxPath.row);
    evTable = [eventList objectAtIndex:idxPath.row];
    if (evTable.blnRead == EF_EVENT_READ) {
        evTable.blnRead = EF_EVENT_UNREAD;   //未讀
    }
    else
    {
        evTable.blnRead = EF_EVENT_READ;   //已讀
    }
    
    // save it to database
    [dbControl updateEventFromDatabase:evTable table:@"event"];
    
    [mainTable reloadData];
}

- (void)startLiveOrPlayback:(NSString *)cmd
{
    if (self.xmsView == nil) {
        XMSViewController *viewController = [[XMSViewController alloc] initWithNibName:@"ChannelViewController" bundle:[NSBundle mainBundle]];
        self.xmsView = viewController;
        SAVE_FREE(viewController);
    }
    
    ////////// Get device from database then compare ipaddress with event's source_ip to find out which xms is
    Device *dev = [[Device alloc] initWithProduct:evTable.dev_type];
    
    NSMutableArray *dvrs = [[NSMutableArray alloc] init];
    [dbControl getDevicesToArray:dvrs type:DEV_DVR];
    
    BOOL ret = NO;
    for (Device *tmpDvr in dvrs) {
        if ([tmpDvr.ip isEqualToString:evTable.source_ip]) {
            dev.user = tmpDvr.user;
            dev.password = tmpDvr.password;
            ret = YES;
        }
    }
    
    if (!ret) {
        [self showAlarm:NSLocalizedString(@"MsgDeviceErr", nil)];
        DBGLog(@"[RV] Device not found");
        return;
    }
    
    dev.ip = evTable.source_ip;
    dev.name = evTable.dev_name;
    dev.type = XMS_SERIES;
    dev.streamType = STREAM_XMS;
    
    evTable.blnRead = EF_EVENT_READ;
    
    [self.xmsView.event release];
    self.xmsView.event = [evTable retain];
    self.xmsView.device = dev;
    self.xmsView.blnChSelector = NO;
    self.xmsView.blnEventMode = YES;
    self.xmsView.blnWillOpenSearch = NO;
    
    if ([cmd isEqualToString:@"playback"]) {
        self.xmsView.blnOpenSearch = YES;
        self.xmsView.blnPlaybackMode = YES;
    }
    else {
        self.xmsView.blnOpenSearch = NO;
        self.xmsView.blnPlaybackMode = NO;
    }
    
    // save it to database
    [dbControl updateEventFromDatabase:evTable table:@"event"];
    
    [self.navigationController pushViewController:self.xmsView animated:YES];
}

#pragma mark - Button Action

- (IBAction)buttonAction:(id)sender
{
    NSInteger selectBtn = ((UIButton *)sender).tag;
    evListFilter = selectBtn;
    
    switch (evListFilter) {
        case XMS_EVENT_NORMAL:
            generalBtn.selected = YES;
            xFleetBtn.selected = NO;
            break;
            
        case XMS_EVENT_XFLEET:
            generalBtn.selected = NO;
            xFleetBtn.selected = YES;
            break;
            
        default:
            break;
    }
    
    [eventList removeAllObjects];
    [self getEventsFromTableArray];
    [mainTable reloadData];
}

#pragma mark - XMS Event Handle Process

- (void)xmsEventParser:(NSDictionary *)source
{
    NSDictionary *evDict1, *evDict2;
    
    @try {
        if ([source[@"result"] boolValue]) {
            evDict1 = source[@"data"];
            if (evDict1.count) {
                evDict2 = evDict1[@"data"];
                for (NSDictionary *Info in evDict2) {
                    EventTable *eventQry = [[EventTable alloc] initWithEvent];
                    eventQry.source_uuid = [[evDict1[@"xmsInfo"] objectForKey:@"uuid"] copy];
                    eventQry.source_name = [m_Device.name copy];
                    eventQry.source_ip = [m_Device.ip copy];
                    eventQry.event_seq = [Info[@"Sequence"] integerValue];
                    eventQry.event_time = [Info[@"Timestamp"] integerValue];
                    NSDictionary *eventInfo = Info[@"eventInfo"];
                    if (eventInfo.count) {
                        if ([eventInfo[@"devType"] isEqualToString:@"DVR"]) {
                            eventQry.dev_type = DEV_DVR;
                        }
                        else
                            eventQry.dev_type = DEV_IPCAM;
                        
                        NSString *encodeString = [eventInfo[@"devName"] copy];
                        NSData *decodeData=[[NSData alloc] initWithBase64Encoding:encodeString];
                        eventQry.dev_name = [[NSString alloc] initWithData:decodeData encoding:NSUTF8StringEncoding];
                        eventQry.event_type = [eventInfo[@"eventType"] copy];
                    }
                    NSDictionary *devData = Info[@"devData"];
                    if (devData.count) {
                        eventQry.dev_eid = [devData[@"eid"] integerValue];
                        eventQry.dev_ip = [devData[@"ip"] copy];
                        eventQry.channel = [devData[@"ch"] integerValue]+1;  //devData的channel是從0開始，實際上ch是從1開始
                    }
                    
                    if (![self checkExistEvent:eventQry]) {
                        if ([dbControl addEventToTable:eventQry table:@"event"]) {
                            [[appDelegate.events firstObject] addObject:eventQry];
                            [eventList addObject:eventQry];
                            [tmpQueryList addObject:eventQry];
                            [self resortEventList];
                            [self limitEventCount];
                        }
                    }
                }
            }
        }
    }
    @catch (NSException *exception) {
        RELog(@"[ENVC] Parse Data Exception: %@",exception.reason);
    }
}

- (BOOL)checkExistEvent:(EventTable *)evn
{
    BOOL exist = NO;
    if (generalBtn.selected) {
        if ([evn.event_type rangeOfString:@":"].location == NSNotFound) {
            for (EventTable *existEV in eventList) {
                if ((existEV.event_time == evn.event_time && [existEV.event_type isEqual:evn.event_type]) || (existEV.event_seq == evn.event_seq)) {
                    return exist = YES;
                }
            }
        }
        else
            exist = YES;
    }
    
    if (xFleetBtn.selected) {
        if ([evn.event_type rangeOfString:@":"].location != NSNotFound) {
            for (EventTable *existEV in eventList) {
                if ((existEV.event_time == evn.event_time && [existEV.event_type isEqual:evn.event_type]) || (existEV.event_seq == evn.event_seq)) {
                    return exist = YES;
                }
            }
        }
        else
            exist = YES;
    }
    
    return exist;
}

#pragma mark - Stream Delegate

- (void)xmsEventStatusCallback:(NSDictionary *)data
{
    [self xmsEventParser:data];
    [self updateEventSeqInUserDefaultIfNeeded];
    [self performSelectorOnMainThread:@selector(stopQuerry) withObject:nil waitUntilDone:YES];
}

- (void)EventCallback:(NSString *)msg
{
}

#pragma mark - GestureRecognizer Control

- (void)longPressHandlerOnCell:(UILongPressGestureRecognizer *)gesture
{
    if(gesture.state == UIGestureRecognizerStateBegan)
    {
        UITableViewCell *cell = (UITableViewCell *)gesture.view;
        [self changeEventTextColor:cell];
    }
}

#pragma mark - ScrollView Control

- (void)scrollToTop
{
    NSIndexPath *top = [NSIndexPath indexPathForRow:NSNotFound inSection:0];
    [mainTable scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    [eventList removeAllObjects];
    [self getEventsFromTableArray];
    [self limitEventCount];
    [appDelegate resetBadgeNumber];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (eventList.count == 0) {
        return;
    }
    
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (endScrolling >= scrollView.contentSize.height)
    {
        DBGLog(@"[ENVC] Scroll End Called");
        [self startQuerryForOldData];
    }
}

#pragma mark - AlertView Delegate

- (void)showAlarm:(NSString *)message
{
    if ( message!=nil ) {
        
        // open an alert with just an OK button, touch "OK" will do nothing
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appDelegate._AppName message:message
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"BtnOK", nil) otherButtonTitles:nil];
        
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
    }
}

- (void)showAlarmAndDelete:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"BtnCancel", nil)
                                          otherButtonTitles:NSLocalizedString(@"BtnOK", nil) ,nil];
    
    alert.tag = alert_delete;
    
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
    [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
}

- (void)showAlarmAndSelect:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"BtnCancel", nil)
                                          otherButtonTitles:NSLocalizedString(@"BtnLive", nil),NSLocalizedString(@"BtnPlayback", nil),nil];
    
    alert.tag = alert_select;
    
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
    [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // use "buttonIndex" to decide your action
    switch (actionSheet.tag) {
            
        case alert_delete:
            
            if (buttonIndex == 1) {
                [self deleteAll];
            }
            
            break;
            
        case alert_select:
            
            if (buttonIndex == 1) {  //This is live view
                
                [self startLiveOrPlayback:@"live"];
            }
            else if (buttonIndex == 2) {  //This is playback view
                
                [self startLiveOrPlayback:@"playback"];
            }
            break;
            
        default:
            break;
    }
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self showAlarmAndDelete:[NSString stringWithFormat:@"Are you sure to delete all events?"]];
            break;
            
        default:
            break;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
}

#pragma mark - TableView data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger section = 1;
    return section;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 20)] autorelease];
    [headerView setBackgroundColor:[UIColor BACOLOR]];
    UILabel *lbTitle = [[[UILabel alloc] initWithFrame:headerView.frame] autorelease];
    [lbTitle setBackgroundColor:[UIColor clearColor]];
    [lbTitle setTextColor:[UIColor whiteColor]];
    [lbTitle setFont:[UIFont systemFontOfSize:14]];
    
    NSInteger total_Event_Read = 0;
    NSInteger total_Event_Count = [eventList count];
    for (EventTable *tmpEV in eventList) {
        if (tmpEV.blnRead == EF_EVENT_READ) {
            total_Event_Read++;
        }
    }
    
    if (tableView == searchDisplay.searchResultsTableView)
        [lbTitle setText:[NSString stringWithFormat:@"%@  [%lu] ",NSLocalizedString(@"SearchResults", nil),(unsigned long)searchResult.count]];
    else {
        [lbTitle setText:[NSString stringWithFormat:@"%@  [%ld / %ld] ",NSLocalizedString(@"Event List", nil), (long)(total_Event_Count - total_Event_Read), (long)total_Event_Count]];
    }
    
    [headerView addSubview:lbTitle];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 20.0;
    return height;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    if (tableView == searchDisplay.searchResultsTableView) {
        
        rows = searchResult.count;
    }
    else
        rows = eventList.count;
    
    return rows;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    CGFloat height = 90.0;
//    return height;
//}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        //        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        //        cell.showsReorderControl = YES;
        
        // set background
        [cell.textLabel setTextColor:[UIColor lightTextColor]];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [cell.textLabel setNumberOfLines:0];
        [cell.detailTextLabel setTextColor:[UIColor greenColor]];
        [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
        cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
    }
    
    // Configure the cell.
    EventTable *tmpEV;
    
    if (tableView == searchDisplay.searchResultsTableView)
        tmpEV = [searchResult objectAtIndex:indexPath.row];
    else
    {
        tmpEV = [eventList objectAtIndex:indexPath.row];
    }
    
    if (tmpEV.dev_type == DEV_DVR) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@\n%@ - CH:%ld",tmpEV.event_type,tmpEV.dev_name,(long)tmpEV.channel];
    }
    else
        cell.textLabel.text = [NSString stringWithFormat:@"%@\n%@",tmpEV.event_type,tmpEV.dev_name];
    
    [cell.textLabel setTextColor:(tmpEV.blnRead ? [UIColor whiteColor] : [UIColor redColor])];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    cell.detailTextLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:tmpEV.event_time]];
    [cell.detailTextLabel setTextColor:[UIColor lightTextColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressHandlerOnCell:)];
    [longPressRecognizer setDelegate:self];
    [longPressRecognizer setMinimumPressDuration:1.0];
    [cell addGestureRecognizer:longPressRecognizer];
    SAVE_FREE(longPressRecognizer);
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventTable *evn = (tableView == searchDisplay.searchResultsTableView ? [searchResult objectAtIndex:indexPath.row] : [eventList objectAtIndex:indexPath.row]);
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if (tableView == searchDisplay.searchResultsTableView)
            [searchResult removeObject:evn];
        
//        [tableView beginUpdates];
//        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        DBGLog(@"[RV] Delete Event[%ld]:%@",(long)evn.rowID,evn.dev_name);
        [self deleteEvent:evn];
//        [tableView endUpdates];
        [tableView reloadData];
    }
}

#pragma mark - TableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!blnStop) {
        return;
    }
    
    NSString *strMessage;
    
    if (tableView == searchDisplay.searchResultsTableView)
        evTable = [searchResult objectAtIndex:indexPath.row];
    else
        evTable = [eventList objectAtIndex:indexPath.row];
    
    //DBGLog(@"[RV] Select : %ld/%ld",(long)evTable.rowID,(long)indexPath.row);
    if (evTable.dev_type == DEV_DVR) {
        strMessage = [NSString stringWithFormat:@"%@ - CH:%ld",evTable.dev_name, (long)evTable.channel];
    }
    else
        strMessage = [NSString stringWithFormat:@"%@",evTable.dev_name];
    
    [self showAlarmAndSelect:strMessage];
}

#pragma mark - UISearhDisplay Delegate

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    tableView.backgroundColor = [UIColor BGCOLOR];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                                         objectAtIndex:[self.searchDisplayController.searchBar
                                                                        selectedScopeButtonIndex]]];
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willHideSearchResultsTableView:(UITableView *)tableView
{
    [mainTable reloadData];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text]
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:searchOption]];
    
    return YES;
    
}

-(void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    searchBar.showsCancelButton = YES;
    if (appDelegate.os_version >= 7.0) {
        
    }else {
        
        CGRect frame = mainTable.frame;
        frame.origin.y -= TOOLBAR_HEIGHT_V;
        [mainTable setFrame:frame];
    }
    
    if (mainTable.editing)
        [self setEditing:NO animated:YES];
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    if (appDelegate.os_version >= 7.0) {
        
    }else {
        CGRect frame = mainTable.frame;
        frame.origin.y += TOOLBAR_HEIGHT_V;
        [mainTable setFrame:frame];
    }
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [searchResult removeAllObjects];
    
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF.dev_name contains[cd] %@",
                                    searchText];
    
    [searchResult addObjectsFromArray:[eventList filteredArrayUsingPredicate:resultPredicate]];
}

#pragma mark - Network Detector

- (BOOL)checkNetworkStatus
{
    BOOL ret = NO;
    
    Reachability *deviceReach = [Reachability reachabilityWithHostName:m_Device.ip];
    NetworkStatus netStatus = [deviceReach currentReachabilityStatus];
    
    if (netStatus != NotReachable) {
        ret = YES;
    }
    
    return ret;
}

@end
