//
//  ShareViewController.m
//  EFViewerHD
//
//  Created by Nobel on 13/10/1.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import "ExportViewController.h"

@interface ExportViewController()
{
    NSInteger       popType;
}

- (void)exportDeviceList;
- (void)closeView;

- (NSString *)encodeString:(NSString *)string;
- (void)showAlarm:(NSString *)message;

@end

@implementation ExportViewController

@synthesize delegate,qrView;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MMLog(@"[EPV] Init %@",self);
    devList = [[NSMutableArray alloc] init];
    checkList = [[NSMutableArray alloc] init];
    
    [btnSelectAll setTitle:NSLocalizedString(@"BtnSelectAll", nil)];
    [btnDeselect setTitle:NSLocalizedString(@"BtnDeselectAll", nil)];
    
    //Set Left nav button : Cancel button
    UIBarButtonItem *closeBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(closeView)];
    [self.navigationItem setLeftBarButtonItem:closeBtn];
    [closeBtn release];
    
    //Set Right nav button : export button
    UIBarButtonItem *exportBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"BtnExport", nil)
                                                                  style:UIBarButtonItemStyleBordered
                                                                 target:self
                                                                 action:@selector(exportDeviceList)];
    [self.navigationItem setRightBarButtonItem:exportBtn];
    [exportBtn release];
    
    //load devlist
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    for (NSMutableArray *tmpArray in appDelegate.dvrs)
        [devList addObjectsFromArray:tmpArray];
    
    for (NSMutableArray *tmpArray in appDelegate.ipcams)
        [devList addObjectsFromArray:tmpArray];
    
    NSInteger idx = 0;
    for (Device *dev in devList)
        dev.rowID = idx++;
    
    popType = epvt_export;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [devTableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.preferredContentSize = POPVIEW_FRAME.size;
}

- (void)dealloc
{
    MMLog(@"[EPV] dealloc %@",self);
    
    [devList removeAllObjects];
    [checkList removeAllObjects];
    
    if (devList) {
        [devList release];
        devList = nil;
    }
    if (checkList) {
        [checkList release];
        checkList = nil;
    }
    if (self.qrView)
        [self.qrView release];
    
    [btnSelectAll release];
    btnSelectAll = nil;
    
    [btnDeselect release];
    btnDeselect = nil;
    
    [devTableView release];
    devTableView = nil;
    
    self.delegate = nil;
    
    [super dealloc];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action

- (void)exportDeviceList
{
    if (checkList.count == 0 || checkList.count > 16) {
        NSString *msgErr = checkList.count==0 ? @"MsgNoSelect" : @"MsgQRMax";
        [self showAlarm:NSLocalizedString(msgErr, nil)];
        return;
    }

    //the qrcode is square. now we make it 250 pixels wide
    int qrcodeImageDimension = 300;
    
    //the string can be very long
    NSString *devCode = @"<MFQR>";
    for (Device *dev in checkList) {
        NSString *devCmd = [NSString stringWithFormat:@"<DEVICE>%@|%@|%ld|%ld|%ld|%ld|%@|%@|%ld",dev.name,dev.ip,(long)dev.port,(long)dev.type,(long)dev.streamType,(long)dev.product,dev.user,dev.password,(long)dev.rtspPort];
        devCode = [devCode stringByAppendingString:devCmd];
    }
    NSString *encStr = [self encodeString:devCode];
    
    //first encode the string into a matrix of bools, TRUE for black dot and FALSE for white. Let the encoder decide the error correction level and version
    DataMatrix* qrMatrix = [QREncoder encodeWithECLevel:QR_ECLEVEL_AUTO version:QR_VERSION_AUTO string:encStr];
    //then render the matrix
    UIImage* qrcodeImage = [QREncoder renderDataMatrix:qrMatrix imageDimension:qrcodeImageDimension];
    
    if (self.qrView == nil) {
        QRViewController *viewController = [[QRViewController alloc] initWithNibName:@"QRViewController"
                                                                              bundle:[NSBundle mainBundle]];
        self.qrView = viewController;
        [viewController release];
    }
    
    self.qrView.qrImage = qrcodeImage;
    self.qrView.delegate = self.delegate;
    [self.navigationController pushViewController:self.qrView animated:YES];
}

- (void)closeView
{
    [self.delegate dismissPopView:popType];
}

- (IBAction)selectAll
{
    [checkList removeAllObjects];
    
    for (NSInteger i=0; i<devList.count; i++) {
        
        NSIndexPath *tmpPath = [NSIndexPath indexPathForRow:i inSection:0];
        [[devTableView delegate] tableView:devTableView didSelectRowAtIndexPath:tmpPath];
    }
}

- (IBAction)deselectAll
{
    [checkList removeAllObjects];
    [devTableView reloadData];
}

#pragma mark - String Encoding

- (NSString *)encodeString:(NSString *)string
{
    
    if (string == nil)  return string;
    
    NSMutableString *resultString = [[[NSMutableString alloc] init] autorelease];
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    const Byte *byteArray = (const Byte *)[data bytes];
    int length = [data length];
    
    for (int i=0; i < length; i++)  {
        Byte byteValue = byteArray[i];
        byteValue ^= 117;
        
        [resultString appendFormat:@"%02x", byteValue];
    }
    
    return resultString;
}

#pragma mark - Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
    return devList.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 20)] autorelease];
    [headerView setBackgroundColor:[UIColor BACOLOR]];
    UILabel *lbTitle = [[[UILabel alloc] initWithFrame:headerView.frame] autorelease];
    [lbTitle setBackgroundColor:[UIColor clearColor]];
    [lbTitle setTextColor:[UIColor whiteColor]];
    [lbTitle setFont:[UIFont systemFontOfSize:14]];
    [lbTitle setText:[NSString stringWithFormat:@"%@  [%lu/%lu] ",NSLocalizedString(@"DeviceList", nil), (unsigned long)checkList.count, (unsigned long)devList.count]];
    
    [headerView addSubview:lbTitle];
    
    return headerView;
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
		cell.showsReorderControl = YES;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		// set background
        cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.backgroundColor = [UIColor clearColor];
		cell.detailTextLabel.backgroundColor = [UIColor clearColor];
        cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
        
        // set label style
        [cell.textLabel setTextColor:[UIColor EFCOLOR]];
        [cell.detailTextLabel setTextColor:[UIColor lightTextColor]];
        
        UIImageView *checkBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addDev1.png"]];
        [checkBtn setFrame:CGRectMake(0, 0, 27, 27)];
        [cell setAccessoryView:checkBtn];
        [checkBtn release];
        [cell.accessoryView setHidden:YES];
    }
    
	// Configure the cell.
	Device *dev = [devList objectAtIndex:indexPath.row];
	
	cell.textLabel.text = dev.name;
	
    BOOL bFind = NO;
    for (Device *tmpDev in checkList)
    {
        if (dev.rowID == tmpDev.rowID) {
            bFind = YES;
            break;
        }
    }
    
    [cell.accessoryView setHidden:!bFind];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Device *dev = [devList objectAtIndex:indexPath.row];
    if (checkList.count == 0) {
        
        [checkList addObject:dev];
    }else {
        
        BOOL bFind = NO;
        for (Device *tmpDev in checkList) {
            if ([tmpDev isEqual:dev]) {
                
                [checkList removeObject:dev];
                bFind = YES;
                break;
            }
        }
        
        if (!bFind)
            [checkList addObject:dev];
    }
    
    [devTableView reloadData];
}

#pragma mark - Alarm Delegate

- (void)showAlarm:(NSString *)message
{
    if ( message!=nil ) {
		
		// open an alert with just an OK button, touch "OK" will do nothing
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"BtnOK", nil) otherButtonTitles:nil];
		
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];[alert show];
	}
}

@end
