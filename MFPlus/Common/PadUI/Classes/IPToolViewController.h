//
//  IPToolViewController.h
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/3.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
#import "UDPSender.h"
#import "WS-Discovery.h"

@interface IPToolViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView        *mainTable;
    IBOutlet UILabel            *decLabel;
    IBOutlet UIView             *maskView;
    IBOutlet UIActivityIndicatorView    *loadAct;
    Device                      *device;
    UDPSender                   *udpSender;
    WS_Discovery                *ws_discovery;
    NSMutableArray              *deviceList;
    NSMutableArray              *udp_deviceList;
    
    NSTimer                     *refreshTimer;
    NSInteger                   repeatTime;
}

@property(nonatomic,retain) Device      *device;
@property(nonatomic)        NSInteger   devListFilter;

- (void)scanStart;
- (void)scanStop;
- (void)refreshList;

@end
