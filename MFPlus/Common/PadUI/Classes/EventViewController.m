//
//  EventViewController.m
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/30.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import "EventViewController.h"

@interface EventViewController ()

- (UIImage *)eventParser:(NSInteger)type;
- (void)searchProcess;
- (void)showMaskView:(BOOL)blnShow;

@end

@implementation EventViewController

@synthesize streamReceiver,uSearchST,uSearchET,eventFilter;
@synthesize evDelegate;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MMLog(@"[EV] Init %@",self);
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    maskView = [[UIView alloc] initWithFrame:self.view.frame];
    [maskView setBackgroundColor:[UIColor lightTextColor]];
    loadActivity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [maskView addSubview:loadActivity];
    [self.view addSubview:maskView];
    [maskView setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.preferredContentSize = POPVIEW_FRAME.size;
    
    eventList = streamReceiver.SearchResultlist;
    
    if (streamReceiver.m_DiskGMT)
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:streamReceiver.m_DiskGMT]];
    
    [mainTable reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [maskView setFrame:mainTable.frame];
    [loadActivity setCenter:maskView.center];
    
    [NSThread detachNewThreadSelector:@selector(searchProcess)
                             toTarget:self
                           withObject:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    blnSearching = NO;
    while (streamReceiver.blnGetSearchInfo) {
        [NSThread sleepForTimeInterval:0.5f];
    }
    
    [eventList removeAllObjects];
    eventList = nil;
}

- (void)dealloc
{
    MMLog(@"[EV] dealloc %@",self);
    
    self.streamReceiver = nil;
    self.evDelegate = nil;
    
    [dateFormatter release];
    dateFormatter = nil;
    
    [loadActivity release];
    [maskView release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Event Process

- (void)searchProcess
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

    [self showMaskView:YES];
    streamReceiver.blnGetSearchInfo = blnSearching = YES;
    [streamReceiver getSearchIDFrom:uSearchST To:uSearchET byFilter:eventFilter];
    
    if (streamReceiver.m_intSearchResultTag == 1) {
        
        streamReceiver.m_intSearchProgress = 0;
        while (streamReceiver.m_intSearchResultTag==1 && streamReceiver.m_intSearchProgress<100
               && streamReceiver.blnGetSearchInfo && blnSearching)
        {
            [NSThread sleepForTimeInterval:1.0f];
            [streamReceiver getEventResult];
            [mainTable performSelectorOnMainThread:@selector(reloadData)
                                        withObject:nil waitUntilDone:YES];
        }
    }
    
    streamReceiver.blnGetSearchInfo = NO;
    [self showMaskView:NO];

    [mainTable performSelectorOnMainThread:@selector(reloadData)
                                withObject:nil waitUntilDone:YES];
    
    [pool release];
}

- (UIImage *)eventParser:(NSInteger)type
{
    UIImage *returnImg;
    
    switch (type) {
        case EVENT_ALARM:
            returnImg = IMG_ALARM;
            break;
        case EVENT_MOTION:
            returnImg = IMG_MOTION;
            break;
        case EVENT_VLOSS:
            returnImg = IMG_VLOSS;
            break;
    }
    
    return returnImg;
}

- (void)showMaskView:(BOOL)blnShow
{
    if (blnShow) {
        [maskView setHidden:NO];
        [loadActivity performSelectorOnMainThread:@selector(startAnimating)
                                       withObject:nil
                                    waitUntilDone:NO];
    }else {
        [maskView setHidden:YES];
        [loadActivity performSelectorOnMainThread:@selector(stopAnimating)
                                       withObject:nil
                                    waitUntilDone:NO];
    }
}

#pragma mark - TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return eventList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 25, tableView.bounds.size.width, 30)] autorelease];
    [headerView setBackgroundColor:[UIColor BACOLOR]];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.bounds.size.width-10, 18)] autorelease];
    [titleLabel setText:[NSString stringWithFormat:@"%@ (%lu)",NSLocalizedString(@"EventList", nil),(unsigned long)eventList.count]];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [cell setBackgroundColor:[UIColor clearColor]];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setTextColor:[UIColor EFCOLOR]];
        [cell.detailTextLabel setTextColor:[UIColor lightTextColor]];
    }
    
    if (eventList.count > 0) {
        
        SearchResultEntry *tmpEntry = [eventList objectAtIndex:indexPath.row];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        CameraInfo *tmpInfo = [streamReceiver getInfoFromChannelIndex:tmpEntry.uiChannel];
        
        cell.textLabel.text = tmpInfo ? tmpInfo.title : [NSString stringWithFormat:@"CH %d",tmpEntry.uiChannel+1];
        cell.detailTextLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:tmpEntry.uiStartTime]];
        cell.imageView.image = [self eventParser:tmpEntry.uiEventType];
    }else {
        cell.textLabel.text = nil;
        cell.detailTextLabel.text = nil;
        cell.imageView.image = nil;
    }
    
    return cell;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchResultEntry *tmpEntry = [eventList objectAtIndex:indexPath.row];
    [evDelegate playbackEvent:tmpEntry];
}

@end
