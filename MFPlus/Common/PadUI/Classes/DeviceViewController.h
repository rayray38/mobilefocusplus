//
//  DeviceViewController.h
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/1.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFViewerAppDelegate.h"
#import "DeviceEditViewController.h"
#import "IPToolViewController.h"
#import "StreamReceiver.h"

@interface DeviceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView    *mainTable;
    NSArray                 *devTypeArray;
    StreamReceiver          *streamReceiver;
}

@property(nonatomic,assign) id<contentDelegate> delegate;
@property(nonatomic)        BOOL                blnNewDevice;
@property(nonatomic,retain) Device              *device;
@property(nonatomic,retain) Device              *deviceCopy;
@property(nonatomic,retain) DeviceEditViewController *editView;
@property(nonatomic,retain) IPToolViewController     *scanView;

@end
