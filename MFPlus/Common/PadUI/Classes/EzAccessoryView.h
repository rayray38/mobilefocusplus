//
//  EzAccessoryView.h
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/11/28.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import <Foundation/Foundation.h>

/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
typedef enum
{
    edt_none = 0,
    edt_button,
    edt_switch,
}EF_DETAIL_TYPE;

typedef enum
{
    edm_alarmPop = 0x10,
    edm_smoothmode,
    
}EF_DETAIL_MODE;

@protocol EZViewDelegate
@required
- (void)detailStatusCallback:(NSString *)message;
@end

@interface EZAccessoryView : UIView
@property(nonatomic,assign) id<EZViewDelegate> delegate;
@property(nonatomic,retain) UIButton    *detailButton;
@property(nonatomic,retain) UISwitch    *detailSwitch;

- (void)setDetailType:(NSInteger)_type;
- (void)changeDetailSetting:(id)sender;
+ (BOOL)getDetailSettingBy:(NSString *)key;
+ (void)changeDetailSetting:(BOOL)isOn with:(NSString *)key;

@end
