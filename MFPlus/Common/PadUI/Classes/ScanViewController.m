//
//  ScanViewController.m
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/9.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import "ScanViewController.h"

@interface ScanViewController ()
{
    NSInteger       popType;
}

- (void)openLibrary;
- (BOOL)validateQRCode:(NSString *)code;
- (NSString *)decodeString:(NSString *)string;

- (void)closeView;
- (void)relocateReaderOrientation:(UIInterfaceOrientation)toInterfaceOrientation;
- (void)showAlarm:(NSString *)message;

@end

@implementation ScanViewController

@synthesize delegate,mode,importView;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MMLog(@"[SCV] Init %@",self);
    
    UIBarButtonItem *closeBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                              target:self
                                                                              action:@selector(closeView)];
    [self.navigationItem setLeftBarButtonItem:closeBtn];
    [closeBtn release];
    
    [ZBarReaderView class];
    readerView.readerDelegate = self;
    
    popType = epvt_scan;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self relocateReaderOrientation:[self interfaceOrientation]];
}

- (void)viewDidAppear:(BOOL)animated
{
    self.preferredContentSize = POPVIEW_FRAME.size;
    mode == 0 ? [readerView start] : [self openLibrary];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (mode == 0)
        [readerView stop];
}

- (void)dealloc
{
    MMLog(@"[SCV] dealloc %@",self);
    
    [readerView release];
    
    [self.importView release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action

- (void)openLibrary
{
    ZBarReaderController *reader = [ZBarReaderController new];
    reader.readerDelegate = self;
    [reader.scanner setSymbology:ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 1];
    reader.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;

    reader.showsZBarControls = NO;
    reader.showsHelpOnFail = NO;
    
    [reader.navigationBar setHidden:YES];
    CGRect thisFrame = self.view.frame;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] > 7.0)
        thisFrame.origin.y -= TOOLBAR_HEIGHT_V;
    else
        thisFrame.size = POPVIEW_FRAME.size;
    [reader.view setFrame:thisFrame];
    [self.view addSubview:reader.view];
}

- (void)closeView
{
    [delegate dismissPopView:popType];
}

- (void)relocateReaderOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        readerView.previewTransform = CGAffineTransformMakeRotation(M_PI_2);
    }else {
        readerView.previewTransform = CGAffineTransformMakeRotation(-M_PI_2);
    }
}

#pragma mark - Device Encode

- (BOOL)validateQRCode:(NSString *)code
{
    BOOL ret = NO;
    
    NSString *decCode = [self decodeString:code];
    NSString *marker = [decCode substringToIndex:6];
    if ([marker isEqualToString:@"<MFQR>"]) {
        ret = YES;
    }
    
    return ret;
}

- (NSString *)decodeString:(NSString *)string
{
    //  Convert incoming string to ASCII data first
    NSData *data = [string dataUsingEncoding:NSASCIIStringEncoding];
    const Byte *incomingArray = (const Byte *)[data bytes];
    
    //  Allocate decode buffer
    int charCount = [string length]/2;
    Byte *byteArray = (Byte *)malloc(charCount);
    
    int sourceOffset = 0;
    int targetOffset = 0;
    for (int i=0; i<charCount; i++)
    {
        Byte highByte = incomingArray[sourceOffset+0];
        Byte lowByte = incomingArray[sourceOffset+1];
        sourceOffset += 2;
        
        if (highByte >= '0' && highByte <= '9')  {highByte -= '0';}
        if (highByte >= 'a' && highByte <= 'f')  {highByte -= ('a'-10);}
        
        if (lowByte >= '0' && lowByte <= '9')  {lowByte -= '0';}
        if (lowByte >= 'a' && lowByte <= 'f')  {lowByte -= ('a'-10);}
        
        Byte charValue = (highByte<<4)|lowByte;
        
        //  Decryption
        charValue ^= 117;
        
        byteArray[targetOffset] = charValue;
        targetOffset++;
    }
    
    //  Convert byte array to NSString
    NSString *decodedString = [[[NSString alloc] initWithBytes:byteArray
                                                       length:charCount
                                                     encoding:NSUTF8StringEncoding] autorelease];
    free(byteArray);
    
    return decodedString;
}

#pragma mark - ZBar Delegate

- (void) readerView:(ZBarReaderView*)view
     didReadSymbols:(ZBarSymbolSet*)syms
          fromImage:(UIImage*)img
{
    [readerView stop];
    
    // do something useful with results
    ZBarSymbol *symbol;
    for(symbol in syms)
        break;
    
    if ([self validateQRCode:symbol.data]) {
        
        if (!self.importView) {
            ImportViewController *viewController = [[ImportViewController alloc] initWithNibName:@"ImportViewController" bundle:[NSBundle mainBundle]];
            self.importView = viewController;
            [viewController release];
        }
        
        self.importView.resultString = [self decodeString:symbol.data];
        self.importView.delegate = self.delegate;
        [self.navigationController pushViewController:self.importView animated:YES];
        
    }else {
        [self showAlarm:NSLocalizedString(@"MsgInvalidQR", nil)];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];
    ZBarSymbol * symbol;
    for(symbol in results)
        break;
    
    if ([self validateQRCode:symbol.data]) {
        
        if (!self.importView) {
            ImportViewController *viewController = [[ImportViewController alloc] initWithNibName:@"ImportViewController" bundle:[NSBundle mainBundle]];
            self.importView = viewController;
            [viewController release];
        }
        
        self.importView.resultString = [self decodeString:symbol.data];
        self.importView.delegate = self.delegate;
        [self.navigationController pushViewController:self.importView animated:YES];
        
    }else {
        [self showAlarm:NSLocalizedString(@"MsgInvalidQR", nil)];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self closeView];
}

#pragma mark - Alarm Delegate

- (void)showAlarm:(NSString *)message
{
    if ( message!=nil ) {
		
		// open an alert with just an OK button, touch "OK" will do nothing
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];[alert show];
	}
}

@end
