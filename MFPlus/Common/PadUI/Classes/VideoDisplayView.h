//
//  VideoDisplayView.h
//  EFViewerHD
//
//  Created by James Lee on 12/10/8.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "EFViewerAppDelegate.h"

#define REC_IMG     [UIImage imageNamed:@"rec.png"]
#define EMPTY_CHANNEL 99

@interface VideoDisplayView : NSObject
{
    
	UIImageView *mainView;
    UILabel     *lblTitle;  //add by robert hsu 20120105 for show Camera title
	UILabel     *infoLabel;
	UIImageView *videoImageView;
	UIActivityIndicatorView *activityIndicator;
    UIButton    *closeBtn;
    UIImageView *recImageView;
    NSInteger   iTmp;
    NSInteger   playingChannel;
    BOOL        blnIsPlay;
    BOOL        blnIsRec;
}

- (id)initWithView:(UIImageView *)view button:(UIButton *)btn;
- (void)setVideoImage:(UIImage *)image;
- (void)setInfoText:(NSString *)strTitle Time:(NSString *) strVideoTime;
- (void)startActivityIndicatorAnimating;
- (void)stopActivityIndicatorAnimating;
- (void)resetView:(BOOL)animated isFull:(BOOL)blnFull;
- (void)cleanView;
- (BOOL)isIndicatorAnimating;
- (void)showView:(BOOL)blnShow;    //add by robert hsu 20120106 for multiview
- (void)setFitScale:(BOOL)blnFit;  //add by robert hsu 20120109 for multiview not need to fit scale bue singil channel mode need
- (UIImage *)videoImage;

@property(nonatomic, retain) UIImageView *videoImageView;
@property(nonatomic, retain) UIImageView *mainView;
@property(nonatomic, retain) UILabel     *lblTitle;
@property(nonatomic, retain) UILabel     *infoLabel;
@property(nonatomic, retain) UIButton    *closeBtn;
@property(nonatomic, retain) UIImageView *recImageView;
@property(nonatomic)         NSInteger   playingChannel; // record current playing channel
@property(nonatomic)         BOOL        blnIsPlay;
@property(nonatomic)         BOOL        blnIsRec;

@end
