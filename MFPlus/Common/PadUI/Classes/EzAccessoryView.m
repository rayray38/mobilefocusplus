//
//  EzAccessoryView.m
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/11/28.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import "EzAccessoryView.h"

@implementation EZAccessoryView

@synthesize delegate;
@synthesize detailButton,detailSwitch;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat xPos = ([[[UIDevice currentDevice] systemVersion] floatValue] > 7.0) ? -20 : -40;
        
        self.detailButton = [[UIButton alloc] initWithFrame:self.frame];
        [self.detailButton setBackgroundColor:[UIColor clearColor]];
        [self.detailButton setImage:[UIImage imageNamed:@"detail.png"] forState:UIControlStateNormal];
        
        self.detailSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(xPos, 0, 30, 30)];
        
        [self addSubview:self.detailButton];
        [self addSubview:self.detailSwitch];
    }
    
    return self;
}

- (void)setDetailType:(NSInteger)_type
{
    switch (_type) {
        case edt_none:
            [self.detailButton setHidden:YES];
            [self.detailSwitch setHidden:YES];
            break;
        case edt_button:
            [self.detailButton setHidden:NO];
            [self.detailSwitch setHidden:YES];
            break;
        case edt_switch:
            [self.detailButton setHidden:YES];
            [self.detailSwitch setHidden:NO];
            break;
    }
}

#pragma mark - Config Setter/Getter

- (void)changeDetailSetting:(id)sender
{
    UISwitch *pSwitch = (UISwitch *)sender;
    
    NSString *_key = NULL;
    switch (pSwitch.tag) {
        case edm_alarmPop:
            _key = KEY_ALARMPOP;
            break;
        case edm_smoothmode:
            _key = KEY_SMOOTHMODE;
            break;
    }
    
    NSUserDefaults  *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:pSwitch.isOn forKey:_key];
    [userDefault synchronize];
    
    if (pSwitch.tag == edm_alarmPop)
        [delegate detailStatusCallback:[NSString stringWithFormat:@"%@:%@",KEY_ALARMPOP,pSwitch.isOn?@"ON":@"OFF"]];
}

+ (BOOL)getDetailSettingBy:(NSString *)key
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault boolForKey:key];
}

+ (void)changeDetailSetting:(BOOL)isOn with:(NSString *)key
{
    NSUserDefaults  *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:isOn forKey:key];
    [userDefault synchronize];
}

@end