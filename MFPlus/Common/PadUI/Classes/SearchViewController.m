//
//  SearchViewController.m
//  EFViewerHD
//
//  Created by James Lee on 13/3/7.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import "SearchViewController.h"
#import "CameraInfo.h"

@interface SearchViewController()
{
    NSInteger       popType;
}

- (void)closeView;
- (void)openPlaybackBy:(NSUInteger)_Time with:(NSInteger)_Channel;
- (void)startPlayback;
- (void)searchEvent;
- (void)selectEvent:(id)sender;
- (void)showAlarm:(NSString *)message;
- (void)checkDST_NV;

@end

@implementation SearchViewController

@synthesize streamReceiver,searchDelegate,delegate,searchType,eventView,playingChannel;

#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    MMLog(@"[SV] Init %@",self);
    
    //Set Left button : Close button
    UIBarButtonItem *closeBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                              target:self
                                                                              action:@selector(closeView)];
    [self.navigationItem setLeftBarButtonItem:closeBtn];
    SAVE_FREE(closeBtn);
    
    //Init dateformatter
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    //Init datePicker
    [datePicker setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [datePicker setBackgroundColor:[UIColor whiteColor]];
    
    CGSize sizePicker = datePicker.frame.size;
    [datePicker setFrame:CGRectMake(0, POPVIEW_FRAME.size.height-sizePicker.height, POPVIEW_FRAME.size.width, sizePicker.height)];
    [pickerBar setFrame:CGRectMake(0, datePicker.frame.origin.y-TOOLBAR_HEIGHT_V, POPVIEW_FRAME.size.width, TOOLBAR_HEIGHT_V)];
    [self.view addSubview:pickerMenu];
    
    popType = epvt_search;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.preferredContentSize = POPVIEW_FRAME.size;
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    if (strHddST) {
        [strHddST release];
        strHddST = nil;
    }
    if (strHddET) {
        [strHddET release];
        strHddET = nil;
    }
    
    // calculate DST
    [self checkDST_NV];
    
    if (streamReceiver.m_intdlsEnable == 1)
    {
        if (streamReceiver.m_DiskGMT)
        {
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:streamReceiver.m_DiskGMT]];
            [datePicker setTimeZone:[NSTimeZone timeZoneWithAbbreviation:streamReceiver.m_DiskGMT]];
        }
        NSDate *tmpDate = [NSDate dateWithTimeIntervalSince1970:streamReceiver.m_intDiskStartTime];
        strHddST = [[dateFormatter stringFromDate:tmpDate] copy];
        [datePicker setMinimumDate:tmpDate];
        tmpDate = [NSDate dateWithTimeIntervalSince1970:streamReceiver.m_intDiskEndTime + streamReceiver.m_dst_time_sec];
        strHddET = [[dateFormatter stringFromDate:tmpDate] copy];
        [datePicker setMaximumDate:tmpDate];
        
        //Set default select time
        uSelectST = uStartTime = streamReceiver.m_intDiskEndTime + streamReceiver.m_dst_time_sec - 5*60;
        uSelectET = uEndTime = streamReceiver.m_intDiskEndTime + streamReceiver.m_dst_time_sec;
        [datePicker setDate:[NSDate dateWithTimeIntervalSince1970:uSelectST]];
    }
    else
    {
        if (streamReceiver.m_DiskGMT)
        {
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:streamReceiver.m_DiskGMT]];
            [datePicker setTimeZone:[NSTimeZone timeZoneWithAbbreviation:streamReceiver.m_DiskGMT]];
        }
        NSDate *tmpDate = [NSDate dateWithTimeIntervalSince1970:streamReceiver.m_intDiskStartTime];
        strHddST = [[dateFormatter stringFromDate:tmpDate] copy];
        [datePicker setMinimumDate:tmpDate];
        tmpDate = [NSDate dateWithTimeIntervalSince1970:streamReceiver.m_intDiskEndTime];
        strHddET = [[dateFormatter stringFromDate:tmpDate] copy];
        [datePicker setMaximumDate:tmpDate];
        
        //Set default select time
        uSelectST = uStartTime = streamReceiver.m_intDiskEndTime - 5*60;
        uSelectET = uEndTime = streamReceiver.m_intDiskEndTime;
        [datePicker setDate:[NSDate dateWithTimeIntervalSince1970:uSelectST]];
    }
    
    //reset parameters
    eventFilter = 0;
    
    [pickerMenu setHidden:YES];
    
    [mainTable reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    MMLog(@"[SV] dealloc %@",self);
    
    self.streamReceiver = nil;
    self.delegate = nil;
    self.searchDelegate = nil;
    
    if (self.eventView)
        [self.eventView release];

    [strHddST release];
    strHddST = nil;
    [strHddET release];
    strHddET = nil;
    
    [dateFormatter release];
    dateFormatter = nil;
    
    [mainTable release];
    mainTable = nil;
    
    [super dealloc];
}

#pragma mark - Action

- (void)closeView
{
    [delegate dismissPopView:popType];
}

- (void)startPlayback
{
//    [self openPlaybackBy:uSelectST with:((CameraInfo *   )[streamReceiver.cameraList firstObject]).index-1];
    [self openPlaybackBy:uSelectST with:self.playingChannel];
}

- (void)openPlaybackBy:(NSUInteger)_Time with:(NSInteger)_Channel
{
    DBGLog(@"[SV] OpenPlayback Channel:%ld, Time:%lu",(long)_Channel,(unsigned long)_Time);
    streamReceiver.m_blnPlaybackMode = YES;
    streamReceiver.m_intPlaybackStartTime = _Time;
    [searchDelegate playbackByMask:_Channel];
    [self closeView];
}

- (void)searchEvent
{
    if (self.eventView == nil) {
        EventViewController *viewController = [[EventViewController alloc] initWithNibName:@"EventViewController" bundle:[NSBundle mainBundle]];
        self.eventView = viewController;
        [viewController release];
    }
    self.eventView.streamReceiver = streamReceiver;
    self.eventView.evDelegate = self;
    self.eventView.uSearchST = uSelectST;
    self.eventView.uSearchET = uSelectET;
    self.eventView.eventFilter = eventFilter;
    
    [self.navigationController pushViewController:self.eventView animated:YES];
}

- (void)selectEvent:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    if (btn.tag & eventFilter) {
        eventFilter ^= btn.tag;
        [btn setImage:IMG_UNCHECK forState:UIControlStateNormal];
    }else {
        eventFilter |= btn.tag;
        [btn setImage:IMG_CHECK forState:UIControlStateNormal];
    }
}

- (void)TimeSelected
{
    if (pickerIdx/2)   // 0,1:uSelectST; 2,3:uSelectET
        uSelectET = [datePicker.date timeIntervalSince1970];
    else
        uSelectST = [datePicker.date timeIntervalSince1970];
    
    //[self checkDST_NV];
    
    [pickerMenu setHidden:YES];
    [mainTable reloadData];
}

- (void)checkDST_NV
{
    if (streamReceiver.m_intdlsEnable == 1)
    {
        NSUInteger currentZoneTime = streamReceiver.m_intCurrentTime + streamReceiver.m_DiskGMT_sec;
        NSUInteger weekDayPlus,weekDayPlus1;
        NSDate *tmpDate = [NSDate dateWithTimeIntervalSince1970:streamReceiver.m_intCurrentTime];
        
        // Get Current Year
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy"];
        NSString *currentYear = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:tmpDate]];
        
        // Get startTime and adjustTime
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSUInteger startMonth = streamReceiver.m_intStartMonth;
        NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@-%lu-01 00:00:00",currentYear,(unsigned long)startMonth +1]];
        
        // Get days in month
        NSCalendar *cal = [NSCalendar currentCalendar];
        NSRange rng1 = [cal rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:date];
        NSUInteger daysofStartMonth = rng1.length;
        //NSLog(@"[SV] Numbers Of Days in Month : %lu",(unsigned long)daysofStartMonth);
        
        NSDateComponents *comps = [cal components:NSWeekdayCalendarUnit fromDate:date];
        NSUInteger weekday = [comps weekday] - 1;
        if (streamReceiver.m_intStartWeekday >= weekday)
        {
            streamReceiver.m_intStartWeek = streamReceiver.m_intStartWeek;
            weekDayPlus = streamReceiver.m_intStartWeekday - weekday;
        }
        else
        {
            streamReceiver.m_intStartWeek = streamReceiver.m_intStartWeek + 1;
            weekDayPlus = streamReceiver.m_intStartWeekday - weekday;
        }
        
        NSUInteger startDate = (weekDayPlus + 1) + 7 * streamReceiver.m_intStartWeek;
        if (startDate > daysofStartMonth)
        {
            startDate = startDate - 7;
        }
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSDate *dateStart = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@-%lu-%lu %lu:%lu",currentYear,(unsigned long)startMonth +1,(unsigned long)startDate,(unsigned long)streamReceiver.m_intStartHour,(unsigned long)streamReceiver.m_intStartMin]];
        NSTimeInterval timeInterval = [dateStart timeIntervalSince1970];
        NSUInteger startTime = fabs(timeInterval);
        
        NSUInteger adjustTime = ((streamReceiver.m_intSetHour - streamReceiver.m_intStartHour) *60 + streamReceiver.m_intSetMin - streamReceiver.m_intStartMin) *60 *1000;
        
        // Get endTime
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSUInteger endMonth = streamReceiver.m_intEndMonth;
        NSDate *dateEnd = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@-%lu-01 00:00:00",currentYear,(unsigned long)endMonth +1]];
        NSRange rng2 = [cal rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:dateEnd];
        NSUInteger daysofEndMonth = rng2.length;
        
        NSDateComponents *comps2 = [cal components:NSWeekdayCalendarUnit fromDate:dateEnd];
        NSUInteger weekday1 = [comps2 weekday] - 1;
        if (streamReceiver.m_intStartWeekday >= weekday1)
        {
            streamReceiver.m_intEndWeek = streamReceiver.m_intEndWeek;
            weekDayPlus1 = streamReceiver.m_intEndWeekday - weekday1;
        }
        else
        {
            streamReceiver.m_intEndWeek = streamReceiver.m_intEndWeek + 1;
            weekDayPlus1 = streamReceiver.m_intEndWeekday - weekday1;
        }
        
        NSUInteger endDate = (weekDayPlus1 + 1) + 7 * streamReceiver.m_intEndWeek;
        if (endDate > daysofEndMonth)
        {
            endDate = endDate - 7;
        }
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSDate *EndDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@-%lu-%lu %lu:%lu",currentYear,(unsigned long)endMonth +1,(unsigned long)endDate,(unsigned long)streamReceiver.m_intEndHour,(unsigned long)streamReceiver.m_intEndMin]];
        NSTimeInterval timeInterval2 = [EndDate timeIntervalSince1970];
        NSUInteger endTime = fabs(timeInterval2);
        
        if (currentZoneTime >= startTime && currentZoneTime < endTime)
        {
            streamReceiver.m_dst_time_sec = adjustTime / 1000;
        }
        else
        {
            streamReceiver.m_dst_time_sec = 0;
        }
    }
    else
    {
        streamReceiver.m_dst_time_sec = 0;
        return;
    }
}

#pragma mark - Event Delegate

- (void)playbackEvent:(SearchResultEntry *)_Event
{
    [self openPlaybackBy:_Event.uiStartTime with:_Event.uiChannel];
}

#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 2;
    
    if (searchType == SEARCH_EVENT && section==1)
        rows = 4;

    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    CGFloat height = 0.0;
    
    if (section == tableView.numberOfSections-1)
        height = searchType == SEARCH_TIME ? 60 : 180;
    
    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 30)] autorelease];
    [headerView setBackgroundColor:[UIColor BACOLOR]];
    UILabel *lbTitle = [[[UILabel alloc] initWithFrame:headerView.frame] autorelease];
    [lbTitle setBackgroundColor:[UIColor clearColor]];
    [lbTitle setTextColor:[UIColor whiteColor]];
    [lbTitle setFont:[UIFont boldSystemFontOfSize:18]];
    
    switch (section) {
        case 0:
            [lbTitle setText:NSLocalizedString(@"HDDSTET", nil)];
            break;
        case 1:
            [lbTitle setText:(searchType==SEARCH_TIME ?
                              NSLocalizedString(@"TimePlayback", nil) : NSLocalizedString(@"EventPlayback", nil))];
            break;
    }
    [headerView addSubview:lbTitle];

    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60)] autorelease];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    if (searchType == SEARCH_TIME) {
        UIButton *searchBtn = [[UIButton alloc] initWithFrame:CGRectMake((POPVIEW_FRAME.size.width-50)/2, 5, 50, 50)];
        [searchBtn setImage:[UIImage imageNamed:@"searchEvent.png"] forState:UIControlStateNormal];
        [searchBtn addTarget:self action:@selector(startPlayback) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:searchBtn];  [searchBtn release];
    }else {
        //init event icon
        UIImageView *imgA = [[UIImageView alloc] initWithImage:IMG_ALARM2];
        UIImageView *imgV = [[UIImageView alloc] initWithImage:IMG_VLOSS2];
        UIImageView *imgM = [[UIImageView alloc] initWithImage:IMG_MOTION2];
        [imgA setFrame:CGRectMake(20, 10, 40, 40)];
        [imgV setFrame:CGRectMake(140, 10, 40, 40)];
        [imgM setFrame:CGRectMake(260, 10, 40, 40)];
        [footerView addSubview:imgA];   [imgA release];
        [footerView addSubview:imgV];   [imgV release];
        [footerView addSubview:imgM];   [imgM release];
        
        //init check button
        UIButton *btnA = [[UIButton alloc] initWithFrame:CGRectMake(70, 18, 24, 24)];
        UIButton *btnV = [[UIButton alloc] initWithFrame:CGRectMake(190, 18, 24, 24)];
        UIButton *btnM = [[UIButton alloc] initWithFrame:CGRectMake(310, 18, 24, 24)];
        [btnA setImage:(eventFilter&EVENT_ALARM ? IMG_CHECK : IMG_UNCHECK) forState:UIControlStateNormal];
        [btnV setImage:(eventFilter&EVENT_VLOSS ? IMG_CHECK : IMG_UNCHECK) forState:UIControlStateNormal];
        [btnM setImage:(eventFilter&EVENT_MOTION ? IMG_CHECK : IMG_UNCHECK) forState:UIControlStateNormal];
        [btnA setTag:EVENT_ALARM];
        [btnV setTag:EVENT_VLOSS];
        [btnM setTag:EVENT_MOTION];
        [btnA addTarget:self action:@selector(selectEvent:) forControlEvents:UIControlEventTouchUpInside];
        [btnV addTarget:self action:@selector(selectEvent:) forControlEvents:UIControlEventTouchUpInside];
        [btnM addTarget:self action:@selector(selectEvent:) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:btnA];   [btnA release];
        [footerView addSubview:btnV];   [btnV release];
        [footerView addSubview:btnM];   [btnM release];
        
        //init search button
        UIButton *searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(POPVIEW_FRAME.size.width-50-15, 5, 50, 50)];
        [searchBtn setImage:[UIImage imageNamed:@"searchEvent.png"] forState:UIControlStateNormal];
        [searchBtn addTarget:self action:@selector(searchEvent) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:searchBtn];  [searchBtn release];
    }
    
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.showsReorderControl = YES;
		cell.selectionStyle = UITableViewCellSelectionStyleGray;
		
		// set background
		cell.textLabel.backgroundColor = [UIColor clearColor];
        [cell.textLabel setTextColor:[UIColor EFCOLOR]];
		cell.detailTextLabel.backgroundColor = [UIColor clearColor];
        [cell.detailTextLabel setTextColor:[UIColor lightTextColor]];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
    }
    
    switch (indexPath.section) {
        case 0:
            
            if (indexPath.row == 0) {
                [cell.textLabel setText:NSLocalizedString(@"StartTime", nil)];
                [cell.detailTextLabel setText:strHddST];
            }else {
                [cell.textLabel setText:NSLocalizedString(@"EndTime", nil)];
                [cell.detailTextLabel setText:strHddET];
            }
            break;
        case 1:
            if (searchType == SEARCH_TIME) {
                
                if (indexPath.row == 0) {
                    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                    [cell.textLabel setText:NSLocalizedString(@"SelectDate", nil)];
                    [cell.detailTextLabel setText:[dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:uSelectST]]];
                }else {
                    [dateFormatter setDateFormat:@"HH:mm"];
                    [cell.textLabel setText:NSLocalizedString(@"SelectTime", nil)];
                    [cell.detailTextLabel setText:[dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:uSelectST]]];
                }
            }else {
                
                switch (indexPath.row) {
                    case 0:
                        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                        [cell.textLabel setText:NSLocalizedString(@"StartDate", nil)];
                        [cell.detailTextLabel setText:[dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:uSelectST]]];
                        break;
                    case 1:
                        [dateFormatter setDateFormat:@"HH:mm"];
                        [cell.textLabel setText:NSLocalizedString(@"StartTime", nil)];
                        [cell.detailTextLabel setText:[dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:uSelectST]]];
                        break;
                    case 2:
                        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                        [cell.textLabel setText:NSLocalizedString(@"EndDate", nil)];
                        [cell.detailTextLabel setText:[dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:uSelectET]]];
                        break;
                    case 3:
                        [dateFormatter setDateFormat:@"HH:mm"];
                        [cell.textLabel setText:NSLocalizedString(@"EndTime", nil)];
                        [cell.detailTextLabel setText:[dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:uSelectET]]];
                        break;
                }
            }
            break;
    }
    
    return cell;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
        return;
    
    switch (indexPath.row% 2) {
            
        case INDEX_DATE:
            [datePicker setDatePickerMode:UIDatePickerModeDate];
            break;
        case INDEX_TIME:
            [datePicker setDatePickerMode:UIDatePickerModeTime];
            break;
    }
    pickerIdx = indexPath.row;
    
    [pickerMenu setHidden:NO];
}

#pragma mark - AlertView 

- (void)showAlarm:(NSString *)message
{
	if ( message!=nil ) {
		
		// open an alert with just an OK button, touch "OK" will do nothing
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"BtnOK", nil) otherButtonTitles:nil];
		
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];[alert show];
	}
}

@end
