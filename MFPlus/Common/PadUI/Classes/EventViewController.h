//
//  EventViewController.h
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/30.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreamReceiver.h"
#import "EFViewerAppDelegate.h"

#define EVENT_ALARM  1
#define EVENT_MOTION 4
#define EVENT_VLOSS  8

#define IMG_ALARM       [UIImage imageNamed:@"alarm.png"]
#define IMG_MOTION      [UIImage imageNamed:@"motion.png"]
#define IMG_VLOSS       [UIImage imageNamed:@"videoloss.png"]

#define IMG_ALARM2      [UIImage imageNamed:@"alarm2.png"]
#define IMG_MOTION2     [UIImage imageNamed:@"motion2.png"]
#define IMG_VLOSS2      [UIImage imageNamed:@"videoloss2.png"]

@class EventViewController;

@protocol eventDelegate

@required
- (void)playbackEvent:(SearchResultEntry *)_Event;

@end

@interface EventViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView        *mainTable;
    UIView                      *maskView;
    UIActivityIndicatorView     *loadActivity;
    
    NSMutableArray              *eventList;
    NSDateFormatter             *dateFormatter;
    
    BOOL                        blnSearching;
}

@property(nonatomic,retain) StreamReceiver          *streamReceiver;
@property(nonatomic,assign) id<eventDelegate>       evDelegate;
@property(nonatomic)        NSUInteger              uSearchST;
@property(nonatomic)        NSUInteger              uSearchET;
@property(nonatomic)        NSUInteger              eventFilter;

@end
