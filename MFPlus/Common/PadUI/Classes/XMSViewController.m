//
//  XmsVC.m
//  EFViewerPlus
//
//  Created by Ray Lin on 2015/05/26.
//  Copyright (c) 2015年 EverFocus. All rights reserved.
//

#import "XMSViewController.h"
#import "XMSStreamReceiver.h"
#import "EzAccessoryView.h"

@interface XMSViewController ()

- (void)addStreaming:(NSInteger)chIdx viewIdx:(NSInteger)vIdx;
- (void)deleteStreaming:(NSInteger)chIdx viewIdx:(NSInteger)vIdx;

@end

@implementation XMSViewController
{
    BOOL blnVehSection;     //20151126 add by Ray Lin, 用來判斷是否為Vehicle Section
    BOOL blnSupportTreeView;
}

@synthesize channelTreeItems;

#pragma mark - View LifeCycle

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    MMLog(@"[IV] Init %@",self);
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navView = [appDelegate.splitViewController.viewControllers objectAtIndex:1];
    detailView = [navView.viewControllers  objectAtIndex:0];
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicator setHidesWhenStopped:YES];
    [self.view addSubview:activityIndicator];
}

- (void)viewWillAppear:(BOOL)animated
{
    detailView.delegate = self;
    
    if (blnOpenPtz) {
        blnOpenPtz = NO;
        return;
    }
    
    detailView.device = device;
    [detailView changeControlMode:ecm_live with:0];
    blnIsSoundOn = NO;
    blnOpenPtz = NO;
    blnGetInfo = NO;
    blnFirstConnect = YES;
    statusTimer = nil;
    
    // Init cameraList
    //playingIndexList = [[NSMutableArray alloc] init];
    ipcamArray = [[NSMutableArray alloc] init];
    dvrArray = [[NSMutableArray alloc] init];
    rtspArray = [[NSMutableArray alloc] init];
    vehicleArray = [[NSMutableArray alloc] init];  //20151120 added by Ray Lin, for xms support vehicle tree view test
    self.channelTreeItems = [NSMutableArray array];
    tmp2RemoveAry = [[NSMutableArray alloc] init];
    tmpSelectDVR = [[NSMutableArray alloc] init];
    
    //Initial UI
    [searchBtn setEnabled:YES];
    
    [mainTable reloadData];
    
    // Get Device Info
    if (device != nil && streamReceiver == nil) {
        streamReceiver = [[XMSStreamReceiver alloc] initWithDevice:device];
        DBGLog(@"[XMS] Initiate XMS streaming receiver %p",streamReceiver);
        [streamReceiver setDelegate:self];
        
        if (self.blnEventMode) {
            
            if (blnOpenSearch && self.blnPlaybackMode) {
                streamReceiver.m_blnPlaybackMode = YES;
                if (!self.blnEventMode_pb) {
                    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
                    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
                    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                    NSString *dateString = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.event.event_time - 5]];   //從event發生的前5秒開始回放
                    NSDate *currentDate = [dateFormatter dateFromString:dateString];
                    streamReceiver.m_intPlaybackStartTime = [currentDate timeIntervalSince1970];
                    //DBGLog(@"[XMS] Event Playback Time : %lu\n[XMS] Event Playback Date : %@\n[XMS] Event Playback String : %@",(unsigned long)streamReceiver.m_intPlaybackStartTime,currentDate,dateString);
                }
            }
        }
        
        [NSThread detachNewThreadSelector:@selector(getDeviceInfomation)
                                 toTarget:self
                               withObject:nil];
    }
    
    currentPlayingMask = 0;
    
    CGRect frame = self.view.frame;
    [activityIndicator setFrame:CGRectMake((frame.size.width-INDICATOR_HEIGHT)/2,(frame.size.height-INDICATOR_HEIGHT)/2, INDICATOR_HEIGHT, INDICATOR_HEIGHT)];
    
    //get popalarm
    blnPopEnable = NO;
    BOOL bShowPop = [EZAccessoryView getDetailSettingBy:KEY_ALARMPOP];
    [alarmBtn setHidden:!bShowPop];
    
    if (bShowPop)
        [self switchAlarmPop];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (blnOpenPtz)
        return;
    
    [detailView disconnectAll];
    [streamReceiver stopStreaming];
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (blnOpenPtz)
        return;
    
    while (blnStreamThread) {
        [NSThread sleepForTimeInterval:0.01f];
    }
    
    SAVE_FREE(streamReceiver);
    
    //release cameraList
    [ipcamArray removeAllObjects];
    SAVE_FREE(ipcamArray);
    [dvrArray removeAllObjects];
    SAVE_FREE(dvrArray);
    [rtspArray removeAllObjects];
    SAVE_FREE(rtspArray);
    [vehicleArray removeAllObjects];//20151120 added by Ray Lin, for xms support vehicle tree view test
    SAVE_FREE(vehicleArray);
    [self.channelTreeItems removeAllObjects];
    [tmp2RemoveAry removeAllObjects];
    SAVE_FREE(tmp2RemoveAry);
    [tmpSelectDVR removeAllObjects];
    SAVE_FREE(tmpSelectDVR);
    [self clearCredentialsCache];
}

- (void)dealloc
{
    MMLog(@"[XMS] dealloc %@",self);
    
    SAVE_FREE(mainTable);
    SAVE_FREE(streamReceiver);
    SAVE_FREE(device);
    SAVE_FREE(activityIndicator);
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    DBGLog(@"[XMS] Receive Memory Warning");
}

#pragma mark - View Control

- (IBAction)changePlaybackMode:(NSInteger)btnId
{
    NSInteger mode;
    
    if (btnId == PLAYBACK_EXIT) {
        
        DBGLog(@"[XMS] Playback -> Live");
        //disconnect all streaming
        if (blnXmsIPcam) {
            [detailView disconnectAll];
        }
        else
            [streamReceiver stopStreaming];
        
        //connect live streaming
        [detailView changeControlMode:ecm_live with:0];
        streamReceiver.m_blnPlaybackMode = NO;
        blnXms1stConnect = blnFirstConnect = YES;
        [playingIndexList removeAllObjects];
//        VideoDisplayView *tmpView = [streamReceiver.videoControl.aryDisplayViews objectAtIndex:detailView.selectWindow];
//        if (streamReceiver.currentCHList.count) {
//            [playingIndexList addObject:[streamReceiver.currentCHList objectAtIndex:tmpView.playingChannel]];
//            [self addStreaming:tmpView.playingChannel viewIdx:detailView.selectWindow];
//        }
//        else {
//            NSIndexPath *tmpPath = [NSIndexPath indexPathForRow:tmpView.playingChannel inSection:intSelectSection];
//            [[mainTable delegate] tableView:mainTable didSelectRowAtIndexPath:tmpPath];
//        }
        NSIndexPath *targetPath = [NSIndexPath indexPathForRow:intSelectRow inSection:intSelectSection];
        [[mainTable delegate] tableView:mainTable didSelectRowAtIndexPath:targetPath];
        return;
    }
    
    switch (btnId) {
            
        case PLAYBACK_PAUSE:
            if (blnPlaybackPause) {
                mode = PLAYBACK_FORWARD;
                [streamReceiver.videoControl resume];
                pbSpeed = 1;
            }else {
                mode = PLAYBACK_PAUSE;
                [streamReceiver.videoControl pause];
                pbSpeed = 0;
            }
            blnPlaybackPause = !blnPlaybackPause;
            break;
            
        case PLAYBACK_FAST_FORWARD:
        case PLAYBACK_FAST_BACKWARD:
            if (blnPlaybackPause) {
                [streamReceiver.videoControl resume];
                blnPlaybackPause = NO;
            }
            
            if (btnId == PLAYBACK_FAST_FORWARD) {
                pbSpeed = (pbSpeed > 0 ? pbSpeed*2 : 1);
                if (pbSpeed > 32)   pbSpeed = 32;
            }else {
                pbSpeed = (pbSpeed < 0 ? pbSpeed*2 : -1);
                if (pbSpeed < -32)  pbSpeed = -32;
            }
            mode = btnId;
            break;
    }
    
    [streamReceiver changePlaybackMode:mode withValue:abs(pbSpeed)];
    [detailView changeControlMode:mode with:pbSpeed];
}

#pragma mark - Streaming Control

- (void)getDeviceInfomation
{
    DBGLog(@"[XMS] GetDeviceInfo Start");
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [activityIndicator performSelectorOnMainThread:@selector(startAnimating) withObject:nil waitUntilDone:NO];
    CameraInfo *currentEntry = [[CameraInfo alloc] init];
    blnInfoThread = YES;
    
    if (!blnGetInfo) {
        
        // Get Device Information
        if (![streamReceiver getDeviceInfo])
        {
            if (!streamReceiver.errorDesc) {
                streamReceiver.errorDesc = NSLocalizedString(@"MsgInfoErr", nil);
            }
            blnInfoThread = NO;
            [self showAlarmAndBack:streamReceiver.errorDesc];
            [pool release];
            return;
        }
        
        blnGetInfo = YES;
        
        if (streamReceiver.m_DiskGMT) {
            device.devTimeZone = [streamReceiver.m_DiskGMT copy];
            DBGLog(@"[XMS] Device TIMEZONE : %@",device.devTimeZone);
        }
        
        for (int i=0; i<streamReceiver.cameraList.count; i++) {
            CameraInfo *tmpEntry = [streamReceiver.cameraList objectAtIndex:i];
            
            //DBGLog(@"[XMS] Current Dev : %@, %ld",currentEntry.title, (long)currentEntry.deviceType);
            
            if (tmpEntry.deviceType == XMS_IPCAM) {
                [ipcamArray addObject:tmpEntry];
            }
            else if (tmpEntry.deviceType == XMS_DVR || tmpEntry.deviceType == XMS_NVR) {
                [dvrArray addObject:tmpEntry];
            }
            else if (tmpEntry.deviceType == XMS_VEHICLE) {    //20151120 added by Ray Lin, for xms support vehicle tree view test
                [vehicleArray addObject:tmpEntry];
            }
            else
                [rtspArray addObject:tmpEntry];
        }
    }
    intSelectSection = 0;
    
    if (!self.blnEventMode) {
        //update channel parameter
        NSMutableArray *tmpArray = [NSMutableArray new];
        
        if (ipcamArray.count > 0) {
            currentEntry = [ipcamArray firstObject];
            tmpArray = [ipcamArray copy];
            NSArray *_objary = [[NSArray alloc] initWithObjects:tmpArray, currentEntry, nil];
            [self performSelectorOnMainThread:@selector(autoFillMask:) withObject:_objary waitUntilDone:NO];
        }
        else if (dvrArray.count > 0) {
            NSIndexPath *targetPath = [NSIndexPath indexPathForRow:0 inSection:XMS_SECTION_DVR];
            [[mainTable delegate] tableView:mainTable didSelectRowAtIndexPath:targetPath];
        }
        else if (rtspArray.count > 0) {
            currentEntry = [rtspArray firstObject];
            tmpArray = [rtspArray copy];
            NSArray *_objary = [[NSArray alloc] initWithObjects:tmpArray, currentEntry, nil];
            [self performSelectorOnMainThread:@selector(autoFillMask:) withObject:_objary waitUntilDone:NO];
        }
        else {  //vehicle array
            NSIndexPath *targetPath = [NSIndexPath indexPathForRow:0 inSection:XMS_SECTION_VEHICLE];
            [[mainTable delegate] tableView:mainTable didSelectRowAtIndexPath:targetPath];
        }
    }
    else {
        if (self.blnOpenSearch) {
            //connect playback streaming
            blnPlaybackPause = NO;
            pbSpeed = 1;
            [detailView changeControlMode:ecm_playback with:0];
        }
        
        blnFirstConnect = YES;
        blnXms1stConnect = YES;
        
        switch (self.event.dev_type) {
                
            case DEV_IPCAM:
                intSelectSection = XMS_SECTION_IPCAM;
                for (int idx=0; idx<ipcamArray.count; idx++) {
                    CameraInfo *tmpDev = [ipcamArray objectAtIndex:idx];
                    if ([tmpDev.title isEqualToString:self.event.dev_name]) {
                        intSelectRow = idx;
                        break;
                    }
                }
                break;
                
            case DEV_DVR:
                intSelectSection = XMS_SECTION_DVR;
                for (int idx=0; idx<dvrArray.count; idx++) {
                    CameraInfo *tmpDev = [dvrArray objectAtIndex:idx];
                    if ([tmpDev.title isEqualToString:self.event.dev_name]) {
                        intSelectRow = idx;
                        break;
                    }
                }
                break;
                
            default:
                break;
        }
        
        NSIndexPath *targetPath = [NSIndexPath indexPathForRow:intSelectRow inSection:intSelectSection];
        [[mainTable delegate] tableView:mainTable didSelectRowAtIndexPath:targetPath];
    }
    
    [mainTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    [activityIndicator performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:NO];
    
    //20160314 added by Ray Lin, for DVR/NVR record btn control in xms
    detailView.streamReceiver = streamReceiver;
    
    blnInfoThread = NO;
    [currentEntry release];
    [pool release];
    DBGLog(@"[XMS] GetDeviceInfo Stop");
}

- (NSMutableArray *)getChannelArray:(NSArray *)_array fromObject:(CameraInfo *)curInfo
{
    NSMutableArray *mutableAry = [NSMutableArray new];
    for (CameraInfo *info in _array) {
        if (curInfo.deviceType == XMS_DVR || curInfo.deviceType == XMS_NVR) {
            if ([curInfo.relations containsObject:info.strDeviceID]) {
                [mutableAry addObject:info];
            }
        }
        else {  //get vehicle channels
            
        }
    }
    return mutableAry;
}

- (void)startStreaming
{ // thread
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    blnStreamThread = YES;
    DBGLog(@"[XMS] Streaming thread start");
    
    if (streamReceiver.m_blnPlaybackMode) {
        
        if ( ![streamReceiver startPlaybackStreaming:currentPlayingMask] ) {
            [self showAlarm:[streamReceiver errorDesc]];
            streamReceiver.errorDesc = nil;
        }
    }else {
        
        if ( ![streamReceiver startLiveStreaming:currentPlayingMask] ) {
            [self showAlarm:[streamReceiver errorDesc]];
            streamReceiver.errorDesc = nil;
        }
    }
    
    DBGLog(@"[XMS] Streaming thread stop");
    blnStreamThread = NO;
    [pool release];
}

- (void)addStreaming:(NSInteger)chIdx viewIdx:(NSInteger)vIdx
{
    [streamReceiver.outputList replaceObjectAtIndex:chIdx withObject:[NSString stringWithFormat:@"%ld",(long)vIdx]];
    streamReceiver.currentPlayCH = 0x1<<chIdx;
    DBGLog(@"[XMS] Add CH:%ld View:%ld",(long)chIdx,(long)vIdx);
    
    if (streamReceiver.blnDvrChannel) {
        if (blnXms1stConnect) {
            
            currentPlayingMask = 0;
            currentPlayingMask |= 0x1<<chIdx;
            //currentPlayingMask = chIdx;
            if (detailView.videoControl != nil ) {
                
                streamReceiver.videoControl = detailView.videoControl;
                [streamReceiver.videoControl startByCH:vIdx];
                [streamReceiver.videoControl setBlnPSLimit:NO];
                
                streamReceiver.blnEventMode = self.blnEventMode;
                [streamReceiver.event release];
                streamReceiver.event = [self.event retain];
            }else {
                
                DBGLog(@"[XMS] VideoControl loss");
                return;
            }
            
            blnXms1stConnect = blnFirstConnect = NO;
            [NSThread detachNewThreadSelector:@selector(startStreaming)
                                     toTarget:self
                                   withObject:nil];
        } else {
            
            while (!streamReceiver.blnReceivingStream) {
                if (streamReceiver.blnConnectionClear) {
                    return;
                }
                [NSThread sleepForTimeInterval:0.01f];
            }
            //[NSThread sleepForTimeInterval:0.3f];
            currentPlayingMask |= 0x1<<chIdx;
            [streamReceiver.videoControl startByCH:vIdx];
            [streamReceiver changeVideoMask:currentPlayingMask];
        }
    }
    else {
        currentPlayingMask = chIdx;
        blnXms1stConnect = blnFirstConnect = NO;
        
        if (detailView.videoControl != nil ) {
            
            streamReceiver.videoControl = detailView.videoControl;
            [streamReceiver.videoControl startByCH:vIdx];
            [streamReceiver.videoControl setBlnPSLimit:NO];
            
            streamReceiver.blnEventMode = self.blnEventMode;
            [streamReceiver.event release];
            streamReceiver.event = [self.event retain];
        }
        else {
            
            DBGLog(@"[XMS] VideoControl loss");
            return;
        }
        
        [NSThread detachNewThreadSelector:@selector(startStreaming)
                                 toTarget:self
                               withObject:nil];
    }
    
    [streamReceiver.videoControl setPlayingChannelByCH:vIdx Playing:chIdx];
}

- (void)deleteStreaming:(NSInteger)chIdx viewIdx:(NSInteger)vIdx
{
    DBGLog(@"[XMS] deleteStreaming CH:%ld View:%ld",(long)chIdx,(long)vIdx);
    
    CameraInfo *tmpEntry = [playingIndexList lastObject];
    if (tmpEntry.deviceType  != XMS_CHANNEL) {
        [streamReceiver stopStreamingByView:vIdx];
        [streamReceiver.videoControl stopByCH:vIdx];
    }
    else
    {
        if (blnXms1stConnect) {
            [streamReceiver stopStreaming];
        }
        else {
            currentPlayingMask ^= 0x1<<chIdx;
            
            if (streamReceiver.blnReceivingStream) {
                [streamReceiver changeVideoMask:currentPlayingMask];
            }
            
            [streamReceiver.videoControl stopByCH:vIdx];
        }
    }
    
    [streamReceiver.outputList replaceObjectAtIndex:chIdx withObject:[NSString stringWithFormat:@"99"]];
    [mainTable reloadData];
}

- (void)closeView:(NSInteger)chIdx with:(NSInteger)vIdx
{
    DBGLog(@"[XMS] Disconnect CH:%ld View:%ld",(long)chIdx,(long)vIdx);
    for (CameraInfo *tmpEntry in playingIndexList)
    {
        //DBGLog(@"[XMS] PlayingIndex[%d] CH:%d",i,currentEntry.index-1);
        if (tmpEntry.index-1 == chIdx) {
            
            //DBGLog(@"[XMS] RemoveFromPlayList");
            tmpEntry.status = 0;    //reset status when delete streaming
            [self deleteStreaming:chIdx viewIdx:vIdx];
            [playingIndexList removeObject:tmpEntry];
            break;
        }
    }
    
    [mainTable reloadData];
}

- (void)outputListReset
{
    for (NSInteger i=0; i<totalChannel; i++) {
        [streamReceiver.outputList replaceObjectAtIndex:i withObject:[NSString stringWithFormat:@"99"]];
    }
}

- (void)autoFillMask
{
    if (self.blnEventMode && !blnFirstConnect)
        return;
    
    CameraInfo *currentInfo = [playingIndexList lastObject];
    NSMutableArray *currArray = [NSMutableArray new];
    
    switch (currentInfo.deviceType) {
        case XMS_IPCAM:
            currArray = ipcamArray;
            break;
            
        case XMS_RTSP:
            currArray = rtspArray;
            break;
            
        case XMS_CHANNEL:
        case XMS_VEHICLE_CH:
            currArray = streamReceiver.currentCHList;
            break;
            
        default:
            currArray = streamReceiver.cameraList;
            break;
    }
    
    NSArray *_ary = [NSArray arrayWithObjects:currArray, currentInfo, nil];
    [self autoFillMask:_ary];
    [mainTable reloadData];
}

- (void)autoFillMask:(NSArray *)tmpArray
{
    NSMutableArray *chArray = [tmpArray objectAtIndex:0];
    CameraInfo *camObj = [tmpArray objectAtIndex:1];
    
    for (CameraInfo *tmpInfo in chArray)
    {
        if ([playingIndexList indexOfObject:tmpInfo] == NSNotFound) {  //沒有在播放的camera
            
            if (playingIndexList.count+1 > pow([detailView getViewMode],2)) //現有view已點滿了
                return;
            
            //找個空的view來add
            for (VideoDisplayView *tmpView in detailView.videoDispViews)
            {
                if (!tmpView.blnIsPlay) {
                    [detailView changeSelectWindowTo:[detailView.videoDispViews indexOfObject:tmpView]];
                    
                    if (tmpInfo.deviceType == XMS_CHANNEL) {
                        
                        streamReceiver.blnReceivingStream = NO;
                        //檢查要從哪個channel開始播
                        if (tmpInfo.index < camObj.index) {
                            break;
                        }
                    }
                    [playingIndexList addObject:tmpInfo];
                    [NSThread sleepForTimeInterval:0.2f];
                    [self addStreaming:tmpInfo.index-1 viewIdx:detailView.selectWindow];
                    break;
                }
            }
        }
    }
}

/* 20151120 added by Ray Lin, for xms support vehicle tree view test */
#pragma mark - Actions

- (NSMutableArray *)listItemsWithCamera:(CameraInfo *)camInfo
{
    NSMutableArray *tmpVehicleArry = [[[NSMutableArray alloc] init] autorelease];
    
    if (camInfo.deviceType == XMS_VEHICLE) {
        
        NSMutableArray *tmpchannelArray = [[[NSMutableArray alloc] init] autorelease];
        for (CameraInfo *channelInfo in streamReceiver.v_channelList) {
            if ([channelInfo.parent isEqualToString:camInfo.child]) {
                [tmpchannelArray addObject:channelInfo];
            }
        }
        for (CameraInfo *tmpInfo in streamReceiver.vehicleList) {
            if ([tmpInfo.path isEqualToString:camInfo.child]) {
                [tmpInfo setSubmersionLevel:1];
                [tmpInfo setParentSelectingItem:camInfo];
                [tmpInfo setAncestorSelectingItems:tmpchannelArray];
                [tmpInfo setNumberOfSubitems:tmpchannelArray.count];
                [tmpInfo setSelected:NO];
                [tmpVehicleArry addObject:tmpInfo];
            }
        }
    }
    else if (camInfo.deviceType == XMS_DVR || camInfo.deviceType == XMS_NVR || camInfo.deviceType == XMS_VEHICLE_DVR) {
        if (blnVehSection) {
            for (CameraInfo *tmpInfo in streamReceiver.v_channelList) {
                if ([tmpInfo.parent isEqualToString:camInfo.path]) {
                    [tmpInfo setSubmersionLevel:2];
                    [tmpInfo setParentSelectingItem:camInfo];
                    [tmpInfo setAncestorSelectingItems:[NSMutableArray array]];
                    [tmpInfo setNumberOfSubitems:0];
                    [tmpVehicleArry addObject:tmpInfo];
                }
            }
        }
        else {
            for (CameraInfo *tmpInfo in streamReceiver.channelList) {
                if ([camInfo.relations containsObject:tmpInfo.path]) {
                    [tmpInfo setSubmersionLevel:2];
                    [tmpInfo setParentSelectingItem:camInfo];
                    [tmpInfo setAncestorSelectingItems:[NSMutableArray array]];
                    [tmpInfo setNumberOfSubitems:0];
                    [tmpVehicleArry addObject:tmpInfo];
                }
            }
        }
    }
    else {
        
    }
    
    return tmpVehicleArry;
}

- (void)iconButtonAction:(ExTableViewCell *)cell
{
    blnFirstConnect = NO;
    [cell.iconButton setSelected:!cell.iconButton.selected];
    cell.cameraItem.selected = cell.iconButton.selected;
}

- (void)selectingItemsToDelete:(CameraInfo *)selItems saveToArray:(NSMutableArray *)deleteSelectingItems
{
    for (CameraInfo *obj in selItems.ancestorSelectingItems)
    {
        [self selectingItemsToDelete:obj saveToArray:deleteSelectingItems];
    }
    
    [deleteSelectingItems addObject:selItems];
}

- (NSMutableArray *)removeIndexPathForTreeItems:(NSMutableArray *)treeItemsToRemove inSection:(NSInteger)section
{
    NSMutableArray *result = [NSMutableArray array];
    
    for (NSInteger i = 0; i < [mainTable numberOfRowsInSection:section]; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        CameraInfo *info = [tmpTreeArray_copy objectAtIndex:indexPath.row];
        
        for (CameraInfo *tmpCamItem in treeItemsToRemove) {
            if ([info isEqualToSelectingItem:tmpCamItem]) {
                [result addObject:indexPath];
            }
        }
    }
    
    return result;
}

#pragma mark - Search Delegate

- (void)playbackByMask:(NSUInteger)channel
{
    NSIndexPath *tmpPath;
    
    NSLog(@"[XMS] Live -> Playback");
    if (0x1<<channel & streamReceiver.validChannel) {
        
        //disconnect all streaming
        [detailView disconnectAll];
        [streamReceiver stopStreaming];
        
        //connect playback streaming
        blnXms1stConnect = blnFirstConnect = YES;
        blnPlaybackPause = NO;
        streamReceiver.m_blnPlaybackMode = YES;
        pbSpeed = 1;
        [detailView changeControlMode:ecm_playback with:0];
        tmpPath = [NSIndexPath indexPathForRow:intSelectRow inSection:intSelectSection];
        [[mainTable delegate] tableView:mainTable didSelectRowAtIndexPath:tmpPath];
        
//        if (streamReceiver.currentCHList.count) {
//            [playingIndexList addObject:[streamReceiver.currentCHList objectAtIndex:channel]];
//            [self addStreaming:channel viewIdx:detailView.selectWindow];
//        }
//        else {
//            tmpPath = [NSIndexPath indexPathForRow:channel inSection:intSelectSection];
//            [[mainTable delegate] tableView:mainTable didSelectRowAtIndexPath:tmpPath];
//        }
    }
}

/* 20151120 added by Ray Lin, for xms support vehicle tree view test */

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 20;
    switch (section) {
        case XMS_SECTION_IPCAM:
            if ([ipcamArray count] == 0) {
                height = 0;
            }
            break;
        case XMS_SECTION_DVR:
            if ([dvrArray count] == 0) {
                height = 0;
            }
            break;
        case XMS_SECTION_RTSP:
            if ([rtspArray count] == 0) {
                height = 0;
            }
            break;
        case XMS_SECTION_VEHICLE:
            if ([vehicleArray count] == 0) {
                height = 0;
            }
            break;
            
        default:
            break;
    }
    
    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 20)] autorelease];
    [headerView setBackgroundColor:[UIColor BACOLOR]];
    UILabel *lbTitle = [[[UILabel alloc] initWithFrame:headerView.frame] autorelease];
    [lbTitle setBackgroundColor:[UIColor clearColor]];
    [lbTitle setTextColor:[UIColor whiteColor]];
    [lbTitle setFont:[UIFont systemFontOfSize:15]];
    switch (section) {
        case XMS_SECTION_IPCAM:
            [lbTitle setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"IPCAM", nil)]];
            break;
        case XMS_SECTION_DVR:
            [lbTitle setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"DVR/NVR", nil)]];
            break;
        case XMS_SECTION_RTSP:
            [lbTitle setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"RTSP", nil)]];
            break;
        case XMS_SECTION_VEHICLE:
            [lbTitle setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"VEHICLE", nil)]];
            break;
            
        default:
            break;
    }
    //[lbTitle setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"CameraList", nil)]];
    [headerView addSubview:lbTitle];
    
    return headerView;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    switch (section) {
        case XMS_SECTION_IPCAM:
            rows = [ipcamArray count];
            break;
        case XMS_SECTION_DVR:
            rows = [dvrArray count];
            break;
        case XMS_SECTION_RTSP:
            rows = [rtspArray count];
            break;
        case XMS_SECTION_VEHICLE:
            rows = [vehicleArray count];
            break;
            
        default:
            break;
    }
    return rows;
    //return streamReceiver.cameraList.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell...
    NSMutableArray *subarry = [[NSMutableArray alloc] init];
    switch (indexPath.section) {
        case XMS_SECTION_IPCAM:
            for (int i=0; i<ipcamArray.count; i++) {
                CameraInfo *currentEntry = [ipcamArray objectAtIndex:i];
                [subarry addObject:currentEntry];
                blnSupportTreeView = NO;
            }
            break;
        case XMS_SECTION_DVR:
            for (int i=0; i<dvrArray.count; i++) {
                CameraInfo *currentEntry = [dvrArray objectAtIndex:i];
                [subarry addObject:currentEntry];
                blnSupportTreeView = YES;
            }
            break;
        case XMS_SECTION_RTSP:
            for (int i=0; i<rtspArray.count; i++) {
                CameraInfo *currentEntry = [rtspArray objectAtIndex:i];
                [subarry addObject:currentEntry];
                blnSupportTreeView = NO;
            }
            break;
        case XMS_SECTION_VEHICLE:
            for (int i=0; i<vehicleArray.count; i++) {
                CameraInfo *currentEntry = [vehicleArray objectAtIndex:i];
                [subarry addObject:currentEntry];
                blnSupportTreeView = YES;
            }
            break;
            
        default:
            break;
    }
    
    CameraInfo *subEntry = [subarry objectAtIndex:indexPath.row];
    
    
    //------------------------------------ 20151120 added by Ray Lin, for xms support vehicle tree view test ------------------------------------//
    //if (!blnVehSection) {
    if (!blnSupportTreeView) {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell setBackgroundColor:[UIColor clearColor]];
            [cell.textLabel setBackgroundColor:[UIColor clearColor]];
            [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
            [cell.textLabel setNumberOfLines:0];
            [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
            [cell.textLabel setTextColor:[UIColor lightTextColor]];
            cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
            
            UIImageView *checkBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addDev1.png"]];
            [checkBtn setFrame:CGRectMake(0, 0, 25, 25)];
            [cell setAccessoryView:checkBtn];
            [checkBtn release];
            [cell.accessoryView setHidden:YES];
        }
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[subEntry.title stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        BOOL bVisible = ([playingIndexList indexOfObject:subEntry] != NSNotFound);
        [cell.accessoryView setHidden:!bVisible];
        
        return  cell;
    }
    else {
        static NSString *CellIdentifier = @"ExTableCell";
        ExTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[[ExTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell setBackgroundColor:[UIColor clearColor]];
            cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
        }
        
        cell.cameraItem = [subEntry retain];
        
        [cell setLevel:[subEntry submersionLevel]];
        [cell setTitle:[NSString stringWithFormat:@"%@",[subEntry.title stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        
        if (cell.cameraItem.submersionLevel != 2) {
            
            if (blnFirstConnect) {
                cell.iconButton.selected = NO;
            }
            else {
                if (cell.cameraItem.selected) {
                    cell.iconButton.selected = YES;
                }
                else
                    cell.iconButton.selected = NO;
            }
            
            [cell setAccessoryView:cell.iconButton];
            [cell.accessoryView setHidden:NO];
        }
        else {
            
            UIImageView *checkBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addDev1.png"]];
            [checkBtn setFrame:CGRectMake(0, 0, 23, 23)];
            [cell setAccessoryView:checkBtn];
            [checkBtn release];
            
            BOOL bVisible = ([playingIndexList indexOfObject:subEntry] != NSNotFound);
            [cell.accessoryView setHidden:!bVisible];
        }
        
        [subEntry release];
        
        return cell;
    }
    //------------------------------------ 20151120 added by Ray Lin, for xms support vehicle tree view test ------------------------------------//
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (streamReceiver.cameraList.count == 0) {
        [self showAlarmAndBack:NSLocalizedString(@"MsgNoDevice", nil)];
        return;
    }
    
    ExTableViewCell *cell = nil;
    cell = (ExTableViewCell *)[mainTable cellForRowAtIndexPath:indexPath];
    
    intSelectSection = indexPath.section;
    intSelectRow = indexPath.row;
    
    CameraInfo *currentEntry = [[CameraInfo alloc] init];
    NSMutableArray *tmpTreeArray = [[NSMutableArray alloc] init];
    //CameraInfo *currentEntry = [streamReceiver.cameraList objectAtIndex:indexPath.row];
    
    switch (intSelectSection) {
        case XMS_SECTION_IPCAM:
            if (ipcamArray.count > 0) {
                
                blnXmsIPcam = YES;
                blnVehSection = NO;
                blnSupportTreeView = NO;
                
                if ((self.blnEventMode || self.blnEventMode_pb) && blnFirstConnect) {
                    BOOL ret = NO;
                    for (CameraInfo *tmpCam in streamReceiver.cameraList) {
                        if ([tmpCam.title isEqualToString:self.device.name]) {
                            currentEntry = tmpCam;
                            ret = YES;
                        }
                    }
                    
                    if (!ret) {
                        [self showAlarmAndBack:NSLocalizedString(@"MsgDeviceErr", nil)];
                        DBGLog(@"[XMS] Device not found in XMS camera list");
                        return;
                    }
                }
                else
                    currentEntry = [ipcamArray objectAtIndex:intSelectRow];
            }
            else
            {
                [self showAlarm:@"No IP camera in camera list"];
                [mainTable reloadData];
                return;
            }
            
            break;
        case XMS_SECTION_DVR:
            tmpTreeArray = dvrArray;
            
            blnXmsIPcam = NO;
            blnVehSection = NO;
            blnSupportTreeView = YES;
            
            if ((self.blnEventMode || self.blnEventMode_pb) && blnFirstConnect) {
                BOOL ret = NO;
                for (CameraInfo *tmpCam in streamReceiver.channelList) {
                    if (tmpCam.ptzID == self.event.dev_eid) {
                        currentEntry = tmpCam;
                        ret = YES;
                    }
                }
                
                if (!ret) {
                    [self showAlarmAndBack:NSLocalizedString(@"MsgDeviceErr", nil)];
                    DBGLog(@"[XMS] Device not found in XMS camera list");
                    return;
                }
            }
            else if (cell != nil && cell.cameraItem.submersionLevel == 0) {
                currentEntry = [dvrArray objectAtIndex:intSelectRow];
            }
            else if (channelTreeItems.count>0 && cell != nil) {
                currentEntry = [channelTreeItems objectAtIndex:[channelTreeItems indexOfObject:cell.cameraItem]];
            }
            else
                currentEntry = [streamReceiver.channelList objectAtIndex:intSelectRow];
            
            break;
        case XMS_SECTION_RTSP:
            
            blnXmsIPcam = YES;
            blnVehSection = NO;
            blnSupportTreeView = NO;
            if ((self.blnEventMode || self.blnEventMode_pb) && blnFirstConnect) {
                BOOL ret = NO;
                for (CameraInfo *tmpCam in streamReceiver.cameraList) {
                    if ([tmpCam.title isEqualToString:self.device.name]) {
                        currentEntry = tmpCam;
                        ret = YES;
                    }
                }
                
                if (!ret) {
                    [self showAlarmAndBack:NSLocalizedString(@"MsgDeviceErr", nil)];
                    DBGLog(@"[XMS] Device not found in XMS camera list");
                    return;
                }
            }
            else
                currentEntry = [rtspArray objectAtIndex:intSelectRow];
            
            break;
        case XMS_SECTION_VEHICLE:
            tmpTreeArray = vehicleArray;
            
            blnXmsIPcam = YES;
            blnVehSection = YES;
            blnSupportTreeView = YES;
            
            if ((self.blnEventMode || self.blnEventMode_pb) && blnFirstConnect) {
                BOOL ret = NO;
                for (CameraInfo *tmpCam in streamReceiver.cameraList) {
                    if ([tmpCam.title isEqualToString:self.device.name]) {
                        currentEntry = tmpCam;
                        ret = YES;
                    }
                }
                
                if (!ret) {
                    [self showAlarmAndBack:NSLocalizedString(@"MsgDeviceErr", nil)];
                    DBGLog(@"[XMS] Device not found in XMS camera list");
                    return;
                }
            }
            else
                currentEntry = [vehicleArray objectAtIndex:intSelectRow];
            
            break;
            
        default:
            break;
    }
    
    //設定TreeView的展開或是收縮
    if (blnSupportTreeView && !blnInfoThread && (cell != nil)) {
        
        cell.cameraItem = currentEntry;
        
        NSMutableArray *insertIndexPaths = [NSMutableArray array];
        NSMutableArray *insertselectingItems = [self listItemsWithCamera:cell.cameraItem];
        tmpTreeArray_copy = [[[NSMutableArray alloc] initWithArray:tmpTreeArray] autorelease];
        NSMutableArray *removeIndexPaths = [NSMutableArray array];
        NSMutableArray *treeItemsToRemove = [NSMutableArray array];
        
        //20160605 added by Ray Lin, close the channels, avoid expanding two DVR/Vehicle's channels at the same time.
        for (CameraInfo *tmpDVR in tmpSelectDVR) {
            
            if (intSelectSection != lastSelectSection) {
                [self showAlarm:@"Please collapse the current selected DVR/Vehicle before expanding another one."];
                return;
            }
            
            if (![tmpDVR isEqualToSelectingItem:currentEntry] && currentEntry.submersionLevel == 0) {
                
                removeIndexPaths = [self removeIndexPathForTreeItems:tmp2RemoveAry inSection:lastSelectSection];
                
                for (CameraInfo *rmTreeItem in tmp2RemoveAry) {
                    [tmpTreeArray removeObject:rmTreeItem];
                    
                    for (CameraInfo *tmp3TreeItem in channelTreeItems) {
                        if ([tmp3TreeItem isEqualToSelectingItem:rmTreeItem]) {
                            //DBGLog(@"[XMS] Remove Cell:%@", tmp3TreeItem.title);
                            [channelTreeItems removeObject:rmTreeItem];
                            break;
                        }
                    }
                }
                
                NSIndexPath *indexPth = [NSIndexPath indexPathForRow:lastSelectRow inSection:lastSelectSection];
                ExTableViewCell *lastSelectCell = (ExTableViewCell *)[mainTable cellForRowAtIndexPath:indexPth];
                tmpDVR.selected = NO;
                lastSelectCell.iconButton.selected = tmpDVR.selected;
                [mainTable deleteRowsAtIndexPaths:removeIndexPaths withRowAnimation:UITableViewRowAnimationFade];
                [tmpSelectDVR removeAllObjects];
                [tmp2RemoveAry removeAllObjects];
                [treeItemsToRemove removeAllObjects];
                [removeIndexPaths removeAllObjects];
            }
        }

        NSInteger insertTreeItemIndex = [tmpTreeArray indexOfObject:cell.cameraItem];
        
        if (insertTreeItemIndex != NSNotFound) {
            
            for (CameraInfo *tmpTreeItem in insertselectingItems) {
                
                [tmpTreeItem setParentSelectingItem:cell.cameraItem];
                
                [cell.cameraItem.ancestorSelectingItems removeAllObjects];
                [cell.cameraItem.ancestorSelectingItems addObjectsFromArray:insertselectingItems];
                
                insertTreeItemIndex++;
                
                BOOL contains = NO;
                
                for (CameraInfo *tmp2TreeItem in tmpTreeArray) {
                    
                    if ([tmp2TreeItem isEqualToSelectingItem:tmpTreeItem]) {
                        contains = YES;
                        
                        [self selectingItemsToDelete:tmp2TreeItem saveToArray:treeItemsToRemove];
                        
                        removeIndexPaths = [self removeIndexPathForTreeItems:(NSMutableArray *)treeItemsToRemove inSection:intSelectSection];
                    }
                }
                
                for (CameraInfo *tmp2TreeItem in treeItemsToRemove) {
                    [tmpTreeArray removeObject:tmp2TreeItem];
                    
                    for (CameraInfo *tmp3TreeItem in self.channelTreeItems) {
                        if ([tmp3TreeItem isEqualToSelectingItem:tmp2TreeItem]) {
                            //DBGLog(@"[XMS] Remove Cell:%@", tmp3TreeItem.title);
                            [self.channelTreeItems removeObject:tmp2TreeItem];
                            [tmp2RemoveAry removeObject:tmp2TreeItem];
                            break;
                        }
                    }
                }
                
                if (!contains) {
                    
                    [tmpTreeItem setSubmersionLevel:tmpTreeItem.submersionLevel];
                    
                    [tmpTreeArray insertObject:tmpTreeItem atIndex:insertTreeItemIndex];
                    
                    if (tmpTreeItem.deviceType == XMS_CHANNEL || tmpTreeItem.deviceType == XMS_VEHICLE_CH) {
                        [channelTreeItems addObject:tmpTreeItem];
                    }
                    
                    [tmp2RemoveAry addObject:tmpTreeItem];
                    
                    NSIndexPath *indexPth = [NSIndexPath indexPathForRow:insertTreeItemIndex inSection:intSelectSection];
                    [insertIndexPaths addObject:indexPth];
                }
            }
            
            if (cell.cameraItem.submersionLevel != 2) {
                [self iconButtonAction:cell];
            }
            
            if (currentEntry.submersionLevel != 2 && [insertIndexPaths count]==0 && [removeIndexPaths count]==0) {
                return;
            }
            
            if ([insertIndexPaths count]) {
                
                if (currentEntry.submersionLevel == 0) {
                    lastSelectSection = intSelectSection;
                    lastSelectRow = intSelectRow;
                    [tmpSelectDVR addObject:currentEntry];
                }
                [mainTable insertRowsAtIndexPaths:insertIndexPaths withRowAnimation:UITableViewRowAnimationFade];
                return;
            }
            
            if ([removeIndexPaths count]) {
                
                if (currentEntry.submersionLevel == 0) {
                    [tmpSelectDVR removeObject:currentEntry];
                }
                [mainTable deleteRowsAtIndexPaths:removeIndexPaths withRowAnimation:UITableViewRowAnimationFade];
                return;
            }
        }
    }
    
    //把tmpInfo.relations設給streamReceiver.currentCHList
    if (currentEntry.deviceType == XMS_CHANNEL || currentEntry.deviceType == XMS_DVR || currentEntry.deviceType == XMS_NVR) {
        if (channelTreeItems.count == 0) {
            NSMutableArray *tmpChAry = [[NSMutableArray alloc] init];
            for (CameraInfo *chInfo in streamReceiver.channelList) {
                if ([currentEntry.relations containsObject:chInfo.path]) {
                    [tmpChAry addObject:chInfo];
                }
            }
            streamReceiver.currentCHList = [NSMutableArray arrayWithArray:tmpChAry];
            [tmpChAry release];
        }
        else
            streamReceiver.currentCHList = [NSMutableArray arrayWithArray:channelTreeItems];
    }
    else if (currentEntry.deviceType == XMS_VEHICLE || currentEntry.deviceType == XMS_VEHICLE_DVR) {
        NSMutableArray *tmpChAry = [[NSMutableArray alloc] init];
        for (CameraInfo *v_chInfo in streamReceiver.v_channelList) {
            if ([v_chInfo.parent isEqual:currentEntry.child]) {
                [tmpChAry addObject:v_chInfo];
            }
        }
        streamReceiver.currentCHList = [NSMutableArray arrayWithArray:tmpChAry];
        [tmpChAry release];
    }
    else if (currentEntry.deviceType == XMS_VEHICLE_CH) {
        streamReceiver.currentCHList = [NSMutableArray arrayWithArray:channelTreeItems];
    }
    
    if (playingIndexList.count == 0) {
        
        streamReceiver.blnVehChannel = (blnVehSection ? YES : NO);
        streamReceiver.blnDvrChannel = (blnXmsIPcam ? NO : YES);
        blnXms1stConnect = YES;
        
        if ((currentEntry.deviceType == XMS_CHANNEL || currentEntry.deviceType == XMS_VEHICLE) && [detailView getViewMode] != 1 && !self.blnEventMode) {
            NSArray *_ary = [NSArray arrayWithObjects:streamReceiver.currentCHList, currentEntry, nil];
            [self autoFillMask:_ary];
        }
        else {
            [playingIndexList addObject:currentEntry];
            [self addStreaming:currentEntry.index-1 viewIdx:detailView.selectWindow];
        }
    }
    else {
        CameraInfo *tmpInfo = [[playingIndexList lastObject] retain];
        
        for(CameraInfo *tmpEntry in playingIndexList)
        {
            if ([tmpEntry isEqual:currentEntry])
            {
                DBGLog(@"[XMS] CH:%ld is Playing",currentEntry.index-1);
                return;
            }
        }
        
        if (0x1<<detailView.selectWindow & streamReceiver.videoControl.outputMask) {
            
            VideoDisplayView *tmpView = [streamReceiver.videoControl.aryDisplayViews objectAtIndex:detailView.selectWindow];
            if (tmpView.playingChannel == EMPTY_CHANNEL)
            {
                DBGLog(@"[XMS] View:%ld is empty",(long)detailView.selectWindow);
                return;
            }
            [self closeView:tmpView.playingChannel with:detailView.selectWindow];
        }
        
        if (![tmpInfo.ipAddr isEqualToString:currentEntry.ipAddr]) {
            blnXms1stConnect = YES;
        }
        
        if (playingIndexList.count<pow([detailView getViewMode],2) || [detailView getViewMode]==1) {
            
            if (tmpInfo.deviceType != currentEntry.deviceType || (!blnXmsIPcam && ![tmpInfo.ipAddr isEqualToString:currentEntry.ipAddr]))
            {
                [detailView disconnectAll];
                [NSThread sleepForTimeInterval:0.3f];
                for (NSInteger i=0; i<256; i++) {
                    [streamReceiver.outputList replaceObjectAtIndex:i withObject:[NSString stringWithFormat:@"99"]];
                }
                [playingIndexList removeAllObjects];
                currentPlayingMask = 0;
            }
            
            streamReceiver.blnVehChannel = (blnVehSection ? YES : NO);
            streamReceiver.blnDvrChannel = (blnXmsIPcam ? NO : YES);
            
            if (streamReceiver.blnDvrChannel && [detailView getViewMode] != 1 && blnXms1stConnect)
            {
                streamReceiver.blnConnectionClear = NO;
                streamReceiver.sessionId = nil;
                
                NSArray *_ary = [NSArray arrayWithObjects:streamReceiver.currentCHList, currentEntry, nil];
                [self autoFillMask:_ary];
            }
            else
            {
                [playingIndexList addObject:currentEntry];
                [self addStreaming:currentEntry.index-1 viewIdx:detailView.selectWindow];
            }
        }
        
        [tmpInfo release];
    }
    
    [currentEntry release];
    [mainTable reloadData];
}

@end
