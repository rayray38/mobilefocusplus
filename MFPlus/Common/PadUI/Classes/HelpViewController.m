//
//  HelpViewController.m
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/7.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()
{
    NSInteger       popType;
}

-(void)loadDocumentInHelpView;

@end

@implementation HelpViewController

@synthesize delegate,uType;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MMLog(@"[HV] Init %@",self);
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    [closeBtn setTitle:NSLocalizedString(@"BtnClose", nil)];
    [textView setText:[NSString stringWithFormat:@"v%@\n\n%@",appDelegate.version,NSLocalizedString(@"MsgUpdate", nil)]];
    [textView setFont:[UIFont systemFontOfSize:22]];
    [textView setTextColor:[UIColor lightTextColor]];
    [helpView setHidden:YES];
    [noteView setHidden:YES];
    
    popType = epvt_help;
}

- (void)viewWillAppear:(BOOL)animated
{
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.preferredContentSize = uType==0 ? HELPFRAME.size : POPVIEW_FRAME.size;
    
    if (uType == 0) {
        
        [self performSelector:@selector(loadDocumentInHelpView) withObject:nil afterDelay:0.0f];
        [helpView setHidden:NO];
        [noteView setHidden:YES];
    }else {
        
        [helpView setHidden:YES];
        [noteView setHidden:NO];
    }
}

- (void)dealloc
{
    MMLog(@"[HV] dealloc %@",self);
    
    [helpView loadHTMLString:@"" baseURL:nil];
    [helpView stopLoading];
    helpView.delegate = nil;
    
    SAVE_FREE(helpView);
    SAVE_FREE(textView);
    SAVE_FREE(noteView);
    SAVE_FREE(closeBtn);
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action

-(void)loadDocumentInHelpView
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",HELPURL]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    helpView.delegate = self;
    [helpView loadRequest:request];
}

- (void)closeView
{
    [delegate dismissPopView:popType];
}

@end
