//
//  DeviceViewController.m
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/1.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import "DeviceViewController.h"
#import "EzAccessoryView.h"

enum {
#define MAX_ROW_ITEM    9
    edr_name = 0x0,
    edr_model,
    edr_url,
    edr_port,
    edr_user,
    edr_pwd,
    edr_sub,
    edr_push,
    edr_scan,
} EDeviceRow;

@interface DeviceViewController ()
{
    NSInteger       popType;
}

- (void)saveDevice;
- (void)closeView;
- (void)showAlarm:(NSString *)message;
- (void)subSwitchChangeValue:(id)sender;
- (void)subSwitchCloseNotify:(id)sender;

@end

@implementation DeviceViewController

@synthesize delegate,blnNewDevice,editView,scanView;
@synthesize device,deviceCopy;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MMLog(@"[DV] Init %@",self);
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(closeView)];
    [self.navigationItem setLeftBarButtonItem:cancelBtn];
    SAVE_FREE(cancelBtn);
    
    UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveDevice)];
    [self.navigationItem setRightBarButtonItem:saveBtn];
    SAVE_FREE(saveBtn);
    
    devTypeArray = deviceCopy.product == DEV_DVR ? [Device createDvrModelArray] : [Device createIpcamModelArray];
    
    popType = epvt_device;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.preferredContentSize = POPVIEW_FRAME.size;
    
    [mainTable reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.title = blnNewDevice ? NSLocalizedString(@"AddDevice", nil) : deviceCopy.name;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)dealloc
{
    MMLog(@"[DV] dealloc %@",self);
    
    SAVE_FREE(editView);
    //SAVE_FREE(scanView);
    SAVE_FREE(device);
    SAVE_FREE(deviceCopy);
    SAVE_FREE(devTypeArray);
    delegate = nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action

- (void)closeView
{
    [delegate dismissPopView:popType];
}

- (void)saveDevice
{
    // check if all items have been filled
	if ([deviceCopy.name length]==0 || [deviceCopy.ip length]==0 || deviceCopy.port==0 /*||
		[deviceCopy.user length]==0 || [deviceCopy.password length]==0*/ ) {
		
		[self showAlarm:NSLocalizedString(@"MsgCheckAllItem", nil)];
	} else {
    
		EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
		
		if (blnNewDevice) {
			
            NSMutableArray *tmpArray = (deviceCopy.product == DEV_DVR ? [appDelegate.dvrs lastObject] : [appDelegate.ipcams lastObject]);
			[tmpArray addObject:deviceCopy];
		}
		else
			[device setValueWithDevice:deviceCopy];
		
		// save it to database
        BOOL ret = (deviceCopy.product == DEV_DVR) ? [appDelegate refreshDVRsIntoDatabase] : [appDelegate refreshIPCamsIntoDatabase];
        if (!ret) {
            [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            // Register deviceToken to server
            if (deviceCopy.type == XMS_SERIES) {
                
                if (appDelegate._DeviceToken != nil) {
                    
                    streamReceiver = [[StreamReceiver alloc] initWithDevice:deviceCopy];
                    NSArray *ary = [NSArray arrayWithObjects:appDelegate._DeviceToken, deviceCopy.name, nil];
                    [streamReceiver registDevTokenToServer:ary];
                    if (streamReceiver.xmsUUID != nil) {
                        
                        deviceCopy.uuid = [streamReceiver.xmsUUID copy];
                        [device setValueWithDevice:deviceCopy];
                        [appDelegate refreshDVRsIntoDatabase];
                    }
                    [streamReceiver release];
                }
                else
                    DBGLog(@"[DV] DeviceToken nil");
            }
        });
        
        [self closeView];
	}
}

- (void)subSwitchChangeValue:(id)sender
{
    UISwitch *subSwitch = (UISwitch *)sender;
    if (subSwitch.isOn) {
        
        if ( ![self ifSupportMobileStream:deviceCopy] ) {
            
            [self showAlarm:NSLocalizedString(@"MsgSubDenied", nil)];
            [subSwitch setOn:NO animated:YES];
        }
        else
            deviceCopy.dualStream = 1;
    }
    else
        deviceCopy.dualStream = 0;
//    deviceCopy.dualStream = subSwitch.isOn;
}

- (void)subSwitchCloseNotify:(id)sender
{
    UISwitch *subSwitch = (UISwitch *)sender;
    if (subSwitch.isOn) {
        
        deviceCopy.blnCloseNotify = 0;   //receive remote notify
    }
    else
        deviceCopy.blnCloseNotify = 1;  //do not receive remote notify
}

#pragma mark - TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //return blnNewDevice ? 2 : 1;
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return MAX_ROW_ITEM;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 1 ? 0 : 25;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = nil;
    
    headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 25, tableView.bounds.size.width, 30)] autorelease];
    [headerView setBackgroundColor:[UIColor BACOLOR]];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.bounds.size.width-10, 18)] autorelease];
    NSString *title = @"";
    
    switch ( deviceCopy.product ) {
        case DEV_DVR:
            title = @"DVR";
            break;
        case DEV_IPCAM:
            title = @"IPCam";
            break;
    }
    
    [titleLabel setText:title];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    if (indexPath.row == edr_push) {
        if (deviceCopy.type == XMS_SERIES) {
            height = 44;
        }
        else
            height = 0;
    }
    else if (indexPath.row == edr_port) {
        if (deviceCopy.type == RTSP || [deviceCopy.ip rangeOfString:@".everfocusddns.com" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            height = 0;
        }
        else
            height = 44;
    }
    else if (indexPath.row == edr_sub) {
        if (deviceCopy.type == RTSP) {
            height = 0;
        }
        else
            height = 44;
    }
    else
        height = 44;
    
    return height;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		[cell setEditingAccessoryType:UITableViewCellAccessoryNone];
		[cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setTextColor:[UIColor EFCOLOR]];
        [cell.detailTextLabel setTextColor:[UIColor lightTextColor]];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
		
        if (indexPath.row == edr_sub) {
            
            EZAccessoryView *ezView = [[EZAccessoryView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
            [ezView.detailSwitch addTarget:self action:@selector(subSwitchChangeValue:) forControlEvents:UIControlEventValueChanged];
            
            [cell setAccessoryView:ezView];
            SAVE_FREE(ezView);
        }
        
        if (indexPath.row == edr_push) {
            
            //EzAccessoryView
            EZAccessoryView *ezView = [[EZAccessoryView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
            [ezView.detailSwitch addTarget:self action:@selector(subSwitchCloseNotify:) forControlEvents:UIControlEventValueChanged];
            
            [cell setAccessoryView:ezView];
            SAVE_FREE(ezView);
        }
	}
	
    // Configure the cell...
    EZAccessoryView *ezView = (EZAccessoryView *)cell.accessoryView;
    switch (indexPath.row) {
        case edr_name:
            cell.textLabel.text = NSLocalizedString(@"DeviceName",nil);
            cell.detailTextLabel.text = deviceCopy.name;
            break;
        case edr_model:
            cell.textLabel.text = NSLocalizedString(@"DeviceModel",nil);
            cell.detailTextLabel.text = [Device modelIndexToString:deviceCopy.type product:deviceCopy.product];
            break;
        case edr_url:
            cell.textLabel.text = NSLocalizedString(@"DeviceIP",nil);
            cell.detailTextLabel.text = deviceCopy.ip;
            break;
        case edr_port:
            if (deviceCopy.type == RTSP || [deviceCopy.ip rangeOfString:@".everfocusddns.com" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [cell setHidden:YES];
            }
            else {
                cell.textLabel.text = NSLocalizedString(@"DevicePort",nil);
                cell.detailTextLabel.text = @"";
                if (deviceCopy.port!=0)
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld",(long)deviceCopy.port];
            }
            break;
        case edr_user:
            cell.textLabel.text = NSLocalizedString(@"DeviceUser",nil);
            cell.detailTextLabel.text = deviceCopy.user;
            break;
        case edr_pwd:
            cell.textLabel.text = NSLocalizedString(@"DevicePWD",nil);
            cell.detailTextLabel.text = [@"" stringByPaddingToLength:[deviceCopy.password length]
                                                          withString:@"*" startingAtIndex:0];
            break;
        case edr_sub:
            if (deviceCopy.type != RTSP) {
                cell.textLabel.text = NSLocalizedString(@"DeviceSub",nil);
                [ezView setDetailType:edt_switch];
                if (deviceCopy.product == DEV_DVR) {
                    if (deviceCopy.type==ECOR264_8D1 || deviceCopy.type==ELR_8D_8F || deviceCopy.type==EPARA264_32) {
                        deviceCopy.dualStream = 0;
                    }
                }
                [ezView.detailSwitch setOn:(deviceCopy.dualStream==1?YES:NO) animated:NO];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            else
                [cell setHidden:YES];
            
            break;
        case edr_push:
            if (deviceCopy.type == XMS_SERIES) {
                cell.textLabel.text = NSLocalizedString(@"NotifyReceive", nil);
                [ezView setDetailType:edt_switch];
                [ezView.detailSwitch setOn:(deviceCopy.blnCloseNotify==0?YES:NO) animated:NO];
            }
            else
                [cell setHidden:YES];
            break;
        case edr_scan:
            if (blnNewDevice) {
                cell.textLabel.font = [UIFont boldSystemFontOfSize:20];
                cell.textLabel.text = NSLocalizedString(@"ScanDevice", nil);
                cell.detailTextLabel.text = nil;
            }
            else
                [cell setHidden:YES];
            break;
    }
    
    return cell;
}

#pragma mark - TableView Delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (indexPath.row == edr_sub || indexPath.row == edr_push) ? nil : indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == edr_scan) {
        //push to ScanView here
        if (!self.scanView)      //init ScanView
        {
            IPToolViewController *viewController = [[IPToolViewController alloc] initWithNibName:@"IPToolViewController" bundle:[NSBundle mainBundle]];
            self.scanView = viewController;
            [viewController release];
        }
        
        self.scanView.devListFilter = deviceCopy.product;
        self.scanView.device = deviceCopy;
        [self.navigationController pushViewController:self.scanView animated:YES];
        return;
    }
    
    if (self.editView == nil) {
        DeviceEditViewController *viewController = [[DeviceEditViewController alloc] initWithNibName:@"DeviceEditViewController" bundle:[NSBundle mainBundle]];
        self.editView = viewController;
        [viewController release];
    }
    
    self.editView.device = deviceCopy;
    self.editView.pickerViewArray = devTypeArray;
    switch (indexPath.row) {
        case edr_name:
            self.editView.editedPropertyKey = @"name";
            self.editView.editedPropertyDisplayName = NSLocalizedString(@"DeviceName", nil);
            break;
        case edr_model:
            self.editView.editedPropertyKey = @"type";
            self.editView.editedPropertyDisplayName = NSLocalizedString(@"DeviceModel", nil);
            break;
        case edr_url:
            self.editView.editedPropertyKey = @"ip";
            self.editView.editedPropertyDisplayName = NSLocalizedString(@"DeviceIP", nil);
            break;
        case edr_port:
            self.editView.editedPropertyKey = @"port";
            self.editView.editedPropertyDisplayName = NSLocalizedString(@"DevicePort", nil);
            break;
        case edr_user:
            self.editView.editedPropertyKey = @"user";
            self.editView.editedPropertyDisplayName = NSLocalizedString(@"DeviceUser", nil);
            break;
        case edr_pwd:
            self.editView.editedPropertyKey = @"password";
            self.editView.editedPropertyDisplayName = NSLocalizedString(@"DevicePWD", nil);
            break;
//        case edr_rtsp:
//            self.editView.editedPropertyKey = @"rtspport";
//            self.editView.editedPropertyDisplayName = NSLocalizedString(@"DeviceRTSP", nil);
//            break;
    }
    
    if (indexPath.row != edr_sub || indexPath.row != edr_push) {
        [self.navigationController pushViewController:self.editView animated:YES];
    }
}

#pragma mark - UIAlertView Delegate

- (void)showAlarm:(NSString *)message
{
	if ( message!=nil ) {
		
		// open an alert with just an OK button, touch "OK" will do nothing
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"BtnOK", nil) otherButtonTitles:nil];
		
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
	}
}

#pragma mark - Local functions

- (BOOL)ifSupportMobileStream:(Device *)dev
{
    BOOL ret;
    if (dev.product == DEV_IPCAM) {
        return ret = YES;
    }
    if (dev.type==ECOR264_8D1 || dev.type==ELR_8D_8F || dev.type==EPARA264_32) {
        dev.dualStream = 0;
        ret = NO;
    }
    else
        ret = YES;
    
    return ret;
}

@end
