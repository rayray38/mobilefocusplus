//
//  SplitViewController.h
//  EFViewerHD
//
//  Created by James Lee on 12/11/28.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/CALayer.h>

enum {
    epvt_device = 0x1000,
    epvt_group,
    epvt_help,
    epvt_scan,
    epvt_import,
    epvt_export,
    epvt_search,
    epvt_alarm,
    
} EPopViewType;

@protocol contentDelegate

- (void)dismissPopView:(NSInteger)_type;
- (void)statusCallback:(NSString *)_message;

@end

@interface SplitViewController : UISplitViewController<UISplitViewControllerDelegate,UIPopoverControllerDelegate>
{
    UIView  *maskView;
    UIPopoverController     *popView;
    
    UIView          *tipView;
    UILabel         *tipText;
}

- (void)showPopViewWith:(UIViewController *)viewCtrl;
- (void)showPopAlarmWith:(UIViewController *)viewCtrl;
- (void)closePopView;
- (void)showTips:(NSString *)message with:(CGFloat)duration;
- (void)closeTips;

@end
