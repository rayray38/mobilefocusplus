//
//  ScanViewController.h
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/9.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFViewerAppDelegate.h"
#import "ZBarSDK.h"
#import "ImportViewController.h"

@interface ScanViewController : UIViewController<ZBarReaderDelegate,ZBarReaderViewDelegate>
{
    IBOutlet ZBarReaderView     *readerView;
}

@property(nonatomic,retain) id<contentDelegate>     delegate;
@property(nonatomic)        NSInteger               mode;
@property(nonatomic,retain) ImportViewController    *importView;

@end
