//
//  XmsVC.h
//  EFViewerPlus
//
//  Created by Ray Lin on 2015/05/26.
//  Copyright (c) 2015年 EverFocus. All rights reserved.
//

#import "ChannelViewController.h"
#import "ExTableHeader.h"

#define XMS_SECTION_IPCAM   0
#define XMS_SECTION_DVR     1
#define XMS_SECTION_RTSP    2
#define XMS_SECTION_VEHICLE 3

@interface XMSViewController : ChannelViewController
{
    NSMutableArray     *streamArray; //save the streamReceivers
    NSMutableArray     *ipcamArray;  //20150716 added by Ray Lin to save the ipcam in XMS
    NSMutableArray     *dvrArray;    //20150716 added by Ray Lin to save the dvr in XMS
    NSMutableArray     *rtspArray;   //20150917 added by Ray Lin to save the rtsp in XMS
    NSMutableArray     *vehicleArray;//20151120 added by Ray Lin, for xms support vehicle tree view test
    NSMutableArray     *tmpTreeArray_copy;//20151120 added by Ray Lin, for xms support vehicle tree view test
    NSMutableArray     *tmp2RemoveAry;
    NSMutableArray     *tmpSelectDVR;//20160602 added by Ray Lin, avoid expanding two DVR's channels at the same time
    
    NSInteger          intSelectSection;
    NSInteger          intSelectRow;
    NSInteger          lastSelectSection;//20160604 added by Ray Lin, to remember last select section in XMS
    NSInteger          lastSelectRow;//20160604 added by Ray Lin, to remember last select row in XMS
}

@property (nonatomic, retain) NSMutableArray *channelTreeItems;   //20151120 added by Ray Lin, for xms support vehicle tree view test

@end
