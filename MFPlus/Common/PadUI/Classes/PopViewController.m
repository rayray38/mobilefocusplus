//
//  PopViewController.m
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/12/3.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import "PopViewController.h"

@interface PopViewController ()
{
    UIImageView          *render;
    VideoDisplayView     *vdView;
    NSInteger            popType;
    NSInteger            timerTick;
    NSTimer              *wDog;
}

- (void)closeViewAct;
- (void)closeView;
- (void)snapShotSingle;
- (void)killWDog;
- (void)checkAlarmProcess;
- (void)showAlarm:(NSString *)message;

@end

@implementation PopViewController

@synthesize delegate,streamer,viewIdx;

#pragma mark - View LifeCycle

- (id)init
{
    if (self = [super init]) {
        
    }
    
    MMLog(@"[POP] Init %@",self);
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //Set Left button : Close button
    UIBarButtonItem *closeBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                              target:self
                                                                              action:@selector(closeViewAct)];
    [self.navigationItem setLeftBarButtonItem:closeBtn];
    SAVE_FREE(closeBtn);
    
    //Set Right button : Snap button
    UIButton *btnSnap = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [btnSnap setImage:[UIImage imageNamed:@"snapshot.png"] forState:UIControlStateNormal];
    [btnSnap addTarget:self action:@selector(snapShotSingle) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *snapBtn = [[UIBarButtonItem alloc] initWithCustomView:btnSnap];
    self.navigationItem.rightBarButtonItem = snapBtn;
    SAVE_FREE(btnSnap);
    SAVE_FREE(snapBtn);
    
    [self.view setBackgroundColor:[UIColor BGCOLOR]];
    render = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, POPALARM_FRAME.size.width, POPALARM_FRAME.size.height)];
    [self.view addSubview:render];
    
    popType = epvt_alarm;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.preferredContentSize = POPALARM_FRAME.size;
    
    vdView = [[VideoDisplayView alloc] initWithView:render button:nil];
    [vdView showView:YES];
    [vdView resetView:YES isFull:NO];
    
    [streamer.videoControl setSubWindowWithView:vdView byIndex:viewIdx];
    
    wDog = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                 target:self
                                               selector:@selector(checkAlarmProcess)
                                               userInfo:nil
                                                repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [streamer.videoControl removeSubWindow];
    streamer = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    MMLog(@"[POP] dealloc %@",self);
    
    SAVE_FREE(vdView);
    SAVE_FREE(render);
    
    [super dealloc];
}

#pragma mark - Action

- (void)closeViewAct
{
    if (wDog)
        [self killWDog];
    
    [self closeView];
}

- (void)closeView
{
    if (wDog) {
        [wDog invalidate];
        wDog = nil;
    }
    
    [delegate dismissPopView:popType];
}

- (void)snapShotSingle
{
    [self killWDog];

    UIImage *image = [[vdView videoImage] retain];
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *albumName = [NSString stringWithFormat:@"%@",appDelegate._AppName];
    [appDelegate.mediaLibrary saveImage:image toAlbum:albumName withCompletionBlock:^(NSError *error) {
        if (error!=nil) {
            [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
        }else {
            [self showAlarm:NSLocalizedString(@"MsgSnapOK", nil)];
        }
    }];
    
    [image release];
}

- (void)checkAlarmProcess
{
    if (++timerTick > 5) {
        [wDog invalidate];
        wDog = nil;
        [self closeView];
    }
}

- (void)killWDog
{
    if (wDog) {
        [wDog invalidate];
        wDog = nil;
    }
    [delegate statusCallback:@"ALARM_POP:OFF"];
}

#pragma mark - Alert View delegate

- (void)showAlarm:(NSString *)message
{
    if ( message!=nil ) {
        
        // open an alert with just an OK button, touch "OK" will do nothing
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];[alert show];
    }
}

@end
