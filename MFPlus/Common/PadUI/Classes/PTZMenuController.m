//
//  PTZMenuController.m
//  EFViewerHD
//
//  Created by James Lee on 12/11/1.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "PTZMenuController.h"
#import "iCStreamReceiver.h"

@interface PTZMenuController()

- (void)zoomFromGesture:(NSString *)msg;
- (void)swipeFromGesture:(NSString *)msg;
- (void)stopFromGesture;
- (void)passPtzCmd:(NSInteger)Idx;

@end
@implementation PTZMenuController

@synthesize ptzStreamReceiver,liveChannel,ptzStreamArray;

#pragma mark - View LifeCycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MMLog(@"[PTZ] Init %@",self);
    statusBtnArray = [[NSMutableArray alloc] initWithObjects:btnPreset,btnTour,btnPattern,btnOSD,btnAutoPan, nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    DetailViewController *detailView = (DetailViewController *)[[[appDelegate.splitViewController.viewControllers objectAtIndex:1] viewControllers] firstObject];
    detailView.delegate = self;
    
    strValue = [[NSMutableString alloc] initWithFormat:@"0"];
    ptzStatus = PTZ_PRESET;
    
    for (UIButton *tmpBtn in statusBtnArray) {
        
        if (tmpBtn.tag == PTZ_PRESET) 
            [tmpBtn setBackgroundImage:BTN_2_OVER forState:UIControlStateNormal];
        else
            [tmpBtn setBackgroundImage:BTN_2 forState:UIControlStateNormal];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.ptzStreamReceiver = nil;
    self.ptzStreamArray = nil;

    [strValue release];
    [super viewWillDisappear:animated];
}

- (void)dealloc
{
    MMLog(@"[PTZ] dealloc %@",self);
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Close PTZ View

- (void)closePTZView
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - PTZ Command

- (void)sendPtzCmd:(id)sender
{
    UIButton *tmpBtn = (UIButton *)sender;
    [self passPtzCmd:tmpBtn.tag];
}

- (void)passPtzCmd:(NSInteger)Idx
{
    NSInteger channel = self.liveChannel;
    if (ptzStreamReceiver.streamType == STREAM_ICATCH) {
        CameraInfo *curInfo = [ptzStreamReceiver.cameraList objectAtIndex:self.liveChannel];
        [ptzStreamReceiver ptzController].baudrate = curInfo.baudrate;
        
        [ptzStreamReceiver ptzController].ptzType = curInfo.ptzType;
        channel = curInfo.ptzID;
    }
    
    switch (Idx) {
        case 11:
            [[ptzStreamReceiver ptzController] ptzTiltUp:channel];
            [panelImage setImage:IMG_PTZ1];
            break;
        case 12:
            [[ptzStreamReceiver ptzController] ptzRightUp:channel];
            [panelImage setImage:IMG_PTZ2];
            break;
        case 13:
            [[ptzStreamReceiver ptzController] ptzPanRight:channel];
            [panelImage setImage:IMG_PTZ3];
            break;
        case 14:
            [[ptzStreamReceiver ptzController] ptzRightDown:channel];
            [panelImage setImage:IMG_PTZ4];
            break;
        case 15:
            [[ptzStreamReceiver ptzController] ptzTiltDown:channel];
            [panelImage setImage:IMG_PTZ5];
            break;
        case 16:
            [[ptzStreamReceiver ptzController] ptzLeftDown:channel];
            [panelImage setImage:IMG_PTZ6];
            break;
        case 17:
            [[ptzStreamReceiver ptzController] ptzPanLeft:channel];
            [panelImage setImage:IMG_PTZ7];
            break;
        case 18:
            [[ptzStreamReceiver ptzController] ptzLeftUp:channel];
            [panelImage setImage:IMG_PTZ8];
            break;
        case 19:
            [[ptzStreamReceiver ptzController] ptzZoomIn:channel];
            break;
        case 20:
            [[ptzStreamReceiver ptzController] ptzZoomOut:channel];
            break;
        case 21:
            [[ptzStreamReceiver ptzController] ptzFocusNear:channel];
            break;
        case 22:
            [[ptzStreamReceiver ptzController] ptzFocusFar:channel];
            break;
        case 23:
            [[ptzStreamReceiver ptzController] ptzIrisOpen:channel];
            break;
        case 24:
            [[ptzStreamReceiver ptzController] ptzIrisClose:channel];
            break;
        case 25:
            if (blnAutoPan) 
                [self sendPtzCmd:btnAutoPan];
            
            switch (ptzStatus) {
                case PTZ_PRESET:
                    [[ptzStreamReceiver ptzController] ptzPresetGo:channel value:ptzValue];
                    break;
                case PTZ_PATTERN:
                    [[ptzStreamReceiver ptzController] ptzPattern:channel value:ptzValue];
                    break;
                case PTZ_TOUR:
                    [[ptzStreamReceiver ptzController] ptzTour:channel value:ptzValue];
                    break;
                default:
                    break;
            }
            ptzValue = 0;
            [strValue setString:@"0"];
            [valueLabel setText:strValue];
            
            break;
        case 26:
            [[ptzStreamReceiver ptzController] ptzPresetSet:channel value:ptzValue];
            break;
        case 27:
            if (ptzStatus == PTZ_AUTOPAN) break;
            
            if (blnOsdOpen) {
                [[ptzStreamReceiver ptzController] ptzOsdExit:channel];
                [btnOSD setBackgroundImage:BTN_2 forState:UIControlStateNormal];
                ptzStatus = PTZ_NONE;
            }else {
                [[ptzStreamReceiver ptzController] ptzOsdOpen:channel];
                [btnOSD setBackgroundImage:BTN_2_OVER forState:UIControlStateNormal];
                ptzStatus = PTZ_OSD;
                [self setPtzStatus:btnOSD];
            }
            blnOsdOpen = !blnOsdOpen;
            break;
        case 28:
            if (ptzStatus == PTZ_OSD) break;
            
            if (blnAutoPan) {
                [[ptzStreamReceiver ptzController] ptzAutoPanStop:channel];
                [btnAutoPan setBackgroundImage:BTN_2 forState:UIControlStateNormal];
                ptzStatus = PTZ_NONE;
            }else {
                [[ptzStreamReceiver ptzController] ptzAutoPanRun:channel];
                [btnAutoPan setBackgroundImage:BTN_2_OVER forState:UIControlStateNormal];
                ptzStatus = PTZ_AUTOPAN;
                [self setPtzStatus:btnAutoPan];
            }
            blnAutoPan = !blnAutoPan;
            break;
        default:
            break;
    }
}

- (void)sendPtzStop
{
    NSInteger channel = self.liveChannel;
    if (ptzStreamReceiver.streamType == STREAM_ICATCH) {
        CameraInfo *curInfo = [ptzStreamReceiver.cameraList objectAtIndex:self.liveChannel];
        [ptzStreamReceiver ptzController].baudrate = curInfo.baudrate;
        [ptzStreamReceiver ptzController].ptzType = curInfo.ptzType;
        channel = curInfo.ptzID;
    }
    
    [[ptzStreamReceiver ptzController] ptzStop:channel];
    [panelImage setImage:IMG_NONE];
}

- (IBAction)setPtzStatus:(id)sender
{
    NSInteger statusTag = ((UIButton *)sender).tag;
    for (NSInteger i=0; i<3; i++) {
        [(UIButton *)[statusBtnArray objectAtIndex:i] setBackgroundImage:BTN_2 forState:UIControlStateNormal];
    }
    
    if (ptzStatus==PTZ_OSD || ptzStatus==PTZ_AUTOPAN)
        return;
    
    switch (statusTag) {
        case PTZ_PRESET:
            [statusLabel setText:@"PRESET"];
            [btnPreset setBackgroundImage:BTN_2_OVER forState:UIControlStateNormal];
            break;
        case PTZ_PATTERN:
            [statusLabel setText:@"PATTERN"];
            [btnPattern setBackgroundImage:BTN_2_OVER forState:UIControlStateNormal];
            break;
        case PTZ_TOUR:
            [statusLabel setText:@"TOUR"];
            [btnTour setBackgroundImage:BTN_2_OVER forState:UIControlStateNormal];
            break;
        default:
            break;
    }
    
    ptzStatus = statusTag;
}

- (IBAction)setPtzValue:(id)sender
{
    NSInteger valueTag = ((UIButton *)sender).tag;
    
    if (valueTag == 55) {
        
        NSInteger lastStrPos = strValue.length;
        if (lastStrPos == 1) {
            [strValue setString:@"0"];
        }else {
            [strValue deleteCharactersInRange:NSMakeRange(lastStrPos-1, 1)];
        }
        [valueLabel setText:strValue];
        ptzValue = [strValue integerValue];
        return;
    }
    if ([strValue isEqualToString:@"0"]) {
        [strValue setString:[NSString stringWithFormat:@"%ld",(long)valueTag]];
    }else {
        [strValue appendString:[NSString stringWithFormat:@"%ld",(long)valueTag]];
    }
    [valueLabel setText:strValue];
    ptzValue = [strValue integerValue];
}

- (void)zoomFromGesture:(NSString *)msg
{
    if ([msg isEqualToString:@"IN"])
        [self passPtzCmd:19];
    else
        [self passPtzCmd:20];
}

- (void)swipeFromGesture:(NSString *)msg
{
    if ([msg isEqualToString:@"LEFT"]) {
        
        [self passPtzCmd:17];
    }
    else if ([msg isEqualToString:@"RIGHT"]) {

        [self passPtzCmd:13];
    }
    else if ([msg isEqualToString:@"UP"]) {
        
        [self passPtzCmd:11];
    }
    else if ([msg isEqualToString:@"DOWN"]) {
        
        [self passPtzCmd:15];
    }
}

- (void)stopFromGesture
{
    [self sendPtzStop];
}

#pragma mark - mainControl Delegate

- (void)controlStatusCallback:(NSString *)message
{
    CMLog(@"[PTZ](ctrlMsg) %@",message);
    NSArray *cmdArray = [message componentsSeparatedByString:@":"];
    if ([[cmdArray firstObject] isEqualToString:@"CLOSEPTZ"]) {
        [self closePTZView];
    }
    else if ([[cmdArray firstObject] isEqualToString:@"PTZZOOM"]) {
        [self zoomFromGesture:[cmdArray objectAtIndex:1]];
    }
    else if ([[cmdArray firstObject] isEqualToString:@"PTZSWIPE"]) {
        [self swipeFromGesture:[cmdArray objectAtIndex:1]];
    }
    else if ([[cmdArray firstObject] isEqualToString:@"PTZSTOP"]) {
        [self stopFromGesture];
    }
    else if ([[cmdArray firstObject] isEqualToString:@"CHANGECH"]) {
        self.liveChannel = [[cmdArray objectAtIndex:1] integerValue];
        
        if (ptzStreamArray) {
            for (StreamReceiver *tmpStreamReceiver in ptzStreamArray) {
                
                if ([[tmpStreamReceiver.outputList firstObject] integerValue] == [[cmdArray objectAtIndex:2] integerValue]) {
                    ptzStreamReceiver = tmpStreamReceiver;
                    self.liveChannel = 0;
                    break;
                }
            }
        }
    }
}

@end
