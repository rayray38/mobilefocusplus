//
//  PTZMenuController.h
//  EFViewerHD
//
//  Created by James Lee on 12/11/1.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreamReceiver.h"
#import "DetailViewController.h"

#define BTN_1             [UIImage imageNamed:@"button.png"]
#define BTN_1_OVER        [UIImage imageNamed:@"button_over.png"]
#define BTN_2             [UIImage imageNamed:@"button2.png"]
#define BTN_2_OVER        [UIImage imageNamed:@"button2_over.png"]
#define IMG_NONE          [UIImage imageNamed:@"ptz0.png"]
#define IMG_PTZ1          [UIImage imageNamed:@"ptz1.png"]
#define IMG_PTZ2          [UIImage imageNamed:@"ptz2.png"]
#define IMG_PTZ3          [UIImage imageNamed:@"ptz3.png"]
#define IMG_PTZ4          [UIImage imageNamed:@"ptz4.png"]
#define IMG_PTZ5          [UIImage imageNamed:@"ptz5.png"]
#define IMG_PTZ6          [UIImage imageNamed:@"ptz6.png"]
#define IMG_PTZ7          [UIImage imageNamed:@"ptz7.png"]
#define IMG_PTZ8          [UIImage imageNamed:@"ptz8.png"]

@interface PTZMenuController : UIViewController<mainControlDelegate>
{
    NSInteger           ptzStatus;
    NSInteger           ptzValue;
    NSMutableString     *strValue;
//    IBOutlet UILabel    *channelLabel;
    IBOutlet UILabel    *statusLabel;
    IBOutlet UITextField*valueLabel;
    IBOutlet UIImageView*panelImage;
    
    BOOL                blnOsdOpen;
    BOOL                blnAutoPan;
    
    IBOutlet UIButton   *btnPreset;
    IBOutlet UIButton   *btnTour;
    IBOutlet UIButton   *btnPattern;
    IBOutlet UIButton   *btnOSD;
    IBOutlet UIButton   *btnAutoPan;
    
    NSMutableArray      *statusBtnArray;
    
}

@property(nonatomic,retain) StreamReceiver      *ptzStreamReceiver;
@property(nonatomic,retain) NSMutableArray      *ptzStreamArray;
@property(nonatomic)        NSUInteger          liveChannel;

- (void)closePTZView;
- (IBAction)sendPtzCmd:(id)sender;
- (IBAction)sendPtzStop;
- (IBAction)setPtzStatus:(id)sender;
- (IBAction)setPtzValue:(id)sender;
- (void)setLiveChannel:(NSUInteger)ch;

@end
