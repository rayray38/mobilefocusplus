//
//  MasterViewController.h
//  EFViewerHD
//
//  Created by James Lee on 12/10/5.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"
#import "ChannelViewController.h"
#import "IPViewController.h"
#import "XMSViewController.h"
#import "PTZMenuController.h"
#import "Reachability.h"
#import "EzAccessoryView.h"
#import "DBControl.h"
#import "StreamReceiver.h"
#import "EventNotifyListVC.h"  //20160808 ++ by Ray Lin

#define IMG_DVR [UIImage imageNamed:@"dvr.png"]
#define IMG_DVROVER [UIImage imageNamed:@"dvrover.png"]
#define IMG_IPCAM [UIImage imageNamed:@"ipcam.png"]
#define IMG_IPCAMOVER [UIImage imageNamed:@"ipcamover.png"]
#define IMG_EVENT [UIImage imageNamed:@"alarmpop.png"]
#define IMG_EVENTOVER [UIImage imageNamed:@"alarmpopover.png"]
#define IMG_HELP [UIImage imageNamed:@"setting.png"]
#define IMG_HELPOVER [UIImage imageNamed:@"settingover.png"]

#define TOOLBAR_HEIGHT    44

#define MsgUserNotify @"Tap the link to download the User Manual in PDF. Ensure your mobile phone have PDF reader installed. Please be noted that downloading file will consume certain data volume of your network traffic."

@interface MasterViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,
                                                    UISearchBarDelegate, UISearchDisplayDelegate,
                                                    UIActionSheetDelegate, UIGestureRecognizerDelegate,
                                                    contentDelegate, mainControlDelegate,EZViewDelegate>
{
    IBOutlet UITableView *devTableView;
    IBOutlet UIToolbar   *devToolBar;
    IBOutlet UIButton    *dvrBtn;
    IBOutlet UIButton    *ipBtn;
    IBOutlet UIButton    *setBtn;
    IBOutlet UIButton    *eventBtn;  //20160107 added by Ray Lin, for push event list
    UIBarButtonItem      *btnAdd;
    
	NSMutableArray       *devArray;
    NSInteger            devListFilter;
    NSMutableArray       *groupArray;
    NSMutableArray       *groupFilterArray;
    
    UISearchDisplayController   *searchDisplay;
    UISearchBar                 *searchBar;
    NSMutableArray              *searchResult;
    
    BOOL                        blnEventMode;
    DBControl                   *dbControl;
    StreamReceiver              *streamReceiver;
    EventTable                  *evn;
}

@property (nonatomic, retain) ChannelViewController *channelView;
@property (nonatomic, retain) IPViewController      *ipView;
@property (nonatomic, retain) XMSViewController     *xmsView;
@property (nonatomic, retain) PTZMenuController     *ptzView;
@property (nonatomic, retain) EventNotifyListVC     *notifyView;//20160808 ++ by Ray Lin

- (IBAction)toolBarItemSelect:(id)sender;

@end
