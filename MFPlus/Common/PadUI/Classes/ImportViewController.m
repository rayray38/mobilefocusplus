//
//  ImportViewController.m
//  EFViewerHD
//
//  Created by Nobel on 13/10/2.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import "ImportViewController.h"

@interface ImportViewController()
{
    NSInteger       popType;
}

- (void)closeView;
- (void)parseStringToList:(NSString *)string;
- (void)addToDeviceList;
- (void)showAlarm:(NSString *)message;

@end

@implementation ImportViewController

@synthesize delegate,resultString;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MMLog(@"[IPV] Init %@",self);
    
    devList = [[NSMutableArray alloc] init];
    checkList = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *btnImport = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"BtnImport", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(addToDeviceList)];
    [self.navigationItem setRightBarButtonItem:btnImport];
    [btnImport release];
    
    popType = epvt_import;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.preferredContentSize = POPVIEW_FRAME.size;
    
    [self parseStringToList:self.resultString];
    [devTableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [devList removeAllObjects];
    [checkList removeAllObjects];
}

- (void)dealloc
{
    MMLog(@"[IPV] dealloc %@",self);
    
    [devList release];
    devList = nil;
    
    [checkList release];
    checkList = nil;
    
    [resultString release];
    resultString = nil;
    
    [devTableView release];
    devTableView = nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)closeView
{
    [delegate dismissPopView:popType];
}

- (void)parseStringToList:(NSString *)string
{
    NSArray *devAry = [string componentsSeparatedByString:@"<DEVICE>"];
    for (NSInteger i=1; i<devAry.count; i++) {
        NSString *devStr = [devAry objectAtIndex:i];
        NSArray *strAry = [devStr componentsSeparatedByString:@"|"];
        Device *dev = [[Device alloc] init];
        dev.name = [strAry objectAtIndex:0];
        dev.ip = [strAry objectAtIndex:1];
        dev.port = [[strAry objectAtIndex:2] integerValue];
        dev.type = [[strAry objectAtIndex:3] integerValue];
        dev.streamType = [[strAry objectAtIndex:4] integerValue];
        dev.product = [[strAry objectAtIndex:5] integerValue];
        dev.user = [strAry objectAtIndex:6];
        dev.password = [strAry objectAtIndex:7];
        dev.rtspPort = [[strAry objectAtIndex:8] integerValue];
        dev.dualStream = 1;
        dev.rowID = devList.count;
        dev.group = 0;
        
        [devList addObject:dev];
        [dev release];
    }
}

- (void)addToDeviceList
{
    if (checkList.count == 0) {
        [self showAlarm:NSLocalizedString(@"MsgNoSelect", nil)];
        return;
    }
    
    BOOL ret = NO;
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    for (Device *dev in checkList) {
        
        if (dev.product == DEV_DVR) {
            
            dev.rowID = [Device getNewIDfromArray:appDelegate.dvrs];
            [[appDelegate.dvrs lastObject] addObject:dev];
            ret = [appDelegate refreshDVRsIntoDatabase];
        }else {
            dev.rowID = [Device getNewIDfromArray:appDelegate.ipcams];
            [[appDelegate.ipcams lastObject] addObject:dev];
            ret = [appDelegate refreshIPCamsIntoDatabase];
        }
        if (!ret) {
            [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
            break;
        }
	}
    
    if (ret)
        [self showAlarm:NSLocalizedString(@"MsgImportDone", nil)];
    
    [self closeView];
}

#pragma mark - TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return devList.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 20)] autorelease];
    [headerView setBackgroundColor:[UIColor BACOLOR]];
    UILabel *lbTitle = [[[UILabel alloc] initWithFrame:headerView.frame] autorelease];
    [lbTitle setBackgroundColor:[UIColor clearColor]];
    [lbTitle setTextColor:[UIColor whiteColor]];
    [lbTitle setFont:[UIFont systemFontOfSize:14]];
    [lbTitle setText:[NSString stringWithFormat:@"%@  [%lu/%lu] ",NSLocalizedString(@"DeviceList", nil), (unsigned long)checkList.count, (unsigned long)devList.count]];
    
    [headerView addSubview:lbTitle];
    
    return headerView;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
		cell.showsReorderControl = YES;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		// set background
		cell.textLabel.backgroundColor = [UIColor clearColor];
		cell.detailTextLabel.backgroundColor = [UIColor clearColor];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
        
        UIImageView *checkBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addDev1.png"]];
        [cell setAccessoryView:checkBtn];
        [checkBtn release];
        [cell.accessoryView setHidden:YES];
    }
    
	// Configure the cell.
	Device *dev = [devList objectAtIndex:indexPath.row];
	cell.textLabel.text = dev.name;
    
    BOOL bFind = NO;
    for (Device *tmpDev in checkList)
    {
        if (dev.rowID == tmpDev.rowID) {
            bFind = YES;
            break;
        }
    }
    
    [cell.accessoryView setHidden:!bFind];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // set background
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor lightTextColor];
    cell.detailTextLabel.textColor = [UIColor lightTextColor];
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Device *dev = [devList objectAtIndex:indexPath.row];
    if (checkList.count == 0) {
        
        [checkList addObject:dev];
    }else {
        
        BOOL bFind = NO;
        for (Device *tmpDev in checkList) {
            if ([tmpDev isEqual:dev]) {
                
                [checkList removeObject:dev];
                bFind = YES;
                break;
            }
        }
        
        if (!bFind)
            [checkList addObject:dev];
    }
    
    [devTableView reloadData];
}

#pragma mark - Alarm Delegate

- (void)showAlarm:(NSString *)message
{
    if ( message!=nil ) {
		
		// open an alert with just an OK button, touch "OK" will do nothing
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];[alert show];
	}
}

@end
