//
//  GroupViewController.h
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/3.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFViewerAppDelegate.h"

@interface GroupViewController : UIViewController<UITableViewDataSource,UITableViewDelegate
                                                ,UISearchBarDelegate,UISearchDisplayDelegate
                                                ,UIAlertViewDelegate>
{
    IBOutlet UITableView  *devTableView;
    NSMutableArray        *selectArray;
    
    UISearchDisplayController   *searchDisplay;
    UISearchBar                 *searchBar;
    NSMutableArray              *searchResult;
}

@property(nonatomic,assign) id<contentDelegate> delegate;
@property(nonatomic, retain) DeviceGroup        *group;
@property(nonatomic, assign) NSMutableArray     *devArray;
@property(nonatomic, assign) NSMutableArray     *curGArray;
@property(nonatomic)         BOOL               blnNewGroup;
@property(nonatomic)         NSInteger          devType;

@end
