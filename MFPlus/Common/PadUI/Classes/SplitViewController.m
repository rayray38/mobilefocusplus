//
//  SplitViewController.m
//  EFViewerHD
//
//  Created by James Lee on 12/11/28.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "SplitViewController.h"

@implementation SplitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    maskView = [[UIView alloc] initWithFrame:MAIN_FRAME];
    [maskView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.3]];
    [self.view addSubview:maskView];
    [maskView setHidden:YES];
    
    tipView = [[UIView alloc] initWithFrame:TIPV_FRAME];
    [tipView setBackgroundColor:[UIColor BACOLOR]];
    tipText = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, TIPV_FRAME.size.width, TIPV_FRAME.size.height)];
    [tipText setBackgroundColor:[UIColor clearColor]];
    [tipText setTextColor:[UIColor whiteColor]];
    [tipText setTextAlignment:NSTextAlignmentCenter];
    [tipText setFont:[UIFont boldSystemFontOfSize:18]];
    [tipView addSubview:tipText];
    
    [self.view addSubview:tipView];
    [tipView setAlpha:0.0f];
}

- (void)dealloc
{
    SAVE_FREE(maskView);
    SAVE_FREE(tipText);
    SAVE_FREE(tipView);
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Tips

- (void)showTips:(NSString *)message with:(CGFloat)duration
{
    CGFloat detailVC_width = [[[[self.viewControllers objectAtIndex:1] viewControllers] objectAtIndex:0] view].frame.size.width - 1; //weird issue, sometimes it will be 743.5
    CGRect tipFrame = (detailVC_width > DETAILVIEW_WIDTH) ? TIPV_FRAMEW : TIPV_FRAME;
    [tipView setFrame:tipFrame];
    [tipText setFrame:CGRectMake(0, 0, tipFrame.size.width, tipFrame.size.height)];
    
    [tipText setText:message];
    [UIView animateWithDuration:0.5f animations:^{
                            [tipView setAlpha:1.0];
    }];
    
    [self performSelector:@selector(closeTips) withObject:nil afterDelay:duration];
}

- (void)closeTips
{
    [UIView animateWithDuration:0.5f animations:^{
        [tipView setAlpha:0.0];
    }];
}

#pragma mark - PopView Delegate

- (void)showPopViewWith:(UIViewController *)viewCtrl
{
    SAVE_FREE(popView);
    
    [maskView setHidden:NO];
    popView = [[UIPopoverController alloc] initWithContentViewController:viewCtrl];
    [popView setDelegate:self];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) { //add custom border
        [[popView contentViewController].view.layer setBorderColor:[UIColor whiteColor].CGColor];
        [[popView contentViewController].view.layer setCornerRadius:15.0];
        [[popView contentViewController].view.layer setBorderWidth:4.0];
    }
    
    [popView presentPopoverFromRect:POPVIEW_FRAME
                                  inView:maskView
                permittedArrowDirections:0 //no arraw
                                animated:YES];
}

- (void)showPopAlarmWith:(UIViewController *)viewCtrl
{
    SAVE_FREE(popView);
    
    CGFloat detailVC_width = [[[[self.viewControllers objectAtIndex:1] viewControllers] objectAtIndex:0] view].frame.size.width - 1;
    CGRect popFrame = (detailVC_width > DETAILVIEW_WIDTH) ? POPALARM_FRAMEW : POPALARM_FRAME;
    CGRect maskFrame = (detailVC_width > DETAILVIEW_WIDTH) ? MAIN_FRAMEW : MAIN_FRAME;
    
    [maskView setFrame:maskFrame];
    [maskView setHidden:NO];
    popView = [[UIPopoverController alloc] initWithContentViewController:viewCtrl];
    [popView setDelegate:self];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) { //add custom border
        [[popView contentViewController].view.layer setBorderColor:[UIColor whiteColor].CGColor];
        [[popView contentViewController].view.layer setCornerRadius:15.0];
        [[popView contentViewController].view.layer setBorderWidth:4.0];
    }
    
    [popView presentPopoverFromRect:popFrame
                             inView:maskView
           permittedArrowDirections:0 //no arraw
                           animated:YES];
}

- (void)closePopView
{
    [popView dismissPopoverAnimated:YES];
    [maskView setHidden:YES];
    
    SAVE_FREE(popView);
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    [maskView setHidden:YES];
    
    SAVE_FREE(popView);
}

@end
