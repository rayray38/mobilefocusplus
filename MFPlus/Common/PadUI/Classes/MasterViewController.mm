//
//  MasterViewController.m
//  EFViewerHD
//
//  Created by James Lee on 12/10/5.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "MasterViewController.h"
#import "DeviceViewController.h"
#import "GroupViewController.h"
#import "HelpViewController.h"
#import "ScanViewController.h"
#import "ExportViewController.h"
#import "ExTableHeader.h"

@interface MasterViewController()
{
    UIImageView *imageView;
}

- (void)openAddMenu;
- (void)openGroupHeader:(id)sender;
- (void)deleteGroup:(id)sender;
- (void)editGroup:(id)sender;
- (void)addDevice;
- (void)addGroup;
- (void)removeGroup:(NSInteger)groupID;
- (void)setEditing:(BOOL)editing animated:(BOOL)animated;
- (void)showAlarm:(NSString *)message;

- (void)openScanMenu;
- (void)openScanView:(NSInteger)mode;

- (void)showUpdateMessage;

- (void)handleLongPressOnNavBarLogo:(UILongPressGestureRecognizer *)gesture;
- (BOOL)checkSecureText:(NSString *)secuString;

@end

@implementation MasterViewController

@synthesize channelView,ptzView,ipView,xmsView;

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createNSNotificationCenter];
	// Do any additional setup after loading the view, typically from a nib.
    btnAdd= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(openAddMenu)];

    // set title
    self.title = NSLocalizedString(@"DeviceList", nil);
    self.navigationItem.title = nil;
    
    // set navigation image
    UIImage *image = [UIImage imageNamed: @"NavBarLogo.png"];
    imageView = [[UIImageView alloc] initWithImage: image];
    imageView.userInteractionEnabled = YES;
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressOnNavBarLogo:)];
    [longPressRecognizer setDelegate:self];
    [longPressRecognizer setMinimumPressDuration:10.0];
    [imageView addGestureRecognizer:longPressRecognizer];
    SAVE_FREE(longPressRecognizer);
    self.navigationItem.titleView = imageView;
    //[imageView release];
    
    // set button
	self.navigationItem.leftBarButtonItem = self.editButtonItem;
	self.navigationItem.rightBarButtonItem = btnAdd;
    
    //Get group Array
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    groupArray = appDelegate.groups;
    groupFilterArray = [groupArray objectAtIndex:devListFilter];
    
    // Initial dbControl
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:SQL_FILE_NAME];
    dbControl = [[DBControl alloc] initByPath:path];
    
    //Initial search bar
    searchBar = [[UISearchBar alloc] init];
    searchBar.delegate = self;
    searchBar.placeholder = NSLocalizedString(@"SearchDevice", nil);
    [searchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [searchBar sizeToFit];
    devTableView.tableHeaderView = searchBar;
    devTableView.tableHeaderView.backgroundColor = [UIColor BGCOLOR];
    
    searchDisplay = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    searchDisplay.delegate = self;
    searchDisplay.searchResultsDataSource = self;
    searchDisplay.searchResultsDelegate = self;
    
    [devTableView registerNib:[UINib nibWithNibName:@"ExTableHeader" bundle:nil] forHeaderFooterViewReuseIdentifier:@"ExTableHeader"];
    
    if (appDelegate.os_version >= 7.0) {
        [searchBar setBarTintColor:[UIColor blackColor]];
    }else {
        [searchBar setBarStyle:UIBarStyleBlackTranslucent];
    }
    
    searchResult = [[NSMutableArray alloc] init];
    
    [self toolBarItemSelect:dvrBtn];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    DetailViewController *detailView = (DetailViewController *)[[[[appDelegate.splitViewController viewControllers] objectAtIndex:1] viewControllers] firstObject];
    detailView.delegate = self;
    
    groupFilterArray = [groupArray objectAtIndex:devListFilter];
    
    // move searchbar under the navBar
    CGRect newBounds = devTableView.bounds;
    if (devTableView.bounds.origin.y < TOOLBAR_HEIGHT) {
        newBounds.origin.y += searchBar.bounds.size.height;
        devTableView.bounds = newBounds;
    }
    
    [devTableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self showUpdateMessage];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.os_version < 7.0 && searchDisplay.isActive)
        [searchDisplay setActive:NO animated:YES];
}

- (void)dealloc
{
    MMLog(@"[MV] dealloc");
    
    [btnAdd release];
    
    [self.channelView release];
    [self.ptzView release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == (UIInterfaceOrientationLandscapeRight|UIInterfaceOrientationLandscapeLeft));
}

#pragma mark - Action

- (void)toolBarItemSelect:(id)sender
{
    NSInteger selectItem = ((UIButton *)sender).tag;
	
    devListFilter = selectItem;
	EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
	if (devListFilter == DEV_DVR ) {
        // set button
        self.navigationItem.leftBarButtonItem = self.editButtonItem;
        UIBarButtonItem *addBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(openAddMenu)];
        self.navigationItem.rightBarButtonItem = addBtn;
        [addBtn release];
		
        devArray = appDelegate.dvrs;
        blnEventMode = self.xmsView.blnEventMode = NO;
        [dvrBtn setImage:IMG_DVROVER forState:UIControlStateNormal];
        [ipBtn setImage:IMG_IPCAM forState:UIControlStateNormal];
        [eventBtn setImage:IMG_EVENT forState:UIControlStateNormal];
        [setBtn setImage:IMG_HELP forState:UIControlStateNormal];
        groupFilterArray = [groupArray objectAtIndex:devListFilter];
    }
    else if (devListFilter == DEV_IPCAM) {
        // set button
        self.navigationItem.leftBarButtonItem = self.editButtonItem;
        UIBarButtonItem *addBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(openAddMenu)];
        self.navigationItem.rightBarButtonItem = addBtn;
        [addBtn release];
        
        devArray = appDelegate.ipcams;
        blnEventMode = self.xmsView.blnEventMode = NO;
        [dvrBtn setImage:IMG_DVR forState:UIControlStateNormal];
        [ipBtn setImage:IMG_IPCAMOVER forState:UIControlStateNormal];
        [eventBtn setImage:IMG_EVENT forState:UIControlStateNormal];
        [setBtn setImage:IMG_HELP forState:UIControlStateNormal];
        groupFilterArray = [groupArray objectAtIndex:devListFilter];
    }
    else if (devListFilter == EVENT) {
        // set button
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = nil;
        
        NSMutableArray *xmsAry = [NSMutableArray new];
        for (NSMutableArray *tmpAry1 in appDelegate.dvrs) {
            for (Device *tmpDvrs in tmpAry1) {
                if (tmpDvrs.type == XMS_SERIES) {
                    [xmsAry addObject:tmpDvrs];
                }
            }
        }
        devArray = [[NSMutableArray arrayWithObject:xmsAry] retain];
        blnEventMode = YES;
        [dvrBtn setImage:IMG_DVR forState:UIControlStateNormal];
        [ipBtn setImage:IMG_IPCAM forState:UIControlStateNormal];
        [eventBtn setImage:IMG_EVENTOVER forState:UIControlStateNormal];
        [setBtn setImage:IMG_HELP forState:UIControlStateNormal];
    }
    else {
        
        blnEventMode = NO;
        [self.navigationItem setRightBarButtonItem:nil];
        [self.navigationItem setLeftBarButtonItem:nil];
        [dvrBtn setImage:IMG_DVR forState:UIControlStateNormal];
        [ipBtn setImage:IMG_IPCAM forState:UIControlStateNormal];
        [eventBtn setImage:IMG_EVENT forState:UIControlStateNormal];
        [setBtn setImage:IMG_HELPOVER forState:UIControlStateNormal];
    }
    [searchBar setHidden:(selectItem==SETTING)];
	[devTableView reloadData];
}

- (void)openAddMenu
{
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.os_version >= 8.0) { //iOS8 only
        
        UIAlertController *addMenu = [UIAlertController alertControllerWithTitle:@"MobileFocus HD"
                                                                         message:nil
                                                                  preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actAddDev = [UIAlertAction actionWithTitle:NSLocalizedString(@"AddDevice", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              [self addDevice];
                                                          }];
        
        UIAlertAction *actAddGroup = [UIAlertAction actionWithTitle:NSLocalizedString(@"AddGroup", nil)
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction *action) {
                                                                [self addGroup];
                                                            }];
        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"BtnCancel", nil)
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction *action) {}];
        [addMenu addAction:actAddDev];
        [addMenu addAction:actAddGroup];
        [addMenu addAction:actCancel];
        //[addMenu.view setTintColor:[UIColor darkTextColor]];
        
        [self presentViewController:addMenu animated:YES completion:^{}];
        
    } else {
        
        UIActionSheet *addMenu = [[[UIActionSheet alloc] initWithTitle:nil
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                destructiveButtonTitle:nil
                                                     otherButtonTitles:NSLocalizedString(@"AddDevice", nil),NSLocalizedString(@"AddGroup", nil),nil]
                                  autorelease];
        addMenu.tag = 1;
        [addMenu showInView:self.view];
    }
}

- (void)openGroupHeader:(id)sender
{
    UIButton *bgBtn = (UIButton *)sender;
    NSInteger group_id = bgBtn.tag;
    
    ExTableHeader *headerView = nil;
    for(NSInteger i=0; i<devTableView.numberOfSections-1; i++)
    {
        ExTableHeader *tmpHeader = (ExTableHeader *)[devTableView headerViewForSection:i];
        if ([tmpHeader getGroupID] == group_id) {
            headerView = tmpHeader;
            break;
        }
    }
    
    DeviceGroup *curGroup = [DeviceGroup getGroupByID:group_id from:groupFilterArray];
    curGroup.expand = !curGroup.expand;
    
    [devTableView reloadData];
}

- (void)deleteGroup:(id)sender
{
    UIButton *deleteBtn = (UIButton *)sender;
    NSInteger group_id = deleteBtn.tag;
    
    [self removeGroup:group_id];
    
    [devTableView reloadData];
}

- (void)editGroup:(id)sender
{
    UIButton *detailBtn = (UIButton *)sender;
    NSInteger group_id = detailBtn.tag;
                                                             
    GroupViewController *groupView = [[GroupViewController alloc] initWithNibName:@"GroupViewController"
                                                                           bundle:[NSBundle mainBundle]];
    groupView.delegate = self;
    groupView.blnNewGroup = YES;
    
    DeviceGroup *curGroup = [DeviceGroup getGroupByID:group_id from:groupFilterArray];
    groupView.group = curGroup;
    groupView.curGArray = [DeviceGroup getArrayByGroupID:group_id from:devArray];
    groupView.devArray = [devArray lastObject];
    //MMLog(@"[MV] devArray last %@ rCount:%d",[devArray lastObject],[[devArray lastObject] retainCount]);
    groupView.devType = devListFilter;
    groupView.blnNewGroup = NO;
    
    UINavigationController *navGroupView = [[UINavigationController alloc] initWithRootViewController:groupView];
    [groupView release];
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.splitViewController showPopViewWith:navGroupView];
    [navGroupView release];
}

- (void)openScanMenu
{
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.os_version >= 8.0) { //iOS8 only
        
        UIAlertController *scanMenu = [UIAlertController alertControllerWithTitle:nil
                                                                         message:nil
                                                                  preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actScan = [UIAlertAction actionWithTitle:NSLocalizedString(@"BtnScanQRCode", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              [self openScanView:0];
                                                          }];
        
        UIAlertAction *actLoad = [UIAlertAction actionWithTitle:NSLocalizedString(@"BtnLoadQRCode", nil)
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction *action) {
                                                                [self openScanView:1];
                                                            }];
        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"BtnCancel", nil)
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction *action) {
                                                                [devTableView reloadData];
                                                          }];
        [scanMenu addAction:actScan];
        [scanMenu addAction:actLoad];
        [scanMenu addAction:actCancel];
        [scanMenu.view setTintColor:[UIColor darkTextColor]];
        
        [self presentViewController:scanMenu animated:YES completion:^{}];
    } else {
        
        UIActionSheet *scanMenu = [[[UIActionSheet alloc] initWithTitle:nil
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                destructiveButtonTitle:nil
                                                     otherButtonTitles:NSLocalizedString(@"BtnScanQRCode", nil),NSLocalizedString(@"BtnLoadQRCode", nil),nil]
                                  autorelease];
        scanMenu.tag = 2;
        [scanMenu showInView:self.view];
    }
}

- (void)openScanView:(NSInteger)mode
{
    ScanViewController *scanView = [[ScanViewController alloc] initWithNibName:@"ScanViewController"
                                                                            bundle:[NSBundle mainBundle]];
    scanView.delegate = self;
    scanView.mode = mode;
    
    UINavigationController *navScanView = [[UINavigationController alloc] initWithRootViewController:scanView];
    [scanView release];
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.splitViewController showPopViewWith:navScanView];
    [navScanView release];
}

- (void)openHelpMenu
{
    if ([UIAlertController class]) {    //iOS 8 and above support UIAlertController
        
        UIAlertController *notifyMenu = [UIAlertController alertControllerWithTitle:nil
                                                                            message:MsgUserNotify
                                                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actOK = [UIAlertAction actionWithTitle:NSLocalizedString(@"BtnOK", nil)
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [self openHelpView];
                                                      }];
        
        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"BtnCancel", nil)
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction *action) {
                                                              [devTableView reloadData];
                                                          }];
        [notifyMenu addAction:actOK];
        [notifyMenu addAction:actCancel];
        [self presentViewController:notifyMenu animated:YES completion:nil];
        [notifyMenu.view setTintColor:[UIColor darkTextColor]];
    }
    else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus HD"
                                                        message:MsgUserNotify
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"BtnCancel", nil)
                                              otherButtonTitles:NSLocalizedString(@"BtnOK", nil), nil];
        
        alert.tag = 0;
        
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
    }
}

- (void)openHelpView
{
    HelpViewController *helpView = [[HelpViewController alloc] initWithNibName:@"HelpViewController"
                                                                        bundle:[NSBundle mainBundle]];
    helpView.delegate = self;
    helpView.uType = 0;
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.splitViewController showPopViewWith:helpView];
    [helpView release];
}

- (void)showUpdateMessage
{
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults  *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *checker = [userDefault valueForKey:appDelegate.version];
    if (!checker) {
        HelpViewController *helpView = [[HelpViewController alloc] initWithNibName:@"HelpViewController"
                                                                            bundle:[NSBundle mainBundle]];
        helpView.delegate = self;
        helpView.uType = 1;
        
        [appDelegate.splitViewController showPopViewWith:helpView];
        [helpView release];
        
        [userDefault setValue:@"READ" forKey:appDelegate.version];
        [userDefault synchronize];
    }
}

- (BOOL)checkSecureText:(NSString *)secuString
{
    BOOL ret = NO;
    
    NSString *answer = [NSString stringWithFormat:@"26982334"];
    
    if ([secuString isEqual:answer]) {
        ret = YES;
    }
    else
        ret = NO;
    
    return ret;
}

- (void)createNSNotificationCenter
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getNotification:)
                                                 name:@"DidReceivedRemoteNotify"
                                               object:nil];
}

- (void)getNotification:(NSNotification *)notification
{
    //處理收到 NSNotification 後要做的動作: 讓有Event發生的Device改為顯示紅色字體
    NSDictionary *userInfo = [notification userInfo]; //讀取userInfo
    NSDictionary *dataInfo = userInfo[@"data"];
    NSDictionary *xmsInfo = dataInfo[@"xmsInfo"];
    NSString *xmsUUID = [xmsInfo[@"uuid"] copy];
    RELog(@"[MV] 處理 locale 改變的狀況 : %@",xmsUUID);
    for (Device *dev in [devArray firstObject]) {
        if ([dev.uuid isEqualToString:xmsUUID]) {
            dev.blnEvNotify = YES;
        }
    }
    [devTableView reloadData];
}

- (void)openEventNotifyListView:(Device *)dev
{
    if (self.notifyView == nil) {
        EventNotifyListVC *viewController = [[EventNotifyListVC alloc] initWithNibName:@"EventNotifyListVC" bundle:[NSBundle mainBundle]];
        self.notifyView = viewController;
        [viewController release];
    }
    
    dev.blnEvNotify = NO;
    self.notifyView.m_Device = dev;
    [self.navigationController pushViewController:self.notifyView animated:YES];
}

#pragma mark - Device/Group management

- (void)addDevice
{
    DeviceViewController *deviceView = [[DeviceViewController alloc] initWithNibName:@"DeviceViewController"
                                                                              bundle:[NSBundle mainBundle]];
    deviceView.delegate = self;
    deviceView.blnNewDevice = YES;
    Device *newDevice = [[Device alloc] initWithProduct:devListFilter];
    newDevice.rowID = [Device getNewIDfromArray:devArray];
    deviceView.deviceCopy = newDevice;
    [newDevice release];
    
    UINavigationController *navDeviceView = [[UINavigationController alloc] initWithRootViewController:deviceView];
    [deviceView release];
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.splitViewController showPopViewWith:navDeviceView];
    [navDeviceView release];
}

- (void)addGroup
{
    GroupViewController *groupView = [[GroupViewController alloc] initWithNibName:@"GroupViewController"
                                                                           bundle:[NSBundle mainBundle]];
    groupView.delegate = self;
    groupView.blnNewGroup = YES;
    
    DeviceGroup *newGroup = [[DeviceGroup alloc] initWithProduct:devListFilter];
    newGroup.group_id = [DeviceGroup getNewIDFromArray:groupArray];
    newGroup.type = devListFilter;
    groupView.group = newGroup;
    [newGroup release];
    
    groupView.devArray = [devArray lastObject];
    groupView.devType = devListFilter;
    
    UINavigationController *navGroupView = [[UINavigationController alloc] initWithRootViewController:groupView];
    [groupView release];
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.splitViewController showPopViewWith:navGroupView];
    [navGroupView release];
}

- (void)removeGroup:(NSInteger)groupID
{
    DeviceGroup *group = [DeviceGroup getGroupByID:groupID from:groupFilterArray];
    
    // change all device in the group into unclassified
    NSMutableArray *devGroup = [DeviceGroup getArrayByGroupID:group.group_id from:devArray];
    while(devGroup.count > 0)
    {
        Device *dev = [devGroup lastObject];
        NSLog(@"[MV] Dev:%@ change group from %ld to 0",dev.name,(long)dev.group);
        dev.group = 0;
        [[devArray lastObject] addObject:dev];
        [devGroup removeObject:dev];
    }
    
    // delete the array
    [devArray removeObject:devGroup];
    
    // delete the group
    NSLog(@"[MV] Group[%ld]:%@ is empty - Remove",(long)group.group_id,group.name);
    [groupFilterArray removeObject:group];
    
    // save it to database
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    BOOL ret = YES;
    ret |= (devListFilter==DEV_DVR ? [appDelegate refreshDVRsIntoDatabase] : [appDelegate refreshIPCamsIntoDatabase]);
    ret |= [appDelegate refreshGroupIntoDatabase];
    if (!ret)
        [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
}

#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger section = 1;
    
    if (devListFilter == SETTING)
        section = 3;
    else if (tableView != searchDisplay.searchResultsTableView) {
        if (!blnEventMode) {
            section = devArray.count;
        }
    }
    
    return section;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == searchDisplay.searchResultsTableView || section == devArray.count-1 || devListFilter == SETTING)
    {
        UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 20)] autorelease];
        [headerView setBackgroundColor:[UIColor BACOLOR]];
        UILabel *lbTitle = [[[UILabel alloc] initWithFrame:headerView.frame] autorelease];
        [lbTitle setBackgroundColor:[UIColor clearColor]];
        [lbTitle setTextColor:[UIColor whiteColor]];
        [lbTitle setFont:[UIFont systemFontOfSize:14]];
        
        if (devListFilter == SETTING) {
            NSString *title = @"";
            switch (section) {
                case 0:
                    title = NSLocalizedString(@"Help", nil);
                    break;
                case 1:
                    title = NSLocalizedString(@"DeviceIO", nil);
                    break;
                case 2:
                    title = NSLocalizedString(@"Configuration", nil);
                    break;
            }
            [lbTitle setText:title];
        } else if (tableView == searchDisplay.searchResultsTableView) {
            [lbTitle setText:[NSString stringWithFormat:@"%@  [%lu] ",NSLocalizedString(@"SearchResults", nil),(unsigned long)searchResult.count]];
        } else {
            [lbTitle setText:[NSString stringWithFormat:@"%@  [%lu] ",NSLocalizedString(@"Unclassified", nil),(unsigned long)[(NSArray *)[devArray lastObject] count]]];
        }
        [headerView addSubview:lbTitle];
        
        return headerView;
    } else {
        
        static NSString *headerIdentifier = @"ExTableHeader";
        ExTableHeader *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerIdentifier];
        [headerView setSize:CGSizeMake(MASTERVIEW_WIDTH, 60)];   //Pad version resize
        [headerView.bgButton addTarget:self action:@selector(openGroupHeader:) forControlEvents:UIControlEventTouchUpInside];
        [headerView.detailBtn addTarget:self action:@selector(editGroup:) forControlEvents:UIControlEventTouchUpInside];
        [headerView.deleteBtn addTarget:self action:@selector(deleteGroup:) forControlEvents:UIControlEventTouchUpInside];
        
        Device *dev = [[devArray objectAtIndex:section] firstObject];
        DeviceGroup *curGroup = [DeviceGroup getGroupByID:dev.group from:groupFilterArray];
        [headerView setGroupID:curGroup.group_id];
        [headerView setTitle:curGroup.name];
        [headerView.badgeBtn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[(NSArray *)[devArray objectAtIndex:section] count]] forState:UIControlStateNormal];
        [headerView setEditing:devTableView.isEditing];
        
        return headerView;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 60.0;
    
    if (section == devArray.count-1 || tableView == searchDisplay.searchResultsTableView || devListFilter == SETTING)
        height = 20;
    
    return height;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger rows = 0;
	
    if (tableView == searchDisplay.searchResultsTableView) {
        
        rows = searchResult.count;
    }else {
        
        if (devListFilter == SETTING) {
            
            switch (section) {
                case 0: //Help UserManual/Release Note
                case 1: //Device Import/Export
                case 2: //Smooth Mode/Allow PopUP
                    rows = 2;
                    break;
            }
        }else {
            if (devArray.count == 0) {
                return rows;
            }
            NSMutableArray *subArray = [devArray objectAtIndex:section];
            rows = subArray.count;
            Device *dev = [subArray firstObject];
            if (!blnEventMode && dev.group > 0) {
                DeviceGroup *curGroup = [DeviceGroup getGroupByID:dev.group from:groupFilterArray];
                if (!curGroup.expand)
                    rows = 0;
            }
        }
	}
    
    return rows;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
		cell.showsReorderControl = YES;
		cell.selectionStyle = UITableViewCellSelectionStyleGray;
		
		// set background
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
		cell.detailTextLabel.backgroundColor = [UIColor clearColor];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
    }
    
	// Configure the cell.
    if (devListFilter == SETTING) {
        
        cell.detailTextLabel.text = nil;

        EZAccessoryView *ezView = [[EZAccessoryView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [ezView.detailButton addTarget:self action:@selector(accessoryButtonTappedForRowWithIndexPath:) forControlEvents:UIControlEventTouchDown];
        [ezView.detailSwitch addTarget:ezView action:@selector(changeDetailSetting:) forControlEvents:UIControlEventValueChanged];
        ezView.delegate = self;
        
        switch (indexPath.section) {
            case 0: //Help
                cell.textLabel.text = indexPath.row == 0 ? NSLocalizedString(@"UserManual", nil) : NSLocalizedString(@"ReleaseNote", nil);
                [ezView setDetailType:edt_none];
                break;
            case 1: //Device Import/Export
                cell.textLabel.text = indexPath.row == 0 ? NSLocalizedString(@"DeviceImport", nil) : NSLocalizedString(@"DeviceExport", nil);
                [ezView setDetailType:edt_none];
                break;
            case 2: //Alarm PopUp/Smooth Video
                if (indexPath.row == 0) {
                    cell.textLabel.text = NSLocalizedString(@"AlarmFocus", nil);
                    [ezView setDetailType:edt_switch];
                    [ezView.detailSwitch setOn:[EZAccessoryView getDetailSettingBy:KEY_ALARMPOP]];
                    [ezView.detailSwitch setTag:edm_alarmPop];
                } else if (indexPath.row == 1) {
                    cell.textLabel.text = NSLocalizedString(@"SmoothMode", nil);
                    [ezView setDetailType:edt_switch];
                    [ezView.detailSwitch setOn:[EZAccessoryView getDetailSettingBy:KEY_SMOOTHMODE]];
                    [ezView.detailSwitch setTag:edm_smoothmode];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                break;
        }
        
        [cell setAccessoryView:ezView];
        [ezView release];
        
    }else {
        Device *dev;
        
        if (tableView == searchDisplay.searchResultsTableView)
            dev = [searchResult objectAtIndex:indexPath.row];
        else
        {
            NSMutableArray *subArray = [devArray objectAtIndex:indexPath.section];
            dev = [subArray objectAtIndex:indexPath.row];
        }
        
        cell.textLabel.text = dev.name;
        if (blnEventMode) {
            [cell.textLabel setTextColor:(dev.blnEvNotify == YES ? [UIColor redColor] : [UIColor lightTextColor])];
        }
        else
            cell.textLabel.textColor = [UIColor lightTextColor];
        
        cell.detailTextLabel.text = nil;
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        EZAccessoryView *ezView = [[EZAccessoryView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [ezView.detailButton addTarget:self action:@selector(accessoryButtonTappedForRowWithIndexPath:) forControlEvents:UIControlEventTouchDown];
        ezView.delegate = self;
        [ezView.detailButton setTag:dev.rowID];
        [ezView setDetailType:edt_button];
        
        [cell setAccessoryView:ezView];
        [ezView release];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger height = 0;
    height = 60;
    
    return height;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (blnEventMode) {
        return;
    }
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    Device *dev = (tableView == searchDisplay.searchResultsTableView ? [searchResult objectAtIndex:indexPath.row] : [[devArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]);
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if (tableView == searchDisplay.searchResultsTableView)
            [searchResult removeObject:dev];
        
        [tableView beginUpdates];
        NSInteger target_group = dev.group;
        NSMutableArray *targetArray = [DeviceGroup getArrayByGroupID:target_group from:devArray];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        NSLog(@"[MV] Delete Dev[%ld]:%@",(long)dev.rowID,dev.name);
        
        //remove deviceToken from server
        if (dev.type == XMS_SERIES) {
            streamReceiver = [[[StreamReceiver alloc] initWithDevice:dev] autorelease];
            streamReceiver.blnCloseNotify = YES;
            NSArray *ary = [NSArray arrayWithObjects:appDelegate._DeviceToken, dev.name, nil];
            [streamReceiver performSelectorInBackground:@selector(registDevTokenToServer:) withObject:ary];
        }
        
        [targetArray removeObject:dev];
        
        if (targetArray.count == 0 & target_group > 0) {
            DeviceGroup *empty_group = [DeviceGroup getGroupByID:target_group from:groupFilterArray];
            NSLog(@"[MV] Group[%ld]:%@ is empty - Remove",(long)empty_group.group_id,empty_group.name);
            [groupFilterArray removeObject:empty_group];
            [devArray removeObjectAtIndex:[devArray indexOfObject:targetArray]];
            [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
        
        [tableView endUpdates];
        
        // save it to database
        BOOL ret = (devListFilter==DEV_DVR ? [appDelegate refreshDVRsIntoDatabase] : [appDelegate refreshIPCamsIntoDatabase]);
        if (!ret)
            [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
    }
    [tableView reloadData];
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
	Device *dev = [[[devArray objectAtIndex:fromIndexPath.section] objectAtIndex:fromIndexPath.row] retain];
    
    NSMutableArray *_lastArray = [devArray objectAtIndex:fromIndexPath.section];
    NSMutableArray *_newArray = [devArray objectAtIndex:toIndexPath.section];
    
    //get target array's group_id
    NSInteger new_id = (toIndexPath.section == devArray.count-1 ? 0 : ((Device *)[_newArray firstObject]).group);
	
    //move the device
    [_lastArray removeObjectAtIndex:fromIndexPath.row];
	[_newArray insertObject:dev atIndex:toIndexPath.row];
    
    //check if last section is empty
    if (_lastArray.count == 0 && fromIndexPath.section < devArray.count-1) {
        
        NSInteger last_id = dev.group;
        DeviceGroup *empty_group = [DeviceGroup getGroupByID:last_id from:groupFilterArray];
        NSLog(@"[MV] Group[%ld]:%@ is empty - Remove",(long)empty_group.group_id,empty_group.name);
        [groupFilterArray removeObject:empty_group];
        [devArray removeObject:_lastArray];
    }
 	
    //replace device's group_id
    NSLog(@"[MV] Dev:%@ change group from %ld to %ld",dev.name,(long)dev.group,(long)new_id);
    dev.group = new_id;
    [dev release];
    
    [tableView reloadData];
}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return blnEventMode == YES ? NO : YES;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return devListFilter == SETTING ? NO : YES;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // set background
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor lightTextColor];
    cell.detailTextLabel.textColor = [UIColor lightTextColor];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!blnEventMode) {
        if (devListFilter == SETTING) {
            
            if (indexPath.section == 0) {
                
                if (indexPath.row == 0) {   //Help
                    [self openHelpMenu];
                }
                else {
                    HelpViewController *helpView = [[HelpViewController alloc] initWithNibName:@"HelpViewController"
                                                                                        bundle:[NSBundle mainBundle]];
                    helpView.delegate = self;
                    helpView.uType = indexPath.row;
                    
                    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
                    [appDelegate.splitViewController showPopViewWith:helpView];
                    [helpView release];
                }
                
            }else if (indexPath.section == 1) {     // Device Import / Export
                
                if (indexPath.row == 0)  //Import
                    [self openScanMenu];
                else {                   //Export
                    
                    ExportViewController *exportView = [[ExportViewController alloc] initWithNibName:@"ExportViewController"
                                                                                              bundle:[NSBundle mainBundle]];
                    exportView.delegate = self;
                    UINavigationController *navExportView = [[UINavigationController alloc] initWithRootViewController:exportView];
                    [exportView release];
                    
                    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
                    [appDelegate.splitViewController showPopViewWith:navExportView];
                    [navExportView release];
                }
            }
        } else {
            
            //Set SmoothMode first
            EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
            UINavigationController *navView = [appDelegate.splitViewController.viewControllers objectAtIndex:1];
            DetailViewController *detailView = [navView.viewControllers  objectAtIndex:0];
            [detailView setSmoothMode:[EZAccessoryView getDetailSettingBy:KEY_SMOOTHMODE]];
            
            Device *dev;
            if (tableView == searchDisplay.searchResultsTableView)
                dev = [searchResult objectAtIndex:indexPath.row];
            else
                dev = [[devArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            
            if (devListFilter == DEV_DVR) {
                
                if (dev.streamType == STREAM_XMS) {
                    if (self.xmsView == nil) {
                        XMSViewController *viewController = [[XMSViewController alloc] initWithNibName:@"ChannelViewController"
                                                                                                bundle:[NSBundle mainBundle]];
                        self.xmsView = viewController;
                        [viewController release];
                    }
                    
                    self.xmsView.device = dev;
                    self.xmsView.ptzView = self.ptzView;
                    self.xmsView.blnWillOpenSearch = NO;
                    [self.navigationController pushViewController:self.xmsView animated:YES];
                }
                else {
                    if (self.channelView == nil) {
                        ChannelViewController *viewController = [[ChannelViewController alloc] initWithNibName:@"ChannelViewController"
                                                                                                        bundle:[NSBundle mainBundle]];
                        self.channelView = viewController;
                        [viewController release];
                    }
                    
                    self.channelView.device = dev;
                    self.channelView.ptzView = self.ptzView;
                    self.channelView.blnWillOpenSearch = NO;
                    [self.navigationController pushViewController:self.channelView animated:YES];
                }
            }
            else {
                
                if (self.ipView == nil) {
                    IPViewController *viewController = [[IPViewController alloc] initWithNibName:@"ChannelViewController"
                                                                                          bundle:[NSBundle mainBundle]];
                    self.ipView = viewController;
                    [viewController release];
                }
                self.ipView.device = dev;
                [self.ipView setDevArray:[DeviceGroup getArrayByGroupID:dev.group from:devArray]];
                self.ipView.ptzView = self.ptzView;
                
                [self.navigationController pushViewController:self.ipView animated:YES];
            }
        }
    }
    else {
        
        Device *dev;
        if (tableView == searchDisplay.searchResultsTableView)
            dev = [searchResult objectAtIndex:indexPath.row];
        else
            dev = [[devArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        [self openEventNotifyListView:dev];
    }
}

- (void)accessoryButtonTappedForRowWithIndexPath:(id)sender
{
	// called when the accessory view (disclosure button) is touched
    NSInteger index = ((UIButton *)sender).tag;
    Device *dev = [Device getDeviceByID:index from:devArray];
    
    if (!blnEventMode) {
        
        DeviceViewController *deviceView = [[DeviceViewController alloc] initWithNibName:@"DeviceViewController"
                                                                                  bundle:[NSBundle mainBundle]];
        deviceView.delegate = self;
        deviceView.blnNewDevice = NO;
        deviceView.device = dev;
        
        Device *devCopy = [[Device alloc] initWithDevice:dev];
        deviceView.deviceCopy = devCopy;
        [devCopy release];
        
        UINavigationController *navDeviceView = [[UINavigationController alloc] initWithRootViewController:deviceView];
        [deviceView release];
        
        EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.splitViewController showPopViewWith:navDeviceView];
        [navDeviceView release];
    }
    else
        [self openEventNotifyListView:dev];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[super setEditing:editing animated:animated];
	[devTableView setEditing:editing animated:animated];
    for (NSInteger i=0; i<devTableView.numberOfSections-1; i++)
        [((ExTableHeader*)[devTableView headerViewForSection:i]) setEditing:editing];
    
    [[self.navigationItem rightBarButtonItem] setEnabled:!editing];
	
	if ( !editing ) {
        
        // save it to database
		EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
		BOOL ret = YES;
        ret |= (devListFilter==DEV_DVR ? [appDelegate refreshDVRsIntoDatabase] : [appDelegate refreshIPCamsIntoDatabase]);
        ret |= [appDelegate refreshGroupIntoDatabase];
        if (!ret)
            [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
        
        [devTableView reloadData];
	}
}

#pragma mark - UISearhDisplay Delegate

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    tableView.backgroundColor = [UIColor BGCOLOR];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                                         objectAtIndex:[self.searchDisplayController.searchBar
                                                                        selectedScopeButtonIndex]]];
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willHideSearchResultsTableView:(UITableView *)tableView
{
    [devTableView reloadData];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller  shouldReloadTableForSearchScope:(NSInteger)searchOption {
    
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text]
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:searchOption]];
    
    return YES;
    
}

-(void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    searchBar.showsCancelButton = YES;
    if (appDelegate.os_version >= 7.0) {
        
        UIBarButtonItem *cancelButton;
        UIView *topView = searchBar.subviews[0];
        for (UIView *subView in topView.subviews) {
            if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
                cancelButton = (UIBarButtonItem*)subView;
            }
        }
        
        if (cancelButton)
            [cancelButton setTintColor:[UIColor lightTextColor]];
    }else {
        
        CGRect frame = devTableView.frame;
        frame.origin.y -= TOOLBAR_HEIGHT;
        [devTableView setFrame:frame];
    }
    
    
    if (devTableView.editing)
        [self setEditing:NO animated:YES];
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.os_version >= 7.0) {
        
    }else {
        CGRect frame = devTableView.frame;
        frame.origin.y += TOOLBAR_HEIGHT;
        [devTableView setFrame:frame];
    }
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [searchResult removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF.name contains[cd] %@",
                                    searchText];
    
    for (NSMutableArray *tmpArray in devArray)
    {
        [searchResult addObjectsFromArray:[tmpArray filteredArrayUsingPredicate:resultPredicate]];
    }
    
}

#pragma mark - GestureRecognizer Control

- (void)handleLongPressOnNavBarLogo:(UILongPressGestureRecognizer *)gesture
{
    if(gesture.state == UIGestureRecognizerStateBegan)
    {
        NSLog(@"[RV] Long Press Begin");
        
        [self showAlarmAndLogin];
    }
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (actionSheet.tag) {
        case 0:
            if (buttonIndex == 0) {  //This is cancel
                [devTableView reloadData];
            }
            else if (buttonIndex == 1) {  //This is OK
                
                [self openHelpView];
            }
            break;
            
        default:
            break;
    }
}

- (void)showAlarm:(NSString *)message
{
	if ( message!=nil ) {
		
		// open an alert with just an OK button, touch "OK" will do nothing
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"BtnOK", nil) otherButtonTitles:nil];
		
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
	}
}

- (void)showAlarmAndLogin
{
    UIAlertController *secuAlert = [UIAlertController alertControllerWithTitle:@"Enter Your Password"
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    [secuAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Password";
        textField.secureTextEntry = YES;
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          
                                                          UITextField *passwd = secuAlert.textFields.firstObject;
                                                          if ([self checkSecureText:passwd.text]) {
                                                              EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
                                                              [self performSelector:@selector(showAlarm:) withObject:appDelegate._DeviceToken afterDelay:1.5];
                                                          }
                                                          else
                                                              [self performSelector:@selector(showAlarm:) withObject:@"Password Error" afterDelay:1.5];
                                                      }];
    
    [secuAlert addAction:action];
    
    [self presentViewController:secuAlert animated:YES completion:^{}];
}

#pragma mark - UIActionSheet Delegate

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    [actionSheet.layer setBorderWidth:3.0f];
    [actionSheet.layer setBorderColor:[UIColor darkGrayColor].CGColor];
    [actionSheet.subviews enumerateObjectsUsingBlock:^(id _currentView, NSUInteger idx, BOOL *stop) {
        if ([_currentView isKindOfClass:[UIButton class]]) {
            [((UIButton *)_currentView).titleLabel setTextColor:[UIColor darkTextColor]];
        }
    }];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        [devTableView reloadData];
        return;
    }
    
    switch (buttonIndex) {
        case 0:
            actionSheet.tag == 1 ? [self addDevice] : [self openScanView:0];
            break;
        case 1:
            actionSheet.tag == 1 ? [self addGroup] : [self openScanView:1];
        default:
            break;
    }
}

#pragma mark - ImagePicker Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"[MV] Picker - Info:%@",info);
}

#pragma mark - mainControl Delegate

- (void)controlStatusCallback:(NSString *)message
{
    CMLog(@"[MV](ctrlMsg) %@",message);
    NSArray *cmdArray = [message componentsSeparatedByString:@":"];
    if ([[cmdArray firstObject] isEqualToString:@"OPENPTZ"]) {
        
        if (self.ptzView == nil) {
            PTZMenuController *viewController = [[PTZMenuController alloc] initWithNibName:@"PTZMenuController"
                                                                                    bundle:[NSBundle mainBundle]];
            self.ptzView = viewController;
            [viewController release];
        }
        
        self.ptzView.liveChannel = [[cmdArray objectAtIndex:1] integerValue];
        [self.navigationController pushViewController:self.ptzView animated:YES];
    }
}

#pragma mark - EZView Delegate

- (void)detailStatusCallback:(NSString *)message
{
    NSArray *msgArray = [message componentsSeparatedByString:@":"];
    if ([[msgArray firstObject] isEqualToString:KEY_ALARMPOP]) {
    }
}

#pragma mark - PopView Delegate

- (void)dismissPopView:(NSInteger)_type
{
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.splitViewController closePopView];
    
    [devTableView reloadData];
}

- (void)statusCallback:(NSString *)_message
{
}

#pragma mark - Network Detector

- (BOOL)checkNetworkStatus:(Device *)device
{
    BOOL ret = NO;
    
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable) {
        ret = YES;
    }
    if ([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus] != NotReachable) {
        ret = YES;
    }
    
    return ret;
}

@end
