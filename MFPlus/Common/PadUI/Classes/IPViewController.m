//
//  IPViewController.m
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/8/5.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import "IPViewController.h"
#import "NevioStreamReceiver.h"
#import "HDIPStreamReceiver.h"
#import "CGIIPStreamReceiver.h"
#import "DynaStreamReceiver.h"
#import "GTStreamReceiver.h"
#import "PTStreamReceiver.h"
#import "OnvifStreamReceiver.h"

@interface IPViewController ()

- (BOOL)initStreamingWithDevice:(Device *)dev;
- (void)addStreamingWithViewId:(NSInteger)vIdx;

@end

@implementation IPViewController

#pragma mark - View LifeCycle

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)getDeviceInfomatiionNotify:(NSString *)error withDev:(Device *)dev
{
    if (dev.main_stream_uri != nil || dev.streamType == STREAM_ONVIF) {
        [self initStreamingWithDevice:dev];
        [self addStreamingWithViewId:detailView.selectWindow];
    }
    else
    {
        if (streamArray.count > 1)
            [self showAlarm:error];
        else
            [self showAlarmAndBack:error];
    }
}

- (void)viewDidLoad
{
    // Do any additional setup after loading the view from its nib.
    MMLog(@"[IV] Init %@",self);
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navView = [appDelegate.splitViewController.viewControllers objectAtIndex:1];
    detailView = [navView.viewControllers  objectAtIndex:0];
    
    playingIndexList = [[NSMutableArray alloc] init];
    streamArray = [[NSMutableArray alloc] init];
    
    [searchBtn setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    detailView.delegate = self;
    
    if (blnOpenPtz) {
        return;
    }
    
    //[detailView changeControlMode:ecm_liveIP with:0];
    blnIsSoundOn = NO;
    blnIsSpeakOn = NO;
    blnOpenPtz = NO;
    
    [mainTable reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    if (blnOpenPtz) {
        blnOpenPtz = NO;
        return;
    }
    
    NSInteger devIdx = [devArray indexOfObject:device];
    NSIndexPath *targetPath = [NSIndexPath indexPathForRow:devIdx inSection:0];
    [[mainTable delegate] tableView:mainTable didSelectRowAtIndexPath:targetPath];
    
    [self autoFillMask];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (blnOpenPtz)
        return;
    
    if (streamArray.count == 0) {
        device.blnIsPlaying = NO;
    }
    else if (detailView.videoControl.outputMask == 0) {   //20151112 modified by Ray Lin, RTSP connection error後返回，畫面卡住的問題
        StreamReceiver *tmpStreamReceiver = [streamArray lastObject];
        VideoDisplayView *tmpView = [tmpStreamReceiver.videoControl.aryDisplayViews objectAtIndex:detailView.selectWindow];
        [self closeView:tmpView.playingChannel with:detailView.selectWindow];
    }
    else
        [detailView disconnectAll];
    
    if (blnIsSoundOn)
        [detailView changeSoundMode];
    
    if (blnIsSpeakOn)
        [detailView changeSpeakMode];
    
    [self clearCredentialsCache];
    
    //change back to live control
    [detailView changeControlMode:ecm_live with:0];
    
    //make sure the screen is clean
    [detailView.videoControl refresh];
    
    device = detailView.device = nil;
    speakControl = nil;
    
    [playingIndexList removeAllObjects];
}

- (void)dealloc
{
    MMLog(@"[IV] dealloc %@",self);
    [streamArray removeAllObjects];
    [streamArray release];
    streamArray = nil;
    
    [playingIndexList removeAllObjects];
    [playingIndexList release];
    playingIndexList = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)autoFillMask
{
/*
    for (Device *tmpDevice in devArray)
    {
        if ([playingIndexList indexOfObject:tmpDevice] == NSNotFound) {  //沒有在播放的camera
            
            if (playingIndexList.count+1 > pow([detailView getViewMode],2)) //現有view已點滿了
                return;
            
            //找個空的view來add
            for (VideoDisplayView *tmpView in detailView.videoDispViews)
            {
                if (!tmpView.blnIsPlay) {
                    [detailView changeSelectWindowTo:[detailView.videoDispViews indexOfObject:tmpView]];
                    NSIndexPath *targetPath = [NSIndexPath indexPathForRow:[devArray indexOfObject:tmpDevice] inSection:0];
                    [NSThread sleepForTimeInterval:0.1f];
                    [[mainTable delegate] tableView:mainTable didSelectRowAtIndexPath:targetPath];
                    break;
                }
            }
        }
    }
*/
}

#pragma mark - Table View Data Source

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return devArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setTextColor:[UIColor lightTextColor]];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
        
        UIImageView *checkBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addDev1.png"]];
        [checkBtn setFrame:CGRectMake(0, 0, 27, 27)];
        [cell setAccessoryView:checkBtn];
        [checkBtn release];
        [cell.accessoryView setHidden:YES];
    }
    
    // Configure the cell...
    Device *dev = [devArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@",dev.name];
    
    NSUInteger devIdx = [playingIndexList indexOfObject:dev];
    [cell.accessoryView setHidden:(devIdx == NSNotFound)];
    
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *errDesc = nil;
    
    Device *dev = [devArray objectAtIndex:indexPath.row];
    
    //1.check the device is playing or not
    NSUInteger devIdx = [playingIndexList indexOfObject:dev];
    if (devIdx != NSNotFound) {
        errDesc =  NSLocalizedString(@"MsgNowPlaying2", nil);
        goto Exit;
    }
    
    //2.check the view is available to use
    if (0x1<<detailView.selectWindow & detailView.videoControl.outputMask) {
        
        /*將原本的stream斷線*/
        /*20160112 modified by Ray Lin, let user can watch different ipcam's live stream in quad view*/
        StreamReceiver *tmpStreamReceiver = [streamArray lastObject];
        VideoDisplayView *tmpView = [tmpStreamReceiver.videoControl.aryDisplayViews objectAtIndex:detailView.selectWindow];
        if (tmpView.playingChannel == EMPTY_CHANNEL) {
            NSLog(@"[LV] View:%ld is empty",(long)detailView.selectWindow);
            return;
        }
        
        [self closeView:tmpView.playingChannel with:detailView.selectWindow];
        /**/
        
        //errDesc = NSLocalizedString(@"MsgNowPlaying", nil);
        //goto Exit;
    }
    
    //3.check the device is available to connect
    BOOL ret = [self checkNetworkStatus:dev];
    if (!ret) {
        errDesc = NSLocalizedString(@"MsgNoInternet", nil);
        goto Exit;
    }
    
    //4.initial streaming
    if (dev.streamType == STREAM_ONVIF)
    {
        OnvifDefine *onvif_Req = [[[OnvifDefine alloc] init] autorelease];
        
        if (dev.type == RTSP) {
            dev.device_service = [dev.ip copy];
            ret = [self initStreamingWithDevice:dev];
            if (!ret) {
                errDesc = NSLocalizedString(@"MsgDeviceErr", nil);
                goto Exit;
            }
            
            //5.add streaming
            [self addStreamingWithViewId:detailView.selectWindow];
        }
        else {
            NSString *str_service = [NSString stringWithFormat:@"http://%@:%ld/%@",dev.ip, (long)dev.port, STR_ONVIF_DEVICE_SERVICE];
            dev.device_service = [str_service copy];
            onvif_Req.dev = dev;
            [onvif_Req onvif_GetDeviceInfomation:self];
        }
    }
    else {
        ret = [self initStreamingWithDevice:dev];
        if (!ret) {
            errDesc = NSLocalizedString(@"MsgDeviceErr", nil);
            goto Exit;
        }
        
        //5.add streaming
        [self addStreamingWithViewId:detailView.selectWindow];
    }
    
Exit:
    [self showAlarm:errDesc];
    [mainTable reloadData];
}

#pragma mark - Streaming Control

- (BOOL)initStreamingWithDevice:(Device *)dev
{
    BOOL ret = NO;
    StreamReceiver  *tmpStreamReceiver;
    
    switch (dev.streamType) {
            
        case STREAM_NEVIO:
            tmpStreamReceiver = [[NevioStreamReceiver alloc] initWithDevice:dev];
            NSLog(@"[IV] Initiate Nevio streaming receiver %@",tmpStreamReceiver);
            break;
        case STREAM_HDIP:
            //tmpStreamReceiver = [[HDIPStreamReceiver alloc] initWithDevice:device];
            tmpStreamReceiver = [[CGIIPStreamReceiver alloc] initWithDevice:dev];
            NSLog(@"[IV] Initiate HDIP streaming receiver %@",tmpStreamReceiver);
            break;
        case STREAM_DYNA:
            tmpStreamReceiver = [[DynaStreamReceiver alloc] initWithDevice:dev];
            NSLog(@"[IV] Initiate DYNA streaming receiver %@",tmpStreamReceiver);
            break;
        case STREAM_GEMTEK:
            tmpStreamReceiver = [[GTStreamReceiver alloc] initWithDevice:dev];
            NSLog(@"[IV] Initiate GT streaming receiver %@",tmpStreamReceiver);
            break;
        case STREAM_AFREEY:
            tmpStreamReceiver = [[PTStreamReceiver alloc] initWithDevice:dev];
            NSLog(@"[IV] Initiate PT streaming receiver %@",tmpStreamReceiver);
            break;
        case STREAM_ONVIF:
            tmpStreamReceiver = [[OnvifStreamReceiver alloc] initWithDevice:dev];
            NSLog(@"[IV] Initiate ONVIF streaming receiver");
            break;
    }
    
    if (tmpStreamReceiver != nil) {
        
        [streamArray addObject:tmpStreamReceiver];
        [tmpStreamReceiver release];
        [playingIndexList addObject:dev];
        ret = YES;
    }
    return ret;
}

- (void)addStreamingWithViewId:(NSInteger)vIdx
{
    StreamReceiver *tmpStreamReceiver = [streamArray lastObject];
    VideoDisplayView *tmpView = [detailView.videoControl.aryDisplayViews objectAtIndex:vIdx];
    tmpView.playingChannel = tmpStreamReceiver.deviceId;
    
    //20160112 added by Ray Lin, for management control
    detailView.device = device;
    [detailView checkValidButtonItems:0];   /*因為在IPView的時候button要全開，所以這邊的0不重要*/
    
    if (detailView.videoControl != nil) {
        
        tmpStreamReceiver.videoControl = detailView.videoControl;
        [tmpStreamReceiver.videoControl startByCH:vIdx];
        [tmpStreamReceiver.videoControl setBlnPSLimit:YES];
        [tmpStreamReceiver.outputList replaceObjectAtIndex:0 withObject:[NSString stringWithFormat:@"%ld",(long)vIdx]];
    }
    [mainTable reloadData];
    
    [NSThread detachNewThreadSelector:@selector(startStreaming)
                             toTarget:self
                           withObject:nil];
}

- (void)startStreaming
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    blnStreamThread = YES;
    NSInteger streamID = streamArray.count - 1;
    NSLog(@"[IV] Streaming[%ld] Thread Start %@",(long)streamID,[NSThread currentThread]);
    StreamReceiver *tmpStreamReceiver = [streamArray lastObject];
    NSInteger vIdx = [[tmpStreamReceiver.outputList firstObject] integerValue];
    
    if (blnIsSoundOn) {
        [self closeSound];
        tmpStreamReceiver.audioControl = detailView.audioControl;
        [tmpStreamReceiver openSound];
        blnIsSoundOn = YES;
    }
    
    if (![tmpStreamReceiver startLiveStreaming:0])
    {
        [tmpStreamReceiver.videoControl stopByCH:vIdx];
        
        if (streamArray.count > 1)
            [self showAlarm:tmpStreamReceiver.errorDesc];
        else
            [self showAlarmAndBack:tmpStreamReceiver.errorDesc];
    }
    
    @synchronized (self)
    {
        //remove device from playingList
        for (Device *dev in devArray)
        {
            if (dev.rowID == tmpStreamReceiver.deviceId) {
                NSLog(@"[IV] Device (%@) disconnect",dev.name);
                [playingIndexList removeObject:dev];
                [mainTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
                break;
            }
        }
        
        while (!tmpStreamReceiver.blnConnectionClear)
            [NSThread sleepForTimeInterval:0.1f];
        
        [streamArray removeObject:tmpStreamReceiver];
    }
    NSLog(@"[IV] Streaming[%ld] Thread Stop %@",(long)streamID,[NSThread currentThread]);
    [pool release];
    
    if (streamArray.count == 0)
        blnStreamThread = NO;
}

- (void)closeView:(NSInteger)chIdx with:(NSInteger)vIdx
{
    for (StreamReceiver *tmpStreamReceiver in streamArray)
    {
        if ([[tmpStreamReceiver.outputList firstObject] integerValue] == vIdx) {
            
            if (streamArray.count == 1) {
                
                if (blnIsSpeakOn) {
                    [self closeSpeak];
                }
                if (blnIsSoundOn) {
                    [self closeSound];
                }
            }
            [tmpStreamReceiver stopStreamingByView:vIdx];
            break;
        }
    }
}

- (void)openSound:(NSInteger)chIdx with:(NSInteger)vIdx
{
    for (StreamReceiver *tmpStreamReceiver in streamArray) {
        
        if ([[tmpStreamReceiver.outputList firstObject] integerValue] == vIdx) {
            
            if (detailView.audioControl && !streamReceiver.audioControl)
                tmpStreamReceiver.audioControl = detailView.audioControl;
            
            [tmpStreamReceiver openSound];
            blnIsSoundOn = YES;
            break;
        }
    }
}

- (void)closeSound
{
    for (StreamReceiver *tmpStreamReceiver in streamArray)
        [tmpStreamReceiver closeSound];
    
    blnIsSoundOn = NO;
}

- (void)openSpeak:(NSInteger)chIdx with:(NSInteger)vIdx
{
    if (detailView.speakControl!=nil) {
        
        speakControl = detailView.speakControl;
        [speakControl close];
        
        for (StreamReceiver *tmpStreamReceiver in streamArray) {
            if ([[tmpStreamReceiver.outputList firstObject] integerValue] == vIdx) {
                
                switch (tmpStreamReceiver.streamType) {
                    case STREAM_NEVIO:
                        speakControl = [speakControl initWithCodecId:CODEC_ID_ADPCM_IMA_WAV srate:8000 bps:4 balign:256 fsize:505];
                        NSLog(@"[IV] Initiate NEVIO audio sender");
                        break;
                    case STREAM_HDIP:
                    case STREAM_GEMTEK:
                        speakControl = [speakControl initWithCodecId:CODEC_ID_PCM_MULAW srate:8000 bps:8 balign:320 fsize:320];
                        NSLog(@"[IV] Initiate HDIP audio sender");
                        break;
                }
                [speakControl setPostProcessAction:self action:@selector(sendingData:)];
                [speakControl start];
                [tmpStreamReceiver openSpeak];
                blnIsSpeakOn = YES;
                break;
            }
        }
    }
}

- (void)sendingData:(NSData *)data
{
    for (StreamReceiver *tmpStreamReceiver in streamArray)
    {
        if (tmpStreamReceiver.speakOn) {
            [tmpStreamReceiver.audioSender sendData:data];
            break;
        }
    }
}

- (void)closeSpeak
{
    blnIsSpeakOn = NO;
    for (StreamReceiver *tmpStreamReceiver in streamArray)
        [tmpStreamReceiver closeSpeak];
    
    [speakControl stop];
    
}

- (void)changeAudioCH:(NSInteger)chIdx with:(NSInteger)vIdx
{
    if (blnIsSoundOn || blnIsSpeakOn) {
        
        for (StreamReceiver *tmpStreamReceiver in streamArray) {
            if ([[tmpStreamReceiver.outputList objectAtIndex:0] integerValue] == vIdx) {
                if (blnIsSoundOn && !tmpStreamReceiver.audioOn) {
                    [self closeSound];
                    [self openSound:0 with:vIdx];
                }
                if (blnIsSpeakOn && !tmpStreamReceiver.speakOn) {
                    [self closeSpeak];
                    [self openSpeak:0 with:vIdx];
                }
            }
        }
    }
}

- (void)changePlaybackMode:(NSInteger)btnId
{
    //do nothing
}

- (void)changeResolution:(NSInteger)chIdx
{
    //do nothing
}

#pragma mark - PTZ Menu Control

- (void)openPTZView:(NSInteger)chIdx
{
    if (self.ptzView == nil) {
        
        PTZMenuController *viewController = [[PTZMenuController alloc] initWithNibName:@"PTZMenuController" bundle:[NSBundle mainBundle]];
        self.ptzView = viewController;
        [viewController release];
    }
    
    blnOpenPtz = YES;
    for (StreamReceiver *tmpStreamReceiver in streamArray) {
        
        if ([[tmpStreamReceiver.outputList firstObject] integerValue] == detailView.selectWindow) {
            self.ptzView.ptzStreamReceiver = tmpStreamReceiver;
            break;
        }
    }
    self.ptzView.ptzStreamArray = streamArray;
    self.ptzView.liveChannel = 0;
    
    [self.navigationController pushViewController:self.ptzView animated:YES];
}

@end
