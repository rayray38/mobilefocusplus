//
//  DeviceEditViewController.m
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/7/2.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import "DeviceEditViewController.h"

@interface DeviceEditViewController ()

@end

@implementation DeviceEditViewController

@synthesize editedPropertyKey,editedPropertyDisplayName,pickerViewArray,device;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    MMLog(@"[DEV] Init %@",self);
    // Do any additional setup after loading the view from its nib.
    pickerView.showsSelectionIndicator = YES;
    pickerView.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                             target:self
                                                                             action:@selector(editDone)];
    [self.navigationItem setRightBarButtonItem:doneBtn];
    [doneBtn release];
    
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backBtn.png"]
                                                                style:UIBarButtonItemStyleBordered
                                                               target:self
                                                               action:@selector(editDone)];
    [self.navigationItem setLeftBarButtonItem:backBtn];
    [backBtn release];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.preferredContentSize = POPVIEW_FRAME.size;
	
    self.title = editedPropertyDisplayName;
    [textField setText:@""];
    [textField setHidden:NO];
    [textField setPlaceholder:editedPropertyDisplayName];
    [textField setSecureTextEntry:NO];
    [pickerView setHidden:YES];
    
    if ( [editedPropertyKey isEqualToString:@"name"] ) {
        
        textField.text = device.name;
        textField.keyboardType = UIKeyboardTypeDefault;
    }
    else if ( [editedPropertyKey isEqualToString:@"type"] ) {
        [pickerView reloadAllComponents];
        [pickerView setHidden:NO];
        [textField setHidden:YES];
        
        NSInteger index = [pickerViewArray indexOfObject:[Device modelIndexToString:device.type product:device.product]];
        [pickerView selectRow:index inComponent:0 animated:YES];
    }
    else if ( [editedPropertyKey isEqualToString:@"ip"] ) {
        textField.text = device.ip;
        textField.keyboardType = UIKeyboardTypeURL;
    }
    else if ( [editedPropertyKey isEqualToString:@"port"] ) {
        if (device.port != 0 )
            textField.text = [NSString stringWithFormat:@"%ld",(long)device.port];
        textField.placeholder = editedPropertyDisplayName;
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }
    else if ( [editedPropertyKey isEqualToString:@"user"] ) {
        textField.text = device.user;
        textField.keyboardType = UIKeyboardTypeDefault;
    }
    else if ( [editedPropertyKey isEqualToString:@"password"] ) {
        textField.text = device.password;
        textField.keyboardType = UIKeyboardTypeDefault;
        [textField setSecureTextEntry:YES];
    }
    else if ( [editedPropertyKey isEqualToString:@"rtspport"] )
    {
        if (device.rtspPort != 0 )
            textField.text = [NSString stringWithFormat:@"%ld",(long)device.rtspPort];
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }
    
    if (![editedPropertyKey isEqualToString:@"type"])
        [textField becomeFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if ( [editedPropertyKey isEqualToString:@"name"] ) {
        
        device.name = textField.text;
    }
    else if ( [editedPropertyKey isEqualToString:@"type"] ) {
        
        device.type = [Device modelStringToIndex:[pickerViewArray objectAtIndex:[pickerView selectedRowInComponent:0]] product:device.product];
        device.streamType = [Device getStreamType:device.type product:device.product];
    }
    else if ( [editedPropertyKey isEqualToString:@"ip"] ) {
        
        if (device.type == RTSP) {
            // check and add "rtsp://"        20151110 ++ by Ray Lin
            NSString *result = nil;
            NSString *trimmedStr = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if ( (trimmedStr != nil) && (trimmedStr.length != 0) ) {
                NSRange schemeMarkerRange = [trimmedStr rangeOfString:@"://"];
                
                if (schemeMarkerRange.location == NSNotFound) {
                    result = [[@"rtsp://" stringByAppendingString:trimmedStr] retain];
                }
                else {
                    result = [trimmedStr retain];
                }
            }
            
            device.ip= result;
            SAVE_FREE(result);
        }
        else {
            // check and remove "http://"
            NSString *result = nil;
            NSString *trimmedStr = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if ( (trimmedStr != nil) && (trimmedStr.length != 0) ) {
                NSRange schemeMarkerRange = [trimmedStr rangeOfString:@"://"];
                
                if (schemeMarkerRange.location != NSNotFound) {
                    result = [[trimmedStr substringFromIndex:schemeMarkerRange.location+3] retain];
                }
                else {
                    result = [trimmedStr retain];
                }
            }
            
            device.ip = result;
            SAVE_FREE(result);
        }
    }
    else if ( [editedPropertyKey isEqualToString:@"port"] ) {
        
        device.port = [textField.text intValue];
    }
    else if ( [editedPropertyKey isEqualToString:@"user"] ) {
        
        device.user = textField.text;
    }
    else if ( [editedPropertyKey isEqualToString:@"password"] ) {
        
        device.password = textField.text;
    }
    else if ( [editedPropertyKey isEqualToString:@"rtspport"] ) 
    {//add by robert hsu 20120203
        
        device.rtspPort = [textField.text intValue];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    MMLog(@"[DEV] dealloc %@",self);
    
    [device release];

    if (editedPropertyKey) {
        [editedPropertyKey release];
        editedPropertyKey = nil;
    }
    if (editedPropertyDisplayName) {
        [editedPropertyDisplayName release];
        editedPropertyDisplayName = nil;
    }
    
    [pickerViewArray release];
    
    [pickerView release];
    pickerView = nil;
    
    [textField release];
    textField = nil;
    
    [super dealloc];
}

#pragma mark - Action

- (void)editDone
{
    if ([textField isFirstResponder])
        [textField resignFirstResponder];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}

#pragma mark - UIPickerViewDataSource

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [pickerViewArray objectAtIndex:row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerViewArray.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

@end
