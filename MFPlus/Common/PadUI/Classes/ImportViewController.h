//
//  ImportViewController.h
//  EFViewerHD
//
//  Created by Nobel on 13/10/2.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFViewerAppDelegate.h"

@interface ImportViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView		*devTableView;
    IBOutlet UIBarButtonItem    *btnSelectAll;
    IBOutlet UIBarButtonItem    *btnDeSelect;
    
    NSMutableArray              *devList;
    NSMutableArray              *checkList;
}

@property(nonatomic,retain) id<contentDelegate>     delegate;
@property(nonatomic,retain) NSString*               resultString;

@end
