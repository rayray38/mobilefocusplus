//
//  PopViewController.h
//  EFViewerHDPlus
//
//  Created by Nobel on 2014/12/3.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFViewerAppDelegate.h"
#import "StreamReceiver.h"

@interface PopViewController : UIViewController

@property(nonatomic, assign) id<contentDelegate> delegate;
@property(nonatomic, assign) StreamReceiver      *streamer;
@property(nonatomic)         NSInteger           viewIdx;

@end
