//
//  ChannelViewController.h
//  EFViewerHD
//
//  Created by James Lee on 12/10/8.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Device.h"
#import "StreamReceiver.h"
#import "VideoDisplayView.h"
#import "EFViewerAppDelegate.h"
#import "DetailViewController.h"
#import "PTZMenuController.h"
#import "SearchViewController.h"
#import "PopViewController.h"
#import "Reachability.h"

#define INDICATOR_HEIGHT        37

@interface ChannelViewController : UIViewController <UITableViewDelegate,UITableViewDataSource
                                                    ,UIAlertViewDelegate,UIActionSheetDelegate
                                                    ,searchDelegate,mainControlDelegate
                                                    ,contentDelegate, StreamDelegate>
{
    IBOutlet UITableView  *mainTable;
    IBOutlet UIButton     *searchBtn;
    IBOutlet UIButton     *alarmBtn;
    
    StreamReceiver        *streamReceiver;
    DetailViewController  *detailView;
    UIActivityIndicatorView    *activityIndicator;
    
    Device                *device;
    PTZMenuController     *ptzView;
    AudioInput            *speakControl;
    
    NSMutableArray        *playingIndexList;
    NSMutableArray        *devArray;
    NSUInteger            currentPlayingMask;
    
    BOOL                  blnFullScreen;
    BOOL                  blnFirstConnect;
    BOOL                  blnIsSoundOn;
    BOOL                  blnIsSpeakOn;
    BOOL                  blnOpenPtz;
    BOOL                  blnFavorite;
    BOOL                  blnOpenSearch;
    BOOL                  blnPlaybackPause;
    BOOL                  blnStreamThread;
    BOOL                  blnInfoThread;
    BOOL                  blnGetInfo;
    BOOL                  blnPopEnable;
    BOOL                  blnShowPop;
    BOOL                  blnSmoothMode;
    BOOL                  blnXmsIPcam;
    BOOL                  blnXms1stConnect;
    
    NSTimer               *statusTimer;
    
    NSInteger             pbSpeed;
    NSInteger             totalChannel;
}

- (IBAction)openSearchMenu;
- (IBAction)switchAlarmPop;

- (void)showAlarmAndBack:(NSString *)message;
- (void)showAlarm:(NSString *)message;
- (void)clearCredentialsCache;
- (BOOL)checkNetworkStatus:(Device *)device;

@property (nonatomic, retain) Device                *device;
@property (nonatomic, assign) EventTable            *event;
@property (nonatomic, retain) PTZMenuController     *ptzView;
@property (nonatomic, assign) NSMutableArray        *devArray;
@property (nonatomic)         BOOL                  blnChSelector;
@property (nonatomic)         BOOL                  blnEventMode;
@property (nonatomic)         BOOL                  blnEventMode_pb;
@property (nonatomic)         BOOL                  blnOpenSearch;
@property (nonatomic)         BOOL                  blnWillOpenSearch;
@property (nonatomic)         BOOL                  blnPlaybackMode;

@end
