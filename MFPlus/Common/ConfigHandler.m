//
//  ConfigHandler.m
//  EFSideKick
//
//  Created by Nobel on 2014/3/6.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import "ConfigHandler.h"
#import "Device.h"

@implementation NetSeed

@synthesize blnStatic,ipv4,ipv6,subMask,httpPort,rtspPort,name;

@end

@implementation ConfigHandler

@synthesize host,user,passwd;

#pragma mark - Init / Dealloc

- (id)initWithHost:(NSString *)theHost
{
    if ((self = [super init])) {
        
        // init cmdSender
		cmdSender = [[CommandSender alloc] initWithHost:theHost delegate:self];
        
        // init xmlArray
        xmlArray = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (void)dealloc
{
    if (cmdSender)
        [cmdSender release];
    
	if (user)
        [user release];
    
	if (passwd)
        [passwd release];
    
	if (currentXmlElement)
        [currentXmlElement release];
    
    if (responseData)
        [responseData release];
    
    if (xmlArray) {
        [xmlArray removeAllObjects];
        [xmlArray release];
    }
    
    [super dealloc];
}

#pragma mark - Device Information

- (NSString *)getDeviceNameByProtocol:(NSInteger)type
{
    NSString *name;
    NSString *cmd;
    NSString *response;
    
    if (host != cmdSender.host)
        [cmdSender setHost:host];
    
    if (type == STREAM_HDIP) {
        
    }else if (type == STREAM_AFREEY) {
        cmd = [NSString stringWithFormat:@"%@=3&TitleBar",AFREEY_SYSINFO];
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
        response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        //NSLog(@"response:%@",response);
        if ([response rangeOfString:AFREEY_OK].location != NSNotFound) {
            name = [self getStringFromLine:response targetString:@"TitleBar"];
        }
    }
    
    return name;
}

#pragma mark - Network Configuration

- (BOOL)getNetworkConfigByProtocol:(NSInteger)type
{
    NSString *cmd;
    NSString *response;
    BOOL ret = YES;
    
    if (host != cmdSender.host)
        [cmdSender setHost:host];
    
    [xmlArray removeAllObjects];
    
    if (type == STREAM_HDIP) {
        
        //get portSetting
        cmd = [NSString stringWithFormat:@"%@",PSIA_CUSTOM];
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
        if (responseData != nil) {
            response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            //NSLog(@"response:%@",response);
            [cmdSender parseXmlResponse:response];
            [xmlArray setObject:response forKey:PSIA_CUSTOM];
            [response release];
        } else
            ret = NO;
        
        //get device info
        cmd = [NSString stringWithFormat:@"%@",PSIA_INFO];
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
        if (responseData != nil) {
            response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            //NSLog(@"response:%@",response);
            [cmdSender parseXmlResponse:response];
            [xmlArray setObject:response forKey:PSIA_INFO];
            [response release];
        } else
            ret = NO;
        
        //get ipAddress
        cmd = [NSString stringWithFormat:@"%@",PSIA_IPADDRESS];
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
        if (responseData != nil) {
            response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            //NSLog(@"response:%@",response);
            [cmdSender parseXmlResponse:response];
            [xmlArray setObject:response forKey:PSIA_IPADDRESS];
            [response release];
        } else
            ret = NO;
        
    }else if (type == STREAM_AFREEY) {
        
        //get all network config
        cmd = [NSString stringWithFormat:@"%@=1",AFREEY_NETWORK];
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
        response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        //NSLog(@"response:%@",response);
        if ([response rangeOfString:AFREEY_OK].location == NSNotFound) {
            ret = NO;
        }else {
            //IPType 0:DHCP 1:Fixed IP
            NSString *strConfig = [self getStringFromLine:response targetString:@"IPType"];
            [xmlArray setObject:strConfig forKey:@"IPType"];
            
            //IPAddr
            strConfig = [self getStringFromLine:response targetString:@"IPAddr"];
            [xmlArray setObject:strConfig forKey:@"IPAddr"];
            
            //Http Port
            strConfig = [self getStringFromLine:response targetString:@"HttpPort"];
            [xmlArray setObject:strConfig forKey:@"HttpPort"];
            
            //Rtsp Port
            strConfig = [self getStringFromLine:response targetString:@"RTSPPort"];
            [xmlArray setObject:strConfig forKey:@"RTSPPort"];
        }
        [response release];
    }
    
    return ret;
}

- (BOOL)setNetworkConfigByProtocol:(NSInteger)type Seed:(NetSeed *)seed
{
    BOOL ret =  YES;
    NSString *cmd;
    
    if (host != cmdSender.host)
        [cmdSender setHost:host];
    
    if (type == STREAM_HDIP) {
        
        /* ---------------------------------set ipAddress------------------------------------- */
        cmd = [xmlArray objectForKey:PSIA_IPADDRESS];
        
        BOOL blnSetIP = NO;
        //Set IP Type
        if(seed.blnStatic && [[xmlArray objectForKey:@"addressingType"] isEqualToString:PSIA_DYNAMIC]) {
            cmd = [cmd stringByReplacingOccurrencesOfString:PSIA_DYNAMIC withString:PSIA_STATIC];
            //Set IP address
            cmd = [cmd stringByReplacingOccurrencesOfString:[xmlArray objectForKey:@"ipAddress"] withString:seed.ipv4];
            blnSetIP = YES;
        }else if(!seed.blnStatic && [[xmlArray objectForKey:@"addressingType"] isEqualToString:PSIA_STATIC]) {
            cmd = [cmd stringByReplacingOccurrencesOfString:PSIA_STATIC withString:PSIA_DYNAMIC];
            blnSetIP = YES;
        }
        
        if (blnSetIP) {
            [cmdSender putPSIACommand:cmd with:PSIA_IPADDRESS connection:&requestConnection synchronously:YES];
            NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            //NSLog(@"[CH] response:%@",response);
            ret = [[cmdSender getResult:response] isEqualToString:@"OK"];
            [response release];
        }
        
        /* ----------------------------------set deviceInfo------------------------------------ */
        cmd = [xmlArray objectForKey:PSIA_INFO];

        BOOL blnSetName = NO;
        if (![[xmlArray objectForKey:@"deviceName"] isEqualToString:seed.name]) {
            //set Device Name
            NSString *nameStr = [NSString stringWithFormat:@"<deviceName>%@</deviceName>",[xmlArray objectForKey:@"deviceName"]];
            NSString *setStr = [NSString stringWithFormat:@"<deviceName>%@</deviceName>",seed.name];
            cmd = [cmd stringByReplacingOccurrencesOfString:nameStr withString:setStr];
            blnSetName = YES;
        }
        
        if (blnSetName) {
            [cmdSender putPSIACommand:cmd with:PSIA_INFO connection:&requestConnection synchronously:YES];
            NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            //NSLog(@"[CH] response:%@",response);
            ret = [[cmdSender getResult:response] isEqualToString:@"OK"];
            [response release];
        }
        
        /* ------------------------------------------------------------------------------------ */
        cmd = [xmlArray objectForKey:PSIA_CUSTOM];
        
        BOOL blnSetPort = NO;
        if ([[xmlArray objectForKey:@"HttpPortNo"] integerValue] != seed.httpPort) {
            
            //Set Http Port
            NSString *portStr = [NSString stringWithFormat:@"<HTTPPortSetting><PortNo>%@</PortNo></HTTPPortSetting>",[xmlArray objectForKey:@"HttpPortNo"]];
            NSString *setStr = [NSString stringWithFormat:@"<HTTPPortSetting><PortNo>%d</PortNo></HTTPPortSetting>",seed.httpPort];
            cmd = [cmd stringByReplacingOccurrencesOfString:portStr withString:setStr];
            blnSetPort = YES;
        }
        if ([[xmlArray objectForKey:@"RtspPortNo"] integerValue] != seed.rtspPort) {
            //Set RTSP Port
            NSString *portStr = [NSString stringWithFormat:@"<RtspPortNo>%@</RtspPortNo>",[xmlArray objectForKey:@"RtspPortNo"]];
            NSString *setStr = [NSString stringWithFormat:@"<RtspPortNo>%d</RtspPortNo>",seed.rtspPort];
            cmd = [cmd stringByReplacingOccurrencesOfString:portStr withString:setStr];
            blnSetPort = YES;
        }
        
        if (blnSetPort) {
            [cmdSender putPSIACommand:cmd with:PSIA_CUSTOM connection:&requestConnection synchronously:YES];
            NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            //NSLog(@"[CH] response:%@",response);
            ret = [[cmdSender getResult:response] isEqualToString:@"OK"];
            [response release];
        }
    }else if (type == STREAM_AFREEY) {
        
        /* ---------------------------------------------------------------------- */
        cmd = [NSString stringWithFormat:@"%@=2",AFREEY_SYSINFO];
        
        BOOL blnInfoSend = NO;
        //set device info
        if (![[xmlArray objectForKey:@"TitleBar"] isEqualToString:seed.name]) {
            cmd = [cmd stringByAppendingString:[NSString stringWithFormat:@"&TitleBar=%@",seed.name]];
            blnInfoSend = YES;
        }
        
        if (blnInfoSend) {
            [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
            NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            //NSLog(@"response:%@",response);
            [response release];
        }
        /* ---------------------------------------------------------------------- */

        cmd = [NSString stringWithFormat:@"%@=2",AFREEY_NETWORK];
        
        BOOL blnSend = NO;
        //Set IPType & IPAddr
        if (seed.blnStatic && [[xmlArray objectForKey:@"IPType"] integerValue] != 1) {
            cmd = [cmd stringByAppendingString:[NSString stringWithFormat:@"&IPType=1&IPAddr=%@",seed.ipv4]];
            blnSend = YES;
        }else if (!seed.blnStatic && [[xmlArray objectForKey:@"IPType"] integerValue] != 0) {
            cmd = [cmd stringByAppendingString:@"&IPType=0"];
            blnSend = YES;
        }
        //Set HttpPort
        if ([[xmlArray objectForKey:@"HttpPort"] integerValue] != seed.httpPort) {
            cmd = [cmd stringByAppendingString:[NSString stringWithFormat:@"&HttpPort=%d",seed.httpPort]];
            blnSend = YES;
        }
        //Set RtspPort
        if ([[xmlArray objectForKey:@"RTSPPort"] integerValue] != seed.rtspPort) {
            cmd = [cmd stringByAppendingString:[NSString stringWithFormat:@"&RTSPPort=%d",seed.rtspPort]];
            blnSend = YES;
        }
        
        if (blnSend) {
            [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
            NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            //NSLog(@"response:%@",response);
            [response release];
        }
    }
    
    return ret;
}

#pragma mark - Parser Delegate

- (NSString *)getStringFromLine:(NSString *)data targetString:(NSString *)target
{
    NSString  *echoString = nil;
    NSString  *lineString;
    NSArray *lines = [data componentsSeparatedByString:@"\n"];
    NSEnumerator *nse = [lines objectEnumerator];
    NSString  *tmp = [NSString stringWithFormat:@"%@=",target];
    
    while (lineString = [nse nextObject]) {
        NSScanner *scanner = [NSScanner scannerWithString:lineString];
        [scanner scanUpToString:tmp intoString:nil];
        [scanner scanString:tmp intoString:nil];
        [scanner scanUpToString:@"=" intoString:&echoString];
        if (echoString != NULL) {
            break;
        }
    }
    //NSLog(@"[CH] echo:%@",echoString);
    return echoString;
}

// Start of element
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
	attributes:(NSDictionary *)attributeDict
{
    [currentXmlElement release];
    currentXmlElement = [elementName copy];
    
    if ([currentXmlElement isEqualToString:@"deviceName"]) {
        blnGetName = YES;
    }
    if ([currentXmlElement isEqualToString:@"addressingType"]) {
        blnGetIPAddress = YES;
    }
    if ([currentXmlElement isEqualToString:@"HTTPPortSetting"] ||
        [currentXmlElement isEqualToString:@"EverFocusEstablish"]) {
        blnGetCustom = YES;
    }
}

// Found Character
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSMutableString *)string
{
    //PSIA
    if (blnGetName) {
        if ([currentXmlElement isEqualToString:@"deviceName"]) {
            [xmlArray setObject:string forKey:@"deviceName"];
        }
    }
    if (blnGetIPAddress) {
    
        if ([currentXmlElement isEqualToString:@"addressingType"]) {
            [xmlArray setObject:string forKey:@"addressingType"];
        }
        if ([currentXmlElement isEqualToString:@"ipAddress"]) {
            [xmlArray setObject:string forKey:@"ipAddress"];
        }
        if ([currentXmlElement isEqualToString:@"subnetMask"]) {
            [xmlArray setObject:string forKey:@"subnetMask"];
        }
    }
    if (blnGetCustom) {
        
        if ([currentXmlElement isEqualToString:@"PortNo"]) {
            [xmlArray setObject:string forKey:@"HttpPortNo"];
        }
        if ([currentXmlElement isEqualToString:@"RtspPortNo"]) {
            [xmlArray setObject:string forKey:@"RtspPortNo"];
        }
    }
}

// End Element
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"deviceName"]) {
        blnGetName = NO;
    }
    if ([elementName isEqualToString:@"bitMask"]) {
        blnGetIPAddress = NO;
    }
    if ([elementName isEqualToString:@"HTTPPortSetting"] ||
        [elementName isEqualToString:@"EverFocusEstablish"]) {
        blnGetCustom = NO;
    }
}

#pragma mark - Connection Delegate

- (void)connection:(NSURLConnection *)theConnection didReceiveResponse:(NSURLResponse *)response
// A delegate method called by the NSURLConnection when the request/response
// exchange is complete.
{
	NSHTTPURLResponse * httpResponse;
	httpResponse = (NSHTTPURLResponse *) response;
	
	if ((httpResponse.statusCode / 100) != 2) {
		
        errorDesc = [NSString stringWithFormat:@"HTTP error %zd", (ssize_t) httpResponse.statusCode];
        NSLog(@"[CH] %@",errorDesc);
		return;
	}
    
	if ( theConnection == requestConnection ) {
		
		if (responseData != nil)
		{
			[responseData release];
			responseData = nil;
		}
		
		responseData = [NSMutableData alloc];
	}
}

- (void)connectionDidFinishLoading:(NSURLConnection *)theConnection
// A delegate method called by the NSURLConnection when the connection has been done successfully.
{
	if ( theConnection == requestConnection ) {
		
		requestConnection = nil;
	}

	NSLog(@"[CH] Connection finished");
}


- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error
// A delegate method called by the NSURLConnection if the connection fails.
{
    if ( theConnection==requestConnection ) {
		
		requestConnection = nil;
	}
	
    errorDesc = [error localizedDescription];
	NSLog(@"[CH] Connection failed with error: %@", [error localizedDescription]);
}


- (void)connection:(NSURLConnection *)theConnection
didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
	NSURLCredential *EWScreds = [NSURLCredential credentialWithUser: user
														   password: passwd
														persistence: NSURLCredentialPersistenceForSession];
	
    NSLog(@"[CH] Authentication Challenge %@:%@",user,passwd);
    if ([challenge previousFailureCount] == 0)
    {
		[[challenge sender] useCredential:EWScreds forAuthenticationChallenge:challenge];
    }
    else
	{
		if ( theConnection==requestConnection ) {
			
			requestConnection = nil;
		}
		
		[theConnection cancel];
        errorDesc = NSLocalizedString(@"MsgAuthFail", nil);
		
        NSLog(@"[CH] Authentication failed on attempt %d", [challenge previousFailureCount]);
	}
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    
    return nil;
}

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
{
    if ( theConnection==requestConnection ) {
		
		[responseData appendData:data];
	}
}

@end
