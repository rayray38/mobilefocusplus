//
//  OnvifDefine.h
//  EFViewerPlus
//
//  Created by Ray Lin on 2015/7/3.
//  Copyright (c) 2015年 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Device.h"

#define STR_ONVIF_DEVICE_SERVICE @"onvif/device_service"

typedef enum {
    ONVIF_REQUEST_NONE = 0,
    ONVIF_REQUEST_GET_MAIN_STREAMURI,
    ONVIF_REQUEST_GET_SUB_STREAMURI,
    ONVIF_REQUEST_GET_CAPABILITIES,
    ONVIF_REQUEST_GET_PROFILES,
    ONVIF_REQUEST_GET_PTZ_CONFIG,
} ONVIF_REQUEST;

@interface OnvifDefine : NSObject <NSURLSessionDataDelegate,NSURLSessionTaskDelegate>
{
    NSMutableData *_responseData; // Added by Steven Chang
    ONVIF_REQUEST onvif_request;
    id aTarget;
    id message;
}

@property (nonatomic, retain)    Device   *dev;

- (void)onvif_GetDeviceInfomation:(id)target;
- (BOOL)parseCapabilitiesXml:(NSString*)xmlString;
- (BOOL)parseProfilesXml:(NSString*)xmlString;
- (BOOL)parseStreamUriXml:(NSString*)xmlString;

@end

@protocol deviceNotify

- (void)getDeviceInfomatiionNotify:(NSString *)error  withDev:(Device *)dev;

@end
