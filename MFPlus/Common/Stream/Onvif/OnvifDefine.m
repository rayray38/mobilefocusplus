//
//  OnvifDefine.m
//  EFViewerPlus
//
//  Created by Ray Lin on 2015/7/3.
//  Copyright (c) 2015年 EverFocus. All rights reserved.
//

#import "OnvifDefine.h"
#import "Device.h"
#include "soap_security.h"

#define NONCE_LEN	(20)
#define NONCE_BASE64_LEN	((NONCE_LEN+3)/3*4+1)
#define REQUEST_TIMEOUT (5.0)

#define MAX_CREATED_LEN  (28)

NSString *httpGetCapabilitiesContent =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
"<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tds=\"http://www.onvif.org/ver10/device/wsdl\" xmlns:tt=\"http://www.onvif.org/ver10/schema\">\r\n"
"  <s:Header xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\">\r\n"
"    <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\r\n"
"      <wsse:UsernameToken>\r\n"
"        <wsse:Username>%@</wsse:Username>\r\n"
"        <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">%@</wsse:Password>\r\n"
"        <wsse:Nonce>%@</wsse:Nonce>\r\n"
"        <wsu:Created>%@</wsu:Created>\r\n"
"      </wsse:UsernameToken>\r\n"
"    </wsse:Security>\r\n"
"  </s:Header>\r\n"
"  <soap:Body>\r\n"
"    <tds:GetCapabilities>\r\n"
"      <tds:Category>All</tds:Category>\r\n"
"    </tds:GetCapabilities>\r\n"
"  </soap:Body>\r\n"
"</soap:Envelope>\r\n";
// Username, PasswordDigest, Nonce, Created
// media_uri

NSString *httpGetProfilesContent =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
"<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:trt=\"http://www.onvif.org/ver10/media/wsdl\" xmlns:tt=\"http://www.onvif.org/ver10/schema\">\r\n"
"  <s:Header xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\">\r\n"
"    <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\r\n"
"      <wsse:UsernameToken>\r\n"
"        <wsse:Username>%@</wsse:Username>\r\n"
"        <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">%@</wsse:Password>\r\n"
"        <wsse:Nonce>%@</wsse:Nonce>\r\n"
"        <wsu:Created>%@</wsu:Created>\r\n"
"      </wsse:UsernameToken>\r\n"
"    </wsse:Security>\r\n"
"  </s:Header>\r\n"
"  <soap:Body>\r\n"
"    <trt:GetProfiles />\r\n"
"  </soap:Body>\r\n"
"</soap:Envelope>\r\n\r\n";
// Username, PasswordDigest, Nonce, Created
// media_uri

NSString *http_PTZ_GetConfigurationOptions =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
"<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tptz=\"http://www.onvif.org/ver20/ptz/wsdl\">\r\n"
"  <s:Header xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\">\r\n"
"    <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\r\n"
"      <wsse:UsernameToken>\r\n"
"        <wsse:Username>%@</wsse:Username>\r\n"
"        <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">%@</wsse:Password>\r\n"
"        <wsse:Nonce>%@</wsse:Nonce>\r\n"
"        <wsu:Created>%@</wsu:Created>\r\n"
"      </wsse:UsernameToken>\r\n"
"    </wsse:Security>\r\n"
"  </s:Header>\r\n"
"  <soap:Body>\r\n"
"    <tptz:GetConfigurationOptions>\r\n"
"       <tptz:ConfigurationToken>ptzconf0</tptz:ConfigurationToken>\r\n"
"    </tptz:GetConfigurationOptions>\r\n"
"  </soap:Body>\r\n"
"</soap:Envelope>\r\n\r\n";
// Username, PasswordDigest, Nonce, Created

NSString *httpGetStreamUriContent =
@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
"<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:trt=\"http://www.onvif.org/ver10/media/wsdl\" xmlns:tt=\"http://www.onvif.org/ver10/schema\">\r\n"
"  <s:Header xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\">\r\n"
"    <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\r\n"
"      <wsse:UsernameToken>\r\n"
"        <wsse:Username>%@</wsse:Username>\r\n"
"        <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">%@</wsse:Password>\r\n"
"        <wsse:Nonce>%@</wsse:Nonce>\r\n"
"        <wsu:Created>%@</wsu:Created>\r\n"
"      </wsse:UsernameToken>\r\n"
"    </wsse:Security>\r\n"
"  </s:Header>\r\n"
"  <soap:Body>\r\n"
"    <GetStreamUri xmlns=\"http://www.onvif.org/ver10/media/wsdl\">\r\n"
"      <StreamSetup>\r\n"
"        <!-- Attribute Wild card could not be matched. Generated XML may not be valid. -->\r\n"
"        <Stream xmlns=\"http://www.onvif.org/ver10/schema\">RTP-Unicast</Stream>\r\n"
"        <Transport xmlns=\"http://www.onvif.org/ver10/schema\">\r\n"
"          <Protocol>TCP</Protocol>\r\n"
"        </Transport>\r\n"
"      </StreamSetup>\r\n"
"      <ProfileToken>%@</ProfileToken>\r\n"
"    </GetStreamUri>\r\n"
"  </soap:Body>\r\n"
"</soap:Envelope>\r\n";
// Username, PasswordDigest, Nonce, Created, ProfileToken
// media_uri

#define DEFAULT_CONCURRENTOPERATION 1

@interface OnvifDefine()
@property (nonatomic, strong) NSOperationQueue *operationQueue;
@property (nonatomic, strong) NSMutableArray   *operations;
@end

@implementation OnvifDefine

@synthesize dev;

+ (instancetype)sharedManager
{
    static OnvifDefine *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[OnvifDefine alloc] init];
    });
    return manager;
}

- (instancetype)init
{
    if (self = [super init]) {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = DEFAULT_CONCURRENTOPERATION;
    }
    return self;
}

- (void)onvif_GetDeviceInfomation:(id)target
{
    aTarget = target;
    message = @"Authentication Failed";
    if (dev.user == nil || dev.password == nil) {
        [target performSelector:@selector(showAlarm:)  withObject:message];
        return;
    }
    onvif_request = ONVIF_REQUEST_GET_CAPABILITIES;
    [self onvif_GetCapablilities];
}

- (void)onvif_GetCapablilities
{
    unsigned char nonce[NONCE_LEN];
    char nonce_base64[NONCE_BASE64_LEN];
    char created[MAX_CREATED_LEN];
    unsigned char HA[SOAP_SMD_SHA1_SIZE];
    char HABase64[29];
    
    gen_nonce(nonce, NONCE_LEN);
    get_created(created, MAX_CREATED_LEN);
    calc_digest(created, nonce, NONCE_LEN, [dev.password UTF8String], HA);
    soap_s2base64(nonce, nonce_base64, NONCE_LEN);
    soap_s2base64(HA, HABase64, SOAP_SMD_SHA1_SIZE);
    
    NSLog(@"[WS] Device request [%@]\n", dev.device_service);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:dev.device_service]];
    
    // Specify that it will be a POST request
    [request setHTTPMethod: @"POST"];
    
    // This is how we set header fields
    [request setValue:@"application/soap+xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    // Convert your data and set your request's HTTPBody property
    NSString *stringData = [NSString stringWithFormat:httpGetCapabilitiesContent,
                            dev.user,
                            [NSString stringWithCString:HABase64 encoding:NSUTF8StringEncoding],
                            [NSString stringWithCString:nonce_base64 encoding:NSUTF8StringEncoding],
                            [NSString stringWithCString:created encoding:NSUTF8StringEncoding]];
    //NSLog(@"stringData:%@",stringData);
    NSData *requestBodyData = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:requestBodyData];
    [request setTimeoutInterval:REQUEST_TIMEOUT];
    
    // Create url connection and fire request
    //SAVE_FREE(conn);
    //NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (BOOL)parseCapabilitiesXml:(NSString*)xmlString
{
    NSRange range1 = [xmlString rangeOfString:@"<tt:Media>" options:NSCaseInsensitiveSearch];
    NSRange range2 = [xmlString rangeOfString:@"</tt:Media>" options:NSCaseInsensitiveSearch];
    if (range1.location == NSNotFound || range2.location == NSNotFound)
        return false;
    NSRange range = { range1.location+range1.length, range2.location-range1.location-range1.length };
    range1 = [xmlString rangeOfString:@"<tt:XAddr>" options:NSCaseInsensitiveSearch range:range];
    range2 = [xmlString rangeOfString:@"</tt:XAddr>" options:NSCaseInsensitiveSearch range:range];
    if (range1.location == NSNotFound || range2.location == NSNotFound)
        return false;
    range = NSMakeRange(range1.location+range1.length, range2.location-range1.location-range1.length);
    dev.media_uri = [xmlString substringWithRange:range];
    NSLog(@"[WS] media_uri:%@", dev.media_uri);
    
    range1 = [xmlString rangeOfString:@"<tt:Device>" options:NSCaseInsensitiveSearch];
    range2 = [xmlString rangeOfString:@"</tt:Device>" options:NSCaseInsensitiveSearch];
    if (range1.location == NSNotFound || range2.location == NSNotFound)
        return false;
    range = NSMakeRange(range1.location+range1.length, range2.location-range1.location-range1.length);
    range1 = [xmlString rangeOfString:@"<tt:XAddr>" options:NSCaseInsensitiveSearch range:range];
    range2 = [xmlString rangeOfString:@"</tt:XAddr>" options:NSCaseInsensitiveSearch range:range];
    if (range1.location == NSNotFound || range2.location == NSNotFound)
        return false;
    range = NSMakeRange(range1.location+range1.length, range2.location-range1.location-range1.length);
    dev.device_uri = [xmlString substringWithRange:range];
    NSLog(@"[WS] device_uri:%@", dev.device_uri);
    return true;
}

- (void)onvif_GetProfiles
{
    unsigned char nonce[NONCE_LEN];
    char nonce_base64[NONCE_BASE64_LEN];
    char created[MAX_CREATED_LEN];
    unsigned char HA[SOAP_SMD_SHA1_SIZE];
    char HABase64[29];
    
    gen_nonce(nonce, NONCE_LEN);
    get_created(created, MAX_CREATED_LEN);
    calc_digest(created, nonce, NONCE_LEN, [dev.password UTF8String], HA);
    soap_s2base64(nonce, nonce_base64, NONCE_LEN);
    soap_s2base64(HA, HABase64, SOAP_SMD_SHA1_SIZE);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:dev.media_uri]];
    
    // Specify that it will be a POST request
    request.HTTPMethod = @"POST";
    
    // This is how we set header fields
    [request setValue:@"application/soap+xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    // Convert your data and set your request's HTTPBody property
    NSString *stringData = [NSString stringWithFormat:httpGetProfilesContent,
                            dev.user,
                            [NSString stringWithCString:HABase64 encoding:NSUTF8StringEncoding],
                            [NSString stringWithCString:nonce_base64 encoding:NSUTF8StringEncoding],
                            [NSString stringWithCString:created encoding:NSUTF8StringEncoding]];
    NSData *requestBodyData = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody : requestBodyData ];
    [request setTimeoutInterval:REQUEST_TIMEOUT];
    
    // Create url connection and fire request
    //SAVE_FREE(conn);
    //conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (BOOL)parseProfilesXml:(NSString*)xmlString
{
    NSRange range1 = [xmlString rangeOfString:@"<trt:GetProfilesResponse>" options:NSCaseInsensitiveSearch];
    NSRange range2 = [xmlString rangeOfString:@"</trt:GetProfilesResponse>" options:NSCaseInsensitiveSearch];
    if (range1.location == NSNotFound || range2.location == NSNotFound)
        return false;
    NSRange responseRange = { range1.location+range1.length, range2.location-range1.location-range1.length };
    range1 = [xmlString rangeOfString:@"<trt:Profiles" options:NSCaseInsensitiveSearch range:responseRange];
    range2 = [xmlString rangeOfString:@">" options:NSCaseInsensitiveSearch range:responseRange];
    NSRange range3 = [xmlString rangeOfString:@"</trt:Profiles>" options:NSCaseInsensitiveSearch range:responseRange];
    if (range1.location == NSNotFound || range2.location == NSNotFound)
        return false;
    NSRange range = NSMakeRange(range1.location+range1.length, range2.location-range1.location-range1.length);
    range1 = [xmlString rangeOfString:@"token=\"" options:NSCaseInsensitiveSearch range:range];
    if (range1.location == NSNotFound)
        return false;
    range = NSMakeRange(range1.location+range1.length, range2.location-range1.location-range1.length);
    range2 = [xmlString rangeOfString:@"\"" options:NSCaseInsensitiveSearch range:range];
    if (range2.location == NSNotFound)
        return false;
    range = NSMakeRange(range1.location+range1.length, range2.location-range1.location-range1.length);
    dev.main_profile_token = [xmlString substringWithRange:range];
    NSLog(@"[WS] main profile_token:%@", dev.main_profile_token);
    
    range.location = range3.location + range3.length;
    range.length = responseRange.length - (range3.location + range3.length - responseRange.location);
    range1 = [xmlString rangeOfString:@"<trt:Profiles" options:NSCaseInsensitiveSearch range:range];
    range2 = [xmlString rangeOfString:@">" options:NSCaseInsensitiveSearch range:range];
    if (range1.location == NSNotFound || range2.location == NSNotFound)
        return true;
    
    range = NSMakeRange(range1.location+range1.length, range2.location-range1.location-range1.length);
    range1 = [xmlString rangeOfString:@"token=\"" options:NSCaseInsensitiveSearch range:range];
    if (range1.location == NSNotFound)
        return true;
    range = NSMakeRange(range1.location+range1.length, range2.location-range1.location-range1.length);
    range2 = [xmlString rangeOfString:@"\"" options:NSCaseInsensitiveSearch range:range];
    if (range2.location == NSNotFound)
        return true;
    range = NSMakeRange(range1.location+range1.length, range2.location-range1.location-range1.length);
    dev.sub_profile_token = [xmlString substringWithRange:range];
    NSLog(@"[WS] sub profile_token:%@", dev.sub_profile_token);
    
    return true;
}

- (void)onvif_PTZ_GetConfigurationOptions
{
    unsigned char nonce[NONCE_LEN];
    char nonce_base64[NONCE_BASE64_LEN];
    char created[MAX_CREATED_LEN];
    unsigned char HA[SOAP_SMD_SHA1_SIZE];
    char HABase64[29];
    
    gen_nonce(nonce, NONCE_LEN);
    get_created(created, MAX_CREATED_LEN);
    calc_digest(created, nonce, NONCE_LEN, [dev.password UTF8String], HA);
    soap_s2base64(nonce, nonce_base64, NONCE_LEN);
    soap_s2base64(HA, HABase64, SOAP_SMD_SHA1_SIZE);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:dev.media_uri]];
    
    // Specify that it will be a POST request
    request.HTTPMethod = @"POST";
    
    // This is how we set header fields
    [request setValue:@"application/soap+xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    // Convert your data and set your request's HTTPBody property
    NSString *stringData = [NSString stringWithFormat:http_PTZ_GetConfigurationOptions,
                            dev.user,
                            [NSString stringWithCString:HABase64 encoding:NSUTF8StringEncoding],
                            [NSString stringWithCString:nonce_base64 encoding:NSUTF8StringEncoding],
                            [NSString stringWithCString:created encoding:NSUTF8StringEncoding]];
    NSData *requestBodyData = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody : requestBodyData ];
    [request setTimeoutInterval:REQUEST_TIMEOUT];
    
    // Create url connection and fire request
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)onvif_GetStreamUri:(NSString*)profile_token
{
    unsigned char nonce[NONCE_LEN];
    char nonce_base64[NONCE_BASE64_LEN];
    char created[MAX_CREATED_LEN];
    unsigned char HA[SOAP_SMD_SHA1_SIZE];
    char HABase64[29];
    
    gen_nonce(nonce, NONCE_LEN);
    get_created(created, MAX_CREATED_LEN);
    calc_digest(created, nonce, NONCE_LEN, [dev.password UTF8String], HA);
    soap_s2base64(nonce, nonce_base64, NONCE_LEN);
    soap_s2base64(HA, HABase64, SOAP_SMD_SHA1_SIZE);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:dev.media_uri]];
    
    // Specify that it will be a POST request
    request.HTTPMethod = @"POST";
    
    // This is how we set header fields
    [request setValue:@"application/soap+xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    // Convert your data and set your request's HTTPBody property
    NSString *stringData = [NSString stringWithFormat:httpGetStreamUriContent,
                            dev.user,
                            [NSString stringWithCString:HABase64 encoding:NSUTF8StringEncoding],
                            [NSString stringWithCString:nonce_base64 encoding:NSUTF8StringEncoding],
                            [NSString stringWithCString:created encoding:NSUTF8StringEncoding],
                            profile_token];
    NSData *requestBodyData = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody : requestBodyData];
    [request setTimeoutInterval:REQUEST_TIMEOUT];
    
    // Create url connection and fire request
    //SAVE_FREE(conn);
    //conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [NSURLConnection connectionWithRequest:request delegate:self];
}

- (BOOL)parseStreamUriXml:(NSString*)xmlString
{
    NSRange range1 = [xmlString rangeOfString:@"<trt:MediaUri>" options:NSCaseInsensitiveSearch];
    NSRange range2 = [xmlString rangeOfString:@"</trt:MediaUri>" options:NSCaseInsensitiveSearch];
    if (range1.location == NSNotFound || range2.location == NSNotFound)
        return false;
    NSRange range = { range1.location+range1.length, range2.location-range1.location-range1.length };
    range1 = [xmlString rangeOfString:@"<tt:Uri>" options:NSCaseInsensitiveSearch range:range];
    range2 = [xmlString rangeOfString:@"</tt:Uri>" options:NSCaseInsensitiveSearch range:range];
    if (range1.location == NSNotFound || range2.location == NSNotFound)
        return false;
    range = NSMakeRange(range1.location+range1.length, range2.location-range1.location-range1.length);
    if (onvif_request == ONVIF_REQUEST_GET_MAIN_STREAMURI)
    {
        dev.main_stream_uri = [xmlString substringWithRange:range];
        if ([dev.main_stream_uri rangeOfString:@"&amp;"].location != NSNotFound) {        //special case for dahua
            dev.main_stream_uri = [dev.main_stream_uri stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        }
        NSLog(@"[WS] main_stream_uri:%@", dev.main_stream_uri);
    }
    else
    {
        dev.sub_stream_uri = [xmlString substringWithRange:range];
        if ([dev.sub_stream_uri rangeOfString:@"&amp;"].location != NSNotFound) {
            dev.sub_stream_uri = [dev.sub_stream_uri stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        }
        NSLog(@"[WS] sub_stream_uri:%@", dev.sub_stream_uri);
    }
    return true;
}

#pragma mark - NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{   // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    SAVE_FREE(_responseData);
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{   // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse
{   // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{   // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    NSString *xmlString = [[[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding] autorelease];
    DBGLog(@"[WS] response=\n%@\n",xmlString);
    [NSThread sleepForTimeInterval:1.0f];
    switch (onvif_request)
    {
        case ONVIF_REQUEST_NONE:
            break;
        case ONVIF_REQUEST_GET_CAPABILITIES:
            if ([self parseCapabilitiesXml:xmlString])
            {
                onvif_request = ONVIF_REQUEST_GET_PROFILES;
                [self onvif_GetProfiles];
            }
            else
            {
                [aTarget getDeviceInfomatiionNotify:message withDev:nil];
            }
            break;
        case ONVIF_REQUEST_GET_PROFILES:
            if ([self parseProfilesXml:xmlString])
            {
                onvif_request = ONVIF_REQUEST_GET_MAIN_STREAMURI;
                [self onvif_GetStreamUri:dev.main_profile_token];
            }
            else
            {
                [aTarget getDeviceInfomatiionNotify:message withDev:nil];
            }
            break;
        case ONVIF_REQUEST_GET_MAIN_STREAMURI:
            if ([self parseStreamUriXml:xmlString])
            {
                onvif_request = ONVIF_REQUEST_GET_SUB_STREAMURI;
                [self onvif_GetStreamUri:dev.sub_profile_token];
            }
            else
            {
                [aTarget getDeviceInfomatiionNotify:message withDev:nil];
            }
            break;
        case ONVIF_REQUEST_GET_SUB_STREAMURI:
            if ([self parseStreamUriXml:xmlString])
            {
                [aTarget getDeviceInfomatiionNotify:nil withDev:dev];
            }
            else
            {
                [aTarget getDeviceInfomatiionNotify:message withDev:nil];
            }
            break;
            
        default:
            break;
    }
}

#pragma mark - NSURLSessionDataDelegate

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    SAVE_FREE(_responseData);
    _responseData=[[NSMutableData alloc] init];
    [_responseData setLength:0];
    
    completionHandler(NSURLSessionResponseAllow);
}

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
   didReceiveData:(NSData *)data {
    
    [_responseData appendData:data];
}

#pragma mark - NSURLSessionTaskDelegate

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if (error) {
        // Handle error
    }
    else {
        // perform operations for the  NSDictionary response
        NSString *xmlString = [[[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding] autorelease];
        NSLog(@"[WS] response=\n%@\n",xmlString);
        switch (onvif_request)
        {
            case ONVIF_REQUEST_NONE:
                break;
            case ONVIF_REQUEST_GET_CAPABILITIES:
                if ([self parseCapabilitiesXml:xmlString])
                {
                    onvif_request = ONVIF_REQUEST_GET_PROFILES;
                    [self onvif_GetProfiles];
                }
                else
                {
                    [aTarget getDeviceInfomatiionNotify:message withDev:nil];
                }
                break;
            case ONVIF_REQUEST_GET_PROFILES:
                if ([self parseProfilesXml:xmlString])
                {
                    onvif_request = ONVIF_REQUEST_GET_MAIN_STREAMURI;
                    [self onvif_GetStreamUri:dev.main_profile_token];
                }
                else
                {
                    [aTarget getDeviceInfomatiionNotify:message withDev:nil];
                }
                break;
            case ONVIF_REQUEST_GET_MAIN_STREAMURI:
                if ([self parseStreamUriXml:xmlString])
                {
                    onvif_request = ONVIF_REQUEST_GET_SUB_STREAMURI;
                    [self onvif_GetStreamUri:dev.sub_profile_token];
                }
                else
                {
                    [aTarget getDeviceInfomatiionNotify:message withDev:nil];
                }
                break;
            case ONVIF_REQUEST_GET_SUB_STREAMURI:
                if ([self parseStreamUriXml:xmlString])
                {
                    [aTarget getDeviceInfomatiionNotify:nil withDev:dev];
                }
                else
                {
                    [aTarget getDeviceInfomatiionNotify:message withDev:nil];
                }
                break;
                
            default:
                break;
        }
    }
}

@end
