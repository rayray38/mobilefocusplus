//
//  OnvifStreamReceiver.h
//  EFSideKick
//
//  Created by Ray Lin on 2015/3/20.
//  Copyright (c) 2015年 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StreamReceiver.h"
#import "RTSPClientSession.h"

@interface OnvifStreamReceiver : StreamReceiver<RTSPSubsessionDelegate>
{
    NSString *machineName;
    BOOL	 isEstablishTag;
    
    RTSPClientSession* rtspClientSession;
    NSArray* subsessions;
    char exit;
    
    int seq;
    uint8_t             scH264[4];
    NSMutableData       *keyBuf[MAX_SUPPORT_DISPLAY];
    int                 sequence[MAX_SUPPORT_DISPLAY];
    uint8_t             startBuf[4];
    
    VIDEO_PACKAGE spsPkg;
    VIDEO_PACKAGE ppsPkg;
    VIDEO_PACKAGE iframePkg;
    VIDEO_PACKAGE pframePkg;
    
    int audioSize;
    
    NSMutableArray *targetLine;
    NSMutableArray *targetInfo;
    NSString  *sdpPath;
    
    NSInteger iPTZCHMask ; // add by robert hsu 20110928 for keep enable PTZ channel mask
    NSInteger iCurrentStream;
    BOOL blnGetIDR;
    BOOL blnIsFirstIframe;
}
@end
