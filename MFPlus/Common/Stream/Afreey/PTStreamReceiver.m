//
//  PTStreamReceiver.m
//  EFViewerHD
//
//  Created by James Lee on 13/8/19.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import "PTStreamReceiver.h"
#import "PTPTZController.h"

@implementation PTStreamReceiver
#pragma mark - Streaming Control

- (id)initWithDevice:(Device *)device
{
    outputList = [[NSMutableArray alloc] init];
    NSString *nan = [NSString stringWithFormat:@"99"];
    for (NSInteger i=0; i<4; i++)
        [outputList addObject:nan];
    
    machineName = [device.name copy];
    
    return [super initWithDevice:device];
}

- (BOOL)startLiveStreaming:(NSUInteger)channel {
	
	BOOL ret = YES;
	
	if ( ![super startLiveStreaming:channel] )
	{
		ret =  NO;
		goto Exit;
	}
	
	// set initial parameters
	seq = 0;
	startBuf[0] = 0x00;
	startBuf[1] = 0x00;
	startBuf[2] = 0x00;
	startBuf[3] = 0x01;
	
	// set available channels
	validChannel = 3; // 0011 // no use
    iMaxSupportChannel = 1;
	
	///// get info /////
    if(![self getDeviceInfo]) {
        
        ret = NO;
        goto Exit;
    }
    
	///// RTSP /////
    // init RTSP client
    //modify by robert hsu 20120203 for add rtsp port
	rtspClientSession = [[RTSPClientSession alloc]
						 initWithURL:[NSString stringWithFormat:@"rtsp://%@:%ld/%@",[self getIp],(long)iRtspPort,sdpPath]
						 user:user passwd:passwd];
	
    //iChannel means steam id not channel id
    currentCHMask = channel;//get really current channel add by robert hsu 20110928
    
	// get SDP and create session
	if (![rtspClientSession setup])
	{
		self.errorDesc = NSLocalizedString(@"MsgRtspErr1", nil);
        
        [rtspClientSession release];
        rtspClientSession = nil;
        
		ret =  NO;
		goto Exit;
	}
	
	// get subsession
	subsessions = [[rtspClientSession getSubsessions] retain];
	if([subsessions count]==0)
	{
		self.errorDesc = NSLocalizedString(@"MsgRtspErr2", nil);
		ret =  NO;
		goto Exit;
	}
	
	// setup subsession
    BOOL videoAvailable = NO;
	for (int i=0; i<2; i++) {  //modify by James Lee 20120723 for Dyna IPCAM
#ifndef _NEW_RTSP
        if (![rtspClientSession setupSubsession:[subsessions objectAtIndex:i] useTCP:YES])//modify by robert hsu 2012.02.01 for use over tcp
		{
			self.errorDesc = NSLocalizedString(@"MsgRtspErr3", nil);
			ret =  NO;
			goto Exit;
		}
#endif
		RTSPSubsession* subsession = [subsessions objectAtIndex:i];
		NSLog(@"[PT] Subsession[%d] %@: MediumName=%@ CodecName=%@",
			  i, [subsession getSessionId], [subsession getMediumName], [subsession getCodecName]);
        
        if ([[subsession getMediumName] isEqual:@"video"]) {
            videoAvailable = YES;
        }
		
		[[subsessions objectAtIndex:i] setDelegate:(id)self];
	}
    
    if(!videoAvailable)
	{
		self.errorDesc = NSLocalizedString(@"MsgRtspErr4", nil);
		ret =  NO;
		goto Exit;
	}
	
	// play
	[rtspClientSession play];
	blnReceivingStream = YES;
	
    // initiate PTZ controller
	if (ptzController != nil) {
        
        [ptzController release];
        ptzController = nil;
    }
	ptzController = [[PTPTZController alloc] initWithHost:host sid:nil toKen:nil];
    ptzController.speed = 5;
    
    liveTimer = [NSTimer timerWithTimeInterval:5.0
                                        target:self
                                      selector:@selector(keepAlive)
                                      userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:liveTimer forMode:NSRunLoopCommonModes];
    
	// run loop
	exit = 0;
	[rtspClientSession runEventLoop:&exit];
	
Exit:
	blnReceivingStream = NO;
	
	return ret;
}

- (void)stopStreamingByView:(NSInteger)vIdx {
	
	exit = 1;
	[liveTimer invalidate];
	[super stopStreamingByView:vIdx];
    
    if (rtspClientSession) {
        
        BOOL ret = [rtspClientSession teardown];
        if (!ret) {
            NSLog(@"[PT] teardown failed");
        }
    }
    
    blnConnectionClear = YES;
}

- (void)changeLiveChannel:(NSInteger)channel
{
    // no-op
}

- (void)openSound {
    
	// create audio decoder
    if (audioControl!=nil) {
        
        NSLog(@"[PT] Open sound");
        if (audioControl.codecId != AV_CODEC_ID_PCM_MULAW) {
            
            [audioControl close];
            [audioControl initWithCodecId:AV_CODEC_ID_PCM_MULAW srate:8000 bps:8 balign:audioSize fsize:audioSize channel:1];
            audioControl.codecId = audioControl.amFlag = AV_CODEC_ID_PCM_MULAW;
            NSLog(@"[PT] Create new audio decoder with codec id %d", AV_CODEC_ID_PCM_MULAW);
        }
        
        [audioControl start];
        audioOn = YES;
    }
}

- (void)closeSound {
	
	NSLog(@"[PT] close sound");
	
	[audioControl stop];
    
    audioOn = NO;
}

- (void)keepAlive
{
    if (!m_bGetFrame) {
        NSLog(@"[PT] connection loss");
        [liveTimer invalidate];
        [self stopStreamingByView:0];
    }
    m_bGetFrame = NO;
}

- (void)dealloc
{
    if (ppsPkg.data != nil) {
        free(ppsPkg.data);
    }
    if (spsPkg.data != nil) {
        free(spsPkg.data);
    }
    if (iframePkg.data != nil) {
        free(iframePkg.data);
    }
    if (pframePkg.data != nil) {
        free(pframePkg.data);
    }
    if (machineName != nil) {
        
        [machineName release];
        machineName = nil;
    }
    if (rtspClientSession != nil) {
        
        [rtspClientSession release];
        rtspClientSession = nil;
    }
    if (subsessions != nil) {
        
        [subsessions release];
        subsessions = nil;
    }
	
	[super dealloc];
}

#pragma mark - Parser Delegate

- (BOOL)getDeviceInfo
{
    BOOL ret = YES;
    
    NSString *cmd = [NSString stringWithFormat:@"cgi-bin/getparam.cgi"];
	[cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
	// connection failed
	if ( errorDesc!=nil ) {
		
		ret =  NO;
		goto Exit;
	}
	
	if ( blnStopStreaming ) {
		
        ret = NO;
		goto Exit;
	}
	
	// parse response
	NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	//NSLog(@"response:%@",response);
    if(dualStream)
        //sdpPath = [self getStringFromLine:response targetString:@"rtspAvStream2" withQuote:@"\""];
        sdpPath = @"live3.sdp";
    else
        sdpPath = [self getStringFromLine:response targetString:@"rtspAvStream1" withQuote:@"\""];
	[response release];
    
    cmd = [NSString stringWithFormat:@"cgi-bin/getsystem.cgi"];
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
    // connection failed
    if ( errorDesc!=nil ) {
        
        ret =  NO;
        goto Exit;
    }
    
    if ( blnStopStreaming ) {
        
        ret = NO;
        goto Exit;
    }
    // parse response
    response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //NSLog(@"response:%@",response);
    m_DiskGMT = [[self getStringFromLine:response targetString:@"timezone" withQuote:@""] copy];
    [response release];
    
Exit:
	blnReceivingStream = NO;
	
	return ret;
}

#pragma mark - Connection Delegate

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.
{
	if ( theConnection==requestConnection ) {
		
		[responseData appendData:data];
	}
}

#pragma mark - RTSP delegate

- (void)didReceiveMessage:(NSString *)message
{
}

- (void)didReceiveFrame:(const uint8_t*)frameData
		frameDataLength:(int)frameDataLength
	   presentationTime:(struct timeval)presentationTime
 durationInMicroseconds:(unsigned)duration
			 subsession:(RTSPSubsession*)subsession
{
    // WatchDog
    m_bGetFrame = YES;
    
	// Video frame
	if ([[subsession getMediumName] isEqual:@"video"]) {
		
		// build video package
		VIDEO_PACKAGE videoPkg;
		
		videoPkg.sec	= presentationTime.tv_sec + ([m_DiskGMT integerValue]*15 - 720)*60;
		videoPkg.ms		= presentationTime.tv_usec/1000;
		videoPkg.width	= 0; // don't care
		videoPkg.height	= 0; // don't care
        
        NSInteger iBufferIdx = [[outputList objectAtIndex:0] integerValue];
        
		if ([[subsession getCodecName] isEqual:@"H264"]) { // H264
			
			videoPkg.codec	= AV_CODEC_ID_H264;
			
			if ( (frameData[0] & 0X1F) == 0X7) { // SPS
                
                if (spsPkg.data != NULL) {
                    free(spsPkg.data);
                }
				spsPkg.size	= sizeof(startBuf)+frameDataLength;
				spsPkg.data	= malloc(spsPkg.size);
				memcpy((spsPkg.data), startBuf, sizeof(startBuf));
				memcpy((spsPkg.data+sizeof(startBuf)), frameData, frameDataLength);
#ifdef H264DEBUG
                NSLog(@"[264] Get SPS ---- frameData[0]:%x, size:%ld, startBuf:%lu, frameDataLength:%d",frameData[0],spsPkg.size,sizeof(startBuf),frameDataLength);
#endif
                blnIsFirstIframe = NO;
                return;
                
			}else if ((frameData[0] & 0X0F) == 0X8) { // PPS
                
                if (ppsPkg.data != NULL) {
                    free(ppsPkg.data);
                }
                ppsPkg.size	= sizeof(startBuf)+frameDataLength;
				ppsPkg.data	= malloc(ppsPkg.size);
				memcpy((ppsPkg.data), startBuf, sizeof(startBuf));
				memcpy((ppsPkg.data+sizeof(startBuf)), frameData, frameDataLength);
#ifdef H264DEBUG
                NSLog(@"[264] Get PPS ---- frameData[0]:%x, size:%ld, startBuf:%lu, frameDataLength:%d",frameData[0],ppsPkg.size,sizeof(startBuf),frameDataLength);
#endif
                return;
                
			}else if ((frameData[0] & 0X1F) == 0X5 ) { //I frame
                
                seq = 0;
                videoPkg.seq	= seq;
                videoPkg.type	= VO_TYPE_IFRAME;
                videoPkg.size	= spsPkg.size + ppsPkg.size + sizeof(startBuf)+frameDataLength;                         //Single
                videoPkg.data	= malloc(videoPkg.size);
                memcpy((videoPkg.data), spsPkg.data, spsPkg.size);
                memcpy((videoPkg.data+spsPkg.size), ppsPkg.data, ppsPkg.size);
                memcpy((videoPkg.data+spsPkg.size+ppsPkg.size), startBuf, sizeof(startBuf));                            //Single
                memcpy((videoPkg.data+spsPkg.size+ppsPkg.size+sizeof(startBuf)), frameData, frameDataLength);           //Single
#ifdef H264DEBUG
                NSLog(@"[264] Get I frame ---- frameData[0]:%x, size:%ld, startBuf:%lu, frameDataLength:%d",frameData[0],videoPkg.size,sizeof(startBuf),frameDataLength);
#endif
			}
			else if ( (frameData[0] & 0X1F) == 0X1) { // P frame
				
                seq++;
                videoPkg.seq	= seq;
                videoPkg.type	= VO_TYPE_PFRAME;
                videoPkg.size	= sizeof(startBuf)+frameDataLength;
                videoPkg.data	= malloc(videoPkg.size);
                memcpy(videoPkg.data, startBuf, sizeof(startBuf));
                memcpy((videoPkg.data+sizeof(startBuf)), frameData, frameDataLength);
#ifdef H264DEBUG
                NSLog(@"[264] Get P frame ---- frameData[0]:%x, size:%ld seq:%d",frameData[0],videoPkg.size,seq);
#endif
            }
			else {
				NSLog(@"[PT] Unknown %@ frame type", [subsession getCodecName]);
				return;
			}
		} else if ([[subsession getCodecName] isEqual:@"JPEG"]) { // JPEG
			
			videoPkg.codec	= AV_CODEC_ID_MJPEG;
			
			seq = 0;
			videoPkg.seq	= seq;
			videoPkg.type	= VO_TYPE_IFRAME;
			videoPkg.size	= frameDataLength;
			videoPkg.data	= malloc(videoPkg.size);
			memcpy(videoPkg.data, frameData, frameDataLength);
            
		} else if ([[subsession getCodecName] isEqual:@"MP4V-ES"]){ // MPEG4
            
            if ((frameData[4] & 0x40) == 0) {
                videoPkg.codec = AV_CODEC_ID_MPEG4;
                seq = 0;
                videoPkg.seq	= seq;
                videoPkg.type	= VO_TYPE_IFRAME;
                videoPkg.size	= frameDataLength;
                videoPkg.data	= malloc(videoPkg.size);
                memcpy(videoPkg.data, frameData, frameDataLength);
#ifdef H264DEBUG
                NSLog(@"[264] Get Mpeg I-Frame:%d",frameDataLength);
#endif
            }else if ((frameData[4] & 0x40) == 0x40) {
                seq++;
                videoPkg.codec = AV_CODEC_ID_MPEG4;
                videoPkg.seq	= seq;
                videoPkg.type	= VO_TYPE_PFRAME;
                videoPkg.size	= frameDataLength;
                videoPkg.data	= malloc(videoPkg.size);
                memcpy(videoPkg.data, frameData, frameDataLength);
#ifdef H264DEBUG
                NSLog(@"[264] Get Mpeg P-Frame:%d",frameDataLength);
#endif
            }else {
                NSLog(@"[PT] Unknown %@ frame type", [subsession getCodecName]);
				return;
            }
            
        } else {
			
			NSLog(@"[PT] Unknown codec type: %@", [subsession getCodecName]);
			return;
		}
        
        if (![machineName isEqualToString:[self.videoControl.aryTitle objectAtIndex:iBufferIdx]])
            [self.videoControl setTitleByCH:iBufferIdx title:machineName];
        
		// put package to VideoOutput buffer
        [videoControl putFrameIntoBufferByCH:&videoPkg chIndex:iBufferIdx];
		
		// update frame size (get from decoder)
		frameWidth	= [videoControl getVideoWidthbyCH:0];
		frameHeight = [videoControl getVideoHeightbyCH:0];
	}
	
	// Audio frame
	else if ([[subsession getMediumName] isEqual:@"audio"]) {
        
		if (audioControl!=nil && audioOn) {
			[audioControl playAudio:(Byte *)frameData length:frameDataLength];
		}
		else {
			//NSLog(@"[PT] Receive audio (OFF)");
		}
#ifdef DUMPAUDIO
        NSError *error = nil;
        NSString *path = [NSString stringWithFormat:@"/Users/EverFocus/Desktop/AAC/%u.aac",frameDataLength];
        NSData *data = [NSData dataWithBytes:(Byte *)frameData length:frameDataLength];
        BOOL ret = [data writeToFile:path options:NSDataWritingAtomic error:&error];
        if(!ret)
            NSLog(@"Write returned error: %@", [error localizedDescription]);
#endif
	}
	
	else {
		NSLog(@"[PT] Unknown medium name: %@", [subsession getMediumName]);
	}
}
@end
