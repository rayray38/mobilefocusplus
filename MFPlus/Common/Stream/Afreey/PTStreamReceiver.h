//
//  PTStreamReceiver.h
//  EFViewerHD
//
//  Created by James Lee on 13/8/19.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StreamReceiver.h"
#import "RTSPClientSession.h"

@interface PTStreamReceiver : StreamReceiver<RTSPSubsessionDelegate>
{
    NSString *machineName;
	BOOL	 isEstablishTag;
	
	RTSPClientSession* rtspClientSession;
	NSArray* subsessions;
	char exit;
	
	int seq;
	uint8_t startBuf[4];
    
    VIDEO_PACKAGE spsPkg;
    VIDEO_PACKAGE ppsPkg;
    VIDEO_PACKAGE iframePkg;
    VIDEO_PACKAGE pframePkg;
	
	int audioSize;
    
    NSMutableArray *targetLine;
    NSMutableArray *targetInfo;
    NSString  *sdpPath;
    
    NSInteger iPTZCHMask ; // add by robert hsu 20110928 for keep enable PTZ channel mask
    NSInteger iCurrentStream;
    BOOL blnIsFirstIframe;
}

@end
