//
//  PTPTZController.m
//  EFViewerHD
//
//  Created by James Lee on 13/8/20.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import "PTPTZController.h"

@implementation PTPTZController
#pragma mark -
#pragma mark CGI Command

- (void)ptzTiltDown:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/camctrl.cgi?move=5&speedpan=30&domespeedpan=3&speedtilt=30"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzTiltUp:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/camctrl.cgi?move=1&speedpan=30&domespeedpan=3&speedtilt=30"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzPanLeft:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/camctrl.cgi?move=2&speedpan=30&domespeedpan=3&speedtilt=30"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzPanRight:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/camctrl.cgi?move=4&speedpan=30&domespeedpan=3&speedtilt=30"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzZoomIn:(NSInteger)ch
{
    NSString *urlString = [NSString stringWithFormat:@"cgi-bin/ccdctrl.cgi?move=1&speedZoom=3"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzZoomOut:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/ccdctrl.cgi?move=2&speedZoom=3"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzZoomStop:(NSInteger)ch
{    
}

- (void)ptzFocusFar:(NSInteger)ch
{
    NSString *urlString = [NSString stringWithFormat:@"cgi-bin/ccdctrl.cgi?move=4&speedFocus=3"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzFocusNear:(NSInteger)ch
{
    NSString *urlString = [NSString stringWithFormat:@"cgi-bin/ccdctrl.cgi?move=3&speedFocus=3"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzFocusStop:(NSInteger)ch
{
}

- (void)ptzIrisOpen:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/ccdctrl.cgi?move=5"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzIrisClose:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/ccdctrl.cgi?move=6"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzStop:(NSInteger)ch
{
}

- (void)ptzPresetGo:(NSInteger)ch value:(NSInteger)value
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/camctrl.cgi?recall=%ld",(long)value];
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzPresetSet:(NSInteger)ch value:(NSInteger)value
{
//	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/setpre.cgi?index=%d&name=noname",value];
//	
//	[self cancelConnection];
//	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzPresetCancel:(NSInteger)ch value:(NSInteger)value
{
//	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/deletepreset.cgi?num=%d",value];
//	
//	[self cancelConnection];
//	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzAutoPanRun:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/camctrl.cgi?auto=6&speedapp=1"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzAutoPan360Run:(NSInteger)ch
{
//	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/camctrl.cgi?speedpan=25&speedapp=3"];
//	
//	[self cancelConnection];
//	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzAutoPanStop:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/camctrl.cgi?auto=7"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzPattern:(NSInteger)ch value:(NSInteger)value
{
//	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/com/ptz.cgi?gotoserversequenceno=1"];
//	
//	[self cancelConnection];
//	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzTour:(NSInteger)ch value:(NSInteger)value
{
//	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/com/ptz.cgi?gotoservercruiseno=1"];
//	
//	[self cancelConnection];
//	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

@end