//
//  StreamReceiver.m
//  EFViewerHD
//
//  Created by James Lee on 12/10/9.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "StreamReceiver.h"
#import "netdb.h"

@implementation StreamReceiver

@synthesize host,user,passwd,errorDesc,dualStream,userLevel,validChannel,validPlaybackCh,validPtzChannel,iMaxSupportChannel,iRtspPort,streamType,audioCodec,deviceId;
@synthesize videoDisplayViews,currentCHMask,currentPlayCH,frameHeight,frameWidth,cameraList,outputList,channelList,videoControl,audioControl,audioSender,ptzController;
@synthesize blnReceivingStream,blnResetStreaming,blnConnectionClear,blnChSupportPTZ,blnIsAudioEnable,audioOn,speakOn,m_blnPlaybackMode,blnGetChannelInfo,blnIsMJPEG;
@synthesize m_intDiskStartTime,m_intDiskEndTime,m_DiskGMT,m_CurrentPlaybackTime,m_intPlaybackStartTime,m_SpeedIndex,m_intdlsEnable,m_intCurrentTime,m_DiskGMT_sec,m_dst_time_sec;
@synthesize m_intStartMonth,m_intStartWeek,m_intStartWeekday,m_intStartHour,m_intStartMin,m_intSetHour,m_intSetMin,m_intEndMonth,m_intEndWeek,m_intEndWeekday,m_intEndHour,m_intEndMin;
@synthesize m_SearchID,m_intSearchResultTag,m_intSearchProgress,SearchResultlist,blnGetSearchInfo;
@synthesize delegate,m_stream_uri,blnCloseNotify,event,blnEventMode,xmsUUID,m_blnEventSearchRS,eventSearchCH;
@synthesize blnVehChannel,blnDvrChannel,vehicleList,v_channelList,sessionId,currentCHList,httpPort;


#pragma mark - Initialization

- (id)initWithDevice:(Device *)device
{
	if ((self = [super init])) {
		
		// set device parameters
		self.user	= [device.user copy];
		self.passwd = [device.password copy];
		self.dualStream = device.dualStream;
        self.iRtspPort = device.rtspPort;//add by robert hsu 20120203
        self.streamType = device.streamType;
        self.deviceId = device.rowID;
        self.m_stream_uri = (device.type == RTSP ? device.ip : device.main_stream_uri);
        self.httpPort = device.port;
		
		if ( [device.ip rangeOfString:@".everfocusddns.com" options:NSCaseInsensitiveSearch].location!=NSNotFound ) {
			
			self.host = [NSString stringWithFormat:@"%@",device.ip];
		}
		else {
			
			self.host = [NSString stringWithFormat:@"%@:%ld",device.ip,(long)device.port];
		}
        
        self.blnCloseNotify = (device.blnCloseNotify == 0 ? NO : YES);
		
		// set initial status
		self.blnReceivingStream = NO;
		
		// init cmdSender
		cmdSender = [[CommandSender alloc] initWithHost:self.host delegate:self];
		
		// init parameters
		validChannel = 0;
        validPlaybackCh = 0;
        validPtzChannel = 0;
		userLevel = -1; // determine if have login device
		frameWidth	= 0;
		frameHeight = 0;
		blnFirstConnection = YES;
        DeviceType = device.type;   // add by robert hsu 20110926
        videoControl = nil;
        blnChSupportPTZ = NO;       //add by robert hsu 20110927
//        cameraList = nil;
        blnConnectionClear = NO;
        m_bGetFrame = NO;
        blnIsMJPEG = NO;
        
        user_copy = [self.user copy];
        passwd_copy = [self.passwd copy];
	}
    MMLog(@"[SR] Init %@",self);
	return self;
}

#pragma mark - Streaming Control

- (BOOL)startLiveStreaming:(NSUInteger)mask {
	
	// set parameters
	currentCHMask = mask;
	frameWidth	= 0;
	frameHeight = 0;
    
	self.errorDesc = nil;
	
	if ( errorDesc!=nil || blnStopStreaming ) {
		
		return NO;
	}
    
	return YES;
}

- (BOOL)startPlaybackStreaming:(NSUInteger)mask
{
    // set parameters
	currentCHMask = mask;
	frameWidth	= 0;
	frameHeight = 0;
    
	self.errorDesc = nil;
	
	if ( errorDesc!=nil || blnStopStreaming ) {
		
		return NO;
	}
    
	return YES;
}

- (void)changeLiveChannel:(NSInteger)channel
{
}

- (void)stopStreaming
{
	if ( blnStopStreaming ) return;
    blnStopStreaming = YES;
	
	if (dnsConnection != nil) {
		
        [dnsConnection cancel];
        dnsConnection = nil;
    }
	
	if (requestConnection != nil) {
		
        [requestConnection cancel];
        requestConnection = nil;
    }
	
	if (streamConnection != nil) {
		
        [streamConnection cancel];
        streamConnection = nil;
    }
    
	// wait for streaming stop
	while ( self.blnReceivingStream ) 
		[NSThread sleepForTimeInterval:0.001];
    
    [videoControl stop];
    blnConnectionClear = YES;
    DBGLog(@"[SR] stopStreaming");
}

- (void)stopStreamingByView:(NSInteger)vIdx
{
	if ( blnStopStreaming ) return;
    //blnStopStreaming = YES;
	
	if (dnsConnection != nil) {
		
        [dnsConnection cancel];
        dnsConnection = nil;
    }
	
	if (requestConnection != nil) {
		
        [requestConnection cancel];
        requestConnection = nil;
    }
	
	if (streamConnection != nil) {
		
        [streamConnection cancel];
        streamConnection = nil;
    }
    
	// wait for streaming stop
	while ( self.blnReceivingStream )
		[NSThread sleepForTimeInterval:0.001];
    
    if (videoControl == nil) {
        DBGLog(@"videocontrol is nil");
    }
    else
        [videoControl stopByCH:vIdx];
    DBGLog(@"[SR] stopStreamingByView[%ld]",(long)vIdx);
}

- (BOOL)getDeviceInfo
{
	self.blnReceivingStream = YES;
	blnStopStreaming = NO;
    blnResetStreaming = NO;
    cameraList = nil;
    
	[self getHostAndPortFromDNSIfNeed];
    if ( errorDesc!=nil || blnStopStreaming || !blnGetDNS) {
		
        if (!blnGetDNS) {
            errorDesc = NSLocalizedString(@"MsgDnsFailed", nil);
        }
		return NO;
    }
    
    return YES;
}

- (BOOL)getSupportPTZ:(NSInteger)channel
{
    BOOL ret = NO;
    
    return ret;
}

- (void)openSound
{
}

- (void)closeSound
{
}

- (void)openSpeak
{
    [audioSender start];
    speakOn = YES;
}

- (void)closeSpeak
{
    [audioSender stop];
    speakOn = NO;
}

- (void)changeAudioChannel
{
}

- (void)changeViewMode:(BOOL)tag
{
}

- (void)changeVideoMask:(NSUInteger)mask
{
}

- (void)changeVideoResolution:(NSUInteger)mask video:(NSInteger)type
{
}

- (void)getSearchIDFrom:(NSInteger)start To:(NSInteger)end byFilter:(NSInteger)mask
{
}

- (void)getEventResult
{
}

- (void)cancelEventSearch
{
}

- (void)changePlaybackMode:(NSInteger)mode withValue:(NSInteger)value
{
}

- (void)keepAlive
{
}

- (void)getVehicleStatus:(CameraInfo *)vehicle
{
}

- (void)registDevTokenToServer:(NSArray *)devArray
{
    NSString *cmd,*strToken;
    NSDictionary *response;
    NSInteger iErrorCode = 0;
    NSString *devToken = [devArray objectAtIndex:0];     //device token
    NSString *devName = [devArray objectAtIndex:1];      //device name
    SAVE_FREE(xms_version);
    SAVE_FREE(tokenInfo);
    
    //Check is ddns or not
    [self getHostAndPortFromDNSIfNeed];
    if ( errorDesc!=nil || blnStopStreaming || !blnGetDNS) {
        
        if (!blnGetDNS) {
            errorDesc = NSLocalizedString(@"MsgDnsFailed", nil);
            [self showAlarm:errorDesc];
        }
        return;
    }

    //First, login and get tokenInfo
    blnGetTokenInfo = YES;
    cmd = [NSString stringWithFormat:@"Login"];
    [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES withuser:self.user withpwd:self.passwd];
    
    // connection failed
    if (errorDesc!=nil || blnStopStreaming) {
        
        iErrorCode = -2;
        if (!self.blnCloseNotify)
            [self showAlarm:@"Login failed"];//NSLocalizedString(@"MsgNotifyFailed", nil)];
        return;
    }
    response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
    if (!JSON_SUCCESS(response)) { //check json valid or not
        
        iErrorCode = -2;
        return;
    }
    
    [self parseJSonData:response];
    
    blnGetTokenInfo = NO;
    
    strToken = [NSString stringWithFormat:@"Bearer %@",tokenInfo];
    
    blnGetVersion = YES;
    cmd = [NSString stringWithFormat:@"api/Device/DeviceInfo"];
    [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES tokenInfo:strToken deviceToken:nil deviceName:nil];
    
    // connection failed
    if (errorDesc!=nil || blnStopStreaming) {
        
        iErrorCode = -2;
        //return;
    }
    else {
        response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        DBGLog(@"[XMS] response : %@",response);
        if (!JSON_SUCCESS(response)) { //check json valid or not
            
            iErrorCode = -2;
            //return;
        }
        
        [self parseJSonData:response];
    }
    blnGetVersion = NO;
    
    //Second, check XMS firmware version, only v2.0.3(for MF)/v2.0.5(for MFHD) or above did implement push notification function.
    BOOL valid = NO;
    NSString *appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    if ([appName isEqualToString:@"MobileFocus"] || [appName isEqualToString:@"MobileFocus+"]) {
        valid = [self checkValidVersion_MF:xms_version];
    }
    else
        valid = [self checkValidVersion_MFHD:xms_version];
    
    if (valid) {
        //Third, according to button "receive notify" to decide which url should be send, then put tokenInfo in header and deviceToken in body
        if (self.blnCloseNotify) {
            cmd = [NSString stringWithFormat:@"api/Device/DeleteByMobile"];
            [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES tokenInfo:strToken deviceToken:devToken deviceName:nil];
            
            // connection failed
            if (errorDesc!=nil || blnStopStreaming) {
                
                iErrorCode = -2;
                RELog(@"[SR] Delete deviceToken from XMS failed");
                return;
            }
            else
                RELog(@"[SR] Delete deviceToken from XMS successed");
        }
        else {
            
            blnGetUUID = YES;
            
            cmd = [NSString stringWithFormat:@"api/Device/InsertByMobile"];
            [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES tokenInfo:strToken deviceToken:devToken deviceName:devName];
            
            // connection failed
            if (errorDesc!=nil || blnStopStreaming) {
                
                iErrorCode = -2;
                [self showAlarm:NSLocalizedString(@"MsgNotifyFailed", nil)];
                return;
            }
            else {
                [self showAlarm:[NSString stringWithFormat:@"Regist notify sucess"]];
                RELog(@"[SR] Receive remote notify ON");
            }
            response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
            DBGLog(@"[SR] response : %@",response);
            if (!JSON_SUCCESS(response)) { //check json valid or not
                
                iErrorCode = -2;
                return;
            }
            [self parseJSonData:response];
            
            blnGetUUID = NO;
        }
    }
    else {
        if (!self.blnCloseNotify) {
            if ([appName isEqualToString:@"MobileFocus"] || [appName isEqualToString:@"MobileFocus+"]) {
                [self showAlarm:[NSString stringWithFormat:@"To use Push Notification, please upgrade the firmware version to v2.0.3 or above."]];
            }
            else
                [self showAlarm:[NSString stringWithFormat:@"To use Push Notification, please upgrade the firmware version to v2.0.5 or above."]];
        }
    }
}

- (void)updateDevTokenToServer:(NSArray *)devArray
{
    NSString *cmd,*strToken;
    NSDictionary *response;
    NSString *devToken = [devArray objectAtIndex:0];     //device token
    NSString *devName = [devArray objectAtIndex:1];      //device name
    SAVE_FREE(xms_version);
    SAVE_FREE(tokenInfo);
    
    //Check is ddns or not
    [self getHostAndPortFromDNSIfNeed];
    if ( errorDesc!=nil || blnStopStreaming || !blnGetDNS) {
        
        if (!blnGetDNS) {
            errorDesc = NSLocalizedString(@"MsgDnsFailed", nil);
            [self showAlarm:errorDesc];
        }
        return;
    }
    
    //First, login and get tokenInfo
    blnGetTokenInfo = YES;
    cmd = [NSString stringWithFormat:@"Login"];
    [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES withuser:self.user withpwd:self.passwd];
    
    // connection failed
    if (errorDesc!=nil || blnStopStreaming) {
        
        DBGLog(@"[SR] Login failed");
        return;
    }
    response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
    if (!JSON_SUCCESS(response)) { //check json valid or not
        
        DBGLog(@"[SR] Invalid response");
        return;
    }
    
    [self parseJSonData:response];
    blnGetTokenInfo = NO;
    
    strToken = [NSString stringWithFormat:@"Bearer %@",tokenInfo];
    
    blnGetVersion = YES;
    cmd = [NSString stringWithFormat:@"api/Device/DeviceInfo"];
    [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES tokenInfo:strToken deviceToken:nil deviceName:nil];
    
    // connection failed
    if (errorDesc!=nil || blnStopStreaming) {
        DBGLog(@"[SR] Connection failed");
        return;
    }
    response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
    [self parseJSonData:response];
    blnGetVersion = NO;
    
    //Second, check XMS firmware version, only v2.0.3(for MF)/v2.0.5(for MFHD) or above did implement push notification function.
    BOOL valid = NO;
    NSString *appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    if ([appName isEqualToString:@"MobileFocus"] || [appName isEqualToString:@"MobileFocus+"]) {
        valid = [self checkValidVersion_MF:xms_version];
    }
    else
        valid = [self checkValidVersion_MFHD:xms_version];
    
    if (valid) {
        if (!self.blnCloseNotify) {
            cmd = [NSString stringWithFormat:@"api/Device/InsertByMobile"];
            [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES tokenInfo:strToken deviceToken:devToken deviceName:devName];
            
            // connection failed
            if (errorDesc!=nil || blnStopStreaming) {
                RELog(@"[SR] Update device token failed");
                return;
            }
            else {
                RELog(@"[SR] Update device token succeed");
            }
        }
    }
}

- (BOOL)checkValidVersion_MF:(NSString *)xms_Version
{
    if (!xms_Version) {
        return NO;
    }
    BOOL valid_xms_version = YES;
    
    NSString *lowerString = [xms_Version lowercaseStringWithLocale:[NSLocale currentLocale]];  //20161007 added by Ray Lin, 先將字串全部轉小寫再處理，以免出錯
    NSArray *version_Array = [lowerString componentsSeparatedByString:@"_v"];
    NSString *version_string = [version_Array objectAtIndex:1];
    NSArray *tmpArray = [version_string componentsSeparatedByString:@"."];
    
    if ([[tmpArray objectAtIndex:0] integerValue]>2) {
        return valid_xms_version;
    }
    if ([[tmpArray objectAtIndex:0] integerValue]<2) {
        return valid_xms_version = NO;
    }
    if ([[tmpArray objectAtIndex:1] integerValue]>0) {
        return valid_xms_version;
    }
    if ([[tmpArray objectAtIndex:2] integerValue]<3) {
        return valid_xms_version = NO;
    }
    
    return valid_xms_version;
}

- (BOOL)checkValidVersion_MFHD:(NSString *)xms_Version
{
    if (!xms_Version) {
        return NO;
    }
    BOOL valid_xms_version = YES;
    
    NSString *lowerString = [xms_Version lowercaseStringWithLocale:[NSLocale currentLocale]];  //20161007 added by Ray Lin, 先將字串全部轉小寫再處理，以免出錯
    NSArray *version_Array = [lowerString componentsSeparatedByString:@"_v"];
    NSString *version_string = [version_Array objectAtIndex:1];
    NSArray *tmpArray = [version_string componentsSeparatedByString:@"."];
    
    if ([[tmpArray objectAtIndex:0] integerValue]>2) {
        return valid_xms_version;
    }
    if ([[tmpArray objectAtIndex:0] integerValue]<2) {
        return valid_xms_version = NO;
    }
    if ([[tmpArray objectAtIndex:1] integerValue]>0) {
        return valid_xms_version;
    }
    if ([[tmpArray objectAtIndex:2] integerValue]<5) {
        return valid_xms_version = NO;
    }
    
    return valid_xms_version;
}

- (BOOL)checkXmsVersion_EventQuery:(NSString *)xms_Version
{
    if (!xms_Version) {
        return NO;
    }
    BOOL valid_xms_version = YES;
    
    NSString *lowerString = [xms_Version lowercaseStringWithLocale:[NSLocale currentLocale]];  //20161007 added by Ray Lin, 先將字串全部轉小寫再處理，以免出錯
    NSArray *version_Array = [lowerString componentsSeparatedByString:@"_v"];
    NSString *version_string = [version_Array objectAtIndex:1];
    NSArray *tmpArray = [version_string componentsSeparatedByString:@"."];
    
    if ([[tmpArray objectAtIndex:0] integerValue]>2) {
        return valid_xms_version;
    }
    if ([[tmpArray objectAtIndex:0] integerValue]<2) {
        return valid_xms_version = NO;
    }
    if ([[tmpArray objectAtIndex:1] integerValue]>1) {
        return valid_xms_version;
    }
    if ([[tmpArray objectAtIndex:1] integerValue]<1) {
        return valid_xms_version = NO;
    }
    if ([[tmpArray objectAtIndex:2] integerValue]<1) {
        return valid_xms_version = NO;
    }
    
    return valid_xms_version;
}

- (void)getXmsEventFromLocal:(NSInteger)eventSeq byType:(NSString *)vtype
{
    NSString *cmd,*strToken;
    NSDictionary *response;
    blnStopStreaming = NO;
    
    @try {
        //Check is ddns or not
        [self getHostAndPortFromDNSIfNeed];
        if (errorDesc!=nil || blnStopStreaming || !blnGetDNS) {
            
            if (!blnGetDNS) {
                errorDesc = NSLocalizedString(@"MsgDnsFailed", nil);
                [self showAlarm:errorDesc];
            }
            goto Exit;
        }
        
        //First, login and get tokenInfo
        blnGetTokenInfo = YES;
        cmd = [NSString stringWithFormat:@"Login"];
        [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES withuser:self.user withpwd:self.passwd];
        
        // connection failed
        if (errorDesc!=nil || blnStopStreaming) {
            
            DBGLog(@"[SR] Login failed");
            goto Exit;
        }
        response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        if (!JSON_SUCCESS(response)) { //check json valid or not
            
            DBGLog(@"[SR] Invalid response");
            goto Exit;
        }
        
        [self parseJSonData:response];
        blnGetTokenInfo = NO;
        
        if (!tokenInfo) {
            errorDesc = [NSString stringWithFormat:@"Invalid token"];
            [self showAlarm:errorDesc];
            goto Exit;
        }
        
        strToken = [NSString stringWithFormat:@"Bearer %@",tokenInfo];
        
        blnGetVersion = YES;
        cmd = [NSString stringWithFormat:@"api/Device/DeviceInfo"];
        [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES tokenInfo:strToken deviceToken:nil deviceName:nil];
        
        // connection failed
        if (errorDesc!=nil || blnStopStreaming) {
            DBGLog(@"[SR] Connection failed");
            goto Exit;
        }
        response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        [self parseJSonData:response];
        blnGetVersion = NO;
        
        //Second, check XMS firmware version, only v2.1.1 or above did implement event query function.
        BOOL valid = NO;
        valid = [self checkXmsVersion_EventQuery:xms_version];
        
        if (valid) {
            blnEventQry = YES;
            cmd = [NSString stringWithFormat:@"api/Event/GetMobileEvent"];
            
            if ([vtype rangeOfString:@"normal" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES withToken:strToken withSequence:eventSeq count:DEFAULT_QUERY_EVENT_COUNT eventType:STR_XMS_EVTYPE];
            }
            else
                [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES withToken:strToken withSequence:eventSeq count:DEFAULT_QUERY_EVENT_COUNT eventType:STR_XFLEET_EVTYPE];
            
            // connection failed
            if (errorDesc!=nil || blnStopStreaming) {
                DBGLog(@"[SR] Connection failed");
                goto Exit;
            }
            response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
            [self.delegate xmsEventStatusCallback:response];
            blnEventQry = NO;
        }
    }
    @catch (NSException *exception) {
        DBGLog(@"[SR] === EXCEPTION: %@ ===", exception.reason);
        goto Exit;
    }
    
Exit:
    DBGLog(@"[SR] Stop Query");
}

- (void)dealloc
{
	// release command sender
    SAVE_FREE(cmdSender);
	// release PTZ controller
    SAVE_FREE(ptzController);
	// release audio sender
    SAVE_FREE(audioSender);
    // release output list
    [outputList removeAllObjects];
    SAVE_FREE(outputList);
//    [cameraList removeAllObjects];
//    SAVE_FREE(cameraList);
    [SearchResultlist removeAllObjects];
    SAVE_FREE(SearchResultlist);
    
    audioControl = nil;
    videoControl = nil;
    
	// release others
    SAVE_FREE(host);
    SAVE_FREE(user);
    SAVE_FREE(passwd);
    SAVE_FREE(currentXmlElement);
    SAVE_FREE(responseData);
    SAVE_FREE(sessionId);
    SAVE_FREE(m_SearchID);
    SAVE_FREE(m_DiskGMT);
    
    DBGLog(@"[SR] release %@ - %lu",self,(unsigned long)self.retainCount);
    [super dealloc];
}

#pragma mark - Common Functions

- (NSString *)getIp
{
	NSArray *items = [host componentsSeparatedByString:@":"];
	return [items objectAtIndex:0];
}

- (NSInteger)getPort
{
    NSArray *items = [host componentsSeparatedByString:@":"];
    return [[items objectAtIndex:1] integerValue];
}

- (void)getHostByName
{
    NSString *ip = [self getIp];
    NSInteger port = [self getPort];
    struct hostent *host_entry = gethostbyname(ip.UTF8String);
    if (!host_entry) {
        DBGLog(@"[SR] Get Host By Name Failed");
        return;
    }
    char addr[32];
    inet_ntop(host_entry->h_addrtype, host_entry->h_addr, addr, sizeof(addr));
    NSString *result = [[NSString alloc] initWithUTF8String:addr];
    NSString *newHost = [[NSString alloc] initWithFormat:@"%@:%ld",result,(long)port];
    DBGLog(@"[SR] Get %@ from %@",newHost,host);
    
    // update cmdSender host
    [cmdSender setHost:newHost];
}

- (void)getHostAndPortFromDNSIfNeed
{
    blnGetDNS = YES;
    
    if ( [host rangeOfString:@"dns"].location==NSNotFound ) return;
    
    if ( [host rangeOfString:@".everfocusddns.com" options:NSCaseInsensitiveSearch].location!=NSNotFound ) {
        [self getIPAddrFromEverFocusDDNS];
    }
    else {
        [self getHostByName];
    }
}

- (void)getIPAddrFromEverFocusDDNS
{
    NSString *urlString = [NSString stringWithFormat:@"http://%@/",host];
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    
    // connect
    dnsConnection = [NSURLConnection connectionWithRequest:request delegate:self];
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
//                                            completionHandler:
//                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
//                                      
//                                      // update cmdSender host
//                                      [cmdSender setHost:host];
//                                  }];
//    
//    [task resume];
    
    while( dnsConnection!=nil ) {
        
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
    
    // update cmdSender host
    [cmdSender setHost:host];
}

- (NSString *)getStringFromLine:(NSString *)data targetString:(NSString *)target withQuote:(NSString *)quote
{
    NSString  *echoString = nil;
    NSString  *lineString;
    NSArray *lines = [data componentsSeparatedByString:@"\n"];
    NSEnumerator *nse = [lines objectEnumerator];
    NSString  *tmp = [NSString stringWithFormat:@"%@=",target];
    
    while (lineString = [nse nextObject]) {
        NSScanner *scanner = [NSScanner scannerWithString:lineString];
        [scanner scanUpToString:tmp intoString:nil];
        [scanner scanString:tmp intoString:nil];
        [scanner scanUpToString:@"=" intoString:&echoString];
        if (echoString && quote!=NULL)
        {
            echoString = [echoString stringByReplacingOccurrencesOfString:quote withString:@""];
            echoString = [echoString stringByReplacingOccurrencesOfString:@"<br>" withString:@""];
            echoString = [echoString stringByReplacingOccurrencesOfString:@";" withString:@""];
            break;
        }
    }
    //NSLog(@"[PT] echo:%@",echoString);
    return echoString;
}

- (CameraInfo *)getInfoFromChannelIndex:(NSInteger)ChIdx
{
    CameraInfo *camInfo = nil;
    
    for (CameraInfo *curInfo in cameraList)
    {
        if (curInfo.index == ChIdx+1) {
            camInfo = curInfo;
            break;
        }
    }
    
    return camInfo;
}

#pragma mark - UIAlertView Delegate

- (void)showAlarm:(NSString *)message
{
    if ( message!=nil ) {
        
        // open an alert with just an OK button, touch "OK" will do nothing
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"BtnOK", nil) otherButtonTitles:nil];
        
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
    }
}

#pragma mark - JSON Parser

- (void)parseJSonData:(NSDictionary *)_json
{
    NSDictionary *source;
    if (blnGetTokenInfo) {
        source = _json;
        if (source.count) {
            tokenInfo = [[source valueForKey:@"TokenInfo"] copy];
        }
    }
    if (blnGetUUID) {
        source = _json;
        if (source.count) {
            xmsUUID = [[source valueForKey:@"uuid"] copy];
        }
    }
    if (blnGetVersion) {
        source = _json;
        if (source.count) {
            xms_version = [[source valueForKey:@"Version"] copy];
        }
    }
}

#pragma mark - Connection Delegate

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.
// it's a kind of callback function
//when call CommandSender.getData will set self to be delegate
//then in CommandSender.getData will set to become NSURLConnection's delegate
//after NSURLConnection receive data will trigger this method
{
    //NSLog(@"Received: %d", [data length]);
    if ( theConnection==requestConnection ) {
        
        [responseData appendData:data];
    }
}

- (void)connection:(NSURLConnection *)theConnection didReceiveResponse:(NSURLResponse *)response
// A delegate method called by the NSURLConnection when the request/response
// exchange is complete.
{
	NSHTTPURLResponse * httpResponse;
	httpResponse = (NSHTTPURLResponse *) response;
    //NSLog(@"Response:%@",httpResponse);
	if ((httpResponse.statusCode / 100) != 2) {
		
		self.errorDesc = [NSString stringWithFormat:@"HTTP error %zd", (ssize_t) httpResponse.statusCode];
        if (theConnection==streamConnection)
            [self stopStreaming];
        
        blnConnectionClear = YES;
		return;
	}
	
	if ( theConnection==dnsConnection ) {
		
		self.host = [NSString stringWithFormat:@"%@:%@",response.URL.host,response.URL.port];
		
		DBGLog(@"[SR] Get %@ from DNS",self.host);
        if (response.URL.host==NULL || response.URL.port==NULL) {
            blnGetDNS = NO;
        }
		[dnsConnection cancel];
		dnsConnection = nil;
	}
	else if ( theConnection==requestConnection ) {
		
        SAVE_FREE(responseData);
		responseData = [NSMutableData alloc];
	}
}

- (void)connectionDidFinishLoading:(NSURLConnection *)theConnection
// A delegate method called by the NSURLConnection when the connection has been done successfully.
{
	if ( theConnection==dnsConnection ) {
		
		dnsConnection = nil;
	}
	else if ( theConnection==requestConnection ) {
		
		requestConnection = nil;
	}
	else if ( theConnection==streamConnection ) {
		
        streamConnection = nil;
	}
    
	DBGLog(@"[SR] Connection finished");
}

- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error
// A delegate method called by the NSURLConnection if the connection fails.
{
	if ( theConnection==dnsConnection ) {
		
		dnsConnection = nil;
	}
	else if ( theConnection==requestConnection ) {
		
		requestConnection = nil;
	}
	else if ( theConnection==streamConnection ) {
		
		streamConnection = nil;
	}
	
    blnConnectionClear = YES;
//	self.errorDesc = NSLocalizedString(@"MsgTimeOut", nil);
    self.errorDesc = [error localizedDescription];
	DBGLog(@"[SR] Connection failed with error: %@", [error localizedDescription]);
}

- (void)connection:(NSURLConnection *)theConnection
didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    DBGLog(@"[SR] Authentication Challenge %@",challenge);
	NSURLCredential *EWScreds = [NSURLCredential credentialWithUser: self.user
														   password: self.passwd
														persistence: NSURLCredentialPersistenceForSession];
    if ([challenge previousFailureCount] == 0)
    {
		[[challenge sender] useCredential:EWScreds forAuthenticationChallenge:challenge];
    }
    else
	{
		if ( theConnection==dnsConnection ) {
			
			dnsConnection = nil;
		}
		else if ( theConnection==requestConnection ) {
			
			requestConnection = nil;
		}
		else if ( theConnection==streamConnection ) {
			
			streamConnection = nil;
		}
		
		[theConnection cancel];
        blnConnectionClear = YES;
		self.errorDesc = NSLocalizedString(@"MsgAuthFail", nil);
		
        DBGLog(@"[SR] Authentication failed on attempt %ld", (long)[challenge previousFailureCount]);
	}
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    //NSLog(@"[SR] willCacheResponse:%@",cachedResponse);
    return nil;
}

@end
