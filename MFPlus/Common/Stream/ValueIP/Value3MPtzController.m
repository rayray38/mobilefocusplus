//
//  Value3MPtzController.m
//  EFViewerPlus
//
//  Created by EFRD on 2016/12/27.
//  Copyright © 2016年 EverFocus. All rights reserved.
//

#import "Value3MPtzController.h"

@implementation Value3MPtzController
{
    NSString *STR_USER;
    NSString *STR_PWD;
    NSInteger intSpeed;
    NSInteger intType;
    NSInteger tmpOper;
}

#pragma mark - Initial

- (id)initWithHost:(NSString *)addr username:(NSString *)_user password:(NSString *)_pwd
{
    if ([super initWithHost:addr sid:nil toKen:nil]) {
        STR_USER = _user;
        STR_PWD = _pwd;
        intSpeed = 1;
        intType = 3;
    }
    return self;
}

- (NSDictionary *)valueCommand:(NSInteger)_ch operationCode:(NSInteger)_code start:(NSInteger)_start
{
    NSDictionary *cmdDict = [NSDictionary new];
    NSString *ipAddr = @"";
    _ch = 1;
    if ([self.host rangeOfString:@":"].location != NSNotFound) {
        ipAddr = [[self.host componentsSeparatedByString:@":"] firstObject];
    }
    else
        ipAddr = self.host;
    
    NSDictionary *_param = [NSDictionary dictionaryWithObjectsAndKeys:
                            ipAddr,@"ip",
                            STR_USER,@"username",
                            STR_PWD,@"pwd",
                            nil];
    
    NSDictionary *_oper = [NSDictionary dictionaryWithObjectsAndKeys:
                           @(_code),@"code",
                           @(_start),@"start",
                           @(intSpeed),@"speed",
                           nil];
    
    NSDictionary *_data = [NSDictionary dictionaryWithObjectsAndKeys:
                           @(intType),@"type",
                           @(_ch),@"ch",
                           _oper,@"oper",
                           nil];
    
    cmdDict = [NSDictionary dictionaryWithObjectsAndKeys:
               _param,@"param",
               _data,@"data",
               nil];
    
    return cmdDict;
}

#pragma mark - CGI Command

- (void)ptzZoomIn:(NSInteger)ch
{
    NSDictionary *reqDict = [self valueCommand:ch operationCode:PTZ_OPERATION_CMD_ZOOM_IN start:PTZ_CMD_START];
    [self cancelConnection];
    [cmdSender post3MCommand:STR_VALUE3M_PTZ_CMD dictionary:reqDict connection:&cmdConnection synchronously:NO];
    tmpOper = PTZ_OPERATION_CMD_ZOOM_IN;
}

- (void)ptzZoomOut:(NSInteger)ch
{
    NSDictionary *reqDict = [self valueCommand:ch operationCode:PTZ_OPERATION_CMD_ZOOM_OUT start:PTZ_CMD_START];
    [self cancelConnection];
    [cmdSender post3MCommand:STR_VALUE3M_PTZ_CMD dictionary:reqDict connection:&cmdConnection synchronously:NO];
    tmpOper = PTZ_OPERATION_CMD_ZOOM_OUT;
}

- (void)ptzFocusNear:(NSInteger)ch
{
    NSDictionary *reqDict = [self valueCommand:ch operationCode:PTZ_OPERATION_CMD_FOCUS_NEAR start:PTZ_CMD_START];
    [self cancelConnection];
    [cmdSender post3MCommand:STR_VALUE3M_PTZ_CMD dictionary:reqDict connection:&cmdConnection synchronously:NO];
    tmpOper = PTZ_OPERATION_CMD_FOCUS_NEAR;
}

- (void)ptzFocusFar:(NSInteger)ch
{
    NSDictionary *reqDict = [self valueCommand:ch operationCode:PTZ_OPERATION_CMD_FOCUS_FAR start:PTZ_CMD_START];
    [self cancelConnection];
    [cmdSender post3MCommand:STR_VALUE3M_PTZ_CMD dictionary:reqDict connection:&cmdConnection synchronously:NO];
    tmpOper = PTZ_OPERATION_CMD_FOCUS_FAR;
}

- (void)ptzStop:(NSInteger)ch
{
    NSDictionary *reqDict = [self valueCommand:ch operationCode:tmpOper start:PTZ_CMD_STOP];
    [self cancelConnection];
    [cmdSender post3MCommand:STR_VALUE3M_PTZ_CMD dictionary:reqDict connection:&cmdConnection synchronously:NO];
}

@end
