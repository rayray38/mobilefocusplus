//
//  Value3MPtzController.h
//  EFViewerPlus
//
//  Created by EFRD on 2016/12/27.
//  Copyright © 2016年 EverFocus. All rights reserved.
//

#import "PtzController.h"

#define STR_VALUE3M_PTZ_CMD @"goform/frmPTZ"

typedef enum : NSUInteger {
    PTZ_CMD_START = 0,
    PTZ_CMD_STOP,
    PTZ_OPERATION_CMD_ZOOM_IN = 11,
    PTZ_OPERATION_CMD_ZOOM_OUT,
    PTZ_OPERATION_CMD_FOCUS_NEAR,
    PTZ_OPERATION_CMD_FOCUS_FAR,
} Value3MPtzCommand;

@interface Value3MPtzController : PtzController

- (id)initWithHost:(NSString *)addr username:(NSString *)_user password:(NSString *)_pwd;

@end
