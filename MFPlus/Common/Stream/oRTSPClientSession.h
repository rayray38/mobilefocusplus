//
//  RTSPClientSession.h
//  EFViewer
//
//  Created by James Lee on 2011/4/25.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MAX_BUF_SIZE 768000

@protocol RTSPSubsessionDelegate;

@interface RTSPSubsession : NSObject {
	struct RTSPSubsessionContext *context;
}

- (NSString*)getSessionId;
- (NSString*)getMediumName;
- (NSString*)getProtocolName;
- (NSString*)getCodecName;
- (NSUInteger)getServerPortNum;
- (NSString*)getSDP_spropparametersets;
- (NSString*)getSDP_config;
- (NSString*)getSDP_mode;
- (NSUInteger)getSDP_VideoWidth;
- (NSUInteger)getSDP_VideoHeight;
- (NSUInteger)getClientPortNum;
- (int)getSocket;
- (void)increaseReceiveBufferTo:(NSUInteger)size;
- (void)setPacketReorderingThresholdTime:(NSUInteger)uSeconds;
- (BOOL)timeIsSynchronized;

@property (nonatomic, assign) id <RTSPSubsessionDelegate> delegate;
@property (nonatomic)   NSInteger   channel;

@end


@protocol RTSPSubsessionDelegate
- (void)didReceiveFrame:(const uint8_t*)frameData
		frameDataLength:(int)frameDataLength
	   presentationTime:(struct timeval)presentationTime
 durationInMicroseconds:(unsigned)duration
			 subsession:(RTSPSubsession*)subsession;
@end


@interface RTSPClientSession : NSObject {

	struct RTSPClientSessionContext *context;
	NSString *url;
	NSString *username;
	NSString *password;
	NSString *sdp;
    char     cancel;
}

- (id)initWithURL:(NSString*)url user:(NSString*)user passwd:(NSString*)passwd;
- (BOOL)setup;
- (NSArray*)getSubsessions;
- (BOOL)setupSubsession:(RTSPSubsession*)subsession useTCP:(BOOL)useTCP;
- (BOOL)play;
- (BOOL)teardown;
- (void)runEventLoop;
- (BOOL)runEventLoop:(char*)cancelSession;
- (NSString*)getLastErrorString;
- (NSString*)getSDP;
- (int)getSocket;

@end
