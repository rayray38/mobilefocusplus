//
//  GTStreamReceiver.m
//  EFViewerHD
//
//  Created by James Lee on 13/3/19.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import "GTStreamReceiver.h"
#import "GTAudioSender.h"

@implementation GTStreamReceiver

#pragma mark - Streaming Control

- (id)initWithDevice:(Device *)device
{
    outputList = [[NSMutableArray alloc] init];
    NSString *nan = [NSString stringWithFormat:@"99"];
    for (NSInteger i=0; i<4; i++)
        [outputList addObject:nan];
    
    machineName = [device.name copy];
    
    return [super initWithDevice:device];
}

- (BOOL)startLiveStreaming:(NSUInteger)channel {
    
	BOOL ret = YES;
	
	if ( ![super startLiveStreaming:currentCHMask] )
	{
		ret =  NO;
		goto Exit;
	}
	
	// set initial parameters
	seq = 0;
	startBuf[0] = 0x00;
	startBuf[1] = 0x00;
	startBuf[2] = 0x00;
	startBuf[3] = 0x01;
	
	
	NSLog(@"[GT] Init RtspClientSession");
    
	///// RTSP /////
    // init RTSP client
    //modify by robert hsu 20120203 for add rtsp port
	rtspClientSession = [[RTSPClientSession alloc]
						 initWithURL:[NSString stringWithFormat:@"rtsp://%@/stream/bidirect/channel%ld", [self getIp],(long)dualStream+1]
						 user:user passwd:passwd];
    
	// get SDP and create session
	if (![rtspClientSession setup])
	{
		self.errorDesc = NSLocalizedString(@"MsgRtspErr1", nil);
        
        [rtspClientSession release];
        rtspClientSession = nil;
        
		ret =  NO;
		goto Exit;
	}
	
	// get subsession
	subsessions = [[rtspClientSession getSubsessions] retain];
	if([subsessions count]==0)
	{
		self.errorDesc = NSLocalizedString(@"MsgRtspErr2", nil);
		ret =  NO;
		goto Exit;
	}
    NSLog(@"[GT] Subsession Count:%lu",(unsigned long)[subsessions count]);
	
	// setup subsession
    BOOL videoAvailable = NO;
    
    blnIsFirstIframe = NO;
	for (int i=0; i<[subsessions count]; i++) {
#ifndef _NEW_RTSP
        if (![rtspClientSession setupSubsession:[subsessions objectAtIndex:i] useTCP:YES])//modify by robert hsu 2012.02.01 for use over tcp
		{
			self.errorDesc = NSLocalizedString(@"MsgRtspErr3", nil);
			ret =  NO;
			goto Exit;
		}
#endif
		RTSPSubsession* subsession = [subsessions objectAtIndex:i];
		NSLog(@"[GT] Subsession %@: MediumName=%@ CodecName=%@",
			  [subsession getSessionId], [subsession getMediumName], [subsession getCodecName]);
        
        if ([[subsession getMediumName] isEqual:@"video"]) {
            
            if (![[subsession getCodecName] isEqual:@"JPEG"]) {
                
                [self SdpParser:[subsession getSDP_spropparametersets]];

            }
            videoAvailable = YES;
        }
		
		[[subsessions objectAtIndex:i] setDelegate:(id)self];
	}
    
    if(!videoAvailable)
	{
		self.errorDesc = NSLocalizedString(@"MsgRtspErr4", nil);
		ret =  NO;
		goto Exit;
	}
	
	// play
	[rtspClientSession play];
	blnReceivingStream = YES;
    
    // initiate audio sender
	if (audioSender != nil) {
        [audioSender release];
        audioSender = nil;
    }
	audioSender = [[GTAudioSender alloc] initWithHost:host user:user passwd:passwd sid:nil];
	
    
	// run loop
	exit = 0;
	[rtspClientSession runEventLoop:&exit];
	
Exit:
	blnReceivingStream = NO;
	
	return ret;
}

- (void)stopStreamingByView:(NSInteger)vIdx {
	
	exit = 1;
	
	[super stopStreamingByView:vIdx];
    
    if (rtspClientSession) {
        
        BOOL ret = [rtspClientSession teardown];
        if (!ret) {
            NSLog(@"[RTSP] teardown failed");
        }
    }
    
    blnConnectionClear = YES;
}

- (void)changeLiveChannel:(NSInteger)channel
{
    // no-op
}

#pragma Audio Control

- (void)openSound {
    
	// create audio decoder
    if (audioControl!=nil) {
        
        NSLog(@"[GT] Open sound");
        if (audioControl.codecId != AV_CODEC_ID_PCM_MULAW) {
            
            [audioControl close];
            [audioControl initWithCodecId:AV_CODEC_ID_PCM_MULAW srate:8000 bps:8 balign:audioSize fsize:audioSize channel:1];
            audioControl.codecId = audioControl.amFlag = AV_CODEC_ID_PCM_MULAW;
            NSLog(@"[GT] Create new audio decoder with codec id %d", AV_CODEC_ID_PCM_MULAW);
        }
        
        [audioControl start];
        audioOn = YES;
    }
}

- (void)closeSound {
    
	NSLog(@"[GT] Close sound");
    
    [audioControl stop];
    audioOn = NO;
}

- (void)dealloc
{
    if (ppsPkg.data != nil) {
        free(ppsPkg.data);
    }
    if (spsPkg.data != nil) {
        free(spsPkg.data);
    }
    if (rtspClientSession != nil) {
        
        [rtspClientSession release];
    }
    if (subsessions != nil) {
        
        [subsessions release];
    }
    
    machineName = nil;
	
	[super dealloc];
}

#pragma mark - Parser Delegate
/*
 NO XML need to parse @ GT series
*/
// Start of element
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
	attributes:(NSDictionary *)attributeDict
{

}

// Found Character
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSMutableString *)string
{
	
}

// End Element
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
{
	
}

// Get SPS & PPS from SDP    20130319 James Lee
- (void)SdpParser:(NSString *)sdpStr
{
    NSArray *strArray = [sdpStr componentsSeparatedByString:@","];
    NSData *spsData = [Decryptor base64:[strArray objectAtIndex:0]];
    NSData *ppsData = [Decryptor base64:[strArray objectAtIndex:1]];
//    NSLog(@"[GT] extra:%@, SPS:%@(%d), PPS:%@(%d)",sdpStr,[strArray objectAtIndex:0],spsData.length,[strArray objectAtIndex:1],ppsData.length);
    
    spsPkg.size = sizeof(startBuf) + spsData.length;
    ppsPkg.size = sizeof(startBuf) + ppsData.length;
    spsPkg.data = malloc(spsPkg.size);
    ppsPkg.data = malloc(ppsPkg.size);
    memcpy(spsPkg.data, startBuf, sizeof(startBuf));
    memcpy(spsPkg.data+sizeof(startBuf), [spsData bytes], spsData.length);
    memcpy(ppsPkg.data, startBuf, sizeof(startBuf));
    memcpy(ppsPkg.data+sizeof(startBuf), [ppsData bytes], ppsData.length);
}

#pragma mark - Connection Delegate

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.
{
	if ( theConnection==requestConnection ) {
		
		[responseData appendData:data];
	}
}

#pragma mark - RTSP delegate

- (void)didReceiveMessage:(NSString *)message
{
}

- (void)didReceiveFrame:(const uint8_t*)frameData
		frameDataLength:(int)frameDataLength
	   presentationTime:(struct timeval)presentationTime
 durationInMicroseconds:(unsigned)duration
			 subsession:(RTSPSubsession*)subsession
{
	//NSLog(@"didReceiveFrame");
	
	// Video frame
	if ([[subsession getMediumName] isEqual:@"video"]) {
		
		//NSLog(@"[GT] Video frame: frameDataLength=%d presentationTime=%ld:%d",
		//	  frameDataLength, presentationTime.tv_sec, presentationTime.tv_usec);
		
		// build video package
		VIDEO_PACKAGE videoPkg;
		
		videoPkg.sec	= presentationTime.tv_sec;
		videoPkg.ms		= presentationTime.tv_usec/1000;
		videoPkg.width	= 0; // don't care
		videoPkg.height	= 0; // don't care
        
        NSInteger iBufferIdx = [[outputList objectAtIndex:0] intValue];
		
		if ([[subsession getCodecName] isEqual:@"H264"]) { // H264

        
			videoPkg.codec	= AV_CODEC_ID_H264;
			
			if ((frameData[0] & 0X1F) == 0X5 ) { //I frame
                
				seq = 0;
				videoPkg.seq	= seq;
				videoPkg.type	= VO_TYPE_IFRAME;
				videoPkg.size	= spsPkg.size + ppsPkg.size + sizeof(startBuf)+frameDataLength;
				videoPkg.data	= malloc(videoPkg.size);
                
                memcpy((videoPkg.data), spsPkg.data, spsPkg.size);
                memcpy((videoPkg.data+spsPkg.size), ppsPkg.data, ppsPkg.size);
				memcpy((videoPkg.data+spsPkg.size+ppsPkg.size), startBuf, sizeof(startBuf));
				memcpy((videoPkg.data+spsPkg.size+ppsPkg.size+sizeof(startBuf)), frameData, frameDataLength);
                
                blnIsFirstIframe = YES;
#ifdef H264DEBUG
                NSLog(@"[264] Get I frame ---- frameData[0]:%x, size:%ld, startBuf:%lu, frameDataLength:%d",frameData[0],videoPkg.size,sizeof(startBuf),frameDataLength);
#endif
			}
			else if ((frameData[0] & 0X1F) == 0X1) { // P frame
				
                if (!blnIsFirstIframe) {
                    NSLog(@"[264] Waiting for I-Frame");
                    return;
                }
				seq++;
				videoPkg.seq	= seq;
				videoPkg.type	= VO_TYPE_PFRAME;
				videoPkg.size	= sizeof(startBuf)+frameDataLength;
				videoPkg.data	= malloc(videoPkg.size);
				memcpy(videoPkg.data, startBuf, sizeof(startBuf));
				memcpy((videoPkg.data+sizeof(startBuf)), frameData, frameDataLength);
#ifdef H264DEBUG
                NSLog(@"[264] Get P frame ---- frameData[0]:%x, size:%ld seq:%d",frameData[0],videoPkg.size,seq);
#endif
			}
			else {
#ifdef H264DEBUG
				NSLog(@"[264] Unknown %@ frame type", [subsession getCodecName]);
#endif
				return;
            }
        }else if ([[subsession getCodecName] isEqual:@"JPEG"]) { // JPEG
			
			videoPkg.codec	= AV_CODEC_ID_MJPEG;
			
			seq = 0;
			videoPkg.seq	= seq;
			videoPkg.type	= VO_TYPE_IFRAME;
			videoPkg.size	= frameDataLength;
			videoPkg.data	= malloc(videoPkg.size);
			memcpy(videoPkg.data, frameData, frameDataLength);
		}else if ([[subsession getCodecName] isEqual:@"MP4V-ES"]){ // MPEG4
        
            if ((frameData[4] & 0x40) == 0) {
                videoPkg.codec = AV_CODEC_ID_MPEG4;
                seq = 0;
                videoPkg.seq	= seq;
                videoPkg.type	= VO_TYPE_IFRAME;
                videoPkg.size	= frameDataLength;
                videoPkg.data	= malloc(videoPkg.size);
                memcpy(videoPkg.data, frameData, frameDataLength);
#ifdef H264DEBUG
                NSLog(@"[264] Get Mpeg I-Frame:%d",frameDataLength);
#endif
            }else if ((frameData[4] & 0x40) == 0x40) {
                seq++;
                videoPkg.codec = AV_CODEC_ID_MPEG4;
                videoPkg.seq	= seq;
                videoPkg.type	= VO_TYPE_PFRAME;
                videoPkg.size	= frameDataLength;
                videoPkg.data	= malloc(videoPkg.size);
                memcpy(videoPkg.data, frameData, frameDataLength);
#ifdef H264DEBUG
                NSLog(@"[264] Get Mpeg P-Frame:%d",frameDataLength);
#endif
            }
        }
		else {
			NSLog(@"[GT] Unknown codec type: %@", [subsession getCodecName]);
			return;
		}
        
        if (![machineName isEqualToString:[self.videoControl.aryTitle objectAtIndex:iBufferIdx]])
            [self.videoControl setTitleByCH:iBufferIdx title:machineName];
        
		// put package to VideoOutput buffer
        [videoControl putFrameIntoBufferByCH:&videoPkg chIndex:iBufferIdx];
		
		// update frame size (get from decoder)
		frameWidth	= [videoControl getVideoWidthbyCH:iBufferIdx];
		frameHeight = [videoControl getVideoHeightbyCH:iBufferIdx];
	}
	// Audio frame
	else if ([[subsession getMediumName] isEqual:@"audio"]) {
        
		if (audioControl!=nil && audioOn) {
			[audioControl playAudio:(Byte *)frameData length:frameDataLength];
		}
		else {
			//NSLog(@"[GT] Receive audio (OFF)");
		}
        
	}else {
		NSLog(@"[GT] Unknown medium name: %@", [subsession getMediumName]);
	}
}

@end
