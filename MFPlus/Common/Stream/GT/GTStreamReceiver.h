//
//  GTStreamReceiver.h
//  EFViewerHD
//
//  Created by James Lee on 13/3/19.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StreamReceiver.h"
#import "RTSPClientSession.h"

@interface GTStreamReceiver : StreamReceiver <RTSPSubsessionDelegate>
{
	NSString *machineName;
	
	RTSPClientSession* rtspClientSession;
	NSArray* subsessions;
	char exit;
	
	int seq;
	uint8_t startBuf[4];
    VIDEO_PACKAGE spsPkg;
    VIDEO_PACKAGE ppsPkg;
	
	int audioSize;
    
    BOOL blnIsFirstIframe;      //add by James Lee 20120709 to avoid P-Frame send to decoder without I-Frame
}

- (void)SdpParser:(NSString *)sdpStr;

@end
