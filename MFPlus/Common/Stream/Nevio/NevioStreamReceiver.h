//
//  NevioStreamReceiver.h
//  EFViewerHD
//
//  Created by James Lee on 12/10/25.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StreamReceiver.h"
#import "NevioNet.h"

#define CMD_QUICK		"00010601"
#define CMD_QUICK_ACK	"80010601"
#define CMD_QUICK_NAK	"40010601"
#define CMD_SWITCH		"00010602"
#define CMD_SWITCH_ACK	"80010602"
#define CMD_SWITCH_NAK	"40010602"
#define CMD_STREAM      "00030102"  //get stream info add by robert hsu 20110927
#define CMD_STREAM_ACK  "80030102"
#define CMD_STREAM_NAK  "40030102"
#define START_LIVE      0           //add by robert hsu 20110927 for check command action
#define CHANGE_CHANNEL  1
#define CMD_EVS_PTZ     "00041002"  //add by robert hsu 20110927 for get evs200 ptz support
#define CMD_EVS_PTZ_ACK "80041002"
#define CMD_EVS_PTZ_NAK "40041002"

@interface NevioStreamReceiver : StreamReceiver
{
    NSString *ackNumber;
	NSString *machineName;
	
	Byte    pFrameBuf[MAX_FRAME_SIZE];
	int     payloadSize;
	int     headerOffset;
	int     payloadOffset;
	int     tailOffset;
    
    int     iCurrentStream;     //add by robert hsu 20110927 for keep current channel when parse xml
    int     iStreamMask;        //add by robert hsu 20110927 for keep channel mask
}

- (BOOL)getEnabledChannel;
- (BOOL)getStreamInfo;
- (BOOL)sendXMLCmd:(NSString *)strcmd with:(NSString *)strACKFlag;
- (void)setStreaming;

@end
