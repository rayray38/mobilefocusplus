//
//  NevioPtzController.m
//  EFViewer
//
//  Created by James Lee on 2010/9/5.
//  Copyright 2010 EverFocus. All rights reserved.
//

#import "NevioPtzController.h"

#define CMD_PTZ "00080101"

@interface NevioPtzController ()

- (void)sendCommand;

@end

@implementation NevioPtzController

#pragma mark -
#pragma mark Initialization Command

- (id)initWithHost:(NSString *)addr sid:(NSString *)sid toKen:(NSString *)_tokenKey
{
	if (self = [super initWithHost:addr sid:sid toKen:nil]) {
		
		speed = 0x50;
		grade = 0x60;
		
		// set fixed bytes
		byte[0] = 0xFA;
		byte[2] = 0x20;
		byte[3] = 0x00;
		byte[5] = 0x00;
	}
	
	return self;
}

#pragma mark -
#pragma mark Private Functions

- (void)sendCommand
{
	byte[6] = (byte[0]+byte[1]+byte[2]+byte[3]+byte[4]+byte[5]) & 0x7F;
	
	NSString *urlString = [NSString stringWithFormat:@"video=2&xml=<CMD>%s<TEXT>0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X</TEXT></CMD>",
						   CMD_PTZ,byte[0],byte[1],byte[2],byte[3],byte[4],byte[5],byte[6]];
	
	[self cancelConnection];
    [cmdSender getNevioData:urlString connection:&cmdConnection synchronously:NO withPath:@"cgi-bin/cgiStream"];
	//[cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"cgi-bin/cgiStream" forOldDev:YES];  //20151008 modified by Ray Lin
}

#pragma mark -
#pragma mark CGI Command

- (void)ptzTiltDown:(NSInteger)ch
{
	byte[1] = 0x02;  // down
	byte[4] = speed; // speed
    iCurrentCH = ch;	
	[self sendCommand];
}

- (void)ptzTiltUp:(NSInteger)ch
{
	byte[1] = 0x01;  // up
	byte[4] = speed; // speed
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzPanLeft:(NSInteger)ch
{
	byte[1] = 0x03;  // left
	byte[4] = speed; // speed
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzPanRight:(NSInteger)ch
{
	byte[1] = 0x04;  // right
	byte[4] = speed; // speed
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzLeftUp:(NSInteger)ch
{
	byte[1] = 0x05;  // left-up
	byte[4] = speed; // speed
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzLeftDown:(NSInteger)ch
{
	byte[1] = 0x06;  // left-down
	byte[4] = speed; // speed
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzRightUp:(NSInteger)ch
{
	byte[1] = 0x07;  // right-up
	byte[4] = speed; // speed
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzRightDown:(NSInteger)ch;
{
	byte[1] = 0x08;  // right-down
	byte[4] = speed; // speed
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzZoomIn:(NSInteger)ch
{
	byte[1] = 0x0B;  // zoom tele
	byte[4] = grade; // grade
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzZoomOut:(NSInteger)ch
{
	byte[1] = 0x0C;  // zoom wide
	byte[4] = grade; // grade
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzFocusFar:(NSInteger)ch
{
	byte[1] = 0x0D;  // focus far
	byte[4] = grade; // grade
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzFocusNear:(NSInteger)ch
{
	byte[1] = 0x0E;  // focus near
	byte[4] = grade; // grade
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzIrisOpen:(NSInteger)ch
{
	byte[1] = 0x09;  // iris open
	byte[4] = grade; // grade
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzIrisClose:(NSInteger)ch
{
	byte[1] = 0x0A;  // iris close
	byte[4] = grade; // grade
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzStop:(NSInteger)ch
{
	byte[1] = 0x01; // up
	byte[4] = 0x00; // speed
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzPresetGo:(NSInteger)ch value:(NSInteger)value
{
	byte[1] = 0x76;   // goto preset pos.
	byte[4] = value; // pp
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzPresetSet:(NSInteger)ch value:(NSInteger)value
{
	byte[1] = 0x75;   // set preset pos.
	byte[4] = value; // pp
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzPresetCancel:(NSInteger)ch value:(NSInteger)value
{
	byte[1] = 0xA7;   // clear preset pos.
	byte[4] = value; // pp
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzAutoPanRun:(NSInteger)ch
{
	byte[1] = 0x10; // start line scan
	byte[4] = 0x30; // speed
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzAutoPan360Run:(NSInteger)ch
{
    byte[1] = 0x11; // start line scan
	byte[4] = 0x30; // speed
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzAutoPanStop:(NSInteger)ch
{
	[self ptzStop:ch];
}
 
- (void)ptzPattern:(NSInteger)ch value:(NSInteger)value
{
	byte[1] = 0x71; // start cruise group
	byte[4] = value+16; // group (17~20 PATTERN)
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzTour:(NSInteger)ch value:(NSInteger)value
{
	byte[1] = 0x71; // start cruise group
	byte[4] = value; // group (1~16 TOUR)
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzOsdOpen:(NSInteger)ch
{
	byte[1] = 0x90; // setup menu
	byte[4] = 0x01; // operate: enter menu
	iCurrentCH = ch;
	[self sendCommand];
}

- (void)ptzOsdExit:(NSInteger)ch
{
	byte[1] = 0x90; // setup menu
	byte[4] = 0x02; // operate: exit menu
	iCurrentCH = ch;
	[self sendCommand];
}

@end
