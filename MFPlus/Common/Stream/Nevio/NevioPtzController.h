//
//  NevioPtzController.h
//  EFViewer
//
//  Created by James Lee on 2010/9/5.
//  Copyright 2010 EverFocus. All rights reserved.
//

#import "PtzController.h"


@interface NevioPtzController : PtzController {

	NSInteger	speed;
	NSInteger	grade;
	Byte		byte[7];
    NSInteger   iCurrentCH; //add by robert hsu 20110928 for PSIA
}

@end
