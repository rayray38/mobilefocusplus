//
//  NevioStreamReceiver.m
//  EFViewerHD
//
//  Created by James Lee on 12/10/25.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "NevioStreamReceiver.h"
#import "NevioAudioSender.h"
#import "NevioPtzController.h"

@implementation NevioStreamReceiver

#pragma mark - Streaming Control

- (id)initWithDevice:(Device *)device
{
    outputList = [[NSMutableArray alloc] init];
    NSString *nan = [[NSString alloc] initWithFormat:@"99"];
    for (NSInteger i=0; i<4; i++)
        [outputList addObject:nan];
    
    return [super initWithDevice:device];
}

- (BOOL)startLiveStreaming:(NSUInteger)channel {
    
	BOOL ret = YES;
	NSString *cmd;
	
	if ( ![super startLiveStreaming:channel] )
	{
		ret =  NO;
		goto Exit;
	}
	
	// reset parameters
	headerOffset = 0;
	payloadOffset = 0;
	tailOffset = 0;
	payloadSize = 0;
	
	// set available channels
	validChannel = 3; // 0011 // no use
    
    BOOL blnRtn = [self getStreamInfo];
    //NSLog(@"[NEVIO] Streaming Count:%d",streamInfoList.count);
    
    if(! blnRtn)
    {// if get enable command fail
        ret = NO;
        NSLog(@"%@",self.errorDesc);
        goto Exit;
    }
    
    [self setStreaming];
    
    if(blnStopStreaming)
    {
        goto Exit;
    }
    
    validChannel = iStreamMask;
    
    if (validChannel == 0) {
        ret = NO;
        goto Exit;
        
    }
    
	// connection failed
    if(!blnRtn)
    {//send start stream command fail
        NSLog(@"%@",self.errorDesc);
		ret =  NO;
		goto Exit;
	}
	
	if ( blnStopStreaming ) {
		
		goto Exit;
	}
	
	// initiate PTZ controller
	[ptzController release];
	ptzController = [[NevioPtzController alloc] initWithHost:host sid:nil toKen:nil];
	
	// initiate audio sender
	[audioSender release];
	audioSender = [[NevioAudioSender alloc] initWithHost:host user:user passwd:passwd sid:sessionId];
	
	// start streaming
	cmd = [NSString stringWithFormat:@"video=1&sessionid=%@",sessionId];
    if (DeviceType < 9) {
        [cmdSender postCommand:cmd connection:&streamConnection synchronously:YES withPath:@"cgi-bin/cgiStream" forOldDev:YES];
    }
    else
        [cmdSender postCommand:cmd connection:&streamConnection synchronously:YES withPath:@"cgi-bin/cgiStream" forOldDev:NO];
	
	if ( errorDesc!=nil ) {
		
		ret = NO;
		goto Exit;
	}
	
Exit:
	blnReceivingStream = NO;
	
	return ret;
}

- (void)stopStreamingByView:(NSInteger)vIdx
{
    if (audioOn)
        [self closeSound];
    
	[super stopStreamingByView:vIdx];
    
    blnConnectionClear = YES;
}

- (void)changeLiveChannel:(NSInteger)channel // no use
{
    CameraInfo *tmpEntry;
    if (dualStream && cameraList.count>1)
        tmpEntry =[cameraList objectAtIndex:1];
    else
        tmpEntry = [cameraList firstObject];
    
    NSString *cmd = [NSString stringWithFormat:@"video=2&xml=<CMD>%s<CID>%@</CID><LIVEV><STREAM>%ld:%ld</STREAM></LIVEV>\
                     <LIVEA><CHANNEL>1</CHANNEL></LIVEA><LIVEEVENT>OFF</LIVEEVENT></CMD>",CMD_SWITCH,sessionId,(long)channel,(long)tmpEntry.index];
    NSString *ackFlag = [[NSString alloc] initWithFormat:@"%s",CMD_SWITCH_ACK];
    [self sendXMLCmd:cmd with:ackFlag];
}

- (void)openSound {
	
	// create audio decoder
	if ( audioControl!=nil ) {
		
        NSLog(@"[NEVIO] Open sound");
        if (audioControl.codecId != AV_CODEC_ID_ADPCM_IMA_WAV) {
            
            [audioControl close];
            [audioControl initWithCodecId:AV_CODEC_ID_ADPCM_IMA_WAV srate:8000 bps:4 balign:256 fsize:505 channel:1];
            audioControl.codecId = audioControl.amFlag = AV_CODEC_ID_ADPCM_IMA_WAV;
            NSLog(@"[NEVIO] Create new audio decoder with codec id %d", AV_CODEC_ID_ADPCM_IMA_WAV);
        }
        [audioControl start];
        audioOn = YES;
	}
}

- (void)closeSound {
	
	NSLog(@"[NEVIO] Close sound");
                                                
    [audioControl stop];
    audioOn = NO;
}

- (void)dealloc {
	
	[ackNumber release];
	[machineName release];
	
	[super dealloc];
}

#pragma mark - XML Parser Delegate

// Start of element
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    [currentXmlElement release];
	currentXmlElement = [elementName copy];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSMutableString *)string
{
	if ( [currentXmlElement isEqualToString:@"ACK"] ) {
		
		[ackNumber release];
		ackNumber = [string copy];
	}
	else if ( [currentXmlElement isEqualToString:@"CID"] ) {
		
		[sessionId release];
		sessionId = [string copy];
	}
	else if ( [currentXmlElement isEqualToString:@"AUTHORITY"] || [currentXmlElement isEqualToString:@"ATHORITY"]) {
		
		if ( [string isEqualToString:@"SUPERVISOR"] ) {
			userLevel = 4;
		}
		else if ( [string isEqualToString:@"ADMIN"] ) {
			userLevel = 3;
		}
		else if ( [string isEqualToString:@"USER"] ) {
			userLevel = 2;
		}
		else if ( [string isEqualToString:@"GUEST"] ) {
			userLevel = 1;
		}
		else {
			userLevel = 0;
		}
	}
    else if ( [currentXmlElement isEqualToString:@"MODEL"] ) {
        
        
    }
	else if ( [currentXmlElement isEqualToString:@"MACHINENAME"] ) {
		
		[machineName release];
		machineName = [string copy];
	}
    else if( [currentXmlElement isEqualToString:@"STREAMID"])
    {// add by robert hsu 20110927 for get stream numbert for current xml node
        NSString *strItem;
        strItem = [[NSString alloc] initWithFormat:@"%s",CMD_STREAM_ACK];
        
        if ([ackNumber isEqualToString:strItem])
        {
            iCurrentStream = [string intValue];
            CameraInfo  *emptyEntry = [[[CameraInfo alloc] init] autorelease];
            emptyEntry.index = iCurrentStream;
            [cameraList addObject:emptyEntry];
            blnGetChannelInfo = YES;
        }
        [strItem release];
    }
    if (blnGetChannelInfo) {
        
        if( [currentXmlElement isEqualToString:@"ENABLE"])
        {// add by robert hsu 20110927 parse current stream is disabled or enabled
        
            CameraInfo *currentEntry = [cameraList lastObject];
            
            if([string isEqualToString:@"TRUE"]) {
                iStreamMask +=  (iCurrentStream/2) +1 ;
                currentEntry.enable = YES;
            }else
                currentEntry.enable = NO;
        }
        else if( [currentXmlElement isEqualToString:@"RESOLUTION"])
        {
            CameraInfo *currentEntry = [cameraList lastObject];
            currentEntry.codecType = [string copy];
            blnGetChannelInfo = NO;
        }
    }
    else if( [currentXmlElement isEqualToString:@"PTZFUNC"])
    {//add by robert hsu 20110927 for get PTZ function enabled value
        if([string isEqualToString:@"ON"])
        {
            self.blnChSupportPTZ = YES;
        }
    }
}

// End Element
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
{
    if( [elementName isEqualToString:@"STREAMID"]) {
        CameraInfo *currentEntry = [cameraList lastObject];
        if (!currentEntry.enable)
            [cameraList removeObject:currentEntry];
    }
}

#pragma mark -
#pragma mark Connection Delegate


- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.
{
	//NSLog(@"---------- Received: %d ----------", [data length]);
	
	if ( theConnection==requestConnection ) {
		
		[responseData appendData:data];
	}
	else if ( theConnection==streamConnection ) {
		
		self.blnReceivingStream = YES;
		
		if ( blnStopStreaming ) return;
		
		int readLen = 0;
		int remanentLen = [data length];
		
		while ( remanentLen>0 ) {
			
			// read header data
			if ( headerOffset<sizeof(NET_MEDIA_HEAD) ) {
				
				Byte *buf = &pFrameBuf[headerOffset];
				int remains = sizeof(NET_MEDIA_HEAD)-headerOffset;
				int len = (remains>remanentLen)?remanentLen:remains;
				
				[data getBytes:buf range:NSMakeRange(readLen, len)];
				
				readLen += len;
				remanentLen -= len;
				headerOffset += len;
				
				// header data finish
				if ( headerOffset==sizeof(NET_MEDIA_HEAD) ) {
					
					NET_MEDIA_HEAD	*pHeader  = (NET_MEDIA_HEAD *)&pFrameBuf[0];
					
					// check header magic number
					if ( ntohl(pHeader->net_magic)!=NET_MAGIC ) {
						
                        self.errorDesc = NSLocalizedString(@"MsgDataErr", nil);
						[self stopStreamingByView:[[outputList objectAtIndex:0] integerValue]];
						return;
					}
					// convert header for different endian
					pHeader->size   = ntohl(pHeader->size);
					pHeader->sec    = ntohl(pHeader->sec);
					pHeader->u_sec  = ntohl(pHeader->u_sec);
					pHeader->height = ntohs(pHeader->height);
					pHeader->width  = ntohs(pHeader->width);
					pHeader->v_sn   = 0; // Nevio audio does not init to 0
					
					payloadSize = pHeader->size;
					frameWidth	= pHeader->width;
					frameHeight = pHeader->height;
                    
                    //NSLog(@"DataType:%d",pHeader->id_type);
				}
			}
			
			// read payload data
			if ( payloadOffset<payloadSize && remanentLen>0 ) {
				
				Byte *buf = &pFrameBuf[sizeof(NET_MEDIA_HEAD)+payloadOffset];
				int remains = payloadSize-payloadOffset;
				int len = (remains>remanentLen)?remanentLen:remains;
				
				[data getBytes:buf range:NSMakeRange(readLen, len)];
				
				readLen += len;
				remanentLen -= len;
				payloadOffset += len;
			}
			
			// read tail data
			if ( tailOffset<TAIL_SIZE && remanentLen>0 ) {
				
				Byte *buf = &pFrameBuf[sizeof(NET_MEDIA_HEAD)+payloadSize+tailOffset];
				int remains = TAIL_SIZE-tailOffset;
				int len = (remains>remanentLen)?remanentLen:remains;
				
				[data getBytes:buf range:NSMakeRange(readLen, len)];
				
				readLen += len;
				remanentLen -= len;
				tailOffset += len;
			}
			
			// finish getting one frame
			if ( tailOffset==TAIL_SIZE ) {
				
				NET_MEDIA_HEAD  *pHeader	= (NET_MEDIA_HEAD *)&pFrameBuf[0];
				void			*data		= (void *)&pFrameBuf[sizeof(NET_MEDIA_HEAD)];
				unsigned long   *pTail		= (unsigned long *)&pFrameBuf[sizeof(NET_MEDIA_HEAD)+payloadSize];
				
				// check tail data
				if ( ntohl(*pTail)!=NET_TAIL ) {
					
                    self.errorDesc = NSLocalizedString(@"MsgDataErr", nil);
					[self stopStreamingByView:[[outputList objectAtIndex:0] integerValue]];
					return;
				}
				
				// process the frame here
                // decode and show the received VIDEO frame
                if ( pHeader->id_type==NET_H264I || pHeader->id_type==NET_H264P ||
                    pHeader->id_type==NET_MP4I  || pHeader->id_type==NET_MP4P  ||
                    pHeader->id_type==NET_JPEG ) {
                    
                    //NSLog(@"Receive completed: %d-%d",pHeader->i_sn,pHeader->f_sn);
                    
                    // check video channel, ignore the privious frames when channel change
                    // NSLog(@"current Channel=%d Stream_id=%d \n",currentChannel ,pHeader->streamid);
                    //if ( ((0x1<<(pHeader->streamid/2)) & currentCHMask) != 0 ) {
                    if (YES) {
                        
                        VIDEO_PACKAGE videoPkg;
                        
                        videoPkg.sec	= pHeader->sec;
                        videoPkg.ms		= pHeader->u_sec/1000;
                        videoPkg.seq	= pHeader->f_sn;
                        videoPkg.size	= pHeader->size;
                        videoPkg.width	= pHeader->width;
                        videoPkg.height	= pHeader->height;
                        videoPkg.data	= malloc(pHeader->size);
                        memcpy(videoPkg.data, data, pHeader->size);
                        NSInteger iBufferIdx = [[outputList objectAtIndex:0] intValue];
                        
                        switch (pHeader->id_type) {
                            case NET_H264I:
                                videoPkg.codec	= AV_CODEC_ID_H264;
                                videoPkg.type	= VO_TYPE_IFRAME;
                                break;
                            case NET_H264P:
                                videoPkg.codec	= AV_CODEC_ID_H264;
                                videoPkg.type	= VO_TYPE_PFRAME;
                                break;
                            case NET_MP4I:
                                videoPkg.codec	= AV_CODEC_ID_MPEG4;
                                videoPkg.type	= VO_TYPE_IFRAME;
                                //NSLog(@"GET I-Frame:%lu",pHeader->size);
                            case NET_MP4P:
                                videoPkg.codec	= AV_CODEC_ID_MPEG4;
                                videoPkg.type	= VO_TYPE_PFRAME;
                                //NSLog(@"GET P-Frame:%lu",pHeader->size);
                                break;
                            case NET_JPEG:
                                videoPkg.codec	= AV_CODEC_ID_MJPEG;
                                videoPkg.type	= VO_TYPE_IFRAME;
                                break;
                            default:
                                videoPkg.codec	= AV_CODEC_ID_NONE;
                                videoPkg.type	= VO_TYPE_NONE;
                                break;
                        }
                        
                        if (![machineName isEqualToString:[self.videoControl.aryTitle objectAtIndex:iBufferIdx]])
                            [self.videoControl setTitleByCH:iBufferIdx title:machineName];
                        
                        [self.videoControl putFrameIntoBufferByCH:&videoPkg chIndex:iBufferIdx];
                    }
                }
				// decode and play the received AUDIO frame
				else if ( pHeader->id_type==NET_ADPCM)
				{
					if (audioControl!=nil && audioOn) {
                        
						//NSLog(@"Receive audio");
						
						// decode and play the audio frame
						NET_MEDIA_HEAD  *pHeader  = (NET_MEDIA_HEAD *)&pFrameBuf[0];
						Byte			*pPayload = &pFrameBuf[sizeof(NET_MEDIA_HEAD)];
						[audioControl playAudio:pPayload length:pHeader->size];
					}
					else {
						//NSLog(@"Receive audio (OFF)");
					}
				}
                
				// reset parameters
				memset(&pFrameBuf[0],0,MAX_FRAME_SIZE); // clean buffer
				headerOffset=0;
				payloadOffset=0;
				tailOffset=0;
				payloadSize = 0;
			}
		}
	}
}

#pragma mark - Get Information

- (BOOL) getEnabledChannel
{
    iCurrentStream = 0;
    iStreamMask = 0;
    
    // init cmdSender
	//CommandSender* chCmdSender = [[CommandSender alloc] initWithHost:host delegate:self];
    
    BOOL blnSend = YES;
    NSString *cmd;
    NSString *strAckFlag;
    strAckFlag = [[NSString alloc] initWithFormat:@"%s",CMD_STREAM_ACK];
    cmd = [NSString stringWithFormat:@"video=2&xml=<CMD>%s</CMD>",CMD_STREAM];
    blnSend = [self sendXMLCmd:cmd with:strAckFlag];
    
    // Get Enabled channels fail
	if ( !blnSend) {
		self.errorDesc = NSLocalizedString(@"MsgInfoErr", nil);
        blnSend = NO;
    }
    //[chCmdSender release];
    return blnSend;
}

- (BOOL)getStreamInfo
{
    cameraList = [[NSMutableArray alloc] init];
    currentPlayCH = 1;
    
    [self getSupportPTZ:1];
    
    BOOL blnSend = YES;
    NSString *cmd;
    NSString *strAckFlag;
    strAckFlag = [NSString stringWithFormat:@"%s",CMD_STREAM_ACK];
    cmd = [NSString stringWithFormat:@"video=2&xml=<CMD>%s</CMD>",CMD_STREAM];
    blnSend = [self sendXMLCmd:cmd with:strAckFlag];
    
    // Get Enabled channels fail
	if ( !blnSend) {
		self.errorDesc = NSLocalizedString(@"MsgInfoErr", nil);
        blnSend = NO;
    }
    
    return blnSend;
}

- (void)setStreaming
{
    NSInteger streamID = 0;
    CameraInfo *tmpEntry;
    
    if (dualStream && cameraList.count>1)
        tmpEntry =[cameraList objectAtIndex:1];
    else
        tmpEntry = [cameraList firstObject];
    
    streamID = tmpEntry.index;
    
    BOOL blnSend = YES;
    NSString *cmd;
    NSString *ackFlag;
    cmd = [NSString stringWithFormat:@"video=2&xml=<CMD>%s<LIVEV><STREAM>%lu:%ld</STREAM></LIVEV>\
           <LIVEA><CHANNEL>1</CHANNEL></LIVEA><LIVEEVENT>OFF</LIVEEVENT></CMD>",CMD_QUICK,(unsigned long)currentPlayCH,(long)streamID];
    
    ackFlag = [NSString stringWithFormat:@"%s",CMD_QUICK_ACK];
    //NSLog(@"command:%@ ,ack flag:%@",cmd,ackFlag);
    blnSend = [self sendXMLCmd:cmd with:ackFlag];
}

- (BOOL)sendXMLCmd:(NSString *)strcmd with:(NSString *)strACKFlag
{
    BOOL blnRtn = YES;
    NSString *response;
    
    if (DeviceType < 9) {
        [cmdSender postCommand:strcmd connection:&requestConnection synchronously:YES withPath:@"cgi-bin/cgiStream" forOldDev:YES];
    }
    else
        [cmdSender postCommand:strcmd connection:&requestConnection synchronously:YES withPath:@"cgi-bin/cgiStream" forOldDev:NO];
    
    // parse response
	response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	//NSLog(@"[Nevio] response:%@",response);
	[cmdSender parseXmlResponse:response];
	
    if (![ackNumber isEqualToString:[NSString stringWithFormat:@"%@",strACKFlag]]) {
        blnRtn = NO;
    }
    [response release];
    return blnRtn;
}

- (BOOL)getSupportPTZ:(NSInteger)channel
{
    self.blnChSupportPTZ =NO;
    NSString *cmd;
    NSString *flag;
    
    switch (DeviceType) {
        case EPN3100:
        case EPN3600:
            self.blnChSupportPTZ = YES;
            break;
        case EVS200A_AW:
            cmd = [NSString stringWithFormat:@"video=2&xml=<CMD>%s<CHANNEL>%ld</CHANNEL></CMD>",CMD_EVS_PTZ,(long)channel];
            flag = [NSString stringWithFormat:@"%s",CMD_EVS_PTZ_ACK];
            [self sendXMLCmd:cmd with:flag];
            break;
        default:
            self.blnChSupportPTZ = NO;
            break;
    }
    return self.blnChSupportPTZ;
}
@end
