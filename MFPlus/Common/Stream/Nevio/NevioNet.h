//
//  NevioNet.h
//  LiveStream
//
//  Created by James Lee on 2010/7/15.
//  Copyright 2010 EverFocus. All rights reserved.
//

#define TAIL_SIZE       4

//NET_MEDIA_HEAD: net_magic
#define NET_MAGIC	0xa74f80e6 
#define NET_TAIL	0xabcd8765

//id_type of NET_MEDIA_HEAD //src: v1.0.0.9 200800304
#define NET_MP4I		0x81
#define NET_MP4P		0x82
#define NET_H264I		0x85
#define NET_H264P		0x86
#define NET_JPEG		0x88
#define NET_EVENT		0x8A
#define NET_HISI_H264I	0x8C
#define NET_HISI_H264P	0x8D
#define NET_HISI_H265I	0x8E //HEVC
#define NET_HISI_H265P	0x8F //HEVC
#define NET_ADPCM		0x91 //Nevio IMA_ADPCM
#define NET_G723		0x92
#define NET_PCMU		0x93
#define NET_PCMA		0x94
#define NET_G726_24		0x95
#define NET_G726_32		0x96

#define RES_4CIF_P      @"704x480"
#define RES_4CIF_N      @"704x576"
#define RES_VGA_P       @"640x480"
#define RES_VGA_N       @"640x480"
#define RES_CIF_P       @"352x240"
#define RES_CIF_N       @"352x288"
#define RES_QVGA_P      @"320x240"
#define RES_QVGA_N      @"320x240"
#define RES_QCIF_P      @"176x112"
#define RES_QCIF_N      @"176x144"

#define CODEC_IS_4CIF(X) ([X isEqualToString:RES_4CIF_P] || [X isEqualToString:RES_4CIF_N])
#define CODEC_IS_VGA(X)  ([X isEqualToString:RES_VGA_P]  || [X isEqualToString:RES_VGA_N])
#define CODEC_IS_CIF(X)  ([X isEqualToString:RES_CIF_P]  || [X isEqualToString:RES_CIF_N])
#define CODEC_IS_QVGA(X) ([X isEqualToString:RES_QVGA_P] || [X isEqualToString:RES_QVGA_N])
#define CODEC_IS_QCIF(X) ([X isEqualToString:RES_QCIF_P] || [X isEqualToString:RES_QCIF_N])

typedef struct _NET_MEDIA_HEAD
{
	unsigned int net_magic;//
	unsigned int size;		// following data size
	unsigned short height;
	unsigned short width;
	unsigned char fps;		// 
	unsigned char ch;		// data channel: 0
	unsigned char streamid;
	unsigned char id_type;
	unsigned int sec;
	unsigned int u_sec;
	unsigned char tag[17];	// 16 bytes + NULL
    unsigned char i_sn;		//I frame 0~255
    unsigned char f_sn;		//0=I frame, 1~255=P frame
    unsigned char v_sn;		//20090705 video format serial number
	unsigned char reserved[4];
} NET_MEDIA_HEAD, *PNET_MEDIA_HEAD; //48


//v1.0.0.21 20080512
typedef struct EVENT_CONTENT {    //byte count
	//==first part 0~127 byte=====================================================
	unsigned int alarmIn;          //4 phoenix, alarm input channel 0~31
	unsigned int alarmOut;         //8 phoenix, alarm output channel 0~31
	unsigned int motionAtArea[5];  //28 phoenix, motion detected at channel 0~31
	unsigned int manuallyTrigger;  //32
	unsigned int videoLoss;        //36 phoenix, video loss at channel 0~31
	unsigned int uninstall;        //40 phoenix, install at channel 0~31
	unsigned int covered;          //44 phoenix, cover at channel 0~31
	//--The settingChangeFlag is added by jacky at 20080507-----------------------
#define VIDEO_SETTING_CHANGED           0x00000001 //bit 1, related cmd=0x00030101
#define AUDIO_SETTING_CHANGED           0x00000002 //bit 2,             0x00030301
#define USER_SETTING_CHANGED            0x00000004 //bit 3,             0x00050101
#define EVENT_SETTING_CHANGED           0x00000008 //bit 4,             0x00060101
#define TIME_MASK_SETTING_CHANGED       0x00000010 //bit 5,             0x00060301
#define PP_DURATING_SETTING_CHANGED     0x00000020 //bit 6,             0x00060601
#define RETRIGGER_SETTING_CHANGED       0x00000040 //bit 7,             0x00060701
#define MACHINE_NAME_SETTING_CHANGED    0x00000080 //bit 8,             0x00070301
#define VIDEO_RECORDING_SETTING_CHANGED 0x00000100 //bit 9,             0x00030201
#define NUMBER_SETTING_CHANGED 9
	unsigned int settingChangedFlag; //48
	//--The uSettingChangeFlag is added by jacky at 20080514----------------------
	unsigned char uSettingChangedUserName[16]; //64, the name of target user
#define LIVE_VIEW_USETTING_CHANGED        0x00000001 //bit 1, related cmd=0x00010203
#define LIVE_MESSAGE_USETTING_CHANGED     0x00000002 //bit 2, 0x00010401
#define DATE_TIME_FORMAT_USETTING_CHANGED 0x00000004 //bit 3, 0x00010403
#define REMORT_RECORDING_USETTING_CHANGED 0x00000008 //bit 4, 0x00030401
#define NUMBER_USETTING_CHANGED 4
	unsigned int uSettingChangedFlag; //68, user related setting
	//--The systemStatus is added by jacky at 20080514-----------------------------
#define SYS_STATUS_IP_CHANGED    0x00000001
#define SYS_STATUS_FLASH_BURNING 0x00000002
#define SYS_STATUS_REBOOTING     0x00000004
#define NUMBER_SYS_STATUS 3
	unsigned int systemStatus;      //72
	unsigned int reserved1[14];     //128-72=56;
	//==second part 128~255 byte==================================================
	unsigned char dateFmt;          //129 ???
	unsigned char timeFmt;          //130 ???
	unsigned char logoutNote;       //131
	unsigned char flashBurningPercentage; //132
	unsigned char rebootCountDown;  //133
	unsigned char ipChangeCountDown; //134
	unsigned char ipChangeType;     //135
	unsigned char ipChangeDest[4];  //139 destip, effect when ip type is static
	//--jimmy add 6 member--------------------------------------------------------
	unsigned char fanfail;          //140 phoenix, bit0~bit?
	unsigned char hddfail;          //141 phoenix, 1->hdd fail, 0->non fail
	unsigned char hddfull;          //142 phoenix, 1->hdd full, 0->non full
	unsigned char locallogin;       //143 phoenix, 1->login, 0->nologin
	unsigned char record;           //144 phoenix, 1->recording, 0->non record
	unsigned char temperature;      //145 phoenix, 1->over heat, 0->working well
	unsigned char relay;            //146 phoenix, bit0~bit3
	char notificationSD;			//147 used percentage of SD card
	unsigned char powerloss;        //148 1->powerloss
	unsigned char hddoff;           //149 0->switch off, 1->switch on
	unsigned char formatting;       //150
	unsigned char reserved2[106];   //256-150=106
} EVENT_CONTENT;    //total 256 bytes