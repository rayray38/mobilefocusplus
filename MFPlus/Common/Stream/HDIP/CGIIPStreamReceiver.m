//
//  CGIIPStreamReceiver.m
//  EFViewerHD
//
//  Created by James Lee on 12/10/26.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "CGIIPStreamReceiver.h"
#import "PSIAPTZController.h"
#import "HDIPAudioSender.h"

@implementation CGIIPStreamReceiver

#pragma mark - Streaming Control

- (id)initWithDevice:(Device *)device
{
    outputList = [[NSMutableArray alloc] init];
    NSString *nan = [NSString stringWithFormat:@"99"];
    for (NSInteger i=0; i<4; i++)
        [outputList addObject:nan];
    
    return [super initWithDevice:device];
}

- (BOOL)getDeviceInfo
{    
    BOOL ret = NO;
    
    if (![super getDeviceInfo]) {
        goto Exit;
    }
    
    cameraList = [[NSMutableArray alloc] init];
    ptzMask = 0;
    currentPlayCH = 0;
    
    // set available channels
	validChannel = 3; // 0011 // no use
    iMaxSupportChannel = 1;
    
    // get Streaming Info
    ret = [self getStreamInfo];
    
    if (!ret) {
        goto Exit;
    }
    
    if(blnStopStreaming)
    {
        goto Exit;
    }
    
    NSLog(@"[HDIP] Streaming Count:%lu",(unsigned long)cameraList.count);
    [self setStreaming];
    
    [self getSupportPTZMask];
    
    if(blnStopStreaming)
    {
        goto Exit;
    }
    
    blnReceivingStream = YES;
	// request quick streaming
	NSString *cmd = [NSString stringWithFormat:@"PSIA/Custom/EverFocus"];
	[cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
    blnReceivingStream = NO;
	
	// connection failed
	if (errorDesc!=nil) {
		
		ret =  NO;
		goto Exit;
	}
	
	if (blnStopStreaming) {
		
		goto Exit;
	}
	
	// parse response
	NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	//NSLog(@"response:%@",response);
	[cmdSender parseXmlResponse:response];
	[response release];
    
    return YES;
    
Exit:
	blnReceivingStream = NO;
	
	return NO;
}

- (BOOL)startLiveStreaming:(NSUInteger)channel {
    
	BOOL ret = YES;
	
	if ( ![super startLiveStreaming:currentCHMask] )
	{
		ret =  NO;
		goto Exit;
	}
    
    BOOL blnRtn = [self getDeviceInfo];
    
//    if(!blnRtn && self.errorDesc)
    if(!blnRtn)
    {// if get enable channel fail
        ret = NO;
        NSLog(@"[HDIP] Error:%@",self.errorDesc);
        goto Exit;
    }
    
    if (blnStopStreaming ) {
		
        ret = NO;
		goto Exit;
	}
    
	blnReceivingStream = YES;
	
	// initiate audio sender
	if (audioSender != nil) {
        [audioSender release];
        audioSender = nil;
    }
	audioSender = [[HDIPAudioSender alloc] initWithHost:host user:user passwd:passwd sid:nil];
	
    // initiate PTZ controller
	if (ptzController != nil) {
        
        [ptzController release];
        ptzController = nil;
    }
	ptzController = [[PSIAPTZController alloc] initWithHost:host sid:nil toKen:nil];
    if(DeviceType!=EPN4122_4220 && DeviceType!=EPN4122i_4220i && DeviceType!=EPN4230_P)
        ptzController.blnNcb = YES;
    
    NSString *cmd;
    cmd = [NSString stringWithFormat:@"PSIA/Custom/Everfocus/liveStream/%lu",(unsigned long)currentPlayCH];
	[cmdSender getData:cmd connection:&streamConnection synchronously:YES random:NO];
    
    if ( errorDesc!=nil )
		ret = NO;
	
Exit:
	blnReceivingStream = NO;
	
	return ret;
}

- (void)stopStreamingByView:(NSInteger)vIdx
{
    if (audioOn)
        [self closeSound];
    
    [super stopStreamingByView:vIdx];
    
    blnConnectionClear = YES;
}

- (void)changeLiveChannel:(NSInteger)channel
{
    // no-op
}

#pragma Audio Control

- (void)openSound {
    
	// create audio decoder
    if (audioControl!=nil) {
        
        NSLog(@"[CGIIP] Open sound");
        if (audioControl.codecId != AV_CODEC_ID_PCM_MULAW) {
            
            [audioControl close];
            [audioControl initWithCodecId:AV_CODEC_ID_PCM_MULAW srate:8000 bps:8 balign:audioSize fsize:audioSize channel:1];
            audioControl.codecId = audioControl.amFlag = AV_CODEC_ID_PCM_MULAW;
            NSLog(@"[CGIIP] Create new audio decoder with codec id %d", AV_CODEC_ID_PCM_MULAW);
        }
        
        [audioControl start];
        audioOn = YES;
    }
}

- (void)closeSound {
    
	NSLog(@"[CGIIP] Close sound");
    
    [audioControl stop];
    
    audioOn = NO;
}

- (void)dealloc {
//20150812 commented by ray lin
//    if (machineName) {
//        [machineName release];
//        machineName = nil;
//    }
	
	[super dealloc];
}



#pragma mart - Get Information

-(BOOL) getEnabledChannel :(NSInteger) iStream
{
    BOOL blnSend = YES;
    NSString *cmd = [NSString stringWithFormat:@"%s%ld",CMD_CHANNEL_INFO, (long)iStream];
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];

    // parse response
	NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	//NSLog(@"response:%@",response);
	[cmdSender parseXmlResponse:response];
    
    // Get Enabled channels fail
	if ( !blnSend) {
		self.errorDesc = NSLocalizedString(@"MsgInfoErr", nil);
        blnSend = NO;
    }
    [response release];
    return blnSend;
}

-(BOOL) getStreamInfo
{
    BOOL blnSend = YES;
    
    NSString *cmd = [NSString stringWithFormat:@"%s",CMD_CHANNEL_INFO];
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
    
    // parse response
	NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	//NSLog(@"response:%@",response);
	[cmdSender parseXmlResponse:response];
    blnReceivingStream = NO;
    
    if ([response isEqualToString:@""])
        blnSend = NO;
    
    [response release];
    return blnSend;
}

-(void)setStreaming
{
    if (cameraList.count != 0) {
        
        if (cameraList.count > 1 && self.dualStream == 1) {
            currentCHMask = currentPlayCH = cameraList.count-1;
            CameraInfo *tmpEntry = [cameraList objectAtIndex:cameraList.count-1];
            iRtspPort = tmpEntry.rtspPort;
            NSLog(@"[HDIP] currentCHMask:%lu",(unsigned long)currentCHMask);
        }
    }
}

-(BOOL)getSupportPTZMask
{
    BOOL blnRtn = YES;
    blnReceivingStream = YES;
    NSString *cmd = [NSString stringWithFormat:@"%s",CMD_PTZ_INFO];
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
    
    NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //NSLog(@"response:%@",response);
    [cmdSender parseXmlResponse:response];
    blnReceivingStream = NO;
    
    [response release];
    return blnRtn;
}

-(BOOL) getSupportPTZ :(NSInteger)channel
{
    BOOL blnRtn = NO;
    if(ptzMask  & (0x01 << (channel-1)))
        blnRtn = YES;
    
    return blnRtn;
}

#pragma mark - Parser Delegate

// Start of element
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
	attributes:(NSDictionary *)attributeDict
{
	if (blnEstablishTag || blnCheckChannelInfo || blnChSupportPTZ) {
        [currentXmlElement release];
		currentXmlElement = [elementName copy];
	}
    
    
	if ([elementName isEqualToString:@"EverFocusEstablish"]) {
		blnEstablishTag = YES;
	}
    else if ([elementName isEqualToString:@"StreamingChannel"]) {
		blnCheckChannelInfo = YES;
        blnNeedGetData = YES;
        CameraInfo  *emptyEntry = [[[CameraInfo alloc] init] autorelease];
        [cameraList addObject:emptyEntry];
    }
    else if ([elementName isEqualToString:@"PTZChannelList"]) {
        //isCheckPTZInfo = YES;
        blnNeedGetData = YES;
	}
}

/*
 Modify :
 1. by robert hsu 20110928
 porpose :
 (1). add parse channel support info
 (2). add parse ptz support info
 */
// Found Character
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSMutableString *)string
{
	
	if (blnEstablishTag) {
        
		if ( [currentXmlElement isEqualToString:@"Authority"] ) {
			
			if ( [string isEqualToString:@"SUPERVISOR"] ) {
				userLevel = 4;
			}
			else if ( [string isEqualToString:@"ADMIN"] ) {
				userLevel = 3;
			}
			else if ( [string isEqualToString:@"USER"] ) {
				userLevel = 2;
			}
			else if ( [string isEqualToString:@"GUEST"] ) {
				userLevel = 1;
			}
			else {
				userLevel = 0;
			}
		}
		else if ( [currentXmlElement isEqualToString:@"DeviceName"] ) {

            if (!machineName) {
                machineName = [string copy];
            }
            else
                machineName = [machineName stringByAppendingString:string];
		}
	}
    else if(blnCheckChannelInfo)
    {// parse channel info command add by robert hsu 20110928
        if(blnNeedGetData)
        {
            CameraInfo *currentEntry = [cameraList lastObject];
            // NSLog(@"Tag:%@",currentXmlElement);
            if ([currentXmlElement isEqualToString:@"id"])
            {// get current process channel
                iCurrentStream = [string intValue];
                currentEntry.index = iCurrentStream;
            }
            
            if ([currentXmlElement isEqualToString:@"enabled"])
            {// if enabled tag
                
                if ( [string isEqualToString:@"1"] )
                {//iCurrentStream [0~7] when evs410 ,count validChannel for EVS410
                    validChannel += 0x01 << iCurrentStream/2;
                    currentEntry.enable = YES;
                }else {
                    currentEntry.enable = NO;
                    blnNeedGetData = NO;
                }
            }
            
            if ([currentXmlElement isEqualToString:@"rtspPortNo"]) {
                
                currentEntry.rtspPort = [string integerValue];
                blnNeedGetData = NO;
            }
        }
    }
    else if(blnCheckPTZInfo) {//check PTZ support info
        
        if(blnNeedGetData) {
            //NSLog(@"Tag:%@",currentXmlElement);
            if ([currentXmlElement isEqualToString:@"id"]) {
                iCurrentStream = [string intValue];
                blnNeedGetData = YES;
            }
            
            if ([currentXmlElement isEqualToString:@"enabled"]) {
                if ( [string isEqualToString:@"true"] ) //iCurrentStream [0~7] when evs410
                    ptzMask += 0x01 << iCurrentStream;
            }
        }
    }
}

// End Element
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
{
	if ([elementName isEqualToString:@"EverFocusEstablish"]) {
		blnEstablishTag = NO;
	}
    else if ([elementName isEqualToString:@"StreamingChannel"]) {
		blnCheckChannelInfo = NO;
        blnNeedGetData = NO;
        CameraInfo *currentEntry = [cameraList lastObject];
        
        if (!currentEntry.enable)
            [cameraList removeObject:currentEntry];
	}
    else if ([elementName isEqualToString:@"PTZChannelList"]) {
        //isCheckPTZInfo = NO;
        blnNeedGetData = NO;
	}
}

#pragma mark - Connection Delegate

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.
{
	if ( theConnection==requestConnection ) {
		
		[responseData appendData:data];
	}
    else if ( theConnection==streamConnection ) {
		
		self.blnReceivingStream = YES;
		
		if ( blnStopStreaming ) return;
        
		NSInteger readLen = 0;
		NSInteger remanentLen = [data length];
        
        while ( remanentLen>0 ) {
			
			// read header data
			if ( headerOffset<sizeof(NET_MEDIA_HEAD) ) {
				
				Byte *buf = &pFrameBuf[headerOffset];
				int remains = sizeof(NET_MEDIA_HEAD)-headerOffset;
				NSInteger len = (remains>remanentLen)?remanentLen:remains;
				
				[data getBytes:buf range:NSMakeRange(readLen, len)];
				
				readLen += len;
				remanentLen -= len;
				headerOffset += len;
				
				// header data finish
				if ( headerOffset==sizeof(NET_MEDIA_HEAD) ) {
					
					NET_MEDIA_HEAD	*pHeader  = (NET_MEDIA_HEAD *)&pFrameBuf[0];
					
					// check header magic number
					if ( ntohl(pHeader->net_magic)!=NET_MAGIC ) {
						
                        self.errorDesc = NSLocalizedString(@"MsgDataErr", nil);
                        self.blnReceivingStream = NO;
                        [self stopStreamingByView:[[outputList objectAtIndex:0] integerValue]];
						return;
					}
					
					// convert header for different endian
					pHeader->size   = ntohl(pHeader->size);
					pHeader->sec    = ntohl(pHeader->sec);
					pHeader->u_sec  = ntohl(pHeader->u_sec);
					pHeader->height = ntohs(pHeader->height);
					pHeader->width  = ntohs(pHeader->width);
					pHeader->v_sn   = 0; // Nevio audio does not init to 0
					
					payloadSize = pHeader->size;
					frameWidth	= pHeader->width;
					frameHeight = pHeader->height;
				}
			}
			
			// read payload data
			if ( payloadOffset<payloadSize && remanentLen>0 ) {
				
				Byte *buf = &pFrameBuf[sizeof(NET_MEDIA_HEAD)+payloadOffset];
				long remains = payloadSize-payloadOffset;
				long len = (remains>remanentLen)?remanentLen:remains;
				
				[data getBytes:buf range:NSMakeRange(readLen, len)];
				
				readLen += len;
				remanentLen -= len;
				payloadOffset += len;
			}
			
			// read tail data
			if ( tailOffset<TAIL_SIZE && remanentLen>0 ) {
				
				Byte *buf = &pFrameBuf[sizeof(NET_MEDIA_HEAD)+payloadSize+tailOffset];
				int remains = TAIL_SIZE-tailOffset;
				NSInteger len = (remains>remanentLen)?remanentLen:remains;
				
				[data getBytes:buf range:NSMakeRange(readLen, len)];
				
				readLen += len;
				remanentLen -= len;
				tailOffset += len;
			}
			
			// finish getting one frame
			if ( tailOffset==TAIL_SIZE ) {
				
				NET_MEDIA_HEAD  *pHeader	= (NET_MEDIA_HEAD *)&pFrameBuf[0];
				void			*data		= (void *)&pFrameBuf[sizeof(NET_MEDIA_HEAD)];
				unsigned long   *pTail		= (unsigned long *)&pFrameBuf[sizeof(NET_MEDIA_HEAD)+payloadSize];
				
				// check tail data
				if ( ntohl(*pTail)!=NET_TAIL ) {
					
                    self.errorDesc = NSLocalizedString(@"MsgDataErr", nil);
                    self.blnReceivingStream = NO;
					[self stopStreamingByView:[[outputList objectAtIndex:0] integerValue]];
					return;
				}
				
				// process the frame here
                // decode and show the received VIDEO frame
                if ( pHeader->id_type==NET_H264I || pHeader->id_type==NET_H264P ||
                    pHeader->id_type==NET_HISI_H264I || pHeader->id_type==NET_HISI_H264P  ||
                    pHeader->id_type==NET_HISI_H265I || pHeader->id_type==NET_HISI_H265P  ||
                    pHeader->id_type==NET_MP4I  || pHeader->id_type==NET_MP4P  ||
                    pHeader->id_type==NET_JPEG ) {
                    
                    //NSLog(@"Receive completed: %d-%d",pHeader->i_sn,pHeader->f_sn);
                    
                    VIDEO_PACKAGE videoPkg;
                    
                    videoPkg.sec	= pHeader->sec;
                    videoPkg.ms		= pHeader->u_sec/1000;
                    videoPkg.seq	= pHeader->f_sn;
                    videoPkg.size	= pHeader->size;
                    videoPkg.width	= pHeader->width;
                    videoPkg.height	= pHeader->height;
                    videoPkg.data	= malloc(pHeader->size);
                    memcpy(videoPkg.data, data, pHeader->size);
                    NSInteger iBufferIdx = [[outputList objectAtIndex:0] intValue];
                    
                    switch (pHeader->id_type) {
                        case NET_H264I:
                        case NET_HISI_H264I:
                            videoPkg.codec	= AV_CODEC_ID_H264;
                            videoPkg.type	= VO_TYPE_IFRAME;
                            break;
                        case NET_H264P:
                        case NET_HISI_H264P:
                            videoPkg.codec	= AV_CODEC_ID_H264;
                            videoPkg.type	= VO_TYPE_PFRAME;
                            break;
                        case NET_HISI_H265I:
                            videoPkg.codec	= AV_CODEC_ID_HEVC;
                            videoPkg.type	= VO_TYPE_IFRAME;
                            break;
                        case NET_HISI_H265P:
                            videoPkg.codec	= AV_CODEC_ID_HEVC;
                            videoPkg.type	= VO_TYPE_PFRAME;
                            break;
                        case NET_MP4I:
                            videoPkg.codec	= AV_CODEC_ID_MPEG4;
                            videoPkg.type	= VO_TYPE_IFRAME;
                            //NSLog(@"GET I-Frame:%lu",pHeader->size);
                        case NET_MP4P:
                            videoPkg.codec	= AV_CODEC_ID_MPEG4;
                            videoPkg.type	= VO_TYPE_PFRAME;
                            //NSLog(@"GET P-Frame:%lu",pHeader->size);
                            break;
                        case NET_JPEG:
                            videoPkg.codec	= AV_CODEC_ID_MJPEG;
                            videoPkg.type	= VO_TYPE_IFRAME;
                            break;
                        default:
                            videoPkg.codec	= AV_CODEC_ID_NONE;
                            videoPkg.type	= VO_TYPE_NONE;
                            break;
                    }
                    
                    if (![machineName isEqualToString:[self.videoControl.aryTitle objectAtIndex:iBufferIdx]])
                        [self.videoControl setTitleByCH:iBufferIdx title:machineName];
                    
                    [self.videoControl putFrameIntoBufferByCH:&videoPkg chIndex:iBufferIdx];
                }
				
				// decode and play the received AUDIO frame
				else if ( pHeader->id_type==NET_PCMU)
				{
					if (audioControl!=nil && audioOn) {
                        
						//NSLog(@"[CGIIP] Receive audio");
                        
						// decode and play the audio frame
						NET_MEDIA_HEAD  *pHeader  = (NET_MEDIA_HEAD *)&pFrameBuf[0];
						Byte			*pPayload = &pFrameBuf[sizeof(NET_MEDIA_HEAD)];
                        
						[audioControl playAudio:pPayload length:pHeader->size];
					}
					else {
						//NSLog(@"Receive audio (OFF)");
					}
				}
                
				// reset parameters
				memset(&pFrameBuf[0],0,MAX_FRAME_SIZE); // clean buffer
				headerOffset=0;
				payloadOffset=0;
				tailOffset=0;
				payloadSize = 0;
			}
        }
    }
}

@end
