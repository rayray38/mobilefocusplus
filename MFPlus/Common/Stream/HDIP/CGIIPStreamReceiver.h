//
//  CGIIPStreamReceiver.h
//  EFViewerHD
//
//  Created by James Lee on 12/10/26.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreamReceiver.h"
#import "NevioNet.h"

#define CMD_CHANNEL_INFO    "PSIA/Streaming/channels"         //add by robert hsu 20110928
#define CMD_PTZ_INFO        "PSIA/PTZ/Channels"

@interface CGIIPStreamReceiver : StreamReceiver
{
    NSString *machineName;
	BOOL	 blnEstablishTag;
	
    Byte    pFrameBuf[MAX_FRAME_SIZE];
	int     payloadSize;
	int     headerOffset;
	int     payloadOffset;
	int     tailOffset;
	
	int audioSize;
    
    NSInteger iCurrentStream;
    BOOL blnCheckChannelInfo;    //add by robert hsu 20110928 for check current xml ack is channel info or not
    BOOL blnCheckPTZInfo;        //add by robert hsu 20110928 for check current xml ack is PTZ support info or not
    BOOL blnNeedGetData;        //add by robert hsu 20110928 a flag to check need to get value from xml node or not
    BOOL blnIsFirstIframe;      //add by James Lee 20120709 to avoid P-Frame send to decoder without I-Frame
    
    NSUInteger      ptzMask;
}

- (BOOL)getEnabledChannel : (NSInteger)iStream;
- (BOOL)getStreamInfo;
- (BOOL)getSupportPTZMask;
- (void)setStreaming;


@end
