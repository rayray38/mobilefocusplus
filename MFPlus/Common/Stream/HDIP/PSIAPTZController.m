//
//  PSIAPTZController.m
//  EFViewer
//
//  Created by  RD user on 2011/9/28.
//  Copyright 2011年 EverFocus. All rights reserved.
/*
    purpose :
        1. process PSIA PTZ command 
        2. inheritance from NevioPtzController
        3. only override sendCommand function
*/
#import "PSIAPTZController.h"

@interface PSIAPTZController ()

- (void)sendCommand;

@end

@implementation PSIAPTZController

#pragma mark -
#pragma mark Initialization Command

#pragma mark -
#pragma mark Private Functions

- (void)sendCommand
{
	byte[6] = (byte[0]+byte[1]+byte[2]+byte[3]+byte[4]+byte[5]) & 0x7F;
	
	NSString *urlString = [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X"
                            ,byte[0],byte[1],byte[2],byte[3],byte[4],byte[5],byte[6]];
	
    NSLog(@"[PSIA PTZ] CMD=%@",urlString);
    NSString *strSerial = [NSString stringWithFormat:@"%s",PTZ_CMD_SERIAL];
    strSerial = [strSerial stringByReplacingOccurrencesOfString:@"cmd"
                                         withString:@""];
    
    NSString *strCmd = [NSString stringWithFormat:@"%s",PTZ_CMD_ACTION];
    strCmd = [strCmd stringByReplacingOccurrencesOfString:@"cmd" withString:urlString];
    
    NSString *strActCH = [NSString stringWithFormat:@"%s", PTZ_CMD_CHAIN];
    strActCH = [strActCH stringByReplacingOccurrencesOfString:@"cmd" withString:[NSString stringWithFormat:@"%ld",(long)iCurrentCH]];
    
    NSString *cmd = [NSString stringWithFormat:@"%s%@%@%@%s"
                     ,PTZ_CMD_HEADER,strSerial,strActCH ,strCmd ,PTZ_CMD_TAIL];
    NSString *uri = [NSString stringWithFormat:@"%s",PTZ_CMD_URI];
	[self cancelConnection];
    [cmdSender putPSIACommand:cmd with:uri connection:&cmdConnection synchronously:NO];
}

- (void)sendCommandEx:(NSString *)cmd URI:(NSString *)uri
{
    NSLog(@"[PSIA PTZ] URI:%@, CMD=%@",uri,cmd);
    [self cancelConnection];
    [cmdSender putPSIACommand:cmd with:uri connection:&cmdConnection synchronously:NO];
}

#pragma mark -
#pragma mark CGI Command

- (void)ptzZoomIn:(NSInteger)ch
{
    if (!super.blnNcb) {
        [super ptzZoomIn:ch];
    }else {
        blnZoom = YES;
        NSString *cmd = @"<PTZData version=\"1.0\" xmlns=\"urn:psialliance-org\"><zoom>10</zoom></PTZData>";
        iCurrentCH = ch;
        [self sendCommandEx:cmd URI:PTZ_CMD_URI_ZOOM];
    }
}

- (void)ptzZoomOut:(NSInteger)ch
{
    if (!super.blnNcb) {
        [super ptzZoomOut:ch];
    }else {
        blnZoom = YES;
        NSString *cmd = @"<PTZData version=\"1.0\" xmlns=\"urn:psialliance-org\"><zoom>-10</zoom></PTZData>";
        iCurrentCH = ch;
        [self sendCommandEx:cmd URI:PTZ_CMD_URI_ZOOM];
    }
}

- (void)ptzStop:(NSInteger)ch
{
    if (!super.blnNcb) {
        [super ptzStop:ch];
    }else {
        
        if (blnZoom) {
            NSString *cmd = @"<PTZData version=\"1.0\" xmlns=\"urn:psialliance-org\"><zoom>0</zoom></PTZData>";
            iCurrentCH = ch;
            [self sendCommandEx:cmd URI:PTZ_CMD_URI_ZOOM];
            blnZoom = NO;
        }
        
        if (blnFocus) {
            NSString *cmd = @"<FocusData version=\"1.0\" xmlns=\"urn:psialliance-org\"><focus>0</focus></FocusData>";
            iCurrentCH = ch;
            [self sendCommandEx:cmd URI:PTZ_CMD_URI_FOCUS];
            blnFocus = NO;
        }
    }
}

- (void)ptzFocusFar:(NSInteger)ch
{
    if (!super.blnNcb) {
        [super ptzFocusFar:ch];
    }else {
        blnFocus = YES;
        NSString *cmd = @"<FocusData version=\"1.0\" xmlns=\"urn:psialliance-org\"><focus>5</focus></FocusData>";
        iCurrentCH = ch;
        [self sendCommandEx:cmd URI:PTZ_CMD_URI_FOCUS];
    }
    
}

- (void)ptzFocusNear:(NSInteger)ch
{
    if (!super.blnNcb) {
        [super ptzFocusNear:ch];
    }else {
        blnFocus = YES;
        NSString *cmd = @"<FocusData version=\"1.0\" xmlns=\"urn:psialliance-org\"><focus>-5</focus></FocusData>";
        iCurrentCH = ch;
        [self sendCommandEx:cmd URI:PTZ_CMD_URI_FOCUS];
    }
}


@end
