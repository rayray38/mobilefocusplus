//
//  HDIPStreamReceiver.h
//  LiveStream
//
//  Created by James Lee on 2011/4/18.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StreamReceiver.h"
#import "RTSPClientSession.h"

#define CMD_CHANNEL_INFO    "PSIA/Streaming/channels"         //add by robert hsu 20110928
#define CMD_PTZ_INFO        "PSIA/PTZ/Channels"

@interface HDIPStreamReceiver : StreamReceiver <RTSPSubsessionDelegate>
{
	NSString *machineName;
	BOOL	 blnEstablishTag;
	
	RTSPClientSession* rtspClientSession;
	NSArray* subsessions;
	char exit;
	
	int seq;
	uint8_t startBuf[4];
    VIDEO_PACKAGE spsPkg;
    VIDEO_PACKAGE ppsPkg;
	
	int audioSize;
    
    NSInteger iPTZCHMask ; // add by robert hsu 20110928 for keep enable PTZ channel mask
    NSInteger iCurrentStream;
    BOOL blnCheckChannelInfo;    //add by robert hsu 20110928 for check current xml ack is channel info or not
    BOOL blnCheckPTZInfo;        //add by robert hsu 20110928 for check current xml ack is PTZ support info or not
    BOOL blnNeedGetData;        //add by robert hsu 20110928 a flag to check need to get value from xml node or not
    BOOL blnIsFirstIframe;      //add by James Lee 20120709 to avoid P-Frame send to decoder without I-Frame
}

- (BOOL)getEnabledChannel : (NSInteger)iStream;
- (BOOL)getStreamInfo;
- (BOOL)getSupportPTZMask;
- (void)setStreaming;

@end