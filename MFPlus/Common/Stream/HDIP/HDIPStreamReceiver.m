//
//  HDIPStreamReceiver.mm
//  LiveStream
//
//  Created by James Lee on 2011/4/18.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import "HDIPStreamReceiver.h"
#import "HDIPAudioSender.h"
#import "PSIAPTZController.h"

@implementation HDIPStreamReceiver

#pragma mark - Streaming Control

- (id)initWithDevice:(Device *)device
{
    outputList = [[NSMutableArray alloc] init];
    NSString *nan = [NSString stringWithFormat:@"99"];
    for (NSInteger i=0; i<4; i++)
        [outputList addObject:nan];

    return [super initWithDevice:device];
}

- (BOOL)getDeviceInfo {
    
    BOOL ret = NO;
    
    [super getDeviceInfo];
    
    cameraList = [[NSMutableArray alloc] init];
    // set available channels
	validChannel = 3; // 0011 // no use
    iMaxSupportChannel = 1;
    
    // get Streaming Info
    ret = [self getStreamInfo];
    
    if (!ret) {
        goto Exit;
    }
    
    //NSLog(@"[HDIP] Streaming Count:%d",streamInfoList.count);
    [self setStreaming];
    
    //[self getSupportPTZMask];
    
	// request quick streaming
	NSString *cmd = [NSString stringWithFormat:@"PSIA/Custom/EverFocus"];
	[cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
	
	// connection failed
	if ( errorDesc!=nil ) {
		
		ret =  NO;
		goto Exit;
	}
	
	if ( blnStopStreaming ) {
		
		goto Exit;
	}
	
	// parse response
	NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	//NSLog(@"response:%@",response);
	[cmdSender parseXmlResponse:response];
	[response release];
    
    blnReceivingStream = NO;
    
    return YES;
    
Exit:
	blnReceivingStream = NO;
	
	return ret;
}

- (BOOL)startLiveStreaming:(NSUInteger)channel {
    
	BOOL ret = YES;
    if (![self getDeviceInfo]) {
        
        ret = NO;
        goto Exit;
    }
	
	if ( ![super startLiveStreaming:currentCHMask] )
	{
		ret =  NO;
		goto Exit;
	}
	
	// set initial parameters
	seq = 0;
	startBuf[0] = 0x00;
	startBuf[1] = 0x00;
	startBuf[2] = 0x00;
	startBuf[3] = 0x01;
	
	
	NSLog(@"[HDIP] Init RtspClientSession");
    
	///// RTSP /////
    // init RTSP client
    //modify by robert hsu 20120203 for add rtsp port 
	rtspClientSession = [[RTSPClientSession alloc] 
						 initWithURL:[NSString stringWithFormat:@"rtsp://%@:%ld/Streaming/channels/%lu", [self getIp] ,(long)iRtspPort, (unsigned long)currentCHMask]
						 user:user passwd:passwd];
    
	// get SDP and create session
	if (![rtspClientSession setup])
	{
		self.errorDesc = NSLocalizedString(@"MsgRtspErr1", nil);
        
        [rtspClientSession release];
        rtspClientSession = nil;
        
		ret =  NO;
		goto Exit;
	}
	
	// get subsession
	subsessions = [[rtspClientSession getSubsessions] retain];
	if([subsessions count]==0)
	{
		self.errorDesc = NSLocalizedString(@"MsgRtspErr2", nil);
		ret =  NO;
		goto Exit;
	}
    NSLog(@"[HDIP] Subsession Count:%lu",(unsigned long)[subsessions count]);
	
	// setup subsession
    BOOL videoAvailable = NO;
    
    blnIsFirstIframe = NO;
	for (int i=0; i<[subsessions count]; i++) {
#ifndef _NEW_RTSP
        if (![rtspClientSession setupSubsession:[subsessions objectAtIndex:i] useTCP:YES])//modify by robert hsu 2012.02.01 for use over tcp
		{
			self.errorDesc = NSLocalizedString(@"MsgRtspErr3", nil);
			ret =  NO;
			goto Exit;
		}
#endif
		RTSPSubsession* subsession = [subsessions objectAtIndex:i];
		NSLog(@"[HDIP] Subsession %@: MediumName=%@ CodecName=%@", 
			  [subsession getSessionId], [subsession getMediumName], [subsession getCodecName]);
        
        if ([[subsession getMediumName] isEqual:@"video"]) {
            videoAvailable = YES;
        }
		
		[[subsessions objectAtIndex:i] setDelegate:(id)self];
	}
    
    if(!videoAvailable)
	{
		self.errorDesc = NSLocalizedString(@"MsgRtspErr4", nil);
		ret =  NO;
		goto Exit;
	}
	
	// play
	[rtspClientSession play];
	blnReceivingStream = YES;
	
	// initiate audio sender
	if (audioSender != nil) {
        [audioSender release];
        audioSender = nil;
    }
	audioSender = [[HDIPAudioSender alloc] initWithHost:host user:user passwd:passwd sid:nil];
	
    // initiate PTZ controller
	if (ptzController != nil) {
        
        [ptzController release];
        ptzController = nil;
    }
	ptzController = [[PSIAPTZController alloc] initWithHost:host sid:nil toKen:nil];
    
	// run loop
	exit = 0;
	[rtspClientSession runEventLoop:&exit];
	
Exit:
	blnReceivingStream = NO;
	
	return ret;
}

- (void)stopStreamingByView:(NSInteger)vIdx {
	
	exit = 1;
	
	[super stopStreamingByView:vIdx];
    
    if (rtspClientSession) {
        
        BOOL ret = [rtspClientSession teardown];
        if (!ret) {
            NSLog(@"[HDIP] teardown failed");
        }
    }
    
    blnConnectionClear = YES;
}

- (void)changeLiveChannel:(NSInteger)channel
{
    // no-op
}

#pragma Audio Control

- (void)openSound {
    
	// create audio decoder
    if (audioControl!=nil) {
        
        if (audioControl.codecId != AV_CODEC_ID_PCM_MULAW) {
            
            [audioControl close];
            [audioControl initWithCodecId:AV_CODEC_ID_PCM_MULAW srate:8000 bps:8 balign:audioSize fsize:audioSize channel:1];
            audioControl.codecId = audioControl.amFlag = AV_CODEC_ID_PCM_MULAW;
            NSLog(@"[HDIP] Create new audio decoder with codec id %d", AV_CODEC_ID_PCM_MULAW);
        }
        
        [audioControl start];
        audioOn = YES;
    }
}

- (void)closeSound
{
    
	NSLog(@"[HDIP] Close sound");
    
    [audioControl stop];
    audioOn = NO;
}

- (void)dealloc
{
    if (ppsPkg.data != NULL) {
        free(ppsPkg.data);
    }
    if (spsPkg.data != NULL) {
        free(spsPkg.data);
    }
    if (machineName != NULL) {
        
        [machineName release];
    }
    if (rtspClientSession != NULL) {
        
        [rtspClientSession release];
    }
    if (subsessions != NULL) {
        
        [subsessions release];
    }
	
	[super dealloc];
}

#pragma mark - Parser Delegate

// Start of element
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
	attributes:(NSDictionary *)attributeDict
{
	if (blnEstablishTag || blnCheckChannelInfo || blnChSupportPTZ) {
        [currentXmlElement release];
		currentXmlElement = [elementName copy];
	}

    
	if ([elementName isEqualToString:@"EverFocusEstablish"]) {
		blnEstablishTag = YES;
	}
    else if ([elementName isEqualToString:@"StreamingChannel"]) {
		blnCheckChannelInfo = YES;
        blnNeedGetData = YES;
        CameraInfo *emptyEntry = [[[CameraInfo alloc] init] autorelease];
        [cameraList addObject:emptyEntry];
    }
    else if ([elementName isEqualToString:@"PTZChannelList"]) {
        //isCheckPTZInfo = YES;
        blnNeedGetData = YES;
	}

}

/*
    Moidyf :
        1. by robert hsu 20110928
            porpose :
                (1). add parse channel support info
                (2). add parse ptz support info
*/
// Found Character
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSMutableString *)string
{
	
	if (blnEstablishTag) {
		
		if ( [currentXmlElement isEqualToString:@"Authority"] ) {
			
			if ( [string isEqualToString:@"SUPERVISOR"] ) {
				userLevel = 4;
			}
			else if ( [string isEqualToString:@"ADMIN"] ) {
				userLevel = 3;
			}
			else if ( [string isEqualToString:@"USER"] ) {
				userLevel = 2;
			}
			else if ( [string isEqualToString:@"GUEST"] ) {
				userLevel = 1;
			}
			else {
				userLevel = 0;
			}
		}
		else if ( [currentXmlElement isEqualToString:@"DeviceName"] ) {
			
            if (machineName) {
                [machineName release];
            }
            machineName = [string copy];
		}
        else if ( [currentXmlElement isEqualToString:@"RtspPortNo"] ) {
            iRtspPort = [string integerValue];
        }
	}
    else if(blnCheckChannelInfo)
    {// parse channel info command add by robert hsu 20110928
        if(blnNeedGetData)
        {
            CameraInfo *currentEntry = [cameraList lastObject];
           // NSLog(@"Tag:%@",currentXmlElement);
            if ([currentXmlElement isEqualToString:@"id"]) 
            {// get current process channel
                iCurrentStream = [string intValue];
                currentEntry.index = iCurrentStream;
            }
            
            if ([currentXmlElement isEqualToString:@"enabled"])
            {// if enabled tag
                
                if ( [string isEqualToString:@"1"] )
                {//iCurrentStream [0~7] when evs410 ,count validChannel for EVS410
                    validChannel += 0x01 << iCurrentStream/2;
                    currentEntry.enable = YES;
                }else {
                    currentEntry.enable = NO;
                    blnNeedGetData = NO;
                }
            }
            
            if ([currentXmlElement isEqualToString:@"rtspPortNo"]) {
                
                currentEntry.rtspPort = [string integerValue];
                blnNeedGetData = NO;
            }
        }
    }
    else if(blnCheckPTZInfo)
    {//check PTZ support info
        if(blnNeedGetData)
        {
            //NSLog(@"Tag:%@",currentXmlElement);
            if ([currentXmlElement isEqualToString:@"id"]) 
            {
                iCurrentStream = [string intValue];
                blnNeedGetData = YES;
            }
            
            if ([currentXmlElement isEqualToString:@"enabled"]) 
            {
                if ( [string isEqualToString:@"true"] )
                {//iCurrentStream [0~7] when evs410
                    iPTZCHMask += 0x01 << iCurrentStream;
                }
                
            }

        }
    }
}

// End Element
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
{
	if ([elementName isEqualToString:@"EverFocusEstablish"]) {
		blnEstablishTag = NO;
	}
    else if ([elementName isEqualToString:@"StreamingChannel"]) {
		blnCheckChannelInfo = NO;
        blnNeedGetData = NO;
        CameraInfo *currentEntry = [cameraList lastObject];
        
        if (!currentEntry.enable)
            [cameraList removeObject:currentEntry];
	}
    else if ([elementName isEqualToString:@"PTZChannelList"]) {
        //isCheckPTZInfo = NO;
        blnNeedGetData = NO;
	}

}

#pragma mark - Connection Delegate

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.
{
	if ( theConnection==requestConnection ) {
		
		[responseData appendData:data];
	}
}

#pragma mark - RTSP delegate

- (void)didReceiveMessage:(NSString *)message
{
}

- (void)didReceiveFrame:(const uint8_t*)frameData
		frameDataLength:(int)frameDataLength
	   presentationTime:(struct timeval)presentationTime
 durationInMicroseconds:(unsigned)duration
			 subsession:(RTSPSubsession*)subsession
{
	//NSLog(@"[HDIP] Medium:%@, Codec:%@, Size:%d, Time:%ld",[subsession getMediumName],[subsession getCodecName],frameDataLength,presentationTime.tv_sec);
	
	// Video frame
	if ([[subsession getMediumName] isEqual:@"video"]) {
		
		//NSLog(@"[HDIP] Video frame: frameDataLength=%d presentationTime=%ld:%d", 
		//	  frameDataLength, presentationTime.tv_sec, presentationTime.tv_usec);
		
		// build video package
		
		VIDEO_PACKAGE videoPkg;
		
		videoPkg.sec	= presentationTime.tv_sec;
		videoPkg.ms		= presentationTime.tv_usec/1000;
		videoPkg.width	= 0; // don't care
		videoPkg.height	= 0; // don't care
        
        NSInteger iBufferIdx = [[outputList objectAtIndex:0] intValue];
		
		if ([[subsession getCodecName] isEqual:@"H264"]) { // H264
			
			videoPkg.codec	= AV_CODEC_ID_H264;
			
			if ( (frameData[0] & 0X1F) == 0X7) { // SPS
                
                if (spsPkg.data != NULL) {
                    free(spsPkg.data);
                }
				spsPkg.size	= sizeof(startBuf)+frameDataLength;
				spsPkg.data	= malloc(spsPkg.size);
				memcpy((spsPkg.data), startBuf, sizeof(startBuf));
				memcpy((spsPkg.data+sizeof(startBuf)), frameData, frameDataLength);
                //NSLog(@"Get SPS ---- frameData[0]:%x, size:%ld, startBuf:%lu, frameDataLength:%d",frameData[0],spsPkg.size,sizeof(startBuf),frameDataLength); 
                blnIsFirstIframe = NO;
                return;
                
			}else if ((frameData[0] & 0X0F) == 0X8) { // PPS
                
                if (ppsPkg.data != NULL) {
                    free(ppsPkg.data);
                }
                ppsPkg.size	= sizeof(startBuf)+frameDataLength;
				ppsPkg.data	= malloc(ppsPkg.size);
				memcpy((ppsPkg.data), startBuf, sizeof(startBuf));
				memcpy((ppsPkg.data+sizeof(startBuf)), frameData, frameDataLength);
                //NSLog(@"Get PPS ---- frameData[0]:%x, size:%ld, startBuf:%lu, frameDataLength:%d",frameData[0],ppsPkg.size,sizeof(startBuf),frameDataLength);
                return;
                
			}else if ((frameData[0] & 0X1F) == 0X5 ) { //I frame
                
				seq = 0;
				videoPkg.seq	= seq;
				videoPkg.type	= VO_TYPE_IFRAME;
				videoPkg.size	= spsPkg.size + ppsPkg.size + sizeof(startBuf)+frameDataLength;
				videoPkg.data	= malloc(videoPkg.size);
                memcpy((videoPkg.data), spsPkg.data, spsPkg.size);
                memcpy((videoPkg.data+spsPkg.size), ppsPkg.data, ppsPkg.size);
				memcpy((videoPkg.data+spsPkg.size+ppsPkg.size), startBuf, sizeof(startBuf));
				memcpy((videoPkg.data+spsPkg.size+ppsPkg.size+sizeof(startBuf)), frameData, frameDataLength);
                blnIsFirstIframe = YES;
                
                //NSLog(@"Get I frame ---- frameData[0]:%x, size:%ld, startBuf:%lu, frameDataLength:%d",frameData[0],videoPkg.size,sizeof(startBuf),frameDataLength);                
			}
			else if ( (frameData[0] & 0X1F) == 0X1) { // P frame
				
                if (!blnIsFirstIframe) {
                    NSLog(@"Waiting for I-Frame");
                    return;
                }
				seq++;
				videoPkg.seq	= seq;
				videoPkg.type	= VO_TYPE_PFRAME;
				videoPkg.size	= sizeof(startBuf)+frameDataLength;
				videoPkg.data	= malloc(videoPkg.size);
				memcpy(videoPkg.data, startBuf, sizeof(startBuf));
				memcpy((videoPkg.data+sizeof(startBuf)), frameData, frameDataLength);
                
                //NSLog(@"Get P frame ---- frameData[0]:%x, size:%ld seq:%d",frameData[0],videoPkg.size,seq); 
			}
			else {
				NSLog(@"[HDIP] Unknown %@ frame type", [subsession getCodecName]);
				return;
            }
        }
        else if ([[subsession getCodecName] isEqual:@"MP4V-ES"])
        {
            videoPkg.codec = AV_CODEC_ID_MPEG4;
            
            seq = 0;
            videoPkg.seq    = seq;
            videoPkg.type   = VO_TYPE_IFRAME;
            videoPkg.size   = frameDataLength;
            videoPkg.data   = malloc(videoPkg.size);
            memcpy(videoPkg.data, frameData, frameDataLength);
        }
		else if ([[subsession getCodecName] isEqual:@"JPEG"]) { // JPEG
			
			videoPkg.codec	= AV_CODEC_ID_MJPEG;
			
			seq = 0;
			videoPkg.seq	= seq;
			videoPkg.type	= VO_TYPE_IFRAME;
			videoPkg.size	= frameDataLength;
			videoPkg.data	= malloc(videoPkg.size);
			memcpy(videoPkg.data, frameData, frameDataLength);
		}
		else {
			NSLog(@"[HDIP] Unknown codec type: %@", [subsession getCodecName]);
			return;
		}

        if (![machineName isEqualToString:[self.videoControl.aryTitle objectAtIndex:iBufferIdx]])
            [self.videoControl setTitleByCH:iBufferIdx title:machineName];
        
		// put package to VideoOutput buffer
        [videoControl putFrameIntoBufferByCH:&videoPkg chIndex:iBufferIdx];
		
		// update frame size (get from decoder)
		frameWidth	= [videoControl getVideoWidthbyCH:iBufferIdx];
		frameHeight = [videoControl getVideoHeightbyCH:iBufferIdx];
	}
	
	// Audio frame
	else if ([[subsession getMediumName] isEqual:@"audio"]) {
	
		if (audioControl!=nil) {
			[audioControl playAudio:(Byte *)frameData length:frameDataLength];
		}
		else {
			// EQN:320 EAN:512
			audioSize = frameDataLength;
		}
	}
	else {
		NSLog(@"[HDIP] Unknown medium name: %@", [subsession getMediumName]);
	}
}

#pragma mart - Get Information

-(BOOL) getEnabledChannel :(NSInteger) iStream
{
    BOOL blnSend = YES;
    NSString *cmd;
    
    
    cmd = [NSString stringWithFormat:@"%s/%ld",CMD_CHANNEL_INFO, (long)iStream];
    
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
    // parse response
	NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	//NSLog(@"response:%@",response);
	[cmdSender parseXmlResponse:response];
    
    // Get Enabled channels fail
	if ( !blnSend) {
		self.errorDesc = NSLocalizedString(@"MsgInfoErr", nil);
        blnSend = NO;
    }
    [response release];
    return blnSend;
}

-(BOOL) getStreamInfo
{
    BOOL blnSend = YES;
    NSString *cmd;
    
    cmd = [NSString stringWithFormat:@"%s",CMD_CHANNEL_INFO];
    
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
    // parse response
	NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	//NSLog(@"response:%@",response);
	[cmdSender parseXmlResponse:response];

    if (response == nil) {
        blnSend = NO;
    }
    
    [response release];
    
    return blnSend;
}

-(void)setStreaming
{
    currentCHMask = self.dualStream ? cameraList.count-1 : 0;
    CameraInfo *tmpEntry = [cameraList lastObject];
    iRtspPort = tmpEntry.rtspPort;
    NSLog(@"[HDIP] currentCHMask:%lu,RTSP:%ld",(unsigned long)currentCHMask,(long)iRtspPort);
}

-(BOOL) getSupportPTZMask
{
    BOOL blnRtn = YES;
    NSString *cmd;
    cmd = [NSString stringWithFormat:@"%s",CMD_PTZ_INFO];
    
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
    NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //NSLog(@"response:%@",response);
    [cmdSender parseXmlResponse:response];

    [response release];
    return blnRtn;
}

-(BOOL) getSupportPTZ :(NSInteger)channel
{
    BOOL blnRtn = NO;
    if(iPTZCHMask  & (0x01 << (channel-1)))
    {
        blnRtn = YES;
    }
    return blnRtn;
}
@end
