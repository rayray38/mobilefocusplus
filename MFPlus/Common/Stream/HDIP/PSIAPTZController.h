//
//  PSIAPTZController.h
//  EFViewer
//
//  Created by  RD user on 2011/9/28.
//  Copyright 2011年 EverFocus. All rights reserved.
//

#import "NevioPtzController.h"

#define PTZ_CMD_HEADER  "<SerialCommand version=\"1.0\" xmlns=\"urn:psialliance-org\">"
#define PTZ_CMD_TAIL    "</SerialCommand>"
#define PTZ_CMD_SERIAL  "<SerialPortId>cmd</SerialPortId>"
#define PTZ_CMD_CHAIN   "<chainNo>cmd</chainNo>"
#define PTZ_CMD_ACTION  "<command>cmd</command>"
#define PTZ_CMD_URI     "PSIA/System/Serial/ports/0/command"
#define PTZ_CMD_URI_ZOOM  @"PSIA/PTZ/channels/0/continuous"
#define PTZ_CMD_URI_FOCUS @"PSIA/System/Video/inputs/channels/0/focus"

@interface PSIAPTZController : NevioPtzController {
    
    BOOL blnZoom;
    BOOL blnFocus;
}

- (void)sendCommandEx:(NSString *)cmd URI:(NSString *)uri;

@end
