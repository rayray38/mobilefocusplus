//
//  XMSPTZController.m
//  EFViewerPlus
//
//  Created by Jamese Lee on 2015/1/19.
//  Copyright (c) 2015年 EverFocus. All rights reserved.
//

#import "XMSPTZController.h"

@implementation XMSPTZController

#pragma mark - Converter

- (NSString *)XMSCommand:(NSString *)_command with:(NSString *)_value
{
    NSString *cmdString = @"";
    
    
    NSDictionary *_parameter = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSString stringWithFormat:@"%ld",(long)self.speed],@"speed"
                                ,_value,@"number"
                                , nil];
    
    NSDictionary *_json = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"",@"from"
                           ,self.sessionId,@"eid"
                           ,_command,@"command"
                           ,_parameter,@"parameters"
                           , nil];
    
    cmdString = [self convertJSON2String:_json];
    return cmdString;
}

- (NSString *)convertJSON2String:(NSDictionary *)_json
{
    NSString *jsonString = @"";
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:_json
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    if (jsonData.length) {
        NSString *_target = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] autorelease];
        jsonString = [jsonString stringByAppendingString:_target];
    }
    
    return jsonString;
}

#pragma mark - CGI Command

- (void)ptzTiltDown:(NSInteger)ch
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=tilt&value=down",
                               self.sessionId,1<<ch];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"down" with:@"0"];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzTiltUp:(NSInteger)ch
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=tilt&value=up",
                               self.sessionId,1<<ch];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"up" with:@"0"];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzPanLeft:(NSInteger)ch
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=pan&value=left",
                               self.sessionId,1<<ch];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"left" with:@"0"];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzPanRight:(NSInteger)ch
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=pan&value=right",
                               self.sessionId,1<<ch];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"right" with:@"0"];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzZoomIn:(NSInteger)ch
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=zoom&value=in",
                               self.sessionId,1<<ch];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"zoom_in" with:@"0"];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzZoomOut:(NSInteger)ch
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=zoom&value=out",
                               self.sessionId,1<<ch];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"zoom_out" with:@"0"];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzZoomStop:(NSInteger)ch {
    
    [self ptzStop:ch];
}


- (void)ptzFocusFar:(NSInteger)ch
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=focus&value=far",
                               self.sessionId,1<<ch];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"focus_far" with:@"0"];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzFocusNear:(NSInteger)ch
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=focus&value=near",
                               self.sessionId,1<<ch];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"focus_near" with:@"0"];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzFocusStop:(NSInteger)ch
{
    [self ptzStop:ch];
}

- (void)ptzIrisOpen:(NSInteger)ch
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=iris&value=open",
                               self.sessionId,1<<ch];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"iris_open" with:@"0"];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzIrisClose:(NSInteger)ch
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=iris&value=close",
                               self.sessionId,1<<ch];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"iris_close" with:@"0"];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzStop:(NSInteger)ch
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=stop",
                               self.sessionId,1<<ch];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"stop" with:@"0"];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzPresetGo:(NSInteger)ch value:(NSInteger)value
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=go_preset&value=%ld",
                               self.sessionId,1<<ch,(long)value];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"goto_preset" with:[NSString stringWithFormat:@"%ld",(long)value]];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzPresetSet:(NSInteger)ch value:(NSInteger)value
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=set_preset&value=%ld",
                               self.sessionId,1<<ch,(long)value];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"set_preset" with:[NSString stringWithFormat:@"%ld",(long)value]];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzPresetCancel:(NSInteger)ch value:(NSInteger)value
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=clear_preset&value=%ld",
                               self.sessionId,1<<ch,(long)value]; // action is "clear_preset" NOT "clear"
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"clear_preset" with:[NSString stringWithFormat:@"%ld",(long)value]];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzAutoPanRun:(NSInteger)ch
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=auto_pan&value=%@",
                               self.sessionId,1<<ch,[self.ptzType isEqualToString:@"NVR"] ? @"2" : @"run"];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"run_autopan" with:@"0"];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzAutoPan360Run:(NSInteger)ch
{
    [self ptzAutoPanRun:ch];
}

- (void)ptzAutoPanStop:(NSInteger)ch
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=auto_pan&value=stop",
                               self.sessionId,1<<ch];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"autopan_stop" with:@"0"];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzPattern:(NSInteger)ch value:(NSInteger)value
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = @"";
        if ([self.ptzType isEqualToString:@"NVR"])
            urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=pattern&value=run&value2=%ld",self.sessionId,1<<ch,(long)value];
        else
            urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=pattern&value=%ld",self.sessionId,1<<ch,(long)value];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"run_pattern" with:[NSString stringWithFormat:@"%ld",(long)value]];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

- (void)ptzTour:(NSInteger)ch value:(NSInteger)value
{
    if ([self.ptzType isEqualToString:@"DVR"] || [self.ptzType isEqualToString:@"NVR"]) {
        NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=go_tour&value=%ld",
                               self.sessionId,1<<ch,(long)value];
        
        [self cancelConnection];
        [cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
    }
    else {
        NSString *urlString = [self XMSCommand:@"tour" with:[NSString stringWithFormat:@"%ld",(long)value]];
        
        [self cancelConnection];
        [cmdSender postCommand:urlString connection:&cmdConnection synchronously:NO withPath:@"api/PTZ/ptz" withToken:self.tokenKey];
    }
}

@end
