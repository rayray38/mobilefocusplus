//
//  XMSStreamReceiver.m
//  EFViewerPlus
//
//  Created by JamesLee on 2014/7/28.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import "XMSStreamReceiver.h"
#import "RTSPClientSession.h"
#import "XMSPTZController.h"

@interface XMSStreamReceiver()<RTSPSubsessionDelegate>
{
    BOOL  blnGetVehInfo;
    BOOL  blnGetVehStatus;
    BOOL  blnIsVehConnected;
    BOOL  blnReqVehStream;
    BOOL  blnReqVehPBStream;
    BOOL  blnGetVehStream;
    BOOL  blnGetVehPBStream;
    
    CameraInfo  *curDevice;
    NSString    *RtspUrl;
}

@property (nonatomic, strong) NSOperationQueue *queue;

- (void)sendHTTPPost:(NSString *)cmd withToken:(NSString *)tokenString playTime:(NSInteger)stTime;
- (void)parseJSonData:(NSDictionary *)_json;
- (void)getVehicleStatus:(CameraInfo *)vehicle;
- (void)requestVehicleStream:(CameraInfo *)vehicle;
- (void)requestVehiclePBStream:(CameraInfo *)vehicle withStartTime:(NSInteger)stTime;
- (void)removeMVRIfNeeded;

@end

@implementation XMSStreamReceiver

- (id)initWithDevice:(Device *)device
{
    memset(scH264, 0, sizeof(scH264));
    scH264[3] = 0x01;
    
    streamArray = [[NSMutableDictionary alloc] init];
    sidArray = [[NSMutableDictionary alloc] init];
    
    return [super initWithDevice:device];
}

- (void)dealloc
{
    [streamArray removeAllObjects];
    SAVE_FREE(streamArray);
    
    [sidArray removeAllObjects];
    SAVE_FREE(sidArray);
    
//    [channelList removeAllObjects];
//    SAVE_FREE(channelList);
//    
//    [vehicleList removeAllObjects];
//    SAVE_FREE(vehicleList);
//    
//    [v_channelList removeAllObjects];
//    SAVE_FREE(v_channelList);
//    
//    [currentCHList removeAllObjects];
//    SAVE_FREE(currentCHList);
    
    for (NSInteger n=0; n<MAX_SUPPORT_DISPLAY; n++)
        SAVE_FREE(keyBuf[n]);
    
    [super dealloc];
}

#pragma mark - Streaming Control

- (BOOL)startLiveStreaming:(NSUInteger)mask
{
    BOOL ret = YES;
    NSString *cmd;
    NSString *response;
    NSInteger iErrorCode = 0;
    blnStopStreaming = NO;
    blnVideoOK = NO;
    blnIsVehConnected = NO;
    
    index = 1;
    ptzMask = 0;
    blnGetP3Mask = NO;
    
    if ( ![super startLiveStreaming:mask] )
    {
        ret =  NO;
        goto Exit;
    }
    
    // reset parameters
    packetRemanent = 0;
    packetHeaderOffset = 0;
    stchHeaderOffset = 0;
    frameHeaderOffset = 0;
    frameOffset = 0;
    frameSize = 0;
    
    seq = 0;
    startBuf[0] = 0x00;
    startBuf[1] = 0x00;
    startBuf[2] = 0x00;
    startBuf[3] = 0x01;
    
    CameraInfo *camInfo;
    if (self.blnVehChannel) {
        
        @try {
            //Get vehicle status
            camInfo = [currentCHList objectAtIndex:mask];
            [self getVehicleStatus:camInfo];
            if (!blnIsVehConnected) {
                
                self.errorDesc = NSLocalizedString(@"MsgSigUnstable", nil);
                ret =  NO;
                goto Exit;
            }
            
            // xFleet live streaming request
            [self requestVehicleStream:camInfo];
            if (!blnGetVehStream) {
                
                self.errorDesc = NSLocalizedString(@"MsgSigUnstable", nil);
                ret =  NO;
                goto Exit;
            }
            
            [NSThread sleepForTimeInterval:3.0f];  //20160315 added by Ray Lin, thread sleep 2 sec waiting for vehicle live streaming.
        } @catch (NSException *exception) {
            NSLog(@"[XMS] Exception: %@", exception);
        } @finally {
            if (!camInfo) {
                ret = NO;
                goto Exit;
            }
        }
    }
    else if (self.blnDvrChannel) {
        
        @try {
            mask = log2(mask);
            camInfo = [currentCHList objectAtIndex:mask];
        } @catch (NSException *exception) {
            NSLog(@"[XMS] Exception: %@", exception);
        } @finally {
            if (!camInfo) {
                ret = NO;
                goto Exit;
            }
        }
    }
    else {
        @try {
            camInfo = [cameraList objectAtIndex:mask];
        } @catch (NSException *exception) {
            NSLog(@"[XMS] Exception: %@", exception);
        } @finally {
            if (!camInfo) {
                ret = NO;
                goto Exit;
            }
        }
    }
    
    // IPCam/MVR ----> rtsp ; DVR/NVR ----> P2Streaming
    if (camInfo.deviceType == XMS_IPCAM || camInfo.deviceType == XMS_RTSP || camInfo.deviceType == XMS_VEHICLE_CH) {
        
        ///// RTSP /////
        // init RTSP client
        //modify by robert hsu 20120203 for add rtsp port
        NSString *url = [NSString stringWithFormat:@"rtsp://%@:%ld/live/%ld/%ld", [self getIp], (long)camInfo.rtspPort, (long)camInfo.ptzID, (long)dualStream];
        
        RTSPClientSession *rtspClnt = [[RTSPClientSession alloc]
                                       initWithURL:url
                                       user:user_copy passwd:passwd_copy];
        NSLog(@"[XMS] URL:%@",url);
        
        //init ptz controller
        SAVE_FREE(ptzController);
        ptzController = [[XMSPTZController alloc] initWithHost:host sid:nil toKen:tokenKey];
        ptzController.speed = 1;
        ptzController.sessionId = [NSString stringWithFormat:@"%ld",(long)camInfo.ptzID];
        
        // get SDP and create session
        if (![rtspClnt setup])
        {
            self.errorDesc = [NSString stringWithFormat:@"%@ - %@",camInfo.title, NSLocalizedString(@"MsgRtspErr1", nil)];
            SAVE_FREE(rtspClnt);
            
            ret =  NO;
            goto Exit;
        }
        
        // get subsession
        NSArray *subsessions = [[rtspClnt getSubsessions] retain];
        if (subsessions.count == 0) {
            
            self.errorDesc = [NSString stringWithFormat:@"%@ - %@",camInfo.title, NSLocalizedString(@"MsgRtspErr2", nil)];
            ret =  NO;
            goto Exit;
        }
        NSLog(@"[XMS] Subsession Count:%lu",(unsigned long)[subsessions count]);
        
        // setup subsession
        BOOL videoAvailable = NO;
        
        for (int i=0; i<[subsessions count]; i++) {
#ifndef _NEW_RTSP
            if (![rtspClnt setupSubsession:[subsessions objectAtIndex:i] useTCP:YES])//modify by robert hsu 2012.02.01 for use over tcp
            {
                self.errorDesc = [NSString stringWithFormat:@"%@ - %@",camInfo.title, NSLocalizedString(@"MsgRtspErr3", nil)];
                ret =  NO;
                goto Exit;
            }
#endif
            RTSPSubsession* subsession = [subsessions objectAtIndex:i];
            NSLog(@"[XMS] Subsession %@: MediumName=%@ CodecName=%@",
                  [subsession getSessionId], [subsession getMediumName], [subsession getCodecName]);
            
            if ([[subsession getMediumName] isEqual:@"video"]) {
                
                if (![[subsession getCodecName] isEqual:@"JPEG"]) {
                    
                    if ([[subsession getCodecName] isEqualToString:@"H265"]) {
                        [self HEVCParser:[subsession getVPSandSPSandPPS]];
                    }
                    else
                        [self SdpParser:[subsession getSDP_spropparametersets]];
                    
                }
                videoAvailable = YES;
            }
            
            [subsession setChannel:camInfo.index-1];
            [[subsessions objectAtIndex:i] setDelegate:(id)self];
        }
        
        if(!videoAvailable)
        {
            self.errorDesc = [NSString stringWithFormat:@"%@ - %@",camInfo.title, NSLocalizedString(@"MsgRtspErr4", nil)];
            ret =  NO;
            goto Exit;
        }
        
        // play
        NSNumber *keyIdx = [NSNumber numberWithInteger:camInfo.index-1];
        [streamArray setObject:rtspClnt forKey:keyIdx];
        [rtspClnt play];
        blnReceivingStream = YES;
        
        // run loop
        [rtspClnt runEventLoop];
        
        [streamArray removeObjectForKey:keyIdx];
    }
    else
    {
        // check device portforward or not, then reset host, user and password
        if (camInfo.portForward == 0) {
            [cmdSender setHost:[NSString stringWithFormat:@"%@:%ld",camInfo.ipAddr,(long)camInfo.httpPort]];
        }
        else
            [cmdSender setHost:[NSString stringWithFormat:@"%@:%ld",[self getIp],(long)camInfo.forwardPort]];
        
        [self setUser:camInfo.dev_user];
        [self setPasswd:camInfo.dev_password];
        
        blnGetDvrInfo = YES;
        if (camInfo.type == EPHD_04) {    //special case for EPHD04+, will use this detection method for all DVR later
            blnSupportNCB = NO;
            cmd = [NSString stringWithFormat:@"DvrInfo.xml"];
            [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
            if (errorDesc != nil) {
                errorDesc = nil;
                cmd = [NSString stringWithFormat:@"xml/DeviceInfo.xml"];
                [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
                
                if( errorDesc != nil)
                {
                    iErrorCode = -1;
                    ret = NO;
                    goto Exit;
                }
                blnSupportNCB = NO;
            }
        } else {
            blnSupportNCB = (camInfo.type >= 50);
            cmd = blnSupportNCB ? [NSString stringWithFormat:@"xml/DeviceInfo.xml"] : [NSString stringWithFormat:@"DvrInfo.xml"];
            [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
            if( errorDesc != nil)
            {
                iErrorCode = -1;
                ret = NO;
                goto Exit;
            }
        }
        
        if(blnStopStreaming)
        {
            goto Exit;
        }
        // parse dvr info response
        response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        //NSLog(@"[SR] response:%@",response);
        [cmdSender parseXmlResponse:response];
        [response release];
        
        // get login info
        cmd = [NSString stringWithFormat:@"Login_Info.cgi"];
        
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
        
        // connection failed
        if ( errorDesc!=nil ) {
            
            ret =  NO;
            goto Exit;
        }
        if (blnStopStreaming ) {
            
            goto Exit;
        }
        // parse login info response
        response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        //NSLog(@"[SR] Login response:%@",response);
        [cmdSender parseXmlResponse:response];
        [response release];
        blnGetDvrInfo = NO;
        
        // request session id
        if (blnSupportNCB) {
            cmd = [NSString stringWithFormat:@"Live.cgi?cmd=register_stream"];
        }else {
            NSString *auth = [Encryptor sha1:[NSString stringWithFormat:@"%@:%@",camInfo.dev_user,camInfo.dev_password]];
            cmd = [NSString stringWithFormat:@"Live.cgi?cmd=register_stream&auth=%@",auth];
        }

        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
        
        // connection failed
        if (errorDesc!=nil ) {
            
            iErrorCode = -2;
            ret =  NO;
            goto Exit;
        }
        
        if (blnStopStreaming ) {
            
            goto Exit;
        }
        
        // check session id
        response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSLog(@"[XMS] CID response:%@",response);
        if ( ([response rangeOfString:@"-1"].location!=NSNotFound) || ([response rangeOfString:@"-2"].location!=NSNotFound)  )
        {// if find " -1" in response string means that get session failed (some product with "-2")
            
            self.errorDesc = NSLocalizedString(@"MsgCidFail", nil);
            iErrorCode = -3;
            [response release];
            ret = NO;
            goto Exit;
        }
        
        // set sid
        if (sessionId) {
            [sessionId release];
        }
        sessionId = [response retain];
        NSLog(@"[XMS] sessionID:%@",sessionId);
        [response release];
        
        //init ptz controller
        SAVE_FREE(ptzController);
        if (camInfo.portForward == 0) {
            ptzController = [[XMSPTZController alloc] initWithHost:[NSString stringWithFormat:@"%@:%ld",camInfo.ipAddr,(long)camInfo.httpPort] sid:sessionId toKen:tokenKey];
        }
        else
            ptzController = [[XMSPTZController alloc] initWithHost:[NSString stringWithFormat:@"%@:%ld",[self getIp],(long)camInfo.forwardPort] sid:sessionId toKen:tokenKey];
        
        ptzController.speed = 1;
        ptzController.blnNcb = blnSupportNCB;
        
        if (camInfo.deviceType == XMS_NVR)
            ptzController.ptzType = @"NVR";
        else
            ptzController.ptzType = @"DVR";
        
        SAVE_FREE(audioSender);
        audioSender = [[P2AudioSender alloc] initWithHost:host user:user passwd:passwd sid:sessionId];
        audioSender.blnSupportNCB = blnSupportNCB;
#ifdef BPS_DISPLAY
        connectionTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(getBps) userInfo:nil repeats:YES]; //J-BPS
#endif
        
        // start live streaming
        cmd = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%lu&meta=1&cmd=stream_ctrl&action=start&video_type=%ld",sessionId,(unsigned long)currentCHMask,(long)dualStream];
        [cmdSender getData:cmd connection:&streamConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
        
        if ( errorDesc!=nil ) {
            iErrorCode = -4;
            ret = NO;
            goto Exit;
        }
    }
    
Exit:
    self.blnReceivingStream = NO;
    //[self stopStreamingByView:mask];
    ret = NO;
    
    return ret;
}

- (BOOL)startPlaybackStreaming:(NSUInteger)mask
{
    BOOL ret = YES;
    NSString *cmd;
    NSDictionary *response;
    NSInteger iErrorCode = 0;
    blnStopStreaming = NO;
    blnVideoOK = NO;
    audioOn = NO;
    
    if ( ![super startPlaybackStreaming:mask] )
    {
        ret =  NO;
        goto Exit;
    }
    
    // reset parameters
    packetRemanent = 0;
    packetHeaderOffset = 0;
    stchHeaderOffset = 0;
    frameHeaderOffset = 0;
    frameOffset = 0;
    frameSize = 0;
    m_SpeedIndex = 1;
    NSUInteger playTime = (m_intPlaybackStartTime ? m_intPlaybackStartTime : lastTimeTick+1);
    
    //CameraInfo *camInfo = [cameraList objectAtIndex:currentCHMask];
    CameraInfo *camInfo;
    if (self.blnVehChannel) {
        
        @try {
            //Get vehicle status
            camInfo = [currentCHList objectAtIndex:mask];
            [self getVehicleStatus:camInfo];
            if (!blnIsVehConnected) {
                
                self.errorDesc = NSLocalizedString(@"MsgSigUnstable", nil);
                ret =  NO;
                goto Exit;
            }
            
            // xFleet live streaming request
            [self requestVehicleStream:camInfo];
            if (!blnGetVehStream) {
                
                self.errorDesc = NSLocalizedString(@"MsgSigUnstable", nil);
                ret =  NO;
                goto Exit;
            }
            
            [NSThread sleepForTimeInterval:2.0f];  //20160315 added by Ray Lin, thread sleep 2 sec waiting for vehicle live streaming.
        } @catch (NSException *exception) {
            NSLog(@"[XMS] Exception: %@", exception);
        } @finally {
            if (!camInfo) {
                ret = NO;
                goto Exit;
            }
        }
    }
    else if (self.blnDvrChannel) {
        
        @try {
            mask = log2(mask);
            camInfo = [currentCHList objectAtIndex:mask];
        } @catch (NSException *exception) {
            NSLog(@"[XMS] Exception: %@", exception);
        } @finally {
            if (!camInfo) {
                ret = NO;
                goto Exit;
            }
        }
    }
    else {
        @try {
            camInfo = [cameraList objectAtIndex:mask];
        } @catch (NSException *exception) {
            NSLog(@"[XMS] Exception: %@", exception);
        } @finally {
            if (!camInfo) {
                ret = NO;
                goto Exit;
            }
        }
    }
    
    if (camInfo.deviceType == XMS_IPCAM || camInfo.deviceType == XMS_RTSP || camInfo.deviceType == XMS_VEHICLE_CH) {
        //Get TokenInfo
        blnGetXmsInfo = YES;
        
        if (camInfo.deviceType == XMS_VEHICLE_CH) {
            [cmdSender setHost:[NSString stringWithFormat:@"%@", camInfo.ipAddr]];
        }
        else
            [cmdSender setHost:host];
        
        cmd = [NSString stringWithFormat:@"Login"];
        //    NSString *auth = [Encryptor base64:@"admin:11111111"];
        //    NSString *header = [NSString stringWithFormat:@"Basic %@",auth];
        //    NSLog(@"BaseHeader : %@",header);
        [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES withuser:user_copy withpwd:passwd_copy];
        
        //connection failed
        if (errorDesc!=nil || blnStopStreaming) {
            
            iErrorCode = -2;
            ret = NO;
            goto Exit;
        }
        response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        [self parseJSonData:response];
        blnGetXmsInfo = NO;
        
        blnGetSearchInfo = YES;
        currentCHMask = mask;
        NSString *url = nil;
        
        if (camInfo.deviceType == XMS_VEHICLE_CH) {
            // xFleet playback streaming request
            [self requestVehiclePBStream:camInfo withStartTime:playTime];
            
            if (!blnGetVehPBStream || RtspUrl == nil || sessionId == nil) {
                self.errorDesc = NSLocalizedString(@"MsgSigUnstable", nil);
                ret =  NO;
                goto Exit;
            }
            
            url = [NSString stringWithFormat:@"%@",RtspUrl];
        }
        else {
            //Get Session ID
//            cmd = [NSString stringWithFormat:@"api/Device/GetPlaybackUrl?strDeviceID=%@&intStream=%d&intDateTime=%ld"
//                   ,camInfo.strDeviceID,(dualStream?1:0),(unsigned long)playTime];
            cmd = [NSString stringWithFormat:@"api/Device/GetPlaybackUrl?strDeviceID=%@&intStream=0&intDateTime=%ld"
                   ,camInfo.strDeviceID,(unsigned long)playTime];
            
            NSString *strToken = [NSString stringWithFormat:@"Bearer %@",tokenKey];
            //NSLog(@"[XMS] strToken : %@",strToken);
            
            [cmdSender getData:cmd connection:&requestConnection synchronously:YES withHeader:strToken];
            
            // connection failed
            if (errorDesc!=nil || blnStopStreaming) {
                
                ret = NO;
                goto Exit;
            }
            response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
            [self parseJSonData:response];
            blnGetSearchInfo = NO;
            if (!sessionId) {
                ret = NO;
                goto Exit;
            }
            
            url = [NSString stringWithFormat:@"rtsp://%@:%ld/playback/%@", [self getIp],(long)camInfo.rtspPort,sessionId];
        }
        
        ///// RTSP /////
        // init RTSP client
        //modify by Ray Lin 20150626 for add rtsp port
        
        RTSPClientSession *rtspClnt = [[RTSPClientSession alloc]
                                       initWithURL:url
                                       user:user passwd:passwd];
        NSLog(@"[XMS] URL:%@",url);
        
        // get SDP and create session
        if (![rtspClnt setup])
        {
            self.errorDesc = NSLocalizedString(@"MsgRtspErr1", nil);
            SAVE_FREE(rtspClnt);
            
            ret =  NO;
            goto Exit;
        }
        
        // get subsession
        NSArray *subsessions = [rtspClnt getSubsessions];
        if (0 == subsessions.count) {
            
            self.errorDesc = NSLocalizedString(@"MsgRtspErr2", nil);
            ret =  NO;
            goto Exit;
        }
        NSLog(@"[XMS] Subsession Count:%lu",(unsigned long)[subsessions count]);
        // setup subsession
        BOOL videoAvailable = NO;
        
        for (int i=0; i<[subsessions count]; i++) {
#ifndef _NEW_RTSP
            if (![rtspClnt setupSubsession:[subsessions objectAtIndex:i] useTCP:YES])//modify by robert hsu 2012.02.01 for use over tcp
            {
                self.errorDesc = NSLocalizedString(@"MsgRtspErr3", nil);
                ret =  NO;
                goto Exit;
            }
#endif
            RTSPSubsession* subsession = [subsessions objectAtIndex:i];
            NSLog(@"[XMS] Subsession %@: MediumName=%@ CodecName=%@",
                  [subsession getSessionId], [subsession getMediumName], [subsession getCodecName]);
            
            if ([[subsession getMediumName] isEqual:@"video"]) {
                videoAvailable = YES;
            }
            
            [subsession setChannel:camInfo.index-1];
            [[subsessions objectAtIndex:i] setDelegate:(id)self];
        }
        
        if(!videoAvailable)
        {
            self.errorDesc = NSLocalizedString(@"MsgRtspErr4", nil);
            ret =  NO;
            goto Exit;
        }
        
        // play
        NSNumber *keyIdx = [NSNumber numberWithInteger:camInfo.index-1];
        [streamArray setObject:rtspClnt forKey:keyIdx];
        [sidArray setObject:sessionId forKey:keyIdx];
        [rtspClnt play];
        blnReceivingStream = YES;
        
        // run loop
        [rtspClnt runEventLoop];
        
        [streamArray removeObjectForKey:keyIdx];
        [sidArray removeObjectForKey:keyIdx];
    }
    else
    {
        // check device portforward or not, then reset host, user and password
        if (camInfo.portForward == 0) {
            [cmdSender setHost:[NSString stringWithFormat:@"%@:%ld",camInfo.ipAddr,(long)camInfo.httpPort]];
        }
        else
            [cmdSender setHost:[NSString stringWithFormat:@"%@:%ld",[self getIp],(long)camInfo.forwardPort]];
        
        self.user = camInfo.dev_user;
        self.passwd = camInfo.dev_password;
        
        // request session id
        blnSupportNCB = (camInfo.type >= 50);
        if (blnSupportNCB) {
            cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=register_stream"];
        }else {
            NSString *auth = [Encryptor sha1:[NSString stringWithFormat:@"%@:%@",camInfo.dev_user,camInfo.dev_password]];
            cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=register_stream&auth=%@",auth];
        }
        
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
        
        // connection failed
        if (errorDesc!=nil ) {
            
            iErrorCode = -2;
            ret =  NO;
            goto Exit;
        }
        
        if (blnStopStreaming ) {
            
            goto Exit;
        }
        
        // check session id
        NSString *SID_response;
        SID_response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSLog(@"[XMS] CID response:%@",SID_response);
        if ( ([SID_response rangeOfString:@"-1"].location!=NSNotFound) || ([SID_response rangeOfString:@"-2"].location!=NSNotFound)  )
        {// if find " -1" in response string means that get session failed (some product with "-2")
            
            self.errorDesc = NSLocalizedString(@"MsgCidFail", nil);
            iErrorCode = -3;
            [SID_response release];
            ret = NO;
            goto Exit;
        }
        
        // set sid
        if (sessionId) {
            [sessionId release];
        }
        sessionId = [SID_response retain];
        NSLog(@"[XMS] sessionID:%@",sessionId);
        [SID_response release];
        
        if (self.blnEventMode) {
            
            NSInteger curChIndex = self.event.channel - 1;
            currentCHMask = (unsigned)0x1<<curChIndex;
        }
        
        NSNumber *keyIdx = [NSNumber numberWithInteger:camInfo.index-1];
        [sidArray setObject:sessionId forKey:keyIdx];
        
        // start playback streaming
        cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=stream_ctrl&stream_id=%@&action=start&camera=%lu&start_time=%lu&video_type=%ld",sessionId,(unsigned long)currentCHMask,(unsigned long)playTime,(long)dualStream];
        [cmdSender getData:cmd connection:&streamConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
        
        if ( errorDesc!=nil ) {
            iErrorCode = -4;
            ret = NO;
        }
        [sidArray removeObjectForKey:keyIdx];
    }
Exit:
    if (iErrorCode < -3 || blnStopStreaming) {
        cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=stream_ctrl&stream_id=%@&action=cancel",sessionId];
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
    }
    self.blnReceivingStream = NO;
    [self stopStreaming];
    
    return ret;
}

- (void)changeLiveChannel:(NSInteger)channel
{
    //channel is the ViewIdx, need to convert to channelIdx
    if (currentCHMask == channel)
        return;
    
    for (NSString *optCH in outputList)
    {
        if ([optCH integerValue] == channel) {
            currentCHMask = [outputList indexOfObject:optCH];
            break;
        }
    }
}

- (void)stopStreaming
{
    if ( blnStopStreaming ) return;
    blnStopStreaming = YES;
    
    if (audioOn)
        [self closeSound];
    
    if (dnsConnection != nil) {
        
        [dnsConnection cancel];
        dnsConnection = nil;
    }
    
    if (requestConnection != nil) {
        
        [requestConnection cancel];
        requestConnection = nil;
    }
    
    if (streamConnection != nil) {
        
        [streamConnection cancel];
        streamConnection = nil;
    }
    
    // wait for streaming stop
    while ( self.blnReceivingStream )
        [NSThread sleepForTimeInterval:0.001];
    
    [videoControl stop];
    //[videoControl reset];
    blnConnectionClear = YES;
    NSLog(@"[XMS] stopStreaming");
    
    //[super stopStreaming];
}

- (void)stopStreamingByView:(NSInteger)vIdx
{
    for (NSString *optCH in outputList)
    {
        if ([optCH integerValue] == vIdx) {
            
            CameraInfo *camInfo;
            if (self.blnVehChannel) {
                camInfo = [currentCHList objectAtIndex:[outputList indexOfObject:optCH]];
            }
            else
                camInfo = [cameraList objectAtIndex:[outputList indexOfObject:optCH]];
            
            NSNumber *keyIdx = [NSNumber numberWithInteger:camInfo.index-1];
            RTSPClientSession *rtspClnt = [streamArray objectForKey:keyIdx];
            NSInteger channel = [[[rtspClnt getSubsessions] firstObject] channel];
            
            [rtspClnt stop];
            
            while ([streamArray objectForKey:keyIdx])
                [NSThread sleepForTimeInterval:0.01f];
            
            if(![rtspClnt teardown])
                NSLog(@"[XMS] Close CH:%ld RTSP failed",(long)channel);
            
            [videoControl resetByCH:vIdx];
            break;
        }
    }
}

- (void)openSound
{
    audioOn = YES;
    [audioControl start];
}

- (void)closeSound
{
    audioOn = NO;
    [audioControl stop];
}

- (void)changePlaybackMode:(NSInteger)mode withValue:(NSInteger)value
{
    NSString *cmd;
    if (!sidArray.count)    return;
    
    for (NSString *keys in sidArray)
    {
        NSString *sid = [sidArray objectForKey:keys];
        //CameraInfo *camInfo = [cameraList objectAtIndex:[keys integerValue]];
        //NSLog(@"========  key : %ld  ========",(long)[keys integerValue]);
        CameraInfo *camInfo;
        if (self.blnDvrChannel) {
            camInfo = [channelList objectAtIndex:[keys integerValue]];
        }
        else if (self.blnVehChannel) {
            camInfo = [v_channelList objectAtIndex:[keys integerValue]];
        }
        else
            camInfo = [cameraList objectAtIndex:[keys integerValue]];
        
        if (camInfo.deviceType == XMS_IPCAM || camInfo.deviceType == XMS_RTSP || camInfo.deviceType == XMS_VEHICLE_CH) {
            
            switch (mode) {
                case PLAYBACK_FORWARD:
                    cmd = [NSString stringWithFormat:@"Device/PlaybackControl?strDeviceID=%@&intSession=%@&intDateTime=%ld&strCommand=forward&intSpeed=%d",camInfo.strDeviceID,sid,(long)lastTimeTick,1];
                    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:YES];
                    break;
                    
                case PLAYBACK_FAST_FORWARD :
                    cmd = [NSString stringWithFormat:@"Device/PlaybackControl?strDeviceID=%@&intSession=%@&intDateTime=%ld&strCommand=forward&intSpeed=%ld",camInfo.strDeviceID,sid,(long)lastTimeTick,(long)value];
                    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:YES];
                    break;
                    
                case PLAYBACK_FAST_BACKWARD :
                    cmd = [NSString stringWithFormat:@"Device/PlaybackControl?strDeviceID=%@&intSession=%@&intDateTime=%ld&strCommand=backward&intSpeed=%ld",camInfo.strDeviceID,sid,(long)lastTimeTick,(long)value];
                    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:YES];
                    break;
                    
                case PLAYBACK_PAUSE:
                    cmd = [NSString stringWithFormat:@"Device/PlaybackControl?strDeviceID=%@&intSession=%@&intDateTime=%ld&strCommand=pause&intSpeed=%d",camInfo.strDeviceID,sid,(long)lastTimeTick,1];
                    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:YES];
                    break;
            }
        }
        else {
            switch (mode) {
                case PLAYBACK_FORWARD:
                    cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=stream_ctrl&stream_id=%@&action=forward",sessionId];
                    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
                    break;
                    
                case PLAYBACK_FAST_FORWARD :
                    cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=stream_ctrl&stream_id=%@&action=fast-forward&speed=%ld",sessionId,(long)value];
                    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
                    break;
                    
                case PLAYBACK_FAST_BACKWARD :
                    cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=stream_ctrl&stream_id=%@&action=fast-backward&speed=%ld",sessionId,(long)value];
                    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
                    break;
                    
                case PLAYBACK_PAUSE:
                    cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=stream_ctrl&stream_id=%@&action=pause",sessionId];
                    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
                    break;
            }
        }
        
        if (self.errorDesc)
            NSLog(@"[XMS] Playback Error(%ld):%@",(long)mode,self.errorDesc);
    }
}

#pragma mark - Get Information

- (BOOL)getDeviceInfo
{
    BOOL ret = YES;
    NSString *cmd;
    NSDictionary *response;
    NSInteger iErrorCode = 0;
    
    // init output list to match channel and layout window
    if (!outputList) {
        outputList = [[NSMutableArray alloc] init];
        
        NSString *nan = [NSString stringWithFormat:@"99"];
        for (NSInteger i=0; i<256; i++) {
            [outputList addObject:nan];
        }
    }
    
    if (![super getDeviceInfo]) { //get DDNS if need
        ret =  NO;
		goto Exit;
    }
    
    // init camera list
    if (!cameraList) {
        cameraList = [[NSMutableArray alloc] init];
    }
    if (!channelList) {
        channelList = [[NSMutableArray alloc] init];
    }
    if (!vehicleList) {
        vehicleList = [[NSMutableArray alloc] init];
    }
    if (!v_channelList) {
        v_channelList = [[NSMutableArray alloc] init];
    }
    if (!currentCHList) {
        currentCHList = [[NSMutableArray alloc] init];
    }
    
    blnGetXmsInfo = YES;
    cmd = [NSString stringWithFormat:@"Login"];
    [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES withuser:self.user withpwd:self.passwd];
	//[cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
    
    // connection failed
	if (errorDesc!=nil || blnStopStreaming) {
		
        iErrorCode = -2;
        ret = NO;
        goto Exit;
    }
    response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
    if (!JSON_SUCCESS(response)) { //check json valid or not
        
        iErrorCode = -2;
        ret = NO;
        goto Exit;
    }
    [self parseJSonData:response];
    
    //Get TimeZone
    cmd = [NSString stringWithFormat:@"user/getUser"];
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
    
    // connection failed
    if (errorDesc!=nil || blnStopStreaming) {
        
        iErrorCode = -2;
        ret = NO;
        goto Exit;
    }
    response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
    [self parseJSonData:response];
    if (!m_DiskGMT) {
        
        cmd = [NSString stringWithFormat:@"configuration/get/timezone"];
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
        // connection failed
        if (errorDesc!=nil || blnStopStreaming) {
            
            iErrorCode = -2;
            ret = NO;
            goto Exit;
        }
        response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        [self parseJSonData:response];
    }
    if (m_DiskGMT) {
        NSTimeZone *tz = [[NSTimeZone alloc] initWithName:m_DiskGMT];
        timeZoneAdv = tz.secondsFromGMT;
        SAVE_FREE(tz);
    }
    
    blnGetXmsInfo = NO;
    
    //Get Channel info
    blnGetChannelInfo = YES;
    cmd = [NSString stringWithFormat:@"Device/getDeviceByType?DeviceType[]=IPCAM&DeviceType[]=DVR&DeviceType[]=NVR&DeviceType[]=RTSP"];
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
    
    // connection failed
    if (errorDesc!=nil || blnStopStreaming) {
        
        iErrorCode = -2;
        ret = NO;
        goto Exit;
    }
    response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
    if (!JSON_SUCCESS(response)) {
        
        iErrorCode = -2;
        ret = NO;
        goto Exit;
    }
    //NSLog(@"[XMS] response : %@",response);
    [self parseJSonData:response];
    
    cmd = [NSString stringWithFormat:@"Device/getDeviceByType?DeviceType[]=Channel"];
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
    
    // connection failed
    if (errorDesc!=nil || blnStopStreaming) {
        
        iErrorCode = -2;
        ret = NO;
        goto Exit;
    }
    response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
    if (!JSON_SUCCESS(response)) {
        
        iErrorCode = -2;
        ret = NO;
        goto Exit;
    }
    //NSLog(@"[XMS] response : %@",response);
    [self parseJSonData:response];
    
    //20151214 added by Ray Lin, 因為上面parse出來的channel不一定會照順序，所以需要重新排序
    [self resortChannelList];
    
    blnGetChannelInfo = NO;
    
    //Get record time
    blnGetRecTime = YES;
    cmd = [NSString stringWithFormat:@"Device/GetRecTime"];
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
    
    // connection failed
    if (errorDesc!=nil || blnStopStreaming) {
        
        NSLog(@"[XMS] Can't get disk record time, please upgrade your firmware version.......");
        m_intDiskEndTime = [[NSDate date] timeIntervalSince1970];
        m_intDiskStartTime = m_intDiskEndTime - 60*60*24*14;
    }
    else {
        response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        //NSLog(@"[XMS] response : %@",response);
        [self parseJSonData:response];
    }
    
    blnGetRecTime = NO;
    
// 20151120 added by Ray Lin, for xms support vehicle tree view test, Get Vehicle information
    blnGetVehInfo = YES;
    cmd = [NSString stringWithFormat:@"api/Pkg/Action?PkgName=xFleetMenu"];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {  //iOS 7.0 and above support NSURLSession connection
        [self sendHTTPPost:cmd withToken:tokenKey playTime:nil];
    }
    else {
        [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES withToken:tokenKey];
        
        // connection failed
//        if (errorDesc!=nil || blnStopStreaming) {
//            
//            iErrorCode = -2;
//            ret = NO;
//            goto Exit;
//        }
        response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        [self parseJSonData:response];
    }
    blnGetVehInfo = NO;
    
// 20160316 added by Ray Lin, remove the MVR in camera list which has already be added into the vehicle MVR list
// (This bug should be modified by XMS side, not mobile...)
    [self removeMVRIfNeeded];
    
    self.blnReceivingStream = NO;
    return YES;
    
Exit:
	self.blnReceivingStream = NO;
    [self stopStreaming];
	
	return ret;
}

- (void)changeVideoMask:(NSUInteger)mask
{
    NSString* cmd;
    
    // change video mask
    if(sessionId) {
        NSInteger errCount = 0;
        while (!blnReceivingStream && errCount<100)
        {
            [NSThread sleepForTimeInterval:0.05f];
            errCount++;
        }
        //NSLog(@"[SR] sessionID = %@" ,sessionId);
        currentCHMask = mask;
        if (!self.m_blnPlaybackMode || mask==0){
            cmd = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%lu&cmd=stream_ctrl",sessionId,(unsigned long)currentCHMask];
        }else {
            cmd = [NSString stringWithFormat:@"Playback.cgi?stream_id=%@&camera=%lu&cmd=stream_ctrl",sessionId,(unsigned long)currentCHMask];
        }
        [cmdSender getData:cmd connection:&requestConnection synchronously:NO random:NO supportNCB:blnSupportNCB];
        
        if (audioOn)
            [self changeAudioChannel];
    }
}

- (NSInteger)getFrameStatus:(NSInteger)_Flag
{
    NSInteger status = 0;
    
    status |= FH_IS_ALARM(_Flag);
    // status |= FH_IS_EVENT(_Flag);
    status |= FH_IS_MOTION(_Flag);
    status |= FH_IS_VLOSS(_Flag);
    //NSLog(@"[P2] Flag:%d, status:%d",_Flag,status);
    
    return status;
}

- (NSInteger)checkModelByName:(NSString *)devName
{
    NSInteger model = 99;
    
////DVR.NVR
    if ([devName rangeOfString:@"ECOR264" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        model = ECOR264_16X1;
        
    }
    else if ([devName rangeOfString:@"EMV"].location != NSNotFound) {
        
        if ([devName rangeOfString:@"1201"].location != NSNotFound)
            model = EMV1201;
        else if ([devName rangeOfString:@"01"].location != NSNotFound)
            model = EMV_401_801_1601;
        else if ([devName rangeOfString:@"400S" options:NSCaseInsensitiveSearch].location != NSNotFound)
            model = EMV400S_FHD;
        else if ([devName rangeOfString:@"FHD"].location != NSNotFound)
            model = EMV_FHD_SERIES;
        else if ([devName rangeOfString:@"HD"].location != NSNotFound)
            model = EMV_400_800_1200_HD;
        else
            model = EMV_400_800_1200;
        
    }
    else if ([devName rangeOfString:@"EMX"].location != NSNotFound)
        model = EMX32;
    
    else if ([devName rangeOfString:@"960"].location != NSNotFound) {
        if ([devName rangeOfString:@"ECOR"].location != NSNotFound) {
            if ([devName rangeOfString:@"x1" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                model = ECOR960_16X1;
            }
            else
                model = ECOR960_4_8_16F2;
            
        }
    }
    else if ([devName rangeOfString:@"Paragon" options:NSCaseInsensitiveSearch].location != NSNotFound) {          //20160329 added by Ray Lin, put Paragon Series together
        if ([devName rangeOfString:@"FHD" options:NSCaseInsensitiveSearch].location != NSNotFound)
            model = PARAGON_FHD;
        else if ([devName rangeOfString:@"264"].location != NSNotFound) {
            if ([devName rangeOfString:@"x1" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                model = PARAGON264_16X1;
            }
            else if ([devName rangeOfString:@"x2" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                model = PARAGON264_16X2;
            }
            else
                model = PARAGON264_16X4;
        }
        else if ([devName rangeOfString:@"960"].location != NSNotFound) {
            if ([devName rangeOfString:@"x1" options:NSCaseInsensitiveSearch].location != NSNotFound)
                model = PARAGON960_16X1;
            else
                model = PARAGON960_16X4;
        }
        else
            model = EPARA264_32;
    }
    else if ([devName rangeOfString:@"Paragon32x4" options:NSCaseInsensitiveSearch].location != NSNotFound)
        model = EPARA264_32;
    
    else if ([devName rangeOfString:@"EPHD"].location != NSNotFound) {
        if ([devName rangeOfString:@"08+"].location != NSNotFound)
            model = EPHD08_PLUS;
        else
            model = EPHD_04;
    }
    else if([devName rangeOfString:@"ECORHD"].location != NSNotFound) {
        if ([devName rangeOfString:@"X1" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            model = ECORHD_16X1;
        }
        else
            model = ECORHD_4_8_16F;
    }
    else if([devName rangeOfString:@"ECOR FHD"].location != NSNotFound) {
        model = ECOR_FHD_SERIES;
    }
    else if([devName rangeOfString:@"ELUX"].location != NSNotFound) {
        model = ELUX_SERIES;
    }
    else if ([devName rangeOfString:@"NVR"].location != NSNotFound)
    {
        if ([devName rangeOfString:@"8304"].location != NSNotFound)
            model = ENVR8304D_E_X;
        else if ([devName rangeOfString:@"8316"].location != NSNotFound)
            model = ENVR8316E;
        else
            model = NVR8004X;
    }
    else if ([devName rangeOfString:@"EDR HD"].location != NSNotFound)
        model = EDR_HD_4H4;
    else if ([devName rangeOfString:@"ENDEAVOR"].location != NSNotFound)
        model = ENDEAVOR264X4;
    else if ([devName rangeOfString:@"TUTIS"].location != NSNotFound)
        model = TUTIS_4_8_16F3;
    
////IPCAM
    else if([devName rangeOfString:@"EAN3"].location != NSNotFound) {
        if([devName rangeOfString:@"3200"].location != NSNotFound)
            model = EAN3200;
        else
            model = EAN3300;
    }
    else if([devName rangeOfString:@"EAN7"].location != NSNotFound) {
        
        if([devName rangeOfString:@"221"].location != NSNotFound || [devName rangeOfString:@"260"].location != NSNotFound || [devName rangeOfString:@"360"].location != NSNotFound)
            model = EAN7221_7260_7360;
        else if([devName rangeOfString:@"220"].location != NSNotFound)
            model = EAN7220;
        else
            model = EAN7200;
    }
    else if([devName rangeOfString:@"EAN8"].location != NSNotFound || [devName rangeOfString:@"EAN9"].location != NSNotFound)
        model = EAN800A_AW;
    else if([devName rangeOfString:@"EBN2"].location != NSNotFound)
        model = EBN268_V;
    else if([devName rangeOfString:@"EBN3"].location != NSNotFound)
        model = EBN368_V;
    else if([devName rangeOfString:@"EDN1"].location != NSNotFound)
        model = EDN1120_1220_1320;
    else if([devName rangeOfString:@"EDN228"].location != NSNotFound)
        model = EDN228;
    else if([devName rangeOfString:@"EDN3240"].location != NSNotFound)
        model = EDN3240;
    else if([devName rangeOfString:@"EDN368"].location != NSNotFound)
        model = EDN368M;
    else if([devName rangeOfString:@"EDN3"].location != NSNotFound)
        model = EDN3160;
    else if([devName rangeOfString:@"EDN8"].location != NSNotFound)
        model = EDN800;
    else if([devName rangeOfString:@"EDN"].location != NSNotFound)
        model = EDN2160_2260_2560;
    else if([devName rangeOfString:@"EFN3"].location != NSNotFound) {
        if ([devName rangeOfString:@"c"].location != NSNotFound) {
            model = EFN3320c_3321c;
        }
        else
            model = EFN3320_3321;
    }
    else if([devName rangeOfString:@"EHN"].location != NSNotFound) {
        if([devName rangeOfString:@"EHN1"].location != NSNotFound)
            model = EHN1120_1220_1320;
        else if([devName rangeOfString:@"EHN7"].location != NSNotFound)
            model = EHN7221_7260_7360;
        else if ([devName rangeOfString:@"61"].location != NSNotFound) {
            model = EHN3261_3361;
        }
        else
            model = EHN3160;
    }
    else if([devName rangeOfString:@"EMN2"].location != NSNotFound)
        model = EMN2120_2220_2320;
    else if([devName rangeOfString:@"EPN5"].location != NSNotFound) {
        if ([devName rangeOfString:@"210"].location != NSNotFound) {
            model = EPN5210;
        }
        else if ([devName rangeOfString:@"230"].location != NSNotFound) {
            model = EPN5230;
        }
    }
    else if([devName rangeOfString:@"EPN4"].location != NSNotFound) {
        if ([devName rangeOfString:@"0d"].location != NSNotFound) {
            model = EPN4220_4230_D;
        }
        else if ([devName rangeOfString:@"230"].location != NSNotFound) {
            model = EPN4230_P;
        }
        else
            model = EPN4122_4220;
    }
    else if([devName rangeOfString:@"EPN3"].location != NSNotFound)
        model = EPN3100;
    else if([devName rangeOfString:@"ETN"].location != NSNotFound)
        model = ETN2160_2260_2560;
    else if([devName rangeOfString:@"EVS2"].location != NSNotFound)
        model = EVS200A_AW;
    else if([devName rangeOfString:@"EVS4"].location != NSNotFound)
        model = EVS410;
    else if([devName rangeOfString:@"EZN8"].location != NSNotFound)
        model = EZN850;
    else if([devName rangeOfString:@"EZN368"].location != NSNotFound)
        model = EZN368_V_M;
    else if([devName rangeOfString:@"EZN2"].location != NSNotFound)
        model = EZN268_V;
    else if([devName rangeOfString:@"EZN1"].location != NSNotFound)
        model = EZN1160_1260_1360;
    else if([devName rangeOfString:@"EZN7"].location != NSNotFound)
        model = EZN7221_7260_7360;
    else if([devName rangeOfString:@"EZN"].location != NSNotFound) {
        if ([devName rangeOfString:@"61"].location != NSNotFound) {
            model = EZN3261_3361;
        }
        else
            model = EZN3160;
    }
    else if ([devName rangeOfString:@"EQN"].location != NSNotFound) {
        if ([devName rangeOfString:@"22"].location != NSNotFound)
            model = EQN2200_2201;
        else if ([devName rangeOfString:@"21"].location != NSNotFound)
            model = EQN2101_2171;
    }
    else
        model = ONVIF;
    
    return model;
}

- (NSInteger)getCameraIndex:(NSString *)_id FromArray:(NSMutableArray *)_array
{
    NSInteger _index = 0;
    
    _index = [_array indexOfObject:_id];
    
    return _index+1;
}

#pragma mark - Get Vehicle Information

- (void)getVehicleStatus:(CameraInfo *)vehicle
{
    BOOL ret = YES;
    NSDictionary *response;
    NSInteger iErrorCode = 0;
    blnGetVehStatus = YES;
    curDevice = [[CameraInfo alloc] init];
    curDevice = [vehicle retain];
    NSString *cmd = @"api/Pkg/Action?PkgName=xFleetDevMgr";
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [self sendHTTPPost:cmd withToken:tokenKey playTime:nil];
    }
    else {
        [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES withToken:tokenKey];
        
        // connection failed
        if (errorDesc!=nil || blnStopStreaming) {
            
            iErrorCode = -2;
            ret = NO;
        }
        response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        [self parseJSonData:response];
    }
    blnGetVehStatus = NO;
    SAVE_FREE(curDevice);
}

- (void)requestVehicleStream:(CameraInfo *)vehicle
{
    BOOL ret = YES;
    NSDictionary *response;
    NSInteger iErrorCode = 0;
    blnReqVehStream = YES;
    curDevice = [[CameraInfo alloc] init];
    curDevice = [vehicle retain];
    NSArray *tmpCHIDArray = [[NSArray alloc] initWithObjects:curDevice.path, nil];
    NSString *cmd = @"api/Pkg/Action?PkgName=xFleetVideoPlay";
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [self sendHTTPPost:cmd withToken:tokenKey playTime:0];
    }
    else {
        [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES withToken:tokenKey withStartTime:0 chIDAry:tmpCHIDArray];
        
        // connection failed
        if (errorDesc!=nil || blnStopStreaming) {
            
            iErrorCode = -2;
            ret = NO;
        }
        response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        [self parseJSonData:response];
    }
    blnReqVehStream = NO;
    SAVE_FREE(curDevice);
}

- (void)requestVehiclePBStream:(CameraInfo *)vehicle withStartTime:(NSInteger)stTime
{
    BOOL ret = YES;
    NSDictionary *response;
    NSInteger iErrorCode = 0;
    blnReqVehPBStream = YES;
    curDevice = [[CameraInfo alloc] init];
    curDevice = [vehicle retain];
    NSArray *tmpCHIDArray = [[NSArray alloc] initWithObjects:curDevice.path, nil];
    NSString *cmd = @"api/Pkg/Action?PkgName=xFleetVideoPlay";
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [self sendHTTPPost:cmd withToken:tokenKey playTime:stTime];
    }
    else {
        [cmdSender postCommand:cmd connection:&requestConnection synchronously:YES withToken:tokenKey withStartTime:stTime chIDAry:tmpCHIDArray];
        
        // connection failed
        if (errorDesc!=nil || blnStopStreaming) {
            
            iErrorCode = -2;
            ret = NO;
        }
        response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        [self parseJSonData:response];
    }
    blnReqVehPBStream = NO;
    SAVE_FREE(curDevice);
}

/* 設定sort的值 利用NSSortDescriptor的sortDescriptorWithKey: ascending: 方法
 ** sortDescriptorWithKey:@"index" , 用CameraInfo.index來當作排序的依據
 ** ascending:YES , 可設定YES or NO , YES會從數字小的排到大 , NO就由大到小
 ** 透過Array的sortUsingDescriptors方法來排序, 參數要使用一個Array 使用NSArray的arrayWithObjects方法 , 再將剛才設定的NSSortDescriptor丟入即可排序
 */
- (void)resortChannelList
{
    NSSortDescriptor *sort;
    sort = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
    
    if (cameraList.count) {
        if (channelList.count) {
            [channelList sortUsingDescriptors:[NSArray arrayWithObjects:sort, nil]];
        }
        
        if (v_channelList.count) {
            [v_channelList sortUsingDescriptors:[NSArray arrayWithObjects:sort, nil]];
        }
    }
}

- (void)removeMVRIfNeeded
{
    NSMutableArray *toRemove = [NSMutableArray array];
    for (CameraInfo *tmpMVR in cameraList) {
        if (tmpMVR.deviceType == XMS_DVR) {
            for (CameraInfo *tmpVeh in vehicleList) {
                if ([tmpVeh.path isEqual:tmpMVR.strDeviceID]) {
                    //NSLog(@"[XMS_SR] Remove :%@",tmpMVR.title);
                    [toRemove addObject:tmpMVR];
                }
            }
        }
    }
    
    if (toRemove.count > 0) {
        [cameraList removeObjectsInArray:toRemove];
        [self resortCameraIndex];
    }
}

- (void)resortCameraIndex
{
    NSInteger idx = 0;
    for (CameraInfo *tmpInfo in cameraList) {
        tmpInfo.index = ++idx;
    }
}

# pragma mark - NSURLSession Task

- (void)sendHTTPPost:(NSString *)cmd withToken:(NSString *)tokenString playTime:(NSInteger)stTime
{
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    NSURL *urlString = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",host,cmd]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlString];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:10.0f];
    
    // set headers
    NSString *contentType = [NSString stringWithFormat:@"application/json"];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSString *auth_header = [NSString stringWithFormat:@"Bearer %@",tokenString];
    [request setValue:auth_header forHTTPHeaderField:@"Authorization"];
    
    NSArray *tmpCHIDArray = [[NSArray alloc] initWithObjects:curDevice.path, nil];
    
//    set body
    NSDictionary *req_body = [NSDictionary new];
    if ([cmd rangeOfString:@"xFleetMenu" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        req_body = [NSDictionary dictionaryWithObjectsAndKeys:@"getDevice", @"action",
                                                              @"Vehicle", @"dtype", nil];
    }
    else if ([cmd rangeOfString:@"xFleetDevMgr" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        NSArray *req_array = [NSArray array];
        req_body = [NSDictionary dictionaryWithObjectsAndKeys:@"QueryVehStatus", @"action",
                                                              req_array, @"data", nil];
    }
    else if ([cmd rangeOfString:@"xFleetVideoPlay" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        if (stTime == 0) {
            req_body = [NSDictionary dictionaryWithObjectsAndKeys:@"PlayControl", @"action",
                                                                  tmpCHIDArray, @"ChannelDeviceID",
                                                                  @"sub", @"StreamType",
                                                                  @"StreamReq", @"CMD", nil];
        }
        else {
            req_body = [NSDictionary dictionaryWithObjectsAndKeys:@"PlayControl", @"action",
                                                                  tmpCHIDArray, @"ChannelDeviceID",
                                                                  @(stTime), @"StartTime",
                                                                  @"main", @"StreamType",
                                                                  @"start", @"CMD", nil];
        }
    }
    
    NSError *error;
    NSData *postBody = [NSJSONSerialization dataWithJSONObject:req_body options:0 error:&error];
    [request setHTTPBody:postBody];
    
    NSLog(@"[XMS POST] Request : %@",urlString);
    
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                                 completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                     if (error) {
                                                                         //handle error
                                                                         return ;
                                                                     }
                                                                     //parse data
                                                                     NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                                                     //NSLog(@"[XMS] response : %@",jsonData);
                                                                     [self parseJSonData:jsonData];
                                                                     
                                                                     dispatch_semaphore_signal(semaphore);
                                                                 }];
    [task resume];
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
}

#pragma mark - JSON Parser

- (void)parseJSonData:(NSDictionary *)_json
{
    NSDictionary *source;
    if (blnGetXmsInfo) {
        @try {
            source = _json;
            if (source.count) {
                if ([source valueForKey:@"TokenInfo"] != NULL) {
                    tokenKey = [[source valueForKey:@"TokenInfo"] copy];
                }
                if ([source valueForKey:@"timezone"] != NULL) {
                    m_DiskGMT = [[source valueForKey:@"timezone"] copy];
                }
            }
        }
        @catch (NSException *exception) {
            RELog(@"[XMS] Get XMS Information Exception :%@",exception.reason);
        }
    }
    if (blnGetChannelInfo) {
        @try {
            source = [_json objectForKey:@"source"];
            if (source.count) {
                int n = 0;
                for (NSDictionary *info in source)
                {
                    //NSDictionary *propDic = [info objectForKey:@"Property"];
                    //NSLog(@"[XMS] Name:%@ %d",[info objectForKey:@"DeviceName"],[[info objectForKey:@"Enabled"] boolValue]);
                    //if ([[propDic objectForKey:@"OnServer"] boolValue])
                    if ([[info objectForKey:@"Enabled"] boolValue])
                    {
                        CameraInfo *camInfo = [[[CameraInfo alloc] init] autorelease];
                        NSString *tmpStr;
                        if (info[@"DeviceType"] != nil) {
                            tmpStr = [[[info objectForKey:@"DeviceType"] objectForKey:@"Value"] copy];      //DeviceType: IPCAM DVR/NVR or RTSP
                        }
                        else
                            break;
                        
                        if ([tmpStr isEqualToString:@"IPCAM"])
                            camInfo.deviceType = XMS_IPCAM;
                        else if ([tmpStr isEqualToString:@"DVR"])
                            camInfo.deviceType = XMS_DVR;
                        else if ([tmpStr isEqualToString:@"NVR"])
                            camInfo.deviceType = XMS_NVR;
                        else if ([tmpStr isEqualToString:@"Channel"])
                            camInfo.deviceType = XMS_CHANNEL;
                        else if ([tmpStr isEqualToString:@"RTSP"])
                            camInfo.deviceType = XMS_RTSP;
                        
                        if (camInfo.deviceType == XMS_CHANNEL) {
                            camInfo.title = [[info objectForKey:@"DeviceName"] copy];
                            camInfo.path = [[info valueForKey:@"id"] copy];
                            if (info[@"Parent"] != nil) {
                                camInfo.parent = [[[info objectForKey:@"Parent"] valueForKey:@"DeviceName"] copy];
                            }
                        }
                        else
                            camInfo.title = [[info objectForKey:@"DeviceName"] copy];
                        
                        //if (camInfo.deviceType != XMS_RTSP) {
                        if (camInfo.deviceType == XMS_DVR || camInfo.deviceType == XMS_NVR) {
                            
                            if (info[@"BrandModel"] != nil) {
                                NSDictionary *tmpDict = [info objectForKey:@"BrandModel"];
                                NSString *cam_Model;
                                if (tmpDict[@"ModelName"] != nil) {
                                    cam_Model = [[tmpDict valueForKey:@"ModelName"] copy];
                                }
                                if (cam_Model) {
                                    camInfo.type = [self checkModelByName:cam_Model];
                                }
                            }
                            else
                                camInfo.type = [self checkModelByName:camInfo.title];
                        }
                        else if (camInfo.deviceType == XMS_CHANNEL) {
                            NSString *tmpModel = nil;
                            if (info[@"Parent"] != nil) {
                                NSDictionary *tmpDict1 = [info objectForKey:@"Parent"];
                                if (tmpDict1[@"BrandModel"] != nil) {
                                    NSDictionary *tmpDict2 = [tmpDict1 objectForKey:@"BrandModel"];
                                    if (![[tmpDict2 valueForKey:@"ModelName"] isEqual:[NSNull null]]) {
                                        tmpModel = [[tmpDict2 valueForKey:@"ModelName"] copy];
                                        camInfo.type = [self checkModelByName:tmpModel];
                                    }
                                }
                            }
                        }
                        else
                            camInfo.type = [self checkModelByName:camInfo.title];
                        
                        camInfo.ptzID = [[info objectForKey:@"EID"] integerValue];
                        camInfo.strDeviceID = [[info objectForKey:@"id"] copy];           //20150518 added by Ray Lin for XMS playback
                        camInfo.enable = [[info objectForKey:@"Enabled"] boolValue];
                        
                        //                    if ([info objectForKey:@"Server"] != [NSNull null]) {
                        if (info[@"Server"] != nil) {
                            camInfo.rtspPort = [[[info objectForKey:@"Server"] objectForKey:@"RTSPPort"] integerValue];
                        }
                        else
                            camInfo.rtspPort = [[info objectForKey:@"RTSPPort"] integerValue];
                        
                        camInfo.httpPort = [[info objectForKey:@"Port"] integerValue];
                        camInfo.ipAddr = [[[info objectForKey:@"IPv4"] objectForKey:@"Address"] copy];
                        
                        if (camInfo.deviceType == XMS_DVR || camInfo.deviceType == XMS_NVR) {
                            camInfo.relations = [[info objectForKey:@"Relation"] copy];
                            camInfo.totalChannel = [camInfo.relations count];
                            camInfo.portForward = [[[info objectForKey:@"Property"] objectForKey:@"PortForward"] integerValue];
                            if (camInfo.portForward == 1) {
                                NSDictionary *tmpDict1 = [info objectForKey:@"Property"];
                                NSDictionary *tmpDict2 = [tmpDict1 objectForKey:@"ForwardPort"];
                                if (![[tmpDict2 valueForKey:@"Http"] isEqual:[NSNull null]]) {
                                    camInfo.forwardPort = [[tmpDict2 valueForKey:@"Http"] integerValue];
                                }
                            }
                        }
                        else if (camInfo.deviceType == XMS_CHANNEL) {
                            if (info[@"Parent"] != nil) {
                                camInfo.relations = [[[info objectForKey:@"Parent"] valueForKey:@"Relation"] copy];
                                camInfo.totalChannel = [camInfo.relations count];
                                if ([[info objectForKey:@"Parent"] objectForKey:@"Property"] != [NSNull null]) {
                                    camInfo.portForward = [[[[info objectForKey:@"Parent"] objectForKey:@"Property"] valueForKey:@"PortForward"] integerValue];
                                    if (camInfo.portForward == 1) {
                                        if ([[[info objectForKey:@"Parent"] objectForKey:@"Property"] objectForKey:@"ForwardPort"] != [NSNull null])
                                            camInfo.forwardPort = [[[[[info objectForKey:@"Parent"] objectForKey:@"Property"] objectForKey:@"ForwardPort"] valueForKey:@"Http"] integerValue];
                                    }
                                }
                            }
                        }
                        
                        camInfo.dev_user = [[[info objectForKey:@"Property"] objectForKey:@"UserName"] copy];
                        camInfo.dev_password = [[[info objectForKey:@"Property"] objectForKey:@"Password"] copy];
                        
                        camInfo.install = 1;
                        camInfo.covert = 0;
                        //camInfo.index = ++n;
                        //validChannel |= 0x1<<(n-1);
                        validChannel = 4294967295;  //Support maximum 32 ch
                        
                        if (camInfo.deviceType == XMS_CHANNEL) {
                            camInfo.index = [self getCameraIndex:camInfo.path FromArray:camInfo.relations];
                            [channelList addObject:camInfo];
                            //DBGLog(@"[XMS] Ch_Name:%@ (%ld) -- %ld --- %@",camInfo.title,(long)camInfo.index,(long)camInfo.deviceType,camInfo.path);
                        }
                        else {
                            camInfo.index = ++n;
                            [cameraList addObject:camInfo];
                            //DBGLog(@"[XMS] Name:%@ (%ld) -- %ld -- %ld",camInfo.title,(long)camInfo.index,(long)camInfo.deviceType,(long)camInfo.ptzID);
                        }
                    }
                }
            }
        }
        @catch (NSException *exception) {
            RELog(@"[XMS] parse Channel data Exception :%@",exception.reason);
        }
    }
    if (blnGetRecTime) {
        @try {
            if ([[_json objectForKey:@"success"] boolValue]) {
                source = [_json valueForKey:@"source"];
                if (source.count) {
                    if ([source valueForKey:@"stime"] != NULL) {
                        m_intDiskStartTime = [[source valueForKey:@"stime"] integerValue];
                        m_intDiskEndTime = [[source valueForKey:@"etime"] integerValue];
                        DBGLog(@"[XMS] Record Start Time : %lu, Record End Time : %lu",(unsigned long)m_intDiskStartTime,(unsigned long)m_intDiskEndTime);
                    }
                }
            }
        }
        @catch (NSException *exception) {
            RELog(@"[XMS] Get Record Time Exception :%@",exception.reason);
        }
    }
    if (blnGetSearchInfo) {
        source = [_json valueForKey:@"source"];
        if (source.count) {
            sessionId = [[source valueForKey:@"intSession"] copy];
        }
    }
    if (blnGetVehInfo) {
        source = _json;

/*
        [XMS POST] Request : http://172.16.0.142:80/api/Pkg/Action?PkgName=xFleetMenu
        [XMS] response : {
            code = 507;
            message = "Cannot find action";
            success = 0;
        }
*/
        NSLog(@"[XMS SR] _Json is kind of class :%@",[_json class]);
        if ([source isKindOfClass:[NSDictionary class]]) {
            NSLog(@"[XMS] xFleetMenu request failed!!");
            return;
        }
        if (source.count) {
            NSString *tmpStr = nil;
            for (NSDictionary *info in source) {
                int n = 0;
                if ([[info objectForKey:@"Enabled"] boolValue])
                {
                    @try {
                        CameraInfo *vehicle = [[CameraInfo alloc] init];
                        vehicle.title = [[info valueForKey:@"DeviceName"] copy];
                        
                        tmpStr = [[info valueForKey:@"DeviceType"] copy];   //Vehicle
                        if ([tmpStr isEqualToString:@"Vehicle"]) {
                            vehicle.deviceType = XMS_VEHICLE;
                        }
                        vehicle.path = [[info valueForKey:@"_id"] copy];     //use for xFleetBus's id
                        
                        if (info[@"Relations"] != nil) {
                            NSMutableArray *array0 = [[info objectForKey:@"Relations"] valueForKey:@"Device"];
                            vehicle.child = [[array0 objectAtIndex:0] copy];   //use for child's(DVR) id
                        }
                        
                        [cameraList addObject:vehicle];
                        [vehicle release];
                        
                        CameraInfo *vehicle_dvr = [[CameraInfo alloc] init];
                        // vehicle_dvr.index = ++n;
                        
                        NSMutableArray *array1 = [info objectForKey:@"Relation"];
                        NSDictionary *dict1 = [array1 objectAtIndex:0];
                        
                        vehicle_dvr.title = [[dict1 valueForKey:@"DeviceName"] copy];
                        tmpStr = [[dict1 valueForKey:@"DeviceType"] copy];  //DVR
                        if ([tmpStr isEqualToString:@"DVR"]) {
                            vehicle_dvr.deviceType = XMS_VEHICLE_DVR;
                        }
                        vehicle_dvr.path = [[dict1 valueForKey:@"_id"] copy];   //DVR's id
                        [vehicleList addObject:vehicle_dvr];
                        [vehicle_dvr release];
                        
                        NSMutableArray *array2 = [dict1 objectForKey:@"Relation"];
                        
                        for (int i=0; i<array2.count; i++) {
                            
                            NSDictionary *dict2 = [array2 objectAtIndex:i];
                            CameraInfo *vehicle_dvr_ch = [[CameraInfo alloc] init];
                            vehicle_dvr_ch.index = ++n;
                            vehicle_dvr_ch.title = [[dict2 valueForKey:@"DeviceName"] copy];
                            tmpStr = [[dict2 valueForKey:@"DeviceType"] copy];   //Channel
                            if ([tmpStr isEqualToString:@"Channel"]) {
                                vehicle_dvr_ch.deviceType = XMS_VEHICLE_CH;
                            }
                            vehicle_dvr_ch.path = vehicle_dvr_ch.strDeviceID = [[dict2 valueForKey:@"_id"] copy];   //Channel's id
                            vehicle_dvr_ch.parent = [[dict2 valueForKey:@"Parent"] copy];  //use for parent's id
                            if (dict1[@"Relations"] != nil) {
                                vehicle_dvr_ch.channelIDs = [[dict1 objectForKey:@"Relations"] objectForKey:@"Device"];
                            }
                            vehicle_dvr_ch.rtspPort = [[dict1 valueForKey:@"RTSPPort"] integerValue];
                            
                            if (vehicle_dvr_ch.rtspPort == 0) {
                                vehicle_dvr_ch.rtspPort = 554;
                            }
                            
                            vehicle_dvr_ch.ptzID = [[dict2 valueForKey:@"EID"] integerValue];
                            [v_channelList addObject:vehicle_dvr_ch];
                            //DBGLog(@"[XMS] xFleet_Ch_Name:%@ (%ld) -- %ld -- %ld",vehicle_dvr_ch.title,(long)vehicle_dvr_ch.index,(long)vehicle_dvr_ch.deviceType,(long)vehicle_dvr_ch.ptzID);
                            [vehicle_dvr_ch release];
                        }
                    }
                    @catch (NSException *exception) {
                        RELog(@"[XMS] parse xFleet data Exception :%@",exception.reason);
                    }
                }
            }
        }
    }
    if (blnGetVehStatus) {
        @try {
            if ([[_json valueForKey:@"Result"] boolValue]) {
                source = [_json valueForKey:@"Item"];
                if (source.count) {
                    
                    if ([[[source objectForKey:curDevice.parent] valueForKey:@"IsConnected"] boolValue]) {
                        NSDictionary *dict1 = [[source objectForKey:curDevice.parent] valueForKey:@"CH"];
                        if ([[[dict1 objectForKey:curDevice.path] valueForKey:@"isLive"] boolValue]) {
                            blnIsVehConnected = YES;
                        }
                        else
                            blnIsVehConnected = NO;
                    }
                    else
                        blnIsVehConnected = NO;
                }
            }
            else
                blnIsVehConnected = NO;
        }
        @catch (NSException *exception) {
            RELog(@"[XMS] Get Vehicle Status Exception :%@",exception.reason);
            blnIsVehConnected = NO;
        }
    }
    if (blnReqVehStream) {
        if ([[_json valueForKey:@"result"] boolValue]) {
            
            blnGetVehStream = YES;
        }
        else
            blnGetVehStream = NO;
    }
    if (blnReqVehPBStream) {
        @try {
            if ([[_json valueForKey:@"result"] boolValue]) {
                
                sessionId = [[[_json objectForKey:@"data"] valueForKey:@"SessionID"] copy];
                RtspUrl = [[[[[[_json objectForKey:@"data"] objectForKey:@"Channel"] objectForKey:curDevice.path] objectForKey:@"data"] valueForKey:@"RtspURL"] copy];
                
                blnGetVehPBStream = YES;
            }
            else
                blnGetVehPBStream = NO;
        }
        @catch (NSException *exception) {
            RELog(@"[XMS] Request Vehicle PlayBack Stream Exception :%@",exception.reason);
            blnGetVehPBStream = NO;
        }
    }
}

#pragma mark - Parser Delegate

// Start of element
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    [currentXmlElement release];
    currentXmlElement = [elementName copy];
    
    if (blnGetDvrInfo && [currentXmlElement isEqualToString:@"RemoteChnAccess"])    //JamesLee++ 20140625 for P3 user covert
        blnGetP3Mask = YES;
}

// Found Character
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSMutableString *)string
{
    if ([string rangeOfString:@"\n"].location != NSNotFound)        // NewCodeBase issue
        return;
    
    if (blnGetDvrInfo) {
        if ( [currentXmlElement isEqualToString:@"user_level"] || [currentXmlElement isEqualToString:@"level"] ) {
            
            if (blnSupportNCB) {
                
                if ([string isEqualToString:@"Admin"])
                    userLevel = UA_ADMIN;
                else if ([string isEqualToString:@"Manager"])
                    userLevel = UA_MANAGER;
                else
                    userLevel = UA_OPERATOR;
            }else
                userLevel = [string integerValue];
        }
        else if ( [currentXmlElement isEqualToString:@"covert"] || [currentXmlElement isEqualToString:@"maskChannel"]) {
            
            covert = [string longLongValue];
        }
        else if ( [currentXmlElement isEqualToString:@"install"] || ([currentXmlElement isEqualToString:@"ctrlLiveChnmap"] && blnGetP3Mask) ) {
            
            install = [string longLongValue];
        }
        else if ( [currentXmlElement isEqualToString:@"ch_num"] || [currentXmlElement isEqualToString:@"NUM_CH_ANALOG_MAIN"]) {// add by robert hsu 20111031 for get maximum support channel
            
            iMaxSupportChannel = [string integerValue];
        }
        else if ([currentXmlElement isEqualToString:@"NUM_CH_IP_MAIN"]) {
            
            //if (DeviceType==NVR8004X || DeviceType==ENVR8304D_E_X)
            //    iMaxSupportChannel = [string integerValue];
            iMaxSupportChannel += [string integerValue];
        }
        else if ( [currentXmlElement isEqualToString:@"codec"] || [currentXmlElement isEqualToString:@"AV_FORMAT"]) {// add by robert hsu 20111031 for get maximum support channel
            
            if ([string integerValue]) {
                audioCodec = blnSupportNCB ? P3_AUDIO_CODEC([string integerValue]):P2_AUDIO_CODEC([string integerValue]);
                
                NSLog(@"[P2] AudioCodec:%ld",(long)audioCodec);
            }
        }
    }
}

// End Element
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
{
    [currentXmlElement release];
    currentXmlElement = [elementName copy];
    
    if (blnGetDvrInfo && [currentXmlElement isEqualToString:@"RemoteChnAccess"])    //JamesLee++ 20140625 for P3 user covert
        blnGetP3Mask = NO;
}

#pragma mark - SDP Parser

// Get SPS & PPS from SDP    20130319 James Lee
- (void)SdpParser:(NSString *)sdpStr
{
    if ([sdpStr rangeOfString:@","].location != NSNotFound) {
        NSArray *strArray = [sdpStr componentsSeparatedByString:@","];
        NSData *spsData = [Decryptor base64:[strArray objectAtIndex:0]];
        NSData *ppsData = [Decryptor base64:[strArray objectAtIndex:1]];
        //NSLog(@"[ONVIF] extra:%@, SPS:%@(%ld), PPS:%@(%ld)",sdpStr,[strArray objectAtIndex:0],(unsigned long)spsData.length,[strArray objectAtIndex:1],(unsigned long)ppsData.length);
        
        spsPkg.size = sizeof(startBuf) + spsData.length;
        ppsPkg.size = sizeof(startBuf) + ppsData.length;
        spsPkg.data = malloc(spsPkg.size);
        ppsPkg.data = malloc(ppsPkg.size);
        memcpy(spsPkg.data, startBuf, sizeof(startBuf));
        memcpy(spsPkg.data+sizeof(startBuf), [spsData bytes], spsData.length);
        memcpy(ppsPkg.data, startBuf, sizeof(startBuf));
        memcpy(ppsPkg.data+sizeof(startBuf), [ppsData bytes], ppsData.length);
    }
}

// 20151223 added by Ray Lin, for h265 vps, sps and pps parser
- (void)HEVCParser:(NSMutableDictionary *)hevcDict
{
    NSString *vpsStr = [hevcDict valueForKey:@"sprop-vps"];
    NSString *spsStr = [hevcDict valueForKey:@"sprop-sps"];
    NSString *ppsStr = [hevcDict valueForKey:@"sprop-pps"];
    NSData *vpsData = [Decryptor base64:vpsStr];
    NSData *spsData = [Decryptor base64:spsStr];
    NSData *ppsData = [Decryptor base64:ppsStr];
    //NSLog(@"[ONVIF] extra:%@, SPS:%@(%ld), PPS:%@(%ld)",sdpStr,[strArray objectAtIndex:0],(unsigned long)spsData.length,[strArray objectAtIndex:1],(unsigned long)ppsData.length);
    
    vpsPkg.size = sizeof(startBuf) + vpsData.length;
    spsPkg.size = sizeof(startBuf) + spsData.length;
    ppsPkg.size = sizeof(startBuf) + ppsData.length;
    vpsPkg.data = malloc(vpsPkg.size);
    spsPkg.data = malloc(spsPkg.size);
    ppsPkg.data = malloc(ppsPkg.size);
    memcpy(vpsPkg.data, startBuf, sizeof(startBuf));
    memcpy(vpsPkg.data+sizeof(startBuf), [vpsData bytes], vpsData.length);
    memcpy(spsPkg.data, startBuf, sizeof(startBuf));
    memcpy(spsPkg.data+sizeof(startBuf), [spsData bytes], spsData.length);
    memcpy(ppsPkg.data, startBuf, sizeof(startBuf));
    memcpy(ppsPkg.data+sizeof(startBuf), [ppsData bytes], ppsData.length);
}

#pragma mark - Connection Delegate
- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.
// it's a kind of callback function
//when call CommandSender.getData will set self to be delegate
//then in CommandSender.getData will set to become NSURLConnection's delegate
//after NSURLConnection receive data will trigger this method
{
    //NSLog(@"Received: %d", [data length]);
	if ( theConnection==requestConnection ) {
        
		[responseData appendData:data];
    }
    else if ( theConnection==streamConnection ) {
#ifdef BPS_DISPLAY
        nReceiveCount += data.length;   //J-BPS
#endif
        self.blnReceivingStream = YES;
        
        if ( blnStopStreaming ) return;
        
        NSInteger readLen = 0;
        NSInteger remanentLen = [data length];
        
        // check if NO stream
        if ( remanentLen<100) {
            
            if(frameOffset!=frameSize)
            {
                
            }
            else
            {
                BOOL isNullStream = YES;
                char* buf = (char*)[data bytes];
                
                for (int i=0; i<remanentLen; i++) {
                    if (buf[i]!=0) {
                        isNullStream=NO;
                    }
                }
                
                if (isNullStream) {
                    NSLog(@"[P2] No Streaming %ld packetRemanent=%d " ,(long)remanentLen ,packetRemanent);
                    
                    //                    if (dualStream!=0) {                                                  //disable by James 20130403
                    //                        self.errorDesc = NSLocalizedString(@"MsgSubDenied2", nil);
                    //                    }
                    //                    else {
                    //                        self.errorDesc = NSLocalizedString(@"MsgReceiveNull", nil);
                    //                    }
                    
                    //                    self.blnReceivingStream = NO;
                    //                    [self stopStreaming];
                    //                    return;
                }
            }
        }
        
        while ( remanentLen>0 ) {
            
            // check whether packet receive finished
            if ( packetHeaderOffset==sizeof(P2_PACKET_HEADER) && packetRemanent==0 ) {
                
                // reset packet header data length
                packetHeader.audio_len = 0;
                packetHeader.meta_len  = 0;
                packetHeader.video_len = 0;
                
                packetHeaderOffset = 0;
            }
            
            // read packet header
            if ( packetHeaderOffset<sizeof(P2_PACKET_HEADER) || packetRemanent==0 )
            {
                
                Byte *buf = (Byte *)(&packetHeader)+packetHeaderOffset;
                unsigned long remains = sizeof(P2_PACKET_HEADER)-packetHeaderOffset;
                unsigned long len = MIN(remains,remanentLen);
                
                [data getBytes:buf range:NSMakeRange(readLen, len)];
                
                readLen += len;
                remanentLen -= len;
                packetHeaderOffset += len;
                
                
                
                // packet header receive finished
                if ( packetHeaderOffset==sizeof(P2_PACKET_HEADER) ) {
                    
                    packetHeader.audio_len = ntohs(packetHeader.audio_len);
                    packetHeader.meta_len  = ntohs(packetHeader.meta_len);
                    packetHeader.video_len = ntohl(packetHeader.video_len);
                    
//                    NSLog(@"packet(%d/%lu) - flag:0x%02x audio:%d meta:%d video:%u",
//                          readLen,(unsigned long)[data length],packetHeader.flag[0],packetHeader.audio_len,
//                          packetHeader.meta_len,packetHeader.video_len);
                    
                    packetRemanent = packetHeader.audio_len + packetHeader.meta_len + packetHeader.video_len;
                    
                    // check packet header
                    if ( (packetHeader.flag[0]&(~(FRAME_START|FRAME_END)))!=0x0 ) {
                        
                        NSLog(@"[XMS] Packet header data error!");
                        
                        self.errorDesc = NSLocalizedString(@"MsgDataErr", nil);
                        self.blnReceivingStream = NO;
                        [self stopStreaming];
                        return;
                    }
                    
                    // receive a new frame
                    if ( (packetHeader.flag[0]&FRAME_START)!=0x0 ) {
                        
                        // reset parameters
                        memset(&pFrameBuf[0],0,MAX_FRAME_SIZE); // clean buffer
                        frameHeaderOffset = 0;
                        frameOffset = 0;
                        frameSize = 0;
                    }
                    
                    // reset audio/meta paras
                    amHeaderOffset = 0;
                    amSize = 0;
                    amOffset = 0;
                }
            }
            
            // get AUDIO frame
            if ( packetHeader.audio_len!=0 ) {
                
                if ( amHeaderOffset<sizeof(P2_AM_HEADER) && remanentLen>0 && packetRemanent>0 ) {
                    
                    Byte *buf = (Byte *)(&amHeader)+amHeaderOffset;
                    long remain = sizeof(P2_AM_HEADER)-amHeaderOffset;
                    long len = MIN(remain, MIN(remanentLen, packetRemanent));
                    
                    [data getBytes:buf range:NSMakeRange(readLen, len)];
                    
                    readLen += len;
                    remanentLen -= len;
                    amHeaderOffset += len;
                    packetRemanent -= len;
                    
                    // am header receive finished
                    if ( packetHeaderOffset==sizeof(P2_PACKET_HEADER) ) {
                        
                        amSize = packetHeader.audio_len-sizeof(P2_AM_HEADER);
                        amOffset = 0;
                        
                        pAMBuf = malloc(amSize);
                        
                        //NSLog(@"AM header received: ch=%d flag=0x%02x size=%d", amHeader.ch, amHeader.flag, amSize);
                    }
                }
                
                if ( amOffset<amSize && remanentLen>0 && packetRemanent>0 ) {
                    
                    Byte *buf = pAMBuf+amOffset;
                    long remain = amSize-amOffset;
                    long len = MIN(remain, MIN(remanentLen, packetRemanent));
                    
                    [data getBytes:buf range:NSMakeRange(readLen, len)];
                    
                    readLen += len;
                    remanentLen -= len;
                    amOffset += len;
                    packetRemanent -= len;
                    
                    // am data receive finished
                    if ( amOffset==amSize ) {
                        
                        //NSLog(@"AUDIO received finished");
                        
                        // process the audio data
                        // check channel
                        NSInteger playingCH = ((VideoDisplayView*) [videoControl.aryDisplayViews objectAtIndex:videoControl.selectWindow]).playingChannel;
                        // check amFlag to find codec
                        NSInteger auCodec;
                        if ( (((0x1<<(amHeader.ch)) & 0x1<<playingCH) != 0) && audioOn ) {
                            //NSLog(@"AudioType audio type: 0x%02x", amHeader.flag);
                            // create audio decoder
                            auCodec = blnSupportNCB ? P3_AUDIO_CODEC(amHeader.flag):P2_AUDIO_CODEC(amHeader.flag);
                            if (audioControl!=nil && (amHeader.flag != audioControl.amFlag)) {
                                
                                //[audioControl close];
                                //NSLog(@"[AC] flag:%d,codecID:%d",amHeader.flag&0xF0,audioControl.codecId);
                                switch (auCodec) {
                                        
                                    case P2_AUDIO_TYPE_ADPCM: // HiSi (header:4 audio:168-4=164 bsp:4or8?)
                                    case P3_AUDIO_TYPE_ADPCM:
                                        [audioControl initWithCodecId:AV_CODEC_ID_ADPCM_IMA_WAV srate:8000 bps:4 balign:164 fsize:321 channel:1];
                                        NSLog(@"[XMS] Create new audio decoder with codec id %d", AV_CODEC_ID_ADPCM_IMA_WAV);
                                        break;
                                        
                                    case P2_AUDIO_TYPE_G711: // Stretch (header:256 audio:2816-256=2560)
                                    case P3_AUDIO_TYPE_G711:
                                        [audioControl initWithCodecId:AV_CODEC_ID_PCM_MULAW srate:16000 bps:8 balign:2560 fsize:2560 channel:amHeader.flag==0x33?2:1];
                                        NSLog(@"[XMS] Create new audio decoder with codec id %d", AV_CODEC_ID_PCM_MULAW);
                                        break;
                                        
                                        
                                    case P3_AUDIO_TYPE_NVRG711:
                                        [audioControl initWithCodecId:AV_CODEC_ID_PCM_MULAW srate:8000 bps:8 balign:320 fsize:320 channel:1];
                                        NSLog(@"[XMS] Create new audio decoder with codec id(NVR) %d", AV_CODEC_ID_PCM_MULAW);
                                        break;
                                        
                                    case P2_AUDIO_TYPE_G723:
                                        audioControl = [audioControl initWithG723Codec];
                                        NSLog(@"[XMS] Create new audio decoder with codec id %d", AV_CODEC_ID_G723_1);
                                        break;
                                    default:
                                        NSLog(@"[XMS] Unupported audio type: 0x%02x", amHeader.flag);
                                        break;
                                }
                                audioControl.amFlag = amHeader.flag;
                                if (audioControl!=nil)
                                    [audioControl start];
                            }
                            
                            // decode and play the audio frame
                            if (audioControl!=nil) {
                                
                                int offset = 0;
                                long length = amSize;
                                
                                switch (auCodec) {
                                        
                                    case P2_AUDIO_TYPE_ADPCM:
                                    case P3_AUDIO_TYPE_ADPCM:
                                        while (length>=168) { // may have more than one frame at one time
                                            [audioControl playAudio:pAMBuf+offset+4 length:164];
                                            offset += 168;
                                            length -= 168;
                                        }
                                        break;
                                        
                                    case P2_AUDIO_TYPE_G711:
                                    case P3_AUDIO_TYPE_G711:
                                        [audioControl playAudio:pAMBuf+256 length:2560];
                                        break;
                                        
                                    case P3_AUDIO_TYPE_NVRG711:
                                        [audioControl playAudio:pAMBuf length:320];
                                        break;
                                        
                                    case P2_AUDIO_TYPE_G723:    //Paragon2(EPARA-16D3)    stretch
                                        [audioControl playAudio:pAMBuf length:length];
                                        break;
                                        
                                    default:
                                        break;
                                }
                            }
                        }
                        
                        free(pAMBuf);
                    }
                }
            }
            
            // get META frame (do nothing)
            if ( packetHeader.meta_len!=0 ) {
                
                if ( amHeaderOffset<sizeof(P2_AM_HEADER) && remanentLen>0 && packetRemanent>0 ) {
                    
                    Byte *buf = (Byte *)(&amHeader)+amHeaderOffset;
                    unsigned long remain = sizeof(P2_AM_HEADER)-amHeaderOffset;
                    unsigned long len = MIN(remain, MIN(remanentLen, packetRemanent));
                    
                    [data getBytes:buf range:NSMakeRange(readLen, len)];
                    
                    readLen += len;
                    remanentLen -= len;
                    amHeaderOffset += len;
                    packetRemanent -= len;
                    
                    // am header receive finished
                    if ( packetHeaderOffset==sizeof(P2_PACKET_HEADER) ) {
                        
                        amSize = packetHeader.meta_len-sizeof(P2_AM_HEADER);
                        amOffset = 0;
                        
                        pAMBuf = malloc(amSize);
                        
                        //NSLog(@"AM header received: ch=%d flag=0x%02x size=%d", amHeader.ch, amHeader.flag, amSize);
                    }
                }
                
                if ( amOffset<amSize && remanentLen>0 && packetRemanent>0 ) {
                    
                    Byte *buf = pAMBuf+amOffset;
                    unsigned long remain = amSize-amOffset;
                    unsigned long len = MIN(remain, MIN(remanentLen, packetRemanent));
                    
                    [data getBytes:buf range:NSMakeRange(readLen, len)];
                    
                    readLen += len;
                    remanentLen -= len;
                    amOffset += len;
                    packetRemanent -= len;
                    
                    // am data receive finished
                    if ( amOffset==amSize ) {
                        
                        NSLog(@"[P2] META received finished");
                        
                        // process meta data
                        
                        free(pAMBuf);
                    }
                }
            }
            
            // get VIDEO frame
            if ( packetHeader.video_len!=0 ) {
                
                
                // read frame header data
                if ( frameHeaderOffset<sizeof(P2_FRAME_HEADER) && remanentLen>0 && packetRemanent>0 )
                {
                    
                    
                    Byte *buf = &pFrameBuf[frameHeaderOffset];
                    long remains = sizeof(P2_FRAME_HEADER)-frameHeaderOffset;
                    long len = MIN(remains,MIN(remanentLen,packetRemanent));
                    
                    [data getBytes:buf range:NSMakeRange(readLen, len)];
                    
                    readLen += len;
                    remanentLen -= len;
                    frameHeaderOffset += len;
                    packetRemanent -= len;
                    
                    // header data finish
                    if ( frameHeaderOffset==sizeof(P2_FRAME_HEADER) ) {
                        
                        P2_FRAME_HEADER	*pHeader  = (P2_FRAME_HEADER *)&pFrameBuf[0];
                        
                        // convert header for different endian
                        pHeader->ms			= ntohs(pHeader->ms);
                        pHeader->sec		= ntohl(pHeader->sec);
                        pHeader->seq		= ntohl(pHeader->seq);
                        pHeader->time_zone	= ntohs(pHeader->time_zone);
                        pHeader->width		= ntohs(pHeader->width);
                        pHeader->height		= ntohs(pHeader->height);
                        pHeader->size		= ntohl(pHeader->size);
                        pHeader->event_flag = ntohl(pHeader->event_flag);
                        
                        frameSize	= pHeader->size;
                        frameWidth	= pHeader->width;
                        frameHeight = pHeader->height;
                        
                        // check frame header data
                        if ( pHeader->type > IMAGE_TYPE_PFRAME ) {
                            
                            NSLog(@"[XMS] Frame header data error!");
                            
                            self.blnReceivingStream = NO;
                            self.errorDesc = NSLocalizedString(@"MsgDataErr", nil);
                            [self stopStreaming];
                            return;
                        }
                        
                        //video available, callback to UI Controller
                        if (!blnVideoOK) {
                            [self.delegate EventCallback:@"VIDEO_OK"];
                            blnVideoOK = YES;
                            
//                            if (m_intDiskStartTime == 0 || m_intDiskEndTime == 0)
//                                [self.delegate EventCallback:@"PLAYBACK_NO"];
                        }
                    }
                }
                
                // read frame data
                if ( frameOffset<frameSize && remanentLen>0 && packetRemanent>0 )
                {
                    
                    // for take off Stretch header
                    // I-frame: (256-header)(24-SPS)(256-header)(9-PPS)(256-header)(FRAME)
                    // P-frame: (256-header)(FRAME)
                    
                    P2_FRAME_HEADER  *pHeader  = (P2_FRAME_HEADER *)&pFrameBuf[0];
                    
                    if ( (pHeader->codec_type&0xf0)==CODEC_H264_TYPE_B )
                    {
                        
                        // 1st Stretch header
                        if (frameOffset<sizeof(STCH_HEADER)) {
                            
                            Byte *buf = (Byte *)(&stchHeader)+stchHeaderOffset;
                            
                            long remains = sizeof(STCH_HEADER)-stchHeaderOffset;
                            long len = MIN(remains,MIN(remanentLen,packetRemanent));
                            
                            [data getBytes:buf range:NSMakeRange(readLen, len)];
                            //NSLog(@"read %d bytes", len);
                            
                            readLen += len;
                            remanentLen -= len;
                            stchHeaderOffset += len;
                            frameOffset += len;
                            packetRemanent -= len;
                            
                            // header data finish
                            if ( stchHeaderOffset==sizeof(STCH_HEADER) ) {
                                
                                // get payload(SPS/PPS) size
                                stchSpsSize	= ntohl(stchHeader.payload_size);
                                stchHeaderOffset = 0;
                            }
                        }
                        
                        if (pHeader->type==IMAGE_TYPE_IFRAME) {
                            // SPS
                            if (frameOffset<sizeof(STCH_HEADER)+stchSpsSize) {
                                
                                Byte *buf = &pFrameBuf[sizeof(P2_FRAME_HEADER)+frameOffset-sizeof(STCH_HEADER)];
                                long remains = sizeof(STCH_HEADER)+stchSpsSize-frameOffset;
                                long len = MIN(remains,MIN(remanentLen,packetRemanent));
                                
                                [data getBytes:buf range:NSMakeRange(readLen, len)];
                                //NSLog(@"read %d bytes", len);
                                
                                readLen += len;
                                remanentLen -= len;
                                frameOffset += len;
                                packetRemanent -= len;
                            }
                            // 2nd Stretch header
                            if (frameOffset<sizeof(STCH_HEADER)+stchSpsSize+sizeof(STCH_HEADER)) {
                                
                                Byte *buf = (Byte *)(&stchHeader)+stchHeaderOffset;
                                
                                long remains = sizeof(STCH_HEADER)-stchHeaderOffset;
                                long len = MIN(remains,MIN(remanentLen,packetRemanent));
                                
                                [data getBytes:buf range:NSMakeRange(readLen, len)];
                                //NSLog(@"read %d bytes", len);
                                
                                readLen += len;
                                remanentLen -= len;
                                stchHeaderOffset += len;
                                frameOffset += len;
                                packetRemanent -= len;
                                
                                // header data finish
                                if ( stchHeaderOffset==sizeof(STCH_HEADER) ) {
                                    
                                    // get payload(SPS/PPS) size
                                    stchPpsSize	= ntohl(stchHeader.payload_size);
                                    stchHeaderOffset = 0;
                                    
                                    //NSLog(@"SPS Size: %d PPS Size: %d", stchSpsSize, stchPpsSize);
                                }
                            }
                            // PPS
                            if (frameOffset<sizeof(STCH_HEADER)+stchSpsSize+sizeof(STCH_HEADER)+stchPpsSize) {
                                
                                Byte *buf = &pFrameBuf[sizeof(P2_FRAME_HEADER)+frameOffset-(sizeof(STCH_HEADER)*2)]; /////
                                long remains = sizeof(STCH_HEADER)+stchSpsSize+sizeof(STCH_HEADER)+stchPpsSize-frameOffset;
                                long len = MIN(remains,MIN(remanentLen,packetRemanent));
                                
                                [data getBytes:buf range:NSMakeRange(readLen, len)];
                                //NSLog(@"read %d bytes", len);
                                
                                readLen += len;
                                remanentLen -= len;
                                frameOffset += len;
                                packetRemanent -= len;
                            }
                            // 3rd Stretch header
                            if (frameOffset<sizeof(STCH_HEADER)+stchSpsSize+sizeof(STCH_HEADER)+stchPpsSize+sizeof(STCH_HEADER)) {
                                
                                long remains = sizeof(STCH_HEADER)+stchSpsSize+sizeof(STCH_HEADER)+stchPpsSize+sizeof(STCH_HEADER)-frameOffset;
                                long len = MIN(remains,MIN(remanentLen,packetRemanent));
                                
                                readLen += len;
                                remanentLen -= len;
                                frameOffset += len;
                                packetRemanent -= len;
                            }
                        }
                    }
                    // Stretch header END
                    
                    int stretchHeaderLen = (pHeader->codec_type&0xf0)!=CODEC_H264_TYPE_B?0:(pHeader->type==IMAGE_TYPE_IFRAME?(sizeof(STCH_HEADER)*3):sizeof(STCH_HEADER));
                    
                    Byte *buf = &pFrameBuf[sizeof(P2_FRAME_HEADER)+frameOffset-stretchHeaderLen];
                    long remains = frameSize-frameOffset;
                    long len = MIN(remains,MIN(remanentLen,packetRemanent));
                    
                    [data getBytes:buf range:NSMakeRange(readLen, len)];
                    
                    readLen += len;
                    remanentLen -= len;
                    frameOffset += len;
                    packetRemanent -= len;
                    
                    //frame data finish
                    if ( frameOffset==frameSize ) {
                        
                        P2_FRAME_HEADER	*pHeader	= (P2_FRAME_HEADER *)&pFrameBuf[0];
                        void			*data		= (void *)&pFrameBuf[sizeof(P2_FRAME_HEADER)];
                        
                        // decode and show the received VIDEO frame
                        // check video channel
                        if ( ((0x1<<(pHeader->ch)) & validChannel) != 0 ) {
                            
                            NSInteger iBufferIdx = [[outputList objectAtIndex:pHeader->ch] integerValue];
                            
                            if (iBufferIdx == 99) {
                                continue;
                            }
                            //NSLog(@"iBufferIdx:%ld,pHeader->ch:%d",(long)iBufferIdx,(int)pHeader->ch);
                            [videoControl setPlayingChannelByCH:iBufferIdx Playing:pHeader->ch];  //從外面設定，以免無法Close
                            
                            //channel status callback
                            CameraInfo *targetInfo = [self getInfoFromChannelIndex:pHeader->ch];
                            targetInfo.status = [self getFrameStatus:pHeader->event_flag];
                            //NSLog(@"[P2] CH:%d, Flag:%lu",pHeader->ch,pHeader->event_flag);
                            
                            VIDEO_PACKAGE videoPkg;
                            
                            videoPkg.sec	= blnSupportNCB? pHeader->sec+pHeader->day_light_saving*30*60+(pHeader->time_zone*15-720)*60:pHeader->sec;
                            videoPkg.ms		= pHeader->ms;
                            videoPkg.seq	= pHeader->seq;
                            int stretchHeaderLen = (pHeader->codec_type&0xf0)!=CODEC_H264_TYPE_B?0:(pHeader->type==IMAGE_TYPE_IFRAME?(sizeof(STCH_HEADER)*3):sizeof(STCH_HEADER));
                            videoPkg.size	= pHeader->size-pHeader->title_len-stretchHeaderLen;
                            videoPkg.width	= pHeader->width;
                            videoPkg.height	= pHeader->height;
                            videoPkg.data	= malloc(pHeader->size);
                            videoPkg.channel = 0x1<<pHeader->ch;
                            memcpy(videoPkg.data, data, pHeader->size);
                            
                            if ( pHeader->title_len!=0 ) {
                                
                                NSString *title;
                                
                                if (IS_UNICODE(DeviceType)||blnSupportNCB) {  // Modify by James Lee 20120522 for unicode camera title
                                    
                                    title = [[NSString alloc] initWithBytes:&pFrameBuf[sizeof(P2_FRAME_HEADER)+pHeader->size-pHeader->title_len-stretchHeaderLen] length:pHeader->title_len encoding:NSUTF16LittleEndianStringEncoding];
                                    if ([title rangeOfString:@"\0"].location != NSNotFound) {           // 20120402 add by James Lee, Fix the bug of title length error
                                        NSInteger strLen = [title rangeOfString:@"\0"].location;
                                        //NSLog(@"strLen:%d",strLen);
                                        [title release];
                                        title = [[NSString alloc] initWithBytes:&pFrameBuf[sizeof(P2_FRAME_HEADER)+pHeader->size-pHeader->title_len-stretchHeaderLen] length:2*strLen+2 encoding:NSUTF16LittleEndianStringEncoding];
                                    }
                                }
                                else {
                                    title = [[NSString alloc] initWithUTF8String:(char *)&pFrameBuf[sizeof(P2_FRAME_HEADER)+pHeader->size-pHeader->title_len-stretchHeaderLen]];
                                }
                                if ( ![title isEqualToString:[self.videoControl.aryTitle objectAtIndex:iBufferIdx]]) {
                                    
                                    [self.videoControl setTitleByCH:iBufferIdx title:title];//add by robert hsu 20120104
                                }
                                
                                [title release];
                            }
                            
                            switch (pHeader->codec_type&0xf0) {
                                case CODEC_H264_TYPE_A:
                                case CODEC_H264_TYPE_B:
                                    videoPkg.codec = AV_CODEC_ID_H264;
                                    self.blnIsMJPEG = NO;
                                    break;
                                case CODEC_MPEG4_TYPE_A:
                                    videoPkg.codec = AV_CODEC_ID_MPEG4;
                                    self.blnIsMJPEG = NO;
                                    break;
                                case CODEC_MJPEG_TYPE_A:
                                    videoPkg.codec = AV_CODEC_ID_MJPEG;
                                    self.blnIsMJPEG = YES;
                                    break;
                                default:
                                    videoPkg.codec = AV_CODEC_ID_NONE;
                                    self.blnIsMJPEG = NO;
                                    break;
                            }
                            
                            switch (pHeader->type) {
                                case IMAGE_TYPE_IFRAME:
                                    videoPkg.type = VO_TYPE_IFRAME;
                                    break;
                                case IMAGE_TYPE_PFRAME:
                                    videoPkg.type = VO_TYPE_PFRAME;
                                    break;
                                default:
                                    videoPkg.type = VO_TYPE_NONE;
                                    break;
                            }                                
                            //NSLog(@"[SR] ReceiveCH:%d, Type:%d, seq:%lu, time:%lu",iBufferIdx,pHeader->type,pHeader->seq,pHeader->sec);
                            [self.videoControl putFrameIntoBufferByCH:&videoPkg chIndex:iBufferIdx];
                        }
                        else {
                            
                            NSInteger iBufferIdx = [[outputList objectAtIndex:pHeader->ch] integerValue];
                            
                            if (iBufferIdx == 99) {
                                continue;
                            }
                            //NSLog(@"iBufferIdx:%ld,pHeader->ch:%d",(long)iBufferIdx,(int)pHeader->ch);
                            [videoControl setPlayingChannelByCH:iBufferIdx Playing:pHeader->ch];  //從外面設定，以免無法Close
                            
                            UIImage *image = [UIImage imageNamed: @"NavBarLogo.png"];
                            
                            VideoDisplayView *tmpView = [videoControl.aryDisplayViews objectAtIndex:iBufferIdx];
                            tmpView.blnIsPlay = NO;
                            [tmpView stopActivityIndicatorAnimating];
                            [tmpView setVideoImage:image];
                            
                            //[self.videoControl resetByCH:pHeader->ch];
                        }
                    }
                }
            }
        }
    }
}

#pragma mark - RTSP Delegate

- (void)didReceiveMessage:(NSString *)message
{
    NSLog(@"[XMS] %@",message);
}

- (void)didReceiveFrame:(const uint8_t *)frameData
        frameDataLength:(int)frameDataLength
       presentationTime:(struct timeval)presentationTime
 durationInMicroseconds:(unsigned)duration
             subsession:(RTSPSubsession *)subsession
{
    //NSLog(@"Receive:%@ - %d",[subsession getMediumName],frameDataLength);
    // Video frame
    if ([[subsession getMediumName] isEqual:@"video"]) {
        
        if (presentationTime.tv_sec != lastTimeTick)
            lastTimeTick = presentationTime.tv_sec;
        
        VIDEO_PACKAGE videoPkg = {0};
        videoPkg.sec	= presentationTime.tv_sec + timeZoneAdv;
        videoPkg.ms		= presentationTime.tv_usec/1000;
        videoPkg.channel = subsession.channel;
        
        NSInteger _width, _height;
        [subsession getSDP_VideoWidth:&_width Height:&_height];
        videoPkg.width = _width;
        videoPkg.height = _height;
        
        CameraInfo *camInfo;
        if (self.blnDvrChannel) {
            camInfo = [channelList objectAtIndex:subsession.channel];
        }
        else if (self.blnVehChannel) {
            camInfo = [v_channelList objectAtIndex:subsession.channel];
        }
        else
            camInfo = [cameraList objectAtIndex:subsession.channel];
        
        NSInteger iBufferIdx = [[outputList objectAtIndex:camInfo.index-1] intValue];
        if (iBufferIdx > MAX_SUPPORT_DISPLAY)
            return;
        
        if (!keyBuf[iBufferIdx])
            keyBuf[iBufferIdx] = [[NSMutableData alloc] init];
        
        if ([[subsession getCodecName] isEqual:@"H264"]) { // H264
            videoPkg.codec	= AV_CODEC_ID_H264;
            
            ENaluType NalType = 0x1F & frameData[0];
            switch (NalType) {
                case nalu_type_sps:
                case nalu_type_pps:
                    //case nalu_type_aud:
                    //case nalu_type_sei:
                    [keyBuf[iBufferIdx] appendBytes:scH264 length:sizeof(scH264)];
                    [keyBuf[iBufferIdx] appendBytes:frameData length:frameDataLength];
                    blnGetIDR = NO;
                    return;
                case nalu_type_idr:
                    [keyBuf[iBufferIdx] appendBytes:scH264 length:sizeof(scH264)];
                    [keyBuf[iBufferIdx] appendBytes:frameData length:frameDataLength];
                    videoPkg.size = keyBuf[iBufferIdx].length;
                    videoPkg.data = malloc(videoPkg.size);
                    videoPkg.type = VO_TYPE_IFRAME;
                    memcpy(videoPkg.data, keyBuf[iBufferIdx].bytes, keyBuf[iBufferIdx].length);
                    [keyBuf[iBufferIdx] setLength:0];
                    blnGetIDR = YES;
                    break;
                case nalu_type_slice:
                case nalu_type_dpa:
                case nalu_type_dpb:
                case nalu_type_dpc:
                    if (!blnGetIDR) {
                        NSLog(@"[XMS] IDR Missing - seq:%d Time:%lu.%03d",sequence[iBufferIdx],videoPkg.sec,videoPkg.ms);
                        return;
                    }
                    [keyBuf[iBufferIdx] appendBytes:scH264 length:sizeof(scH264)];
                    [keyBuf[iBufferIdx] appendBytes:frameData length:frameDataLength];
                    videoPkg.size = keyBuf[iBufferIdx].length;
                    videoPkg.data = malloc(videoPkg.size);
                    videoPkg.type = VO_TYPE_PFRAME;
                    memcpy(videoPkg.data, keyBuf[iBufferIdx].bytes, keyBuf[iBufferIdx].length);
                    [keyBuf[iBufferIdx] setLength:0];
                    break;
                default:
                    //NSLog(@"H264 Drop:%d - Time:%ld.%ld Size:%d",NalType,pkg.sec,pkg.ms,frameDataLength);
                    return;
            }
            
            videoPkg.seq = sequence[iBufferIdx]++;
        }
        else if ([[subsession getCodecName] isEqualToString:@"H265"])
        {
            videoPkg.codec = AV_CODEC_ID_HEVC;
            
//            if (((0x7E & frameData[0]) >> 1) == 19  || ((0x7E & frameData[0]) >> 1) == 20) { // I frame
//                
//                seq = 0;
//                videoPkg.seq	= seq;
//                videoPkg.type	= VO_TYPE_IFRAME;
//                videoPkg.size	= vpsPkg.size + spsPkg.size + ppsPkg.size + sizeof(startBuf)+frameDataLength;
//                videoPkg.data	= malloc(videoPkg.size);
//                
//                memcpy((videoPkg.data), vpsPkg.data, vpsPkg.size);
//                memcpy((videoPkg.data+vpsPkg.size), spsPkg.data, spsPkg.size);
//                memcpy((videoPkg.data+vpsPkg.size+spsPkg.size), ppsPkg.data, ppsPkg.size);
//                memcpy((videoPkg.data+vpsPkg.size+spsPkg.size+ppsPkg.size), startBuf, sizeof(startBuf));
//                memcpy((videoPkg.data+vpsPkg.size+spsPkg.size+ppsPkg.size+sizeof(startBuf)), frameData, frameDataLength);
//                
//                blnIsFirstIframe = YES;
//#ifdef H264DEBUG
//                NSLog(@"[265] Get I frame ---- frameData[0]:%x, size:%ld, startBuf:%lu, frameDataLength:%d",frameData[0],videoPkg.size,sizeof(startBuf),frameDataLength);
//#endif
//            }
//            else if (((0x7E & frameData[0]) >> 1) == 0 || ((0x7E & frameData[0]) >> 1) == 1) { // P frame
//                
//                if (!blnIsFirstIframe) {
//                    NSLog(@"[265] Waiting for I-Frame");
//                    return;
//                }
//                seq++;
//                videoPkg.seq	= seq;
//                videoPkg.type	= VO_TYPE_PFRAME;
//                videoPkg.size	= sizeof(startBuf)+frameDataLength;
//                videoPkg.data	= malloc(videoPkg.size);
//                memcpy(videoPkg.data, startBuf, sizeof(startBuf));
//                memcpy((videoPkg.data+sizeof(startBuf)), frameData, frameDataLength);
//#ifdef H264DEBUG
//                NSLog(@"[265] Get P frame ---- frameData[0]:%x, size:%ld seq:%d",frameData[0],videoPkg.size,seq);
//#endif
//            }
//            else {
//#ifdef H264DEBUG
//                NSLog(@"[265] Unknown %@ frame type", [subsession getCodecName]);
//#endif
//                return;
//            }
            
            NSUInteger NalType = ((0x7E & frameData[0]) >> 1);
            switch (NalType) {
                case 32:  //vps
                case 33:  //sps
                case 34:  //pps
                    [keyBuf[iBufferIdx] appendBytes:scH264 length:sizeof(scH264)];
                    [keyBuf[iBufferIdx] appendBytes:frameData length:frameDataLength];
                    blnGetIDR = NO;
                    return;
                case 19:  //I-frame
                    [keyBuf[iBufferIdx] appendBytes:scH264 length:sizeof(scH264)];
                    [keyBuf[iBufferIdx] appendBytes:frameData length:frameDataLength];
                    videoPkg.size = keyBuf[iBufferIdx].length;
                    videoPkg.data = malloc(videoPkg.size);
                    videoPkg.type = VO_TYPE_IFRAME;
                    memcpy(videoPkg.data, keyBuf[iBufferIdx].bytes, keyBuf[iBufferIdx].length);
                    [keyBuf[iBufferIdx] setLength:0];
                    blnGetIDR = YES;
                    break;
                case 0:  //maybe P-frame or B-frame
                case 1:  //P-frame
                    if (!blnGetIDR) {
                        NSLog(@"[XMS] IDR Missing - seq:%d Time:%lu.%03d",sequence[iBufferIdx],videoPkg.sec,videoPkg.ms);
                        return;
                    }
                    [keyBuf[iBufferIdx] appendBytes:scH264 length:sizeof(scH264)];
                    [keyBuf[iBufferIdx] appendBytes:frameData length:frameDataLength];
                    videoPkg.size = keyBuf[iBufferIdx].length;
                    videoPkg.data = malloc(videoPkg.size);
                    videoPkg.type = VO_TYPE_PFRAME;
                    memcpy(videoPkg.data, keyBuf[iBufferIdx].bytes, keyBuf[iBufferIdx].length);
                    [keyBuf[iBufferIdx] setLength:0];
                    break;
                default:
                    //NSLog(@"H264 Drop:%d - Time:%ld.%ld Size:%d",NalType,pkg.sec,pkg.ms,frameDataLength);
                    return;
            }
            
            videoPkg.seq = sequence[iBufferIdx]++;
        }
        
        if (![camInfo.title isEqualToString:[self.videoControl.aryTitle objectAtIndex:iBufferIdx]])
            [self.videoControl setTitleByCH:iBufferIdx title:camInfo.title];
        
        // put package to VideoOutput buffer
        [videoControl putFrameIntoBufferByCH:&videoPkg chIndex:iBufferIdx];
        if (!blnVideoOK) {
            [self.delegate EventCallback:@"VIDEO_OK"];
            blnVideoOK = YES;
        }
    }else if ([[subsession getMediumName] isEqual:@"audio"]) {
        
        if (audioOn && subsession.channel == currentCHMask) {
            
            int cID = AV_CODEC_ID_NONE;
            int sampleRate = [subsession getFrequency];
            int channels = [subsession getChannels];
            NSString *auCodec = [subsession getCodecName];
            if ([auCodec isEqualToString:@"PCMU"]) {
                cID = AV_CODEC_ID_PCM_MULAW;
            }
            
            if (audioControl.codecId != cID) {
                audioControl = [audioControl initWithCodecId:cID srate:sampleRate
                                                         bps:8 balign:frameDataLength
                                                       fsize:frameDataLength channel:channels];
                [audioControl start];
            }
                
            if (audioControl!=nil) {
                [audioControl playAudio:(Byte *)frameData length:frameDataLength];
            }
        }
    }
    else {
        NSLog(@"[XMS] Unknown medium name: %@", [subsession getMediumName]);
    }
}

@end
