//
//  XMSStreamReceiver.h
//  EFViewerPlus
//
//  Created by James Lee on 2014/7/28.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import "StreamReceiver.h"
#import "CameraInfo.h"
#import "P2StreamReceiver.h"
#import "P2AudioSender.h"
#import "P2Net.h"

@interface XMSStreamReceiver : StreamReceiver
{
    NSInteger           index;
    NSUInteger          covert;
    NSUInteger          ptzMask;
    NSUInteger          install;
    BOOL                blnGetP3Mask;
    BOOL                blnGetDvrInfo;
    BOOL                blnGetXmsInfo;
    BOOL                blnGetDiskInfo;
    NSMutableDictionary *streamArray;
    NSMutableDictionary *sidArray;
    
    uint8_t             scH264[4];
    NSMutableData       *keyBuf[MAX_SUPPORT_DISPLAY];
    int                 sequence[MAX_SUPPORT_DISPLAY];
    BOOL                blnGetIDR;
    BOOL                blnVideoOK;
    NSInteger           timeZoneAdv;
    NSInteger           lastTimeTick;
    
    int seq;
    uint8_t startBuf[4];
    VIDEO_PACKAGE vpsPkg;//20151223 added by Ray Lin, for h265 vps, sps and pps parser
    VIDEO_PACKAGE spsPkg;
    VIDEO_PACKAGE ppsPkg;
    BOOL blnIsFirstIframe;
    
    BOOL blnSupportNCB;
    BOOL blnVideoAvailable;
    int frameOffset;
    int frameSize;
    int packetRemanent;
    int packetHeaderOffset;
    int frameHeaderOffset;
    int amHeaderOffset;
    int amSize;
    int amOffset;
    int stchHeaderOffset;
    int stchSpsSize;
    int stchPpsSize;
    P2_PACKET_HEADER	packetHeader;
    P2_AM_HEADER		amHeader;
    STCH_HEADER			stchHeader;
    Byte pFrameBuf[MAX_FRAME_SIZE];
    Byte* pAMBuf;
}

@end
