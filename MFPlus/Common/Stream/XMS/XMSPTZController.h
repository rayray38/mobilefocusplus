//
//  XMSPTZController.h
//  EFViewerPlus
//
//  Created by Nobel on 2015/1/19.
//  Copyright (c) 2015年 EverFocus. All rights reserved.
//

#import "PtzController.h"

@interface XMSPTZController : PtzController
- (NSString *)XMSCommand:(NSString *)_command with:(NSString *)_value;
- (NSString *)convertJSON2String:(NSDictionary *)_json;
@end
