//
//  CameraInfo.h
//  EFViewerHD
//
//  Created by James Lee on 12/10/11.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UA_ADMIN    3
#define UA_MANAGER  2
#define UA_OPERATOR 1

@interface CameraInfo : NSObject

@property(nonatomic)         NSInteger  index;
@property(nonatomic, retain) NSString   *title;
@property(nonatomic)         NSInteger  fps;
@property(nonatomic)         NSInteger  install;
@property(nonatomic)         NSInteger  covert;
@property(nonatomic)         NSInteger  baudrate;
@property(nonatomic)         NSInteger  ptzID;             //use it as EID in XMS
@property(nonatomic, retain) NSString   *strDeviceID;      //use it for XMS playback
@property(nonatomic, retain) NSString   *ptzType;
@property(nonatomic)         NSInteger  rtspPort;
@property(nonatomic)         NSInteger  width;
@property(nonatomic)         NSInteger  height;
@property(nonatomic, retain) NSString   *codecType;
@property(nonatomic)         BOOL       enable;
@property(nonatomic)         NSInteger  status;
@property(nonatomic)         NSInteger  type;              //20150805 added bt Ray Lin for device model
@property(nonatomic)         NSInteger  deviceType;
//@property(nonatomic, retain) NSString   *deviceType;       //20150605 added by Ray Lin to check device type is DVR or IPCAM in XMS
@property(nonatomic, retain) NSString   *parent;           //20150611 added by Ray Lin
@property(nonatomic, retain) NSString   *channelTitle;     //20150708 added by Ray Lin
@property(nonatomic)         NSInteger  httpPort;          //20150715 added by Ray Lin
@property(nonatomic, retain) NSString   *ipAddr;           //20150715 added by Ray Lin
@property(nonatomic)         NSInteger  portForward;
@property(nonatomic)         NSInteger  forwardPort;
@property(nonatomic, strong) NSMutableArray   *relations;
@property(nonatomic)         NSInteger  totalChannel;
@property(nonatomic, retain) NSString   *dev_user;         //20150824 added by Ray Lin
@property(nonatomic, retain) NSString   *dev_password;     //20150824 added by Ray Lin


//------------------------------------ 20151120 added by Ray Lin, for xms support vehicle tree view test ------------------------------------//
@property (nonatomic, retain) NSString       *child;       //use for Vehicle DVR's id
@property (nonatomic, retain) NSString       *path;        //用來綁定車機以及車機channel的id
@property (nonatomic, retain) NSMutableArray *channelIDs;  //20160301 added by Ray Lin
@property (nonatomic)         NSInteger      numberOfSubitems;
@property (nonatomic, retain) CameraInfo     *parentSelectingItem;
@property (nonatomic, retain) NSMutableArray *ancestorSelectingItems;
@property (nonatomic)         NSInteger      submersionLevel;
@property (nonatomic)         BOOL           selected;

- (BOOL)isEqualToSelectingItem:(CameraInfo *)selectingItem;
//------------------------------------ 20151120 added by Ray Lin, for xms support vehicle tree view test ------------------------------------//


@end

@interface SearchResultEntry : NSObject
{
@public
    unsigned int uiStartTime;
    unsigned int uiEndTime;
    unsigned int uiEventType;
    unsigned int uiChannel;
}
@property (nonatomic)   unsigned int uiStartTime;
@property (nonatomic)   unsigned int uiEndTime;
@property (nonatomic)   unsigned int uiEventType;
@property (nonatomic)   unsigned int uiChannel;

@end
