//
//  StreamReceiver.h
//  EFViewer
//
//  Created by James Lee on 2010/8/26.
//  Copyright 2010 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Device.h"
#import "CameraInfo.h"
#import "AudioOutput.h"
#import "VideoControl.h"
#import "PtzController.h"
#import "AudioSender.h"
#import "CommandSender.h"
#import "VideoDisplayView.h"

#define MAX_FRAME_SIZE	768000

#define JSON_SUCCESS(X) [((NSNumber *)[X objectForKey:@"success"]) boolValue]

enum EFrameStatus
{
    efs_alarm = 0x01,
    efs_event = 0x02,
    efs_motion = 0x04,
    efs_vloss = 0x08
};

@protocol StreamDelegate

@required
- (void)EventCallback:(NSString *)msg;
- (void)xmsEventStatusCallback:(NSDictionary *)data;

@end

@interface StreamReceiver : NSObject <NSXMLParserDelegate, NSURLConnectionDelegate>
{	
	NSString			*host; // with port
	NSString			*user;
	NSString			*passwd;
    NSString			*user_copy;
    NSString			*passwd_copy;
	NSString			*sessionId;
    NSString			*errorDesc;
    NSString            *tokenKey;
    NSString            *tokenInfo; //20150807 added by Ray Lin
    NSString            *xmsUUID;   //20150807 added by Ray Lin
    NSString            *version;
    NSString            *xms_version; //20151013 added by Ray Lin
    
	BOOL				blnFirstConnection;
	BOOL				blnReceivingStream;
    BOOL				blnStopStreaming;
    BOOL				blnStartPlayBack;
    BOOL                blnResetStreaming;
    BOOL                blnConnectionClear;
    BOOL                blnIsAudioEnable; // get audio status. add by James Lee 20120727
    BOOL                blnGetChannelInfo;
    BOOL                blnGetRecTime;  //20150617 added by Ray Lin to get XMS record time
	BOOL                audioOn;
    BOOL                speakOn;
    BOOL                blnGetDNS;
    BOOL                blnGetTokenInfo; //20150807 added by Ray Lin
    BOOL                blnGetUUID;      //20150807 added by Ray Lin
    BOOL                blnSendDevToken; //20150807 added by Ray Lin
    BOOL                blnGetVersion;   //20151013 added by Ray Lin
    BOOL                blnEventQry;     //20160727 added by Ray Lin
	
    VideoControl        *videoControl;
	PtzController		*ptzController;
	AudioOutput         *audioControl;
    AudioSender         *audioSender;
	CommandSender		*cmdSender;
	
    NSInteger			httpPort;    //20160113 added by Ray Lin
	NSInteger			dualStream;
	NSInteger			userLevel;      // DVR:3(Administrator)/2(Manager)/1(Operator)/0(Disable)  IPCam:SUPERVISOR/ADMIN/USER/GUEST (4/3/2/1)
    NSUInteger          validChannel;   // modify by robert hsu 20111031 for 32 channel need NSUInteger
    NSUInteger          validPlaybackCh;
    NSUInteger          validPtzChannel;
	NSInteger			frameWidth;
	NSInteger			frameHeight;
    NSUInteger          currentCHMask;  // camera mask add by robert hsu 20120106  for keep camera mask
    NSUInteger          currentPlayCH;  // nowplaying channel with select window
    NSInteger           iRtspPort;      // add by robert hsu 20120203
    NSMutableArray      *cameraList;    // add by James Lee 20121011 for camera list
    NSMutableArray      *vehicleList;   // add by Ray Lin 20151201 for vehicle list
    NSMutableArray      *channelList;   // add by Ray Lin 20150709 for dvr/nvr channel list
    NSMutableArray      *currentCHList;   // add by Ray Lin 20150709 for dvr/nvr channel list
    NSMutableArray      *v_channelList;   // add by Ray Lin 20150709 for vehicle channel list
    NSMutableArray      *outputList;    // add by James Lee 20121015 for matching output render and channel index
	
	NSString			*currentXmlElement;
	NSMutableData		*responseData;
	NSURLConnection		*dnsConnection;
	NSURLConnection		*streamConnection;
	NSURLConnection		*requestConnection;
    
    NSInteger           DeviceType;     //add by robert hsu 20110926 for keep device type (set channel for ip video server)
    NSInteger           streamType;
    NSInteger           deviceId;
    NSInteger           audioCodec;
    BOOL                blnChSupportPTZ; //add by robert hsu 20110927 for check current channel support PTZ or not
    NSInteger           iMaxSupportChannel; //add by robert hsu 20111031 for keep device maximum support channel
    NSInteger           m_SpeedIndex;               //add by robert hsu 20120208 for get current speed from speed array
    
    NSUInteger          m_intDiskStartTime;         //add by robert hsu 20120210 for keep dvr disk recording time start
    NSUInteger          m_intDiskEndTime;           //add by robert hsu 20120210 for keep dvr disk recording time end
    NSUInteger          m_intPlaybackStartTime;     //add by robert hsu 20120210 for keep dvr start playback time
    NSString            *m_DiskGMT;                 //add by James Lee 20120215 for keep dvr disk recording time zone
    NSUInteger          m_DiskGMT_sec;              //20150529 added by Ray Lin
    NSUInteger          m_dst_time_sec;             //20150529 added by Ray Lin
    NSUInteger          m_CurrentPlaybackTime;      //add by robert hsu 20120208 ,current playback video time
    NSInteger           m_intSearchResultTag;       //add by James Lee 20120927 ,search cgi return
    NSString            *m_SearchID;                //add by James Lee 20120927 ,search id
    NSInteger           m_intSearchProgress;        //add by James Lee 20120927 ,search progress
    NSMutableArray      *SearchResultlist;          //add by James Lee 20120927 ,for event search
    NSUInteger          m_intdlsEnable;             //20150528 added by Ray for daylight saving
    NSUInteger          m_intStartMonth;            //20150528 added by Ray for daylight saving
    NSUInteger          m_intCurrentTime;           //20150528 added by Ray for daylight saving
    NSUInteger          m_intStartWeek;             //20150528 added by Ray for daylight saving
    NSUInteger          m_intStartWeekday;          //20150528 added by Ray for daylight saving
    NSUInteger          m_intStartHour;             //20150528 added by Ray for daylight saving
    NSUInteger          m_intStartMin;              //20150528 added by Ray for daylight saving
    NSUInteger          m_intSetHour;               //20150528 added by Ray for daylight saving
    NSUInteger          m_intSetMin;                //20150528 added by Ray for daylight saving
    NSUInteger          m_intEndMonth;              //20150528 added by Ray for daylight saving
    NSUInteger          m_intEndWeek;               //20150528 added by Ray for daylight saving
    NSUInteger          m_intEndWeekday;            //20150528 added by Ray for daylight saving
    NSUInteger          m_intEndHour;               //20150528 added by Ray for daylight saving
    NSUInteger          m_intEndMin;                //20150528 added by Ray for daylight saving
    BOOL                blnGetSearchInfo;

    NSTimer             *liveTimer; //add by James Lee 20140402 for keep alive
    BOOL                m_bGetFrame;
}

- (id)initWithDevice:(Device *)device;

- (BOOL)startLiveStreaming:(NSUInteger)mask;	// If failed return NO, else return YES when streaming stop. Channel number from 0.
- (BOOL)startPlaybackStreaming:(NSUInteger)mask;
- (void)changeLiveChannel:(NSInteger)channel;
- (void)stopStreaming;
- (void)stopStreamingByView:(NSInteger)vIdx;

- (BOOL)getDeviceInfo;                          // get Device info
- (void)openSound;
- (void)closeSound;
- (void)openSpeak;
- (void)closeSpeak;
- (BOOL)getSupportPTZ :(NSInteger)channel ;    //add by robert hsu 20110927 for Video Server to get support PTZ status
- (NSString *)getIp;
- (NSInteger)getPort;
- (void)getHostByName;
- (void)changeViewMode:(BOOL)tag;
- (void)changeVideoMask:(NSUInteger)mask;
- (void)changeVideoResolution:(NSUInteger)mask video:(NSInteger)type;
- (void)changeAudioChannel;
- (void)getSearchIDFrom:(NSInteger)start To:(NSInteger)end byFilter:(NSInteger)mask;
- (void)getEventResult;//add by James Lee 20120927 for event search
- (void)cancelEventSearch;
- (void)changePlaybackMode:(NSInteger)mode withValue:(NSInteger)value;
- (void)keepAlive;
- (NSString *)getStringFromLine:(NSString *)data targetString:(NSString *)target withQuote:(NSString *)quote;
- (CameraInfo *)getInfoFromChannelIndex:(NSInteger)ChIdx;

- (void)registDevTokenToServer:(NSArray *)devArray;   //20150804 added by Ray Lin
- (void)updateDevTokenToServer:(NSArray *)devArray;   //20160616 added by Ray Lin

- (void)showAlarm:(NSString *)message;
- (void)getVehicleStatus:(CameraInfo *)vehicle;   //20151202 ++ by Ray Lin, for query vehicle status

- (void)getXmsEventFromLocal:(NSInteger)eventSeq byType:(NSString *)vtype;   //20160726 ++ by Ray Lin, for querry xms event

@property(nonatomic,retain)		NSString			*host;
@property(nonatomic,retain)		NSString			*user;
@property(nonatomic,retain)		NSString			*passwd;
@property(nonatomic,retain)		NSString			*errorDesc;
@property(nonatomic)			NSInteger			httpPort;    //20160113 added by Ray Lin
@property(nonatomic)			BOOL				blnReceivingStream;
@property(nonatomic)			BOOL				blnResetStreaming;
@property(nonatomic)            BOOL                blnConnectionClear;
@property(nonatomic)            BOOL                blnChSupportPTZ; //add by robert hsu 20110927 for check current channel support PTZ or not
@property(nonatomic)            BOOL                blnGetChannelInfo;
@property(nonatomic,assign)     NSMutableArray      *videoDisplayViews; // add by robert hsu 20120103 for multi-channel
@property(nonatomic,assign)     VideoControl        *videoControl;
@property(nonatomic,assign)     AudioOutput         *audioControl;
@property(nonatomic,readonly)	PtzController		*ptzController;
@property(nonatomic,retain)     AudioSender			*audioSender;
@property(nonatomic, assign)    EventTable          *event;            //20150902 added by Ray Lin
@property(nonatomic)            BOOL                blnEventMode;      //20150902 added by Ray Lin
@property(nonatomic)			NSInteger			dualStream;
@property(nonatomic)            NSInteger           streamType;
@property(nonatomic)            NSInteger           deviceId;
@property(nonatomic)            NSInteger           audioCodec;
@property(nonatomic)			NSInteger			userLevel;
@property(nonatomic)			NSUInteger			validChannel;       // add by robert hsu 20111031 for 32 channel
@property(nonatomic)			NSUInteger			validPlaybackCh;    // 20151013 ++ by Ray Lin for P3 management
@property(nonatomic)			NSUInteger			validPtzChannel;           // 20151013 ++ by Ray Lin for P3 management
@property(nonatomic)			NSInteger			frameWidth;
@property(nonatomic)			NSInteger			frameHeight;
@property(nonatomic)            NSInteger           iMaxSupportChannel; // add by robert hsu 20111031 for keep device maximum support channel
@property(nonatomic)            NSUInteger          currentCHMask;      // add by robert hsu 20120106 for keep camera mask(use mask to replace current camera channel)
@property(nonatomic)            NSUInteger          currentPlayCH;   // nowplaying channel with select window
@property(nonatomic)            NSInteger           iRtspPort;          // add by robert hsu 20120203
@property(nonatomic,retain)     NSMutableArray      *cameraList;        // add by James Lee 20121011 for camera list
@property(nonatomic,retain)     NSMutableArray      *channelList;        // add by Ray Lin 20150709 for channel list
@property(nonatomic,retain)     NSMutableArray      *outputList;        // add by James Lee 20121015 for matching output render and channel index
@property(nonatomic)            NSInteger           m_SpeedIndex;               //add by robert hsu 20120208 for get current speed from speed array
@property(nonatomic)            BOOL                m_blnPlaybackMode;        //add by robert hsu 20120208
@property(nonatomic)            NSUInteger          m_intDiskStartTime;         //add by robert hsu 20120210 for keep dvr disk recording time start
@property(nonatomic)            NSUInteger          m_intDiskEndTime;           //add by robert hsu 20120210 for keep dvr disk recording time end
@property(nonatomic)            NSUInteger          m_intPlaybackStartTime;     //add by robert hsu 20120210 for keep dvr start playback time
@property(nonatomic,retain)     NSString            *m_DiskGMT;                 //add by James Lee 20120215 for keep dvr disk recording time zone
@property(nonatomic)            NSUInteger          m_CurrentPlaybackTime;      //add by robert hsu 20120208 ,current playback video time
@property(nonatomic)            BOOL                blnIsAudioEnable;           //add by James Lee 20120727 ,get audio status
@property(nonatomic)            BOOL                audioOn;
@property(nonatomic)            BOOL                speakOn;
@property(nonatomic)            NSInteger           m_intSearchResultTag;       //add by James Lee 20120927 ,search cgi return
@property(nonatomic,retain)     NSString            *m_SearchID;                //add by James Lee 20120927 ,search id
@property(nonatomic)            NSInteger           m_intSearchProgress;        //add by James Lee 20120927 ,search progress
@property(nonatomic,retain)     NSMutableArray      *SearchResultlist;          //add by James Lee 20120927 ,for event search
@property(nonatomic)            BOOL                blnGetSearchInfo;
@property(nonatomic)            BOOL                blnIsMJPEG;
@property(nonatomic)            NSUInteger          m_intdlsEnable;             //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_intStartMonth;            //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_intCurrentTime;           //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_intStartWeek;             //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_intStartWeekday;          //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_intStartHour;             //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_intStartMin;              //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_intSetHour;               //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_intSetMin;                //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_intEndMonth;              //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_intEndWeek;               //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_intEndWeekday;            //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_intEndHour;               //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_intEndMin;                //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_DiskGMT_sec;              //20150528 added by Ray for daylight saving
@property(nonatomic)            NSUInteger          m_dst_time_sec;             //20150528 added by Ray for daylight saving
@property(nonatomic,assign)     NSString            *m_stream_uri;
@property(nonatomic)            BOOL                blnCloseNotify;
@property(nonatomic)            BOOL                m_blnEventSearchRS;         //20151020 ++ by Ray Lin for event search result
@property(nonatomic)            NSUInteger          eventSearchCH;              //20151020 ++ by Ray Lin for event search result
@property(nonatomic,assign)     NSString            *xmsUUID;
@property(nonatomic,assign)     NSString            *sessionId;

@property(nonatomic,retain)     NSMutableArray      *vehicleList;   // add by Ray Lin 20151201 for vehicle list
@property(nonatomic,retain)     NSMutableArray      *v_channelList; // add by Ray Lin 20151201 for vehicle channel list
@property(nonatomic,retain)     NSMutableArray      *currentCHList;
@property(nonatomic)            BOOL                blnVehChannel;
@property(nonatomic)            BOOL                blnDvrChannel;

@property(nonatomic,assign)     id<StreamDelegate>  delegate;

@end
