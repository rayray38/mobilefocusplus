//
//  RTSPClientSession.m
//  EFViewer
//
//  Created by James Lee on 2011/4/25.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import "RTSPClientSession.h"

#include "liveMedia.hh"
#include "BasicUsageEnvironment.hh"
#include "GroupsockHelper.hh"
#include "BitVector.hh"
#include "Encrypt.h"

//===========================================================================================================
// Implementation of RTSPSubsessionMediaSink and MediaSubSession functions
//
//
//===========================================================================================================

class RTSPSubsessionMediaSink : public MediaSink {
public:
	RTSPSubsessionMediaSink(UsageEnvironment& _env, RTSPSubsession *_subsession) 
	: MediaSink(_env) {
		
		subsession = _subsession;
		bufLen = MAX_BUF_SIZE;
		buf = new uint8_t[bufLen];
	}
	
	virtual ~RTSPSubsessionMediaSink() {
		delete[] buf;
	}
	
	void afterGettingFrame(unsigned frameSize, 
						   unsigned numTruncatedBytes, 
						   struct timeval presentationTime,
						   unsigned durationInMicroseconds) {
		if (numTruncatedBytes > 0)
			NSLog(@"Frame was truncated.");
        
		[subsession.delegate didReceiveFrame:buf
							 frameDataLength:frameSize
							presentationTime:presentationTime 
					  durationInMicroseconds:durationInMicroseconds
								  subsession:subsession];
		
		continuePlaying();		
	}
	
	static void afterGettingFrame(void* clientData, unsigned frameSize, 
								  unsigned numTruncatedBytes, 
								  struct timeval presentationTime,
								  unsigned durationInMicroseconds) {
		// Create an autorelease pool around each invocation of afterGettingFrame because we're being dispached
		// calls from the Live555 event loop, not a normal Cocoa event loop.
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		
		RTSPSubsessionMediaSink *sink = (RTSPSubsessionMediaSink*)clientData;
		sink->afterGettingFrame(frameSize, numTruncatedBytes, presentationTime, durationInMicroseconds);
		
		[pool release];
	}
	
	virtual Boolean continuePlaying() {
		if (fSource) {
			fSource->getNextFrame(buf, bufLen, afterGettingFrame, this, onSourceClosure, this);
			
			return True;
		}
		
		return False;
	}
	
private:
	RTSPSubsession *subsession;
	uint8_t *buf;
	int bufLen;
};

struct RTSPSubsessionContext {
    MediaSubsession *subsession;
    UsageEnvironment *env;
    int channel;
};

static void afterSubsessionPlaying(void* clientData) {
	NSLog(@"[RTSP] afterSubsessionPlaying");
}

static void afterSubsessionReceiveBye(void* clientData) {
    RTSPSubsession *subsession = (RTSPSubsession *)clientData;
    //NSLog(@"[RTSP] %@ BYE",[subsession getMediumName]);
    [subsession.delegate didReceiveMessage:@"BYE"];
}

static void afterGetParameter(RTSPClient* rtspClient, int resultCode, char* resultString) {
    //NSLog(@"[RTSP] afterGetParameter [%d] %s",resultCode,resultString);
}

static void afterSendPlay(RTSPClient* rtspClient, int resultCode, char* resultString) {
    //NSLog(@"[RTSP] afterSendPlay [%d] %s",resultCode,resultString);
}

//===========================================================================================================
// Implementation of RTSPSubsession in Obj-C
//
//
//===========================================================================================================

@implementation RTSPSubsession

@synthesize delegate;

- (id)initWithMediaSubsession:(MediaSubsession*)subsession environment:(UsageEnvironment*)env {
	if ((self = [super init])) {
		context = new RTSPSubsessionContext;
		context->subsession = subsession;
		context->env = env;
	}
	
	return self;
}

- (void)dealloc {
	delete context;
	[super dealloc];
}

- (NSString*)getSessionId {
	return [NSString stringWithCString:context->subsession->sessionId() encoding:NSUTF8StringEncoding];
}

- (NSString*)getMediumName {
	return [NSString stringWithCString:context->subsession->mediumName() encoding:NSUTF8StringEncoding];
}

- (NSString*)getProtocolName {
	return [NSString stringWithCString:context->subsession->protocolName() encoding:NSUTF8StringEncoding];
}

- (NSString*)getCodecName {
	return [NSString stringWithCString:context->subsession->codecName() encoding:NSUTF8StringEncoding];
}

- (NSUInteger)getServerPortNum {
	return context->subsession->serverPortNum;
}

- (NSUInteger)getClientPortNum {
	return context->subsession->clientPortNum();
}

- (NSUInteger)getFrequency {
    return context->subsession->rtpTimestampFrequency();
}

- (NSUInteger)getChannels {
    return context->subsession->numChannels();
}

- (int)getSocket {
	return context->subsession->rtpSource()->RTPgs()->socketNum();
}

- (NSString*)getSDP_spropparametersets {
	return [NSString stringWithCString:context->subsession->fmtp_spropparametersets() encoding:NSUTF8StringEncoding];
}

//20151223 added by Ray Lin, for h265 vps, sps and pps parser
- (NSMutableDictionary*)getVPSandSPSandPPS {
    NSString *vps = [NSString stringWithCString:context->subsession->fmtp_spropvps() encoding:NSUTF8StringEncoding];
    NSString *sps = [NSString stringWithCString:context->subsession->fmtp_spropsps() encoding:NSUTF8StringEncoding];
    NSString *pps = [NSString stringWithCString:context->subsession->fmtp_sproppps() encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:vps, @"sprop-vps",
                                                                                  sps, @"sprop-sps",
                                                                                  pps, @"sprop-pps", nil];
    
    return dict;
}

- (NSString*)getSDP_config {
	return [NSString stringWithCString:context->subsession->fmtp_config() encoding:NSUTF8StringEncoding];
}

- (NSString*)getSDP_mode {
	//return [NSString stringWithCString:context->subsession->fmtp_mode() encoding:NSUTF8StringEncoding];
    return nil;
}

- (NSData *)getSpsData
{
    NSString *strParameter = [NSString stringWithUTF8String:context->subsession->fmtp_spropparametersets()];
    NSString *strSPS = [[strParameter componentsSeparatedByString:@","] firstObject];
    
    return [Decryptor base64:strSPS];
}

- (NSData *)getPpsData
{
    NSString *strParameter = [NSString stringWithUTF8String:context->subsession->fmtp_spropparametersets()];
    NSString *strPPS = [[strParameter componentsSeparatedByString:@","] objectAtIndex:1];
    
    return [Decryptor base64:strPPS];
}

- (double)getNormalPlayTime:(struct timeval)presentationTime {
    return context->subsession->getNormalPlayTime(presentationTime);
}

- (void)getSDP_VideoWidth:(NSInteger *)_width Height:(NSInteger *)_height {
    
    *_width = context->subsession->videoWidth();
    *_height = context->subsession->videoHeight();
    if (*_width == 0 || *_height == 0) {
        
        NSString *strParameter = [NSString stringWithUTF8String:context->subsession->fmtp_spropparametersets()];
        NSString *strSPS = [[strParameter componentsSeparatedByString:@","] firstObject];
        NSData *dataSPS = [Decryptor base64:strSPS];
        
        BitVector bv((unsigned char*)dataSPS.bytes, 0, (unsigned)(8*dataSPS.length));
        bv.skipBits(8); // forbidden_zero_bit; nal_ref_idc; nal_unit_type
        unsigned profile_idc = bv.getBits(8);
        (void)bv.getBits(8); // also "reserved_zero_2bits" at end
        (void)bv.getBits(8);
        (void)bv.get_expGolomb();
        if (profile_idc == 100 || profile_idc == 110 || profile_idc == 122 || profile_idc == 244 || profile_idc == 44 || profile_idc == 83 || profile_idc == 86 || profile_idc == 118 || profile_idc == 128 ) {
            unsigned chroma_format_idc = bv.get_expGolomb();
            if (chroma_format_idc == 3) {
                (void)bv.get1Bit();
            }
            (void)bv.get_expGolomb(); // bit_depth_luma_minus8
            (void)bv.get_expGolomb(); // bit_depth_chroma_minus8
            bv.skipBits(1); // qpprime_y_zero_transform_bypass_flag
            unsigned seq_scaling_matrix_present_flag = bv.get1Bit();
            if (seq_scaling_matrix_present_flag) {
                for (int i = 0; i < ((chroma_format_idc != 3) ? 8 : 12); ++i)
                {
                    unsigned seq_scaling_list_present_flag = bv.get1Bit();
                    if (seq_scaling_list_present_flag) {
                        unsigned sizeOfScalingList = i < 6 ? 16 : 64;
                        unsigned lastScale = 8;
                        unsigned nextScale = 8;
                        for (unsigned j = 0; j < sizeOfScalingList; ++j)
                        {
                            if (nextScale != 0) {
                                unsigned delta_scale = bv.get_expGolomb();
                                nextScale = (lastScale + delta_scale + 256) % 256;
                            }
                            lastScale = (nextScale == 0) ? lastScale : nextScale;
                        }
                    }
                }
            }
        }
        (void)bv.get_expGolomb();
        unsigned pic_order_cnt_type = bv.get_expGolomb();
        if (pic_order_cnt_type == 0) {
            (void)bv.get_expGolomb();
        } else if (pic_order_cnt_type == 1) {
            bv.skipBits(1); // delta_pic_order_always_zero_flag
            (void)bv.get_expGolomb(); // offset_for_non_ref_pic
            (void)bv.get_expGolomb(); // offset_for_top_to_bottom_field
            unsigned num_ref_frames_in_pic_order_cnt_cycle = bv.get_expGolomb();
            for (unsigned i = 0; i < num_ref_frames_in_pic_order_cnt_cycle; ++i)
                (void)bv.get_expGolomb(); // offset_for_ref_frame[i]
        }
        (void)bv.get_expGolomb();
        (void)bv.get1Bit();
        unsigned pic_width_in_mbs_minus1 = bv.get_expGolomb();
        unsigned pic_height_in_map_units_minus1 = bv.get_expGolomb();
        unsigned frame_mbs_only_flag = bv.get1Bit();
        
        *_width = (pic_width_in_mbs_minus1 +1)*16;
        *_height = (2 - frame_mbs_only_flag) * (pic_height_in_map_units_minus1 +1) * 16;
        
        [dataSPS release];
    }
}

- (NSUInteger)getSDP_VideoHeight {
	return context->subsession->videoHeight();
}

- (void)increaseReceiveBufferTo:(NSUInteger)size {
	int recvSocket = context->subsession->rtpSource()->RTPgs()->socketNum();
	increaseReceiveBufferTo(*context->env, recvSocket, (unsigned)size);
}

- (void)setPacketReorderingThresholdTime:(NSUInteger)uSeconds {
	context->subsession->rtpSource()->setPacketReorderingThresholdTime((unsigned)uSeconds);
}

- (BOOL)timeIsSynchronized {
	return context->subsession->rtpSource()->hasBeenSynchronizedUsingRTCP();
}

- (void)setDelegate:(id <RTSPSubsessionDelegate>)_delegate {
	delegate = _delegate;
}

- (MediaSubsession*)getMediaSubsession {
	return context->subsession;
}

@end

//===========================================================================================================
// Implementation of vxRtspClient and MediaSession functions
//
//
//===========================================================================================================

class vxRtspClient : public RTSPClient {
public:
    static vxRtspClient* createNew(UsageEnvironment& env, char const* rtspURL,
                                   int verbosityLevel = 0,
                                   char const* applicationName = NULL,
                                   portNumBits tunnelOverHTTPPortNum = 0);
protected:
    vxRtspClient(UsageEnvironment& env, char const* rtspURL,
                  int verbosityLevel, char const* applicationName, portNumBits tunnelOverHTTPPortNum);
    virtual ~vxRtspClient();
    
public:
    RTSPClientSession *vxSession;
};

//Implementation of vxRtspClient

vxRtspClient *vxRtspClient::createNew(UsageEnvironment& env, char const* rtspURL,
                                      int verbosityLevel,
                                      char const* applicationName,
                                      portNumBits tunnelOverHTTPPortNum) {
    return new vxRtspClient(env, rtspURL, verbosityLevel, applicationName, tunnelOverHTTPPortNum);
}

vxRtspClient::vxRtspClient(UsageEnvironment& env, char const* rtspURL,
                           int verbosityLevel, char const* applicationName, portNumBits tunnelOverHTTPPortNum)
: RTSPClient(env,rtspURL, verbosityLevel, applicationName, tunnelOverHTTPPortNum, -1)
, vxSession(nil) {
}

vxRtspClient::~vxRtspClient() {
}

static void afterSendDescribe(RTSPClient* rtspClient, int resultCode, char* resultString);
void setupNextSubssesion(RTSPClient *rtspClient);
static void afterSendSetup(RTSPClient* rtspClient, int resultCode, char* resultString);

struct RTSPClientSessionContext {
    TaskScheduler *scheduler;
    UsageEnvironment *env;
    vxRtspClient *client;
    MediaSession *session;
    MediaSubsessionIterator *iter;
    char cancelLoop;
    int errCode;
};

static void afterSendDescribe(RTSPClient* rtspClient, int resultCode, char* resultString) {

    vxRtspClient *clnt = (vxRtspClient *)rtspClient;
    RTSPClientSessionContext *ctx = [clnt->vxSession context];

    if (resultCode != 0) {
        NSLog(@"Failed to get SDP");
        ctx->errCode = resultCode;
        ctx->cancelLoop = 1;
        return;
    }
    
    char *sdpDescription = NULL;
    if (strlen(resultString)) {
        sdpDescription = resultString;
        NSLog(@"SDP:%s",sdpDescription);
        [clnt->vxSession setSdp:[[NSString alloc] initWithUTF8String:sdpDescription]];
    }
    
    ctx->session = MediaSession::createNew(*ctx->env, sdpDescription);
    delete [] sdpDescription;
    
    if (ctx->session == NULL) {
        NSLog(@"Failed to create MediaSession:%s",ctx->env->getResultMsg());
        ctx->errCode = ctx->env->getErrno();
        ctx->cancelLoop = 1;
    } else if (!ctx->session->hasSubsessions()) {
        NSLog(@"MediaSession without subsessions");
        ctx->cancelLoop = 1;
    }
    
    ctx->iter = new MediaSubsessionIterator(*ctx->session);
    setupNextSubssesion(rtspClient);
}

void setupNextSubssesion(RTSPClient *rtspClient) {
    
    vxRtspClient *clnt = (vxRtspClient *)rtspClient;
    RTSPClientSessionContext *ctx = [clnt->vxSession context];

    MediaSubsession *subsession = ctx->iter->next();
    
    if (subsession != NULL) { //還有沒setup的subsession
        
        if (!subsession->initiate()) {
            NSLog(@"Failed to init subsession %s, error:%s",subsession->mediumName(),ctx->env->getResultMsg());
            ctx->errCode = ctx->env->getErrno();
            ctx->cancelLoop = 1;
        } else {
            RTSPSubsession *newObj = [[[RTSPSubsession alloc] initWithMediaSubsession:subsession environment:ctx->env] autorelease];
            [[clnt->vxSession subsessions] addObject:newObj];
            clnt->sendSetupCommand(*subsession, afterSendSetup, NO, YES/* streamUsingUDP */);
        }
        return;
    }
    
    //所有subsession都setup完畢, 可以play了
    ctx->cancelLoop = 1;
}

static void afterSendSetup(RTSPClient* rtspClient, int resultCode, char* resultString) {
    
    vxRtspClient *clnt = (vxRtspClient *)rtspClient;
    RTSPClientSessionContext *ctx = [clnt->vxSession context];
    RTSPSubsession *subsession = [[clnt->vxSession subsessions] lastObject];
    
    if (resultCode != 0) {
        NSLog(@"Failed to setup subsession %@, error:%s",[subsession getMediumName],ctx->env->getResultMsg());
        ctx->errCode = resultCode;
        setupNextSubssesion(rtspClient);
    }
    
    MediaSubsession *cppSubsession = [subsession getMediaSubsession];
    RTSPSubsessionMediaSink *sink = new RTSPSubsessionMediaSink(*ctx->env, subsession);
    if (sink == NULL) {
        NSLog(@"Failed to create data sink for %@, error:%s",[subsession getMediumName],ctx->env->getResultMsg());
        ctx->errCode = ctx->env->getErrno();
        setupNextSubssesion(rtspClient);
    }
    
    if([[subsession getMediumName] isEqualToString:@"video"])
        [subsession increaseReceiveBufferTo:MAX_BUF_SIZE];
    
    cppSubsession->sink = sink;
    cppSubsession->miscPtr = rtspClient;
    cppSubsession->sink->startPlaying(*cppSubsession->readSource(), afterSubsessionPlaying, cppSubsession);
    
    if (cppSubsession->rtcpInstance() != NULL)
        cppSubsession->rtcpInstance()->setByeHandler(afterSubsessionReceiveBye, subsession);
    
    setupNextSubssesion(rtspClient);
}

//===========================================================================================================
// Implementation of RTSPClientSession in Obj-C
//
//
//===========================================================================================================

@implementation RTSPClientSession

@synthesize context,sdp,subsessions;

- (id)initWithURL:(NSString*)_url
             user:(NSString*)_user
           passwd:(NSString*)_passwd
{
	if ([super init]) {
		
		url = [_url retain];
		username = [_user retain];
		password = [_passwd retain];
		
		context = new RTSPClientSessionContext;
		memset(context, 0, sizeof(*context));
		
		context->scheduler = BasicTaskScheduler::createNew();
		context->env = BasicUsageEnvironment::createNew(*context->scheduler);
        context->client = vxRtspClient::createNew(*context->env, url.UTF8String);
        context->client->vxSession = self;
        
        subsessions = [[NSMutableArray alloc] init];
	}

	return self;
}

- (void)dealloc
{
    SAVE_FREE(url);
    SAVE_FREE(username);
    SAVE_FREE(password);
    SAVE_FREE(sdp);
    SAVE_FREE(subsessions);
    
    [super dealloc];
}

- (BOOL)setup {
	
	if (username && password) {
        Authenticator *auth = new Authenticator(username.UTF8String, password.UTF8String);
        context->client->sendDescribeCommand(afterSendDescribe, auth);
	}
	else {
        context->client->sendDescribeCommand(afterSendDescribe);
	}
	
    if (context->cancelLoop == 0)
    {
        context->cancelLoop = 0;
        [context->client->vxSession runEventLoop:&context->cancelLoop];
    }
    
	return (context->errCode == 0);
}

- (NSArray*)getSubsessions {
	return subsessions;
}

- (BOOL)play {
	
	// Start playing each subsession, to start the data flow
	//context->client->playMediaSession(*context->session);
    context->client->sendPlayCommand(*context->session, nil);
	
	return YES;
}

- (BOOL)pause {
    
    // Pause playing each subsession
    //context->client->pauseMediaSession(*context->session);
    context->client->sendPauseCommand(*context->session, nil);
    return YES;
}

- (BOOL)resume {
    
    // Resume playing each subsession from last playtime
    //context->client->playMediaSession(*context->session, -1.0f, -1.0f, 1.0f);
    context->client->sendPlayCommand(*context->session, nil,-1.0f);
    return YES;
}

- (BOOL)seekTime:(int)playtime {
    
    // Seek play time
    //context->client->playMediaSession(*context->session, playtime, -1.0f, 1.0f);
    context->client->sendPlayCommand(*context->session, afterSendPlay, playtime, -1.0f, 1.0f);
    return YES;
}

- (BOOL)keepAlive
{
    //send get_parameter to check alive
    context->client->sendGetParameterCommand(*context->session, afterGetParameter, nil);
    return YES;
}

- (BOOL)stop
{
    context->cancelLoop = 1;
    NSLog(@"[RTSP] Kill the loop");
    return YES;
}

- (BOOL)teardown {
    
    if (!context->session)
        return NO;
    
    if (context->client) {
        MediaSubsessionIterator iter(*context->session);
        while (MediaSubsession *subsession = iter.next()) {
            
            if (subsession->sink) {
                Medium::close(subsession->sink);
                //context->client->teardownMediaSubsession(*subsession);
                context->client->sendTeardownCommand(*subsession, nil);
            }
        }
    }
    
    UsageEnvironment* env = NULL;
    TaskScheduler* scheduler = NULL;
    
    if (context->session) {
        env = &(context->session->envir());
        scheduler = &(env->taskScheduler());
    }
    
    Medium::close(context->session);
    Medium::close(context->client);
    
    env->reclaim();
    delete scheduler;
    delete context;
    
	return YES;
}

- (BOOL)runEventLoop:(char*)cancelSession {
	// Main event loop - will block until cancelSession becomes true
	context->scheduler->doEventLoop(cancelSession);
	return YES;
}

- (void)runEventLoop
{
    context->cancelLoop = 0;
    [self runEventLoop:&context->cancelLoop];
    NSLog(@"[RTSP] Leave the loop");
}

- (NSString*)getLastErrorString {
	return [NSString stringWithCString:context->env->getResultMsg() encoding:NSUTF8StringEncoding];
}

- (NSString*)getSDP {
    
	return sdp;
}

- (int)getSocket {
	return context->client->socketNum();
}

- (int)getPlayStartTime {
    return context->session->playStartTime();
}

- (int)getPlayEndTime {
    return context->session->playEndTime();
}

@end
