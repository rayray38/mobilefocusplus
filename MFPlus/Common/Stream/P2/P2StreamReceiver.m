//
//  P2StreamReceiver.m
//  EFViewerHD
//
//  Created by James Lee on 12/10/9.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "P2StreamReceiver.h"
#import "P2PtzController.h"
#import "P2AudioSender.h"
#include <CommonCrypto/CommonDigest.h>

@interface P2StreamReceiver()
- (void)getDVRStatus;
@end

@implementation P2StreamReceiver

- (id)initWithDevice:(Device *)device
{
    SearchResultlist = [[NSMutableArray alloc] init]; 
    
    return [super initWithDevice:device];
}

#pragma mark - Streaming Control

- (BOOL)startLiveStreaming:(NSUInteger)mask {
	
	BOOL ret = YES;
	NSString *cmd;
	NSString *response;
    NSInteger iErrorCode = 0;
    blnStopStreaming = NO;
    blnVideoAvailable = NO;
    
	if ( ![super startLiveStreaming:mask] )
	{
		ret =  NO;
		goto Exit;
	}
    
	// reset parameters
	packetRemanent = 0;
	packetHeaderOffset = 0;
	stchHeaderOffset = 0;
	frameHeaderOffset = 0;
	frameOffset = 0;
	frameSize = 0;
	audioOn = NO;
	
	// request session id
    if (blnSupportNCB) {
        cmd = [NSString stringWithFormat:@"Live.cgi?cmd=register_stream"];
    }else {
        NSString *auth = [Encryptor sha1:[NSString stringWithFormat:@"%@:%@",self.user,self.passwd]];
        cmd = [NSString stringWithFormat:@"Live.cgi?cmd=register_stream&auth=%@",auth];
    }
    
	[cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
	
	// connection failed
	if (errorDesc!=nil ) {
		
        iErrorCode = -2;
		ret =  NO;
		goto Exit;
	}
	
	if (blnStopStreaming ) {
		
		goto Exit;
	}
	
	// check session id
	response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"[P2] CID response:%@",response);
	if ( ([response rangeOfString:@"-1"].location!=NSNotFound) || ([response rangeOfString:@"-2"].location!=NSNotFound)  )
    {// if find " -1" in response string means that get session failed (some product with "-2")
		
		self.errorDesc = NSLocalizedString(@"MsgCidFail", nil);
		iErrorCode = -3;
        [response release];
		ret = NO;
		goto Exit;
	}
	
	// set sid
    if (sessionId) {
        [sessionId release];
    }
	sessionId = [response retain];
    NSLog(@"[P2] sessionID:%@",sessionId);
	[response release];
	
	// initiate PTZ controller
    if (ptzController != nil) {
        [ptzController release];
        ptzController = nil;
    }
	ptzController = [[P2PtzController alloc] initWithHost:host sid:sessionId toKen:nil];
    ptzController.blnNcb = blnSupportNCB;
    if (DeviceType == NVR8004X || DeviceType == ENVR8304D_E_X || DeviceType == ENVR8316E)
        ptzController.ptzType = @"NVR";
    
    SAVE_FREE(audioSender);
    audioSender = [[P2AudioSender alloc] initWithHost:host user:user passwd:passwd sid:sessionId];
    audioSender.blnSupportNCB = blnSupportNCB;
#ifdef BPS_DISPLAY
    connectionTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(getBps) userInfo:nil repeats:YES]; //J-BPS
#endif
    
	// start live streaming
	cmd = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%lu&meta=1&cmd=stream_ctrl&action=start&video_type=%ld",sessionId,(unsigned long)currentCHMask,(long)dualStream];
	[cmdSender getData:cmd connection:&streamConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
	
	if ( errorDesc!=nil ) {
		iErrorCode = -4;
		ret = NO;
		goto Exit;
	}
	
Exit:
//    if (iErrorCode < -3 || blnStopStreaming) {
//        cmd = [NSString stringWithFormat:@"Live.cgi?cmd=stream_ctrl&stream_id=%@&action=cancel",sessionId];
//        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
//    }
	self.blnReceivingStream = NO;
    [self stopStreaming];
    ret = NO;
	
	return ret;
}

- (BOOL)startPlaybackStreaming:(NSUInteger)mask
{
	
	BOOL ret = YES;
	NSString *cmd;
	NSString *response;
    NSInteger iErrorCode = 0;
    blnStartPlayBack = YES;
    blnStopStreaming = NO;
    blnVideoAvailable = NO;
    
	if ( ![super startPlaybackStreaming:mask] )
	{
		ret =  NO;
		goto Exit;
	}
    
	// reset parameters
	packetRemanent = 0;
	packetHeaderOffset = 0;
	stchHeaderOffset = 0;
	frameHeaderOffset = 0;
	frameOffset = 0;
	frameSize = 0;
	audioOn = NO;
    m_SpeedIndex = 1;
	
	// request session id
    if (blnSupportNCB) {
        cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=register_stream"];
    }else {
        NSString *auth = [Encryptor sha1:[NSString stringWithFormat:@"%@:%@",self.user,self.passwd]];
        cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=register_stream&auth=%@",auth];
    }
    
	[cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
	
	// connection failed
	if (errorDesc!=nil ) {
		
        iErrorCode = -2;
		ret =  NO;
		goto Exit;
	}
    
	if (blnStopStreaming ) {
		
		goto Exit;
	}
	
	// check session id
	response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"[P2] CID response:%@",response);
	if ( ([response rangeOfString:@"-1"].location!=NSNotFound) || ([response rangeOfString:@"-2"].location!=NSNotFound)  )
    {// if find " -1" in response string means that get session failed (some product with "-2")
		
		self.errorDesc = NSLocalizedString(@"MsgCidFail", nil);
		iErrorCode = -3;
        [response release];
		ret = NO;
		goto Exit;
	}
	
	// set sid
    if (sessionId) {
        [sessionId release];
    }
	sessionId = [response retain];
    NSLog(@"[P2] sessionID:%@",sessionId);
	[response release];

	// start playback streaming
	cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=stream_ctrl&stream_id=%@&action=start&camera=%lu&start_time=%lu&video_type=%ld",sessionId,(unsigned long)currentCHMask,(unsigned long)m_intPlaybackStartTime,(long)dualStream];
	[cmdSender getData:cmd connection:&streamConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
	
	if ( errorDesc!=nil ) {
		iErrorCode = -4;
		ret = NO;
	}
	
Exit:
    if (iErrorCode < -3 || blnStopStreaming) {
        cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=stream_ctrl&stream_id=%@&action=cancel",sessionId];
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
    }
    blnStartPlayBack = NO;
	self.blnReceivingStream = NO;
    [self stopStreaming];
	
	return ret;
}

- (void)changeVideoMask:(NSUInteger)mask
{
    NSString* cmd;
	
	// change video mask
    if(sessionId)
    {
        NSInteger errCount = 0;
        while (!blnReceivingStream && errCount<100)
        {
            [NSThread sleepForTimeInterval:0.05f];
            errCount++;
        }
        //NSLog(@"[SR] sessionID = %@" ,sessionId);
        currentCHMask = mask;
        if (!self.m_blnPlaybackMode || mask==0){
            cmd = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%lu&cmd=stream_ctrl",sessionId,(unsigned long)currentCHMask];
        }else {
            cmd = [NSString stringWithFormat:@"Playback.cgi?stream_id=%@&camera=%lu&cmd=stream_ctrl",sessionId,(unsigned long)currentCHMask];
        }
        [cmdSender getData:cmd connection:&requestConnection synchronously:NO random:NO supportNCB:blnSupportNCB];

        if (audioOn)
            [self changeAudioChannel];
    }
}

- (void)changeAudioChannel
{
    NSString* cmd;
    NSInteger playCh = 0;
    if (!blnReceivingStream)    return;
    
    if(sessionId)
    {
        //change audio channel
        if ( audioOn ) {
            if (currentPlayCH & currentCHMask) {
                playCh = currentPlayCH;
            }
            
            if (!self.m_blnPlaybackMode) {
                cmd = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&audio=%ld&cmd=stream_ctrl", sessionId, (unsigned long)playCh];
            }else {
                cmd = [NSString stringWithFormat:@"Playback.cgi?stream_id=%@&audio=%lu&cmd=stream_ctrl", sessionId, (unsigned long)playCh];
            }
            [cmdSender getData:cmd connection:&requestConnection synchronously:NO random:NO supportNCB:blnSupportNCB];
        }
    }
}

- (void)changeVideoResolution:(NSUInteger)mask video:(NSInteger)type
{
    NSString* cmd;
    if (!blnReceivingStream)    return;
	// change video resolution
    if(sessionId && !self.m_blnPlaybackMode) {
        
        NSInteger errCount = 0;
        while (!blnReceivingStream && errCount<100)
        {
            [NSThread sleepForTimeInterval:0.05f];
            errCount++;
        }
        cmd = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%lu&cmd=stream_ctrl&video_type=%ld",sessionId,(unsigned long)mask,(long)type];
        [cmdSender getData:cmd connection:&requestConnection synchronously:NO random:NO supportNCB:blnSupportNCB];
    }
}

- (void)openSound
{
    audioOn = YES;
    
    NSString* cmd;
    if (!blnReceivingStream)    return;
    // send cgi command
    if (!self.m_blnPlaybackMode){
        
        cmd = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&audio=%lu&cmd=stream_ctrl", sessionId, (unsigned long)currentPlayCH];
    }else {
        
        cmd = [NSString stringWithFormat:@"Playback.cgi?stream_id=%@&audio=%lu&cmd=stream_ctrl", sessionId, (unsigned long)currentPlayCH];
    }
	[cmdSender getData:cmd connection:&requestConnection synchronously:NO random:NO supportNCB:blnSupportNCB];
    
    if (audioControl) {
        [audioControl start];
    }
}

- (void)closeSound
{
    audioOn = NO;
    NSString* cmd;
    if (!blnReceivingStream)    return;
    
	// send cgi command
	if (!self.m_blnPlaybackMode){
        
        cmd = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&audio=%d&cmd=stream_ctrl", sessionId, 0];
    }else {
        
        cmd = [NSString stringWithFormat:@"Playback.cgi?stream_id=%@&audio=%d&cmd=stream_ctrl", sessionId, 0];
    }
	[cmdSender getData:cmd connection:&requestConnection synchronously:NO random:NO supportNCB:blnSupportNCB];

    if (audioControl) {
        [audioControl stop];
    }
}

- (void)changePlaybackMode:(NSInteger)mode withValue:(NSInteger)value
{
    NSString *cmd;
    if (!blnReceivingStream)    return;
    
    switch (mode) {
        case PLAYBACK_FORWARD:
            cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=stream_ctrl&stream_id=%@&action=forward",sessionId];
            [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
            break;
            
        case PLAYBACK_FAST_FORWARD :
            cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=stream_ctrl&stream_id=%@&action=fast-forward&speed=%ld",sessionId,(long)value];
            [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
            break;
            
        case PLAYBACK_FAST_BACKWARD :
            cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=stream_ctrl&stream_id=%@&action=fast-backward&speed=%ld",sessionId,(long)value];
            [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
            break;
            
        case PLAYBACK_PAUSE:
            cmd = [NSString stringWithFormat:@"Playback.cgi?cmd=stream_ctrl&stream_id=%@&action=pause",sessionId];
            [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
            break;
    }
    
    if (self.errorDesc) {
        NSLog(@"[SV] Playback Error(%ld):%@",(long)mode,self.errorDesc);
    }
}

- (void)stopStreaming
{
    if (audioOn) {
        [self closeSound];
    }
    
    [connectionTimer invalidate];   //J-BPS
    [super stopStreaming];
}

#pragma mark - Get Information

- (BOOL)getDeviceInfo
{
    BOOL ret = YES;
    NSString *cmd;
	NSString *response;
    NSInteger iErrorCode = 0;
    index = 1;
    ptzMask = 0;
    blnGetP3Mask = NO;
    
    // init output list to match channel and layout window
    if (!outputList) {
        outputList = [[NSMutableArray alloc] init];
        
        NSString *nan = [NSString stringWithFormat:@"99"];
        for (NSInteger i=0; i<MAX_CH_NUMBER; i++) {
            [outputList addObject:nan];
        }
    }
    
    if (![super getDeviceInfo]) { //get DDNS if need
        ret =  NO;
		goto Exit;
    }
    
    blnGetDvrInfo = YES;
    if (DeviceType == EPHD_04) {    //special case for EPHD04+, will use this detection method for all DVR later
        blnSupportNCB = NO;
        cmd = [NSString stringWithFormat:@"DvrInfo.xml"];
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
        if (errorDesc != nil) {
            errorDesc = nil;
            cmd = [NSString stringWithFormat:@"xml/DeviceInfo.xml"];
            [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
            
            if( errorDesc != nil)
            {
                iErrorCode = -1;
                ret = NO;
                goto Exit;
            }
            blnSupportNCB = NO;
        }
    } else {
        blnSupportNCB = (DeviceType >= 50);
        cmd = blnSupportNCB ? [NSString stringWithFormat:@"xml/DeviceInfo.xml"] : [NSString stringWithFormat:@"DvrInfo.xml"];
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
    }
    
    if(blnStopStreaming)
    {
        goto Exit;
    }
    // parse dvr info response
    response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //NSLog(@"[SR] response:%@",response);
    [cmdSender parseXmlResponse:response];
    [response release];
    
    // get login info
    cmd = [NSString stringWithFormat:@"Login_Info.cgi"];
    
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
    
    // connection failed
    if ( errorDesc!=nil ) {
        
        ret =  NO;
        goto Exit;
    }
    if (blnStopStreaming ) {
        
        goto Exit;
    }
    // parse login info response
    response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //NSLog(@"[P2SR] Login response:%@",response);
    [cmdSender parseXmlResponse:response];
    [response release];
    blnGetDvrInfo = NO;
    
    blnFirstConnection = NO;
    
    // get camera list
    if (!cameraList) {
        cameraList = [[NSMutableArray alloc] init];
    }
    
    blnGetChannelInfo = YES;
    
    NSLog(@"[P2] UserLevel:%ld",(long)userLevel);
    cmd = [NSString stringWithFormat:@"Camera.xml"];
    if (DeviceType == NVR8004X || DeviceType == ENVR8304D_E_X)
        cmd = [NSString stringWithFormat:@"IPCam.xml"];
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
    if(errorDesc != nil)
    {
        iErrorCode = -1; 
        ret = NO;
        goto Exit;
    }
    if(blnStopStreaming)
    {
        goto Exit;
    }
    // parse camera list response
    response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    //NSLog(@"[SR] response:%@",response);
    [cmdSender parseXmlResponse:response];
    [response release];
    
    blnGetChannelInfo = NO;
    
    if (ptzMask != 0) {
        self.blnChSupportPTZ = YES;
    }
    
    // set available channels
    if (blnSupportNCB) {   // P3 codebase
        if (userLevel == UA_ADMIN) {
            validChannel = install;
            validPlaybackCh = pbChnmap;
            validPtzChannel = ptzChnmap;
        }else {
            validChannel = install & ~covert;
            validPlaybackCh = pbChnmap & ~covert;
            validPtzChannel = ptzChnmap & ~covert;
        }
    }
    else {   // P2 codebase
        if (userLevel == UA_OPERATOR) {
            validChannel = install & ~covert;
        }else {
            validChannel = validPlaybackCh = validPtzChannel = install;
        }
    }
    
    
    if (validChannel==0 || cameraList.count==0) {
        errorDesc = NSLocalizedString(@"MsgInfoErr", nil);
        ret = NO;
        goto Exit;
    }
    
    // Get Disk Info
    blnGetDiskInfo = YES;
    cmd = [NSString stringWithFormat:@"Disk.xml"];
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
    if (errorDesc != nil) {
        iErrorCode = -1;
        ret = NO;
        goto Exit;
    }
    if(blnStopStreaming)
    {
        goto Exit;
    }
    // parse DiskInfo response
    response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //NSLog(@"[SR] response:%@",response);
    [cmdSender parseXmlResponse:response];
    [response release];
    
    // Get TimeZone Information
    if (blnSupportNCB) {
        cmd = [NSString stringWithFormat:@"DateTime.xml"];
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
        if (errorDesc != nil) {
            iErrorCode = -1;
            ret = NO;
            goto Exit;
        }
        if(blnStopStreaming)
        {
            goto Exit;
        }
        // parse TimeZone response
        response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        //NSLog(@"[SR] response:%@",response);
        [cmdSender parseXmlResponse:response];
        [response release];
        
        //Check daylight saving
        cmd = [NSString stringWithFormat:@"DaylightSaving.xml"];
        [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
        if (errorDesc != nil) {
            iErrorCode = -1;
            ret = NO;
            goto Exit;
        }
        if(blnStopStreaming)
        {
            goto Exit;
        }
        // parse response
        response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        //NSLog(@"[SR] response:%@",response);
        [cmdSender parseXmlResponse:response];
        [response release];
    }
    
    blnGetDiskInfo = NO;
    self.blnReceivingStream = NO;
    
    return YES;
    
Exit:
    
	self.blnReceivingStream = NO;
    [self stopStreaming];
	
	return ret;
}

- (void)getSearchIDFrom:(NSInteger)start To:(NSInteger)end byFilter:(NSInteger)mask
{
    NSString *cmd;
    NSString *response;
    
    cmd = [NSString stringWithFormat:@"search/Search.cgi?start_time=%ld&end_time=%ld&camera=65535&conditions=%ld&total=400",(long)start,(long)end,(long)mask];
    if (blnSupportNCB) {
        cmd = [cmd substringFromIndex:7];
    }
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
    
    // parse SearchID to response
    response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"response:%@",response);
    [cmdSender parseXmlResponse:response];
    [response release];
}

- (void)getEventResult
{
    NSString *cmd;
    NSString *response;

    cmd = [NSString stringWithFormat:@"search/Search.cgi?search_id=%@", m_SearchID];
    if (blnSupportNCB) {
        cmd = [cmd substringFromIndex:7];
    }
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
    
    // parse event result to response
    response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"response:%@",response);
    [cmdSender parseXmlResponse:response];
    [response release];
}

- (void)cancelEventSearch
{
    NSString *cmd;
    NSString *response;
    
    blnGetSearchInfo = NO;
    cmd = [NSString stringWithFormat:@"search/Search.cgi?search_id=%@&action=cancel",m_SearchID];
    if (blnSupportNCB) {
        cmd = [cmd substringFromIndex:7];
    }
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
    
    // parse cancel result to response
    response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"response:%@",response);
    [cmdSender parseXmlResponse:response];
    [response release];
}

- (NSInteger)getFrameStatus:(NSInteger)_Flag
{
    NSInteger status = 0;
    
    status |= FH_IS_ALARM(_Flag);
   // status |= FH_IS_EVENT(_Flag);
    status |= FH_IS_MOTION(_Flag);
    status |= FH_IS_VLOSS(_Flag);
    //NSLog(@"[P2] Flag:%d, status:%d",_Flag,status);
    
    return status;
}

- (void)getDVRStatus
{
    NSString *cmd;
    NSString *response;

    cmd = blnSupportNCB ? [NSString stringWithFormat:@"xml/Status.xml"] : [NSString stringWithFormat:@""];
    [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO supportNCB:blnSupportNCB];
    
    // parse SearchID to response
    response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //NSLog(@"response:%@",response);
    [cmdSender parseXmlResponse:response];
    [response release];
}
#ifdef BPS_DISPLAY
- (void)getBps
{
    //NSLog(@"[P2] BPS - %d kbps - Total:%d",8 * nReceiveCount/1024,nReceiveCount);
    [self.delegate EventCallback:[NSString stringWithFormat:@"BPS:%dkbps",(8*nReceiveCount)/1024]];
    nReceiveCount = 0;
}
#endif

#pragma mark - Parser Delegate

// Start of element
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    [currentXmlElement release];
	currentXmlElement = [elementName copy];
    
    if (blnGetDvrInfo && [currentXmlElement isEqualToString:@"RemoteChnAccess"])    //JamesLee++ 20140625 for P3 user covert
        blnGetP3Mask = YES;
    
    if (blnGetSearchInfo && [currentXmlElement isEqualToString:@"entry"] )
    {
        SearchResultEntry *emptyEntry;
        emptyEntry = [[SearchResultEntry alloc] init];
        [SearchResultlist addObject:emptyEntry];
        [emptyEntry release];
    }
    
    if (blnGetChannelInfo) {
        
        NSString *channelIndex = [NSString stringWithFormat:@"CH%02ld",(long)index];
        NSString *channelIndex2 = [NSString stringWithFormat:@"camera%02ld",(long)index];
        NSString *channelIndex3 = [NSString stringWithFormat:@"camera%ld",(long)index];
        if ([currentXmlElement isEqualToString:channelIndex]
            || [currentXmlElement isEqualToString:channelIndex2]
            || [currentXmlElement isEqualToString:channelIndex3]) {
            
            //NSLog(@"[SR] tag:%@",channelIndex);
            CameraInfo *emptyEntry;
            emptyEntry = [[[CameraInfo alloc] init] autorelease];
            emptyEntry.index = index;
            [cameraList addObject:emptyEntry];
        }
    }
}

// Found Character
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSMutableString *)string
{
    if ([string rangeOfString:@"\n"].location != NSNotFound)        // NewCodeBase issue
        return;
    
    if (blnGetDvrInfo) {
        if ( [currentXmlElement isEqualToString:@"user_level"] || [currentXmlElement isEqualToString:@"level"] ) {
            
            if (blnSupportNCB) {
                
                if ([string isEqualToString:@"Admin"])
                    userLevel = UA_ADMIN;
                else if ([string isEqualToString:@"Manager"])
                    userLevel = UA_MANAGER;
                else
                    userLevel = UA_OPERATOR;
            }else
                userLevel = [string integerValue];
        }
        else if ( [currentXmlElement isEqualToString:@"covert"] || [currentXmlElement isEqualToString:@"maskChannel"]) {
            
            covert = [string longLongValue];
        }
        else if ( [currentXmlElement isEqualToString:@"install"] || ([currentXmlElement isEqualToString:@"ctrlLiveChnmap"] && blnGetP3Mask) ) {
            
            install = [string longLongValue];
        }
        else if ( ([currentXmlElement isEqualToString:@"ctrlPbChnmap"] && blnGetP3Mask) ) {
            
            pbChnmap = [string longLongValue];
        }
        else if ( ([currentXmlElement isEqualToString:@"ctrlPtzChnmap"] && blnGetP3Mask) ) {
            
            ptzChnmap = [string longLongValue];
        }
        else if ( [currentXmlElement isEqualToString:@"ch_num"] || [currentXmlElement isEqualToString:@"NUM_CH_ANALOG_MAIN"]) {// add by robert hsu 20111031 for get maximum support channel
            
            iMaxSupportChannel = [string integerValue];
        }
        else if ([currentXmlElement isEqualToString:@"NUM_CH_IP_MAIN"]) {
            
            //if (DeviceType==NVR8004X || DeviceType==ENVR8304D_E_X)
            //    iMaxSupportChannel = [string integerValue];
            iMaxSupportChannel += [string integerValue];
        }
        else if ( [currentXmlElement isEqualToString:@"codec"] || [currentXmlElement isEqualToString:@"AV_FORMAT"]) {// add by robert hsu 20111031 for get maximum support channel
            
            if ([string integerValue]) {
                audioCodec = blnSupportNCB ? P3_AUDIO_CODEC([string integerValue]):P2_AUDIO_CODEC([string integerValue]);
                if (DeviceType==NVR8004X || DeviceType==ENVR8304D_E_X) {   //do not support other auCodec in pre-release version 20130620 James Lee
                    audioCodec = P3_AUDIO_TYPE_NVRG711;
                }
                NSLog(@"[P2] AudioCodec:%ld",(long)audioCodec);
            }
        }
    }
    else if (blnGetChannelInfo) {
        if ( [currentXmlElement isEqualToString:@"TITLE"] || [currentXmlElement isEqualToString:@"title"]) {
            if (cameraList.count != 0) {
                CameraInfo *currentEntry = [cameraList objectAtIndex:cameraList.count-1];
                currentEntry.title = [NSString stringWithFormat:@"%@",[string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            }
        }
        else if ( [currentXmlElement isEqualToString:@"NORFPS"] || [currentXmlElement isEqualToString:@"normalspeed"]) {
            if (cameraList.count != 0) {
                CameraInfo *currentEntry = [cameraList objectAtIndex:cameraList.count-1];
                currentEntry.fps = [string integerValue];
            }
        }
        else if ( [currentXmlElement isEqualToString:@"INSTALL"] || [currentXmlElement isEqualToString:@"install"] || [currentXmlElement isEqualToString:@"enabled"]) {
            if (cameraList.count != 0) {
                CameraInfo *currentEntry = [cameraList objectAtIndex:cameraList.count-1];
                currentEntry.install = [string integerValue];
            }
        }
        else if ( [currentXmlElement isEqualToString:@"COVERT"] || [currentXmlElement isEqualToString:@"covert"]) {
            if (cameraList.count != 0) {
                CameraInfo *currentEntry = [cameraList objectAtIndex:cameraList.count-1];
                currentEntry.covert = [string integerValue];
            }
        }
        else if( [currentXmlElement isEqualToString:@"PTZID"] || [currentXmlElement isEqualToString:@"enablePtz"]) {
            
            if ([string integerValue] != 0) {
                ptzMask |= 0x1<<(index-1);
            }
        }
    }
    else if (blnGetDiskInfo) {
        
        if ( [currentXmlElement isEqualToString:@"Total_Rec_Start"] || [currentXmlElement isEqualToString:@"totalRecordStart"] )
        {
            m_intDiskStartTime = [string integerValue];
        }
        else if ( [currentXmlElement isEqualToString:@"Total_Rec_End"] || [currentXmlElement isEqualToString:@"totalRecordEnd"]  )
        {
            m_intDiskEndTime = [string integerValue];
        }
        else if ( [currentXmlElement isEqualToString:@"currentTime"])
        {
            m_intCurrentTime = [string integerValue];
        }
        else if ( [currentXmlElement isEqualToString:@"timeZone"])
        {
            m_DiskGMT = [string copy];
            if ([m_DiskGMT rangeOfString:@"+"].location != NSNotFound)
            {
                NSString *str1 = [[m_DiskGMT componentsSeparatedByString:@"+"] objectAtIndex:1];
                NSUInteger hr = [[[str1 componentsSeparatedByString:@":"] objectAtIndex:0] integerValue];
                NSUInteger min = [[[str1 componentsSeparatedByString:@":"] objectAtIndex:1] integerValue];
                m_DiskGMT_sec = hr * 60 * 60 + min * 60;
            }
            else
            {
                NSString *str1 = [[m_DiskGMT componentsSeparatedByString:@"-"] objectAtIndex:1];
                NSUInteger hr = [[[str1 componentsSeparatedByString:@":"] objectAtIndex:0] integerValue];
                NSUInteger min = [[[str1 componentsSeparatedByString:@":"] objectAtIndex:1] integerValue];
                m_DiskGMT_sec = (hr * 60 * 60 + min * 60) * (-1);
            }
        }
        else if ([currentXmlElement isEqualToString:@"dlsEnable"])
        {
            m_intdlsEnable = [string integerValue];
        }
        else if (m_intdlsEnable == 1)
        {
            if ([currentXmlElement isEqualToString:@"startMon"])
            {
                m_intStartMonth = [string integerValue];
            }
            else if ([currentXmlElement isEqualToString:@"startWeek"])
            {
                m_intStartWeek = [string integerValue];
            }
            else if ([currentXmlElement isEqualToString:@"startWeekDay"])
            {
                m_intStartWeekday = [string integerValue];
            }
            else if ([currentXmlElement isEqualToString:@"startFromHour"])
            {
                m_intStartHour = [string integerValue];
            }
            else if ([currentXmlElement isEqualToString:@"startFromMin"])
            {
                m_intStartMin = [string integerValue];
                if (m_intStartMin == 1) {
                    m_intStartMin = 30;
                }
            }
            else if ([currentXmlElement isEqualToString:@"setHour"])
            {
                m_intSetHour = [string integerValue];
            }
            else if ([currentXmlElement isEqualToString:@"setMin"])
            {
                m_intSetMin = [string integerValue];
                if (m_intSetMin == 1) {
                    m_intSetMin = 30;
                }
            }
            else if ([currentXmlElement isEqualToString:@"endMon"])
            {
                m_intEndMonth = [string integerValue];
            }
            else if ([currentXmlElement isEqualToString:@"endWeek"])
            {
                m_intEndWeek = [string integerValue];
            }
            else if ([currentXmlElement isEqualToString:@"endWeekDay"])
            {
                m_intEndWeekday = [string integerValue];
            }
            else if ([currentXmlElement isEqualToString:@"endFromHour"])
            {
                m_intEndHour = [string integerValue];
            }
            else if ([currentXmlElement isEqualToString:@"endFromMin"])
            {
                m_intEndMin = [string integerValue];
                if (m_intEndMin == 1) {
                    m_intEndMin = 30;
                }
            }
        }
    }
    else if (blnGetSearchInfo) {
        
        if ([currentXmlElement isEqualToString:@"result"] )
        {
            m_intSearchResultTag = [string integerValue];
        }
        else if ([currentXmlElement isEqualToString:@"search_id"] )
        {
            m_SearchID = [string copy];
        }
        else if ([currentXmlElement isEqualToString:@"progress"] )
        {
            m_intSearchProgress = [string integerValue];
        }
        else if ([currentXmlElement isEqualToString:@"st"] )
        {
            SearchResultEntry *currentEntry;
            currentEntry = [SearchResultlist objectAtIndex:[SearchResultlist count]-1];
            NSScanner* pScanner = [NSScanner scannerWithString: string];
            [pScanner scanHexInt: &(currentEntry->uiStartTime)];
        }
        else if ([currentXmlElement isEqualToString:@"et"] )
        {
            SearchResultEntry *currentEntry;
            currentEntry = [SearchResultlist objectAtIndex:[SearchResultlist count]-1];
            NSScanner* pScanner = [NSScanner scannerWithString: string];
            [pScanner scanHexInt: &(currentEntry->uiEndTime)];
        }
        else if ([currentXmlElement isEqualToString:@"type"] )
        {
            SearchResultEntry *currentEntry;
            currentEntry = [SearchResultlist objectAtIndex:[SearchResultlist count]-1];
            NSScanner* pScanner = [NSScanner scannerWithString: string];
            [pScanner scanHexInt: &(currentEntry->uiEventType)];
        }
        else if ([currentXmlElement isEqualToString:@"cam"] )
        {
            SearchResultEntry *currentEntry;
            currentEntry = [SearchResultlist objectAtIndex:[SearchResultlist count]-1];
            NSScanner* pScanner = [NSScanner scannerWithString: string];
            [pScanner scanHexInt: &(currentEntry->uiChannel)];
        }
    }
}

// End Element
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
{
    [currentXmlElement release];
	currentXmlElement = [elementName copy];
    
    if (blnGetChannelInfo) {
        NSString *channelIndex = [NSString stringWithFormat:@"CH%02ld",(long)index];
        NSString *channelIndex2 = [NSString stringWithFormat:@"camera%02ld",(long)index];
        NSString *channelIndex3 = [NSString stringWithFormat:@"camera%ld",(long)index];
        if ([currentXmlElement isEqualToString:channelIndex]
            || [currentXmlElement isEqualToString:channelIndex2]
            || [currentXmlElement isEqualToString:channelIndex3]) {
            CameraInfo *currentEntry = [cameraList objectAtIndex:cameraList.count-1];
            
            if (blnSupportNCB && DeviceType!=EMV_401_801_1601) {    //P3只看URM裡的ctrlLiveChnmap
                
                if (!(0x1<<(currentEntry.index-1) & install) || !currentEntry.install) {
                    NSLog(@"[P2] remove CH:%02ld",(long)currentEntry.index);
                    [cameraList removeObject:currentEntry];
                }
                
            } else if ((currentEntry.install & ~currentEntry.covert)==0
                       && !(DeviceType==EMV_200||DeviceType==EMV_200S_400S||DeviceType==EMV_400_800_1200)
                       && userLevel == UA_OPERATOR) {
                
                [cameraList removeObject:currentEntry];
            }
            index++;
        }
    }
    
    if (blnGetDvrInfo && [currentXmlElement isEqualToString:@"RemoteChnAccess"])    //JamesLee++ 20140625 for P3 user covert
        blnGetP3Mask = NO;
}

#pragma mark Connection Delegate

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.
// it's a kind of callback function
//when call CommandSender.getData will set self to be delegate
//then in CommandSender.getData will set to become NSURLConnection's delegate
//after NSURLConnection receive data will trigger this method
{
    //NSLog(@"Received: %d", [data length]);
	
	if ( theConnection==requestConnection ) {
		
		[responseData appendData:data];
	}
	else if ( theConnection==streamConnection ) {
#ifdef BPS_DISPLAY
        nReceiveCount += data.length;   //J-BPS
#endif
		self.blnReceivingStream = YES;
        
		if ( blnStopStreaming ) return;
		
		int readLen = 0;
		long remanentLen = [data length];
		
		// check if NO stream
		if ( remanentLen<100) {
			
            if(frameOffset!=frameSize)
            {
                
            }
            else
            {
                BOOL isNullStream = YES;
                char* buf = (char*)[data bytes];
                
                for (int i=0; i<remanentLen; i++) {
                    if (buf[i]!=0) {
                        isNullStream=NO;
                    }
                }
                
                if (isNullStream) {
                    NSLog(@"[P2] No Streaming %ld packetRemanent=%d " ,remanentLen ,packetRemanent);
                    
//                    if (dualStream!=0) {                                                  //disable by James 20130403
//                        self.errorDesc = NSLocalizedString(@"MsgSubDenied2", nil);
//                    }
//                    else {
//                        self.errorDesc = NSLocalizedString(@"MsgReceiveNull", nil);
//                    }
                    
//                    self.blnReceivingStream = NO;     
//                    [self stopStreaming];
//                    return;
                }
            }
		}
		
		while ( remanentLen>0 ) {
			
			// check whether packet receive finished
			if ( packetHeaderOffset==sizeof(P2_PACKET_HEADER) && packetRemanent==0 ) {
				
				// reset packet header data length
				packetHeader.audio_len = 0;
				packetHeader.meta_len  = 0;
				packetHeader.video_len = 0;
				
				packetHeaderOffset = 0;
			}
			
			// read packet header
			if ( packetHeaderOffset<sizeof(P2_PACKET_HEADER) || packetRemanent==0 )
            {
				
				Byte *buf = (Byte *)(&packetHeader)+packetHeaderOffset;
				unsigned long remains = sizeof(P2_PACKET_HEADER)-packetHeaderOffset;
				unsigned long len = MIN(remains,remanentLen);
				
				[data getBytes:buf range:NSMakeRange(readLen, len)];
				
				readLen += len;
				remanentLen -= len;
				packetHeaderOffset += len;
                
                
				
				// packet header receive finished
				if ( packetHeaderOffset==sizeof(P2_PACKET_HEADER) ) {
					
					packetHeader.audio_len = ntohs(packetHeader.audio_len);
					packetHeader.meta_len  = ntohs(packetHeader.meta_len);
					packetHeader.video_len = ntohl(packetHeader.video_len);
					
//					NSLog(@"packet(%d/%lu) - flag:0x%02x audio:%d meta:%d video:%u",
//						  readLen,(unsigned long)[data length],packetHeader.flag[0],packetHeader.audio_len,
//						  packetHeader.meta_len,packetHeader.video_len);
					
					packetRemanent = packetHeader.audio_len + packetHeader.meta_len + packetHeader.video_len;
					
					// check packet header
					if ( (packetHeader.flag[0]&(~(FRAME_START|FRAME_END)))!=0x0 ) {
						
						NSLog(@"[P2] Packet header data error!");
                        
                        self.errorDesc = NSLocalizedString(@"MsgDataErr", nil);
                        self.blnReceivingStream = NO;
						[self stopStreaming];
						return;
					}
					
					// receive a new frame
					if ( (packetHeader.flag[0]&FRAME_START)!=0x0 ) {
						
						// reset parameters
						memset(&pFrameBuf[0],0,MAX_FRAME_SIZE); // clean buffer
						frameHeaderOffset = 0;
						frameOffset = 0;
						frameSize = 0;
					}
					
					// reset audio/meta paras
					amHeaderOffset = 0;
					amSize = 0;
					amOffset = 0;
				}
			}
			
			// get AUDIO frame
			if ( packetHeader.audio_len!=0 ) {
				
				if ( amHeaderOffset<sizeof(P2_AM_HEADER) && remanentLen>0 && packetRemanent>0 ) {
					
					Byte *buf = (Byte *)(&amHeader)+amHeaderOffset;
					long remain = sizeof(P2_AM_HEADER)-amHeaderOffset;
					long len = MIN(remain, MIN(remanentLen, packetRemanent));
					
					[data getBytes:buf range:NSMakeRange(readLen, len)];
					
					readLen += len;
					remanentLen -= len;
					amHeaderOffset += len;
					packetRemanent -= len;
					
					// am header receive finished
					if ( packetHeaderOffset==sizeof(P2_PACKET_HEADER) ) {
						
						amSize = packetHeader.audio_len-sizeof(P2_AM_HEADER);
						amOffset = 0;
						
						pAMBuf = malloc(amSize);
						
						//NSLog(@"AM header received: ch=%d flag=0x%02x size=%d", amHeader.ch, amHeader.flag, amSize);
					}
				}
				
				if ( amOffset<amSize && remanentLen>0 && packetRemanent>0 ) {
					
					Byte *buf = pAMBuf+amOffset;
					long remain = amSize-amOffset;
					long len = MIN(remain, MIN(remanentLen, packetRemanent));
					
					[data getBytes:buf range:NSMakeRange(readLen, len)];
					
					readLen += len;
					remanentLen -= len;
					amOffset += len;
					packetRemanent -= len;
					
					// am data receive finished
					if ( amOffset==amSize ) {
						
						//NSLog(@"AUDIO received finished");
						
						// process the audio data
                        // check channel
                        NSInteger playingCH = ((VideoDisplayView*) [videoControl.aryDisplayViews objectAtIndex:videoControl.selectWindow]).playingChannel;
                        
                        // check amFlag to find codec
                        NSInteger auCodec;
                        
                        if ( audioOn ) {
                            //NSLog(@"AudioType audio type: 0x%02x", amHeader.flag);
                            // create audio decoder
                            auCodec = blnSupportNCB ? P3_AUDIO_CODEC(amHeader.flag):P2_AUDIO_CODEC(amHeader.flag);
                            
                            if ( (((0x1<<(amHeader.ch)) & 0x1<<playingCH) != 0) && audioOn ) {
                                
                                if (audioControl!=nil && (amHeader.flag != audioControl.amFlag)) {
                                    
                                    //[audioControl close];
                                    //NSLog(@"[AC] flag:%d,codecID:%d",amHeader.flag&0xF0,audioControl.codecId);
                                    switch (auCodec) {
                                            
                                        case P2_AUDIO_TYPE_ADPCM: // HiSi (header:4 audio:168-4=164 bsp:4or8?)
                                        case P3_AUDIO_TYPE_ADPCM:
                                            [audioControl initWithCodecId:AV_CODEC_ID_ADPCM_IMA_WAV srate:8000 bps:4 balign:164 fsize:321 channel:1];
                                            NSLog(@"[P2] Create new audio decoder with codec id %d", AV_CODEC_ID_ADPCM_IMA_WAV);
                                            break;
                                            
                                        case P2_AUDIO_TYPE_G711: // Stretch (header:256 audio:2816-256=2560)
                                        case P3_AUDIO_TYPE_G711:
                                            [audioControl initWithCodecId:AV_CODEC_ID_PCM_MULAW srate:16000 bps:8 balign:2560 fsize:2560 channel:amHeader.flag==0x33?2:1];
                                            NSLog(@"[P2] Create new audio decoder with codec id %d", AV_CODEC_ID_PCM_MULAW);
                                            break;
                                            
                                            
                                        case P3_AUDIO_TYPE_NVRG711:
                                            [audioControl initWithCodecId:AV_CODEC_ID_PCM_MULAW srate:8000 bps:8 balign:320 fsize:320 channel:1];
                                            NSLog(@"[P2] Create new audio decoder with codec id(NVR) %d", AV_CODEC_ID_PCM_MULAW);
                                            break;
                                            
                                        case P2_AUDIO_TYPE_G723:
                                            audioControl = [audioControl initWithG723Codec];
                                            NSLog(@"[P2] Create new audio decoder with codec id %d", AV_CODEC_ID_G723_1);
                                            break;
                                        default:
                                            NSLog(@"[P2] Unupported audio type: 0x%02x", amHeader.flag);
                                            break;
                                    }
                                    audioControl.amFlag = amHeader.flag;
                                    if (audioControl!=nil)
                                        [audioControl start];
                                }
                            }
                            
                            // decode and play the audio frame
                            if (audioControl!=nil) {
                                
                                int offset = 0;
                                long length = amSize;
                                
                                switch (auCodec) {
                                        
                                    case P2_AUDIO_TYPE_ADPCM:
                                    case P3_AUDIO_TYPE_ADPCM:
                                        while (length>=168) { // may have more than one frame at one time
                                            [audioControl playAudio:pAMBuf+offset+4 length:164];
                                            offset += 168;
                                            length -= 168;
                                        }
                                        break;
                                        
                                    case P2_AUDIO_TYPE_G711:
                                    case P3_AUDIO_TYPE_G711:
                                            [audioControl playAudio:pAMBuf+256 length:2560];
                                        break;
                                        
                                    case P3_AUDIO_TYPE_NVRG711:
                                        [audioControl playAudio:pAMBuf length:320];
                                        break;
                                        
                                    case P2_AUDIO_TYPE_G723:    //Paragon2(EPARA-16D3)    stretch
                                        [audioControl playAudio:pAMBuf length:length];
                                        break;
                                        
                                    default:
                                        break;
                                }
                            }
                        }
						
						free(pAMBuf);
					}
				}
			}
			
			// get META frame (do nothing)
			if ( packetHeader.meta_len!=0 ) {
				
				if ( amHeaderOffset<sizeof(P2_AM_HEADER) && remanentLen>0 && packetRemanent>0 ) {
					
					Byte *buf = (Byte *)(&amHeader)+amHeaderOffset;
					unsigned long remain = sizeof(P2_AM_HEADER)-amHeaderOffset;
					unsigned long len = MIN(remain, MIN(remanentLen, packetRemanent));
					
					[data getBytes:buf range:NSMakeRange(readLen, len)];
					
					readLen += len;
					remanentLen -= len;
					amHeaderOffset += len;
					packetRemanent -= len;
					
					// am header receive finished
					if ( packetHeaderOffset==sizeof(P2_PACKET_HEADER) ) {
						
						amSize = packetHeader.meta_len-sizeof(P2_AM_HEADER);
						amOffset = 0;
						
						pAMBuf = malloc(amSize);
						
						//NSLog(@"AM header received: ch=%d flag=0x%02x size=%d", amHeader.ch, amHeader.flag, amSize);
					}
				}
				
				if ( amOffset<amSize && remanentLen>0 && packetRemanent>0 ) {
					
					Byte *buf = pAMBuf+amOffset;
					unsigned long remain = amSize-amOffset;
					unsigned long len = MIN(remain, MIN(remanentLen, packetRemanent));
					
					[data getBytes:buf range:NSMakeRange(readLen, len)];
					
					readLen += len;
					remanentLen -= len;
					amOffset += len;
					packetRemanent -= len;
					
					// am data receive finished
					if ( amOffset==amSize ) {
						
						NSLog(@"[P2] META received finished");
						
						// process meta data
						
						free(pAMBuf);
					}
				}
			}
			
			// get VIDEO frame
			if ( packetHeader.video_len!=0 ) {
				
                
				// read frame header data
				if ( frameHeaderOffset<sizeof(P2_FRAME_HEADER) && remanentLen>0 && packetRemanent>0 )
                {
                    
					
					Byte *buf = &pFrameBuf[frameHeaderOffset];
					long remains = sizeof(P2_FRAME_HEADER)-frameHeaderOffset;
					long len = MIN(remains,MIN(remanentLen,packetRemanent));
					
					[data getBytes:buf range:NSMakeRange(readLen, len)];
					
					readLen += len;
					remanentLen -= len;
					frameHeaderOffset += len;
					packetRemanent -= len;
					
					// header data finish
					if ( frameHeaderOffset==sizeof(P2_FRAME_HEADER) ) {
						
						P2_FRAME_HEADER	*pHeader  = (P2_FRAME_HEADER *)&pFrameBuf[0];
                        
                        // convert header for different endian
						pHeader->ms			= ntohs(pHeader->ms);
						pHeader->sec		= ntohl(pHeader->sec);
						pHeader->seq		= ntohl(pHeader->seq);
						pHeader->time_zone	= ntohs(pHeader->time_zone);
						pHeader->width		= ntohs(pHeader->width);
						pHeader->height		= ntohs(pHeader->height);
						pHeader->size		= ntohl(pHeader->size);
						pHeader->event_flag = ntohl(pHeader->event_flag);
                        
						frameSize	= pHeader->size;
						frameWidth	= pHeader->width;
						frameHeight = pHeader->height;
                        
						// check frame header data  
						if ( pHeader->type > IMAGE_TYPE_PFRAME ) {
							
							NSLog(@"[P2] Frame header data error!");
                            
                            self.blnReceivingStream = NO;
                            self.errorDesc = NSLocalizedString(@"MsgDataErr", nil);
							[self stopStreaming];
							return;
						}
                        
                        //video available, callback to UI Controller
                        if (!blnVideoAvailable) {
                            [self.delegate EventCallback:@"VIDEO_OK"];
                            blnVideoAvailable = YES;
                            
                            if (m_intDiskStartTime == 0 || m_intDiskEndTime == 0)
                                [self.delegate EventCallback:@"PLAYBACK_NO"];
                        }
					}
				}
				
				// read frame data
				if ( frameOffset<frameSize && remanentLen>0 && packetRemanent>0 )
                {
					
					// for take off Stretch header
					// I-frame: (256-header)(24-SPS)(256-header)(9-PPS)(256-header)(FRAME)
					// P-frame: (256-header)(FRAME)
					
					P2_FRAME_HEADER  *pHeader  = (P2_FRAME_HEADER *)&pFrameBuf[0];
					
					if ( (pHeader->codec_type&0xf0)==CODEC_H264_TYPE_B )
                    {
						
						// 1st Stretch header
						if (frameOffset<sizeof(STCH_HEADER)) {
							
							Byte *buf = (Byte *)(&stchHeader)+stchHeaderOffset;
							
							long remains = sizeof(STCH_HEADER)-stchHeaderOffset;
							long len = MIN(remains,MIN(remanentLen,packetRemanent));
							
							[data getBytes:buf range:NSMakeRange(readLen, len)];
							//NSLog(@"read %d bytes", len);
							
							readLen += len;
							remanentLen -= len;
							stchHeaderOffset += len;
							frameOffset += len;
							packetRemanent -= len;
							
							// header data finish
							if ( stchHeaderOffset==sizeof(STCH_HEADER) ) {
                                
								// get payload(SPS/PPS) size
								stchSpsSize	= ntohl(stchHeader.payload_size);
								stchHeaderOffset = 0;
							}
						}
						
						if (pHeader->type==IMAGE_TYPE_IFRAME) {
							// SPS
							if (frameOffset<sizeof(STCH_HEADER)+stchSpsSize) {
								
								Byte *buf = &pFrameBuf[sizeof(P2_FRAME_HEADER)+frameOffset-sizeof(STCH_HEADER)];
								long remains = sizeof(STCH_HEADER)+stchSpsSize-frameOffset;
								long len = MIN(remains,MIN(remanentLen,packetRemanent));
								
								[data getBytes:buf range:NSMakeRange(readLen, len)];
								//NSLog(@"read %d bytes", len);
								
								readLen += len;
								remanentLen -= len;
								frameOffset += len;
								packetRemanent -= len;
							}
							// 2nd Stretch header
							if (frameOffset<sizeof(STCH_HEADER)+stchSpsSize+sizeof(STCH_HEADER)) {
								
								Byte *buf = (Byte *)(&stchHeader)+stchHeaderOffset;
								
								long remains = sizeof(STCH_HEADER)-stchHeaderOffset;
								long len = MIN(remains,MIN(remanentLen,packetRemanent));
								
								[data getBytes:buf range:NSMakeRange(readLen, len)];
								//NSLog(@"read %d bytes", len);
								
								readLen += len;
								remanentLen -= len;
								stchHeaderOffset += len;
								frameOffset += len;
								packetRemanent -= len;
								
								// header data finish
								if ( stchHeaderOffset==sizeof(STCH_HEADER) ) {
									
									// get payload(SPS/PPS) size
									stchPpsSize	= ntohl(stchHeader.payload_size);
									stchHeaderOffset = 0;
									
									//NSLog(@"SPS Size: %d PPS Size: %d", stchSpsSize, stchPpsSize);
								}
							}
							// PPS
							if (frameOffset<sizeof(STCH_HEADER)+stchSpsSize+sizeof(STCH_HEADER)+stchPpsSize) {
								
								Byte *buf = &pFrameBuf[sizeof(P2_FRAME_HEADER)+frameOffset-(sizeof(STCH_HEADER)*2)]; /////
								long remains = sizeof(STCH_HEADER)+stchSpsSize+sizeof(STCH_HEADER)+stchPpsSize-frameOffset;
								long len = MIN(remains,MIN(remanentLen,packetRemanent));
								
								[data getBytes:buf range:NSMakeRange(readLen, len)];
								//NSLog(@"read %d bytes", len);
								
								readLen += len;
								remanentLen -= len;
								frameOffset += len;
								packetRemanent -= len;
							}
							// 3rd Stretch header
							if (frameOffset<sizeof(STCH_HEADER)+stchSpsSize+sizeof(STCH_HEADER)+stchPpsSize+sizeof(STCH_HEADER)) {
								
								long remains = sizeof(STCH_HEADER)+stchSpsSize+sizeof(STCH_HEADER)+stchPpsSize+sizeof(STCH_HEADER)-frameOffset;
								long len = MIN(remains,MIN(remanentLen,packetRemanent));
								
								readLen += len;
								remanentLen -= len;
								frameOffset += len;
								packetRemanent -= len;
							}
						}
					}
					// Stretch header END
					
					int stretchHeaderLen = (pHeader->codec_type&0xf0)!=CODEC_H264_TYPE_B?0:(pHeader->type==IMAGE_TYPE_IFRAME?(sizeof(STCH_HEADER)*3):sizeof(STCH_HEADER));
					
					Byte *buf = &pFrameBuf[sizeof(P2_FRAME_HEADER)+frameOffset-stretchHeaderLen];
					long remains = frameSize-frameOffset;
					long len = MIN(remains,MIN(remanentLen,packetRemanent));
					
					[data getBytes:buf range:NSMakeRange(readLen, len)];
					
					readLen += len;
					remanentLen -= len;
					frameOffset += len;
					packetRemanent -= len;
					
					//frame data finish
					if ( frameOffset==frameSize ) {
						
						P2_FRAME_HEADER	*pHeader	= (P2_FRAME_HEADER *)&pFrameBuf[0];
						void			*data		= (void *)&pFrameBuf[sizeof(P2_FRAME_HEADER)];
						
						// decode and show the received VIDEO frame
                        // check video channel
                        if ( ((0x1<<(pHeader->ch)) & validChannel) != 0 ) {
                            
                            if (blnStartPlayBack && ((0x1<<(pHeader->ch)) & validPlaybackCh) == 0) {
                                
                                NSInteger iBufferIdx = [[outputList objectAtIndex:pHeader->ch] integerValue];
                                
                                if (iBufferIdx == 99) {
                                    continue;
                                }
                                //NSLog(@"iBufferIdx:%ld,pHeader->ch:%d",(long)iBufferIdx,(int)pHeader->ch);
                                [videoControl setPlayingChannelByCH:iBufferIdx Playing:pHeader->ch];  //從外面設定，以免無法Close
                                
                                UIImage *image = [UIImage imageNamed: @"NavBarLogo.png"];
                                
                                VideoDisplayView *tmpView = [videoControl.aryDisplayViews objectAtIndex:iBufferIdx];
                                tmpView.blnIsPlay = NO;
                                [tmpView stopActivityIndicatorAnimating];
                                [tmpView setVideoImage:image];
                                //[self.videoControl resetByCH:pHeader->ch];
                            }
                            else {
                                
                                NSInteger iBufferIdx = [[outputList objectAtIndex:pHeader->ch] integerValue];
                                
                                if (iBufferIdx == 99) {
                                    continue;
                                }
                                //NSLog(@"iBufferIdx:%ld,pHeader->ch:%d",(long)iBufferIdx,(int)pHeader->ch);
                                [videoControl setPlayingChannelByCH:iBufferIdx Playing:pHeader->ch];  //從外面設定，以免無法Close
                                
                                //channel status callback
                                CameraInfo *targetInfo = [self getInfoFromChannelIndex:pHeader->ch];
                                targetInfo.status = [self getFrameStatus:pHeader->event_flag];
                                //NSLog(@"[P2] CH:%d, Flag:%lu",pHeader->ch,pHeader->event_flag);
                                
                                VIDEO_PACKAGE videoPkg;
                                
                                videoPkg.sec	= blnSupportNCB? pHeader->sec+pHeader->day_light_saving*30*60+(pHeader->time_zone*15-720)*60:pHeader->sec;
                                videoPkg.ms		= pHeader->ms;
                                videoPkg.seq	= pHeader->seq;
                                int stretchHeaderLen = (pHeader->codec_type&0xf0)!=CODEC_H264_TYPE_B?0:(pHeader->type==IMAGE_TYPE_IFRAME?(sizeof(STCH_HEADER)*3):sizeof(STCH_HEADER));
                                videoPkg.size	= pHeader->size-pHeader->title_len-stretchHeaderLen;
                                videoPkg.width	= pHeader->width;
                                videoPkg.height	= pHeader->height;
                                videoPkg.data	= malloc(pHeader->size);
                                videoPkg.channel = 0x1<<pHeader->ch;
                                memcpy(videoPkg.data, data, pHeader->size);
                                
                                if ( pHeader->title_len!=0 ) {
                                    
                                    NSString *title;
                                    
                                    if (IS_UNICODE(DeviceType)||blnSupportNCB) {  // Modify by James Lee 20120522 for unicode camera title
                                        title = [[NSString alloc] initWithBytes:&pFrameBuf[sizeof(P2_FRAME_HEADER)+pHeader->size-pHeader->title_len-stretchHeaderLen] length:pHeader->title_len encoding:NSUTF16LittleEndianStringEncoding];
                                        if ([title rangeOfString:@"\0"].location != NSNotFound) {           // 20120402 add by James Lee, Fix the bug of title length error
                                            NSInteger strLen = [title rangeOfString:@"\0"].location;
                                            //NSLog(@"strLen:%d",strLen);
                                            [title release];
                                            title = [[NSString alloc] initWithBytes:&pFrameBuf[sizeof(P2_FRAME_HEADER)+pHeader->size-pHeader->title_len-stretchHeaderLen] length:2*strLen+2 encoding:NSUTF16LittleEndianStringEncoding];
                                        }
                                    }
                                    else {
                                        title = [[NSString alloc] initWithUTF8String:(char *)&pFrameBuf[sizeof(P2_FRAME_HEADER)+pHeader->size-pHeader->title_len-stretchHeaderLen]];
                                    }
                                    if ( ![title isEqualToString:[self.videoControl.aryTitle objectAtIndex:iBufferIdx]]) {
                                        
                                        [self.videoControl setTitleByCH:iBufferIdx title:title];//add by robert hsu 20120104
                                    }
                                    
                                    [title release];
                                }
                                
                                switch (pHeader->codec_type&0xf0) {
                                    case CODEC_H264_TYPE_A:
                                    case CODEC_H264_TYPE_B:
                                        videoPkg.codec = AV_CODEC_ID_H264;
                                        break;
                                    case CODEC_MPEG4_TYPE_A:
                                        videoPkg.codec = AV_CODEC_ID_MPEG4;
                                        break;
                                    case CODEC_MJPEG_TYPE_A:
                                        videoPkg.codec = AV_CODEC_ID_MJPEG;
                                        self.blnIsMJPEG = YES;
                                        break;
                                    default:
                                        videoPkg.codec = AV_CODEC_ID_NONE;
                                        break;
                                }
                                
                                switch (pHeader->type) {
                                    case IMAGE_TYPE_IFRAME:
                                        videoPkg.type = VO_TYPE_IFRAME;
                                        break;
                                    case IMAGE_TYPE_PFRAME:
                                        videoPkg.type = VO_TYPE_PFRAME;
                                        break;
                                    default:
                                        videoPkg.type = VO_TYPE_NONE;
                                        break;
                                }                                
                                //NSLog(@"[SR] ReceiveCH:%ld, Type:%d, seq:%u, time:%u",(long)iBufferIdx,pHeader->type,pHeader->seq,pHeader->sec);
                                [self.videoControl putFrameIntoBufferByCH:&videoPkg chIndex:iBufferIdx];
                            }
                        }
                        else {
                            
                            NSInteger iBufferIdx = [[outputList objectAtIndex:pHeader->ch] integerValue];
                            
                            if (iBufferIdx == 99) {
                                continue;
                            }
                            //NSLog(@"iBufferIdx:%ld,pHeader->ch:%d",(long)iBufferIdx,(int)pHeader->ch);
                            [videoControl setPlayingChannelByCH:iBufferIdx Playing:pHeader->ch];  //從外面設定，以免無法Close
                            
                            UIImage *image = [UIImage imageNamed: @"NavBarLogo.png"];
                            
                            VideoDisplayView *tmpView = [videoControl.aryDisplayViews objectAtIndex:iBufferIdx];
                            tmpView.blnIsPlay = NO;
                            [tmpView stopActivityIndicatorAnimating];
                            [tmpView setVideoImage:image];
                            //[self.videoControl resetByCH:pHeader->ch];
                        }
					}
				} 
			}
		}
    }
}

@end
