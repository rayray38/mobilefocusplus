//
//  P2StreamReceiver.h
//  EFViewerHD
//
//  Created by James Lee on 12/10/9.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "P2Net.h"
#import "StreamReceiver.h"

@interface P2StreamReceiver : StreamReceiver
{
    NSUInteger      covert;
    NSUInteger      install;
    NSUInteger      pbChnmap;
    NSUInteger      ptzChnmap;
    NSUInteger      ptzMask;
    NSInteger       index;
    
    P2_PACKET_HEADER	packetHeader;
	P2_AM_HEADER		amHeader;
	STCH_HEADER			stchHeader;
	Byte pFrameBuf[MAX_FRAME_SIZE];
	Byte* pAMBuf;
	
    BOOL secondConnection;
    BOOL blnGetDvrInfo;
    BOOL blnGetDiskInfo;
    BOOL blnSupportNCB;
    BOOL blnGetP3Mask;
    BOOL blnVideoAvailable;
    
	int frameSize;
	int stchSpsSize;
	int stchPpsSize;
	int packetRemanent;
	int packetHeaderOffset;
	int stchHeaderOffset;
	int frameHeaderOffset;
	int frameOffset;
	
	int amHeaderOffset;
	int amSize;
	int amOffset;
    
    int nReceiveCount;
    
    NSTimer   *connectionTimer;
}

- (NSInteger)getFrameStatus:(NSInteger)_Flag;

@end
