//
//  P2PtzController.m
//  EFViewer
//
//  Created by James Lee on 2010/6/21.
//  Copyright 2010 EverFocus. All rights reserved.
//

#import "P2PtzController.h"


@implementation P2PtzController


#pragma mark -
#pragma mark CGI Command

- (void)ptzTiltDown:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=tilt&value=down&step=1",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzTiltUp:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=tilt&value=up&step=1",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzPanLeft:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=pan&value=left&step=1",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzPanRight:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=pan&value=right&step=1",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzLeftUp:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=pan_tilt&value=left_up&step=1",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzLeftDown:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=pan_tilt&value=left_down&step=1",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzRightUp:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=pan_tilt&value=right_up&step=1",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzRightDown:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=pan_tilt&value=right_down&step=1",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzZoomIn:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=zoom&value=in&step=1",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzZoomOut:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=zoom&value=out&step=1",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzFocusFar:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=focus&value=far&step=2",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzFocusNear:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=focus&value=near&step=2",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzIrisOpen:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=iris&value=open&step=2",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzIrisClose:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=iris&value=close&step=2",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzStop:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=stop",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzPresetGo:(NSInteger)ch value:(NSInteger)value
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=go_preset&value=%ld",
						   self.sessionId,1<<ch,(long)value];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzPresetSet:(NSInteger)ch value:(NSInteger)value
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=set_preset&value=%ld",
						   self.sessionId,1<<ch,(long)value];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzPresetCancel:(NSInteger)ch value:(NSInteger)value
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=clear_preset&value=%ld",
						   self.sessionId,1<<ch,(long)value]; // action is "clear_preset" NOT "clear"
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzAutoPanRun:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=auto_pan&value=%@",
						   self.sessionId,1<<ch,[self.ptzType isEqualToString:@"NVR"] ? @"2" : @"run"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzAutoPan360Run:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=auto_pan&value=run",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzAutoPanStop:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=auto_pan&value=stop",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzPattern:(NSInteger)ch value:(NSInteger)value
{
	NSString *urlString = @"";
    if ([self.ptzType isEqualToString:@"NVR"])
        urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=pattern&value=run&value2=%ld",self.sessionId,1<<ch,(long)value];
    else
        urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=pattern&value=%ld",self.sessionId,1<<ch,(long)value];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzTour:(NSInteger)ch value:(NSInteger)value
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=go_tour&value=%ld",
						   self.sessionId,1<<ch,(long)value];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzOsdOpen:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=menu&value=open",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

- (void)ptzOsdExit:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"Live.cgi?stream_id=%@&camera=%d&cmd=ptz_ctrl&action=menu&value=exit",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:NO supportNCB:self.blnNcb];
}

@end
