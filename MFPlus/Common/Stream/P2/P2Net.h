//
//  P2Net.h
//  LiveStream
//
//  Created by James Lee on 2010/8/5.
//  Copyright 2010 EverFocus. All rights reserved.
//
//  20150325 change all of "long" to "int" because of arm64

#pragma pack(push,1)

#define FRAME_START		0x80
#define FRAME_MIDDLE	0x00
#define FRAME_END		0x40

typedef struct _P2_PACKET_HEADER
{
	unsigned char  flag[2];		// 2:0 Position flag of video frame slice
								// flag[0] = FRAME_START OR FRAME_MIDDLE OR FRAME_END OR (FRAME_START|FRAME_END)
								// flag[1] = 0 always
	unsigned short audio_len;	// 2:2 Length of audio data
								// audio_len > 0 : packet has audio
	unsigned short meta_len;	// 2:4 Length of meta data
								// meta_len > 0 : packet has metadata
	unsigned int  video_len;	// 4:6 Length of video data
								// video_len > 0 : video slice in packet
} P2_PACKET_HEADER, *PP2_PACKET_HEADER; // 10 bytes   ;14 bytes in 64-bit


#define IMAGE_TYPE_IFRAME	0
#define IMAGE_TYPE_PFRAME	1

#define CODEC_MPEG4_TYPE_A	0x10 // MP4
#define CODEC_H264_TYPE_A	0x20 // Hisi raw data
#define CODEC_H264_TYPE_B	0x30 // Stretch H264
#define CODEC_MJPEG_TYPE_A	0x40 // MJPEG
#define CODEC_H264_TYPE_C   0x50 // Hisi with header

#define EVENT_TYPE_ALARM	0x01
#define EVENT_TYPE_MOTION	0x02
#define EVENT_TYPE_VLOSS	0x04

typedef struct _P2_FRAME_HEADER
{
	unsigned char	ch;					// 1:0 Channel index
	unsigned char	type;				// 1:1 Frame type: IMAGE_TYPE_IFRAME, IMAGE_TYPE_PFRAME
	unsigned short	ms;					// 2:2 Millisecond timestamp of the frame
	unsigned int	sec;				// 4:4 Second timestamp of the frame Seconds from 1970,1,1,0:0, local time
	unsigned int	seq;				// 4:8 Sequence number of frames, counting from 0 after powering up
	short			time_zone;			// 2:12 Timezone of DVR. Not used for now
	unsigned char	day_light_saving;	// 1:14 Daylight-saving flag. Not used now
	unsigned char	codec_type;			// 1:15 Codec type: CODEC_MPEG4_TYPE_A, CODEC_H264_TYPE_A, CODEC_H264_TYPE_B
	unsigned short	width;				// 2:16 Width of the frame
	unsigned short	height;				// 2:18 Height of the frame
	unsigned int	size;				// 4:20 Total size of the frame in byte
	unsigned int	event_flag;			// 4:24 Event flag of the frame, OR result of all available events
	unsigned char	title_len;			// 1:28 Length of title defined in config
	unsigned char	resv[3];			// 3:29
} P2_FRAME_HEADER, *PP2_FRAME_HEADER; // 32 byte


#define P2_AUDIO_TYPE_G723		0x10 // not support by MobileFocus(ffmpeg?)
#define P2_AUDIO_TYPE_ADPCM     0x20 // HiSi
#define P2_AUDIO_TYPE_G711		0x30 // Stretch
#define P3_AUDIO_TYPE_G723      0x01 //
#define P3_AUDIO_TYPE_ADPCM     0x02 // Hisi raw data
#define P3_AUDIO_TYPE_ADPCM2    0x03 // Hisi include header
#define P3_AUDIO_TYPE_G711      0x04 // sr:16k bps, fSize:2560
#define P3_AUDIO_TYPE_NVRG711   0x05 // sr:8k  bps, fSize:320

#define P2_AUDIO_CODEC(X)       (X & 0xF0)
#define P3_AUDIO_CODEC(X)       (X & 0x0F)

// Header of audio and meta data payload
typedef struct _P2_AM_HEADER
{
	unsigned char ch;	// 1:0 Channel index of audio/metadata
	unsigned char flag; // 1:1 Only used on audio now, indicating
	// audio type
} P2_AM_HEADER, *PP2_AM_HEADER; // 2 byte

// Header of HISI audio header  James add
typedef struct _HISI_AU_HEADER
{
    unsigned char reserver;
    unsigned char flag;
    unsigned char length;
    unsigned char sequence;
} HISI_AU_HEADER;

#define FH_ALARM    0x0001
#define FH_EVENT    0x0002
#define FH_MOTION   0x0004
#define FH_VLOSS    0x0008
#define FH_IFRAME   0x8000

#define FH_IS_ALARM(X)     (X & FH_ALARM)
#define FH_IS_EVENT(X)     (X & FH_EVENT)
#define FH_IS_MOTION(X)    (X & FH_MOTION)
#define FH_IS_VLOSS(X)     (X & FH_VLOSS)

// Header of meta
typedef struct _META_DATA_T
{
    unsigned int        marker;
    unsigned short      flag;
    unsigned short      valid;
    unsigned char       gpsInfo[32];
    unsigned char       gsensorInfo[8];
    unsigned char       zbInfo[32];
    unsigned char       fps[16];
    unsigned char       bus_id[16];
    unsigned int        vloss;
    unsigned int        alarm;
    unsigned char       resv[8];
} P2_META_DATA;

// Stretch header
typedef struct _STCH_HEADER_
{
    unsigned int	signature;			//[00-03]
    unsigned short	hdr_version;		//[04-05]/* Internal use field. */
    unsigned short	hdr_size;			//[06-07]
	
    unsigned char	board_id;			//[08-08]
    unsigned char	channel_type;		//[09-09]//sdvr_chan_type_e  channel_type;
    unsigned char	channel_id;			//[10-10]
    unsigned char	frame_type;			//[11-11]//sdvr_frame_type_e frame_type;
	
    unsigned char	motion_detected;	//[12-12]
    unsigned char	blind_detected;		//[13-13] 
    unsigned char	night_detected;		//[14-14]
    unsigned char	av_state_flags;		//[15-15]
	
	
    unsigned char   stream_id;			//[16-16]
    unsigned char   stream_count;		//[17-17]/* Internal use field. */
    unsigned short  reserved3;			//[18-19]
    unsigned int   payload_size;		//[20-23]//For chk sps, pps size
	
    unsigned int   timestamp;
    unsigned int   timestamp_high;
	
    // New fields. motion_value/blind_valueb/night_value[0] same as the compatible fields above
    unsigned char   motion_value[4];
    unsigned char   blind_value[4];
    unsigned char   night_value[4];
	
    unsigned short  active_width;
    unsigned short  padded_width;               /* Internal use field. */
    unsigned short  active_height;
    unsigned short  padded_height;              /* Internal use field. */
	
    unsigned int   seq_number;
    unsigned int   frame_number;
    unsigned int   frame_drop_count;
	
	
    unsigned char   reserved4[3];
    unsigned char   yuv_format;                 /* Internal use field. */
    unsigned int   y_data_size;                /* Internal use field. */
    unsigned int   u_data_size;                /* Internal use field. */
    unsigned int   v_data_size;                /* Internal use field. */
    unsigned int   y_data_offset;              /* Internal use field. */
    unsigned int   u_data_offset;              /* Internal use field. */
    unsigned int   v_data_offset;              /* Internal use field. */
	
    // These fields are for the U and V planes. Assume U and V are always
    // equal width and height
    unsigned short  uv_active_width;            /* Internal use field. */
    unsigned short  uv_padded_width;            /* Internal use field. */
    unsigned short  uv_active_height;           /* Internal use field. */
    unsigned short  uv_padded_height;           /* Internal use field. */
	
    // These fields are for host overlay/video data
    unsigned char   overlay_id;                  /* Internal use field. */
    unsigned char   reserved5[3];               /* Internal use field. */
    unsigned short  layer_width;                /* Internal use field. */
    unsigned short  layer_height;               /* Internal use field. */
    short			layer_top_left_x;           /* Internal use field. */ // int->short
    short			layer_top_left_y;           /* Internal use field. */ // int->short
	
    // These fields are used in SVC mode for packing packets
    unsigned int   num_packets;                /* Internal use field. */
    unsigned int   packet_size[7];             /* Internal use field. */
	
    unsigned int   reserved[20];
	
    // For debug/internal use only.
    unsigned int   internal_dbg[8];
	
    unsigned char   payload[0];     // This means that the payload starts here
} STCH_HEADER; //sdvr_av_buffer_t; //sizes 256 bytes

#pragma pack(pop)