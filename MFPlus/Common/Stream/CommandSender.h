//
//  CommandSender.h
//  EFViewerHD
//
//  Created by James Lee on 12/10/9.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Encrypt.h"

typedef void (^EFRequestHandler)(NSURLResponse *response, NSData *data, NSError *error);

@interface CommandSender : NSObject <NSXMLParserDelegate>
{
    NSString	*host;
	id			delegate;
    
    NSString	*currentXmlElement;
    NSString    *result;
}

- (id)initWithHost:(NSString *)theHost delegate:(id)theDelegate;
- (void)postCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withPath:(NSString *)path forOldDev:(BOOL)OldDev;//Nevio Special case for old device
- (void)getNevioData:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withPath:(NSString *)path;
- (void)postCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withuser:(NSString *)user withpwd:(NSString *)pwd; //XMS Login Command
- (void)postCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withPath:(NSString *)path withToken:(NSString *)token;//XMS post
- (void)getData:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withHeader:(NSString *)header;       //XMS
- (void)getData:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously random:(BOOL)random;       //P2 cgi
- (void)getData:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously random:(BOOL)random supportNCB:(BOOL)support;  //NCB cgi
- (void)putPSIACommand:(NSString *)cmd with:(NSString *)uri connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously;    //PSIA
- (void)postICCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously;                     //iC Post
- (void)getICData:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withUID:(NSString *)uid withAuth:(NSString *)auth; //iC Get
- (void)postCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously tokenInfo:(NSString *)Info deviceToken:(NSString *)devToken deviceName:(NSString *)devName; //APNs cgi
- (void)postCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withToken:(NSString *)token;//xFleet post
- (void)postCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withToken:(NSString *)token withStartTime:(NSInteger)stTime chIDAry:(NSArray *)chID;//xFleet post
- (void)postCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withToken:(NSString *)token withSequence:(NSInteger)seq count:(NSInteger)numberOfData eventType:(NSString *)vtype;//xms event query

- (void)post3MCommand:(NSString *)cmd dictionary:(NSDictionary *)cmdDict connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously;       //ValueIP 3M PTZ Command


//----------------------NSURLSession test----------------------
+ (instancetype)sharedManager;
- (void)addRequest:(NSString *)cmd withHandler:(EFRequestHandler)requestHandler;
//----------------------NSURLSession test----------------------


- (NSString *)getResult:(NSString *)xml;
- (BOOL)parseXmlResponse:(NSString *)xml;

@property (nonatomic,copy)	NSString		*host;
@property (assign)          id				delegate;

@end
