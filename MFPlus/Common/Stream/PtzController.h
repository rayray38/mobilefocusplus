//
//  PtzController.h
//  EFViewer
//
//  Created by James Lee on 2010/8/26.
//  Copyright 2010 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommandSender.h"

#define PTZ_FOCUS   0
#define PTZ_IRISON  1
#define PTZ_IRISOFF 2
#define PTZ_PRESET  3
#define PTZ_AUTOPAN 4
#define PTZ_PATTERN 5
#define PTZ_TOUR    6
#define PTZ_OSD     7
#define PTZ_UP      8
#define PTZ_RIGHT   9
#define PTZ_DOWN    10
#define PTZ_LEFT    11
#define PTZ_ZOOMIN  12
#define PTZ_ZOOMOUT 13
#define PTZ_FFAR    14
#define PTZ_FNEAR   15
#define PTZ_GO      16
#define PTZ_SET     17
#define PTZ_DELETE  18
#define PTZ_NONE    99

@interface PtzController : NSObject
{
	NSURLConnection *cmdConnection;
	CommandSender	*cmdSender;
}

// initiate
- (id)initWithHost:(NSString *)addr sid:(NSString *)sid toKen:(NSString *)_tokenKey;

// PTZ control (not really implement)
- (void)ptzTiltUp:(NSInteger)ch;
- (void)ptzTiltDown:(NSInteger)ch;
- (void)ptzPanLeft:(NSInteger)ch;
- (void)ptzPanRight:(NSInteger)ch;
- (void)ptzZoomIn:(NSInteger)ch;
- (void)ptzZoomOut:(NSInteger)ch;
- (void)ptzZoomStop:(NSInteger)ch;
- (void)ptzFocusFar:(NSInteger)ch;
- (void)ptzFocusNear:(NSInteger)ch;
- (void)ptzFocusStop:(NSInteger)ch;
- (void)ptzIrisOpen:(NSInteger)ch;
- (void)ptzIrisClose:(NSInteger)ch;
- (void)ptzStop:(NSInteger)ch;
- (void)ptzPresetGo:(NSInteger)ch value:(NSInteger)value;
- (void)ptzPresetSet:(NSInteger)ch value:(NSInteger)value;
- (void)ptzPresetCancel:(NSInteger)ch value:(NSInteger)value;
- (void)ptzAutoPanRun:(NSInteger)ch;
- (void)ptzAutoPan360Run:(NSInteger)ch;
- (void)ptzAutoPanStop:(NSInteger)ch;
- (void)ptzPattern:(NSInteger)ch value:(NSInteger)value;
- (void)ptzTour:(NSInteger)ch value:(NSInteger)value;
- (void)ptzOsdOpen:(NSInteger)ch;
- (void)ptzOsdExit:(NSInteger)ch;
// Nevio(EPN/EVS) only
- (void)ptzLeftUp:(NSInteger)ch;
- (void)ptzLeftDown:(NSInteger)ch;
- (void)ptzRightUp:(NSInteger)ch;
- (void)ptzRightDown:(NSInteger)ch;
- (void)ptzAutoTracking:(NSInteger)ch on:(BOOL)on;

// network
- (void)cancelConnection;


@property(nonatomic,retain) NSString *host;
@property(nonatomic,retain) NSString *sessionId;
@property(nonatomic,retain) NSString *ptzType;
@property(nonatomic,retain) NSString *tokenKey;
@property(nonatomic)        NSInteger speed;
@property(nonatomic)        NSInteger baudrate;
@property(nonatomic)        BOOL     blnNcb;
@property(nonatomic)        BOOL     blnDyna18X;

@end
