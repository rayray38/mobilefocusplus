//
//  RTSPClientSession.h
//  EFViewer
//
//  Created by James Lee on 2011/4/25.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>

#define _NEW_RTSP
#define MAX_BUF_SIZE 2*1024*1024

@protocol RTSPSubsessionDelegate;

@interface RTSPSubsession : NSObject {
	struct RTSPSubsessionContext *context;
	id <RTSPSubsessionDelegate> delegate;
}

- (NSString*)getSessionId;
- (NSString*)getMediumName;
- (NSString*)getProtocolName;
- (NSString*)getCodecName;
- (NSUInteger)getServerPortNum;
- (NSString*)getSDP_spropparametersets;
- (NSMutableDictionary*)getVPSandSPSandPPS;//20151223 added by Ray Lin, for h265 vps, sps and pps parser
- (NSString*)getSDP_config;
- (NSString*)getSDP_mode;
- (NSData *)getSpsData;
- (NSData *)getPpsData;
- (double)getNormalPlayTime:(struct timeval)presentationTime;
- (void)getSDP_VideoWidth:(NSInteger *)_width Height:(NSInteger *)_height;
- (NSUInteger)getClientPortNum;
- (NSUInteger)getChannels;
- (NSUInteger)getFrequency;
- (int)getSocket;
- (void)increaseReceiveBufferTo:(NSUInteger)size;
- (void)setPacketReorderingThresholdTime:(NSUInteger)uSeconds;
- (BOOL)timeIsSynchronized;

@property (nonatomic, assign) id <RTSPSubsessionDelegate> delegate;
@property (nonatomic)         NSInteger channel;

@end


@protocol RTSPSubsessionDelegate
- (void)didReceiveFrame:(const uint8_t*)frameData
		frameDataLength:(int)frameDataLength
	   presentationTime:(struct timeval)presentationTime
 durationInMicroseconds:(unsigned)duration
			 subsession:(RTSPSubsession*)subsession;
- (void)didReceiveMessage:(NSString *)message;
@end


@interface RTSPClientSession : NSObject {

	NSString *url;
	NSString *username;
	NSString *password;
}

@property(nonatomic)        struct RTSPClientSessionContext *context;
@property(nonatomic,retain) NSString *sdp;
@property(nonatomic,retain) NSMutableArray *subsessions;

- (id)initWithURL:(NSString*)url user:(NSString*)user passwd:(NSString*)passwd;
- (BOOL)setup;
- (NSArray*)getSubsessions;
- (BOOL)play;
- (BOOL)pause;
- (BOOL)resume;
- (BOOL)seekTime:(int)playtime;
- (BOOL)keepAlive;
- (BOOL)stop;
- (BOOL)teardown;
- (BOOL)runEventLoop:(char*)cancelSession;
- (void)runEventLoop;
- (NSString*)getLastErrorString;
- (NSString*)getSDP;
- (int)getSocket;
- (int)getPlayStartTime;
- (int)getPlayEndTime;

@end
