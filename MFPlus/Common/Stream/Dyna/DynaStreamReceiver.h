//
//  HDIPStreamReceiver.h
//  LiveStream
//
//  Created by James Lee on 2011/4/18.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StreamReceiver.h"
#import "RTSPClientSession.h"

#define D_RES_CIF     @"cif"
#define D_RES_D1      @"d1"
#define D_RES_VGA     @"vga"
#define D_RES_SVGA    @"svga"
#define D_RES_XGA     @"xga"
#define D_RES_720P    @"720p"
#define D_RES_1080P   @"1080p"

@interface DynaStreamReceiver : StreamReceiver <RTSPSubsessionDelegate>
{
	NSString *machineName;
	BOOL	 isEstablishTag;
	
	RTSPClientSession* rtspClientSession;
	NSArray* subsessions;
	char exit;
	
	int seq;
	uint8_t startBuf[4];
    
    VIDEO_PACKAGE spsPkg;
    VIDEO_PACKAGE ppsPkg;
    VIDEO_PACKAGE iframePkg;
    VIDEO_PACKAGE pframePkg;
    
    NSString *codec;    // get from quick stream
    BOOL blnIsDoubleIDR;   // for EAN2150
    NSMutableArray *targetLine;
    NSMutableArray *targetInfo;
    
    int audioCodecID;
	int audioSize;
    int audioBps;
    int audioBitRate;
    NSString *audioCodecStr;
    
    NSInteger iPTZCHMask ; // add by robert hsu 20110928 for keep enable PTZ channel mask
    NSInteger iCurrentStream;
    NSString  *strCurrentCMD;
    BOOL isCheckChannelInfo;    //add by robert hsu 20110928 for check current xml ack is channel info or not
    BOOL isCheckPTZInfo;        //add by robert hsu 20110928 for check current xml ack is PTZ support info or not
    BOOL blnNeedGetData;        //add by robert hsu 20110928 a flag to check need to get value from xml node or not
    BOOL blnIsFirstIframe;
    BOOL blnIsFirstPframe;
    NSInteger iReceiveIframe;
    NSInteger iReceivePframe;
}

-(BOOL) quickStream:(NSString *)data;
-(NSString *) getCodecFromString:(NSString *)data;

@end