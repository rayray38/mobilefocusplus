//
//  DynaPTZController.m
//  EFViewerHD
//
//  Created by James Lee on 13/8/20.
//  Copyright (c) 2013年 EF. All rights reserved.
/*
    purpose :
        1. process PSIA PTZ command 
        2. inheritance from NevioPtzController
        3. only override sendCommand function
*/
#import "DynaPTZController.h"

@implementation DynaPTZController

#pragma mark - CGI Command

- (void)ptzTiltDown:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/com/ptz.cgi?continuouspantiltmove=0,-%ld,0",(long)self.speed];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzTiltUp:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/com/ptz.cgi?continuouspantiltmove=0,%ld,0",(long)self.speed];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzPanLeft:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/com/ptz.cgi?continuouspantiltmove=-%ld,0,0",(long)self.speed];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzPanRight:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/com/ptz.cgi?continuouspantiltmove=%ld,0,0",(long)self.speed];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzZoomIn:(NSInteger)ch
{
    NSString *urlString = [NSString stringWithFormat:@"cgi-bin/ptz_HD.cgi?continuouszoommove=10"];
    if (self.blnDyna18X) {
        urlString = [NSString stringWithFormat:@"cgi-bin/ptz_18x.cgi?continuouszoommove=10"];
    }
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzZoomOut:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/ptz_HD.cgi?continuouszoommove=-10"];
    if (self.blnDyna18X) {
        urlString = [NSString stringWithFormat:@"cgi-bin/ptz_18x.cgi?continuouszoommove=-10"];
    }
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzZoomStop:(NSInteger)ch {
    
    NSString *urlString = [NSString stringWithFormat:@"cgi-bin/com/ptz.cgi?continuouszoommove=0"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}


- (void)ptzFocusFar:(NSInteger)ch
{
    NSString *urlString = [NSString stringWithFormat:@"cgi-bin/ptz_HD.cgi?continuousfocusmove=-10"];
    if (self.blnDyna18X) {
        urlString = [NSString stringWithFormat:@"cgi-bin/ptz_18x.cgi?continuousfocusmove=-10"];
    }
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzFocusNear:(NSInteger)ch
{
    NSString *urlString = [NSString stringWithFormat:@"cgi-bin/ptz_HD.cgi?continuousfocusmove=10"];
    if (self.blnDyna18X) {
        urlString = [NSString stringWithFormat:@"cgi-bin/ptz_18x.cgi?continuousfocusmove=10"];
    }
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzFocusStop:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/com/ptz.cgi?continuousfocusmove=0"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzIrisOpen:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/iris.cgi?value=0"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzIrisClose:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/iris.cgi?value=2"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzStop:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/com/ptz.cgi?continuouspantiltmove=0,0"];
    if (self.blnDyna18X) {
        urlString = [NSString stringWithFormat:@"cgi-bin/ptz_18x.cgi?continuouszoommove=0,0"];
    }
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzPresetGo:(NSInteger)ch value:(NSInteger)value
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/gopreset.cgi?num=%ld",(long)value];
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzPresetSet:(NSInteger)ch value:(NSInteger)value
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/setpre.cgi?index=%ld&name=noname",(long)value];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzPresetCancel:(NSInteger)ch value:(NSInteger)value
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/deletepreset.cgi?num=%ld",(long)value];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzAutoPanRun:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/com/ptz.cgi?gotoserverautopanno=1"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzAutoPan360Run:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"%@%d",
						   self.sessionId,1<<ch];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzAutoPanStop:(NSInteger)ch 
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/com/ptz.cgi?pantiltmove=0,0"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzPattern:(NSInteger)ch value:(NSInteger)value
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/com/ptz.cgi?gotoserversequenceno=1"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

- (void)ptzTour:(NSInteger)ch value:(NSInteger)value
{
	NSString *urlString = [NSString stringWithFormat:@"cgi-bin/com/ptz.cgi?gotoservercruiseno=1"];
	
	[self cancelConnection];
	[cmdSender getData:urlString connection:&cmdConnection synchronously:NO random:YES];
}

@end

