//
//  HDIPStreamReceiver.mm
//  LiveStream
//
//  Created by James Lee on 2011/4/18.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import "DynaStreamReceiver.h"
#import "HDIPAudioSender.h"
#import "DynaPTZController.h"

@implementation DynaStreamReceiver

#pragma mark - Streaming Control

- (id)initWithDevice:(Device *)device
{
    outputList = [[NSMutableArray alloc] init];
    NSString *nan = [NSString stringWithFormat:@"99"];
    for (NSInteger i=0; i<4; i++)
        [outputList addObject:nan];
    
    return [super initWithDevice:device];
}

- (BOOL)startLiveStreaming:(NSUInteger)channel {
	
	BOOL ret = YES;
	
	if ( ![super startLiveStreaming:channel] )
	{
		ret =  NO;
		goto Exit;
	}
	
	// set initial parameters
	seq = 0;
	startBuf[0] = 0x00;
	startBuf[1] = 0x00;
	startBuf[2] = 0x00;
	startBuf[3] = 0x01;
    
    blnIsDoubleIDR = NO;
    iReceiveIframe = 0;
    iReceivePframe = 0;
	
	// set available channels
	validChannel = 3; // 0011 // no use
    iMaxSupportChannel = 1;
	
	///// get info /////
	
	// request quick streaming
	NSString *cmd = [NSString stringWithFormat:@"cgi-bin/admin/param.cgi?action=list"];
	[cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
	// connection failed
	if ( errorDesc!=nil ) {
		
		ret =  NO;
		goto Exit;
	}
	
	if ( blnStopStreaming ) {
		
		goto Exit;
	}
	
	// parse response
	NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	//NSLog(@"response:%@",response);
    if ([response isEqualToString:@"Permission denied\n"]) {  // only Admin user can get quick stream

        self.errorDesc = NSLocalizedString(@"MsgAdminOnly", nil);
        
        ret = NO;
        goto Exit;
    
    }else {
        
        userLevel = 4;
    }
    
    ret = [self quickStream:response];
	[response release];
    
    if (!ret) {
        ret = NO;
        goto Exit;
    }
	
	///// RTSP /////
	
    // init RTSP client
    //modify by robert hsu 20120203 for add rtsp port 
	rtspClientSession = [[RTSPClientSession alloc] 
						 initWithURL:[NSString stringWithFormat:@"rtsp://%@:%ld/%@", [self getIp] ,(long)iRtspPort ,codec]
						 user:user passwd:passwd];
	
    //iChannel means steam id not channel id
    currentCHMask = channel;//get really current channel add by robert hsu 20110928
    
	// get SDP and create session
	if (![rtspClientSession setup])
	{
		self.errorDesc = NSLocalizedString(@"MsgRtspErr1", nil);
        
        [rtspClientSession release];
        rtspClientSession = nil;
        
		ret =  NO;
		goto Exit;
	}
	
	// get subsession
	subsessions = [[rtspClientSession getSubsessions] retain];
	if([subsessions count]==0)
	{
		self.errorDesc = NSLocalizedString(@"MsgRtspErr2", nil);
		ret =  NO;
		goto Exit;
	}
	
	// setup subsession
    BOOL videoAvailable = NO;
	for (int i=0; i<2; i++) {  //modify by James Lee 20120723 for Dyna IPCAM
#ifndef _NEW_RTSP
        if (![rtspClientSession setupSubsession:[subsessions objectAtIndex:i] useTCP:YES])//modify by robert hsu 2012.02.01 for use over tcp
		{
			self.errorDesc = NSLocalizedString(@"MsgRtspErr3", nil);
			ret =  NO;
			goto Exit;
		}
#endif
		RTSPSubsession* subsession = [subsessions objectAtIndex:i];
		NSLog(@"[DYNA] Subsession[%d] %@: MediumName=%@ CodecName=%@", 
			  i, [subsession getSessionId], [subsession getMediumName], [subsession getCodecName]);
        
        if ([[subsession getMediumName] isEqual:@"video"]) {
            videoAvailable = YES;
        }
        if ([[subsession getMediumName] isEqualToString:@"audio"]) {
            audioCodecStr = [subsession getCodecName];
        }
		
		[[subsessions objectAtIndex:i] setDelegate:(id)self];
	}
    
    if(!videoAvailable)
	{
		self.errorDesc = NSLocalizedString(@"MsgRtspErr4", nil);
		ret =  NO;
		goto Exit;
	}
	
	// play
	[rtspClientSession play];
	blnReceivingStream = YES;
	
    // initiate PTZ controller
	if (ptzController != nil) {
        
        [ptzController release];
        ptzController = nil;
    }
	ptzController = [[DynaPTZController alloc] initWithHost:host sid:nil toKen:nil];
    ptzController.speed = 5;
    if (DeviceType == EAN2218) {
        ptzController.blnDyna18X = YES;
    }
    
	// run loop
	exit = 0;
	[rtspClientSession runEventLoop:&exit];
	
Exit:
	blnReceivingStream = NO;
	
	return ret;
}

- (void)stopStreamingByView:(NSInteger)vIdx {
	
	exit = 1;
	
	[super stopStreamingByView:vIdx];
    
    if (rtspClientSession) {
        
        BOOL ret = [rtspClientSession teardown];
        if (!ret) {
            NSLog(@"[DYNA] teardown failed");
        }
    }
    
    blnConnectionClear = YES;
}

- (void)changeLiveChannel:(NSInteger)channel
{
    // no-op
}

- (void)openSound {
	
	// create audio decoder
	if ( audioControl!=nil ) {
		
        NSLog(@"[DYNA] Open sound");
        if ([audioCodecStr isEqualToString:@"PCMA"]) {
            
            audioCodecID = AV_CODEC_ID_PCM_ALAW;
            audioBps = 8;
        }else if ([audioCodecStr isEqualToString:@"PCMU"]) {
            
            audioCodecID = AV_CODEC_ID_PCM_MULAW;
            audioBps = 8;
        }else if ([audioCodecStr isEqualToString:@"G726-16"]) {
            
            audioCodecID = AV_CODEC_ID_ADPCM_G726;
            audioBps = 1;
            audioBitRate = 16000;
        }
		
        if (audioControl.codecId != audioCodecID) {
            
            [audioControl close];
            audioControl = [audioControl initWithCodecId:audioCodecID srate:8000 bps:audioBps balign:audioSize fsize:audioSize channel:1];
            audioControl.codecId = audioControl.amFlag = audioCodecID;
        }
		[audioControl start];
		
		NSLog(@"[DYNA] Create new audio decoder with codec id %d", audioCodecID);
	}
}

- (void)closeSound {
	
	NSLog(@"[DYNA] close sound");
	
	[audioControl stop];
    
    audioOn = NO;
}

- (void)dealloc
{	
    if (ppsPkg.data != nil) {
        free(ppsPkg.data);
    }
    if (spsPkg.data != nil) {
        free(spsPkg.data);
    }
    if (iframePkg.data != nil) {
        free(iframePkg.data);
    }
    if (pframePkg.data != nil) {
        free(pframePkg.data);
    }
    if (machineName != nil) {
        
        [machineName release];
        machineName = nil;
    }
    if (rtspClientSession != nil) {
        
        [rtspClientSession release];
        rtspClientSession = nil;
    }
    if (subsessions != nil) {
        
        [subsessions release];
        subsessions = nil;
    }
	
	[super dealloc];
}

#pragma mark - Parser Delegate

- (BOOL)quickStream:(NSString *)data
{
    
    //get codec
    NSString *tmpLine = [self getStringFromLine:data targetString:@"root.Image.I0.Appearance.Resolution" withQuote:NULL];
    codec = [self getCodecFromString:tmpLine];
    
    //get camera title
    machineName = [[self getStringFromLine:data targetString:@"root.Network.HostName" withQuote:NULL] copy];
    [videoControl setTitleByCH:[[outputList objectAtIndex:0] integerValue] title:machineName];
    
    //get Audio status
    tmpLine = [self getStringFromLine:data targetString:@"root.Properties.Audio.Audio" withQuote:NULL];
    
    if ([tmpLine isEqualToString:@"yes"]) {
        
        blnIsAudioEnable = YES;
    }else {
        
        blnIsAudioEnable = NO;
    }
    
    return YES;
}

- (NSString *)getCodecFromString:(NSString *)data
{
    NSString *cc = nil;
    NSString *lineString;
    NSArray *lines = [data componentsSeparatedByString:@","];
    
    NSInteger codeMask;
    
    if (DeviceType == EAN2150) {
        codeMask = 15;
        for (int i=0; i<4; i++) {
            lineString = [lines objectAtIndex:i];
            if ([lineString isEqualToString:@"disable"]) {
                codeMask = codeMask - (1<<i);
            }
        }
        NSLog(@"[DYNA] codeMask:%ld",(long)codeMask);
        
        switch (codeMask) {
            case 1:
                cc = @"jpeg";
                break;
            case 3:
                if (dualStream) {
                    cc = @"jpeg";
                }else {
                    cc = @"mpeg4";
                }
                break;
            case 4:
                cc = @"h264";
                break;
            case 5:
                if (dualStream) {
                    cc = @"jpeg";
                }else {
                    cc = @"h264";
                    lineString = [lines objectAtIndex:2];
                    if ([lineString isEqualToString:@"720p"]) {
                        blnIsDoubleIDR = YES;
                    }
                }
                break;
            case 12:
                if (dualStream) {
                    cc = @"h264_2";
                    lineString = [lines objectAtIndex:3];
                }else {
                    cc = @"h264";
                    lineString = [lines objectAtIndex:2];
                }
                if ([lineString isEqualToString:@"720p"]) {
                    blnIsDoubleIDR = YES;
                }
            default:
                cc = @"h264";
                break;
        }
    }else {
        codeMask = 7;
        for (int i=0; i<3; i++) {
            lineString = [lines objectAtIndex:i];
            if ([lineString isEqualToString:@"disable"]) {
                codeMask = codeMask - (1<<i);
            }
        }
        NSLog(@"[DYNA] codeMask:%ld",(long)codeMask);
        
        switch (codeMask) {
            case 1:
                cc = @"jpeg";
                break;
            case 2:
                cc = @"h264";
                break;
            case 3:
                if (dualStream) {
                    cc = @"jpeg";
                }else{
                    cc = @"h264";
                }
                break;
            case 6:
                if (dualStream) {
                    cc = @"h264_2";
                }else {
                    cc = @"h264";
                }
                break;
            default:
                cc = @"h264";
                break;
                
        }
    }
    NSLog(@"[DYNA] cc:%@",cc);
    
    return cc;
}

#pragma mark - Connection Delegate

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.
{
	if ( theConnection==requestConnection ) {
		
		[responseData appendData:data];
	}
}

#pragma mark - RTSP delegate

- (void)didReceiveMessage:(NSString *)message
{
}

- (void)didReceiveFrame:(const uint8_t*)frameData
		frameDataLength:(int)frameDataLength
	   presentationTime:(struct timeval)presentationTime
 durationInMicroseconds:(unsigned)duration
			 subsession:(RTSPSubsession*)subsession
{	
	// Video frame
	if ([[subsession getMediumName] isEqual:@"video"]) {
		
		// build video package
		
		VIDEO_PACKAGE videoPkg;
		
		videoPkg.sec	= presentationTime.tv_sec;
		videoPkg.ms		= presentationTime.tv_usec/1000;
		videoPkg.width	= 0; // don't care
		videoPkg.height	= 0; // don't care
        
       NSInteger iBufferIdx = [[outputList objectAtIndex:0] integerValue];
        
		if ([[subsession getCodecName] isEqual:@"H264"]) { // H264
			
			videoPkg.codec	= AV_CODEC_ID_H264;
			
			if ( (frameData[0] & 0X1F) == 0X7) { // SPS
                
                iReceiveIframe = 0;
                if (spsPkg.data != NULL) {
                    free(spsPkg.data);
                }
				spsPkg.size	= sizeof(startBuf)+frameDataLength;
				spsPkg.data	= malloc(spsPkg.size);
				memcpy((spsPkg.data), startBuf, sizeof(startBuf));
				memcpy((spsPkg.data+sizeof(startBuf)), frameData, frameDataLength);
#ifdef H264DEBUG
                NSLog(@"[264] Get SPS ---- frameData[0]:%x, size:%ld, startBuf:%lu, frameDataLength:%d",frameData[0],spsPkg.size,sizeof(startBuf),frameDataLength);
#endif
                blnIsFirstIframe = NO;
                return;
                
			}else if ((frameData[0] & 0X0F) == 0X8) { // PPS
                
                if (ppsPkg.data != NULL) {
                    free(ppsPkg.data);
                }
                ppsPkg.size	= sizeof(startBuf)+frameDataLength;
				ppsPkg.data	= malloc(ppsPkg.size);
				memcpy((ppsPkg.data), startBuf, sizeof(startBuf));
				memcpy((ppsPkg.data+sizeof(startBuf)), frameData, frameDataLength);
#ifdef H264DEBUG
                NSLog(@"[264] Get PPS ---- frameData[0]:%x, size:%ld, startBuf:%lu, frameDataLength:%d",frameData[0],ppsPkg.size,sizeof(startBuf),frameDataLength);
#endif
                return;
                
			}else if ((frameData[0] & 0X1F) == 0X5 ) { //I frame
                
                if ((DeviceType == EAN2150) && blnIsDoubleIDR) {
                    iReceiveIframe++;
                    if (iReceiveIframe == 2) {
                    
                    seq = 0;
                    videoPkg.seq	= seq;
                    videoPkg.type	= VO_TYPE_IFRAME;
                    videoPkg.size	= spsPkg.size + ppsPkg.size + iframePkg.size + sizeof(startBuf)+frameDataLength;
                    videoPkg.data	= malloc(videoPkg.size);
                    memcpy((videoPkg.data), spsPkg.data, spsPkg.size);
                    memcpy((videoPkg.data+spsPkg.size), ppsPkg.data, ppsPkg.size);
                    memcpy((videoPkg.data+spsPkg.size+ppsPkg.size), iframePkg.data, iframePkg.size);
                    memcpy((videoPkg.data+spsPkg.size+ppsPkg.size+iframePkg.size), startBuf, sizeof(startBuf));
                    memcpy((videoPkg.data+spsPkg.size+ppsPkg.size+iframePkg.size+sizeof(startBuf)), frameData, frameDataLength);
                    blnIsFirstIframe = YES;
#ifdef H264DEBUG
                    NSLog(@"[264] Get I frame ---- frameData[0]:%x, size:%ld, startBuf:%lu, frameDataLength:%d",frameData[0],videoPkg.size,sizeof(startBuf),frameDataLength);
#endif
                    }else {
                        if (iframePkg.data != NULL) {
                            free(iframePkg.data);
                        }
                        iframePkg.size = sizeof(startBuf)+frameDataLength;
                        iframePkg.data	= malloc(iframePkg.size);
                        memcpy((iframePkg.data), startBuf, sizeof(startBuf));
                        memcpy((iframePkg.data+sizeof(startBuf)), frameData, frameDataLength);
#ifdef H264DEBUG
                        NSLog(@"[264] Get I frame ---- frameData[0]:%x, size:%ld, startBuf:%lu, frameDataLength:%d",frameData[0],videoPkg.size,sizeof(startBuf),frameDataLength);
#endif
                        return;
                    }
                }else { //Single IDR
                    
                    seq = 0;
                    videoPkg.seq	= seq;
                    videoPkg.type	= VO_TYPE_IFRAME;
                    videoPkg.size	= spsPkg.size + ppsPkg.size + sizeof(startBuf)+frameDataLength;                         //Single
                    videoPkg.data	= malloc(videoPkg.size);
                    memcpy((videoPkg.data), spsPkg.data, spsPkg.size);
                    memcpy((videoPkg.data+spsPkg.size), ppsPkg.data, ppsPkg.size);
                    memcpy((videoPkg.data+spsPkg.size+ppsPkg.size), startBuf, sizeof(startBuf));                            //Single
                    memcpy((videoPkg.data+spsPkg.size+ppsPkg.size+sizeof(startBuf)), frameData, frameDataLength);           //Single
#ifdef H264DEBUG
                    NSLog(@"[264] Get I frame ---- frameData[0]:%x, size:%ld, startBuf:%lu, frameDataLength:%d",frameData[0],videoPkg.size,sizeof(startBuf),frameDataLength);
#endif
                }
                

                                
			}
			else if ( (frameData[0] & 0X1F) == 0X1) { // P frame
				
                if ((DeviceType == EAN2150) && blnIsDoubleIDR) {
                    iReceivePframe++;
                    
                    if (iReceivePframe == 2) {
                        videoPkg.seq  = seq;
                        videoPkg.type = VO_TYPE_PFRAME;
                        videoPkg.size = pframePkg.size + sizeof(startBuf) + frameDataLength; 
                        videoPkg.data = malloc(videoPkg.size);
                        memcpy(videoPkg.data, pframePkg.data, pframePkg.size);
                        memcpy(videoPkg.data+pframePkg.size, startBuf, sizeof(startBuf));
                        memcpy(videoPkg.data+pframePkg.size+sizeof(startBuf), frameData, frameDataLength);
                        
                        iReceivePframe = 0;
#ifdef H264DEBUG
                        NSLog(@"[264] Get P frame[2] ---- frameData[0]:%x, size:%ld frameSize:%d seq:%d",frameData[0],videoPkg.size,frameDataLength,seq);
#endif
                        
                    }else {
                        if (pframePkg.data != NULL) {
                            free(pframePkg.data);
                        }
                        seq++;
                        pframePkg.size = sizeof(startBuf)+frameDataLength;
                        pframePkg.data = malloc(pframePkg.size);
                        memcpy(pframePkg.data, startBuf, sizeof(startBuf));
                        memcpy(pframePkg.data+sizeof(startBuf), frameData, frameDataLength);
#ifdef H264DEBUG
                        NSLog(@"[264] Get P frame[1] ---- frameData[0]:%x, size:%ld seq:%d",frameData[0],pframePkg.size,seq);
#endif
                        return;
                    }
                
                }else { //Single IDR
                    seq++;
                    videoPkg.seq	= seq;
                    videoPkg.type	= VO_TYPE_PFRAME;
                    videoPkg.size	= sizeof(startBuf)+frameDataLength;
                    videoPkg.data	= malloc(videoPkg.size);
                    memcpy(videoPkg.data, startBuf, sizeof(startBuf));
                    memcpy((videoPkg.data+sizeof(startBuf)), frameData, frameDataLength);
#ifdef H264DEBUG
                    NSLog(@"[264] Get P frame ---- frameData[0]:%x, size:%ld seq:%d",frameData[0],videoPkg.size,seq);
#endif
                }
                
            }
			else {
				NSLog(@"[Dyna] Unknown %@ frame type", [subsession getCodecName]);
				return;
			}
		} else if ([[subsession getCodecName] isEqual:@"JPEG"]) { // JPEG
			
			videoPkg.codec	= AV_CODEC_ID_MJPEG;
			
			seq = 0;
			videoPkg.seq	= seq;
			videoPkg.type	= VO_TYPE_IFRAME;
			videoPkg.size	= frameDataLength;
			videoPkg.data	= malloc(videoPkg.size);
			memcpy(videoPkg.data, frameData, frameDataLength);
            
		} else if ([[subsession getCodecName] isEqual:@"MP4V-ES"]){ // MPEG4
                
            if ((frameData[4] & 0x40) == 0) {
                videoPkg.codec = AV_CODEC_ID_MPEG4;
                seq = 0;
                videoPkg.seq	= seq;
                videoPkg.type	= VO_TYPE_IFRAME;
                videoPkg.size	= frameDataLength;
                videoPkg.data	= malloc(videoPkg.size);
                memcpy(videoPkg.data, frameData, frameDataLength);
#ifdef H264DEBUG
                NSLog(@"[264] Get Mpeg I-Frame:%d",frameDataLength);
#endif
            }else if ((frameData[4] & 0x40) == 0x40) {
                seq++;
                videoPkg.codec = AV_CODEC_ID_MPEG4;
                videoPkg.seq	= seq;
                videoPkg.type	= VO_TYPE_PFRAME;
                videoPkg.size	= frameDataLength;
                videoPkg.data	= malloc(videoPkg.size);
                memcpy(videoPkg.data, frameData, frameDataLength);
#ifdef H264DEBUG
                NSLog(@"[264] Get Mpeg P-Frame:%d",frameDataLength);
#endif
            }else {
                NSLog(@"[Dyna] Unknown %@ frame type", [subsession getCodecName]);
				return;
            }
            
        } else { 
			
			NSLog(@"[Dyna] Unknown codec type: %@", [subsession getCodecName]);
			return;
		}
		// put package to VideoOutput buffer
        [videoControl putFrameIntoBufferByCH:&videoPkg chIndex:iBufferIdx];
		
		// update frame size (get from decoder)
		frameWidth	= [videoControl getVideoWidthbyCH:0];
		frameHeight = [videoControl getVideoHeightbyCH:0];
	}
	
	// Audio frame
	else if ([[subsession getMediumName] isEqual:@"audio"]) {
	
		if (audioControl!=nil && audioOn) {
			[audioControl playAudio:(Byte *)frameData length:frameDataLength];
		}
		else {
			//NSLog(@"[Dyna] Receive audio (OFF)");
		}

	}
	
	else {
		NSLog(@"[Dyna] Unknown medium name: %@", [subsession getMediumName]);
	}
}
@end
