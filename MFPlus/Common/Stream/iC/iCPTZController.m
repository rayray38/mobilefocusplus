//
//  iCPTZController.m
//  EFViewerPlus
//
//  Created by James Lee on 2014/7/26.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import "iCPTZController.h"

@interface iCPTZController()

- (NSString *)getCommandBy:(NSString *)Mode withID:(NSInteger)_ID Value:(NSInteger)_Val;

@end

@implementation iCPTZController

#pragma mark -
#pragma mark Initialization Command

- (id)initWithHost:(NSString *)addr sid:(NSString *)sid toKen:(NSString *)_tokenKey
{
	if (self = [super initWithHost:addr sid:sid toKen:nil]) {
        
        ptzCmdDict = @{@"PelcoP" : @{
                               @"Stop":@"A0:[ID]:00:00:00:00:AF:[CS]",
                               @"Up":@"A0:[ID]:00:08:00:1C:AF:[CS]",
                               @"Down":@"A0:[ID]:00:10:00:1E:AF:[CS]",
                               @"Left":@"A0:[ID]:00:04:21:00:AF:[CS]",
                               @"Right":@"A0:[ID]:00:02:1E:00:AF:[CS]",
                               @"UpLeft":@"A0:[ID]:00:0C:21:22:AF:[CS]",
                               @"UpRight":@"A0:[ID]:00:0A:17:18:AF:[CS]",
                               @"DownLeft":@"A0:[ID]:00:14:12:1C:AF:[CS]",
                               @"DownRight":@"A0:[ID]:00:12:22:29:AF:[CS]",
                               @"ZoomIn":@"A0:[ID]:00:20:00:00:AF:[CS]",
                               @"ZoomOut":@"A0:[ID]:00:40:00:00:AF:[CS]",
                               @"FocusIn":@"A0:[ID]:01:00:00:00:AF:[CS]",
                               @"FocusOut":@"A0:[ID]:02:00:00:00:AF:[CS]",
                               @"IrisOpen":@"A0:[ID]:04:00:00:00:AF:[CS]",
                               @"IrisClose":@"A0:[ID]:08:00:00:00:AF:[CS]",
                               @"Preset":@"A0:[ID]:00:03:00:[VAL]:AF:[CS]",
                               @"GoPreset":@"A0:[ID]:00:07:00:[VAL]:AF:[CS]",
                               @"AutoPan":@"A0:[ID]:20:00:00:00:AF:[CS]",
                               @"AutoPanStop":@"A0:[ID]:00:07:00:60:AF:[CS]"
                               },
                       @"PelcoD" : @{
                               @"Stop":@"FF:[ID]:00:00:00:00:[CS]",
                               @"Up":@"FF:[ID]:00:08:00:1C:[CS]",
                               @"Down":@"FF:[ID]:00:10:00:1E:[CS]",
                               @"Left":@"FF:[ID]:00:04:21:00:[CS]",
                               @"Right":@"FF:[ID]:00:02:1E:00:[CS]",
                               @"UpLeft":@"FF:[ID]:00:0C:21:22:[CS]",
                               @"UpRight":@"FF:[ID]:00:0A:17:18:[CS]",
                               @"DownLeft":@"FF:[ID]:00:14:12:1C:[CS]",
                               @"DownRight":@"FF:[ID]:00:12:22:29:[CS]",
                               @"ZoomIn":@"FF:[ID]:00:20:00:00:[CS]",
                               @"ZoomOut":@"FF:[ID]:00:40:00:00:[CS]",
                               @"FocusIn":@"FF:[ID]:00:80:00:00:[CS]",
                               @"FocusOut":@"FF:[ID]:01:00:00:00:[CS]",
                               @"IrisOpen":@"FF:[ID]:02:00:00:00:[CS]",
                               @"IrisClose":@"FF:[ID]:04:00:00:00:[CS]",
                               @"Preset":@"FF:[ID]:00:03:00:[VAL]:[CS]",
                               @"GoPreset":@"FF:[ID]:00:07:00:[VAL]:[CS]",
                               @"AutoPan":@"FF:[ID]:00:11:00:[VAL]:[CS]",
                               @"AutoPanStop":@"FF:[ID]:00:13:00:[VAL]:[CS]"
                               },
                       @"Samsung" : @{
                               @"Stop":@"A0:[ID]:00:00:00:00:00:00:FF:AF:[CS]",
                               @"Up":@"A0:[ID]:00:00:08:00:1C:00:FF:AF:[CS]",
                               @"Down":@"A0:[ID]:00:00:10:00:1E:00:FF:AF:[CS]",
                               @"Left":@"A0:[ID]:00:00:04:21:00:00:FF:AF:[CS]",
                               @"Right":@"A0:[ID]:00:00:02:1E:00:00:FF:AF:[CS]",
                               @"UpLeft":@"A0:[ID]:00:00:0C:21:22:FF:FF:AF:[CS]",
                               @"UpRight":@"A0:[ID]:00:00:0A:17:18:FF:FF:AF:[CS]",
                               @"DownLeft":@"A0:[ID]:00:00:14:12:1C:FF:FF:AF:[CS]",
                               @"DownRight":@"A0:[ID]:00:00:12:22:29:FF:FF:AF:[CS]",
                               @"ZoomIn":@"A0:[ID]:00:00:20:00:00:[VAL]:FF:AF:[CS]",
                               @"ZoomOut":@"A0:[ID]:00:00:40:00:00:[VAL]:FF:AF:[CS]",
                               @"FocusIn":@"A0:[ID]:00:02:00:00:00:[VAL]:FF:AF:[CS]",
                               @"FocusOut":@"A0:[ID]:00:01:00:00:00:[VAL]:FF:AF:[CS]",
                               @"IrisOpen":@"A0:[ID]:00:10:00:00:00:00::FF:AF:[CS]",
                               @"IrisClose":@"A0:[ID]:00:08:00:00:00:00:FF:AF:[CS]",
                               @"Preset":@"A0:[ID]:00:00:03:[VAL]:FF:FF:FF:AF:[CS]",
                               @"GoPreset":@"A0:[ID]:00:00:07:[VAL]:FF:FF:FF:AF:[CS]",
                               @"AutoPan":@"A0:[ID]:00:00:00:00:00:00:FF:AF:[CS]",  //not support
                               @"AutoPanStop":@"A0:[ID]:00:00:00:00:00:00:FF:AF:[CS]",  //not support
                               },
                       @"SamsungII" : @{
                               @"Stop":@"A0:00:[ID]:01:00:00:00:00:[CS]",
                               @"Up":@"A0:00:[ID]:01:00:04:00:1C:[CS]",
                               @"Down":@"A0:00:[ID]:01:00:08:00:1E:[CS]",
                               @"Left":@"A0:00:[ID]:01:00:01:21:00:[CS]",
                               @"Right":@"A0:00:[ID]:01:00:02:1E:00:[CS]",
                               @"UpLeft":@"A0:00:[ID]:01:00:05:21:22:[CS]",
                               @"UpRight":@"A0:00:[ID]:01:00:06:17:18:[CS]",
                               @"DownLeft":@"A0:00:[ID]:01:00:09:12:1C:[CS]",
                               @"DownRight":@"A0:00:[ID]:01:00:0A:22:29:[CS]",
                               @"ZoomIn":@"A0:00:[ID]:01:20:00:00:00:[CS]",
                               @"ZoomOut":@"A0:00:[ID]:01:40:00:00:00:[CS]",
                               @"FocusIn":@"A0:00:[ID]:01:02:00:00:00:[CS]",
                               @"FocusOut":@"A0:00:[ID]:01:01:00:00:00:[CS]",
                               @"IrisOpen":@"A0:00:[ID]:01:08:00:00:00:[CS]",
                               @"IrisClose":@"A0:00:[ID]:01:10:00:00:00:[CS]",
                               @"Preset":@"A0:00:[ID]:03:50:[VAL]:FF:FF:[CS]",
                               @"GoPreset":@"A0:00:[ID]:03:19:[VAL]:FF:FF:[CS]",
                               @"AutoPan":@"A0:00:[ID]:03:1A:01:01:FF:[CS]",
                               @"AutoPanStop":@"A0:00:[ID]:03:1A:01:00:FF:[CS]",
                               },
                       };
    }
    
    return self;
}

#pragma mark - Command convert

- (NSString *)getCommandBy:(NSString *)Mode withID:(NSInteger)_ID Value:(NSInteger)_Val
{
    NSString *cmd = [[ptzCmdDict objectForKey:self.ptzType] objectForKey:Mode];
    cmd = [cmd stringByReplacingOccurrencesOfString:@"[ID]" withString:[NSString stringWithFormat:@"%02lX",(long)_ID]];
    cmd = [cmd stringByReplacingOccurrencesOfString:@"[VAL]" withString:[NSString stringWithFormat:@"%02lX",(long)_Val]];
    NSArray *cmdArray = [cmd componentsSeparatedByString:@":"];
    NSInteger checkSum = 0;
    
    //NSLog(@"[iC] Type:%@, Mode:%@",self.ptzType,Mode);
    if ([self.ptzType isEqualToString:@"PelcoP"]) {
        
        for (NSInteger n=0; n<7; n++) {
            NSInteger hex = strtoul([[cmdArray objectAtIndex:n] UTF8String],0,16);
            checkSum ^= hex;
        }
        cmd = [cmd stringByReplacingOccurrencesOfString:@"[CS]" withString:[NSString stringWithFormat:@"%02lX",(long)checkSum]];
    } else if ([self.ptzType isEqualToString:@"PelcoD"]) {
        
        for (NSInteger n=1; n<6; n++) {
            NSInteger hex = strtoul([[cmdArray objectAtIndex:n] UTF8String], 0, 16);
            checkSum += hex;
        }
        cmd = [cmd stringByReplacingOccurrencesOfString:@"[CS]" withString:[NSString stringWithFormat:@"%02lX",(long)checkSum]];
    } else if ([self.ptzType isEqualToString:@"Samsung"]) {
        
        for (NSInteger n=1; n<9; n++) {
            NSInteger hex = strtoul([[cmdArray objectAtIndex:n] UTF8String], 0, 16);
            checkSum += hex;
        }
    } else if ([self.ptzType isEqualToString:@"SamsungII"]) {
        
        for (NSInteger n=1; n<8; n++) {
            NSInteger hex = strtoul([[cmdArray objectAtIndex:n] UTF8String], 0, 16);
            checkSum += hex;
        }
    }
    
    cmd = [cmd stringByReplacingOccurrencesOfString:@":" withString:@""];
    return cmd;
}

#pragma mark -
#pragma mark CGI Command

- (void)ptzTiltDown:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"Down" withID:ch Value:0],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzTiltUp:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"Up" withID:ch Value:0],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzPanLeft:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"Left" withID:ch Value:0],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzPanRight:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"Right" withID:ch Value:0],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzLeftUp:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"UpLeft" withID:ch Value:0],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzLeftDown:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"DownLeft" withID:ch Value:0],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzRightUp:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"UpRight" withID:ch Value:0],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzRightDown:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"DownRight" withID:ch Value:0],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzZoomIn:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"ZoomIn" withID:ch Value:3],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzZoomOut:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"ZoomOut" withID:ch Value:3],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzFocusFar:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"FocusOut" withID:ch Value:3],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzFocusNear:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"FocusIn" withID:ch Value:3],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzIrisOpen:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"IrisOpen" withID:ch Value:0],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzIrisClose:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"IrisClose" withID:ch Value:0],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzPresetSet:(NSInteger)ch value:(NSInteger)value
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"Preset" withID:ch Value:value],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzPresetGo:(NSInteger)ch value:(NSInteger)value
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"GoPreset" withID:ch Value:value],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzAutoPanRun:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"AutoPan" withID:ch Value:0],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzAutoPanStop:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"AutoPanStop" withID:ch Value:0],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

- (void)ptzStop:(NSInteger)ch
{
	NSString *urlString = [NSString stringWithFormat:@"<PTZ Baudrate=\"%ld\" Code=\"%@\" />"
                           ,(long)self.baudrate,[self getCommandBy:@"Stop" withID:ch Value:0],nil];
	
	[self cancelConnection];
	[cmdSender postICCommand:urlString connection:&cmdConnection synchronously:NO];
}

@end
