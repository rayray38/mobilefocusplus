//
//  iCStreamReceiver.m
//  EFViewerPlus
//
//  Created by James Lee on 2014/6/13.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import "iCStreamReceiver.h"
#import "iCPTZController.h"

@interface iCStreamReceiver()

@end

@implementation iCStreamReceiver

- (id)initWithDevice:(Device *)device
{
    return [super initWithDevice:device];
}

#pragma mark - Streaming Control

- (BOOL)startLiveStreaming:(NSUInteger)mask {
	
	BOOL ret = YES;
	NSString *cmd;
    NSInteger iErrorCode = 0;
    blnStopStreaming = NO;
    blnVideoAvailable = NO;
    
	if ( ![super startLiveStreaming:mask] )
	{
		ret =  NO;
		goto Exit;
    }
    
	// reset parameters
	audioOn = NO;
    memset(sequence, 0, sizeof(sequence));
    
    //generate session ID
    if (sessionId) {
        [sessionId release];
        sessionId = nil;
    }
    sessionId = [[[NSUUID UUID] UUIDString] retain];
    
    // initiate PTZ controller
    if (ptzController != nil) {
        [ptzController release];
        ptzController = nil;
    }
    ptzController = [[iCPTZController alloc] initWithHost:host sid:0 toKen:nil];
    
    //special setting
    if (audioControl != nil)
        audioControl.amFlag = IC_AUDIO_TYPE_NONE;
    
    // special issue - to avoid config and stream use same socket
    blnGetPTZInfo = YES;
    cmd = [NSString stringWithFormat:@"<GetConfiguration File=\"extended.xml\" />"];
    [cmdSender postICCommand:cmd connection:&requestConnection synchronously:NO];
    
    //20160121 added by Ray Lin, avoid urlconnection forget to add authentication in header
    NSString *authValue = [self addAuthorizationInHeaderWithBasic];
    
	// start live streaming
	cmd = [NSString stringWithFormat:@"net_video.cgi?hq=%ld&iframe=%lu&pframe=%lu&audio=0",(long)1-dualStream,(unsigned long)mask,(unsigned long)mask];
	[cmdSender getICData:cmd connection:&streamConnection synchronously:YES withUID:sessionId withAuth:authValue];
	
	if ( errorDesc!=nil ) {
		iErrorCode = -4;
		ret = NO;
	}
	
Exit:
	self.blnReceivingStream = NO;
    [self stopStreaming];
    ret = NO;
	
	return ret;
}

- (BOOL)startPlaybackStreaming:(NSUInteger)mask
{
	BOOL ret = YES;
    NSString *cmd;
    NSInteger iErrorCode = 0;
    blnStopStreaming = NO;
    blnVideoAvailable = NO;
    
    if (![super startPlaybackStreaming:mask]) {
        ret = NO;
        goto Exit;
    }
    
    // reset parameters
    packetRemanent = 0;
    packetHeaderOffset = 0;
    stchHeaderOffset = 0;
    frameHeaderOffset = 0;
    frameOffset = 0;
    frameSize = 0;
    audioOn = NO;
    m_SpeedIndex = 1;
    
    //generate session ID
    if (sessionId) {
        [sessionId release];
        sessionId = nil;
    }
    sessionId = [[[NSUUID UUID] UUIDString] retain];
    
    //special setting
    if (audioControl != nil)
        audioControl.amFlag = IC_AUDIO_TYPE_NONE;
    
    // special issue - to avoid config and stream use same socket
    cmd = [NSString stringWithFormat:@"<GetConfiguration File=\"extended.xml\" />"];
    [cmdSender postICCommand:cmd connection:&requestConnection synchronously:NO];
    
    //20160121 added by Ray Lin, avoid urlconnection forget to add authentication in header
    NSString *authValue = [self addAuthorizationInHeaderWithBasic];
    
	// start live streaming
	cmd = [NSString stringWithFormat:@"net_video.cgi?hq=%ld&iframe=%lu&pframe=%lu&audio=0&beg=%lu&end=%d",(long)1-dualStream,(unsigned long)mask,(unsigned long)mask,(unsigned long)m_intPlaybackStartTime,-1];
	[cmdSender getICData:cmd connection:&streamConnection synchronously:YES withUID:sessionId withAuth:authValue];
	
	if ( errorDesc!=nil ) {
		iErrorCode = -4;
		ret = NO;
	}
    
Exit:
    self.blnReceivingStream = NO;
    [self stopStreaming];
    
    return ret;
}

- (void)stopStreaming
{
    if (audioOn) {
        [self closeSound];
    }
    
    [super stopStreaming];
}

- (void)changeVideoMask:(NSUInteger)mask
{
    NSString* cmd;
	
	// change video mask
    if(sessionId)
    {
        NSInteger errCount = 0;
        while (!blnReceivingStream && errCount<100)
        {
            [NSThread sleepForTimeInterval:0.05f];
            errCount++;
        }
        
        //NSLog(@"[SR] sessionID = %@" ,sessionId);
        currentCHMask = mask;
        cmd = [NSString stringWithFormat:@"<SendRemote Magic=\"%@\" iframe=\"%lu\" pframe=\"%lu\" audio=\"%d\" complete=\"False\" />",sessionId,(unsigned long)mask,(unsigned long)mask, 0];
        [cmdSender postICCommand:cmd connection:&requestConnection synchronously:NO];
        
        if (audioOn)
            [self changeAudioChannel];
        
    }
}

- (void)changeAudioChannel
{
    if (sessionId && audioOn) {
        NSString *cmd = [NSString stringWithFormat:@"<SendRemote Magic=\"%@\" iframe=\"%lu\" pframe=\"%lu\" audio=\"%lu\" complete=\"False\" />",sessionId,(unsigned long)currentCHMask,(unsigned long)currentCHMask,(unsigned long)currentPlayCH];
        [cmdSender postICCommand:cmd connection:&requestConnection synchronously:NO];
    }
}

- (void)openSound
{
    audioOn = YES;
    NSString* cmd = [NSString stringWithFormat:@"<SendRemote Magic=\"%@\" iframe=\"%lu\" pframe=\"%lu\" audio=\"%lu\" complete=\"False\" />",sessionId,(unsigned long)currentCHMask,(unsigned long)currentCHMask,(unsigned long)currentPlayCH];
    [cmdSender postICCommand:cmd connection:&requestConnection synchronously:NO];

    if (audioControl)
        [audioControl start];
}

- (void)closeSound
{
    audioOn = NO;
    NSString* cmd = [NSString stringWithFormat:@"<SendRemote Magic=\"%@\" iframe=\"%lu\" pframe=\"%lu\" audio=\"%d\" complete=\"False\" />",sessionId,(unsigned long)currentCHMask,(unsigned long)currentCHMask,0];
    [cmdSender postICCommand:cmd connection:&requestConnection synchronously:NO];

    if (audioControl)
        [audioControl stop];
}

- (void)changePlaybackMode:(NSInteger)mode withValue:(NSInteger)value
{
    NSString *cmd;
    
    switch (mode) {
        case PLAYBACK_FORWARD:
            cmd = [NSString stringWithFormat:@"<SendRemote Magic=\"%@\" speed=\"1024\" />",sessionId];
            [cmdSender postICCommand:cmd connection:&requestConnection synchronously:NO];
            break;
            
        case PLAYBACK_FAST_FORWARD :
            cmd = [NSString stringWithFormat:@"<SendRemote Magic=\"%@\" speed=\"%ld\" />",sessionId,(long)(value<<10)];
            [cmdSender postICCommand:cmd connection:&requestConnection synchronously:NO];
            break;
            
        case PLAYBACK_FAST_BACKWARD :
            cmd = [NSString stringWithFormat:@"<SendRemote Magic=\"%@\" speed=\"-%ld\" />",sessionId,(long)(value<<10)];
            [cmdSender postICCommand:cmd connection:&requestConnection synchronously:NO];
            break;
            
        case PLAYBACK_PAUSE:
            cmd = [NSString stringWithFormat:@"<SendRemote Magic=\"%@\" pause=\"True\" />",sessionId];
            [cmdSender postICCommand:cmd connection:&requestConnection synchronously:NO];
            break;
    }
    
    if (self.errorDesc) {
        NSLog(@"[SV] Playback Error(%ld):%@",(long)mode,self.errorDesc);
    }
}

#pragma mark - Get Information

- (BOOL)getDeviceInfo
{
    BOOL ret = YES;
    NSString *cmd;
    NSString *response;
    NSInteger iErrorCode = 0;
    
    // init output list to match channel and layout window
    if (!outputList) {
        outputList = [[NSMutableArray alloc] init];
        
        NSString *nan = [NSString stringWithFormat:@"99"];
        for (NSInteger i=0; i<MAX_CH_NUMBER; i++) {
            [outputList addObject:nan];
        }
    }
    
    if (![super getDeviceInfo]) { //get DDNS if need
        ret =  NO;
		goto Exit;
    }
    
    // get camera list
    if (!cameraList) {
        cameraList = [[NSMutableArray alloc] init];
    }
    
    blnGetChannelInfo = YES;
    
    cmd = [NSString stringWithFormat:@"<GetConfiguration File=\"channels.xml\" />"];
	[cmdSender postICCommand:cmd connection:&requestConnection synchronously:YES];
    
    // connection failed
	if (errorDesc!=nil ) {
		
        iErrorCode = -2;
        ret = NO;
        goto Exit;
    }
	
	if (blnStopStreaming )
        goto Exit;
    
    response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //special case for iCatch
    NSRange rStart = [response rangeOfString:@"<Channels>"];
    NSRange rEnd = [response rangeOfString:@"</Channels>"];
    if (rStart.location != NSNotFound && rEnd.location != NSNotFound) {
        NSString *channelXML = [response substringWithRange:NSMakeRange(rStart.location, rEnd.location-rStart.location)];
        //NSLog(@"[iC] response:%@",response);
        [cmdSender parseXmlResponse:channelXML];
    }
    [response release];
    blnGetChannelInfo = NO;
    
    // get HDD info
    blnGetDiskInfo = YES;
    cmd = [NSString stringWithFormat:@"<GetRecordList Type=\"Min\" Begin=\"0\" End=\"%d\" />"
           ,(int)[[NSDate date] timeIntervalSince1970]+604800]; //往後算一週
    [cmdSender postICCommand:cmd connection:&requestConnection synchronously:YES];
    // connection failed
	if (errorDesc!=nil ) {
		
        iErrorCode = -2;
        ret = NO;
        goto Exit;
	}
	
	if (blnStopStreaming )
        goto Exit;
    
    response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //NSLog(@"[iC] response:%@",response);
    [cmdSender parseXmlResponse:response];
    [response release];
    blnGetDiskInfo = NO;
    
    blnGetTZInfo = YES;
    cmd = [NSString stringWithFormat:@"<GetConfiguration File=\"system.xml\" />"]; //往後算一週
    [cmdSender postICCommand:cmd connection:&requestConnection synchronously:YES];
    // connection failed
	if (errorDesc!=nil ) {
		
        iErrorCode = -2;
        ret = NO;
        goto Exit;
	}
	
	if (blnStopStreaming )
        goto Exit;
    
    response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //special case for iCatch
    rStart = [response rangeOfString:@"<System Name=\""];
    rEnd = [response rangeOfString:@"</System>"];
    if (rStart.location != NSNotFound && rEnd.location != NSNotFound) {
        NSString *systemXML = [response substringWithRange:NSMakeRange(rStart.location, rEnd.location-rStart.location)];
        //NSLog(@"[iC] response:%@",systemXML);
        [cmdSender parseXmlResponse:systemXML];
    }
    [response release];
    blnGetTZInfo = NO;
    
    self.blnReceivingStream = NO;
    
    return YES;
    
Exit:
    
	self.blnReceivingStream = NO;
    [self stopStreaming];
	
	return ret;
}

- (NSString *)addAuthorizationInHeaderWithBasic
{
    NSString *authString = nil;
    
    //20160121 added by Ray Lin, avoid urlconnection forget to add authentication in header
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", [self user], [self passwd]];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    authString = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:0]];
    
    return authString;
}

#pragma mark - XML Parser Delegate

// Start of element
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    [currentXmlElement release];
	currentXmlElement = [elementName copy];
    
    if ([currentXmlElement isEqualToString:@"Channel"] && blnGetChannelInfo) {
        
        CameraInfo *emptyEntry = [[[CameraInfo alloc] init] autorelease];
        emptyEntry.index = [[attributeDict objectForKey:@"ID"] integerValue] + 1;
        emptyEntry.title = [[Decryptor base64:[attributeDict objectForKey:@"Title"] with:NSUTF16LittleEndianStringEncoding] copy];
        emptyEntry.install = 1;
        emptyEntry.covert = 0;
        validChannel |= 0x1<<(emptyEntry.index-1);
        iMaxSupportChannel = emptyEntry.index;
        [cameraList addObject:emptyEntry];
    } else if ([currentXmlElement isEqualToString:@"Item"] && blnGetDiskInfo) {
        
        m_intDiskStartTime = [[attributeDict objectForKey:@"Begin"] integerValue];
        m_intDiskEndTime = [[attributeDict objectForKey:@"End"] integerValue];
    } else if ([currentXmlElement isEqualToString:@"System"] && blnGetTZInfo) {
       
        NSString *tz = [attributeDict objectForKey:@"Timezone"];
        m_DiskGMT = [[tz stringByReplacingOccurrencesOfString:@"TZ=" withString:@""] copy];
    } else if (blnGetPTZInfo) {
        
        if ([currentXmlElement isEqualToString:@"Channel"]) {
            currentIndex = [[attributeDict objectForKey:@"ID"] integerValue];
        } else if ([currentXmlElement isEqualToString:@"PTZ"] && currentIndex < MAX_IC_CHANNEL) {
            
            CameraInfo *curInfo = [cameraList objectAtIndex:currentIndex];
            curInfo.ptzType = [[attributeDict objectForKey:@"Protocol"] copy];
            curInfo.baudrate = [[attributeDict objectForKey:@"Baudrate"] integerValue];
            curInfo.ptzID = [[attributeDict objectForKey:@"ID"] integerValue];
            //NSLog(@"[iC] Cam%02d Type:%@, BR:%d, ID:%d",currentIndex, curInfo.ptzType, curInfo.baudrate, curInfo.ptzID);
        }
    }
}

// Found Character
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSMutableString *)string
{
}

// End Element
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
{
    [currentXmlElement release];
	currentXmlElement = [elementName copy];
}

#pragma mark - Connection Delegate
- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.
// it's a kind of callback function
//when call CommandSender.getData will set self to be delegate
//then in CommandSender.getData will set to become NSURLConnection's delegate
//after NSURLConnection receive data will trigger this method
{
    //NSLog(@"Received: %d", [data length]);
	
	if ( theConnection==requestConnection ) {

		[responseData appendData:data];
        
        if (blnGetPTZInfo) {
            
            NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            if ([response rangeOfString:@"</GetConfigurationResponse>"].location != NSNotFound) {
                //NSLog(@"[iC] %@",response);
                //special case for iCatch
                NSRange rStart = [response rangeOfString:@"<Channels>"];
                NSRange rEnd = [response rangeOfString:@"</Channels>"];
                if (rStart.location != NSNotFound && rEnd.location != NSNotFound) {
                    NSString *ptzXML = [response substringWithRange:NSMakeRange(rStart.location, rEnd.location-rStart.location)];
                    [cmdSender parseXmlResponse:ptzXML];
                }
                blnGetPTZInfo = NO;
            }
            [response release];
        }
	}
	else if ( theConnection==streamConnection ) {
		
		self.blnReceivingStream = YES;
        
		if ( blnStopStreaming ) return;
		
		int readLen = 0;
		int remanentLen = [data length];
        
        // check if NO stream
		if ( remanentLen<100) {
        }
        
        while ( remanentLen>0 ) {
            
            // check whether packet receive finished
			if ( packetHeaderOffset==sizeof(IC_PACKET_HEADER) && packetRemanent==0 ) {
				
                //NSLog(@"[iC] Reset packet parameter");
				// reset packet header data length
                memset(&packetHeader, 0, sizeof(IC_PACKET_HEADER));
                memset(&pFrameBuf[0],0,MAX_FRAME_SIZE); // clean buffer
				packetHeaderOffset = 0;
			}
            
            // read packet header
			if ( packetHeaderOffset<sizeof(IC_PACKET_HEADER) || packetRemanent==0 )
            {
				
				Byte *buf = (Byte *)(&packetHeader)+packetHeaderOffset;
				int remains = sizeof(IC_PACKET_HEADER)-packetHeaderOffset;
				int len = MIN(remains,remanentLen);
				
				[data getBytes:buf range:NSMakeRange(readLen, len)];
				
				readLen += len;
				remanentLen -= len;
				packetHeaderOffset += len;
                
                // packet header receive finished
				if ( packetHeaderOffset==sizeof(IC_PACKET_HEADER) ) {
					
					packetRemanent = packetHeader.size;
					//NSLog(@"[iC] PacketHeader - Type:%d, Sync:%X, Time:%d, Num:%d ,Size:%d, xxx:%d, chbits:%d, magic:%d"
                    //      ,packetHeader.type,packetHeader.sync,packetHeader.time,packetHeader.num,packetHeader.size,packetHeader.xxx,packetHeader.av.chbits,packetHeader.magic);
					// check packet header
                    if (packetHeader.sync != IC_SYNC) {
                        NSLog(@"[iC] Packet header data error! %0xX",packetHeader.sync);
                        
                        packetHeaderOffset = 0;
                        packetRemanent = 0;
                    }
					
					// receive a new frame
                    frameHeaderOffset = 0;
                    frameOffset = 0;
                    frameSize = 0;
                    frameNum = 0;
				}
            }
                
            // read frame header data
            if ( frameHeaderOffset<sizeof(IC_FRAME_HEADER) && remanentLen>0 && packetRemanent>0 )
            {
                Byte *buf = &pFrameBuf[frameHeaderOffset];
                int remains = sizeof(IC_FRAME_HEADER)-frameHeaderOffset;
                int len = MIN(remains,MIN(remanentLen,packetRemanent));
                
                [data getBytes:buf range:NSMakeRange(readLen, len)];
                
                readLen += len;
                remanentLen -= len;
                frameHeaderOffset += len;
                packetRemanent -= len;
            }
            
            // header data finish
            if ( frameHeaderOffset==sizeof(IC_FRAME_HEADER) ) {
                
                IC_FRAME_HEADER	*pHeader  = (IC_FRAME_HEADER *)&pFrameBuf[0];
                frameSize = pHeader->size;
                //NSLog(@"[iC] Receive FrameData - CH:%d, Type:%d, Size:%d/%d, remanentLen:%d",pHeader->video.ch,pHeader->type,frameOffset,frameSize, remanentLen);
                
                // read frame data
                if ( frameOffset<frameSize && remanentLen>0)
                {
                    Byte *buf = &pFrameBuf[sizeof(IC_FRAME_HEADER)+frameOffset];
                    int remains = frameSize-frameOffset;
                    int len = MIN(remains,MIN(remanentLen,packetRemanent));
                    
                    [data getBytes:buf range:NSMakeRange(readLen, len)];
                    
                    readLen += len;
                    remanentLen -= len;
                    frameOffset += len;
                    packetRemanent -= len;
                    
                    //frame data finish
                    if ( frameOffset==frameSize ) {
                        
                        Byte *data = (Byte *)&pFrameBuf[sizeof(IC_FRAME_HEADER)];
                        //NSLog(@"[iC] Receive Entire Frame - [%x] remanentLen:%d Packet:%d/%d",data[4],remanentLen,packetHeaderOffset,packetRemanent);
                        
                        frameOffset = 0;
                        frameHeaderOffset = 0;
                        frameSize = 0;
                        
                        //video frame
                        if (pHeader->type < 2) {
                            
                            //decode and show video frame
                            NSInteger chIdx = pHeader->video.ch;
                            NSInteger iBufferIdx = [[outputList objectAtIndex:chIdx] integerValue];
                            if (iBufferIdx == 99) {
                                continue;
                            }
                            [videoControl setPlayingChannelByCH:iBufferIdx Playing:chIdx];  //從外面設定，以免無法Close
                            frameWidth = pHeader->video.width;
                            frameHeight = pHeader->video.height;
                            
                            VIDEO_PACKAGE videoPkg;
                            
                            videoPkg.sec	= packetHeader.time + packetHeader.gmtoff;
                            videoPkg.ms     = 0xFF;
                            videoPkg.seq	= sequence[chIdx]++;
                            videoPkg.size	= pHeader->size;
                            videoPkg.width	= pHeader->video.width;
                            videoPkg.height	= pHeader->video.height;
                            videoPkg.channel = 0x1<<chIdx;
                            videoPkg.codec = AV_CODEC_ID_H264;
                            videoPkg.data	= malloc(pHeader->size);
                            memcpy(videoPkg.data, data, pHeader->size);
                            
                            switch (pHeader->type) {
                                case IC_FRAME_TYPE_I:
                                    videoPkg.type = VO_TYPE_IFRAME;
                                    break;
                                case IC_FRAME_TYPE_P:
                                    videoPkg.type = VO_TYPE_PFRAME;
                                    break;
                                default:
                                    videoPkg.type = VO_TYPE_NONE;
                                    break;
                            }
                            
                            //video available, callback to UI Controller
                            if (!blnVideoAvailable) {
                                [self.delegate EventCallback:@"VIDEO_OK"];
                                blnVideoAvailable = YES;
                                
                                [self.delegate EventCallback:@"RECORD_NO"]; //recording can't be saved, issue!!!
                                if (m_intDiskStartTime == 0 || m_intDiskEndTime == 0)
                                    [self.delegate EventCallback:@"PLAYBACK_NO"];
                                
                            }
                            
                            //set camera title
                            CameraInfo *info = [cameraList objectAtIndex:pHeader->video.ch];
                            if (![info.title isEqualToString:[self.videoControl.aryTitle objectAtIndex:iBufferIdx]]) {
                                
                                [self.videoControl setTitleByCH:iBufferIdx title:info.title];
                            }
                            
                            //NSLog(@"[iC] CH:%lu Time:%lu Type:%d Sync:%d Magic:%d Title:%s FPS:%d",videoPkg.channel,videoPkg.sec,videoPkg.type,packetHeader.sync,packetHeader.magic,pHeader->data,pHeader->video.fps);
                            [self.videoControl putFrameIntoBufferByCH:&videoPkg chIndex:iBufferIdx];
                        }
                        else {  //audio frame
                            
                            if (audioOn) {
                                
                                if(audioControl!=nil && audioControl.codecId==0) {
                                    switch (pHeader->audio.type) {
                                        case IC_AUDIO_TYPE_ADPCM:
                                            [audioControl initWithCodecId:AV_CODEC_ID_ADPCM_IMA_WAV srate:8000 bps:4 balign:164 fsize:321 channel:1];
                                            break;
                                        case IC_AUDIO_TYPE_AAC:
                                            break;
                                            NSLog(@"[iC] Unknown Audio Codec");
                                        default:
                                            break;
                                    }
                                    audioControl.amFlag = pHeader->audio.type;
                                    if (audioControl!=nil)
                                        [audioControl start];
                                }
                                
                                // decode and play the audio frame
                                if (audioControl!=nil) {
                                    
                                    int offset = 0;
                                    int length = pHeader->size;
                                    
                                    switch (pHeader->audio.type) {
                                            
                                        case IC_AUDIO_TYPE_ADPCM:
                                            while (length>=168) { // may have more than one frame at one time
                                                [audioControl playAudio:data+offset+4 length:164];
                                                offset += 168;
                                                length -= 168;
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@end
