//
//  iCPTZController.h
//  EFViewerPlus
//
//  Created by James Lee on 2014/7/26.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import "PtzController.h"

@interface iCPTZController : PtzController
{
    NSDictionary    *ptzCmdDict;
}

@end
