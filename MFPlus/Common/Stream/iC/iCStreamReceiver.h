//
//  iCStreamReceiver.h
//  EFViewerPlus
//
//  Created by James Lee on 2014/6/13.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import "StreamReceiver.h"
#import "iCNet.h"

#define MAX_IC_CHANNEL  16

@interface iCStreamReceiver : StreamReceiver
{
    IC_PACKET_HEADER	packetHeader;
	Byte pFrameBuf[MAX_FRAME_SIZE];
    
    int frameSize;
	int packetRemanent;
	int packetHeaderOffset;
	int stchHeaderOffset;
	int frameHeaderOffset;
	int frameOffset;
    int frameNum;
    
    BOOL blnGetDiskInfo;
    BOOL blnVideoAvailable;
    BOOL blnGetTZInfo;
    BOOL blnGetPTZInfo;
    
    NSInteger currentIndex;
    
    unsigned long sequence[MAX_IC_CHANNEL];
}

@end
