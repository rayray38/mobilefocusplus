//
//  iCNet.h
//  EFViewerPlus
//
//  Created by James Lee on 2014/6/16.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#pragma pack(push,1)

#define IC_FRAME_TYPE_I         0
#define IC_FRAME_TYPE_P         1

#define IC_AUDIO_TYPE_NONE      -1
#define IC_AUDIO_TYPE_ADPCM     0
#define IC_AUDIO_TYPE_AAC       1
#define IC_AUDIO_TYPE_LPCM      2

#define IC_SYNC                 0x1234

typedef struct _IC_PACKET_HEADER
{
	int         sync;
    int         magic;
    int         type;       //0:video 1:audio
    int         time;
    int         gmtoff;
    int         xxx;
    int         size;
    int         num;
    union {
        int     data[64];
        struct {
            int count;
            int chbits;
            int size[32];
        }av;
    };
} IC_PACKET_HEADER, *PIC_PACKET_HEADER;

typedef struct _IC_PACKET_TAIL
{
    int         sync;
    int         size;
} IC_PACKET_TAIL, *PIC_PACKET_TAIL;

typedef struct _IC_VIDEO_HEADER
{
    int         ch;
    int         width;
    int         height;
    int         fps;
    unsigned long long    pts;
} IC_VIDEO_HEADER, *PIC_VIDEO_HEADER;

typedef struct _IC_AUDIO_HEADER
{
    int         ch;
    unsigned long long pts;
    int         type; //AUDIO_TYPE_ADPCM, AUDIO_TYPE_AAC
} IC_AUDIO_HEADER, *PIC_AUDIO_HEADER;

typedef struct _IC_JPEG_HEADER
{
    int         ch;
    int         width;
    int         height;
} IC_JPEG_HEADER, *PIC_JPEG_HEADER;

typedef struct _IC_FRAME_HEADER
{
    int         type;   //I, P, Audio
    union {
        char data[32];
        IC_VIDEO_HEADER   video;
        IC_AUDIO_HEADER   audio;
        IC_JPEG_HEADER    jpeg;
    };
    int         actual_size;
    int         size;
} IC_FRAME_HEADER, *PIC_FRAME_HEADER;

#pragma pack(pop)