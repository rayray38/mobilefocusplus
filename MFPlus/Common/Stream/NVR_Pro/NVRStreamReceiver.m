//
//  NVRStreamReceiver.m
//  EFViewer
//
//  Created by EFRD on 2016/12/7.
//  Copyright © 2016年 EverFocus. All rights reserved.
//

#import "NVRStreamReceiver.h"

@implementation NVRStreamReceiver

- (id)initWithDevice:(Device *)device
{
    streamArray = [NSMutableDictionary new];
    memset(scH264, 0, sizeof(scH264));
    scH264[3] = 0x01;
    
    // init output list to match channel and layout window
    if (!outputList) {
        outputList = [[NSMutableArray alloc] init];
        
        NSString *nan = [NSString stringWithFormat:@"99"];
        for (NSInteger i=0; i<MAX_CH_NUMBER; i++) {
            [outputList addObject:nan];
        }
    }
    
    return [super initWithDevice:device];
}

- (void)dealloc
{
    if (ppsPkg.data != nil) {
        free(ppsPkg.data);
    }
    if (spsPkg.data != nil) {
        free(spsPkg.data);
    }
    [streamArray removeAllObjects];
    SAVE_FREE(streamArray);
    
    machineName = nil;
    
    [super dealloc];
}

#pragma mark - Streaming Control

- (BOOL)startLiveStreaming:(NSUInteger)mask
{
    BOOL ret = YES;
    blnStopStreaming = NO;
    blnVideoAvailable = NO;
    
    if ( ![super startLiveStreaming:mask] )
    {
        ret =  NO;
        goto Exit;
    }
    
    // set initial parameters
    seq = 0;
    startBuf[0] = 0x00;
    startBuf[1] = 0x00;
    startBuf[2] = 0x00;
    startBuf[3] = 0x01;
    
    DBGLog(@"[NVR265] Init RtspClientSession");
    
    if (mask < 9) {
        self.m_stream_uri = [NSString stringWithFormat:@"rtsp://%@:554/ch0%lu/%ld",[self getIp],(unsigned long)mask+1,(long)self.dualStream];
    }
    else
        self.m_stream_uri = [NSString stringWithFormat:@"rtsp://%@:554/ch%lu/%ld",[self getIp],(unsigned long)mask+1,(long)self.dualStream];
    
    ///// RTSP /////
    // init RTSP client
    //modify by robert hsu 20120203 for add rtsp port
    
    RTSPClientSession *rtspClientSession = [[RTSPClientSession alloc] initWithURL:self.m_stream_uri
                                                          user:self.user
                                                        passwd:self.passwd];
    DBGLog(@"[NVR265] URL:%@",self.m_stream_uri);
    
    // get SDP and create session
    if (![rtspClientSession setup])
    {
        self.errorDesc = NSLocalizedString(@"MsgRtspErr1", nil);
        
        [rtspClientSession release];
        rtspClientSession = nil;
        
        ret =  NO;
        goto Exit;
    }
    
    // get subsession
    NSArray *subsessions = [[rtspClientSession getSubsessions] retain];
    if([subsessions count]==0)
    {
        self.errorDesc = NSLocalizedString(@"MsgRtspErr2", nil);
        ret =  NO;
        goto Exit;
    }
    DBGLog(@"[NVR265] Subsession Count:%lu",(unsigned long)[subsessions count]);
    
    // setup subsession
    BOOL videoAvailable = NO;
    
    blnIsFirstIframe = NO;
    for (int i=0; i<[subsessions count]; i++) {
#ifndef _NEW_RTSP
        if (![rtspClientSession setupSubsession:[subsessions objectAtIndex:i] useTCP:YES])//modify by robert hsu 2012.02.01 for use over tcp
        {
            self.errorDesc = NSLocalizedString(@"MsgRtspErr3", nil);
            ret =  NO;
            goto Exit;
        }
#endif
        RTSPSubsession* subsession = [subsessions objectAtIndex:i];
        DBGLog(@"[NVR265] Subsession %@: MediumName=%@ CodecName=%@",
              [subsession getSessionId], [subsession getMediumName], [subsession getCodecName]);
        
        if ([[subsession getMediumName] isEqual:@"video"]) {
            
            if (![[subsession getCodecName] isEqual:@"JPEG"]) {
                
                [self SdpParser:[subsession getSDP_spropparametersets]];
                
            }
            videoAvailable = YES;
        }
        
        [[subsessions objectAtIndex:i] setDelegate:(id)self];
    }
    
    if(!videoAvailable)
    {
        self.errorDesc = NSLocalizedString(@"MsgRtspErr4", nil);
        ret =  NO;
        goto Exit;
    }
    
    // play
    NSNumber *keyIdx = [NSNumber numberWithInteger:currentCHMask];
    [streamArray setObject:rtspClientSession forKey:keyIdx];
    [rtspClientSession play];
    blnReceivingStream = YES;
    
    // initiate audio sender
    if (audioSender != nil) {
        [audioSender release];
        audioSender = nil;
    }
    //audioSender = [[GTAudioSender alloc] initWithHost:host user:user passwd:passwd sid:nil];
    
    
    // run loop
    [rtspClientSession runEventLoop];
    [streamArray removeObjectForKey:keyIdx];
    
Exit:
    self.blnReceivingStream = NO;
    ret = NO;
    
    return ret;
}

- (void)stopStreaming
{
    if (audioOn) {
        [self closeSound];
    }
    [self stopStreamingByView:currentCHMask];
}

- (void)stopStreamingByView:(NSInteger)vIdx
{
    NSNumber *keyIdx = [NSNumber numberWithInteger:currentCHMask];
    RTSPClientSession *rtspClnt = [streamArray objectForKey:keyIdx];
    NSInteger channel = [[[rtspClnt getSubsessions] firstObject] channel];
    
    [rtspClnt stop];
    
    while ([streamArray objectForKey:keyIdx])
        [NSThread sleepForTimeInterval:0.01f];
    
    if(![rtspClnt teardown])
        NSLog(@"[XMS] Close CH:%ld RTSP failed",(long)channel);
}

#pragma mark - Get Information

- (BOOL)getDeviceInfo
{
    BOOL ret = YES;
    
    if (![super getDeviceInfo]) { //get DDNS if need
        ret =  NO;
        goto Exit;
    }
    
    // get camera list
    if (!cameraList) {
        cameraList = [[NSMutableArray alloc] init];
    }
    
    iMaxSupportChannel = 32;
    validChannel = 1;
    
    for (int i=0; i<32; i++) {
        CameraInfo *tmpInfo = [[[CameraInfo alloc] init] autorelease];
        tmpInfo.enable = YES;
        tmpInfo.status = 0;
        tmpInfo.index = i+1;
        tmpInfo.install = 1;
        tmpInfo.title = [NSString stringWithFormat:@"CH%ld",(long)i+1];
        
        [cameraList addObject:tmpInfo];
    }
    
    [NSThread sleepForTimeInterval:1];
    
    self.blnReceivingStream = NO;
    
    return ret;
    
Exit:
    
    self.blnReceivingStream = NO;
    [self stopStreaming];
    
    return ret;
}

// Get SPS & PPS from SDP    20130319 James Lee
- (void)SdpParser:(NSString *)sdpStr
{
    if ([sdpStr rangeOfString:@","].location != NSNotFound) {
        NSArray *strArray = [sdpStr componentsSeparatedByString:@","];
        NSData *spsData = [Decryptor base64:[strArray objectAtIndex:0]];
        NSData *ppsData = [Decryptor base64:[strArray objectAtIndex:1]];
        //DBGLog(@"[NVR265] extra:%@, SPS:%@(%ld), PPS:%@(%ld)",sdpStr,[strArray objectAtIndex:0],(unsigned long)spsData.length,[strArray objectAtIndex:1],(unsigned long)ppsData.length);
        
        spsPkg.size = sizeof(startBuf) + spsData.length;
        ppsPkg.size = sizeof(startBuf) + ppsData.length;
        spsPkg.data = malloc(spsPkg.size);
        ppsPkg.data = malloc(ppsPkg.size);
        memcpy(spsPkg.data, startBuf, sizeof(startBuf));
        memcpy(spsPkg.data+sizeof(startBuf), [spsData bytes], spsData.length);
        memcpy(ppsPkg.data, startBuf, sizeof(startBuf));
        memcpy(ppsPkg.data+sizeof(startBuf), [ppsData bytes], ppsData.length);
    }
}

#pragma mark - Connection Delegate

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.
{
    if ( theConnection==requestConnection ) {
        
        [responseData appendData:data];
    }
}

#pragma mark - RTSP delegate

- (void)didReceiveMessage:(NSString *)message
{
}

- (void)didReceiveFrame:(const uint8_t*)frameData
        frameDataLength:(int)frameDataLength
       presentationTime:(struct timeval)presentationTime
 durationInMicroseconds:(unsigned)duration
             subsession:(RTSPSubsession*)subsession
{
    //DBGLog(@"Receive:%@ - %d",[subsession getMediumName],frameDataLength);
    // Video frame
    if ([[subsession getMediumName] isEqual:@"video"]) {
        
        //        DBGLog(@"[NVR265] Video frame: frameDataLength=%d presentationTime=%ld:%d",
        //        	  frameDataLength, presentationTime.tv_sec, presentationTime.tv_usec);
        
        // build video package
        VIDEO_PACKAGE videoPkg;
        
        videoPkg.sec	= presentationTime.tv_sec;
        videoPkg.ms		= presentationTime.tv_usec/1000;
        videoPkg.channel = subsession.channel;
        
        NSInteger _width, _height;
        [subsession getSDP_VideoWidth:&_width Height:&_height];
        videoPkg.width = _width;
        videoPkg.height = _height;
        
        NSInteger iBufferIdx = [[outputList objectAtIndex:subsession.channel] intValue];
        NSLog(@"Sub.Channel:%ld, iBufferIdx:%ld",(long)subsession.channel,(long)iBufferIdx);
        if (iBufferIdx > MAX_SUPPORT_DISPLAY)
            return;
        
        if (!keyBuf[iBufferIdx])
            keyBuf[iBufferIdx] = [[NSMutableData alloc] init];
        
        if ([[subsession getCodecName] isEqual:@"H264"]) { // H264
            
            videoPkg.codec	= AV_CODEC_ID_H264;
            
            ENaluType NalType = 0x1F & frameData[0];
            switch (NalType) {
                case nalu_type_sps:
                case nalu_type_pps:
                    //case nalu_type_aud:
                    //case nalu_type_sei:
                    [keyBuf[iBufferIdx] appendBytes:scH264 length:sizeof(scH264)];
                    [keyBuf[iBufferIdx] appendBytes:frameData length:frameDataLength];
                    blnGetIDR = NO;
                    return;
                case nalu_type_idr:
                    [keyBuf[iBufferIdx] appendBytes:scH264 length:sizeof(scH264)];
                    [keyBuf[iBufferIdx] appendBytes:frameData length:frameDataLength];
                    videoPkg.size = keyBuf[iBufferIdx].length;
                    videoPkg.data = malloc(videoPkg.size);
                    videoPkg.type = VO_TYPE_IFRAME;
                    memcpy(videoPkg.data, keyBuf[iBufferIdx].bytes, keyBuf[iBufferIdx].length);
                    [keyBuf[iBufferIdx] setLength:0];
                    blnGetIDR = YES;
                    break;
                case nalu_type_slice:
                case nalu_type_dpa:
                case nalu_type_dpb:
                case nalu_type_dpc:
                    if (!blnGetIDR) {
                        NSLog(@"[XMS] IDR Missing - seq:%d Time:%lu.%03d",sequence[iBufferIdx],videoPkg.sec,videoPkg.ms);
                        return;
                    }
                    [keyBuf[iBufferIdx] appendBytes:scH264 length:sizeof(scH264)];
                    [keyBuf[iBufferIdx] appendBytes:frameData length:frameDataLength];
                    videoPkg.size = keyBuf[iBufferIdx].length;
                    videoPkg.data = malloc(videoPkg.size);
                    videoPkg.type = VO_TYPE_PFRAME;
                    memcpy(videoPkg.data, keyBuf[iBufferIdx].bytes, keyBuf[iBufferIdx].length);
                    [keyBuf[iBufferIdx] setLength:0];
                    break;
                default:
                    //NSLog(@"H264 Drop:%d - Time:%ld.%ld Size:%d",NalType,pkg.sec,pkg.ms,frameDataLength);
                    return;
            }
            
            videoPkg.seq = sequence[iBufferIdx]++;
        }
        else if ([[subsession getCodecName] isEqual:@"H265"]) { // /h265
            
            videoPkg.codec = AV_CODEC_ID_HEVC;
            NSUInteger NalType = ((0x7E & frameData[0]) >> 1);
            switch (NalType) {
                case 32:  //vps
                case 33:  //sps
                case 34:  //pps
                    [keyBuf[iBufferIdx] appendBytes:scH264 length:sizeof(scH264)];
                    [keyBuf[iBufferIdx] appendBytes:frameData length:frameDataLength];
                    blnGetIDR = NO;
                    return;
                case 19:  //I-frame
                    [keyBuf[iBufferIdx] appendBytes:scH264 length:sizeof(scH264)];
                    [keyBuf[iBufferIdx] appendBytes:frameData length:frameDataLength];
                    videoPkg.size = keyBuf[iBufferIdx].length;
                    videoPkg.data = malloc(videoPkg.size);
                    videoPkg.type = VO_TYPE_IFRAME;
                    memcpy(videoPkg.data, keyBuf[iBufferIdx].bytes, keyBuf[iBufferIdx].length);
                    [keyBuf[iBufferIdx] setLength:0];
                    blnGetIDR = YES;
                    break;
                case 0:  //maybe P-frame or B-frame
                case 6:  //maybe P-frame or B-frame
                case 1:  //P-frame
                    if (!blnGetIDR) {
                        DBGLog(@"[XMS] IDR Missing - seq:%d Time:%lu.%03d",sequence[iBufferIdx],videoPkg.sec,videoPkg.ms);
                        return;
                    }
                    [keyBuf[iBufferIdx] appendBytes:scH264 length:sizeof(scH264)];
                    [keyBuf[iBufferIdx] appendBytes:frameData length:frameDataLength];
                    videoPkg.size = keyBuf[iBufferIdx].length;
                    videoPkg.data = malloc(videoPkg.size);
                    videoPkg.type = VO_TYPE_PFRAME;
                    memcpy(videoPkg.data, keyBuf[iBufferIdx].bytes, keyBuf[iBufferIdx].length);
                    [keyBuf[iBufferIdx] setLength:0];
                    break;
                default:
                    //DBGLog(@"H264 Drop:%d - Time:%ld.%ld Size:%d",NalType,pkg.sec,pkg.ms,frameDataLength);
                    return;
            }
            
            videoPkg.seq = sequence[iBufferIdx]++;
            
        }
        else if ([[subsession getCodecName] isEqual:@"JPEG"]) { // JPEG
            
            videoPkg.codec	= AV_CODEC_ID_MJPEG;
            
            seq = 0;
            videoPkg.seq	= seq;
            videoPkg.type	= VO_TYPE_IFRAME;
            videoPkg.size	= frameDataLength;
            videoPkg.data	= malloc(videoPkg.size);
            memcpy(videoPkg.data, frameData, frameDataLength);
        }
        else if ([[subsession getCodecName] isEqual:@"MP4V-ES"]){ // MPEG4
            
            if ((frameData[4] & 0x40) == 0) {
                videoPkg.codec = AV_CODEC_ID_MPEG4;
                seq = 0;
                videoPkg.seq	= seq;
                videoPkg.type	= VO_TYPE_IFRAME;
                videoPkg.size	= frameDataLength;
                videoPkg.data	= malloc(videoPkg.size);
                memcpy(videoPkg.data, frameData, frameDataLength);
#ifdef H264DEBUG
                DBGLog(@"[264] Get Mpeg I-Frame:%d",frameDataLength);
#endif
            }else if ((frameData[4] & 0x40) == 0x40) {
                seq++;
                videoPkg.codec = AV_CODEC_ID_MPEG4;
                videoPkg.seq	= seq;
                videoPkg.type	= VO_TYPE_PFRAME;
                videoPkg.size	= frameDataLength;
                videoPkg.data	= malloc(videoPkg.size);
                memcpy(videoPkg.data, frameData, frameDataLength);
#ifdef H264DEBUG
                DBGLog(@"[264] Get Mpeg P-Frame:%d",frameDataLength);
#endif
            }
        }
        else {
            DBGLog(@"[NVR265] Unknown codec type: %@", [subsession getCodecName]);
            return;
        }
        
        CameraInfo *tmpInfo = [cameraList objectAtIndex:currentCHMask];
        machineName = tmpInfo.title;
        
        if (![machineName isEqualToString:[self.videoControl.aryTitle objectAtIndex:iBufferIdx]])
            [self.videoControl setTitleByCH:iBufferIdx title:machineName];
        
        // put package to VideoOutput buffer
        //DBGLog(@"0x%x - size:%d",frameData[0],frameDataLength);
        [videoControl putFrameIntoBufferByCH:&videoPkg chIndex:iBufferIdx];
        
        //video available, callback to UI Controller
        if (!blnVideoAvailable) {
            [self.delegate EventCallback:@"VIDEO_OK"];
            blnVideoAvailable = YES;
        }
    }
    // Audio frame
    else if ([[subsession getMediumName] isEqual:@"audio"]) {
        
        if (audioControl!=nil && audioOn) {
            [audioControl playAudio:(Byte *)frameData length:frameDataLength];
        }
        else {
            //DBGLog(@"[NVR265] Receive audio (OFF)");
        }
        
    }else {
        DBGLog(@"[NVR265] Unknown medium name: %@", [subsession getMediumName]);
    }
}

@end
