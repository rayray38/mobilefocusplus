//
//  NVRStreamReceiver.h
//  EFViewer
//
//  Created by EFRD on 2016/12/7.
//  Copyright © 2016年 EverFocus. All rights reserved.
//

#import "StreamReceiver.h"
#import "RTSPClientSession.h"

@interface NVRStreamReceiver : StreamReceiver<RTSPSubsessionDelegate>
{
    NSString *machineName;
    BOOL     blnVideoAvailable;
    NSMutableDictionary *streamArray;
    
    int                 seq;
    uint8_t             scH264[4];
    NSMutableData       *keyBuf[MAX_SUPPORT_DISPLAY];
    int                 sequence[MAX_SUPPORT_DISPLAY];
    uint8_t             startBuf[4];
    
    VIDEO_PACKAGE spsPkg;
    VIDEO_PACKAGE ppsPkg;
    VIDEO_PACKAGE iframePkg;
    VIDEO_PACKAGE pframePkg;
    
    int audioSize;
    
    NSMutableArray *targetLine;
    NSMutableArray *targetInfo;
    NSString  *sdpPath;
    
    NSInteger iPTZCHMask ; // add by robert hsu 20110928 for keep enable PTZ channel mask
    NSInteger iCurrentStream;
    BOOL blnGetIDR;
    BOOL blnIsFirstIframe;
}

@end
