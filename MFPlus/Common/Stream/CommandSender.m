//
//  CommandSender.m
//  EFViewerHD
//
//  Created by James Lee on 12/10/9.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "CommandSender.h"

#define DEFAULT_CONCURRENTOPERATION 1     //NSURLSession test

@interface CommandSender()
@property (nonatomic, strong) NSOperationQueue *operationQueue;
@property (nonatomic, strong) NSMutableArray   *operations;
@end

@implementation CommandSender

@synthesize delegate,host;

#pragma mark - Initialization

+ (instancetype)sharedManager
{
    static CommandSender *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[CommandSender alloc] init];
    });
    return manager;
}

- (instancetype)init
{
    if (self = [super init]) {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = DEFAULT_CONCURRENTOPERATION;
    }
    return self;
}

- (id)initWithHost:(NSString *)theHost delegate:(id)theDelegate {
    
	if ( (self = [super init]) ) {
		
		self.host = theHost;
		self.delegate = theDelegate;
	}
	
	return self;
}

#pragma mark - EFRequestManager

- (void)addRequest:(NSString *)cmd withHandler:(EFRequestHandler)requestHandler
{
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSURL *urlString = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",host,cmd]];
        NSURLRequest *request = [NSURLRequest requestWithURL:urlString];
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    requestHandler(response, data, error);
                                                }];
        [task resume];
    }];
    
    [_operationQueue addOperation:operation];
}

#pragma mark - Nevio Command
- (void)postCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withPath:(NSString *)path forOldDev:(BOOL)OldDev
{
    NSString *urlString = [NSString stringWithFormat:@"http://%@/",host];
    if (path)
        urlString = [urlString stringByAppendingString:path];
    
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    // set headers
    NSString *contentType;
    if (OldDev) {
        contentType = [NSString stringWithFormat:@"application/x-www-form-urlencoded"];
    }
    else
        contentType = [NSString stringWithFormat:@"application/json"];
    
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create the body
    NSData *postBody = [cmd dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postBody];
    
    // connect
    *pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    DBGLog(@"[CMD Post] Request:%@",urlString);
    
    // wait for connection complete
    if (synchronously) {
        while( (*pConnection)!=nil ) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }
}

- (void)getNevioData:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withPath:(NSString *)path
{
    NSString *urlString = [NSString stringWithFormat:@"http://%@/%@?%@",host,path,cmd];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:10.0f];
    
    // connect
    *pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    DBGLog(@"[CMD GET] Request:%@",urlString);
    
    // wait for connection complete
    if (synchronously) {
        while( (*pConnection)!=nil ) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }
}

#pragma mark - XMS Login Command
- (void)postCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withuser:(NSString *)user withpwd:(NSString *)pwd
{
    NSString *urlString = [NSString stringWithFormat:@"http://%@/%@",host,cmd];
    
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    // set headers
    NSString *contentType = [NSString stringWithFormat:@"application/x-www-form-urlencoded"];
    //NSString *contentType = [NSString stringWithFormat:@"application/json"];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create the body
    NSString *body = [NSString stringWithFormat:@"username=%@&password=%@",user,pwd];
    NSData *postBody = [body dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postBody];
    [request setTimeoutInterval:10.0f];
    
    // connect
    *pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    DBGLog(@"[CMD Post] Request:%@",urlString);
    
    // wait for connection complete
    if (synchronously) {
        while( (*pConnection)!=nil ) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }
}

#pragma mark - XMS Post Command
- (void)postCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withPath:(NSString *)path withToken:(NSString *)token
{
    NSString *urlString = [NSString stringWithFormat:@"http://%@/",host];
    if (path)
        urlString = [urlString stringByAppendingString:path];
    
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:10.0f];
    
    // set headers
    //NSString *contentType = [NSString stringWithFormat:@"application/x-www-form-urlencoded"];
    NSString *contentType = [NSString stringWithFormat:@"application/json"];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSString *auth_header = [NSString stringWithFormat:@"Bearer %@",token];
    [request setValue:auth_header forHTTPHeaderField:@"Authorization"];
    
    // create the body
    NSData *postBody = [cmd dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:postBody];
    
    // connect
    *pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    DBGLog(@"[CMD Post] Request:%@",urlString);
    
    // wait for connection complete
    if (synchronously) {
        while( (*pConnection)!=nil ) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }
}

#pragma mark - XMS Get Command
- (void)getData:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withHeader:(NSString *)header
{
    NSString *urlString = [NSString stringWithFormat:@"http://%@/%@",host,cmd];
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    [request setHTTPShouldHandleCookies:NO];
    
    // set headers
   [request setValue:header forHTTPHeaderField:@"Authorization"];
   [request setTimeoutInterval:10.0f];
    
    // connect
    *pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    DBGLog(@"[CMD GET] Request:%@",urlString);
    
    // wait for connection complete
    if (synchronously) {
        while( (*pConnection)!=nil ) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }
}

#pragma mark - P2 Command

- (void)getData:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously random:(BOOL)random {
	
	NSString *urlString;
	
	if (random) {
		urlString = [NSString stringWithFormat:@"http://%@/%@&rnd=%lu",
					 host, cmd, (unsigned long)[NSDate timeIntervalSinceReferenceDate]];
	}
	else {
		urlString = [NSString stringWithFormat:@"http://%@/%@", host, cmd];
	}
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    
	request.URL = [NSURL URLWithString:urlString];
    request.HTTPMethod = @"GET";
    request.timeoutInterval = 10.0f;
	
    // connect
	*pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    
	DBGLog(@"[CMD GET] Request:%@",urlString);
	
	// wait for connection complete
	if (synchronously) {
		while( (*pConnection)!=nil ) {
			[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		}
	}
}

#pragma mark - NCB Command
- (void)getData:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously random:(BOOL)random supportNCB:(BOOL)support {
	
	NSString *urlString;
	
	if (random) {
		urlString = [NSString stringWithFormat:@"http://%@/%@&rnd=%lu",
					 host, cmd, (unsigned long)[NSDate timeIntervalSinceReferenceDate]];
	}
	else {
        if (!support) {
            
            urlString = [NSString stringWithFormat:@"http://%@/%@", host, cmd];
        }else {
            
            urlString = [NSString stringWithFormat:@"http://%@/cgi-bin/%@", host, cmd];
        }
	}
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    
	request.URL = [NSURL URLWithString:urlString];
	request.HTTPMethod = @"GET";
    request.timeoutInterval = 10.0f;
	
    // connectc
	*pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    
	DBGLog(@"[CMD GET] Request:%@",urlString);
	
	// wait for connection complete
	if (synchronously) {
		while( (*pConnection)!=nil ) {
			[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		}
	}
}

#pragma mark - PSIA Command
/*
 Parm :
 1. (NSString *) cmd : xml for put command
 2. (NSString *) uri : the uri for psia procotol (xml url)
 3. (NSURLConnection **)pConnection
 4. synchronously:(BOOL)
 purpose :
 1. send PSIA put command to device
 
 create :
 by robert hsu 20110928 for psia set command
 */
- (void)putPSIACommand:(NSString *)cmd with:(NSString *)uri connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously
{
    NSString *urlString;
    
    urlString = [NSString stringWithFormat:@"http://%@/%@",
                 host, uri];
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
	[request setURL:[NSURL URLWithString:urlString]];
	[request setHTTPMethod:@"PUT"];
    [request setValue:@"application/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    DBGLog(@"[CMD] Request:%@",urlString);
    // create the body
	NSData *postBody = [cmd dataUsingEncoding:NSUTF8StringEncoding];
	[request setHTTPBody:postBody];
    
	
	// connect
	*pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
	DBGLog(@"[CMD PSIA] Request:%@",cmd);
	
	// wait for connection complete
	if (synchronously) {
		while( (*pConnection)!=nil ) {
			[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		}
	}
}

#pragma mark - iC Command
- (void)postICCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously
{
	NSString *urlString = [NSString stringWithFormat:@"http://%@/dvr/cmd",host];
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
	[request setURL:[NSURL URLWithString:urlString]];
	[request setHTTPMethod:@"DVRPOST"];
    [request setTimeoutInterval:10.0f];
	
	// set headers
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=----DVRBoundary"];
	[request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSString *disposition = [NSString stringWithFormat:@"form-data; name=\"datafile\"; filename=\"command.xml\""];
    [request addValue:disposition forHTTPHeaderField:@"Content-Disposition"];
	
	// create the body
    NSString *bodyString = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?><DVR Platform=\"Hi3520\">%@</DVR>",cmd];
	NSData *postBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
	[request setHTTPBody:postBody];
	
	// connect
	*pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
	DBGLog(@"[CMD iC] Request:%@%@",urlString,bodyString);
	
	// wait for connection complete
	if (synchronously) {
		while( (*pConnection)!=nil ) {
			[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		}
	}
}

- (void)getICData:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withUID:(NSString *)uid withAuth:(NSString *)auth
{
    NSString *urlString = [NSString stringWithFormat:@"http://%@/cgi-bin/%@", host, cmd];
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    
	request.URL = [NSURL URLWithString:urlString];
	request.HTTPMethod = @"DVRGET";
    request.timeoutInterval = 20.0f;
    [request addValue:uid forHTTPHeaderField:@"Magic"];
    [request setValue:auth forHTTPHeaderField:@"Authorization"];
    
    // connect
	*pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    
	DBGLog(@"[CMD iC] Request:%@",urlString);
	
	// wait for connection complete
	if (synchronously) {
		while( (*pConnection)!=nil ) {
			[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		}
	}
}

#pragma mark - APNs Command
- (void)postCommand:(NSString *)cmd
         connection:(NSURLConnection **)pConnection
      synchronously:(BOOL)synchronously
          tokenInfo:(NSString *)Info
        deviceToken:(NSString *)devToken
         deviceName:(NSString *)devName
{
    NSString *urlString = [NSString stringWithFormat:@"http://%@/%@",host,cmd];
    
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:10.0f];
    
    // set headers
    NSString *contentType = [NSString stringWithFormat:@"application/json"];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setValue:Info forHTTPHeaderField:@"Authorization"];
    
    // get iDevice UUID
    NSString *Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    // get applications name
    NSString *appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    NSString *bundleName;
    
    if ([appName isEqualToString:@"MobileFocus"]) {
        bundleName = @"mf";
    }
    else if ([appName isEqualToString:@"MobileFocus+"]) {
        bundleName = @"mfplus";
    }
    else if ([appName isEqualToString:@"MFHD"]) {
        bundleName = @"mfhd";
    }
    else
        bundleName = @"mfhdplus";
    
    NSString *mobileUUID = [NSString stringWithFormat:@"%@%@",bundleName,Identifier];
    
    // create the body
    NSDictionary *requestData;
    
    if ([cmd rangeOfString:@"DeviceInfo"].location != NSNotFound) {
        requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                       //object,keys
                       @"getVersion", @"action",
                       @"xms", @"vtype", nil];
    }
    // InsertByMobile body
    else if ([cmd rangeOfString:@"Insert"].location != NSNotFound) {
        requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                       //object,keys
                       [[NSDictionary alloc] initWithObjectsAndKeys:
                        @"true", @"Enabled",
                        @"Mobile", @"DeviceType",
                        [[UIDevice currentDevice] name], @"DeviceName",
                        [[NSDictionary alloc] initWithObjectsAndKeys:
                         @"ios", @"OS",
                         devToken, @"RegistrationId",
                         bundleName, @"App",
                         mobileUUID, @"MobileID",
                         devName, @"From", nil], @"Property", nil], @"Data", nil];
    }
    // DeleteByMobile body
    else {
        requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                       //object,keys
                       [[NSDictionary alloc] initWithObjectsAndKeys:
                        @"Mobile", @"DeviceType",
                        [[NSDictionary alloc] initWithObjectsAndKeys:
                         devToken, @"RegistrationId",
                         bundleName, @"App",
                         mobileUUID, @"MobileID", nil], @"Property", nil], @"Data", nil];
    }
    
    NSError *error;
    NSData *postBody = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
    [request setHTTPBody:postBody];
    
    // connect
    *pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    DBGLog(@"[CMD APNs] Request:%@",urlString);
    
    // wait for connection complete
    if (synchronously) {
        while( (*pConnection)!=nil ) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }
}

#pragma mark - xFleet Post Command
- (void)postCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withToken:(NSString *)token
{
    NSURL *urlString = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",host,cmd]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlString];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:10.0f];
    
    // set headers
    NSString *contentType = [NSString stringWithFormat:@"application/json"];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSString *auth_header = [NSString stringWithFormat:@"Bearer %@",token];
    [request setValue:auth_header forHTTPHeaderField:@"Authorization"];
    
    // set body
    NSDictionary *req_body = [NSDictionary new];
    if ([cmd rangeOfString:@"xFleetMenu" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        req_body = [NSDictionary dictionaryWithObjectsAndKeys:@"getDevice", @"action",
                    @"Vehicle", @"dtype", nil];
    }
    else if ([cmd rangeOfString:@"xFleetDevMgr" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        NSArray *req_array = [NSArray array];
        req_body = [NSDictionary dictionaryWithObjectsAndKeys:@"QueryVehStatus", @"action",
                    req_array, @"data", nil];
    }
    else {
        
    }
    
    NSError *error;
    NSData *postBody = [NSJSONSerialization dataWithJSONObject:req_body options:0 error:&error];
    [request setHTTPBody:postBody];
    
    DBGLog(@"[xFleet POST] Request : %@",urlString);
    
    // connect
    *pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    
    // wait for connection complete
    if (synchronously) {
        while( (*pConnection)!=nil ) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }
}

- (void)postCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withToken:(NSString *)token withStartTime:(NSInteger)stTime chIDAry:(NSArray *)chID
{
    NSURL *urlString = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",host,cmd]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlString];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:10.0f];
    
    // set headers
    NSString *contentType = [NSString stringWithFormat:@"application/json"];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSString *auth_header = [NSString stringWithFormat:@"Bearer %@",token];
    [request setValue:auth_header forHTTPHeaderField:@"Authorization"];
    
    // set body
    NSDictionary *req_body = [NSDictionary new];
    if ([cmd rangeOfString:@"xFleetVideoPlay" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        if (stTime == 0) {
            req_body = [NSDictionary dictionaryWithObjectsAndKeys:@"PlayControl", @"action",
                        chID, @"ChannelDeviceID",
                        @"sub", @"StreamType",
                        @"StreamReq", @"CMD", nil];
        }
        else {
            req_body = [NSDictionary dictionaryWithObjectsAndKeys:@"PlayControl", @"action",
                        chID, @"ChannelDeviceID",
                        @(stTime), @"StartTime",
                        @"main", @"StreamType",
                        @"start", @"CMD", nil];
        }
    }
    
    NSError *error;
    NSData *postBody = [NSJSONSerialization dataWithJSONObject:req_body options:0 error:&error];
    [request setHTTPBody:postBody];
    
    DBGLog(@"[xFleet POST] Request : %@",urlString);
    
    // connect
    *pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    
    // wait for connection complete
    if (synchronously) {
        while( (*pConnection)!=nil ) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }
}

#pragma mark - XMS Event Query
- (void)postCommand:(NSString *)cmd connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously withToken:(NSString *)token withSequence:(NSInteger)seq count:(NSInteger)numberOfData eventType:(NSString *)vtype
{
    NSURL *urlString = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",host,cmd]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlString];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:10.0f];
    
    // set headers
    NSString *contentType = [NSString stringWithFormat:@"application/json"];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    // set body
    NSDictionary *req_body = [NSDictionary new];
    req_body = [NSDictionary dictionaryWithObjectsAndKeys:@(seq), @"Sequence", @(numberOfData), @"Count", vtype, @"Type", nil];
    
    NSError *error;
    NSData *postBody = [NSJSONSerialization dataWithJSONObject:req_body options:0 error:&error];
    [request setHTTPBody:postBody];
    
    DBGLog(@"[EV Query POST] Request:%@",urlString);
    
    // connect
    *pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    
    // wait for connection complete
    if (synchronously) {
        while( (*pConnection)!=nil ) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }
}

#pragma mark - ValueIP 3M Ptz Command
- (void)post3MCommand:(NSString *)cmd dictionary:(NSDictionary *)cmdDict connection:(NSURLConnection **)pConnection synchronously:(BOOL)synchronously
{
    NSURL *urlString = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",host,cmd]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlString];
    [request setHTTPMethod:@"POST"];
    [request setTimeoutInterval:10.0f];
    
    // set headers
    NSString *contentType = [NSString stringWithFormat:@"application/json"];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSError *error;
    NSData *postBody = [NSJSONSerialization dataWithJSONObject:cmdDict options:0 error:&error];
    [request setHTTPBody:postBody];
    
    DBGLog(@"[3M POST] Request:%@",urlString);
    
    // connect
    *pConnection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    
    // wait for connection complete
    if (synchronously) {
        while( (*pConnection)!=nil ) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }
}

#pragma mark - Response
- (NSString *)getResult:(NSString *)xml {
	
	NSXMLParser *parser = [[[NSXMLParser alloc] initWithData:[xml dataUsingEncoding:NSUTF8StringEncoding]] autorelease];
	
	[parser setDelegate:self];
	[parser parse];
    
    return [result copy];
}

- (BOOL)parseXmlResponse:(NSString *)xml {
    
    xml = [xml stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    xml = [xml stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    xml = [xml stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    xml = [xml stringByReplacingOccurrencesOfString:@"  " withString:@""];
	NSXMLParser *parser = [[[NSXMLParser alloc] initWithData:[xml dataUsingEncoding:NSUTF8StringEncoding]] autorelease];
	[parser setDelegate:delegate];
    
	return [parser parse];
}

- (void)dealloc {
	
    SAVE_FREE(host);
    SAVE_FREE(currentXmlElement);
    SAVE_FREE(result);
    
	[super dealloc];
}

#pragma mark - Parser Delegate

// Start of element
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    SAVE_FREE(currentXmlElement);
	currentXmlElement = [elementName copy];
}

// Found Character
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSMutableString *)string
{
	if ([currentXmlElement isEqualToString:@"result"] || [currentXmlElement isEqualToString:@"statusString"]) {  //P2:result PSIA:statusCode
		
        SAVE_FREE(result);
		result = [string copy];
	}
}

// End Element
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
{
}

@end
