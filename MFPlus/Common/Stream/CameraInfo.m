//
//  CameraInfo.m
//  EFViewerHD
//
//  Created by James Lee on 12/10/11.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "CameraInfo.h"

@implementation CameraInfo

@synthesize index,title,fps,install,covert,baudrate,ptzID,ptzType,strDeviceID,parent,relations,totalChannel;
@synthesize rtspPort,httpPort,ipAddr,width,height,codecType,enable,status,type,deviceType,channelTitle,portForward,forwardPort,dev_user,dev_password;

//------------------------------------ 20151120 added by Ray Lin, for xms support vehicle tree view test ------------------------------------//
@synthesize child, path, channelIDs;
@synthesize numberOfSubitems;
@synthesize parentSelectingItem;
@synthesize ancestorSelectingItems;
@synthesize submersionLevel;
@synthesize selected;

- (BOOL)isEqual:(id)other {
    if (other == self)
        return YES;
    if (!other || ![other isKindOfClass:[self class]])
        return NO;
    return [self isEqualToSelectingItem:other];
}

- (BOOL)isEqualToSelectingItem:(CameraInfo *)selectingItem {
    if (self == selectingItem)
        return YES;
    
    if ([title isEqualToString:selectingItem.title])
        if ([path isEqualToString:selectingItem.path])
            if (numberOfSubitems == selectingItem.numberOfSubitems)
            return YES;
    
    return NO;
}
//------------------------------------ 20151120 added by Ray Lin, for xms support vehicle tree view test ------------------------------------//



- (void)dealloc
{
    MMLog(@"[CameraInfo] dealloc %@",self);

//    if (self.title) {
//        [self.title release];
//        self.title = nil;
//    }
    
    if (self.ptzType) {
        [self.ptzType release];
        self.ptzType = nil;
    }
    
    if (self.codecType) {
        [self.codecType release];
        self.codecType = nil;
    }
    
    [super dealloc];
}

@end

@implementation SearchResultEntry

@synthesize uiStartTime,uiEndTime,uiEventType,uiChannel;

- (void)dealloc
{
    MMLog(@"[SearchEntry] dealloc %@",self);
    [super dealloc];
}

@end
