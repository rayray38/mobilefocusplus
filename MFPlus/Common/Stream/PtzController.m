//
//  PtzController.m
//  EFViewer
//
//  Created by James Lee on 2010/8/26.
//  Copyright 2010 EverFocus. All rights reserved.
//

#import "PtzController.h"


@implementation PtzController

@synthesize host,sessionId,ptzType,tokenKey;
@synthesize speed,baudrate,blnNcb,blnDyna18X;

#pragma mark -
#pragma mark Initialization Command

- (id)initWithHost:(NSString *)addr sid:(NSString *)sid toKen:(NSString *)_tokenKey
{
	if ((self = [super init])) {
		
		self.host = addr;
		self.sessionId = sid;
		
		cmdSender = [[CommandSender alloc] initWithHost:addr delegate:self];
        self.blnNcb = NO;
        self.blnDyna18X = NO;
        self.ptzType = nil;
        self.baudrate = 0;
        self.tokenKey = _tokenKey;
	}
	
	return self;
}

- (void)dealloc {
	
	[self cancelConnection];
	
    SAVE_FREE(host);
    SAVE_FREE(sessionId);
    SAVE_FREE(ptzType);
    SAVE_FREE(cmdConnection);
    SAVE_FREE(cmdSender);
	
	[super dealloc];
}

#pragma mark -
#pragma mark CGI Command

- (void)ptzTiltDown:(NSInteger)ch {
}

- (void)ptzTiltUp:(NSInteger)ch {
}

- (void)ptzPanLeft:(NSInteger)ch {
}

- (void)ptzPanRight:(NSInteger)ch {
}

- (void)ptzZoomIn:(NSInteger)ch {
}

- (void)ptzZoomOut:(NSInteger)ch {
}

- (void)ptzZoomStop:(NSInteger)ch {
}

- (void)ptzFocusFar:(NSInteger)ch {
}

- (void)ptzFocusNear:(NSInteger)ch {
}

- (void)ptzFocusStop:(NSInteger)ch {
}

- (void)ptzIrisOpen:(NSInteger)ch {
}

- (void)ptzIrisClose:(NSInteger)ch {
}

- (void)ptzStop:(NSInteger)ch {
}

- (void)ptzPresetGo:(NSInteger)ch value:(NSInteger)value {
}

- (void)ptzPresetSet:(NSInteger)ch value:(NSInteger)value {
}

- (void)ptzPresetCancel:(NSInteger)ch value:(NSInteger)value {
}

- (void)ptzAutoPanRun:(NSInteger)ch {
}

- (void)ptzAutoPan360Run:(NSInteger)ch{    
}

- (void)ptzAutoPanStop:(NSInteger)ch {
}

- (void)ptzPattern:(NSInteger)ch value:(NSInteger)value{
}

- (void)ptzTour:(NSInteger)ch value:(NSInteger)value{
}

- (void)ptzOsdOpen:(NSInteger)ch {
}

- (void)ptzOsdExit:(NSInteger)ch {
}

- (void)ptzLeftUp:(NSInteger)ch {
}

- (void)ptzLeftDown:(NSInteger)ch {
}

- (void)ptzRightUp:(NSInteger)ch {
}

- (void)ptzRightDown:(NSInteger)ch {
}

- (void)ptzAutoTracking:(NSInteger)ch on:(BOOL)on {
}


#pragma mark -
#pragma mark Network Connection

- (void)cancelConnection
{
	if (cmdConnection != nil) {
		[cmdConnection cancel];
		cmdConnection = nil;
	}
}

#pragma mark -
#pragma mark Connection Delegate

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.
{
	NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];	
	NSLog(@"[PTZ] Receive: %@",result);
	[result release];
}

- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error
// A delegate method called by the NSURLConnection if the connection fails. 
{
	if (theConnection == cmdConnection)
	{
		cmdConnection = nil;
	}
	
	NSLog(@"[PTZ] Error: %@", [error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)theConnection
// A delegate method called by the NSURLConnection when the connection has been done successfully.
{
	if (theConnection == cmdConnection)
	{
		cmdConnection = nil;
	}
}


@end
