//
//  VideoControl.h
//  EFViewerHD
//
//  Created by James Lee on 12/10/12.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VideoDecoder.h"
#import "VideoDisplayView.h"
#import "RecordControl.h"

#define PKG_BUFFER_SIZE	800
#define MAX_BLOCK_FRAME	30
#define MAX_CH_NUMBER   32
#define PS_LIMIT_COUNT  4

@interface VideoControl : NSObject
{
    VideoDecoder        *decoders[MAX_SUPPORT_DISPLAY];
    unsigned long       sequences[MAX_SUPPORT_DISPLAY];
    VIDEO_PACKAGE       pkgBuffers[MAX_SUPPORT_DISPLAY][PKG_BUFFER_SIZE];
    NSTimeInterval      preFrameTimes[MAX_SUPPORT_DISPLAY];
    int                 preFrameMses[MAX_SUPPORT_DISPLAY];
    int                 pkgAmounts[MAX_SUPPORT_DISPLAY];
    int                 iFrameAmounts[MAX_SUPPORT_DISPLAY];
    int                 writePoses[MAX_SUPPORT_DISPLAY];
    int                 readPoses[MAX_SUPPORT_DISPLAY];
    
    NSDateFormatter     *dateFormatterGMTAware;
    NSMutableArray      *aryDisplayViews;                   //add by robert hsu 20120102
    NSMutableArray      *aryTitle;                          //add by robert hsu 20120104
    NSTimeInterval      tmpTime;
    
    NSUInteger          outputMask;
    NSInteger           selectWindow;
    BOOL                blnIsStop[MAX_SUPPORT_DISPLAY];           //record channel status
    BOOL                blnIsDecoding[MAX_SUPPORT_DISPLAY];
    BOOL                blnDrawFinished[MAX_SUPPORT_DISPLAY];
    BOOL                blnClearFinish[MAX_SUPPORT_DISPLAY];
    BOOL                blnIsPause[MAX_SUPPORT_DISPLAY];
    
    NSInteger           renderFpsCount;
    NSTimer             *fpsTimer;
    NSInteger           streamFpsCount[MAX_SUPPORT_DISPLAY];
    time_value_t        streamLastTick[MAX_SUPPORT_DISPLAY];
    
    RecordControl       *recCtrl;
    NSInteger           dropLevel;
    NSInteger           activeViewCount;
}

- (id)initWithVideoDisplayViews:(NSMutableArray *)aryVideoViews;
- (void)start;
- (void)startByCH:(NSInteger)chIdx;
- (void)stop;
- (void)stopByCH:(NSInteger)chIdx;
- (void)pause;
- (void)pauseByCH:(NSInteger)chIdx;
- (void)resume;
- (void)resumeByCH:(NSInteger)chIdx;
- (void)reset;
- (void)resetByCH:(NSInteger)chIdx;
- (void)refresh;
- (void)refreshByCH:(NSInteger)chIdx;
- (void)decodeThreadByCH:(id)chStr;
- (void)decodeAndShowFrameByCH:(VIDEO_PACKAGE *)pFrame chIndex:(NSInteger)chIdx;
- (void)refreshVideoDisplayView:(NSDictionary *)info;
- (NSInteger)getVideoWidthbyCH:(NSInteger)chIdx;     //add by James Lee 20120402
- (NSInteger)getVideoHeightbyCH:(NSInteger)chIdx;    //add by James Lee 20120402
- (BOOL)putFrameIntoBufferByCH:(VIDEO_PACKAGE*)pFrame chIndex:(NSInteger)chIdx;
- (BOOL)getframeByChannel:(VIDEO_PACKAGE *)pFrame chIndex:(NSInteger)chIdx;
- (BOOL)getNextIFrameByCH:(VIDEO_PACKAGE *)pFrame chIndex:(NSInteger)chIdx;
- (BOOL)getLastIFrameByCH:(VIDEO_PACKAGE *)pFrame chIndex:(NSInteger)chIdx;
- (void)setTitleByCH:(NSInteger)chIdx title:(NSString *)strTitle;
- (void)cleanBufferByChannel:(NSInteger)chIdx;
- (BOOL)getVCStatusByCH:(NSInteger)chIdx;
- (void)setPlayingChannelByCH:(NSInteger)chIdx Playing:(NSInteger)playingCh;
- (void)fpsLog;
- (void)setResolution:(NSInteger)mode;
- (AVCodecContext *)getCodecCtxByCH:(NSInteger)chIdx;
- (NSInteger)getFpsByCH:(NSInteger)chIdx;

- (void)setSubWindowWithView:(VideoDisplayView *)_view byIndex:(NSInteger)vIdx;
- (void)removeSubWindow;

@property(nonatomic,retain)   NSMutableArray      *aryTitle;
@property(nonatomic,retain)   NSMutableArray      *aryDisplayViews;
@property(nonatomic)          NSUInteger          outputMask;
@property(nonatomic)          NSInteger           selectWindow;
@property(nonatomic,retain)   RecordControl       *recCtrl;
@property(nonatomic)          BOOL                blnPSLimit;

@end
