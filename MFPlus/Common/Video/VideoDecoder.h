//
//  VideoDecoder.h
//  EFViewerHD
//
//  Created by James Lee on 12/10/9.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "libavformat/avformat.h"
#include "libswscale/swscale.h"

typedef enum : NSUInteger
{
    erm_hd = 0x100,
    erm_d1,
    erm_cif,
    erm_qcif,
} EResolutionMode;

typedef enum : NSUInteger
{
    nalu_type_slice = 1,
    nalu_type_dpa,
    nalu_type_dpb,
    nalu_type_dpc,
    nalu_type_idr,
    nalu_type_sei,
    nalu_type_sps,
    nalu_type_pps,
    nalu_type_aud,
    nalu_type_eoseq,
    nalu_type_eostream,
    nalu_type_fill
} ENaluType;


@interface VideoDecoder : NSObject {
    
	UIImage *currentImage;
	
	AVCodecContext *pCodecCtx;

	AVFrame *pFrame;
	AVPicture picture;
	struct SwsContext *img_convert_ctx;
	
	int codecId;
	int sourceWidth, sourceHeight;
	int outputWidth, outputHeight;
    NSInteger resolutionMode;
    
    NSInteger getHeight;
    NSInteger getWidth;
    NSMutableData    *jpegData;
    
    BOOL      bResizeSetup;
}

- (id)initWithCodecId:(unsigned int)cid;
- (BOOL)decodeFrame:(Byte *)pInFrame length:(int)length;
- (void)setOutputWidth:(int)newW Height:(int)newH;
- (void)setResolution:(NSInteger)_mode;
- (AVCodecContext *)getCodecCtx;

@property(nonatomic,readonly) int		codecId;
@property(nonatomic,readonly) UIImage	*currentImage;
@property(nonatomic,readonly) int		sourceWidth, sourceHeight;
@property(nonatomic) int outputWidth, outputHeight;

@end