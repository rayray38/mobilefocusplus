//
//  VideoDecoder.m
//  EFViewerHD
//
//  Created by James Lee on 12/10/9.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "VideoDecoder.h"

@interface VideoDecoder (private)
-(void)convertFrameToRGB;
-(UIImage *)imageFromAVPicture:(AVPicture)pict width:(int)width height:(int)height;
-(void)setupScaler;

@end


@implementation VideoDecoder

@synthesize codecId,sourceWidth,sourceHeight,outputWidth,outputHeight;


- (id)initWithCodecId:(unsigned int)cid {
	
	if (!(self=[super init])) return nil;
#ifdef MJPEGMODE
    if (cid == CODEC_ID_MJPEG) {    //MJPEG mode : direct render frame ,without initiate decoder. 20130320 James Lee
        codecId = cid;
        jpegData = [[NSMutableData alloc] init];
        return self;
    }
#endif
    AVCodec *pCodec;
    
    // Register all formats and codecs
    av_register_all();
    avcodec_register_all();
    
    // Find the decoder
    pCodec = avcodec_find_decoder(cid);
    if(pCodec==NULL)
        goto initError; // Codec not found
    
    // Allocate codec context
    pCodecCtx = avcodec_alloc_context3(pCodec);
	
    // Open codec
    if(avcodec_open2(pCodecCtx, pCodec, NULL)<0)
        goto initError; // Could not open codec
    
    if(pCodec->capabilities&CODEC_CAP_TRUNCATED)
        pCodecCtx->flags|= CODEC_FLAG_TRUNCATED;
    
    // Allocate video frame
    //pFrame=avcodec_alloc_frame();    //avcodec_alloc_frame is deprecated
    pFrame = av_frame_alloc();
    
    // initial value
    sourceWidth = 0;
    sourceHeight = 0;
    outputWidth = 0;
    outputHeight = 0;
    resolutionMode = erm_cif;
    codecId = pCodecCtx->codec_id;
    bResizeSetup = YES;
    
	return self;
	
initError:
	[self release];
	return nil;
}

- (BOOL)decodeFrame:(Byte *)pInFrame length:(int)length
{
	
#ifdef MJPEGMODE
    if (codecId == CODEC_ID_MJPEG) {
        
        if ((pInFrame[0]==0xFF) && (pInFrame[1]==0xD8) &&
            (pInFrame[length-2]==0xFF) && (pInFrame[length-1]==0xD9)) {
        
            [jpegData setLength:0];
            [jpegData appendBytes:pInFrame length:length];
        
            return 1;
        } else {
            //data error
            NSLog(@"[dec] JpegData Error");
            return 0;
        }
    }
#endif
    
    int frameFinished = 0;
    AVPacket avpkt;
    av_init_packet(&avpkt);
    avpkt.size = length;
    avpkt.data = (uint8_t *)pInFrame;
    avpkt.flags |= AV_PKT_FLAG_KEY;
    
    avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &avpkt);
    av_free_packet(&avpkt);
    
    // Set scaler
    if ( frameFinished!=0 ) {
        
        if ( pCodecCtx->width!=sourceWidth || pCodecCtx->height!=sourceHeight ) {
            
            sourceWidth = pCodecCtx->width;
            sourceHeight = pCodecCtx->height;
            [self setResolution:resolutionMode];
            bResizeSetup = YES;
        }
        
        if (!img_convert_ctx)
            [self setupScaler];
        
        if (pCodecCtx->extradata_size == 0) {
            // 這邊先用EF的格式下去算，之後要改動態抓大小
            pCodecCtx->extradata_size = 21;
            pCodecCtx->extradata = malloc(sizeof(uint8_t)*pCodecCtx->extradata_size);
            memcpy(pCodecCtx->extradata, pInFrame, pCodecCtx->extradata_size);
        }
    }
    
	return frameFinished!=0;
}

- (void)setOutputWidth:(int)newW Height:(int)newH
{
    
    if (outputWidth == newW) return;
    if (outputHeight == newH) return;
	
    outputWidth = newW;
    outputHeight = newH;
    bResizeSetup = YES;
}

- (void)setResolution:(NSInteger)_mode
{
    switch (_mode) {
        case erm_qcif:
            outputWidth = 176;
            outputHeight = 120;
            break;
        case erm_cif:
            outputWidth = 352;
            outputHeight = 240;
            break;
        case erm_d1:
            outputWidth = 704;
            outputHeight = 480;
            break;
        case erm_hd:
            outputWidth = 1280;
            outputHeight = 720;
            break;
    }
    
    if (pCodecCtx->width && pCodecCtx->height)
        outputHeight = (outputWidth * pCodecCtx->height) / pCodecCtx->width;

    resolutionMode = _mode;
    //NSLog(@"[dec] Resolution:%x, W/H:%d/%d",resolutionMode,outputWidth,outputHeight);
    bResizeSetup = YES;
}

/*
 Parm :
 Return :UIImage *
 purpose :
 1. return current frame's image
 2. if codec type is MJPEG then create a UIImage with that frame and return
 3. if codec type not MJPEG need to convert decode data to RGB then call [self imageFormAVPicture ]
 to create a UIImage then return.
 */
- (UIImage *)currentImage
{
#ifdef MJPEGMODE
    if (codecId == CODEC_ID_MJPEG) {
        
        UIImage *tmpImg = [UIImage imageWithData:jpegData];
        if ((sourceWidth != tmpImg.size.width) || (sourceHeight != tmpImg.size.height)) {
            
            sourceWidth = tmpImg.size.width;
            sourceHeight = tmpImg.size.height;
        }
        
        return tmpImg;
    }
#endif
    if (!pFrame->data[0]) return nil;
    
    if (img_convert_ctx == nil) {
        return nil;
    }
    
    [self convertFrameToRGB];
    
    return [self imageFromAVPicture:picture width:outputWidth height:outputHeight];
}

- (void)convertFrameToRGB {
	
    //NSLog(@"[dec] convertFrameToRGB Width :%d ,height : %d " ,pCodecCtx->width ,pCodecCtx->height);
    if (bResizeSetup)
        [self setupScaler];
    
    if(getHeight == pCodecCtx->height) {
        
        sws_scale (img_convert_ctx, (const uint8_t* const*)pFrame->data, pFrame->linesize,
                   0, pCodecCtx->height,
                   picture.data, picture.linesize);
    }
}

- (UIImage *)imageFromAVPicture:(AVPicture)pict width:(int)width height:(int)height {
	
	CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
	CFDataRef data = CFDataCreateWithBytesNoCopy(kCFAllocatorDefault, pict.data[0], pict.linesize[0]*height,kCFAllocatorNull);
	CGDataProviderRef provider = CGDataProviderCreateWithCFData(data);
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGImageRef cgImage = CGImageCreate(width,
									   height,
									   8,
									   24,
									   pict.linesize[0],
									   colorSpace,
									   bitmapInfo,
									   provider,
									   NULL,
									   NO,
									   kCGRenderingIntentDefault);
	CGColorSpaceRelease(colorSpace);
	UIImage *image = [UIImage imageWithCGImage:cgImage];
	CGImageRelease(cgImage);
	CGDataProviderRelease(provider);
	CFRelease(data);
	
	return image;
}

-(void)setupScaler {
	
	// Release old picture and scaler
	avpicture_free(&picture);
	sws_freeContext(img_convert_ctx);
	
	// Allocate RGB picture
	avpicture_alloc(&picture, AV_PIX_FMT_RGB24, outputWidth, outputHeight);
	
    if (pCodecCtx->width == pCodecCtx->height) //Special case for fisheye
        outputWidth = outputHeight;
    
	// Setup scaler
	static int sws_flags = SWS_FAST_BILINEAR;
    NSLog(@"[dec %@] Resize - W:%d/%d, H:%d/%d",self,pCodecCtx->width,outputWidth,pCodecCtx->height,outputHeight);
    getHeight = pCodecCtx->height;
    
    enum AVPixelFormat pixFormat;
    switch (pCodecCtx->pix_fmt) {
        case AV_PIX_FMT_YUVJ420P :
            pixFormat = AV_PIX_FMT_YUV420P;
            break;
        case AV_PIX_FMT_YUVJ422P  :
            pixFormat = AV_PIX_FMT_YUV422P;
            break;
        case AV_PIX_FMT_YUVJ444P   :
            pixFormat = AV_PIX_FMT_YUV444P;
            break;
        case AV_PIX_FMT_YUVJ440P :
            pixFormat = AV_PIX_FMT_YUV440P;
        default:
            pixFormat = pCodecCtx->pix_fmt;
            break;
    }
    
    pCodecCtx->pix_fmt = pixFormat;
    
	img_convert_ctx = sws_getContext(pCodecCtx->width,
									 pCodecCtx->height,
									 pCodecCtx->pix_fmt,
									 outputWidth,
									 outputHeight,
									 AV_PIX_FMT_RGB24,
									 sws_flags, NULL, NULL, NULL);
    
    bResizeSetup = NO;
}

- (AVCodecContext *)getCodecCtx
{
    return pCodecCtx;
}

- (void)dealloc {

    // Free scaler
    sws_freeContext(img_convert_ctx);
    img_convert_ctx = NULL;
    
    // Free RGB picture
    avpicture_free(&picture);
    
    // Free the YUV frame
    av_free(pFrame);
    
    // Close the codec
    if (pCodecCtx) {
        // Free extendData
        if (pCodecCtx->extradata_size)
            free(pCodecCtx->extradata);
        
        avcodec_close(pCodecCtx);
        av_free(pCodecCtx);
    }
    
    // release jpegData
    if (jpegData) {
        
        [jpegData release];
        jpegData = nil;
    }
    
	[super dealloc];
}

@end
