//
//  VideoControl.m
//  EFViewerHD
//
//  Created by James Lee on 12/10/12.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "VideoControl.h"

@interface VideoControl()
{
    NSInteger        m_nResolution;
    VideoDisplayView *subWindow;
    NSInteger        m_nSubWindowIdx;
    BOOL             m_bSubWindowStatus;    // YES:pause
}
@end

@implementation VideoControl

@synthesize aryTitle,aryDisplayViews,outputMask,selectWindow;
@synthesize recCtrl,blnPSLimit;

#pragma mark - Initializtion

-(id)initWithVideoDisplayViews:(NSMutableArray *)aryVideoViews
{
    if(!(self=[super init])) return nil;
    
    aryDisplayViews = aryVideoViews;     //MutableArray for contain VideoDisplayView Class
    
    for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
        
        blnIsStop[i] = YES;
        blnIsDecoding[i] = NO;
        blnDrawFinished[i] = YES;
        blnClearFinish[i] = YES;
        blnIsPause[i] = YES;
    }
    dateFormatterGMTAware = [[NSDateFormatter alloc] init];
    [dateFormatterGMTAware setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateFormatterGMTAware setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    tmpTime = 0;
    activeViewCount = 0;
    m_nSubWindowIdx = -1;
    aryTitle = [[NSMutableArray alloc] init];
    
    NSString *strTmp;
    for(NSInteger i = 0 ; i < MAX_SUPPORT_DISPLAY ; i++)
    {
        strTmp = [[NSString alloc] init];
        [aryTitle addObject:strTmp];
        [strTmp release];
    }

    if (fpsTimer == nil) {
        fpsTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                    target:self
                                                  selector:@selector(fpsLog)
                                                  userInfo:nil
                                                   repeats:YES]; //Test FPS
    }
    MMLog(@"[VC] Init %@",self);
    return self;
}

#pragma mark - Action Control

- (void)start
{
    for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
        
        [self startByCH:i];
    }
}

- (void)startByCH:(NSInteger)chIdx
{
    [self resetByCH:chIdx];
    outputMask |= 0x1<<chIdx;
    blnIsStop[chIdx] = NO;
    VideoDisplayView *tmpDispView = [aryDisplayViews objectAtIndex:chIdx];
    tmpDispView.blnIsPlay = YES;
    [tmpDispView startActivityIndicatorAnimating];
    [tmpDispView.infoLabel setHidden:NO];
    [tmpDispView.lblTitle setHidden:NO];
    
    NSString *chStr = [NSString stringWithFormat:@"%ld",(long)chIdx];
    [NSThread detachNewThreadSelector:@selector(decodeThreadByCH:)
                             toTarget:self
                           withObject:chStr];
    [self resumeByCH:chIdx];
}

- (void)stop
{
    for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
        
        [self stopByCH:i];
    }
}

- (void)stopByCH:(NSInteger)chIdx
{
    @synchronized (self) {
        
        if (blnIsStop[chIdx] == YES) {
            return;
        }
        
        blnIsStop[chIdx] = YES;
        [self pauseByCH:chIdx];
        outputMask ^= 0x1<<chIdx;
        
        streamLastTick[chIdx].seconds = 0;
        streamLastTick[chIdx].microseconds = 0;
        streamFpsCount[chIdx] = 0;
        
        while (blnIsDecoding[chIdx])
            [NSThread sleepForTimeInterval:0.5];
        
        [self cleanBufferByChannel:chIdx];
        
        while (!blnDrawFinished[chIdx])
            [NSThread sleepForTimeInterval:0.5];
        
        SAVE_FREE(decoders[chIdx]);
        
        VideoDisplayView *tmpView = [aryDisplayViews objectAtIndex:chIdx];
        tmpView.blnIsPlay = NO;
        tmpView.playingChannel = EMPTY_CHANNEL;
        [tmpView stopActivityIndicatorAnimating];
        //[tmpView performSelectorOnMainThread:@selector(cleanView) withObject:self waitUntilDone:YES];
        [tmpView cleanView];
        [tmpView.infoLabel setHidden:YES];
        [tmpView.lblTitle setHidden:YES];
        
        VCLog(@"[VC %d] stop",chIdx);
        //NSLog(@"[VC %ld] stop",(long)chIdx);
        
        NSLog(@"[VC %ld] stop , playingChannel:%ld",(long)chIdx,(long)[aryDisplayViews[chIdx] playingChannel]);
    }
}

- (void)pause
{
    for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
        
        [self pauseByCH:i];
    }
}

- (void)pauseByCH:(NSInteger)chIdx
{
    if (!(outputMask & 0x1<<chIdx)) return;
    if (blnIsPause[chIdx]) return;
    
    blnIsPause[chIdx] = YES;
    activeViewCount--;
    VCLog(@"[VC %d] pause",chIdx);
    //NSLog(@"[VC %ld] pause",(long)chIdx);
}

- (void)resume
{
    for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
        
        [self resumeByCH:i];
    }
}

- (void)resumeByCH:(NSInteger)chIdx
{
    if (!(outputMask & 0x1<<chIdx)) return;
    if (!blnIsPause[chIdx]) return;
    
    //[self resetByCH:chIdx];
    blnIsPause[chIdx] = NO;
    activeViewCount++;
    VCLog(@"[VC %d] resume",chIdx);
    //NSLog(@"[VC %ld] resume",(long)chIdx);
}

- (void)reset
{
    for(NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++)
    {
        if (outputMask & 0x1<<i) { // only reset the using videoViews
            
            [self resetByCH:i];
        }
    }
}

- (void)resetByCH:(NSInteger)chIdx
{
	blnClearFinish[chIdx] = NO;
    sequences[chIdx] = 0;
    [self cleanBufferByChannel:chIdx];
    
    VideoDisplayView *tmpView = [aryDisplayViews objectAtIndex:chIdx];
    //tmpView.playingChannel = 99;  //!!!!!!!在HD裡會出問題
    [tmpView cleanView];
    if (![tmpView isIndicatorAnimating] && tmpView.playingChannel != 99)
        [tmpView startActivityIndicatorAnimating];
    
	blnClearFinish[chIdx] = YES;

	VCLog(@"[VC %d] reset",chIdx);
    NSLog(@"[VC %ld] reset , playingChannel:%ld",(long)chIdx,(long)[aryDisplayViews[chIdx] playingChannel]);
}

- (void)refresh
{
    for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
        [self refreshByCH:i];
    }
}

- (void)refreshByCH:(NSInteger)chIdx
{
    VideoDisplayView *tmpView = [aryDisplayViews objectAtIndex:chIdx];
    [tmpView cleanView];
}

- (void)fpsLog
{
//    NSLog(@"[VC] Total FPS:%ld",(long)renderFpsCount);
    NSInteger avgFps = renderFpsCount/3;
    NSInteger tmpDrop = dropLevel;
    
    if (avgFps > 300)
        tmpDrop = 30;
    else if (avgFps > 120)
        tmpDrop = 10;
//    else if (avgFps > 90)
//        tmpDrop = 3;
    else if (avgFps < 50)
        tmpDrop = 1;
    
    if (tmpDrop != dropLevel) {
        VCLog(@"[VC] Drop Level Change:%d - FPS:%d",tmpDrop,avgFps);
        dropLevel = tmpDrop;
    }
    renderFpsCount = 0;
}

#pragma mark - Buffer Management

- (void)ringAdd:(int *)pIdx {
	
	*pIdx = ((*pIdx+1) == PKG_BUFFER_SIZE ? 0 : *pIdx+1);
}

- (BOOL)putFrameIntoBufferByCH:(VIDEO_PACKAGE *)pFrame chIndex:(NSInteger)chIdx
{
    @synchronized(self)
    {//lock when put frame into frame buffer
        //NSLog(@"[VC %d] putFrameSize:%lu",chIdx, pFrame->size);
        
        // 1.counting FPS of streaming
        if (streamLastTick[chIdx].seconds != 0) {
            streamFpsCount[chIdx] = 1000/((pFrame->sec-streamLastTick[chIdx].seconds)*1000 + (pFrame->ms-streamLastTick[chIdx].microseconds));
            //NSLog(@"[VC %d] FPS:%d, %d%d/%lu%d",chIdx,streamFpsCount[chIdx],streamLastTick[chIdx].seconds,streamLastTick[chIdx].microseconds,pFrame->sec,pFrame->ms);
        }
        streamLastTick[chIdx].seconds = pFrame->sec;
        streamLastTick[chIdx].microseconds = pFrame->ms;
        
        // 2.write the Recording frame
        if (recCtrl.blnRecording && chIdx==recCtrl.recView) {
            [recCtrl writeVideoFrame:pFrame];
        }
        
        // 3.when render pause, do not add the frame into buffer
        if (blnIsPause[chIdx]) {
            SAVE_FREEM(pFrame->data);
            return NO;
        }

        // 4.when Limit Mode active, pass all p frame
        if (activeViewCount>=PS_LIMIT_COUNT && blnPSLimit && pFrame->type == VO_TYPE_PFRAME) {
            SAVE_FREEM(pFrame->data);
            return NO;
        }

        if(pkgAmounts[chIdx] < PKG_BUFFER_SIZE)
        {
            pkgBuffers[chIdx][writePoses[chIdx]] = *pFrame;
            
            if(pkgBuffers[chIdx][writePoses[chIdx]].type == VO_TYPE_IFRAME)
            {
                iFrameAmounts[chIdx]++;
            }
            
            [self ringAdd:&writePoses[chIdx]];
            pkgAmounts[chIdx]++;
            
            return YES;
        }
        else
        {
            VCLog(@"[VC %d] buffer is full",chIdx);
            return NO;
        }
    }
}

- (BOOL)getframeByChannel:(VIDEO_PACKAGE *)pFrame chIndex:(NSInteger)chIdx
{
    @synchronized(self) {//comment by robert hsu 20110102 ,use for lock
        
        if (pkgAmounts[chIdx]>0) {
            
            *pFrame = pkgBuffers[chIdx][readPoses[chIdx]];
            
            if (pkgBuffers[chIdx][readPoses[chIdx]].type==VO_TYPE_IFRAME)
            {
                iFrameAmounts[chIdx]--;
            }
            
            [self ringAdd:&readPoses[chIdx]];
            pkgAmounts[chIdx]--;
            //VCLog(@"[VO] get video package");
            return YES;
        }
        else { // buffer is empty
            //NSLog(@"[VC %ld] buffer empty",(long)chIdx);
            return NO;
        }
    }
}

- (BOOL)getNextIFrameByCH:(VIDEO_PACKAGE *)pFrame chIndex:(NSInteger)chIdx
{
    if(iFrameAmounts[chIdx] == 0) return NO;
    
    [self getframeByChannel:pFrame chIndex:chIdx];
    
    while(pFrame->type != VO_TYPE_IFRAME)
    {
        SAVE_FREEM(pFrame->data);
        [self getframeByChannel:pFrame chIndex:chIdx];
    }
    
    VCLog(@"[VC %d] jump to next I-frame",chIdx);
    return YES;
}

- (BOOL)getLastIFrameByCH:(VIDEO_PACKAGE *)pFrame chIndex:(NSInteger)chIdx
{
    if(iFrameAmounts[chIdx] == 0) return NO;
    
    [self getframeByChannel:pFrame chIndex:chIdx];
    
    while(iFrameAmounts[chIdx] > 0)
    {
        SAVE_FREEM(pFrame->data);
        [self getframeByChannel:pFrame chIndex:chIdx];
    }
    
    VCLog(@"[VC %d] jump to last I-frame",chIdx);
    return YES;
}

- (void)cleanBufferByChannel:(NSInteger)chIdx
{
    VIDEO_PACKAGE frame;
	
	while (pkgAmounts[chIdx]>0)
    {
        BOOL blnFrame = NO;
        
        blnFrame = [self getframeByChannel:&frame chIndex:chIdx];
        if(blnFrame)
        {
            SAVE_FREEM(frame.data);
        }
        else
        {
            VCLog(@"[VC %d] GetFrameFail",chIdx);
        }
	}
}

#pragma mark - Decode Process

- (void)decodeThreadByCH:(id)chStr
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init]; // thread need an autorelease pool in it
    
    NSInteger chIdx = ((NSString *)chStr).integerValue;
    VCLog(@"[VC %d] start %@",chIdx,[NSThread currentThread]);
    BOOL frameAvailable;
	VIDEO_PACKAGE frame;
	NSTimeInterval frameTimeInterval, minFrameTimeInterval, timeInterval;

    while (!blnIsStop[chIdx]) {
        
        if ( pkgAmounts[chIdx]==PKG_BUFFER_SIZE && iFrameAmounts[chIdx]!=0) { // buffer full -> get last I-frame
            frameAvailable = [self getLastIFrameByCH:&frame chIndex:chIdx];
            preFrameTimes[chIdx] = 0.0; // avoid sleep
        }
        else if ( pkgAmounts[chIdx]>=MAX_BLOCK_FRAME && iFrameAmounts[chIdx]>=MAX_BLOCK_FRAME) { // all I-frame (MJPEG) -> get next count+1 frames
            int count = MAX_BLOCK_FRAME/2;
            while (count>0) {
                [self getframeByChannel:&frame chIndex:chIdx];
                SAVE_FREEM(frame.data);
                count--;
            }
            [self getframeByChannel:&frame chIndex:chIdx];
            VCLog(@"[VC %d] JumpFrames",chIdx);
            preFrameTimes[chIdx] = 0.0; // avoid sleep
        }
        else if ( pkgAmounts[chIdx]>=MAX_BLOCK_FRAME && iFrameAmounts[chIdx]!=0) { // get next I-frame
        
            frameAvailable = [self getNextIFrameByCH:&frame chIndex:chIdx];
            preFrameTimes[chIdx] = 0.0; // avoid sleep
        }
        else {
            frameAvailable = [self getframeByChannel:&frame chIndex:chIdx];
        }
        
        if (frameAvailable)
        {
            if (decoders[chIdx] == nil || decoders[chIdx].codecId != frame.codec) {
                
                if (decoders[chIdx] != nil) {
                    [decoders[chIdx] release];
                    decoders[chIdx] = nil;
                }
                
                decoders[chIdx] = [[VideoDecoder alloc] initWithCodecId:frame.codec];
                VCLog(@"[VC %d] Init Decoder",chIdx);
                [decoders[chIdx] setResolution:m_nResolution];
            }
            
            //20151117 modified by Ray Lin, 因為某些機種P_Frame裡帶的sequence沒有+1導致P_Frame沒被解碼到
            //if ( frame.seq==(sequences[chIdx]+1) || frame.type==VO_TYPE_IFRAME ) {
            if ( frame.type==VO_TYPE_IFRAME || frame.type==VO_TYPE_PFRAME ) {
                
                //NSLog(@"[VC %ld] Frame:%c\n type:%d\n sec:%lu\n ms:%hu\n seq:%lu\n channel:%lu",(long)chIdx, frame.codec, frame.type, frame.sec, frame.ms, frame.seq,frame.channel);
                
                // control time interval
                if (frame.ms != 0xFF) { //有些機種沒有帶ms
                    frameTimeInterval = (float)(frame.ms-preFrameMses[chIdx])/1000;
                    frameTimeInterval = frameTimeInterval>0.0?frameTimeInterval:frameTimeInterval+1.0;
                    minFrameTimeInterval = frameTimeInterval*0.8;
                    timeInterval = [NSDate timeIntervalSinceReferenceDate]-preFrameTimes[chIdx];
                    
                    if ( timeInterval < minFrameTimeInterval )
                        [NSThread sleepForTimeInterval:(minFrameTimeInterval-timeInterval)];
                    
                    preFrameTimes[chIdx] = [NSDate timeIntervalSinceReferenceDate];
                    preFrameMses[chIdx]	 = frame.ms;
                }
                
                [self decodeAndShowFrameByCH:&frame chIndex:chIdx];
            }
            else {
                VCLog(@"[VC %ld] SkipFrame:%lu/%lu type:%d",(long)chIdx, frame.seq, sequences[chIdx], frame.type);
                DBGLog(@"[VC %ld] SkipFrame:%lu/%lu type:%d",(long)chIdx, frame.seq, sequences[chIdx], frame.type);
            }
            
            SAVE_FREEM(frame.data);
        }
    }
    
    [pool release];
}

- (void)decodeAndShowFrameByCH:(VIDEO_PACKAGE *)pFrame chIndex:(NSInteger)chIdx
{
    
    if(blnIsStop[chIdx]) return;

    while (blnIsDecoding[chIdx]) {
        [NSThread sleepForTimeInterval:0.01f];
    }
    
    blnIsDecoding[chIdx] = YES;
    
    // decode and show
	if ( [decoders[chIdx] decodeFrame:pFrame->data length:pFrame->size] ) {
        //NSLog(@"decode time:%f type:%d",[NSDate timeIntervalSinceReferenceDate]-tickStart,pFrame->type);
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:pFrame->sec];
		NSString *tmpTitle = [aryTitle objectAtIndex:chIdx];
        NSString *strtmpTime = [NSString stringWithFormat:@"%@",[dateFormatterGMTAware stringFromDate:date]];
        
		// must refresh view on main thread
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:chIdx], @"channel", tmpTitle, @"title",strtmpTime,@"videoTime", nil];
        if (blnDrawFinished[chIdx]) {
            
            if (blnPSLimit || !(pFrame->seq%dropLevel)) {
                [self performSelectorOnMainThread:@selector(refreshVideoDisplayView:) withObject:dict waitUntilDone:NO];
                renderFpsCount++;
            }
        }
		tmpTime = [NSDate timeIntervalSinceReferenceDate];
        
		sequences[chIdx] = pFrame->seq;
    } else {
        NSLog(@"[VC %ld] Decode failed - Time:%lu.%02d, Size:%lu, Type:%d",(long)chIdx,pFrame->sec,pFrame->ms,pFrame->size,pFrame->type);
    }
    
	blnIsDecoding[chIdx] = NO;
}

- (void)refreshVideoDisplayView:(NSDictionary *)info
{
    //NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSInteger chIdx = [[info objectForKey:@"channel"] intValue];
    NSString *strTitle = [info objectForKey:@"title"];
    NSString *strTime = [info objectForKey:@"videoTime"];
    UIImage *image = [decoders[chIdx] currentImage];
    //NSLog(@"[Video Control] imageL%p",image);
    blnDrawFinished[chIdx] = NO;
    
    // popAlarm++
    if (subWindow && (chIdx == m_nSubWindowIdx)) {
        
        [subWindow cleanView];
        [subWindow setInfoText:strTitle Time:strTime];
        [subWindow setVideoImage:image];
    } else {
        
        VideoDisplayView *tmpDispView = [aryDisplayViews objectAtIndex:chIdx];
        
        if (image != nil) {
            [tmpDispView cleanView];
            [tmpDispView stopActivityIndicatorAnimating];
            [tmpDispView setInfoText:strTitle Time:strTime];
            
            if(blnClearFinish[chIdx] == NO)
                tmpDispView.videoImageView = nil;
            
            [tmpDispView setVideoImage:image];
        }
        else {
            //NSLog(@"[VC] CurrentImage Is NULL...");
        }
    }
    
    blnDrawFinished[chIdx] = YES;
    
    tmpTime = [NSDate timeIntervalSinceReferenceDate];
    //[pool release];
}

#pragma mark - Setter/Getter

- (void)setResolution:(NSInteger)mode
{
    for (NSInteger n=0; n<MAX_SUPPORT_DISPLAY; n++)
    {
        if (decoders[n])
            [decoders[n] setResolution:mode];
    }
    m_nResolution = mode;
}

- (void)setSubWindowWithView:(VideoDisplayView *)_view byIndex:(NSInteger)vIdx
{
    subWindow = _view;
    m_nSubWindowIdx = vIdx;
    
    m_bSubWindowStatus = blnIsPause[vIdx];
    if (m_bSubWindowStatus)
        [self resumeByCH:vIdx];
    
    [decoders[vIdx] setResolution:erm_hd];
}

- (void)removeSubWindow
{
    subWindow = nil;
    if (m_bSubWindowStatus)
        [self pauseByCH:m_nSubWindowIdx];
    
    [decoders[m_nSubWindowIdx] setResolution:m_nResolution];
    m_nSubWindowIdx = -1;
}

- (NSInteger)getVideoWidthbyCH:(NSInteger)chIdx
{
    if (decoders[chIdx]) {
		return [decoders[chIdx] sourceWidth];
	}
	else {
		return 0;
	}
}

- (NSInteger)getVideoHeightbyCH:(NSInteger)chIdx
{
    if (decoders[chIdx]) {
		return [decoders[chIdx] sourceHeight];
	}
	else {
		return 0;
	}
}

- (BOOL)getVCStatusByCH:(NSInteger)chIdx
{
    return blnIsStop[chIdx];
}

- (void)setTitleByCH:(NSInteger)chIdx title:(NSString *)strTitle
{
    if (strTitle == NULL)
        return;
    
    VCLog(@"[VC %d] SetTitle:%@",chIdx,strTitle);
    [aryTitle replaceObjectAtIndex:chIdx withObject:strTitle];
}

- (void)setPlayingChannelByCH:(NSInteger)chIdx Playing:(NSInteger)playingCh
{
    VideoDisplayView *tmpView = [self.aryDisplayViews objectAtIndex:chIdx];
    
    if ((playingCh != tmpView.playingChannel) && tmpView.blnIsPlay) {
        NSLog(@"[VC %ld] Set Playing Channel:%ld", (long)chIdx,(long)playingCh);
        tmpView.playingChannel = playingCh;
        //VCLog(@"[VC %d] SetPlayingChannel:%d",chIdx,playingCh);
    }
}
    
- (AVCodecContext *)getCodecCtxByCH:(NSInteger)chIdx
{
    if (!decoders[chIdx])
        return nil;
    
    return [decoders[chIdx] getCodecCtx];
}

- (NSInteger)getFpsByCH:(NSInteger)chIdx
{
    return streamFpsCount[chIdx];
}

#pragma mark - dealloc

- (void)dealloc
{
    MMLog(@"[VC] dealloc %@",self);
    if (fpsTimer) {
        [fpsTimer invalidate];
        fpsTimer = nil;
    }
    
    SAVE_FREE(dateFormatterGMTAware);

    [aryTitle removeAllObjects];
    SAVE_FREE(aryTitle);
    
    for (NSInteger n=0; n<MAX_SUPPORT_DISPLAY; n++)
        SAVE_FREE(decoders[n]);
    
    [super dealloc];
}

@end
