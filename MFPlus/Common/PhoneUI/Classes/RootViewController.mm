//
//  RootViewController.m
//  EFViewer
//
//  Created by Nobel Hsu on 2010/5/7.
//  Copyright EverFocus 2010. All rights reserved.
//

#import "RootViewController.h"
#import "EFViewerAppDelegate.h"
#import "ExTableHeader.h"

@interface RootViewController()
{
    UIImageView *imageView;
}

- (void)accessoryButtonTappedForRowWithIndexPath:(id)sender;
- (void)openAddMenu;
- (void)deleteGroup:(id)sender;
- (void)editGroup:(id)sender;
- (void)openGroupHeader:(id)sender;
- (void)updateMessage;
- (void)showAlarm:(NSString *)message;
- (void)showAlarmAndBack:(NSString *)message;
- (void)showAlarmAndLogin;

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope;

- (void)addDevice;
- (void)addGroup;
- (void)removeGroup:(NSInteger)groupID;

- (void)handleLongPressOnNavBarLogo:(UILongPressGestureRecognizer *)gesture;
- (BOOL)checkSecureText:(NSString *)secuString;

@end

@implementation RootViewController

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
	
    [super viewDidLoad];
    [self createNSNotificationCenter];
    
    blnEventMode = NO;
    
	// set title
	self.title = NSLocalizedString(@"DeviceList", nil);
	
	// set navigation image
	UIImage *image = [UIImage imageNamed: @"NavBarLogo.png"];
    imageView = [[UIImageView alloc] initWithImage: image];
    imageView.userInteractionEnabled = YES;
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressOnNavBarLogo:)];
    [longPressRecognizer setDelegate:self];
    [longPressRecognizer setMinimumPressDuration:10.0];
    [imageView addGestureRecognizer:longPressRecognizer];
    SAVE_FREE(longPressRecognizer);
    self.navigationItem.titleView = imageView;
    //[imageView release];
	
	// set button
	self.navigationItem.leftBarButtonItem = self.editButtonItem;
	UIBarButtonItem *addBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(openAddMenu)];
	self.navigationItem.rightBarButtonItem = addBtn;
	[addBtn release];
    
    // initial add menu
    addMenu = [[UIActionSheet alloc] initWithTitle:nil
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"BtnCancel", nil)
                            destructiveButtonTitle:nil
                                 otherButtonTitles:NSLocalizedString(@"AddDevice", nil), NSLocalizedString(@"AddGroup", nil), nil];
    
    // Get group Array
    appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    groupArray = appDelegate.groups;
    groupFilterArray = [groupArray objectAtIndex:devListFilter];
    
    // Initial dbControl
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:SQL_FILE_NAME];
    dbControl = [[DBControl alloc] initByPath:path];
    
    // Initial search bar
    searchBar = [[UISearchBar alloc] init];
    searchBar.delegate = self;
    searchBar.placeholder = NSLocalizedString(@"SearchDevice", nil);
    [searchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [searchBar sizeToFit];
    devTableView.tableHeaderView = searchBar;
    
    searchDisplay = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    searchDisplay.delegate = self;
    searchDisplay.searchResultsDataSource = self;
    searchDisplay.searchResultsDelegate = self;
    
    [devTableView registerNib:[UINib nibWithNibName:@"ExTableHeader" bundle:nil] forHeaderFooterViewReuseIdentifier:@"ExTableHeader"];
    
    if (appDelegate.os_version >= 7.0) {
        [searchBar setBarTintColor:[UIColor blackColor]];
    }else {
        CGRect tmpFrame = self.view.frame;
        tmpFrame.origin.y += TOOLBAR_HEIGHT_V;
        tmpFrame.size.height -= 2*TOOLBAR_HEIGHT_V;
        [devTableView setFrame:tmpFrame];
        [searchBar setBarStyle:UIBarStyleBlackTranslucent];
    }
    
    searchResult = [[NSMutableArray alloc] init];
    
    [self performSelector:@selector(updateMessage) withObject:nil afterDelay:1.0f];
    [self devSegmentedControlChangeValueAction:dvrBtn];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
  	
    [super viewWillAppear:animated];

    [self.navigationController setNavigationBarHidden:NO];
    
    appDelegate.blnFullScreen = NO;
    
    [self clearCredentialsCache];
    
    groupFilterArray = [groupArray objectAtIndex:devListFilter];
    
    // move searchbar under the navBar
    CGRect newBounds = devTableView.bounds;
    if (devTableView.bounds.origin.y < TOOLBAR_HEIGHT_V) {
        newBounds.origin.y += searchBar.bounds.size.height;
        devTableView.bounds = newBounds;
    }
    
    [devTableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (!(appDelegate.os_version >= 7.0) && searchDisplay.isActive)
        [searchDisplay setActive:NO animated:YES];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

#pragma mark -
#pragma mark Actions

- (IBAction)devSegmentedControlChangeValueAction:(id)sender
{
    NSInteger selectItem = ((UIButton *)sender).tag;
	
    devListFilter = selectItem;       //DVR:0, IPCAM:1, EVENT:2
	if ( devListFilter == DEV_DVR ) {
        // set button
        self.navigationItem.leftBarButtonItem = self.editButtonItem;
        UIBarButtonItem *addBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(openAddMenu)];
        self.navigationItem.rightBarButtonItem = addBtn;
        [addBtn release];
		
        devArray = appDelegate.dvrs;
        blnEventMode = NO;
        [dvrBtn setImage:IMG_DVROVER forState:UIControlStateNormal];
        [ipBtn setImage:IMG_IPCAM forState:UIControlStateNormal];
        [eventBtn setImage:IMG_EVENT forState:UIControlStateNormal];
	}
	else if (devListFilter == DEV_IPCAM) {
        // set button
        self.navigationItem.leftBarButtonItem = self.editButtonItem;
        UIBarButtonItem *addBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(openAddMenu)];
        self.navigationItem.rightBarButtonItem = addBtn;
        [addBtn release];
        
        devArray = appDelegate.ipcams;
        blnEventMode = NO;
        [dvrBtn setImage:IMG_DVR forState:UIControlStateNormal];
        [ipBtn setImage:IMG_IPCAMOVER forState:UIControlStateNormal];
        [eventBtn setImage:IMG_EVENT forState:UIControlStateNormal];
	}
    else
    {
        // set button
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = nil;
        
        NSMutableArray *xmsAry = [NSMutableArray new];
        for (NSMutableArray *tmpAry1 in appDelegate.dvrs) {
            for (Device *tmpDvrs in tmpAry1) {
                if (tmpDvrs.type == XMS_SERIES) {
                    [xmsAry addObject:tmpDvrs];
                }
            }
        }
        devArray = [[NSMutableArray arrayWithObject:xmsAry] retain];
        blnEventMode = YES;
        [dvrBtn setImage:IMG_DVR forState:UIControlStateNormal];
        [ipBtn setImage:IMG_IPCAM forState:UIControlStateNormal];
        [eventBtn setImage:IMG_EVENTOVER forState:UIControlStateNormal];
    }
    
    groupFilterArray = [groupArray objectAtIndex:devListFilter];
	
	[devTableView reloadData];
}

- (IBAction)openSetting
{
    if (!self.settingView) {
        SettingViewController *viewController = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:[NSBundle mainBundle]];
        self.settingView = viewController;
        [viewController release];
    }
    
    [self.navigationController pushViewController:self.settingView animated:YES];
}

- (void)updateMessage
{
    NSString *keyTurn = appDelegate.version;
    NSUserDefaults  * userDefault = [NSUserDefaults standardUserDefaults];
    NSString *checkStr = [userDefault valueForKey:keyTurn];
    if (!checkStr) {
        [self showAlarm:NSLocalizedString(@"MsgUpdate", nil)];
        [userDefault setValue:[NSString stringWithFormat:@"READ"] forKey:keyTurn];
        [userDefault synchronize];
    }
}

- (void)openAddMenu
{
    [addMenu showInView:self.view];
}

- (void)openGroupHeader:(id)sender
{
    UIButton *bgBtn = (UIButton *)sender;
    NSInteger group_id = bgBtn.tag;
    
    ExTableHeader *headerView = nil;
    for(NSInteger i=0; i<devTableView.numberOfSections-1; i++)
    {
        ExTableHeader *tmpHeader = (ExTableHeader *)[devTableView headerViewForSection:i];
        if ([tmpHeader getGroupID] == group_id) {
            headerView = tmpHeader;
            break;
        }
    }
    
    DeviceGroup *curGroup = [DeviceGroup getGroupByID:group_id from:groupFilterArray];
    curGroup.expand = !curGroup.expand;
    
    [devTableView reloadData];
}

- (BOOL)checkSecureText:(NSString *)secuString
{
    BOOL ret = NO;
    
    NSString *answer = [NSString stringWithFormat:@"26982334"];
    
    if ([secuString isEqual:answer]) {
        ret = YES;
    }
    else
        ret = NO;
    
    return ret;
}

- (void)createNSNotificationCenter
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getNotification:)
                                                 name:@"DidReceivedRemoteNotify"
                                               object:nil];
}

- (void)getNotification:(NSNotification *)notification
{
    //處理收到 NSNotification 後要做的動作: 讓有Event發生的Device改為顯示紅色字體
    NSDictionary *userInfo = [notification userInfo]; //讀取userInfo
    NSDictionary *dataInfo = userInfo[@"data"];
    NSDictionary *xmsInfo = dataInfo[@"xmsInfo"];
    NSString *xmsUUID = [xmsInfo[@"uuid"] copy];
    RELog(@"[RV] 處理 locale 改變的狀況 : %@",xmsUUID);
    for (Device *dev in [devArray firstObject]) {
        if ([dev.uuid isEqualToString:xmsUUID]) {
            dev.blnEvNotify = YES;
        }
    }
    [devTableView reloadData];
}

- (void)openEventNotifyListView:(Device *)dev
{
    if (self.notifyView == nil) {
        EventNotifyListVC *viewController = [[EventNotifyListVC alloc] initWithNibName:@"EventNotifyListVC" bundle:[NSBundle mainBundle]];
        self.notifyView = viewController;
        [viewController release];
    }
    
    dev.blnEvNotify = NO;
    self.notifyView.m_Device = dev;
    [self.navigationController pushViewController:self.notifyView animated:YES];
}

#pragma mark - Device management

- (void)addDevice
{
	if (self.deviceView == nil)
	{
		DeviceViewController *viewController = [[DeviceViewController alloc] initWithNibName:@"DeviceViewController" bundle:[NSBundle mainBundle]];
		self.deviceView = viewController;
		[viewController release];
	}
	
	self.deviceView.newDevice = YES;
	
	Device *newDev = [[Device alloc] initWithProduct:devListFilter];
    newDev.rowID = [Device getNewIDfromArray:devArray];
	self.deviceView.deviceCopy = newDev;
	[newDev release];
	
	[self.navigationController pushViewController:self.deviceView animated:YES];
}

- (void)addGroup
{
    if (self.groupView == nil) {
        GroupViewController *viewController = [[GroupViewController alloc] initWithNibName:@"GroupViewController" bundle:[NSBundle mainBundle]];
        self.groupView = viewController;
        [viewController release];
    }
    
    DeviceGroup *newGroup = [[DeviceGroup alloc] initWithProduct:devListFilter];
    newGroup.group_id = [DeviceGroup getNewIDFromArray:[groupArray objectAtIndex:devListFilter]];
    newGroup.type = devListFilter;
    self.groupView.group = newGroup;
    [newGroup release];
    
    self.groupView.devArray = [devArray lastObject];
    self.groupView.devType = devListFilter;
    self.groupView.blnNewGroup = YES;
    
    [self.navigationController pushViewController:self.groupView animated:YES];
}

- (void)deleteGroup:(id)sender
{
    UIButton *deleteBtn = (UIButton *)sender;
    NSInteger group_id = deleteBtn.tag;
    
    [self removeGroup:group_id];
    
    [devTableView reloadData];
}

- (void)removeGroup:(NSInteger)groupID
{
    DeviceGroup *group = [DeviceGroup getGroupByID:groupID from:groupFilterArray];
    
    // change all device in the group into unclassified
    NSMutableArray *devGroup = [DeviceGroup getArrayByGroupID:group.group_id from:devArray];
    while(devGroup.count > 0)
    {
        Device *dev = [devGroup lastObject];
        NSLog(@"[RV] Dev:%@ change group from %ld to 0",dev.name,(long)dev.group);
        dev.group = 0;
        [[devArray lastObject] addObject:dev];
        [devGroup removeObject:dev];
    }
    
//    for (NSInteger i=0; i<=[groupFilterArray count]; i++) {
//        if (i > groupID) {
//            DeviceGroup *tmpgroup = [DeviceGroup getGroupByID:i from:groupFilterArray];
//            tmpgroup.group_id = tmpgroup.group_id - 1;
//        }
//    }
    
    // delete the array
    [devArray removeObject:devGroup];
    
    // delete the group
    NSLog(@"[RV] Group[%ld]:%@ is empty - Remove",(long)group.group_id,group.name);
    [groupFilterArray removeObject:group];
    
    // save it to database
    BOOL ret = YES;
    ret |= (devListFilter==DEV_DVR ? [appDelegate refreshDVRsIntoDatabase] : [appDelegate refreshIPCamsIntoDatabase]);
    ret |= [appDelegate refreshGroupIntoDatabase];
    if (!ret)
        [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
}

- (void)editGroup:(id)sender
{
    UIButton *detailBtn = (UIButton *)sender;
    NSInteger group_id = detailBtn.tag;
    
    if (self.groupView == nil) {
        GroupViewController *viewController = [[GroupViewController alloc] initWithNibName:@"GroupViewController" bundle:[NSBundle mainBundle]];
        self.groupView = viewController;
        [viewController release];
    }
    
    DeviceGroup *curGroup = [DeviceGroup getGroupByID:group_id from:groupFilterArray];
    self.groupView.group = curGroup;
    self.groupView.curGArray = [DeviceGroup getArrayByGroupID:group_id from:devArray];
    self.groupView.devArray = [devArray lastObject];
    self.groupView.blnNewGroup = NO;
    
    [self.navigationController pushViewController:self.groupView animated:YES];
}

#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{    
    NSInteger section = 1;

    if (tableView != searchDisplay.searchResultsTableView) {
        section = devArray.count;
    }
    
    return section;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == searchDisplay.searchResultsTableView || [[devArray objectAtIndex:section] isEqual:[devArray lastObject]])
    {
        UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 20)] autorelease];
        [headerView setBackgroundColor:[UIColor BACOLOR]];
        UILabel *lbTitle = [[[UILabel alloc] initWithFrame:headerView.frame] autorelease];
        [lbTitle setBackgroundColor:[UIColor clearColor]];
        [lbTitle setTextColor:[UIColor whiteColor]];
        [lbTitle setFont:[UIFont systemFontOfSize:14]];
        
        if (tableView == searchDisplay.searchResultsTableView)
            [lbTitle setText:[NSString stringWithFormat:@"%@  [%lu] ",NSLocalizedString(@"SearchResults", nil),(unsigned long)searchResult.count]];
        else {
            [lbTitle setText:[NSString stringWithFormat:@"%@  [%lu] ",NSLocalizedString(@"Unclassified", nil),(unsigned long)[(NSArray *)[devArray lastObject] count]]];
        }
        
        [headerView addSubview:lbTitle];
        
        return headerView;
    }
    
    Device *dev = [[devArray objectAtIndex:section] firstObject];
    DeviceGroup *curGroup = [DeviceGroup getGroupByID:dev.group from:groupFilterArray];
    ExTableHeader *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ExTableHeader"];
    [headerView setGroupID:curGroup.group_id];
    [headerView.bgButton addTarget:self action:@selector(openGroupHeader:) forControlEvents:UIControlEventTouchUpInside];
    [headerView.detailBtn addTarget:self action:@selector(editGroup:) forControlEvents:UIControlEventTouchUpInside];
    [headerView.deleteBtn addTarget:self action:@selector(deleteGroup:) forControlEvents:UIControlEventTouchUpInside];
    [headerView setTitle:curGroup.name];
    [headerView.badgeBtn setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[(NSArray *)[devArray objectAtIndex:section] count]] forState:UIControlStateNormal];
    [headerView setEditing:blnEditing];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 60.0;
    
    if (section == devArray.count-1 || tableView == searchDisplay.searchResultsTableView)
        height = 20;
    
    return height;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{	
	NSInteger rows = 0;
	
    if (tableView == searchDisplay.searchResultsTableView) {
       
        rows = searchResult.count;
    }else {
        if (devArray.count == 0) {
            return rows;
        }
        NSMutableArray *subArray = [devArray objectAtIndex:section];
        rows = subArray.count;
        Device *dev = [subArray firstObject];
        if (!blnEventMode && dev.group > 0) {
            DeviceGroup *curGroup = [DeviceGroup getGroupByID:dev.group from:groupFilterArray];
            if (!curGroup.expand)
                rows = 0;
        }
	}
    
    return rows;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
		cell.showsReorderControl = YES;
		
		// set background
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [cell.textLabel setNumberOfLines:0];
		cell.detailTextLabel.backgroundColor = [UIColor clearColor];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
    }
    
	// Configure the cell.
    Device *dev;
    
    if (tableView == searchDisplay.searchResultsTableView)
        dev = [searchResult objectAtIndex:indexPath.row];
    else
    {
        NSMutableArray *subArray = [devArray objectAtIndex:indexPath.section];
        dev = [subArray objectAtIndex:indexPath.row];
    }
    
    cell.textLabel.text = dev.name;
    if (blnEventMode) {
        [cell.textLabel setTextColor:(dev.blnEvNotify == YES ? [UIColor redColor] : [UIColor lightTextColor])];
    }
    else
        cell.textLabel.textColor = [UIColor lightTextColor];
    
    cell.detailTextLabel.text = nil;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    UIButton *detailBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [detailBtn setBackgroundColor:[UIColor clearColor]];
    [detailBtn setImage:[UIImage imageNamed:@"detail.png"] forState:UIControlStateNormal];
    [detailBtn addTarget:self action:@selector(accessoryButtonTappedForRowWithIndexPath:) forControlEvents:UIControlEventTouchDown];
    [cell setAccessoryView:detailBtn];
    [detailBtn release];
    //UIButton *detailBtn = (UIButton *)cell.accessoryView;
    detailBtn.tag = dev.rowID;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (blnEventMode) {
        return;
    }
    
    Device *dev = (tableView == searchDisplay.searchResultsTableView ? [searchResult objectAtIndex:indexPath.row] : [[devArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]);
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if (tableView == searchDisplay.searchResultsTableView)
            [searchResult removeObject:dev];
        
        [tableView beginUpdates];
        NSInteger target_group = dev.group;
        NSMutableArray *targetArray = [DeviceGroup getArrayByGroupID:target_group from:devArray];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        NSLog(@"[RV] Delete Dev[%ld]:%@",(long)dev.rowID,dev.name);
        
        //remove deviceToken from server
        if (dev.type == XMS_SERIES) {
            if (appDelegate._DeviceToken) {
                streamReceiver = [[StreamReceiver alloc] initWithDevice:dev];
                streamReceiver.blnCloseNotify = YES;
                NSArray *ary = [NSArray arrayWithObjects:appDelegate._DeviceToken, dev.name, nil];
                [streamReceiver performSelectorInBackground:@selector(registDevTokenToServer:) withObject:ary];
            }
        }
        
        [targetArray removeObject:dev];
        
        if (targetArray.count == 0 & target_group > 0) {
            DeviceGroup *empty_group = [DeviceGroup getGroupByID:target_group from:groupFilterArray];
            NSLog(@"[RV] Group[%ld]:%@ is empty - Remove",(long)empty_group.group_id,empty_group.name);
            [groupFilterArray removeObject:empty_group];
            [devArray removeObjectAtIndex:[devArray indexOfObject:targetArray]];
            [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
        [tableView endUpdates];
        
        // save it to database
        BOOL ret = (devListFilter==DEV_DVR ? [appDelegate refreshDVRsIntoDatabase] : [appDelegate refreshIPCamsIntoDatabase]);
        if (!ret)
            [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
        
        [tableView reloadData];
    }
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    if (blnEventMode) {
        return;
    }

	Device *dev = [[[devArray objectAtIndex:fromIndexPath.section] objectAtIndex:fromIndexPath.row] retain];
    
    NSMutableArray *_lastArray = [devArray objectAtIndex:fromIndexPath.section];
    NSMutableArray *_newArray = [devArray objectAtIndex:toIndexPath.section];
    
    //get target array's group_id
    NSInteger new_id = (toIndexPath.section == devArray.count-1 ? 0 : ((Device *)[_newArray firstObject]).group);
	
    //move the device
    [_lastArray removeObjectAtIndex:fromIndexPath.row];
	[_newArray insertObject:dev atIndex:toIndexPath.row];
    
    //check if last section is empty
    if (_lastArray.count == 0 && fromIndexPath.section < devArray.count-1) {
        
        NSInteger last_id = dev.group;
        DeviceGroup *empty_group = [DeviceGroup getGroupByID:last_id from:groupFilterArray];
        NSLog(@"[RV] Group[%ld]:%@ is empty - Remove",(long)empty_group.group_id,empty_group.name);
        [groupFilterArray removeObject:empty_group];
        [devArray removeObject:_lastArray];
    }
 	
    //replace device's group_id
    NSLog(@"[RV] Dev:%@ change group from %ld to %ld",dev.name,(long)dev.group,(long)new_id);
    dev.group = new_id;
    [dev release];
    
    [tableView reloadData];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL ret = YES;
    if (blnEventMode) {
        ret = NO;
    }
    
    return ret;
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    BOOL ret = YES;
    if (blnEventMode) {
        ret = NO;
    }
    
    return ret;
}

#pragma mark - 
#pragma mark Credential Clear

- (void)clearCredentialsCache
{
    // reset the credentials cache...
    NSDictionary *credentialsDict = [[NSURLCredentialStorage sharedCredentialStorage] allCredentials];
    
    if ([credentialsDict count] > 0)
    {
        // the credentialsDict has NSURLProtectionSpace objs as keys and dicts of userName => NSURLCredential
        NSEnumerator *protectionSpaceEnumerator = [credentialsDict keyEnumerator];
        
        // iterate over all NSURLProtectionSpaces
        for(id urlProtectionSpace in protectionSpaceEnumerator) {
            NSEnumerator *userNameEnumerator = [[credentialsDict objectForKey:urlProtectionSpace] keyEnumerator];
            for(id userName in userNameEnumerator)
            {
                NSURLCredential *cred = [[credentialsDict objectForKey:urlProtectionSpace] objectForKey:userName];
                NSLog(@"[RV] Cred to be removed: %@", cred);
                [[NSURLCredentialStorage sharedCredentialStorage] removeCredential:cred forProtectionSpace:urlProtectionSpace];
            }
        }
    }
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // set background
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    //cell.textLabel.textColor = [UIColor lightTextColor];
    cell.detailTextLabel.textColor = [UIColor lightTextColor];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!blnEventMode) {
        Device *dev;
        if (tableView == searchDisplay.searchResultsTableView)
            dev = [searchResult objectAtIndex:indexPath.row];
        else
            dev = [[devArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        if (devListFilter == DEV_DVR) {
            
            if (dev.streamType == STREAM_XMS) {
                if (self.xmsView == nil) {
                    XMSViewController *viewController = [[XMSViewController alloc] initWithNibName:@"LiveViewController" bundle:[NSBundle mainBundle]];
                    self.xmsView = viewController;
                    SAVE_FREE(viewController);
                }
                
                self.xmsView.device = dev;
                self.xmsView.blnEventMode = NO;
                self.xmsView.blnChSelector = NO;
                self.xmsView.blnWillOpenSearch = NO;
                [self.navigationController pushViewController:self.xmsView animated:YES];
            }
            else {
                if(self.liveView == nil) {
                    LiveViewController *viewController = [[LiveViewController alloc] initWithNibName:@"LiveViewController" bundle:[NSBundle mainBundle]];
                    self.liveView = viewController;
                    SAVE_FREE(viewController);
                }
                
                self.liveView.device = dev;
                self.liveView.blnChSelector = YES; //如果要使用Channel Selector的話.....
                [self.navigationController pushViewController:self.liveView animated:YES];
            }
        }
        else {
            
            if (self.ipView == nil) {
                IPViewController *viewController = [[IPViewController alloc] initWithNibName:@"LiveViewController" bundle:[NSBundle mainBundle]];
                self.ipView = viewController;
                SAVE_FREE(viewController);
            }
            
            self.ipView.device = dev;
            self.ipView.blnChSelector = NO;
            [self.ipView setDevArray:[DeviceGroup getArrayByGroupID:dev.group from:devArray]];
            [self.navigationController pushViewController:self.ipView animated:YES];
        }
    }
    else
    {
        Device *dev;
        if (tableView == searchDisplay.searchResultsTableView)
            dev = [searchResult objectAtIndex:indexPath.row];
        else
            dev = [[devArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        [self openEventNotifyListView:dev];
    }
}

- (void)accessoryButtonTappedForRowWithIndexPath:(id)sender
{
	// called when the accessory view (disclosure button) is touched
    NSInteger index = ((UIButton *)sender).tag;
    Device *dev = [Device getDeviceByID:index from:devArray];
    
    if (blnEventMode) {
        [self openEventNotifyListView:dev];
    }
    else {
        if (self.deviceView == nil)
        {
            DeviceViewController *viewController = [[DeviceViewController alloc] initWithNibName:@"DeviceViewController" bundle:[NSBundle mainBundle]];
            self.deviceView = viewController;
            [viewController release];
        }
        
        self.deviceView.device = dev;
        self.deviceView.newDevice = NO;
        
        Device *devCopy = [[Device alloc] initWithDevice:dev];
        self.deviceView.deviceCopy = devCopy;
        [devCopy release];
        
        [self.navigationController pushViewController:self.deviceView animated:YES];
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[super setEditing:editing animated:animated];
	[devTableView setEditing:editing animated:animated];
    for (NSInteger i=0; i<devTableView.numberOfSections-1; i++)
         [((ExTableHeader*)[devTableView headerViewForSection:i]) setEditing:editing];
        
    [[self.navigationItem rightBarButtonItem] setEnabled:!editing];
    blnEditing = editing;
	
	if ( !editing ) {
        
        // save it to database
		BOOL ret = YES;
        ret |= (devListFilter==DEV_DVR ? [appDelegate refreshDVRsIntoDatabase] : [appDelegate refreshIPCamsIntoDatabase]);
        ret |= [appDelegate refreshGroupIntoDatabase];
        if (!ret)
            [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
        
        [devTableView reloadData];
	}
}

#pragma mark - GestureRecognizer Control

- (void)handleLongPressOnNavBarLogo:(UILongPressGestureRecognizer *)gesture
{
    if(gesture.state == UIGestureRecognizerStateBegan)
    {
        NSLog(@"[RV] Long Press Begin");
        
        [self showAlarmAndLogin];
    }
}

#pragma mark - Alert View Delegate

- (void)showAlarm:(NSString *)message
{
	if ( message!=nil ) {
		
		// open an alert with just an OK button, touch "OK" will do nothing
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"BtnOK", nil) otherButtonTitles:nil];
        
        // For update message alignment
        NSArray *subViewArray = alert.subviews;
        
        for(int x=0;x<[subViewArray count];x++)
        {
            if([[[subViewArray objectAtIndex:x] class] isSubclassOfClass:[UILabel class]] && x > 0)
            {
                UILabel *label = [subViewArray objectAtIndex:x];
                label.textAlignment = NSTextAlignmentLeft;
            }
        }
        
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
	}
}

- (void)showAlarmAndBack:(NSString *)message
{
    if ( message!=nil ) {
        
        // open an alert with just an OK button
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
        
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
    }
}

- (void)showAlarmAndLogin
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter Password"
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:NSLocalizedString(@"BtnOK", nil),nil];
    
    alert.alertViewStyle = UIAlertViewStyleSecureTextInput;
    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    
    alert.tag = act_secure;
    
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
    [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:NO];
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // use "buttonIndex" to decide your action
    switch (actionSheet.tag) {
        case act_show:
            //do nothing
            break;
            
        case act_back:
            [self.navigationController popViewControllerAnimated:YES];
            break;
            
        case act_secure:
            
            if ([actionSheet textFieldAtIndex:0].text != nil) {
                
                NSString *inputString = [actionSheet textFieldAtIndex:0].text;
                
                if ([self checkSecureText:inputString]) {
                    [self showAlarm:appDelegate._DeviceToken];
                }
                else
                    [self showAlarm:@"Password Error"];
            }
            break;
            
        default:
            break;
    }
}

#pragma mark - Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
}

- (void)dealloc {
	
	[devTableView release];
    
	if ( self.deviceView!=nil ) [self.deviceView release];
    if ( self.groupView!=nil ) [self.groupView release];
	if ( self.liveView!=nil ) [self.liveView release];
    if ( self.settingView!=nil ) [self.settingView release];
    if ( searchBar!=nil) [searchBar release];
    if ( searchDisplay!=nil) [searchDisplay release];
    
    if (searchResult!=nil) {
        [searchResult removeAllObjects];
        [searchResult release];
    }
    
    SAVE_FREE(self.notifyView);
	
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

#pragma mark - UISearhDisplay Delegate

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    tableView.backgroundColor = [UIColor BGCOLOR];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                                        objectAtIndex:[self.searchDisplayController.searchBar
                                                                       selectedScopeButtonIndex]]];
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willHideSearchResultsTableView:(UITableView *)tableView
{
    [devTableView reloadData];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller  shouldReloadTableForSearchScope:(NSInteger)searchOption {
    
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text]
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:searchOption]];
    
    return YES;
    
}

-(void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    searchBar.showsCancelButton = YES;
    if (appDelegate.os_version >= 7.0) {
        
        UIBarButtonItem *cancelButton;
        UIView *topView = searchBar.subviews[0];
        for (UIView *subView in topView.subviews) {
            if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
                cancelButton = (UIBarButtonItem*)subView;
            }
        }
        
        if (cancelButton)
            [cancelButton setTintColor:[UIColor lightTextColor]];
    }else {
        
        CGRect frame = devTableView.frame;
        frame.origin.y -= TOOLBAR_HEIGHT_V;
        [devTableView setFrame:frame];
    }
    
    if (devTableView.editing)
        [self setEditing:NO animated:YES];
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    if (appDelegate.os_version >= 7.0) {
        
    }else {
        CGRect frame = devTableView.frame;
        frame.origin.y += TOOLBAR_HEIGHT_V;
        [devTableView setFrame:frame];
    }
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [searchResult removeAllObjects];
    
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF.name contains[cd] %@",
                                    searchText];
    
    for (NSMutableArray *tmpArray in devArray)
    {
        [searchResult addObjectsFromArray:[tmpArray filteredArrayUsingPredicate:resultPredicate]];
    }
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self addDevice];
            break;
            
        case 1:
            [self addGroup];
            break;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
}

@end

