//
//  EventNotifyListVC.h
//  EFViewer
//
//  Created by EFRD on 2016/8/22.
//  Copyright © 2016年 EverFocus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFViewerAppDelegate.h"
#import "XMSViewController.h"
#import "StreamReceiver.h"
#import "Reachability.h"
#import "DBControl.h"
#import "Device.h"

#define XMS_EVENT_NORMAL 0
#define XMS_EVENT_XFLEET 1

enum {
    alert_show = 0,
    alert_delete,
    alert_select,
} EventNotifyAlertTag;

@interface EventNotifyListVC : UIViewController <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate,UIActionSheetDelegate,UIGestureRecognizerDelegate,StreamDelegate>
{
    IBOutlet UITableView        *mainTable;
    IBOutlet UIButton           *generalBtn;
    IBOutlet UIButton           *xFleetBtn;
    
    EFViewerAppDelegate         *appDelegate;
    StreamReceiver              *streamReceiver;
    EventTable                  *evTable;
    DBControl                   *dbControl;
    
    UISearchDisplayController   *searchDisplay;
    UISearchBar                 *searchBar;
    NSMutableArray              *searchResult;
    
    UIActionSheet               *eventMenu;
    NSMutableArray              *eventList;
    
    UIRefreshControl            *refreshCtrl;
    UIActivityIndicatorView     *indicatorFooter;
    NSTimer                     *refreshTimer;
    NSInteger                   repeatTime;
    NSInteger                   evListFilter;         //normal:0, xfleet:1
}

@property(nonatomic,retain) Device                *m_Device;
@property(nonatomic,retain) XMSViewController     *xmsView;

- (IBAction)buttonAction:(id)sender;

@end
