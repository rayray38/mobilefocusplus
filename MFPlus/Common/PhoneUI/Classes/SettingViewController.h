//
//  SettingViewController.h
//  EFViewer
//
//  Created by James Lee on 13/8/22.
//  Copyright (c) 2013年 EverFocus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelpViewController.h"
#import "ImportViewController.h"
#import "ExportViewController.h"
#import "ZBarSDK.h"

#define ROW_IMPORT  0
#define ROW_EXPORT  1
#define ACT_SHOW    2
#define ACT_BACK    3
#define ACT_HELP    4

#define MsgUserNotify @"Tap the link to download the User Manual in PDF. Ensure your mobile phone have PDF reader installed. Please be noted that downloading file will consume certain data volume of your network traffic."

enum ef_setting_section
{
    SECTION_UM = 0,
    SECTION_DIO,
    SECTION_CONFIG,
};

@interface SettingViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,ZBarReaderDelegate,ZBarReaderViewDelegate>
{
    IBOutlet UITableView		*mainTable;
    IBOutlet UIView             *scannerView;
    IBOutlet UIBarButtonItem    *btnCancel;
    UIActionSheet               *loadMenu;
}

@property(nonatomic,retain) HelpViewController     *helpView;
@property(nonatomic,retain) ImportViewController   *importView;
@property(nonatomic,retain) ExportViewController   *exportView;
@property(nonatomic,retain) IBOutlet ZBarReaderView *readerView;

- (IBAction)closeScanner;

@end
