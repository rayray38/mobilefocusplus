//
//  SearchViewController.h
//  EFViewer
//
//  Created by James Lee on 13/4/3.
//  Copyright (c) 2013年 EverFocus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreamReceiver.h"
#import "EventViewController.h"
#import "Device.h"

#define TABLE_HDDINFO   0
#define TABLE_TIME      1
#define TABLE_EVENT     2

#define INDEX_DATE   0
#define INDEX_TIME   1

#define FOUR_INCH       548
#define FOOTER_HEIGHT   205
#define FOOTER_HEIGHT_4 275

#define IMG_CHECK       [UIImage imageNamed:@"check_on.png"]
#define IMG_UNCHECK     [UIImage imageNamed:@"check_off.png"]
#define IMG_COLLAPSE    [UIImage imageNamed:@"collapse.png"]
#define IMG_EXPAND      [UIImage imageNamed:@"expand.png"]

@interface SearchViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    StreamReceiver          *streamReceiver;
    Device                  *device;
    NSDateFormatter         *dateFormatter;
    
    IBOutlet UITableView    *mainTable;
    IBOutlet UIView         *eventFooter;
    IBOutlet UIView         *pickerMenu;
    IBOutlet UIDatePicker   *datePicker;
    IBOutlet UIToolbar      *pickerBar;
    
    BOOL                    blnFullScreen;
    BOOL                    bln4inch;
    BOOL                    blnTimeEx;
    BOOL                    blnEventEx;
    BOOL                    blnSearch;
    BOOL                    blnIOS7;
    BOOL                    blnShowPicker;
    NSMutableArray          *eventList;
    NSString                *hddStart;
    NSString                *hddEnd;
    
    NSUInteger              iSelectedTime;
    NSUInteger              iSearchST;
    NSUInteger              iSearchET;
    NSInteger               eventFilter;
    NSInteger               pickerIdx;
}

@property(nonatomic,retain) StreamReceiver      *streamReceiver;
@property(nonatomic,retain) UIViewController    *sourceView;
@property(nonatomic,retain) EventViewController *eventView;
@property(nonatomic,retain) Device              *device;

- (void)setPortraitLayout;
- (void)setLandscapeLayout;
- (void)autoScroll;
- (void)checkDST_NV;

- (void)changeSectionMode;
- (IBAction)eventFilter:(id)sender;
- (IBAction)TimeSelected;
- (void)startPlayback;
- (void)startSearch;

@end
