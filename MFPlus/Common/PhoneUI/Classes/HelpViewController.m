//
//  HelpViewController.m
//  EFViewerHD
//
//  Created by James Lee on 13/1/19.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import "HelpViewController.h"
#import "EFViewerAppDelegate.h"

@implementation HelpViewController

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = [NSString stringWithFormat:@"v%@",appDelegate.version]; //version number
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [NSThread detachNewThreadSelector:@selector(loadDocumentInHelpView)
                             toTarget:self
                           withObject:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    SAVE_FREE(helpView);
    [super dealloc];
}

#pragma mark - Action

-(void)loadDocumentInHelpView
{
//    NSString *path = [[NSBundle mainBundle] pathForResource:HELPPDF ofType:nil];
//    NSURL *url = [NSURL fileURLWithPath:path];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",HELPURL]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [helpView loadRequest:request];
}

@end
