//
//  ExportViewController.h
//  EFViewer
//
//  Created by James Lee on 13/8/23.
//  Copyright (c) 2013年 EverFocus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QREncoder.h"
#import "DataMatrix.h"
#import "EFViewerAppDelegate.h"

@interface ExportViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView		*devTableView;
    IBOutlet UIView             *resultView;
    IBOutlet UIImageView        *qrImgView;
    IBOutlet UIButton           *btnSelectAll;
    IBOutlet UIButton           *btnDeselect;
    IBOutlet UIButton           *btnSave;
    IBOutlet UIButton           *btnCancel;
    
    NSMutableArray              *devList;
    NSMutableArray              *checkList;
    
    EFMediaTool                 *mediaLibrary;
}

- (IBAction)selectAll;
- (IBAction)deselectAll;
- (IBAction)saveToAlbum;
- (IBAction)hideQRView;

- (void)loadAllDevices;

@end
