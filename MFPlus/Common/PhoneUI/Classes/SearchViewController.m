//
//  SearchViewController.m
//  EFViewer
//
//  Created by James Lee on 13/4/3.
//  Copyright (c) 2013年 EverFocus. All rights reserved.
//

#import "SearchViewController.h"

@implementation SearchViewController

@synthesize streamReceiver,sourceView,eventView,device;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    [datePicker setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [self.view addSubview:pickerMenu];
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    blnIOS7 = (appDelegate.os_version >= 7.0);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //Set Layout
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    blnFullScreen = appDelegate.blnFullScreen;
    
    if (blnFullScreen) {
       
        [self setLandscapeLayout];
    }else {
       
        [self setPortraitLayout];
    }
    
    // set Hdd Start / End Time
    SAVE_FREE(hddStart);
    SAVE_FREE(hddEnd);
    
    // calculate DST
    [self checkDST_NV];
    
    if (streamReceiver.m_intdlsEnable == 1) {
        
        NSDate *tmpDate = [NSDate dateWithTimeIntervalSince1970:streamReceiver.m_intDiskStartTime];
        if (device.devTimeZone)
        {
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:device.devTimeZone]];
            [datePicker setTimeZone:[NSTimeZone timeZoneWithAbbreviation:device.devTimeZone]];
        }
        else {
            [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            [datePicker setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        }
        
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
        hddStart = [[dateFormatter stringFromDate:tmpDate] copy];
        if (device.type != XMS_SERIES) {
            [datePicker setMinimumDate:tmpDate];
        }
        //[datePicker setMinimumDate:tmpDate];
        tmpDate = [NSDate dateWithTimeIntervalSince1970:streamReceiver.m_intDiskEndTime + streamReceiver.m_dst_time_sec];
        hddEnd = [[dateFormatter stringFromDate:tmpDate] copy];
        if (device.type != XMS_SERIES) {
            [datePicker setMaximumDate:tmpDate];
        }
        //[datePicker setMaximumDate:tmpDate];
        
        //Set default select time
        iSelectedTime = iSearchST = streamReceiver.m_intDiskEndTime + streamReceiver.m_dst_time_sec - 5*60;
        iSearchET = streamReceiver.m_intDiskEndTime + streamReceiver.m_dst_time_sec;
        [datePicker setDate:[NSDate dateWithTimeIntervalSince1970:iSelectedTime]];
    }
    else
    {
        NSDate *tmpDate = [NSDate dateWithTimeIntervalSince1970:streamReceiver.m_intDiskStartTime];
        if (device.devTimeZone)
        {
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:device.devTimeZone]];
            [datePicker setTimeZone:[NSTimeZone timeZoneWithAbbreviation:device.devTimeZone]];
        }
        else {
            [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            [datePicker setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        }
        
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
        hddStart = [[dateFormatter stringFromDate:tmpDate] copy];
        if (device.type != XMS_SERIES) {
            [datePicker setMinimumDate:tmpDate];
        }
        //[datePicker setMinimumDate:tmpDate];
        tmpDate = [NSDate dateWithTimeIntervalSince1970:streamReceiver.m_intDiskEndTime];
        hddEnd = [[dateFormatter stringFromDate:tmpDate] copy];
        if (device.type != XMS_SERIES) {
            [datePicker setMaximumDate:tmpDate];
        }
        //[datePicker setMaximumDate:tmpDate];
        
        //Set default select time
        iSelectedTime = iSearchST = streamReceiver.m_intDiskEndTime - 5*60;
        iSearchET = streamReceiver.m_intDiskEndTime;
        [datePicker setDate:[NSDate dateWithTimeIntervalSince1970:iSelectedTime]];
    }
    
    blnShowPicker = NO;
    [pickerMenu setHidden:YES];
    
    //Set TimeSearch Expand
    blnTimeEx = YES;
    blnEventEx = NO;
    [mainTable reloadData];
    
    //Get Screen Size
    bln4inch = NO;
    if (self.view.bounds.size.height == FOUR_INCH) {
        bln4inch = YES;
    }
    
    blnSearch = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (blnSearch) {
        return;
    }
    
    self.streamReceiver = nil;
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.blnFullScreen = blnFullScreen;
}

- (void)dealloc
{
    [dateFormatter release];
    dateFormatter = nil;
    
    [datePicker release];
    datePicker = nil;
    
    [pickerMenu release];
    pickerMenu = nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    BOOL ret = YES;
    if (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
        ret = NO;
    }
    return ret;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
	{
        blnFullScreen = NO;
	}
	else
	{
        blnFullScreen = YES;
	}
    
    if (blnShowPicker)
        [pickerMenu setHidden:YES];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if (blnFullScreen) {
        [self setLandscapeLayout];
    }else {
        [self setPortraitLayout];
    }
    
    if (blnShowPicker)
        [pickerMenu setHidden:NO];
    
    [mainTable reloadData];
    [self autoScroll];
}

- (void)setPortraitLayout
{
    CGFloat fltBarHeight = blnIOS7 ? 0 : TOOLBAR_HEIGHT_V;
    [mainTable setFrame:CGRectMake(0, fltBarHeight, self.view.frame.size.width, self.view.frame.size.height-fltBarHeight)];
    [pickerMenu setFrame:mainTable.frame];
    CGFloat pickerOffset = pickerMenu.frame.size.height - KEYBOARD_HEIGHT_V;
    [datePicker setFrame:CGRectMake(0, pickerOffset, self.view.frame.size.width, KEYBOARD_HEIGHT_V)];
    [pickerBar setFrame:CGRectMake(0, pickerOffset-TOOLBAR_HEIGHT_V, self.view.frame.size.width, TOOLBAR_HEIGHT_V)];
}

- (void)setLandscapeLayout
{
    CGFloat fltBarHeight = blnIOS7 ? 0 : TOOLBAR_HEIGHT_H;
    [mainTable setFrame:CGRectMake(0, fltBarHeight, self.view.frame.size.width, self.view.frame.size.height-fltBarHeight)];
    [pickerMenu setFrame:mainTable.frame];
    CGFloat pickerOffset = pickerMenu.frame.size.height - KEYBOARD_HEIGHT_H;
    [datePicker setFrame:CGRectMake(0, pickerOffset, self.view.frame.size.width, KEYBOARD_HEIGHT_H)];
    [pickerBar setFrame:CGRectMake(0, pickerOffset-TOOLBAR_HEIGHT_H, self.view.frame.size.width, TOOLBAR_HEIGHT_H)];
}

- (void)checkDST_NV
{
    if (streamReceiver.m_intdlsEnable == 1)
    {
        NSUInteger currentZoneTime = streamReceiver.m_intCurrentTime + streamReceiver.m_DiskGMT_sec;
        NSUInteger weekDayPlus,weekDayPlus1;
        NSDate *tmpDate = [NSDate dateWithTimeIntervalSince1970:streamReceiver.m_intCurrentTime];
        
        // Get Current Year
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy"];
        NSString *currentYear = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:tmpDate]];
        
        // Get startTime and adjustTime
        [dateFormatter setDateFormat:@"yyyy/MM/dd/ HH:mm:ss"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSUInteger startMonth = streamReceiver.m_intStartMonth;
        NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@/%lu/01 00:00:00",currentYear,(unsigned long)startMonth +1]];
        
        // Get days in month
        NSCalendar *cal = [NSCalendar currentCalendar];
        NSRange rng1 = [cal rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:date];
        NSUInteger daysofStartMonth = rng1.length;
        //NSLog(@"[SV] Numbers Of Days in Month : %lu",(unsigned long)daysofStartMonth);
        
        NSDateComponents *comps = [cal components:NSWeekdayCalendarUnit fromDate:date];
        NSUInteger weekday = [comps weekday] - 1;
        if (streamReceiver.m_intStartWeekday >= weekday)
        {
            streamReceiver.m_intStartWeek = streamReceiver.m_intStartWeek;
            weekDayPlus = streamReceiver.m_intStartWeekday - weekday;
        }
        else
        {
            streamReceiver.m_intStartWeek = streamReceiver.m_intStartWeek + 1;
            weekDayPlus = streamReceiver.m_intStartWeekday - weekday;
        }
        
        NSUInteger startDate = (weekDayPlus + 1) + 7 * streamReceiver.m_intStartWeek;
        if (startDate > daysofStartMonth)
        {
            startDate = startDate - 7;
        }
        
        [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm"];
        NSDate *dateStart = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@/%lu/%lu %lu:%lu",currentYear,(unsigned long)startMonth +1,(unsigned long)startDate,(unsigned long)streamReceiver.m_intStartHour,(unsigned long)streamReceiver.m_intStartMin]];
        NSTimeInterval timeInterval = [dateStart timeIntervalSince1970];
        NSUInteger startTime = fabs(timeInterval);
        
        NSUInteger adjustTime = ((streamReceiver.m_intSetHour - streamReceiver.m_intStartHour) *60 + streamReceiver.m_intSetMin - streamReceiver.m_intStartMin) *60 *1000;
        
        // Get endTime
        [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSUInteger endMonth = streamReceiver.m_intEndMonth;
        NSDate *dateEnd = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@/%lu/01 00:00:00",currentYear,(unsigned long)endMonth +1]];
        NSRange rng2 = [cal rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:dateEnd];
        NSUInteger daysofEndMonth = rng2.length;
        
        NSDateComponents *comps2 = [cal components:NSWeekdayCalendarUnit fromDate:dateEnd];
        NSUInteger weekday1 = [comps2 weekday] - 1;
        if (streamReceiver.m_intStartWeekday >= weekday1)
        {
            streamReceiver.m_intEndWeek = streamReceiver.m_intEndWeek;
            weekDayPlus1 = streamReceiver.m_intEndWeekday - weekday1;
        }
        else
        {
            streamReceiver.m_intEndWeek = streamReceiver.m_intEndWeek + 1;
            weekDayPlus1 = streamReceiver.m_intEndWeekday - weekday1;
        }
        
        NSUInteger endDate = (weekDayPlus1 + 1) + 7 * streamReceiver.m_intEndWeek;
        if (endDate > daysofEndMonth)
        {
            endDate = endDate - 7;
        }
        
        [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm"];
        NSDate *EndDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@/%lu/%lu %lu:%lu",currentYear,(unsigned long)endMonth +1,(unsigned long)endDate,(unsigned long)streamReceiver.m_intEndHour,(unsigned long)streamReceiver.m_intEndMin]];
        NSTimeInterval timeInterval2 = [EndDate timeIntervalSince1970];
        NSUInteger endTime = fabs(timeInterval2);
        
        if (currentZoneTime >= startTime && currentZoneTime < endTime)
        {
            streamReceiver.m_dst_time_sec = adjustTime / 1000;
        }
        else
        {
            streamReceiver.m_dst_time_sec = 0;
        }
    }
    else
    {
        streamReceiver.m_dst_time_sec = 0;
        return;
    }
}

#pragma mark - TableView  DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger section = 3;
    if (streamReceiver.streamType == STREAM_ICATCH
        || streamReceiver.streamType == STREAM_XMS)
        section = 2;
    
    return section;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger ret = 0;
    
    switch (section) {
        case TABLE_HDDINFO:
            ret = 2;
            break;
        case TABLE_TIME:
            if (blnTimeEx) {
                ret = 2;
            }
            break;
        case TABLE_EVENT:
            if (blnEventEx) {
                ret = 4;
            }
            break;
    }
    
    return ret;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    CGFloat height = 0;

    switch (section) {
        case TABLE_TIME:
            if (blnTimeEx) {
                height = 40;
            }
            break;
        case TABLE_EVENT:
            if (blnEventEx) {
                height = bln4inch?FOOTER_HEIGHT_4:FOOTER_HEIGHT;
            }
            break;
    }
    
    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)] autorelease];
    [headerView setBackgroundColor:[UIColor clearColor]];
    UILabel *title = [[[UILabel alloc] initWithFrame:CGRectMake(35, 10, tableView.bounds.size.width-10, 18)] autorelease];
    [title setFont:[UIFont boldSystemFontOfSize:14]];
    [title setTextColor:[UIColor lightTextColor]];
    [title setBackgroundColor:[UIColor clearColor]];
    UIButton *expandBtn = [[[UIButton alloc] initWithFrame:CGRectMake(5, 5, 25, 25)] autorelease];
    [expandBtn addTarget:self action:@selector(changeSectionMode) forControlEvents:UIControlEventTouchDown];
    

    switch (section) {
        case TABLE_HDDINFO:
            [title setFrame:CGRectMake(10, 10, tableView.bounds.size.width-10, 18)];
            title.text =  NSLocalizedString(@"HDDSTET", nil);
            break;
        case TABLE_TIME:
//            [expandBtn setTag:TABLE_TIME];
//            [headerView addSubview:expandBtn];
//            if (blnTimeEx) {
//                [expandBtn setImage:IMG_EXPAND forState:UIControlStateNormal];
//            }else {
//                [expandBtn setImage:IMG_COLLAPSE forState:UIControlStateNormal];
//            }
            [title setFrame:CGRectMake(10, 10, tableView.bounds.size.width-10, 18)];
            title.text =  NSLocalizedString(@"TimePlayback", nil);
            break;
        case TABLE_EVENT:
            [expandBtn setTag:TABLE_EVENT];
            [headerView addSubview:expandBtn];
            if (blnEventEx) {
                [expandBtn setImage:IMG_EXPAND forState:UIControlStateNormal];
            }else {
                [expandBtn setImage:IMG_COLLAPSE forState:UIControlStateNormal];
            }
            title.text =  NSLocalizedString(@"EventPlayback", nil);
            break;
    }
    
    [headerView addSubview:title];
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section==TABLE_HDDINFO)
        return nil;
    
    //NSString *title = @"";
    CGFloat height = 0;
    CGRect btnFrame;
    switch (section) {
        case TABLE_TIME:
            
            if (!blnTimeEx)
                return nil;
            
            //title = NSLocalizedString(@"BtnPlay", nil);
            height = 50;
            btnFrame = CGRectMake(tableView.frame.size.width-65, height-45, 40, 40);
            break;
        case TABLE_EVENT:
            
            if (!blnEventEx)
                return nil;
            
            //title = NSLocalizedString(@"BtnSearch", nil);
            height = (bln4inch?FOOTER_HEIGHT_4:FOOTER_HEIGHT) - 15;
            NSInteger btnGap;
            if (blnFullScreen) {
                btnGap = 10;
            }else {
                btnGap = 50;
            }
            btnFrame = CGRectMake(tableView.frame.size.width-65, btnGap, 40, 40);
            break;
    }
    
    UIView *footerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, height)] autorelease];
    UIButton *searchBtn = [[[UIButton alloc] initWithFrame:btnFrame] autorelease];
    [searchBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
    [searchBtn setBackgroundImage:[UIImage imageNamed:@"searchEvent.png"] forState:UIControlStateNormal];
    //[searchBtn setTitle:title forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(selectFunction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:searchBtn];
    searchBtn.tag = section;
    
    if (section == TABLE_EVENT) {
        
        [footerView addSubview:eventFooter];
    }
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setTextColor:[UIColor EFCOLOR]];
        [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
        [cell.detailTextLabel setTextColor:[UIColor lightTextColor]];
    }
    
    switch (indexPath.section) {
        case TABLE_HDDINFO:
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = NSLocalizedString(@"StartTime", nil);
                    cell.detailTextLabel.text = hddStart;
                    break;
                case 1:
                    cell.textLabel.text = NSLocalizedString(@"EndTime", nil);
                    cell.detailTextLabel.text = hddEnd;
                    break;
            }
            break;
        case TABLE_TIME:
            switch (indexPath.row) {
                case 0:
                    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                    cell.textLabel.text = NSLocalizedString(@"SelectDate", nil);
                    cell.detailTextLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:iSelectedTime]];
                    break;
                case 1:
                    [dateFormatter setDateFormat:@"HH:mm"];
                    cell.textLabel.text = NSLocalizedString(@"SelectTime", nil);
                    cell.detailTextLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:iSelectedTime]];
                    break;
            }
            break;
        case TABLE_EVENT:
            switch (indexPath.row) {
                case 0:
                    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                    cell.textLabel.text = NSLocalizedString(@"StartDate", nil);
                    cell.detailTextLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:iSearchST]];
                    break;
                case 1:
                    [dateFormatter setDateFormat:@"HH:mm"];
                    cell.textLabel.text = NSLocalizedString(@"StartTime", nil);
                    cell.detailTextLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:iSearchST]];
                    break;
                case 2:
                    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                    cell.textLabel.text = NSLocalizedString(@"EndDate", nil);
                    cell.detailTextLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:iSearchET]];
                    break;
                case 3:
                    [dateFormatter setDateFormat:@"HH:mm"];
                    cell.textLabel.text = NSLocalizedString(@"EndTime", nil);
                    cell.detailTextLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:iSearchET]];
                    break;
            }
            break;
    }
    
    return cell;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == TABLE_HDDINFO)
        return;
    
    switch (indexPath.row% 2) {
            
        case INDEX_DATE:
            [datePicker setDatePickerMode:UIDatePickerModeDate];
            break;
        case INDEX_TIME:
            [datePicker setDatePickerMode:UIDatePickerModeTime];
            break;
    }
    pickerIdx = indexPath.row;
    
    [pickerMenu setHidden:NO];
    blnShowPicker = YES;
}

#pragma mark - Action

- (void)changeSectionMode
{
    blnTimeEx = !blnTimeEx;
    blnEventEx = !blnEventEx;
    
    [mainTable reloadData];
    
    if (blnFullScreen || !bln4inch)
        [self autoScroll];
}

- (void)TimeSelected
{
    if (blnTimeEx) {
        
        iSelectedTime = [datePicker.date timeIntervalSince1970];
    }else {
        switch (pickerIdx) {
            case 0:
            case 1:
                iSearchST = [datePicker.date timeIntervalSince1970];
                NSLog(@"%lu",(unsigned long)iSearchST);
                break;
            case 2:
            case 3:
                iSearchET = [datePicker.date timeIntervalSince1970];
                NSLog(@"%lu",(unsigned long)iSearchET);
                break;
        }
    }
    [pickerMenu setHidden:YES];
    blnShowPicker = NO;
    [mainTable reloadData];
}

- (void)autoScroll
{
    CGRect frame;
    if (blnEventEx) {
        
        if (blnFullScreen) {
            frame = CGRectMake(0, 135, self.view.frame.size.width, self.view.frame.size.height);
        }else {
            frame = CGRectMake(0, mainTable.frame.size.height, self.view.frame.size.width, mainTable.frame.size.height);
        }
    }else {
        frame = CGRectMake(0, 0, self.view.frame.size.width, mainTable.frame.size.height);
    }
    [mainTable scrollRectToVisible:frame animated:YES];
}

- (IBAction)eventFilter:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    if (btn.tag & eventFilter) {
        eventFilter ^= btn.tag;
        [btn setImage:IMG_UNCHECK forState:UIControlStateNormal];
    }else {
        eventFilter |= btn.tag;
        [btn setImage:IMG_CHECK forState:UIControlStateNormal];
    }
}

- (void)selectFunction:(id)sender
{
    UIButton *funcBtn = (UIButton *)sender;
    
    switch (funcBtn.tag) {
        case TABLE_TIME:
            [self startPlayback];
            break;
        case TABLE_EVENT:
            [self startSearch];
            break;
    }
}

- (void)startPlayback
{
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
    
    streamReceiver.m_blnPlaybackMode = YES;
    streamReceiver.m_intPlaybackStartTime = iSelectedTime;
    //[streamReceiver stopStreaming];
    
    //CameraInfo *tmpInfo = [streamReceiver.cameraList firstObject];
    CameraInfo *tmpInfo;
    
    if (streamReceiver.blnVehChannel) {
        tmpInfo = [streamReceiver.v_channelList firstObject];
    }
    else if (streamReceiver.blnDvrChannel) {
        tmpInfo = [streamReceiver.currentCHList firstObject];
    }
    else
        tmpInfo = [streamReceiver.cameraList firstObject];
    
    streamReceiver.currentPlayCH = tmpInfo.index-1;
    
    //20160127 modified by Ray Lin, consider for xms playback in different type
    if (device.streamType == STREAM_XMS) {
        if (tmpInfo.deviceType == XMS_CHANNEL) {
            [streamReceiver stopStreaming];
        }
        else
            [streamReceiver stopStreamingByView:tmpInfo.index-1];
    }
    else
        [streamReceiver stopStreaming];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)startSearch
{
    if (!self.eventView) {
        EventViewController *viewController = [[EventViewController alloc] initWithNibName:@"EventViewController" bundle:[NSBundle mainBundle]];
        self.eventView = viewController;
        [viewController release];
    }
    
    blnSearch = YES;
    self.eventView.streamReceiver = self.streamReceiver;
    self.eventView.sourceView = self.sourceView;
    if (streamReceiver.m_intdlsEnable == 1) {
        iSearchST = iSearchST - streamReceiver.m_dst_time_sec;
        iSearchET = iSearchET - streamReceiver.m_dst_time_sec;
    }
    self.eventView.iSearchST = iSearchST;
    self.eventView.iSearchET = iSearchET;
    self.eventView.eventFilter = eventFilter;
    [self.navigationController pushViewController:self.eventView animated:YES];
}

@end
