//
//  RootViewController.h
//  EFViewer
//
//  Created by Nobel Hsu on 2010/5/7.
//  Copyright EverFocus 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingViewController.h"
#import "DeviceViewController.h"
#import "GroupViewController.h"
#import "LiveViewController.h"
#import "IPViewController.h"
#import "XMSViewController.h"
#import "DBControl.h"
#import "StreamReceiver.h"
#import "EventNotifyListVC.h"  //20160706 ++ by Ray Lin

#define IMG_DVR [UIImage imageNamed:@"dvr.png"]
#define IMG_DVROVER [UIImage imageNamed:@"dvrover.png"]
#define IMG_IPCAM [UIImage imageNamed:@"ipcam.png"]
#define IMG_IPCAMOVER [UIImage imageNamed:@"ipcamover.png"]
#define IMG_EVENT [UIImage imageNamed:@"event.png"]
#define IMG_EVENTOVER [UIImage imageNamed:@"eventover.png"]
#define IMG_HELP [UIImage imageNamed:@"help.png"]
#define IMG_HELPOVER [UIImage imageNamed:@"helpover.png"]

enum {
    act_show = 0,
    act_back,
    act_select,
    act_secure,
} EFAlertType;

@interface RootViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate,UIActionSheetDelegate,UIGestureRecognizerDelegate>
{

	IBOutlet UITableView		*devTableView;
    IBOutlet UIButton           *dvrBtn;
    IBOutlet UIButton           *ipBtn;
    IBOutlet UIButton           *setBtn;
    IBOutlet UIButton           *eventBtn;
	NSInteger                   devListFilter;         //DVR:0, IPCAM:1
	NSMutableArray              *devArray;
    NSMutableArray              *groupArray;
    NSMutableArray              *groupFilterArray;
    
    UISearchDisplayController   *searchDisplay;
    UISearchBar                 *searchBar;
    NSMutableArray              *searchResult;
    
    UIActionSheet               *addMenu;
    UIActionSheet               *eventMenu;
    
    DBControl                   *dbControl;
    StreamReceiver              *streamReceiver;
    EFViewerAppDelegate         *appDelegate;
    EventTable                  *evn;
    
    BOOL                        blnEditing;
    BOOL                        blnEventMode;
}

@property(nonatomic,retain) SettingViewController *settingView;
@property(nonatomic,retain) DeviceViewController  *deviceView;
@property(nonatomic,retain) GroupViewController   *groupView;
@property(nonatomic,retain) LiveViewController    *liveView;
@property(nonatomic,retain) IPViewController      *ipView;
@property(nonatomic,retain) XMSViewController     *xmsView;
@property(nonatomic,retain) EventNotifyListVC     *notifyView;//20160706 ++ by Ray Lin

- (IBAction)devSegmentedControlChangeValueAction:(id)sender;
- (IBAction)openSetting;

@end
