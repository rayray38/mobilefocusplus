//
//  IPToolViewController.m
//  EFViewer
//
//  Created by James Lee on 13/4/10.
//  Copyright (c) 2013年 EverFocus. All rights reserved.
//

#import "IPToolViewController.h"
#import "EFViewerAppDelegate.h"

@implementation IPToolViewController

@synthesize device,productIdx;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *scanBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(scanStart)];
    [self.navigationItem setRightBarButtonItem:scanBtn];
    [scanBtn release];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];    //scanBtn
    [mask setFrame:self.view.frame];
    
    
    udpSender = [[UDPSender alloc] init];
    ws_discovery = [[WS_Discovery alloc] init];
    udp_deviceList = udpSender.deviceList;
    deviceList = ws_discovery.deviceList;
    
    decLabel.text = NSLocalizedString(@"MsgNoDevice", nil);
    
    udpSender.productFilter = self.productIdx;
    [mainTable reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self scanStart];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (!udpSender.blnStop) {
        [self scanStop];
    }
    if (!ws_discovery.blnStop) {
        [self scanStop];
    }
    
    [deviceList removeAllObjects];
    [udp_deviceList removeAllObjects];
}

- (void)dealloc
{
    SAVE_FREE(udpSender);
    SAVE_FREE(ws_discovery);
    SAVE_FREE(mainTable);
    SAVE_FREE(deviceList);
    SAVE_FREE(udp_deviceList);
    
    if (refreshTimer) {
        [refreshTimer invalidate];
        SAVE_FREE(refreshTimer);
    }
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - IPTool

- (void)scanStart
{
    repeatTime = 0;
    [mask setHidden:NO];
    [loadAct performSelectorOnMainThread:@selector(startAnimating) withObject:nil waitUntilDone:NO];
    [deviceList removeAllObjects];
    [udp_deviceList removeAllObjects];
    [mainTable reloadData];
    
    [udpSender start];
    if (self.productIdx == DEV_IPCAM) {
        [ws_discovery start];
    }
    
    refreshTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                    target:self
                                                  selector:@selector(refreshList)
                                                  userInfo:nil
                                                   repeats:YES];
    
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
}

- (void)scanStop
{
    if (udpSender.blnStop)        return;
    if (self.productIdx == DEV_IPCAM) {
        if (ws_discovery.blnStop)        return;
        [ws_discovery stop];
    }
    
    [udpSender stop];
    [refreshTimer invalidate];
    [mask setHidden:YES];
    [loadAct stopAnimating];
    
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
}

- (void)refreshList
{
    //檢查udp跟ws-discovery搜尋到的device有沒有重複，有的話以udp的為主
    BOOL blnExist = NO;
    for (int i=0; i<udp_deviceList.count; i++) {
        Device *curDev = [udp_deviceList objectAtIndex:i];
        for (int j=0; j<deviceList.count; j++) {
            Device *refInfo = [deviceList objectAtIndex:j];
            blnExist = [refInfo.ip isEqualToString:curDev.ip];
            
            if (blnExist)
                [deviceList removeObject:refInfo];
        }
    }
    [deviceList addObjectsFromArray:udp_deviceList];
    
    if (deviceList.count>50 || repeatTime>4) {
        [self scanStop];
        if (deviceList.count == 0) {
            //NSLog(@"There is no device in devicelist");
        }
    }
    
    repeatTime++;
    [mainTable reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Hide the tableView when list is empty
    if (deviceList.count == 0) {
        [mainTable setHidden:YES];
    }else {
        [mainTable setHidden:NO];
    }
    // Return the number of rows in the section.
    return deviceList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.showsReorderControl = YES;
		cell.selectionStyle = UITableViewCellSelectionStyleGray;
		
		// set background
		cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor EFCOLOR];
		cell.detailTextLabel.backgroundColor = [UIColor clearColor];
        cell.detailTextLabel.textColor = [UIColor lightTextColor];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
    }
    
    if (indexPath.row < deviceList.count) {
        
        Device *curInfo = [deviceList objectAtIndex:indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[curInfo.name stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",curInfo.ip];
    }else {
        
        cell.textLabel.text = nil;
        cell.detailTextLabel.text = nil;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < deviceList.count) {
        
        Device *curInfo = [deviceList objectAtIndex:indexPath.row];
        self.device.name = curInfo.name;
        self.device.ip = curInfo.ip;
        self.device.port = curInfo.port;
        self.device.product = curInfo.product;
        self.device.type = curInfo.type;
        self.device.streamType = curInfo.streamType;
        self.device.device_service = curInfo.device_service;
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    [mainTable reloadData];
}

@end
