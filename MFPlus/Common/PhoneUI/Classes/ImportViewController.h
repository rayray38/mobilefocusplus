//
//  ImportViewController.h
//  EFViewer
//
//  Created by James Lee on 13/8/26.
//  Copyright (c) 2013年 EverFocus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImportViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView		*devTableView;
    IBOutlet UIButton           *btnSelectAll;
    IBOutlet UIButton           *btnDeselect;
    
    NSMutableArray              *devList;
    NSMutableArray              *checkList;
}

@property(nonatomic,retain) NSString *resultString;

- (IBAction)selectAll;
- (IBAction)deselectAll;

@end
