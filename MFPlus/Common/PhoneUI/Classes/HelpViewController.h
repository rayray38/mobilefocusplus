//
//  HelpViewController.h
//  EFViewer
//
//  Created by James Lee on 13/4/16.
//  Copyright (c) 2013年 EverFocus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView  *helpView;
}

-(void)loadDocumentInHelpView;

@end
