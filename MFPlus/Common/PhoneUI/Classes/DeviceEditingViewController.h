//
//  DeviceEditingViewController.h
//  EFViewer
//
//  Created by Nobel Hsu on 2010/5/20.
//  Copyright 2010 EverFocus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"

@interface DeviceEditingViewController : UIViewController <UITextFieldDelegate>
{
}

@property (nonatomic,retain) IBOutlet UITextField		*textField;
@property (nonatomic,retain) IBOutlet UIPickerView		*pickerView;
@property (nonatomic,retain) NSArray *pickerViewArray;

@property (nonatomic,retain) NSString *editedPropertyKey;
@property (nonatomic,retain) NSString *editedPropertyDisplayName;

@property (nonatomic,retain) Device *device;

@end
