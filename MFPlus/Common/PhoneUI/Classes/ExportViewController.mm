//
//  ExportViewController.m
//  EFViewer
//
//  Created by James Lee on 13/8/23.
//  Copyright (c) 2013年 EverFocus. All rights reserved.
//

#import "ExportViewController.h"

@interface ExportViewController()
- (void)exportDeviceList;
- (NSString *)encodeString:(NSString *)string;
- (void)showAlarm:(NSString *)message;
@end

@implementation ExportViewController

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    devList = [[NSMutableArray alloc] init];
    checkList = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *btnGenerate = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"BtnExport", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(exportDeviceList)];
    [self.navigationItem setRightBarButtonItem:btnGenerate];
    [btnGenerate release];
    
    [self.view addSubview:resultView];
    
    [btnSelectAll setTitle:NSLocalizedString(@"BtnSelectAll", nil) forState:UIControlStateNormal];
    [btnDeselect setTitle:NSLocalizedString(@"BtnDeselectAll", nil) forState:UIControlStateNormal];
    [btnSave setTitle:NSLocalizedString(@"BtnSaveToAlbum", nil) forState:UIControlStateNormal];
    [btnCancel setTitle:NSLocalizedString(@"BtnCancel", nil) forState:UIControlStateNormal];
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.os_version >= 7.0) {
        CGRect tmpFrame = self.view.frame;
        tmpFrame.size.height -= TOOLBAR_HEIGHT_V;
        [devTableView setFrame:tmpFrame];
    }
    
    //EFMedia Tool
    mediaLibrary = appDelegate.mediaLibrary;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadAllDevices];
    
    [resultView setFrame:self.view.frame];
    [resultView setHidden:YES];
    
    [devTableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [devList removeAllObjects];
    [checkList removeAllObjects];
    
    [super viewWillDisappear:animated];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)dealloc
{
    if (devTableView) {
        [devTableView release];
        devTableView = nil;
    }
    
    if (devList) {
        [devList release];
        devList = nil;
    }
    if (checkList) {
        [checkList release];
        checkList = nil;
    }
    
    mediaLibrary = nil;
    
    [super dealloc];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action

- (void)exportDeviceList
{
    if (checkList.count == 0 || checkList.count > 16) {
        NSString *msgErr = checkList.count==0 ? @"MsgNoSelect" : @"MsgQRMax";
        [self showAlarm:NSLocalizedString(msgErr, nil)];
        return;
    }
#ifndef SIMULATOR
    //the qrcode is square. now we make it 250 pixels wide
    int qrcodeImageDimension = 300;
    
    //the string can be very long
    NSString *devCode = @"<MFQR>";
    for (Device *dev in checkList) {
        NSString *devCmd = [NSString stringWithFormat:@"<DEVICE>%@|%@|%ld|%ld|%ld|%ld|%@|%@|%ld",dev.name,dev.ip,(long)dev.port,(long)dev.type,(long)dev.streamType,(long)dev.product,dev.user,dev.password,(long)dev.rtspPort];
        devCode = [devCode stringByAppendingString:devCmd];
    }
    NSString *encStr = [self encodeString:devCode];
    
    //first encode the string into a matrix of bools, TRUE for black dot and FALSE for white. Let the encoder decide the error correction level and version
    DataMatrix* qrMatrix = [QREncoder encodeWithECLevel:QR_ECLEVEL_AUTO version:QR_VERSION_AUTO string:encStr];
    
    //then render the matrix
    UIImage* qrcodeImage = [QREncoder renderDataMatrix:qrMatrix imageDimension:qrcodeImageDimension];
    [qrImgView setImage:qrcodeImage];
    
    [resultView setHidden:NO];
#endif
}

- (IBAction)selectAll
{
    [checkList removeAllObjects];
    [checkList addObjectsFromArray:devList];
    
    [devTableView reloadData];
}

- (IBAction)deselectAll
{
    [checkList removeAllObjects];
    [devTableView reloadData];
}

- (void)loadAllDevices
{
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    for (NSMutableArray *tmpArray in appDelegate.dvrs)
        [devList addObjectsFromArray:tmpArray];
    
    for (NSMutableArray *tmpArray in appDelegate.ipcams)
    {
        for (Device *dev in tmpArray)   //modify rowID to avoid duplicate 
        {
            dev.rowID = devList.count;
            [devList addObject:dev];
        }
    }
}

#pragma mark - String Encoding

- (NSString *)encodeString:(NSString *)string
{
    
    if (string == nil)  return string;
    
    NSMutableString *resultString = [[[NSMutableString alloc] init] autorelease];
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    const Byte *byteArray = (const Byte *)[data bytes];
    int length = [data length];
    
    for (int i=0; i < length; i++)  {
        Byte byteValue = byteArray[i];
        byteValue ^= 117;
        
        [resultString appendFormat:@"%02x", byteValue];
    }
    
    return resultString;
}

#pragma mark - Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 20)] autorelease];
    [headerView setBackgroundColor:[UIColor BACOLOR]];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-10, 20)] autorelease];
    
    [titleLabel setText:NSLocalizedString(@"DeviceList", nil)];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [headerView addSubview:titleLabel];
    
    return headerView;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
    return devList.count;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
		cell.showsReorderControl = YES;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		// set background
		cell.textLabel.backgroundColor = [UIColor clearColor];
		cell.detailTextLabel.backgroundColor = [UIColor clearColor];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
        
        UIImageView *checkBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addDev1.png"]];
        [cell setAccessoryView:checkBtn];
        [checkBtn release];
        [cell.accessoryView setHidden:YES];
    }
    
	// Configure the cell.
	Device *dev = [devList objectAtIndex:indexPath.row];
	cell.textLabel.text = dev.name;
    
    BOOL bFind = NO;
    for (Device *tmpDev in checkList)
    {
        if (dev.rowID == tmpDev.rowID) {
            bFind = YES;
            break;
        }
    }
    
    [cell.accessoryView setHidden:!bFind];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // set background
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor lightTextColor];
    cell.detailTextLabel.textColor = [UIColor lightTextColor];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    Device *dev = [devList objectAtIndex:indexPath.row];
    if (checkList.count == 0) {
        
        [checkList addObject:dev];
    }else {
        
        BOOL bFind = NO;
        for (Device *tmpDev in checkList) {
            if ([tmpDev isEqual:dev]) {
                
                [checkList removeObject:dev];
                bFind = YES;
                break;
            }
        }
        
        if (!bFind)
            [checkList addObject:dev];
    }
    
    [devTableView reloadData];
}

#pragma mark - UIActionSheet delegate

- (IBAction)saveToAlbum
{
    //UIImageWriteToSavedPhotosAlbum(qrImgView.image, nil, nil, nil);
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *albumName = [NSString stringWithFormat:@"%@",appDelegate._AppName];
    [mediaLibrary saveImage:qrImgView.image toAlbum:albumName withCompletionBlock:^(NSError *error) {
        if (error!=nil) {
            [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
        }else {
            [self showAlarm:NSLocalizedString(@"MsgSaveOK", nil)];
        }
    }];
    [qrImgView setImage:nil];
    [resultView setHidden:YES];
}

- (IBAction)hideQRView
{
    [qrImgView setImage:nil];
    [resultView setHidden:YES];
}

#pragma mark - Alarm Delegate

- (void)showAlarm:(NSString *)message
{
    if ( message!=nil ) {
		
		// open an alert with just an OK button, touch "OK" will do nothing
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];[alert show];
	}
}

@end
