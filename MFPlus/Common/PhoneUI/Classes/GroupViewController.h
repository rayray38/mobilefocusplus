//
//  GroupViewController.h
//  EFViewerPlus
//
//  Created by James Lee on 2014/6/9.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFViewerAppDelegate.h"

@interface GroupViewController : UIViewController<UITableViewDataSource,UITableViewDelegate
                                                ,UISearchBarDelegate,UISearchDisplayDelegate
                                                ,UIAlertViewDelegate>
{
    IBOutlet UITableView  *devTableView;
    NSMutableArray        *selectArray;
    
    UISearchDisplayController   *searchDisplay;
    UISearchBar                 *searchBar;
    NSMutableArray              *searchResult;
}

@property(nonatomic, retain) DeviceGroup        *group;
@property(nonatomic, assign) NSMutableArray     *devArray;
@property(nonatomic, assign) NSMutableArray     *curGArray;
@property(nonatomic)         BOOL               blnNewGroup;
@property(nonatomic)         NSInteger          devType;

@end
