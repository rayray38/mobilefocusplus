//
//  SettingViewController.m
//  EFViewer
//
//  Created by James Lee on 13/8/22.
//  Copyright (c) 2013年 EverFocus. All rights reserved.
//

#import "SettingViewController.h"
#import "EFViewerAppDelegate.h"
#import "EzAccessoryView.h"

@interface SettingViewController()
- (void)accessoryButtonTappedForRowWithIndexPath:(id)sender;
- (void)setScanViewLayout;
- (void)openScanner;
- (void)openLibrary;
- (BOOL)validateQRCode:(NSString *)code;
- (NSString *)decodeString:(NSString *)string;
- (void)showAlarm:(NSString *)message;

@end

@implementation SettingViewController
{
    NSString *appName;
}

@synthesize helpView,importView,exportView;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    loadMenu = [[UIActionSheet alloc] initWithTitle:nil
                                             delegate:self
                                    cancelButtonTitle:NSLocalizedString(@"BtnCancel", nil)
                               destructiveButtonTitle:nil
                                    otherButtonTitles:NSLocalizedString(@"BtnScanQRCode", nil), NSLocalizedString(@"BtnLoadQRCode", nil), nil];

    [ZBarReaderView class];
    self.readerView.readerDelegate = self;
    
    [btnCancel setTitle:NSLocalizedString(@"BtnCancel", nil)];
    
    [self.view addSubview:scannerView];
    
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.os_version >= 7.0) {
        CGRect tmpFrame = self.view.frame;
        tmpFrame.size.height -= TOOLBAR_HEIGHT_V;
        [mainTable setFrame:tmpFrame];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setScanViewLayout];
    
    appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    
    [scannerView setHidden:YES];
    [mainTable reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (!scannerView.isHidden) {
        
        [self.readerView stop];
        [scannerView setHidden:YES];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)setScanViewLayout
{
    [scannerView setFrame:self.view.frame];
}

- (void)dealloc
{
    SAVE_FREE(mainTable);
    SAVE_FREE(self.helpView);
    SAVE_FREE(self.importView);
    SAVE_FREE(self.exportView);
    SAVE_FREE(self.readerView);
    
    [super dealloc];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger   ret = 1;
    switch (section) {
        case SECTION_UM:
            ret = 2;
            break;
        case SECTION_DIO:
            ret = 2;
            break;
        case SECTION_CONFIG:
            ret = 1;
            break;
    }
    return ret;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 20)] autorelease];
    [headerView setBackgroundColor:[UIColor BACOLOR]];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-10, 20)] autorelease];
    NSString *title = nil;
    switch (section) {
        case SECTION_UM:
            title = NSLocalizedString(@"Help", nil);
            break;
        case SECTION_DIO:
            title = NSLocalizedString(@"DeviceIO", nil);
            break;
        case SECTION_CONFIG:
            title = NSLocalizedString(@"Configuration", nil);
            break;
    }
    
    [titleLabel setText:title];
    //[titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [headerView addSubview:titleLabel];
    
    return headerView;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		[cell setEditingAccessoryType:UITableViewCellAccessoryNone];
		[cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setTextColor:[UIColor lightTextColor]];
        cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
        
        EZAccessoryView *ezView = [[EZAccessoryView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [ezView.detailButton addTarget:self action:@selector(accessoryButtonTappedForRowWithIndexPath:) forControlEvents:UIControlEventTouchDown];
        [ezView.detailSwitch addTarget:ezView action:@selector(changeDetailSetting:) forControlEvents:UIControlEventValueChanged];
        
        [cell setAccessoryView:ezView];
        [ezView release];
	}
	
	// Configure the cell...
    EZAccessoryView *ezView = (EZAccessoryView *)cell.accessoryView;
    [ezView setDetailType:edt_none];
    if (indexPath.section == SECTION_UM) {
        
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = NSLocalizedString(@"UserManual", nil);
                break;
            case 1:
                cell.textLabel.text = NSLocalizedString(@"ReleaseNote", nil);
                break;
        }
        return cell;
    } else if (indexPath.section == SECTION_CONFIG) {
        
        cell.textLabel.text = NSLocalizedString(@"SmoothMode", nil);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [ezView setDetailType:edt_switch];
        [ezView.detailSwitch setOn:[EZAccessoryView getDetailSettingBy:KEY_SMOOTHMODE]];
        [ezView.detailSwitch setTag:edm_smoothmode];
        return cell;
    }
    
	switch (indexPath.row) {
		case 0:
            cell.textLabel.text = NSLocalizedString(@"DeviceImport",nil);
			break;
		case 1:
			cell.textLabel.text = NSLocalizedString(@"DeviceExport",nil);
			break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_CONFIG) {
        // do nothing
        return;
    } else if (indexPath.section == SECTION_UM) {
        
        switch (indexPath.row) {
            case 0:
                [self showAlarmAndSelect:[NSString stringWithFormat:@"%@", MsgUserNotify]];
                break;
            case 1:
                [self showAlarm:NSLocalizedString(@"MsgUpdate", nil)];
                break;
        }
    } else {
    
        switch (indexPath.row) {
            case ROW_IMPORT:
                [loadMenu showInView:self.view];
                break;
                
            case ROW_EXPORT:
                if (!self.exportView) {
                    ExportViewController *viewController = [[ExportViewController alloc] initWithNibName:@"ExportViewController" bundle:[NSBundle mainBundle]];
                    self.exportView = viewController;
                    [viewController release];
                }
                [self.navigationController pushViewController:self.exportView animated:YES];
                break;
        }
    }
    [mainTable reloadData];
}

- (void)accessoryButtonTappedForRowWithIndexPath:(id)sender
{
}

#pragma mark - Action

- (void)openScanner
{
    [scannerView setHidden:NO];
    [self.readerView start];
}

- (void)closeScanner
{
    [scannerView setHidden:YES];
    [self.readerView stop];
}

- (void)openLibrary
{
    ZBarReaderController *reader = [ZBarReaderController new];
    reader.readerDelegate = self;
    [reader.scanner setSymbology:ZBAR_QRCODE config: ZBAR_CFG_ENABLE to: 1];
    reader.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    reader.showsZBarControls = NO;
    reader.showsHelpOnFail = NO;
    [self presentViewController:reader animated:YES completion:nil];
}

- (void)openHelpView
{
    if (!self.helpView) {
        HelpViewController *viewController = [[HelpViewController alloc] initWithNibName:@"HelpViewController" bundle:[NSBundle mainBundle]];
        self.helpView = viewController;
        SAVE_FREE(viewController);
    }
    [self.navigationController pushViewController:self.helpView animated:YES];
}

- (BOOL)validateQRCode:(NSString *)code
{
    BOOL ret = NO;
    
    NSString *decCode = [self decodeString:code];
    NSString *marker = [decCode substringToIndex:6];
    if ([marker isEqualToString:@"<MFQR>"]) {
        ret = YES;
    }
    
    return ret;
}

- (NSString *)decodeString:(NSString *)string
{
    //  Convert incoming string to ASCII data first
    NSData *data = [string dataUsingEncoding:NSASCIIStringEncoding];
    const Byte *incomingArray = (const Byte *)[data bytes];
    
    //  Allocate decode buffer
    NSUInteger charCount = [string length]/2;
    Byte *byteArray = (Byte *)malloc(charCount);
    
    int sourceOffset = 0;
    int targetOffset = 0;
    for (int i=0; i<charCount; i++)
    {
        Byte highByte = incomingArray[sourceOffset+0];
        Byte lowByte = incomingArray[sourceOffset+1];
        sourceOffset += 2;
        
        if (highByte >= '0' && highByte <= '9')  {highByte -= '0';}
        if (highByte >= 'a' && highByte <= 'f')  {highByte -= ('a'-10);}
        
        if (lowByte >= '0' && lowByte <= '9')  {lowByte -= '0';}
        if (lowByte >= 'a' && lowByte <= 'f')  {lowByte -= ('a'-10);}
        
        Byte charValue = (highByte<<4)|lowByte;
        
        //  Decryption
        charValue ^= 117;
        
        byteArray[targetOffset] = charValue;
        targetOffset++;
    }
    
    //  Convert byte array to NSString
    NSString *decodedString = [[NSString alloc] initWithBytes:byteArray length:charCount encoding:NSUTF8StringEncoding];
    free(byteArray);
    
    return decodedString;
}

#pragma mark - UIActionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self openScanner];
            break;
            
        case 1:
            [self openLibrary];
            break;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
}

#pragma mark - ZBarSDK delegate

- (void) readerView:(ZBarReaderView*)view
     didReadSymbols:(ZBarSymbolSet*)syms
          fromImage:(UIImage*)img
{
    [self.readerView stop];
    [scannerView setHidden:YES];
    
    // do something useful with results
    ZBarSymbol *symbol;
    for(symbol in syms)
        break;
    
    if ([self validateQRCode:symbol.data]) {
        
        if (!self.importView) {
            ImportViewController *viewController = [[ImportViewController alloc] initWithNibName:@"ImportViewController" bundle:[NSBundle mainBundle]];
            self.importView = viewController;
            [viewController release];
        }
        
        self.importView.resultString = [self decodeString:symbol.data];
        [self.navigationController pushViewController:self.importView animated:YES];
        
    }else {
        [self showAlarm:NSLocalizedString(@"MsgInvalidQR", nil)];
        [mainTable reloadData];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];
    ZBarSymbol * symbol;
    for(symbol in results)
        break;
    
    if ([self validateQRCode:symbol.data]) {
        
        if (!self.importView) {
            ImportViewController *viewController = [[ImportViewController alloc] initWithNibName:@"ImportViewController" bundle:[NSBundle mainBundle]];
            self.importView = viewController;
            [viewController release];
        }
        
        self.importView.resultString = [self decodeString:symbol.data];
        [self.navigationController pushViewController:self.importView animated:YES];
        
    }else {
        [self showAlarm:NSLocalizedString(@"MsgInvalidQR", nil)];
        [mainTable reloadData];
    }
}

#pragma mark - Alarm Delegate

- (void)showAlarm:(NSString *)message
{
    if ( message!=nil ) {
		
		// open an alert with just an OK button, touch "OK" will do nothing
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appName message:message
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        alert.tag = ACT_SHOW;
		
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
	}
}

- (void)showAlarmAndSelect:(NSString *)message
{
    if (message != nil) {
        if ([UIAlertController class]) {    //iOS 8 and above support UIAlertController
            
            UIAlertController *notifyMenu = [UIAlertController alertControllerWithTitle:appName
                                                                              message:message
                                                                       preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *actOK = [UIAlertAction actionWithTitle:NSLocalizedString(@"BtnOK", nil)
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction *action) {
                                                                [self openHelpView];
                                                            }];
            
            UIAlertAction *actCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"BtnCancel", nil)
                                                                style:UIAlertActionStyleCancel
                                                              handler:^(UIAlertAction *action) {
                                                                  [mainTable reloadData];
                                                              }];
            [notifyMenu addAction:actOK];
            [notifyMenu addAction:actCancel];
            [self presentViewController:notifyMenu animated:YES completion:nil];
            [notifyMenu.view setTintColor:[UIColor darkTextColor]];
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appName
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"BtnCancel", nil)
                                                  otherButtonTitles:NSLocalizedString(@"BtnOK", nil), nil];
            
            alert.tag = ACT_HELP;
            
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
            [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
        }
    }
}

#pragma mark - UIAlerView Delegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (actionSheet.tag) {
        case ACT_SHOW:
            //do nothing
            break;
            
        case ACT_HELP:
            if (buttonIndex == 0) {  //This is cancel
                [mainTable reloadData];
            }
            else if (buttonIndex == 1) {  //This is OK
                
                [self openHelpView];
            }
            break;
            
        default:
            break;
    }
}

@end
