//
//  IPToolViewController.h
//  EFViewer
//
//  Created by James Lee on 13/4/10.
//  Copyright (c) 2013年 EverFocus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
#import "UDPSender.h"
#import "WS-Discovery.h"

@interface IPToolViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView      *mainTable;
    IBOutlet UILabel          *decLabel;
    IBOutlet UIView           *mask;
    IBOutlet UIActivityIndicatorView    *loadAct;
    Device                    *device;
    UDPSender                 *udpSender;
    WS_Discovery              *ws_discovery;
    NSMutableArray            *deviceList;
    NSMutableArray            *udp_deviceList;
    
    NSTimer                   *refreshTimer;
    NSInteger                 repeatTime;
}

@property(nonatomic,retain) Device          *device;
@property(nonatomic)        NSInteger       productIdx;

- (void)scanStart;
- (void)scanStop;
- (void)refreshList;

@end
