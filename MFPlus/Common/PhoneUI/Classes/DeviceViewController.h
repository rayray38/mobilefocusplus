//
//  DeviceViewController.h
//  EFViewerHD
//
//  Created by Nobel Hsu on 12/10/8.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFViewerAppDelegate.h"
#import "Device.h"
#import "DeviceEditingViewController.h"
#import "IPToolViewController.h"
#import "CommandSender.h"
#import "StreamReceiver.h"


@interface DeviceViewController : UITableViewController {
    
	Device                      *device;
	Device                      *deviceCopy;
	NSArray                     *dvrTypeArray;
	NSArray                     *ipcamTypeArray;
	DeviceEditingViewController *editingView;
    IPToolViewController        *scanView;
    CommandSender               *cmdSender;
    StreamReceiver              *streamReceiver;
	
	BOOL                        newDevice;
    UISegmentedControl	        *channelSwitch;
}

@property(nonatomic,retain) Device *device;
@property(nonatomic,retain) Device *deviceCopy;
@property(nonatomic,retain) DeviceEditingViewController *editingView;
@property(nonatomic,retain) IPToolViewController        *scanView;
@property(nonatomic) BOOL newDevice;

@end

