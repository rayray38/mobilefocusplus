    //
//  DeviceEditingViewController.m
//  EFViewer
//
//  Created by Nobel Hsu on 2010/5/20.
//  Copyright 2010 EverFocus. All rights reserved.
//

#import "DeviceEditingViewController.h"


@implementation DeviceEditingViewController

@synthesize textField,pickerView,pickerViewArray,editedPropertyKey,editedPropertyDisplayName,device;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    MMLog(@"[DEV] Init %@",self);
    return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	pickerView.showsSelectionIndicator = YES;
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backBtn.png"]
                                                                style:UIBarButtonItemStyleBordered
                                                               target:self
                                                               action:@selector(editingDone)];
    self.navigationItem.leftBarButtonItem = doneBtn;
    SAVE_FREE(doneBtn);
}

- (void)viewWillAppear:(BOOL)animated {

	[super viewWillAppear:animated];
	
	self.title = editedPropertyDisplayName;
    [textField setText:@""];
    [textField setHidden:NO];
    [textField setPlaceholder:editedPropertyDisplayName];
    [textField setSecureTextEntry:NO];
    [pickerView setHidden:YES];
	
	if ( [editedPropertyKey isEqualToString:@"name"] ) {
		
		textField.text = device.name;
		textField.keyboardType = UIKeyboardTypeDefault;
	}
	else if ( [editedPropertyKey isEqualToString:@"type"] ) {
		[pickerView reloadAllComponents];
        [pickerView setHidden:NO];
        [textField setHidden:YES];
		
		NSInteger index = [pickerViewArray indexOfObject:[Device modelIndexToString:device.type product:device.product]];
		[pickerView selectRow:index inComponent:0 animated:YES];
	}
	else if ( [editedPropertyKey isEqualToString:@"ip"] ) {
        textField.text = device.ip;
		textField.keyboardType = UIKeyboardTypeURL;
	}
	else if ( [editedPropertyKey isEqualToString:@"port"] ) {
		if (device.port != 0 )
			textField.text = [NSString stringWithFormat:@"%ld",(long)device.port];
		textField.placeholder = editedPropertyDisplayName;
		textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
	}
	else if ( [editedPropertyKey isEqualToString:@"user"] ) {
		textField.text = device.user;
		textField.keyboardType = UIKeyboardTypeDefault;
	}
	else if ( [editedPropertyKey isEqualToString:@"password"] ) {
		textField.text = device.password;
		textField.keyboardType = UIKeyboardTypeDefault;
        [textField setSecureTextEntry:YES];
	}
    else if ( [editedPropertyKey isEqualToString:@"rtspport"] ) 
    {
		if (device.rtspPort != 0 )
			textField.text = [NSString stringWithFormat:@"%ld",(long)device.rtspPort];
		textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
	}
    
    if (![editedPropertyKey isEqualToString:@"type"])
        [textField becomeFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if ( [editedPropertyKey isEqualToString:@"name"] ) {
		
		device.name = textField.text;
	}
	else if ( [editedPropertyKey isEqualToString:@"type"] ) {
		
		device.type = [Device modelStringToIndex:[pickerViewArray objectAtIndex:[pickerView selectedRowInComponent:0]] product:device.product];
		device.streamType = [Device getStreamType:device.type product:device.product];
	}
	else if ( [editedPropertyKey isEqualToString:@"ip"] ) {
        if (device.type == RTSP) {
            // check and add "rtsp://"        20151110 ++ by Ray Lin
            NSString *result = nil;
            NSString *trimmedStr = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if ( (trimmedStr != nil) && (trimmedStr.length != 0) ) {
                NSRange schemeMarkerRange = [trimmedStr rangeOfString:@"://"];
                
                if (schemeMarkerRange.location == NSNotFound) {
                    result = [[@"rtsp://" stringByAppendingString:trimmedStr] retain];
                }
                else {
                    result = [trimmedStr retain];
                }
            }
            
            device.ip= result;
            SAVE_FREE(result);
        }
        else {
            // check and remove "http://"
            NSString *result = nil;
            NSString *trimmedStr = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if ( (trimmedStr != nil) && (trimmedStr.length != 0) ) {
                NSRange schemeMarkerRange = [trimmedStr rangeOfString:@"://"];
                
                if (schemeMarkerRange.location != NSNotFound) {
                    result = [[trimmedStr substringFromIndex:schemeMarkerRange.location+3] retain];
                }
                else {
                    result = [trimmedStr retain];
                }
            }
            
            device.ip = result;
            SAVE_FREE(result);
        }
	}
	else if ( [editedPropertyKey isEqualToString:@"port"] ) {
		
		device.port = [textField.text intValue];
	}
	else if ( [editedPropertyKey isEqualToString:@"user"] ) {
			
		device.user = textField.text;
	}
	else if ( [editedPropertyKey isEqualToString:@"password"] ) {
		
		device.password = textField.text;
	}
    else if ( [editedPropertyKey isEqualToString:@"rtspport"] ) 
    {//add by robert hsu 20120203
		
		device.rtspPort = [textField.text intValue];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    MMLog(@"[DEV] dealloc %@",self);
    SAVE_FREE(pickerViewArray);
    SAVE_FREE(editedPropertyKey);
    SAVE_FREE(editedPropertyDisplayName);
    SAVE_FREE(device);
	
    [super dealloc];
}

#pragma mark - Action

- (void)editingDone
{
    [textField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - PickerViewDelegate


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}

#pragma mark UIPickerViewDataSource

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	NSString *returnStr = @"";
	
	if (component == 0)
	{
		returnStr = [pickerViewArray objectAtIndex:row];
	}
	
	return returnStr;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
	CGFloat componentWidth = 0.0;
	
	if (component == 0)
		componentWidth = 280.0;	// first column size is wider to hold names
	
	return componentWidth;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
	return 40.0;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return [pickerViewArray count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

@end
