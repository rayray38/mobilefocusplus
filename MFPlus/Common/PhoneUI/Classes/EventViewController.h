//
//  EventViewController.h
//  EFViewer
//
//  Created by James Lee on 13/4/8.
//  Copyright (c) 2013年 EverFocus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StreamReceiver.h"

#define EVENT_ALARM  1
#define EVENT_MOTION 4
#define EVENT_VLOSS  8

#define IMG_ALARM2      [UIImage imageNamed:@"alarm2.png"]
#define IMG_MOTION2     [UIImage imageNamed:@"motion2.png"]
#define IMG_VLOSS2      [UIImage imageNamed:@"videoloss2.png"]

@interface EventViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView            *mainTable;
    IBOutlet UIView                 *maskView;
    IBOutlet UIActivityIndicatorView *loadAct;
    
    StreamReceiver                  *streamReceiver;
    
    NSDateFormatter                 *dateFormatter;
    NSMutableArray                  *eventList;
    
    BOOL                            blnFullScreen;
}

@property(nonatomic,retain) StreamReceiver          *streamReceiver;
@property(nonatomic,retain) UIViewController        *sourceView;
@property(nonatomic)        NSUInteger              iSearchST;
@property(nonatomic)        NSUInteger              iSearchET;
@property(nonatomic)        NSUInteger              eventFilter;

- (void)searchProcess;
- (UIImage *)eventParser:(NSInteger)type;
- (void)setMask;

@end
