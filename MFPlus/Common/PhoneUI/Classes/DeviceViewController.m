//
//  DeviceViewController.m
//  EFViewerHD
//
//  Created by Nobel Hsu on 12/10/8.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "DeviceViewController.h"
#import "EzAccessoryView.h"

enum {
#define MAX_ROW_ITEM    9
    edr_name = 0x0,
    edr_model,
    edr_url,
    edr_port,
    edr_user,
    edr_pwd,
    edr_sub,
    edr_push,
    edr_scan,
} EDeviceRow;

@interface DeviceViewController ()

- (void)saveDevice;
- (void)subSwitchChangeValue:(id)sender;
- (void)subSwitchCloseNotify:(id)sender;
- (void)showAlarm:(NSString *)message;
- (BOOL)ifSupportMobileStream:(Device *)dev;

@end

@implementation DeviceViewController

@synthesize device,deviceCopy,editingView,newDevice,scanView;

#pragma mark - View lifecycle

- (void)viewDidLoad
{	
    [super viewDidLoad];
    
    MMLog(@"[DV] Init %@",self);
    
    UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                             target:self
                                                                             action:@selector(saveDevice)];
    [self.navigationItem setRightBarButtonItem:saveBtn];
    SAVE_FREE(saveBtn);
	
	dvrTypeArray = [Device createDvrModelArray];
	ipcamTypeArray = [Device createIpcamModelArray];
}

- (void)viewWillAppear:(BOOL)animated
{    
    [super viewWillAppear:animated];
		
    self.title = newDevice ? NSLocalizedString(@"AddDevice",nil) : deviceCopy.name;
    
	[self.tableView reloadData];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Actions

- (void)saveDevice
{
	// check if all items have been filled
    if (ZEROLENGTH(deviceCopy.name) || ZEROLENGTH(deviceCopy.ip)
        /*|| ZEROLENGTH(deviceCopy.user) || ZEROLENGTH(deviceCopy.password)*/
        || deviceCopy.port == 0)
    {
		
		[self showAlarm:NSLocalizedString(@"MsgCheckAllItem", nil)];
	}
	else {
		
        EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
        
        if (newDevice) {
            
            NSMutableArray *tmpArray = (deviceCopy.product == DEV_DVR ? [appDelegate.dvrs lastObject] : [appDelegate.ipcams lastObject]);
            [tmpArray addObject:deviceCopy];
        }
        else {
            [device setValueWithDevice:deviceCopy];
        }
        
        BOOL ret = NO;
        // save it to database
        if ( deviceCopy.product==DEV_DVR )
            ret = [appDelegate refreshDVRsIntoDatabase];
        else
            ret = [appDelegate refreshIPCamsIntoDatabase];
        
        if (!ret)
            [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            // Register deviceToken to server
            if (deviceCopy.type == XMS_SERIES) {
                
                if (appDelegate._DeviceToken) {
                    streamReceiver = [[StreamReceiver alloc] initWithDevice:deviceCopy];
                    NSArray *ary = [NSArray arrayWithObjects:appDelegate._DeviceToken, deviceCopy.name, nil];
                    [streamReceiver registDevTokenToServer:ary];
                    if (streamReceiver.xmsUUID != nil) {
                        
                        deviceCopy.uuid = [streamReceiver.xmsUUID copy];
                        [device setValueWithDevice:deviceCopy];
                        [appDelegate refreshDVRsIntoDatabase];
                    }
                    [streamReceiver release];
                }
                else
                    DBGLog(@"[DV] DeviceToken nil");
            }
        });
        
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (void)subSwitchChangeValue:(id)sender
{
    UISwitch *subSwitch = (UISwitch *)sender;
	if (subSwitch.isOn) {
		
		if ( ![self ifSupportMobileStream:deviceCopy] ) {
			
			[self showAlarm:NSLocalizedString(@"MsgSubDenied", nil)];
			[subSwitch setOn:NO animated:YES];
		}
		else
			deviceCopy.dualStream = 1;
	}
	else
		deviceCopy.dualStream = 0;
}

- (void)subSwitchCloseNotify:(id)sender
{
    UISwitch *subSwitch = (UISwitch *)sender;
    if (subSwitch.isOn) {
        
        deviceCopy.blnCloseNotify = 0;   //receive remote notify
    }
    else
        deviceCopy.blnCloseNotify = 1;  //do not receive remote notify
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    //return newDevice ? 2 : 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
//    if (section == 1) {
//        return 1;
//    }
//    
//    if (deviceCopy.product == DEV_IPCAM && deviceCopy.streamType > STREAM_HDIP)
//        return MAX_ROW_ITEM;
    
    //return MAX_ROW_ITEM - 1;
    return MAX_ROW_ITEM;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 1 ? 0 : 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return nil;
    }
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 25, tableView.bounds.size.width, 30)] autorelease];
    [headerView setBackgroundColor:[UIColor BACOLOR]];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.bounds.size.width-10, 18)] autorelease];
    NSString *title = @"";
    
    switch ( deviceCopy.product ) {
		case DEV_DVR:
			title = @"DVR";
			break;
		case DEV_IPCAM:
			title = @"IPCam";
			break;
	}
    
    [titleLabel setText:title];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    if (indexPath.row == edr_push) {
        if (deviceCopy.type == XMS_SERIES) {
            height = 44;
        }
        else
            height = 0;
    }
    else if (indexPath.row == edr_port) {
        if (deviceCopy.type == RTSP || [deviceCopy.ip rangeOfString:@".everfocusddns.com" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            height = 0;
        }
        else
            height = 44;
    }
    else if (indexPath.row == edr_sub) {
        if (deviceCopy.type == RTSP) {
            height = 0;
        }
        else
            height = 44;
    }
    else
        height = 44;
    
    return height;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		[cell setEditingAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		[cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setTextColor:[UIColor EFCOLOR]];
        [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [cell.textLabel setNumberOfLines:0];
        [cell.detailTextLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [cell.detailTextLabel setNumberOfLines:0];
        [cell.detailTextLabel setTextColor:[UIColor lightTextColor]];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
        
        if (indexPath.row == edr_sub) {
            
            //EzAccessoryView
            EZAccessoryView *ezView = [[EZAccessoryView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
            [ezView.detailSwitch addTarget:self action:@selector(subSwitchChangeValue:) forControlEvents:UIControlEventValueChanged];
            
            [cell setAccessoryView:ezView];
            SAVE_FREE(ezView);
        }
        
        if (indexPath.row == edr_push) {
            
            //EzAccessoryView
            EZAccessoryView *ezView = [[EZAccessoryView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
            [ezView.detailSwitch addTarget:self action:@selector(subSwitchCloseNotify:) forControlEvents:UIControlEventValueChanged];
            
            [cell setAccessoryView:ezView];
            SAVE_FREE(ezView);
        }
	}
	
	// Configure the cell...
//    if (indexPath.section == 1) {
//        cell.textLabel.text = NSLocalizedString(@"ScanDevice", nil);
//        cell.detailTextLabel.text = nil;
//        return cell;
//    }
    
    EZAccessoryView *ezView = (EZAccessoryView *)cell.accessoryView;
	switch (indexPath.row)
    {
		case edr_name:
            cell.textLabel.text = NSLocalizedString(@"DeviceName",nil);
			cell.detailTextLabel.text = deviceCopy.name;
			break;
		case edr_model:
			cell.textLabel.text = NSLocalizedString(@"DeviceModel",nil);
			cell.detailTextLabel.text = [Device modelIndexToString:deviceCopy.type product:deviceCopy.product];
            //20151113 modified by Ray Lin , after search model name cannot change to ONVIF
//            if (deviceCopy.streamType == STREAM_ONVIF && deviceCopy.type != RTSP) {
//                NSInteger index = [WS_Discovery checkModelByName:deviceCopy.name];
//                cell.detailTextLabel.text = [Device modelIndexToString:index product:deviceCopy.product];
//            }
			break;
		case edr_url:
            cell.textLabel.text = NSLocalizedString(@"DeviceIP",nil);
            cell.detailTextLabel.text = deviceCopy.ip;
			break;
		case edr_port:
            if (deviceCopy.type == RTSP || [deviceCopy.ip rangeOfString:@".everfocusddns.com" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [cell setHidden:YES];
            }
            else {
                cell.textLabel.text = NSLocalizedString(@"DevicePort",nil);
                cell.detailTextLabel.text = @"";
                if (deviceCopy.port!=0)
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld",(long)deviceCopy.port];
            }
			break;
		case edr_user:
			cell.textLabel.text = NSLocalizedString(@"DeviceUser",nil);
			cell.detailTextLabel.text = deviceCopy.user;
			break;
		case edr_pwd:
			cell.textLabel.text = NSLocalizedString(@"DevicePWD",nil);
            cell.detailTextLabel.text = [@"" stringByPaddingToLength:[deviceCopy.password length]
                                                          withString:@"*" startingAtIndex:0];
			break;
		case edr_sub:
            if (deviceCopy.type != RTSP) {
                cell.textLabel.text = NSLocalizedString(@"DeviceSub",nil);
                [ezView setDetailType:edt_switch];
                if (deviceCopy.product == DEV_DVR) {
                    if (deviceCopy.type==ECOR264_8D1 || deviceCopy.type==ELR_8D_8F || deviceCopy.type==EPARA264_32) {
                        deviceCopy.dualStream = 0;
                    }
                }
                [ezView.detailSwitch setOn:(deviceCopy.dualStream==1?YES:NO) animated:NO];
            }
            else
                [cell setHidden:YES];
            
			break;
//        case edr_rtsp:
//            cell.textLabel.text = NSLocalizedString(@"DeviceRTSP", nil);
//            cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld",(long)deviceCopy.rtspPort];
//            break;
            
        case edr_push:
            if (deviceCopy.type == XMS_SERIES) {
                cell.textLabel.text = NSLocalizedString(@"NotifyReceive", nil);
                [ezView setDetailType:edt_switch];
                [ezView.detailSwitch setOn:(deviceCopy.blnCloseNotify==0?YES:NO) animated:NO];
            }
            else
                [cell setHidden:YES];
            break;
            
        case edr_scan:
            if (newDevice) {
                cell.textLabel.font = [UIFont boldSystemFontOfSize:20];
                cell.textLabel.text = NSLocalizedString(@"ScanDevice", nil);
                cell.detailTextLabel.text = nil;
            }
            else
                [cell setHidden:YES];
            break;
	}
    
    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	// Do not allow rows to be deleted when in editing mode.
	return UITableViewCellEditingStyleNone;
}


- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
	// Do not indent rows when in editing mode.
	return NO;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (indexPath.row == edr_sub || indexPath.row == edr_push) ? nil : indexPath;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //if (indexPath.section == 1) {
    if (indexPath.row == edr_scan) {
        //push to ScanView here
        if (!self.scanView)      //init ScanView
        {
            IPToolViewController *viewController = [[IPToolViewController alloc] initWithNibName:@"IPToolViewController" bundle:[NSBundle mainBundle]];
            self.scanView = viewController;
            [viewController release];
        }
        
        self.scanView.productIdx = deviceCopy.product;
        self.scanView.device = deviceCopy;
        [self.navigationController pushViewController:self.scanView animated:YES];
        return;
    }
	
	if (self.editingView == nil)    //init DeviceEditingView
	{
		DeviceEditingViewController *viewController = [[DeviceEditingViewController alloc] initWithNibName:@"DeviceEditingViewController" bundle:[NSBundle mainBundle]];
		self.editingView = viewController;
		[viewController release];
	}
	
	if ( deviceCopy.product==DEV_DVR) {
		self.editingView.pickerViewArray = dvrTypeArray;
	}
	else {
		self.editingView.pickerViewArray = ipcamTypeArray;
	}
	
    self.editingView.device = deviceCopy;
	
	switch (indexPath.row) {
        case edr_name:
			self.editingView.editedPropertyKey = @"name";
			self.editingView.editedPropertyDisplayName = NSLocalizedString(@"DeviceName",nil);
			break;
		case edr_model:
			self.editingView.editedPropertyKey = @"type";
			self.editingView.editedPropertyDisplayName = NSLocalizedString(@"DeviceModel",nil);
			break;
		case edr_url:
			self.editingView.editedPropertyKey = @"ip";
			self.editingView.editedPropertyDisplayName = NSLocalizedString(@"DeviceIP",nil);
			break;
		case edr_port:
            self.editingView.editedPropertyKey = @"port";
            self.editingView.editedPropertyDisplayName = NSLocalizedString(@"DevicePort",nil);
			break;
		case edr_user:
			self.editingView.editedPropertyKey = @"user";
			self.editingView.editedPropertyDisplayName = NSLocalizedString(@"DeviceUser",nil);
			break;
        case edr_pwd:
			self.editingView.editedPropertyKey = @"password";
			self.editingView.editedPropertyDisplayName = NSLocalizedString(@"DevicePWD",nil);
			break;
//        case edr_rtsp:
//            self.editingView.editedPropertyKey = @"rtspport";
//            self.editingView.editedPropertyDisplayName = NSLocalizedString(@"DeviceRTSP", nil);
//            break;
	}
	
    if (indexPath.row != edr_sub || indexPath.row != edr_push) {
        [self.navigationController pushViewController:self.editingView animated:YES];
    }
}

#pragma mark - Alert View Delegate

- (void)showAlarm:(NSString *)message
{
    // open an alert with just an OK button, touch "OK" will do nothing
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
    [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
}

#pragma mark - Memory management

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    MMLog(@"[DV] dealloc %@",self);
    SAVE_FREE(device);
    SAVE_FREE(deviceCopy);
    SAVE_FREE(dvrTypeArray);
    SAVE_FREE(ipcamTypeArray);
    SAVE_FREE(editingView);
	
    [super dealloc];
}

#pragma mark - Local functions

- (BOOL)ifSupportMobileStream:(Device *)dev
{
    BOOL ret;
    if (dev.product == DEV_IPCAM) {
        return ret = YES;
    }
    if (dev.type==ECOR264_8D1 || dev.type==ELR_8D_8F || dev.type==EPARA264_32) {
        dev.dualStream = 0;
        ret = NO;
    }
    else
        ret = YES;
    
    return ret;
}

@end
