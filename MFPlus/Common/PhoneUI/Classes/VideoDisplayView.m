//
//  VideoDisplayView.m
//  EFViewerHD
//
//  Created by Nobel Hsu on 12/10/8.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "VideoDisplayView.h"


@implementation VideoDisplayView

@synthesize videoImageView,mainView,lblTitle,infoLabel;
@synthesize playingChannel,blnIsPlay,blnIsRec;

#pragma mark - Initialization

- (id)initWithView:(UIImageView *)view
{
    iTmp = 0;
	if ((self = [super init])) {
		
		mainView = view;
		// add sub views
        
		videoImageView = [[UIImageView alloc] init];
        [self setFitScale:YES];
        [videoImageView setTag:view.tag];   //add by robert hsu 20120103
		[mainView addSubview:videoImageView];
        
		infoLabel = [[UILabel alloc] init];
		[infoLabel setFont:[UIFont boldSystemFontOfSize:12]];
		[infoLabel setTextColor:[UIColor whiteColor]];
        [infoLabel setShadowColor:[UIColor darkGrayColor]];
        [infoLabel setShadowOffset:CGSizeMake(0.8, 0.8)];
		[infoLabel setBackgroundColor:[UIColor clearColor]];
		[mainView addSubview:infoLabel];
        
        lblTitle = [[UILabel alloc] init];
        [lblTitle setFont:[UIFont boldSystemFontOfSize:12]];
		[lblTitle setTextColor:[UIColor whiteColor]];
        [lblTitle setShadowColor:[UIColor darkGrayColor]];
        [lblTitle setShadowOffset:CGSizeMake(0.8, 0.8)];
		[lblTitle setBackgroundColor:[UIColor clearColor]];
		[mainView addSubview:lblTitle];
		
		activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
		[activityIndicator setHidesWhenStopped:YES];
		[mainView addSubview:activityIndicator];
        
        blnIsPlay = NO;
        blnIsRec = NO;
        playingChannel = 99;
	}
	
	return self;
}

#pragma mark - View Control

- (void)showView:(BOOL)blnShow
{
    [mainView setHidden:blnShow ? NO : YES ];
}

- (void)cleanView
{
	[videoImageView setImage:nil];
	[infoLabel setText:nil];
    [lblTitle setText:nil];
}

- (BOOL)isIndicatorAnimating
{
	return activityIndicator.isAnimating;
}

- (void)startActivityIndicatorAnimating
{
	[activityIndicator startAnimating];
}

- (void)stopActivityIndicatorAnimating
{
	[activityIndicator stopAnimating];
}

/*
 add by robert hsu 20120104
 reset position
 Parm :
 iDispMode : 1: mode 1 ,2:mode 4, 3:mode 9, 4:mode 16
 */
- (void)resetView:(BOOL)animated
{
    //CGRect frame = mainView.frame;
    CGRect frame = CGRectMake(0, 0, mainView.frame.size.width, mainView.frame.size.height);
    
    if (animated) {
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.3];
	}
    
    //NSLog(@"frame(%f,%f)",frame.size.width,frame.size.height);
    CGFloat imageWidth = frame.size.width;
    CGFloat imageHeight = frame.size.height;
    CGFloat imageHeightOffset = frame.size.height/2-imageHeight/2;
    
    [videoImageView setFrame:CGRectMake(frame.origin.x, frame.origin.y+imageHeightOffset, imageWidth, imageHeight)];
	[infoLabel setFrame:CGRectMake(frame.origin.x+5, frame.origin.y+frame.size.height-18, frame.size.width-10, 12)];
    [lblTitle setFrame:CGRectMake(frame.origin.x+5, frame.origin.y+5, frame.size.width-10, 12)];
	[activityIndicator setFrame:CGRectMake( (frame.size.width-37)/2, (frame.size.height-37)/2, 37, 37)];
	
    [videoImageView setNeedsDisplay];
	if (animated) {
		[UIView commitAnimations];
	}
}


- (UIImage *)videoImage
{
	return videoImageView.image;
}

- (void)setVideoImage:(UIImage *)image
{
    if(image != nil)
    {
        [videoImageView setImage:image];
    }
}

#pragma mark - Update View
/*add by robert hsu 20120105*/
- (void)setInfoText:(NSString *)strTitle Time:(NSString *) strVideoTime
{
    [infoLabel setText:strVideoTime];
    [lblTitle setText:strTitle];
}

- (void)dealloc {
	
    [infoLabel removeFromSuperview];
    [lblTitle removeFromSuperview];
    [videoImageView removeFromSuperview];
    [activityIndicator removeFromSuperview];
	[infoLabel release];
    [lblTitle release];
	[videoImageView release];
    [activityIndicator release];
    
	[super dealloc];
}

/*add by robert hsu 20120109 for change image fit mode if needed*/
-(void)setFitScale:(BOOL)blnFit
{
    if(blnFit)
    {
        [videoImageView setContentMode:UIViewContentModeScaleAspectFit];
    }
    else
    {
        [videoImageView setContentMode:UIViewContentModeScaleAspectFill];
    }
}

@end
