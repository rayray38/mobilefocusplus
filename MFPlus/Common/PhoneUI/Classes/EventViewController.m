//
//  EventViewController.m
//  EFViewer
//
//  Created by James Lee on 13/4/8.
//  Copyright (c) 2013年 EverFocus. All rights reserved.
//

#import "EventViewController.h"

@implementation EventViewController

@synthesize streamReceiver,iSearchST,iSearchET,eventFilter;
@synthesize sourceView;

#pragma mark - View LifeCycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.os_version >= 7.0)
        [mainTable setFrame:self.view.frame];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    blnFullScreen = appDelegate.blnFullScreen;
    [self setMask];
    
    if (streamReceiver.m_DiskGMT)
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:streamReceiver.m_DiskGMT]];
    
    eventList = streamReceiver.SearchResultlist;
    [maskView setHidden:NO];
    [mainTable reloadData];
    [loadAct performSelectorOnMainThread:@selector(startAnimating)
                              withObject:nil
                           waitUntilDone:NO];
    
    [NSThread detachNewThreadSelector:@selector(searchProcess)
                             toTarget:self
                           withObject:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [eventList removeAllObjects];
    eventList = nil;
    
    self.streamReceiver = nil;
    
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.blnFullScreen = blnFullScreen;
}

- (void)dealloc
{
    [dateFormatter release];
    dateFormatter = nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
	{
        blnFullScreen = NO;
		[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
	}
	else
	{
        blnFullScreen = YES;
		[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
	}
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self setMask];
}

- (void)setMask
{
    [maskView setFrame:self.view.frame];
    [loadAct setCenter:self.view.center];
}

#pragma mark - Search Process

- (void)searchProcess
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    streamReceiver.blnGetSearchInfo = YES;
    [streamReceiver getSearchIDFrom:self.iSearchST To:self.iSearchET byFilter:self.eventFilter];
    
    if (streamReceiver.m_intSearchResultTag == 1) {
        
        streamReceiver.m_intSearchProgress = 0;
        while(streamReceiver.m_intSearchResultTag==1 && streamReceiver.m_intSearchProgress<100 && streamReceiver.blnGetSearchInfo )
        {
            [NSThread sleepForTimeInterval:2.0f];
            [streamReceiver getEventResult];
            [mainTable reloadData];
        }
    }
    
    streamReceiver.blnGetSearchInfo = NO;
    [maskView setHidden:YES];
    [mainTable reloadData];
    
    [pool release];
}

- (UIImage *)eventParser:(NSInteger)type
{
    UIImage *returnImg;
    
    switch (type) {
        case EVENT_ALARM:
            returnImg = IMG_ALARM2;
            break;
        case EVENT_MOTION:
            returnImg = IMG_MOTION2;
            break;
        case EVENT_VLOSS:
            returnImg = IMG_VLOSS2;
            break;
    }
    
    return returnImg;
}

#pragma mark - TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger ret = 8;
    
    if (eventList.count > 0) {
        ret = eventList.count;
    }
    
    return ret;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)] autorelease];
    [headerView setBackgroundColor:[UIColor clearColor]];
    UILabel *title = [[[UILabel alloc] initWithFrame:CGRectMake(5, 10, tableView.bounds.size.width-10, 18)] autorelease];
    [title setFont:[UIFont boldSystemFontOfSize:18]];
    [title setTextColor:[UIColor lightTextColor]];
    [title setBackgroundColor:[UIColor clearColor]];
    
    if (eventList.count > 0) {
        title.text = [NSString stringWithFormat:@"%@(%lu)",NSLocalizedString(@"EventList", nil),(unsigned long)eventList.count];
    }else {
        title.text = NSLocalizedString(@"EventList", nil);
    }
    
    [headerView addSubview:title];
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setTextColor:[UIColor EFCOLOR]];
        [cell.detailTextLabel setTextColor:[UIColor lightTextColor]];
    }
    
    if (eventList.count > 0) {
        
        SearchResultEntry *tmpEntry = [eventList objectAtIndex:indexPath.row];
        [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        
        CameraInfo *tmpInfo = [streamReceiver getInfoFromChannelIndex:tmpEntry.uiChannel];
        cell.textLabel.text = tmpInfo ? tmpInfo.title : [NSString stringWithFormat:@"CH %u",tmpEntry.uiChannel+1];
        
        if (streamReceiver.m_intdlsEnable == 1) {
            cell.detailTextLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:tmpEntry.uiStartTime + streamReceiver.m_dst_time_sec]];
        }
        else
            cell.detailTextLabel.text = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:tmpEntry.uiStartTime]];
        cell.imageView.image = [self eventParser:tmpEntry.uiEventType];
    }else {
        cell.textLabel.text = nil;
        cell.detailTextLabel.text = nil;
        cell.imageView.image = nil;
    }
    
    return cell;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (eventList.count > 0) {
        SearchResultEntry *tmpEntry = [eventList objectAtIndex:indexPath.row];
        
        streamReceiver.m_blnPlaybackMode = YES;
        streamReceiver.m_blnEventSearchRS = YES;
        streamReceiver.m_intPlaybackStartTime = tmpEntry.uiStartTime;
        NSLog(@"[EV] StartTime:%lu, TZ:%@",(unsigned long)tmpEntry.uiStartTime,streamReceiver.m_DiskGMT);
        [streamReceiver stopStreaming];
        streamReceiver.currentPlayCH = tmpEntry.uiChannel;
        streamReceiver.eventSearchCH = tmpEntry.uiChannel;
        
        [self.navigationController popToViewController:self.sourceView animated:YES];
    }
    else
        NSLog(@"[EV] Event List is empty");
}

@end
