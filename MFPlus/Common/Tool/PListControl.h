//
//  PListControl.h
//  EFViewerHDPlus
//
//  Created by James Lee on 2014/8/9.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Device.h"

#define PLIST_NAME  @"efDev.plist"

@interface PListControl : NSObject
{
    NSString *m_sPLPath;
}

- (id)initByPath:(NSString *)_listName;
- (BOOL)addList:(NSArray *)_list toGroup:(NSString *)_groupName;
- (NSArray *)getListFromGroup:(NSString *)_groupName;
- (BOOL)setList:(NSArray *)_list ofGroup:(NSString *)_groupName;
- (BOOL)removeGroup:(NSString *)_groupName;

@end
