
#ifdef __cplusplus
extern "C" {
#endif

#define SOAP_SMD_SHA1_SIZE	(20)

char* soap_s2base64(const unsigned char *s, char *t, int n);
const char* soap_base642s(const char *s, char *t, int l, int *n);

void gen_nonce(unsigned char *nonce, int noncelen);
const char* get_created(char *tmpbuf, int buf_size);
void calc_digest(const char *created, const unsigned char *nonce, int noncelen, const char *password, unsigned char *hash);

#ifdef __cplusplus
}
#endif
