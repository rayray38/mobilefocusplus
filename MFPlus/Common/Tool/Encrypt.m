//
//  Encrypt.m
//  EFViewerHD
//
//  Created by James Lee on 13/9/26
//  Copyright (c) 2013年 EF. All rights reserved.
//

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "Encrypt.h"

#define u32		unsigned int

@implementation Encryptor

/////////////////////////////////////////////////////////////////////////////////////////////////
//    MD5
/////////////////////////////////////////////////////////////////////////////////////////////////

+ (NSString *)md5:(NSString *)input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//    SHA1
/////////////////////////////////////////////////////////////////////////////////////////////////

+ (NSString *)sha1:(NSString *)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (CC_LONG)data.length, digest);
    
    NSMutableString* strSha1 = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [strSha1 appendFormat:@"%02x", digest[i]];
    
    NSString *output = [self base64:strSha1];
    
    return output;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//    Base64
/////////////////////////////////////////////////////////////////////////////////////////////////

+ (NSString *)base64:(NSString *)input
{
    NSData *encData = [input dataUsingEncoding:NSUTF8StringEncoding];
    NSString *encodeStr = [encData base64Encoding];
    
    return encodeStr;
}

@end

@implementation Decryptor

+ (NSData *)base64:(NSString *)input
{
    NSData *encData = NULL;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        
        encData = [[NSData alloc] initWithBase64EncodedString:input options:0];
    } else {
        const char *objPointer = [input cStringUsingEncoding:NSASCIIStringEncoding];
        size_t intLength = strlen(objPointer);
        int intCurrent;
        int i = 0, j = 0, k;
        
        unsigned char *objResult = calloc(intLength, sizeof(unsigned char));
        
        // Run through the whole string, converting as we go
        while ( ((intCurrent = *objPointer++) != '\0') && (intLength-- > 0) ) {
            if (intCurrent == '=') {
                if (*objPointer != '=' && ((i % 4) == 1)) {// || (intLength > 0)) {
                    // the padding character is invalid at this point -- so this entire string is invalid
                    free(objResult);
                    return nil;
                }
                continue;
            }
            
            intCurrent = _base64DecodingTable[intCurrent];
            if (intCurrent == -1) {
                // we're at a whitespace -- simply skip over
                continue;
            } else if (intCurrent == -2) {
                // we're at an invalid character
                free(objResult);
                return nil;
            }
            
            switch (i % 4) {
                case 0:
                    objResult[j] = intCurrent << 2;
                    break;
                    
                case 1:
                    objResult[j++] |= intCurrent >> 4;
                    objResult[j] = (intCurrent & 0x0f) << 4;
                    break;
                    
                case 2:
                    objResult[j++] |= intCurrent >>2;
                    objResult[j] = (intCurrent & 0x03) << 6;
                    break;
                    
                case 3:
                    objResult[j++] |= intCurrent;
                    break;
            }
            i++;
        }
        
        // mop things up if we ended on a boundary
        k = j;
        if (intCurrent == '=') {
            switch (i % 4) {
                case 1:
                    // Invalid state
                    free(objResult);
                    return nil;
                case 2:
                    k++;
                    // flow through
                    break;
                case 3:
                    objResult[k] = 0;
                    break;
            }
        }
        
        // Cleanup and setup the return NSData
        encData = [[NSData alloc] initWithBytes:objResult length:j];
        free(objResult);
    }
    return encData;
}

+ (NSString *)base64:(NSString *)input with:(NSStringEncoding)encodeType
{
    NSString *decodeStr = NULL;
    NSData *encData = [Decryptor base64:input];
    decodeStr = [[NSString alloc] initWithData:encData encoding:encodeType];
    if ([decodeStr rangeOfString:@"\0"].location != NSNotFound) {           // 20140724 add by James Lee, avoid the length error
        NSInteger decodeStrLen = [decodeStr rangeOfString:@"\0"].location;
        [decodeStr release];
        decodeStr = [[NSString alloc] initWithBytes:encData.bytes length:2*decodeStrLen+2 encoding:encodeType];
    }
    SAVE_FREE(encData);
    
    return decodeStr;
}

@end


