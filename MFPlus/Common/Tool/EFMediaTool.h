//
//  EFMediaTool.h
//  EFViewerPlus
//
//  Created by James Lee on 2014/1/18.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

typedef void(^SaveImageCompletion)(NSError* error);
@interface EFMediaTool : ALAssetsLibrary

-(void)saveImage:(UIImage*)image toAlbum:(NSString*)albumName withCompletionBlock:(SaveImageCompletion)completionBlock;
-(void)saveVideoFileAtPath:(NSString *)videoPath toAlbum:(NSString *)_folderName;
-(void)saveVideoFileAtPath:(NSURL*)videoPath toAlbum:(NSString*)albumName withCompletionBlock:(SaveImageCompletion)completionBlock;
-(void)addAssetURL:(NSURL*)assetURL toAlbum:(NSString*)albumName withCompletionBlock:(SaveImageCompletion)completionBlock;

@end
