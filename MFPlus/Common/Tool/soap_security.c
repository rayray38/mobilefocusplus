
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <CommonCrypto/CommonDigest.h>
#include "soap_security.h"

#define SOAP_WSSE_NONCELEN	(20)

void calc_digest(const char *created, const unsigned char *nonce, int noncelen,
                 const char *password, unsigned char *digest)
{
    CC_SHA1_CTX ctx;
    CC_SHA1_Init(&ctx);
    CC_SHA1_Update(&ctx, nonce, noncelen);
    //printf("1 : %s\n",ctx.data);
    CC_SHA1_Update(&ctx, created, strlen(created));
    //printf("2 : %s\n",ctx.data);
    CC_SHA1_Update(&ctx, password, strlen(password));
    //printf("3 : %s\n",ctx.data);
    CC_SHA1_Final(digest, &ctx);
}


#define soap_blank(c)		((c)+1 > 0 && (c) <= 32)
#define soap_notblank(c)	((c) > 32)

const char soap_base64o[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
const char soap_base64i[81] = "\76XXX\77\64\65\66\67\70\71\72\73\74\75XXXXXXX\00\01\02\03\04\05\06\07\10\11\12\13\14\15\16\17\20\21\22\23\24\25\26\27\30\31XXXXXX\32\33\34\35\36\37\40\41\42\43\44\45\46\47\50\51\52\53\54\55\56\57\60\61\62\63";

char* soap_s2base64(const unsigned char *s, char *t, int n)
{ register int i;
  register unsigned long m;
  register char *p;
  if (!t)
    return NULL;
  p = t;
  t[0] = '\0';
  if (!s)
    return p;
  for (; n > 2; n -= 3, s += 3)
  { m = s[0];
    m = (m << 8) | s[1];
    m = (m << 8) | s[2];
    for (i = 4; i > 0; m >>= 6)
      t[--i] = soap_base64o[m & 0x3F];
    t += 4;
  }
  t[0] = '\0';
  if (n > 0) /* 0 < n <= 2 implies that t[0..4] is allocated (base64 scaling formula) */
  { m = 0;
    for (i = 0; i < n; i++)
      m = (m << 8) | *s++;
    for (; i < 3; i++)
      m <<= 8;
    for (i = 4; i > 0; m >>= 6)
      t[--i] = soap_base64o[m & 0x3F];
    for (i = 3; i > n; i--)
      t[i] = '=';
    t[4] = '\0';
  }
  return p;
}

/******************************************************************************/
const char* soap_base642s(const char *s, char *t, int l, int *n)
{ register int i, j;
  register short c;
  register unsigned long m;
  register const char *p;
  if (!s || !*s)
  { if (n)
      *n = 0;
      return NULL;
  }
  if (!t)
    return NULL;
  p = t;
  if (n)
    *n = 0;
  for (i = 0; ; i += 3, l -= 3)
  { m = 0;
    j = 0;
    while (j < 4)
    { c = *s++;
      if (c == '=' || !c)
      { if (l >= j - 1)
        { switch (j)
          { case 2:
              *t++ = (char)((m >> 4) & 0xFF);
              i++;
              l--;
              break;
            case 3:
              *t++ = (char)((m >> 10) & 0xFF);
              *t++ = (char)((m >> 2) & 0xFF);
              i += 2;
              l -= 2;
          }
        }
        if (n)
          *n = (int)i;
        if (l)
          *t = '\0';
        return p;
      }
      c -= '+';
      if (c >= 0 && c <= 79)
      { int b = soap_base64i[c];
        if (b >= 64)
        {
          return NULL;
        }
        m = (m << 6) + b;
        j++;
      }
      else if (!soap_blank(c + '+'))
      {
        return NULL;
      }
    }
    if (l < 3)
    { if (n)
        *n = (int)i;
      if (l)
        *t = '\0';
      return p;
    }
    *t++ = (char)((m >> 16) & 0xFF);
    *t++ = (char)((m >> 8) & 0xFF);
    *t++ = (char)(m & 0xFF);
  }
}

void gen_nonce(unsigned char *nonce, int noncelen)
{
	int i;
    //int r = 305419896;
	time_t r = time(NULL);
    srand(r);
	memcpy(nonce, &r, 4);
	for (i = 4; i < noncelen; i += 4)
	{
		r = rand();
		memcpy(nonce + i, &r, 4);
	}
}

const char* get_created(char *tmpbuf, int buf_size)
{
	struct tm T, *pT = &T;
	if (buf_size < 21)
		return NULL;
	time_t now = time(NULL);
	gmtime_r(&now, pT);
    strftime(tmpbuf, buf_size, "%Y-%m-%dT%H:%M:%SZ", pT);
    
    return tmpbuf;
}

