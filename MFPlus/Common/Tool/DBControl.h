//
//  DBControl.h
//  EFViewerPlus
//
//  Created by James Lee on 2014/6/6.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import <sqlite3.h>
#import "Device.h"

//Device Sqlite define
#define SQL_FILE_NAME     @"device2.sqlite"
#define SQL_INSERT_DVR    @"INSERT INTO dvr (product,name,type,ip,port,user,password,streamtype,dualstream,group_id,blnCloseNotify,uuid) VALUES"
#define SQL_INSERT_IPCAM  @"INSERT INTO ipcam (product,name,type,ip,port,user,password,streamtype,dualstream,rtspport,group_id,device_service) VALUES"
#define SQL_INSERT_GROUP  @"INSERT INTO grouptable (name,type,group_id) VALUES"
#define SQL_INSERT_EVENT  @"INSERT INTO event (source_name,source_ip,source_port,source_uuid,dev_name,dev_type,dev_ip,dev_eid,channel,event_type,event_time,event_seq,blnRead) VALUES"

#define SQL_SELECT_DVR    @"SELECT pk,product,name,type,ip,port,user,password,streamtype,dualstream,group_id,blnCloseNotify,uuid FROM dvr"
#define SQL_SELECT_IP     @"SELECT pk,product,name,type,ip,port,user,password,streamtype,dualstream,rtspport,group_id,device_service FROM ipcam"
#define SQL_SELECT_GROUP  @"SELECT pk,name,type,group_id FROM grouptable"
#define SQL_SELECT_EVENT  @"SELECT pk,source_name,source_ip,source_port,source_uuid,dev_name,dev_type,dev_ip,dev_eid,channel,event_type,event_time,event_seq,blnRead FROM event"

#define SQL_DEMO_DVR_1    @"Elux8,64,elux.everfocusddns.com,80,user1,11111111"
#define SQL_DEMO_DVR_2    @"Paragon FHD,65,paragonfhd.everfocusddns.com,80,user1,11111111"
#define SQL_DEMO_DVR_3    @"Commander2,151,commander2.everfocusddns.com,80,viewer,11111111"
#define SQL_DEMO_IP_1     @"EHN3160 1.3MP,27,ehn3160.everfocusddns.com,80,user1,11111111,3,554"
#define SQL_DEMO_IP_2     @"EZN3260 2MP,20,ezn3260.everfocusddns.com,80,user1,11111111,3,554"
#define SQL_DEMO_IP_3     @"EDN3340 3MP,25,edn3340.everfocusddns.com,80,user1,11111111,3,554"
#define SQL_DEMO_IP_4     @"EAN3220 2MP,16,ean3220.everfocusddns.com,80,user1,11111111,3,554"

@interface DBControl : NSObject
{
    sqlite3      *m_hDatebase;
}

// sqlite functions
- (id)initByPath:(NSString *)_DBName;
- (BOOL)addTableByName:(NSString *)_TBName;
- (BOOL)checkColumnByName:(NSString *)_ColName table:(NSString *)_TBName;
- (BOOL)addColumnToTable:(NSString *)_TBName column:(NSString *)_ColName type:(NSString *)_Type;

- (BOOL)addDeviceToTable:(Device *)_Device table:(NSString *)_TBName;
- (BOOL)addGroupToTable:(DeviceGroup *)_Group table:(NSString *)_TBName;
- (BOOL)addEventToTable:(EventTable *)_Event table:(NSString *)_TBName;
- (BOOL)deleteDeviceFromTable:(Device *)_Device table:(NSString *)_TBName;
- (BOOL)deleteEventsFromTable:(EventTable *)_Event table:(NSString *)_TBName;
- (BOOL)deleteAllEventsFromTable:(NSString *)_UID table:(NSString *)_TBName;
- (BOOL)deleteAllItemsFromTable:(NSString *)_TBName;

- (BOOL)getDevicesToArray:(NSMutableArray *)_Array type:(NSInteger)_Type;
- (BOOL)getGroupsToArray:(NSMutableArray *)_Array;
- (BOOL)getEventsToArray:(NSMutableArray *)_Array;

- (BOOL)updateEventFromDatabase:(EventTable *)_Event table:(NSString *)_TBName;    //20151001 added by Ray Lin for update selected event
- (NSString *)getErrorMessage;

//property list functions

@end
