//
//  DBControl.m
//  EFViewerPlus
//
//  Created by JamesLee on 2014/6/6.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import "DBControl.h"

@implementation DBControl

- (id)initByPath:(NSString *)_DBName
{
    if(!(self=[super init])) return nil;
    
    if (sqlite3_open([_DBName UTF8String], &m_hDatebase) != SQLITE_OK) {
        
        NSLog(@"[DB] Init DBControl Failed! %@ Error:%s",_DBName, sqlite3_errmsg(m_hDatebase));
        m_hDatebase = nil;
        return nil;
    }
    
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (BOOL)addTableByName:(NSString *)_TBName
{
    BOOL ret = YES;
    
    NSString *cmd = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (pk INTEGER PRIMARY KEY AUTOINCREMENT)",_TBName];
    if (sqlite3_exec(m_hDatebase, [cmd UTF8String], NULL, NULL, NULL) != SQLITE_OK) {
        NSLog(@"[DB] Create table [%@] failed Error:%s",_TBName, sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }
    
    return ret;
}

- (BOOL)checkColumnByName:(NSString *)_ColName table:(NSString *)_TBName
{
    if (!m_hDatebase)
        return NO;
    
    BOOL ret = YES;

    sqlite3_stmt *statement;
    NSString *cmd = [NSString stringWithFormat:@"SELECT %@ FROM %@",_ColName, _TBName];
    if (sqlite3_prepare_v2(m_hDatebase, [cmd UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[DB] Can't find %@ in table %@ ERROR:%s",_ColName, _TBName, sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }
    
    sqlite3_finalize(statement);
    
    return ret;
}

- (BOOL)addColumnToTable:(NSString *)_TBName column:(NSString *)_ColName type:(NSString *)_Type
{
    if (!m_hDatebase)
        return NO;
    
    BOOL ret = YES;
    
    sqlite3_stmt *statement;
    NSString *cmd = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN %@ %@",_TBName, _ColName, _Type];
    if (sqlite3_prepare_v2(m_hDatebase, [cmd UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[DB] Add Column %@ into table %@ failed - ERROR:%s",_ColName, _TBName, sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }else {
        
        if(sqlite3_exec(m_hDatebase, [cmd UTF8String], NULL, NULL, NULL) != SQLITE_OK) {
            NSLog(@"[DB] Add Column %@ into table %@ failed - ERROR:%s",_ColName, _TBName, sqlite3_errmsg(m_hDatebase));
            ret = NO;
        }else
            NSLog(@"[DB] Add Column %@ into table %@ success",_ColName, _TBName);
    }
    
    sqlite3_finalize(statement);
    return ret;
}

- (BOOL)getDevicesToArray:(NSMutableArray *)_Array type:(NSInteger)_Type
{
    if (!m_hDatebase)
        return NO;
    
    BOOL ret = YES;

    sqlite3_stmt *statement;
    NSString *cmd = [NSString stringWithFormat:@"%@ ORDER BY pk", _Type==DEV_DVR ? SQL_SELECT_DVR : SQL_SELECT_IP];
    if (sqlite3_prepare_v2(m_hDatebase, [cmd UTF8String], -1, &statement, NULL) == SQLITE_OK) {
        
        // "step" through the results
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            Device *dev = [[Device alloc] init];
            dev.product		= sqlite3_column_int(statement, 1);
            dev.name		= [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
            dev.type		= sqlite3_column_int(statement, 3);
            dev.ip			= [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 4)];
            dev.port		= sqlite3_column_int(statement, 5);
            dev.user		= [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 6)];
            dev.password	= [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 7)];
            dev.streamType	= sqlite3_column_int(statement, 8);
            dev.dualStream  = sqlite3_column_int(statement, 9);
            
            if (_Type == DEV_DVR) {
                dev.group       = sqlite3_column_int(statement, 10);//add by James Lee 20140605 for group
                dev.blnCloseNotify = sqlite3_column_int(statement, 11);
                if (sqlite3_column_text(statement, 12) != nil) {
                    dev.uuid = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 12)];
                }
            }else {
                dev.rtspPort    = sqlite3_column_int(statement, 10);//add by robert hsu 20120203 for add rtsp port
                dev.group       = sqlite3_column_int(statement, 11);//add by James Lee 20140605 for group
                
                //20150703 added by Ray Lin for onvif ipcam
                if (sqlite3_column_text(statement, 12) != nil) {
                    dev.device_service	= [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 12)];
                }                    
            }
            
            dev.rowID = _Array.count;
            
            [_Array addObject:dev];
            [dev release];
        }
    }else {
        
        NSLog(@"[DB] Get devices from %@ failed - ERROR:%s",_Type==DEV_DVR ? @"DVR" : @"IPCAM", sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }
    
    sqlite3_finalize(statement);
    
    return ret;
}

- (BOOL)getGroupsToArray:(NSMutableArray *)_Array
{
    if (!m_hDatebase)
        return NO;
    
    BOOL ret = YES;
    
    sqlite3_stmt *statement;
    NSString *cmd = [NSString stringWithFormat:@"%@ ORDER BY pk",SQL_SELECT_GROUP];
    if (sqlite3_prepare_v2(m_hDatebase, [cmd UTF8String], -1, &statement, NULL) == SQLITE_OK) {
        
        //Initial subArray of groups
        NSMutableArray *dvrGroups = [[NSMutableArray alloc] init];
        NSMutableArray *ipGroups = [[NSMutableArray alloc] init];
        NSMutableArray *eventGroups = [[NSMutableArray alloc] init];
        [_Array addObjectsFromArray:[NSArray arrayWithObjects:dvrGroups,ipGroups,eventGroups, nil]];
        [dvrGroups release];
        [ipGroups release];
        [eventGroups release];
        
        // "step" through the results
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            DeviceGroup *group = [[DeviceGroup alloc] init];
            group.name		= [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
            group.type      = sqlite3_column_int(statement, 2);
            group.group_id  = sqlite3_column_int(statement, 3);
            //NSLog(@"[DB] name : %@ -- %ld",group.name,(long)group.group_id);
            
            [[_Array objectAtIndex:group.type] addObject:group];
            [group release];
        }
    }else {
        
        NSLog(@"[DB] Get groups from grouptable failed - ERROR:%s",sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }
    
    return ret;
}

- (BOOL)getEventsToArray:(NSMutableArray *)_Array
{
    if (!m_hDatebase)
        return NO;
    
    BOOL ret = YES;
    
    sqlite3_stmt *statement;
    NSString *cmd = [NSString stringWithFormat:@"%@ ORDER BY pk",SQL_SELECT_EVENT];
    if (sqlite3_prepare_v2(m_hDatebase, [cmd UTF8String], -1, &statement, NULL) == SQLITE_OK) {
        
        // "step" through the results
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            EventTable *evtable = [[EventTable alloc] init];
            evtable.rowID = sqlite3_column_int(statement, 0);
            if (sqlite3_column_text(statement, 1) != nil)
            {
                evtable.source_name = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
            }
            if (sqlite3_column_text(statement, 2) != nil)
            {
                evtable.source_ip   = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
            }
            evtable.source_port = sqlite3_column_int(statement, 3);
            if (sqlite3_column_text(statement, 4) != nil)
            {
                evtable.source_uuid = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 4)];
            }
            if (sqlite3_column_text(statement, 5) != nil) {
                evtable.dev_name    = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 5)];
            }
            evtable.dev_type    = sqlite3_column_int(statement, 6);
            if (sqlite3_column_text(statement, 7) != nil) {
                evtable.dev_ip      = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 7)];
            }
            evtable.dev_eid     = sqlite3_column_int(statement, 8);
            evtable.channel     = sqlite3_column_int(statement, 9);
            if (sqlite3_column_text(statement, 10) != nil) {
                evtable.event_type  = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 10)];
            }
            evtable.event_time  = sqlite3_column_int(statement, 11);
            evtable.event_seq   = sqlite3_column_int(statement, 12);
            evtable.blnRead   = sqlite3_column_int(statement, 13);
            //NSLog(@"[DB] name : %@ -- %ld",event.dev_name,(long)event.event_time);
            
            //evtable.rowID = _Array.count;
            
            [_Array insertObject:evtable atIndex:0];
            [evtable release];
        }
    }else {
        
        NSLog(@"[DB] Get events from eventTable failed - ERROR:%s",sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }
    
    return ret;
}

- (BOOL)addDeviceToTable:(Device *)_Device table:(NSString *)_TBName
{
    if (!m_hDatebase)
        return NO;
    
    NSString *_name = [_Device.name stringByReplacingOccurrencesOfString:@"\'" withString:@"''"];
    BOOL ret = YES;
    NSString *cmd = NULL;
    if (_Device.product == 0) {  //DVR
        
        cmd = [NSString stringWithFormat:@"%@(%ld,'%@',%ld,'%@',%ld,'%@','%@',%ld,%ld,%ld,%ld,'%@')",SQL_INSERT_DVR
               ,(long)_Device.product,_name,(long)_Device.type,_Device.ip,(long)_Device.port
               ,_Device.user,_Device.password,(long)_Device.streamType,(long)_Device.dualStream,(long)_Device.group,(long)_Device.blnCloseNotify,_Device.uuid];
    }else { //IPCAM
        
        cmd = [NSString stringWithFormat:@"%@(%ld,'%@',%ld,'%@',%ld,'%@','%@',%ld,%ld,%ld,%ld,'%@')",SQL_INSERT_IPCAM
               ,(long)_Device.product,_name,(long)_Device.type,_Device.ip,(long)_Device.port
               ,_Device.user,_Device.password,(long)_Device.streamType,(long)_Device.dualStream,(long)_Device.rtspPort,(long)_Device.group,_Device.device_service];
    }
    
    if (sqlite3_exec(m_hDatebase, [cmd UTF8String], NULL, NULL, NULL) != SQLITE_OK)
    {
        NSLog(@"[DB] Add device %@ from table %@ failed - ERROR:%s", _Device.name, _TBName, sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }
    return ret;
}

- (BOOL)addGroupToTable:(DeviceGroup *)_Group table:(NSString *)_TBName
{
    if (!m_hDatebase)
        return NO;
    
    BOOL ret = YES;
    
    NSString *cmd = [NSString stringWithFormat:@"%@('%@',%ld,%ld)",SQL_INSERT_GROUP,_Group.name,(long)_Group.type,(long)_Group.group_id];
    
    if (sqlite3_exec(m_hDatebase, [cmd UTF8String], NULL, NULL, NULL) != SQLITE_OK)
    {
        NSLog(@"[DB] Add group %@ from table %@ failed - ERROR:%s", _Group.name, _TBName, sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }
    return ret;
}

- (BOOL)addEventToTable:(EventTable *)_Event table:(NSString *)_TBName
{
    if (!m_hDatebase)
        return NO;
    
    BOOL ret = YES;
    NSString *cmd = NULL;
    cmd = [NSString stringWithFormat:@"%@('%@','%@',%ld,'%@','%@',%ld,'%@',%ld,%ld,'%@',%ld,%ld,'%ld')",SQL_INSERT_EVENT,_Event.source_name,_Event.source_ip,(long)_Event.source_port,_Event.source_uuid,_Event.dev_name,(long)_Event.dev_type,_Event.dev_ip,(long)_Event.dev_eid,(long)_Event.channel,_Event.event_type,(long)_Event.event_time,(long)_Event.event_seq,(long)_Event.blnRead];
    
    if (sqlite3_exec(m_hDatebase, [cmd UTF8String], NULL, NULL, NULL) != SQLITE_OK)
    {
        NSLog(@"[DB] Add event %@ from table %@ failed - ERROR:%s", _Event.dev_name, _TBName, sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }
    
    return ret;
}

- (BOOL)deleteDeviceFromTable:(Device *)_Device table:(NSString *)_TBName
{
    if (!m_hDatebase)
        return NO;
    
    BOOL ret = YES;
    NSString *cmd = [NSString stringWithFormat:@"DELETE FROM %@ WHERE pk=%ld",_TBName,_Device.rowID+1];
    
    if (sqlite3_exec(m_hDatebase, [cmd UTF8String], NULL, NULL, NULL) != SQLITE_OK) {
        NSLog(@"[DB] Delete device %ld from table %@ failed - ERROR:%s", _Device.rowID+1, _TBName, sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }
    
    return ret;
}

- (BOOL)deleteEventsFromTable:(EventTable *)_Event table:(NSString *)_TBName
{
    if (!m_hDatebase)
        return NO;
    
    BOOL ret = YES;
    NSString *cmd = [NSString stringWithFormat:@"DELETE FROM %@ WHERE pk=%ld",_TBName,(long)_Event.rowID];
    
    if (sqlite3_exec(m_hDatebase, [cmd UTF8String], NULL, NULL, NULL) != SQLITE_OK) {
        NSLog(@"[DB] Delete event %ld from table %@ failed - ERROR:%s", (long)_Event.rowID, _TBName, sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }
    
    return ret;
}

- (BOOL)deleteAllEventsFromTable:(NSString *)_UID table:(NSString *)_TBName
{
    if (!m_hDatebase)
        return NO;
    
    BOOL ret = YES;
    NSString *cmd = [NSString stringWithFormat:@"DELETE FROM %@ WHERE source_uuid='%@'",_TBName,_UID];
    
    if (sqlite3_exec(m_hDatebase, [cmd UTF8String], NULL, NULL, NULL) != SQLITE_OK) {
        NSLog(@"[DB] Delete events %@ from table %@ failed - ERROR:%s", _UID, _TBName, sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }
    
    return ret;
}

- (BOOL)deleteAllItemsFromTable:(NSString *)_TBName
{
    if (!m_hDatebase)
        return NO;
    
    BOOL ret = YES;
    sqlite3_stmt *statement;
    NSString *cmd = [NSString stringWithFormat:@"DELETE FROM %@",_TBName];
    
    if(sqlite3_prepare_v2(m_hDatebase, [cmd UTF8String], -1, &statement, NULL) != SQLITE_OK) {
		
		NSLog(@"[DB] Delete all items from table %@ failed - ERROR:%s", _TBName, sqlite3_errmsg(m_hDatebase));
        ret = NO;
	}
	
	if(sqlite3_step(statement) != SQLITE_DONE) {
		
		NSLog(@"[DB] Delete all items from table %@ failed - ERROR:%s", _TBName, sqlite3_errmsg(m_hDatebase));
        ret = NO;
	}
	
	sqlite3_finalize(statement);
    
    return ret;
}

- (BOOL)updateEventFromDatabase:(EventTable *)_Event table:(NSString *)_TBName
{
    if (!m_hDatebase)
        return NO;
    
    BOOL ret = YES;
    sqlite3_stmt *statement = nil;
    NSString *update_SQL_cmd = [NSString stringWithFormat:@"UPDATE %@ set blnRead='%ld' WHERE pk=%ld",_TBName,(long)_Event.blnRead,(long)_Event.rowID];
    NSLog(@"[DB] %@",update_SQL_cmd);
    if(sqlite3_prepare_v2(m_hDatebase, [update_SQL_cmd UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[DB] Update selected event from table %@ failed - ERROR:%s", _TBName, sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }
    
    if(sqlite3_step(statement) != SQLITE_DONE) {
        
        NSLog(@"[DB] Update selected event from table %@ failed - ERROR:%s", _TBName, sqlite3_errmsg(m_hDatebase));
        ret = NO;
    }
    
    sqlite3_finalize(statement);
    
    return ret;
}

- (NSString *)getErrorMessage
{
    if (!m_hDatebase)
        return NULL;
    
    return [NSString stringWithFormat:@"%s",sqlite3_errmsg(m_hDatebase)];
}

@end
