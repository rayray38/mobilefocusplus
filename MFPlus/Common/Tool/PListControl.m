//
//  PListControl.m
//  EFViewerHDPlus
//
//  Created by James Lee on 2014/8/9.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import "PListControl.h"

@implementation PListControl

#pragma mark - Init/Dealloc

- (id)initByPath:(NSString *)_listName
{
    if(!(self=[super init])) return nil;
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:_listName]) {
        
        NSMutableDictionary *plCtrl = [[NSMutableDictionary alloc] init];
        BOOL ret = [plCtrl writeToFile:_listName atomically:YES];
        if (!ret) {
            NSLog(@"[PLCONTROL] Write Default PL error");
            return nil;
        }
        [plCtrl release];
    } else
        NSLog(@"[PLCONTROL] PL exist");
    
    m_sPLPath = [_listName copy];
    MMLog(@"[PLCONTROL] Init %@",self);
    return self;
}

- (void)dealloc
{
    [m_sPLPath release];
    MMLog(@"[PLCONTROL] dealloc %@",self);
    [super dealloc];
}

#pragma mark - PList Control

- (BOOL)addList:(NSArray *)_list toGroup:(NSString *)_groupName
{
    if (!m_sPLPath) return NO;
    
    BOOL ret = YES;
    NSMutableDictionary *plCtrl = [NSMutableDictionary dictionaryWithContentsOfFile:m_sPLPath];
    [plCtrl setObject:_list forKey:_groupName];
    ret = [plCtrl writeToFile:m_sPLPath atomically:YES];
    if (!ret)
        NSLog(@"[PLCONTROL] add List to %@ failed!",_groupName);
    
    return ret;
}

- (NSArray *)getListFromGroup:(NSString *)_groupName
{
    if (!m_sPLPath) return nil;
    
    NSMutableDictionary *plCtrl = [NSMutableDictionary dictionaryWithContentsOfFile:m_sPLPath];
    return [plCtrl objectForKey:_groupName];
}

- (BOOL)setList:(NSArray *)_list ofGroup:(NSString *)_groupName
{
    if (!m_sPLPath) return NO;
    
    BOOL ret = YES;
    return ret;
}

- (BOOL)removeGroup:(NSString *)_groupName
{
    if (!m_sPLPath) return NO;
    
    BOOL ret = YES;
    NSMutableDictionary *plCtrl = [NSMutableDictionary dictionaryWithContentsOfFile:m_sPLPath];
    [plCtrl removeObjectForKey:_groupName];
    return ret;
}

@end
