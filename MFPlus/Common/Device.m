//
//  Device.m
//  EFViewer
//
//  Created by James Lee on 2010/5/19.
//  Copyright 2010 EverFocus. All rights reserved.
//

#import "Device.h"


@implementation Device

@synthesize product,name,type,ip,port,user,password,mac,streamType,dualStream,devTimeZone;
@synthesize rtspPort,group,blnIsPlaying,blnStatic,rowID;//add by robert hsu 20120203
@synthesize maskAddr,gateway,dns;
@synthesize device_service,media_uri,device_uri,main_profile_token,main_stream_uri,sub_profile_token,sub_stream_uri; // Adde by Steven Chang
@synthesize network_interface,blnCloseNotify,blnEvNotify;

- (id)initWithProduct:(NSInteger)ptype {
	
	if ((self = [super init])) {
		
		self.product	    = ptype;
		self.name		    = @"";
		self.type		    = 0;
		self.ip			    = @"";
		self.port		    = 80;
		self.user		    = @"";
		self.password	    = @"";
        self.mac            = @"";
		self.streamType	    = [Device getStreamType:type product:product];
		self.dualStream	    = 1; //ptype==DEV_DVR?1:0;
        self.rtspPort       = 554;//add by robert hsu 20120203
        self.group          = 0; //add by James Lee 20140605
        self.blnIsPlaying   = NO;
        self.blnStatic      = NO;
        self.rowID          = 0;
        self.device_service = @"";
        self.blnCloseNotify = 0;  //0:NO  1:YES
        self.uuid           = @"";
        self.blnEvNotify    = NO;
	}
    MMLog(@"[Device] Init %@",self);
	
	return self;
}

- (id)initWithDevice:(Device *)device {
	
	if ((self = [super init])) {
		
		self.product	    = device.product;
		self.name		    = device.name;
		self.type		    = device.type;
		self.ip			    = device.ip;
		self.port		    = device.port;
		self.user		    = device.user;
		self.password	    = device.password;
        self.mac            = device.mac;
		self.streamType	    = device.streamType;
		self.dualStream	    = device.dualStream;
        self.rtspPort       = device.rtspPort;//add by robert hsu 20120203
        self.group          = device.group; //add by James Lee 20140605
        self.rowID          = device.rowID;
        self.device_service = device.device_service;
        self.blnIsPlaying   = NO;
        self.blnStatic      = NO;
        self.blnCloseNotify = device.blnCloseNotify;
        self.uuid           = device.uuid;
        self.blnEvNotify    = NO;
	}
    MMLog(@"[Device] Init %@",self);
	
	return self;
}

- (void)setValueWithDevice:(Device *)device {
	
	self.product	    = device.product;
	self.name		    = device.name;
	self.type		    = device.type;
	self.ip			    = device.ip;
	self.port		    = device.port;
	self.user		    = device.user;
    self.mac            = device.mac;
	self.password	    = device.password;
	self.streamType	    = device.streamType;
	self.dualStream	    = device.dualStream;
    self.rtspPort       = device.rtspPort;//add by robert hsu 20120203
    self.group          = device.group; //add by James Lee 20140605
    self.rowID          = device.rowID;
    self.blnIsPlaying   = device.blnIsPlaying;
    self.blnStatic      = device.blnStatic;
    self.device_service = device.device_service;
    self.blnCloseNotify = device.blnCloseNotify;
    self.uuid           = device.uuid;
    self.blnEvNotify    = device.blnEvNotify;
}

- (void)dealloc {
	
    MMLog(@"[Device] dealloc %@",self);
    SAVE_FREE(name);
    SAVE_FREE(ip);
    SAVE_FREE(user);
    SAVE_FREE(password);
    SAVE_FREE(mac);
    SAVE_FREE(maskAddr);
    SAVE_FREE(gateway);
    SAVE_FREE(dns);
    SAVE_FREE(device_service);
    SAVE_FREE(media_uri);
    SAVE_FREE(device_uri);
    SAVE_FREE(main_profile_token);
    SAVE_FREE(main_stream_uri);
    SAVE_FREE(sub_profile_token);
    SAVE_FREE(sub_stream_uri);
	
	[super dealloc];
}

/**
 * modify by robert hsu 20111102
 * purpose : add model for EPARA264-32X4
 ***/
+ (NSArray *)createDvrModelArray {
	
	return [[NSArray alloc] initWithObjects:
			STR_ECOR264_4D1,
			STR_ECOR264_4D2,
			STR_ECOR264_8D1,
			STR_ECOR264_8D2,
			STR_ECOR264_4_9_16X1,
            STR_ECOR960_16X1,
            STR_ECOR960_4_8_16F2,
            STR_ECORHD_4_8_16F,
            STR_ECORHD_16X1,
            STR_ECOR_FHD_SERIES,
			STR_EDR_16D1_16F1,
			STR_EDR_HD_2H14,
			STR_EDR_HD_4H4,
			STR_ELR_4D_4F,
			STR_ELR_8D_8F,
            STR_ELUX_SERIES,
			STR_EMV_200,
            STR_EMV_200S_400S,
            STR_EMV_1201,
			STR_EMV_400_800_1200,
            STR_EMV401_801_1601,
            STR_EMV_400S_SERIES,
            STR_EMV400_800_1200_HD,
            STR_EMV_FHD_SERIES,
            STR_EMX_32,
			STR_ENDEAVOR264X4L4,
            STR_ENVR8304D_E_X,
            STR_ENVR8316E,
            STR_EPHD_04,
			STR_PARAGON_HD08,
            STR_EPHD08_PLUS,
            STR_EPHD16U,
            STR_ERS_4,
            STR_ESN_FHD_SERIES,
            STR_EVS_FHD_SERIES,
            STR_NVR8004x,
            STR_NVR_PRO,
			STR_PARAGON2,
			STR_PARAGON264_16X1X2X4,
            STR_EPARA264_32,
            STR_PARAGON960_16X1,
            STR_PARAGON960_16X4,
            STR_PARAGON_FHD,
            STR_TUTIS_4_8_16F3,
            STR_XMS_SERIES,
			nil];
}

+ (NSArray *)createIpcamModelArray {
	
	return [[NSArray alloc] initWithObjects:
			STR_EAN850A_800A_AW,
			STR_EAN900,
            STR_EAN2150,
            STR_EAN2218,
            STR_EAN2350,
            STR_EAN3120_3220_3300,
			STR_EAN3200,
            STR_EAN7220,
            STR_EDN228,
			STR_EDN800_850H,
            STR_EDN1120_1220_1320,
            STR_EDN2160_2260_2560,
            STR_EDN2245,
            STR_EDN3160_3260_3340,
            STR_EDN3240,
            STR_EFN3320_3321,
            STR_EFN3320c_3321c,
            STR_EHN1120_1220_1320,
            STR_EHN3160_3260_3340,
            STR_EHN3240,
            STR_EHN3261_3361,
            STR_EMN2120_2220_2320,
            STR_EPN2218,
			STR_EPN3100_3600,
            STR_EPN4122_4220_i,
            STR_EPN4220_4230_D,
            STR_EPN4230_P,
            STR_EPN5210_5230,
            STR_EQN100,
			STR_EQN2200_2201,
            STR_EQN2101_2110_2171,
            STR_ETN2160_2260_2560,
			STR_EVS200A_AW,
            STR_EVS410,
            STR_EXN268_SERIES,
            STR_EXN368_SERIES,
			STR_EZN850,
            STR_EZN1160_1260_1360,
            STR_EZN3160_3260_3340,
            STR_EZN3240,
            STR_EZN3261_3361,
            STR_POLESTAR_7_SERIES,
            STR_ONVIF,
            STR_RTSP,
			nil];
}

/**
 * modify by robert hsu 20110923
 * purpose : add model for EVS410 ,EZN3240 ,EDN3240 ,EDN3200
 ***/
+ (NSInteger)modelStringToIndex:(NSString *)string product:(NSInteger)product {
    
	NSInteger index = 0;
	
	// DVRs
	if (product==DEV_DVR) {
		
		if ([string isEqualToString:STR_ECOR264_4D1]) {
			index = ECOR264_4D1;
		}
		else if ([string isEqualToString:STR_ECOR264_4D2]) {
			index = ECOR264_4D2;
		}
		else if ([string isEqualToString:STR_ECOR264_8D1]) {
			index = ECOR264_8D1;
		}
		else if ([string isEqualToString:STR_ECOR264_8D2]) {
			index = ECOR264_8D2;
		}
		else if ([string isEqualToString:STR_ECOR264_4_9_16X1]) {
			index = ECOR264_4X1;
		}
		else if ([string isEqualToString:STR_EDR_16D1_16F1]) {
			index = EDR_16D1_16F1;
		}
		else if ([string isEqualToString:STR_ELR_4D_4F]) {
			index = ELR_4D_4F;
		}
		else if ([string isEqualToString:STR_ELR_8D_8F]) {
			index = ELR_8D_8F;
		}
		else if ([string isEqualToString:STR_ERS_4]) {
			index = ERS_4;
		}
		else if ([string isEqualToString:STR_PARAGON2]) {
			index = PARAGON2;
		}
		else if ([string isEqualToString:STR_EDR_HD_2H14]) {
			index = EDR_HD_2H14;
		}
		else if ([string isEqualToString:STR_EDR_HD_4H4]) {
			index = EDR_HD_4H4;
		}
		else if ([string isEqualToString:STR_EMV_200]) {
			index = EMV_200;
		}
		else if ([string isEqualToString:STR_EMV_400_800_1200]) {
			index = EMV_400_800_1200;
		}
        else if ([string isEqualToString:STR_EMV400_800_1200_HD]) {
            index = EMV_400_800_1200_HD;
        }
        else if ([string isEqualToString:STR_EMV401_801_1601]) {
            index = EMV_401_801_1601;
        }
        else if ([string isEqualToString:STR_EMV_FHD_SERIES]) {
            index = EMV_FHD_SERIES;
        }
		else if ([string isEqualToString:STR_ENDEAVOR264X4L4]) {
			index = ENDEAVOR264X4;
		}
		else if ([string isEqualToString:STR_PARAGON_HD08]) {
			index = PARAGON_HD08;
		}
        else if ([string isEqualToString:STR_EPARA264_32]) {
			index = EPARA264_32;
		}
        else if ([string isEqualToString:STR_PARAGON264_16X1X2X4]) {        // v2.2.7add by James Lee 20120525
			index = PARAGON264_16X4;
		}
        else if ([string isEqualToString:STR_EMV_200S_400S]) {
			index = EMV_200S_400S;
        }
        else if ([string isEqualToString:STR_ESN_FHD_SERIES]) {
            index = ESN41_41E_FHD;
        }
        else if ([string isEqualToString:STR_EVS_FHD_SERIES]) {
            index = EVS410_FHD;
        }
        else if ([string isEqualToString:STR_EMV_400S_SERIES]) {
            index = EMV400S_FHD;
        }
        else if ([string isEqualToString:STR_PARAGON960_16X4]) {
			index = PARAGON960_16X4;
        }
        else if ([string isEqualToString:STR_PARAGON960_16X1]) {
            index = PARAGON960_16X1;
        }
        else if ([string isEqualToString:STR_PARAGON_FHD]) {
            index = PARAGON_FHD;
        }
        else if ([string isEqualToString:STR_EPHD_04]) {
			index = EPHD_04;
		}
        else if ([string isEqualToString:STR_ECOR960_16X1]) {
            index = ECOR960_16X1;
        }
        else if ([string isEqualToString:STR_NVR8004x]) {
			index = NVR8004X;
        }
        else if ([string isEqualToString:STR_NVR_PRO]) {
            index = NVR_Pro;
        }
        else if ([string isEqualToString:STR_ENVR8304D_E_X]) {
			index = ENVR8304D_E_X;
		}
        else if ([string isEqualToString:STR_EMV_1201]) {
			index = EMV1201;
		}
        else if ([string isEqualToString:STR_ECOR960_4_8_16F2]) {
			index = ECOR960_4_8_16F2;
		}
        else if ([string isEqualToString:STR_TUTIS_4_8_16F3]) {
			index = TUTIS_4_8_16F3;
		}
        else if ([string isEqualToString:STR_EPHD08_PLUS]) {
			index = EPHD08_PLUS;
		}
        else if ([string isEqualToString:STR_EPHD16U]) {
			index = EPHD16U;
        }
        else if ([string isEqualToString:STR_ECORHD_16X1]) {
            index = ECORHD_16X1;
        }
        else if ([string isEqualToString:STR_ECORHD_4_8_16F]) {
            index = ECORHD_4_8_16F;
        }
        else if ([string isEqualToString:STR_ECOR_FHD_SERIES]) {
            index = ECOR_FHD_SERIES;
        }
        else if ([string isEqualToString:STR_ELUX_SERIES]) {
            index = ELUX_SERIES;
        }
        else if ([string isEqualToString:STR_EMX_32]) {
            index = EMX32;
        }
        else if ([string isEqualToString:STR_ENVR8316E]) {
            index = ENVR8316E;
        }
        else if ([string isEqualToString:STR_XMS_SERIES]) {
            index = XMS_SERIES;
        }
	}
	
	// IpCams
	else if (product==DEV_IPCAM) {
        
		if ([string isEqualToString:STR_EAN850A_800A_AW]) {
			index = EAN800A_AW;
		}
		else if ([string isEqualToString:STR_EDN800_850H]) {
			index = EDN800;
		}
		else if ([string isEqualToString:STR_EPN3100_3600]) {
			index = EPN3100;
		}
		else if ([string isEqualToString:STR_EVS200A_AW]) {
			index = EVS200A_AW;
		}
		else if ([string isEqualToString:STR_EZN850]) {
			index = EZN850;
		}
		else if ([string isEqualToString:STR_EAN900]) {
			index = EAN900;
		}
        else if ([string isEqualToString:STR_EQN100]) {
            index = EQN100;
        }
		else if ([string isEqualToString:STR_EQN2200_2201]) {
			index = EQN2200_2201;
		}
		else if ([string isEqualToString:STR_EAN3200]) {
			index = EAN3200;
		}
        else if ([string isEqualToString:STR_EDN3240]) {
            index = EDN3240;
        }
        else if([string isEqualToString:STR_EZN3240]) {
            index = EZN3240;
        }
        else if([string isEqualToString:STR_EHN3240]) {
            index = EHN3240;
        }
        else if([string isEqualToString:STR_EVS410]) {
            index = EVS410;
        }
        else if( [string isEqualToString:STR_EZN3240]) {
            index = EZN3240;
        }
        else if( [string isEqualToString:STR_EAN3120_3220_3300]) {
            index = EAN3120;
        }
        else if( [string isEqualToString:STR_EAN3120_3220_3300]) {
            index = EAN3300;
        }
        else if( [string isEqualToString:STR_EZN3160_3260_3340]) {
            index = EZN3160;
        }
        else if( [string isEqualToString:STR_EZN3160_3260_3340]) {
            index = EZN3340;
        }
        else if( [string isEqualToString:STR_EDN3160_3260_3340]) {
            index = EDN3160;
        }
        else if( [string isEqualToString:STR_EDN3160_3260_3340]) {
            index = EDN3340;
        }
        else if( [string isEqualToString:STR_EHN3160_3260_3340]) {
            index = EHN3160;
        }
        else if( [string isEqualToString:STR_EHN3160_3260_3340]) {
            index = EHN3340;
        }
        else if( [string isEqualToString:STR_EPN4122_4220_i]) {
            index = EPN4122_4220;
        }
        else if( [string isEqualToString:STR_EAN2150]) {
            index = EAN2150;
        }
        else if( [string isEqualToString:STR_EAN2218]) {
            index = EAN2218;
        }
        else if( [string isEqualToString:STR_EAN2350]) {
            index = EAN2350;
        }
        else if( [string isEqualToString:STR_EDN2245]) {
            index = EDN2245;
        }
        else if( [string isEqualToString:STR_EPN2218]) {
            index = EPN2218;
        }
        else if ( [string isEqualToString:STR_EQN2101_2110_2171]) {
            index = EQN2101_2171;
        }
        else if ( [string isEqualToString:STR_EDN1120_1220_1320]) {
            index = EDN1120_1220_1320;
        }
        else if ( [string isEqualToString:STR_EMN2120_2220_2320]) {
            index = EMN2120_2220_2320;
        }
        else if ( [string isEqualToString:STR_ETN2160_2260_2560]) {
            index = ETN2160_2260_2560;
        }
        else if ( [string isEqualToString:STR_EDN2160_2260_2560]) {
            index = EDN2160_2260_2560;
        }
        else if ( [string isEqualToString:STR_EZN1160_1260_1360]) {
            index = EZN1160_1260_1360;
        }
        else if ( [string isEqualToString:STR_EDN2210]) {
            index = EDN2210;
        }
        else if ( [string isEqualToString:STR_EHN1120_1220_1320]) {
            index = EHN1120_1220_1320;
        }
        else if ( [string isEqualToString:STR_EZN3261_3361]) {
            index = EZN3261_3361;
        }
        else if ( [string isEqualToString:STR_EHN3261_3361]) {
            index = EHN3261_3361;
        }
        else if ( [string isEqualToString:STR_EPN4230_P]) {
            index = EPN4230_P;
        }
        else if ( [string isEqualToString:STR_EPN5210_5230]) {
            index = EPN5210;
        }
        else if ( [string isEqualToString:STR_EFN3320_3321]) {
            index = EFN3320_3321;
        }
        else if ( [string isEqualToString:STR_EFN3320c_3321c]) {
            index = EFN3320c_3321c;
        }
        else if ( [string isEqualToString:STR_POLESTAR_7_SERIES]) {
            index = EAN7200;
        }
        else if ( [string isEqualToString:STR_EAN7220]) {
            index = EAN7220;
        }
        else if ( [string isEqualToString:STR_EPN4220_4230_D]) {
            index = EPN4220_4230_D;
        }
        else if ( [string isEqualToString:STR_EXN268_SERIES]) {
            index = EBN268_V;
        }
        else if ( [string isEqualToString:STR_EXN368_SERIES]) {
            index = EBN368_V;
        }
        else if ( [string isEqualToString:STR_EDN228]) {
            index = EDN228;
        }
        else if ( [string isEqualToString:STR_ONVIF]) {
            index = ONVIF;
        }
        else if ( [string isEqualToString:STR_RTSP]) {
            index = RTSP;
        }
    }
	
	return index;
}

+ (NSString *)modelIndexToString:(NSInteger)index product:(NSInteger)product {
	
	NSString *string = @"";
	
	// DVRs
	if (product==DEV_DVR) {
        
        switch (index) {
            case ECOR264_4D1:
                string = STR_ECOR264_4D1;
                break;
            case  ECOR264_4D2:
                string = STR_ECOR264_4D2;
                break;
            case ECOR264_8D1:
                string = STR_ECOR264_8D1;
                break;
            case ECOR264_8D2:
                string = STR_ECOR264_8D2;
                break;
            case ECOR264_4X1:
            case ECOR264_9X1:
            case ECOR264_16X1:
                string = STR_ECOR264_4_9_16X1;
                break;
            case EDR_16D1_16F1:
                string = STR_EDR_16D1_16F1;
                break;
            case ELR_4D_4F:
                string = STR_ELR_4D_4F;
                break;
            case ELR_8D_8F:
                string = STR_ELR_8D_8F;
                break;
            case ERS_4:
                string = STR_ERS_4;
                break;
            case PARAGON2:
                string = STR_PARAGON2;
                break;
            case PARAGON264_16X1:
            case PARAGON264_16X2:
            case PARAGON264_16X4:
                string = STR_PARAGON264_16X1X2X4;
                break;
            case EDR_HD_2H14:
                string = STR_EDR_HD_2H14;
                break;
            case EDR_HD_4H4:
                string = STR_EDR_HD_4H4;
                break;
            case EMV_200:
                string = STR_EMV_200;
                break;
            case EMV_400_800_1200:
                string = STR_EMV_400_800_1200;
                break;
            case EMV_200S_400S:            // v2.2.7 add by James Lee 20120525
                string = STR_EMV_200S_400S;
                break;
            case ENDEAVOR264X4:
            case ENDEAVOR264L4:
                string = STR_ENDEAVOR264X4L4;
                break;
            case PARAGON_HD08:
                string = STR_PARAGON_HD08;
                break;
            case EPARA264_32:
                string = STR_EPARA264_32;
                break;
            case PARAGON960_16X1:
                string = STR_PARAGON960_16X1;
                break;
            case PARAGON960_16X4:
                string = STR_PARAGON960_16X4;
                break;
            case PARAGON_FHD:
                string = STR_PARAGON_FHD;
                break;
            case EPHD_04:
                string = STR_EPHD_04;
                break;
            case ECOR960_16X1:
                string = STR_ECOR960_16X1;
                break;
            case NVR8004X:
                string = STR_NVR8004x;
                break;
            case NVR_Pro:
                string = STR_NVR_PRO;
                break;
            case ENVR8304D_E_X:
                string = STR_ENVR8304D_E_X;
                break;
            case EMV1201:
                string = STR_EMV_1201;
                break;
            case ECOR960_4_8_16F2:
                string = STR_ECOR960_4_8_16F2;
                break;
            case TUTIS_4_8_16F3:
                string = STR_TUTIS_4_8_16F3;
                break;
            case EPHD08_PLUS:
                string = STR_EPHD08_PLUS;
                break;
            case EMV_401_801_1601:
                string = STR_EMV401_801_1601;
                break;
            case EMV_400_800_1200_HD:
                string = STR_EMV400_800_1200_HD;
                break;
            case EMV_FHD_SERIES:
                string = STR_EMV_FHD_SERIES;
                break;
            case ESN41_41E_FHD:
                string = STR_ESN_FHD_SERIES;
                break;
            case EVS410_FHD:
                string = STR_EVS_FHD_SERIES;
                break;
            case EMV400S_FHD:
            case EMV400_SSD:
                string = STR_EMV_400S_SERIES;
                break;
            case EPHD16U:
                string = STR_EPHD16U;
                break;
            case ECORHD_16X1:
                string = STR_ECORHD_16X1;
                break;
            case ECORHD_4_8_16F:
                string = STR_ECORHD_4_8_16F;
                break;
            case ECOR_FHD_SERIES:
                string = STR_ECOR_FHD_SERIES;
                break;
            case ELUX_SERIES:
                string = STR_ELUX_SERIES;
                break;
            case EMX32:
                string = STR_EMX_32;
                break;
            case ENVR8316E:
                string = STR_ENVR8316E;
                break;
            case XMS_SERIES:
                string = STR_XMS_SERIES;
                break;
        }
	}
	
	// IpCams
	else if (product == DEV_IPCAM) {
		
		switch (index) {                        // 2.2.8 fix by James Lee 20120705
            case EAN800A_AW:
            case EAN850A:
                string = STR_EAN850A_800A_AW;
                break;
            case EDN800:
            case EDN850H:
                string = STR_EDN800_850H;
                break;
            case EPN3100:
            case EPN3600:
                string = STR_EPN3100_3600;
                break;
            case EVS200A_AW:
                string = STR_EVS200A_AW;
                break;
            case EZN850:
                string = STR_EZN850;
                break;
            case EAN900:
                string = STR_EAN900;
                break;
            case EQN2200_2201:
                string = STR_EQN2200_2201;
                break;
            case EAN3200:
                string = STR_EAN3200;
                break;
            case EDN3240:
                string = STR_EDN3240;
                break;
            case EHN3240:
                string = STR_EHN3240;
                break;
            case EZN3240:
                string = STR_EZN3240;
                break;
            case EVS410:
                string = STR_EVS410;
                break;
            case EAN3120:
            case EAN3220:
            case EAN3300:
                string = STR_EAN3120_3220_3300;
                break;
            case EZN3160:
            case EZN3260:
            case EZN3340:
            case EZN3560:
                string = STR_EZN3160_3260_3340;
                break;
            case EDN3160:
            case EDN3260:
            case EDN3340:
            case EDN3560:
                string = STR_EDN3160_3260_3340;
                break;
            case EHN3160:
            case EHN3260:
            case EHN3340:
            case EHN3560:
                string = STR_EHN3160_3260_3340;
                break;
            case EPN4122_4220:
            case EPN4122i_4220i:
                string = STR_EPN4122_4220_i;
                break;
            case EAN2150:
                string = STR_EAN2150;
                break;
            case EAN2218:
                string = STR_EAN2218;
                break;
            case EAN2350:
                string = STR_EAN2350;
                break;
            case EDN2245:
                string = STR_EDN2245;
                break;
            case EPN2218:
                string = STR_EPN2218;
                break;
            case EQN2101_2171:
            case EQN2110:
                string = STR_EQN2101_2110_2171;
                break;
            case EDN1120_1220_1320:
                string = STR_EDN1120_1220_1320;
                break;
            case EMN2120_2220_2320:
                string = STR_EMN2120_2220_2320;
                break;
            case ETN2160_2260_2560:
                string = STR_ETN2160_2260_2560;
                break;
            case EDN2160_2260_2560:
                string = STR_EDN2160_2260_2560;
                break;
            case EZN1160_1260_1360:
                string = STR_EZN1160_1260_1360;
                break;
            case EZN3261_3361:
                string = STR_EZN3261_3361;
                break;
            case EHN3261_3361:
                string = STR_EHN3261_3361;
                break;
            case EDN2210:
                string = STR_EDN2210;
                break;
            case EHN1120_1220_1320:
                string = STR_EHN1120_1220_1320;
                break;
            case EPN4230_P:
                string = STR_EPN4230_P;
                break;
            case EPN5210:
            case EPN5230:
                string = STR_EPN5210_5230;
                break;
            case EFN3320_3321:
                string = STR_EFN3320_3321;
                break;
            case EFN3320c_3321c:
                string = STR_EFN3320c_3321c;
                break;
            case EAN7200:
            case EAN7221_7260_7360:
            case EHN7221_7260_7360:
            case EZN7221_7260_7360:
                string = STR_POLESTAR_7_SERIES;
                break;
            case EAN7220:
                string = STR_EAN7220;
                break;
            case EPN4220_4230_D:
                string = STR_EPN4220_4230_D;
                break;
            case EBN268_V:
            case EZN268_V:
                string = STR_EXN268_SERIES;
                break;
            case EBN368_V:
            case EZN368_V_M:
            case EDN368M:
                string = STR_EXN368_SERIES;
                break;
            case EDN228:
                string = STR_EDN228;
                break;
            case EQN100:
                string = STR_EQN100;
                break;
            case ONVIF:
                string = STR_ONVIF;
                break;
            case RTSP:
                string = STR_RTSP;
                break;
        }
    }
	
	return string;
}

+ (NSInteger)getStreamType:(NSInteger)model product:(NSInteger)product {
	
	NSInteger stream = 0;
	
	// DVRs
	if (product==DEV_DVR) {
		
        if (model>150) {
            
            stream = STREAM_XMS;
        }
        else if (model>110) {
            
            stream = STREAM_NVR265;
        }
        else if (model>100) {
            
            stream = STREAM_ICATCH;
        }
        else {
            
            stream = STREAM_P2;
        }
	}
	
	// IpCams
	else if (product==DEV_IPCAM) {
		
		if (model<=8) {
			
			stream = STREAM_NEVIO;
		}
        else if (model >= 105) {
            stream = STREAM_ONVIF;
        }
        else if (model >= 100) {
            stream = STREAM_AFREEY;
        }
        else if (model >= 90) {
            stream = STREAM_GEMTEK;
        }
		else if (model >= 80) {
			
			stream = STREAM_DYNA;
		}
        else {
            
            stream = STREAM_HDIP;
        }
	}
	
	return stream;
}

+ (BOOL)getPTZSupport:(NSInteger)model product:(NSInteger)product
{
    BOOL ret = NO;
    
    if (product == DEV_IPCAM) {
        
        switch (model) {
            case EAN2218:
            case EDN3160:
            case EDN3260:
            case EHN3160:
            case EHN3240:
            case EHN3260:
            case EHN3261_3361:
            case EHN3340:
            case EHN3560:
            case EHN7221_7260_7360:
            case EPN2218:
            case EPN3100:
            case EPN3600:
            case EPN4122_4220:
            case EPN4122i_4220i:
            case EPN4220_4230_D:
            case EPN4230_P:
            case ETN2160_2260_2560:
            case EZN3160:
            case EZN3240:
            case EZN3260:
            case EZN3261_3361:
            case EZN3340:
            case EZN3560:
                ret = YES;
                break;
                
            default:
                break;
        }
    }
    return ret;
}

+ (NSInteger)getNewIDfromArray:(NSMutableArray *)_Array
{
    NSInteger ID = 0;
    for (NSMutableArray *tmpArray in _Array)
        ID += tmpArray.count;
    
    return ID;
}

+ (Device *)getDeviceByID:(NSInteger)_ID from:(NSMutableArray *)_Array
{
    Device *dev;
    
    for (NSMutableArray *tmpArray in _Array)
    {
        for (Device *tmpDev in tmpArray)
        {
            if (tmpDev.rowID == _ID) {
                dev = tmpDev;
                break;
            }
        }
    }
    
    return dev;
}

@end

@implementation DeviceGroup

@synthesize name,type,group_id,expand;

- (id)initWithProduct:(NSInteger)ptype
{
    if ((self = [super init])) {
        self.type = ptype;
        self.name = @"";
        self.group_id = 0;
        self.expand = NO;
    }
    
    MMLog(@"[DeviceGroup] Init %@",self);
    return self;
}

- (void)dealloc
{
    MMLog(@"[DeviceGroup] dealloc %@",self);
    self.name = nil;
    
    [super dealloc];
}

+ (DeviceGroup *)getGroupByID:(NSInteger)_ID from:(NSMutableArray *)_Array
{
    DeviceGroup *result;
    for (DeviceGroup *group in _Array)
    {
        if (_ID == group.group_id) {
            result = group;
            break;
        }
    }
    
    return result;
}

+ (NSMutableArray *)getArrayByGroupID:(NSInteger)_ID from:(NSMutableArray *)_Array
{
    Device *dev = NULL;
    NSMutableArray *resultArray = NULL;
    for (NSMutableArray *tmpArray in _Array)
    {
        dev = [tmpArray firstObject];
        if (_ID == dev.group)
        {
            resultArray = tmpArray;
            break;
        }
    }
    
    return resultArray;
}

+ (NSInteger)getNewIDFromArray:(NSMutableArray *)_Array
{
    NSInteger ID = 0;
//    for (NSMutableArray *tmpArray in _Array)
//        ID += tmpArray.count;
    ID += _Array.count;
    
    return ID+1;
}

@end

@implementation EventTable

@synthesize source_name,source_ip,source_port,source_uuid,dev_name,dev_type,dev_ip,dev_eid,channel,event_type,event_time,event_seq,rowID,blnRead;

- (id)initWithEvent
{
    if ((self = [super init])) {
        self.source_name = @"";
        self.source_ip = @"";
        self.source_port = 80;
        self.source_uuid = @"";
        self.dev_name = @"";
        self.dev_type = 0;
        self.dev_ip = @"";
        self.dev_eid = 0;
        self.channel = 0;
        self.event_type = @"";
        self.event_time = 0;
        self.event_seq = 0;
        self.rowID = 0;
        self.blnRead = EF_EVENT_UNREAD;   //0:NO(未讀) ; 1:YES(已讀)
    }
    
    MMLog(@"[EventTable] Init %@",self);
    return self;
}

@end
