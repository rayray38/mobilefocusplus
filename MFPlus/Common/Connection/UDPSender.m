//
//  UDPServer.m
//  EFViewerHD
//
//  Created by James Lee on 13/1/17.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import "UDPSender.h"

// 20151001 added for iCatch EPHD16+U by Ray Lin
NSString *discovery_probe_str_idvr = @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/08/addressing\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:wsdd=\"http://schemas.xmlsoap.org/ws/2005/04/discovery\"><SOAP-ENV:Header><wsa:MessageID>uuid:%@</wsa:MessageID><wsa:To SOAP-ENV:mustUnderstand=\"true\">urn:schemas-xmlsoap-org:ws:2005:04:discovery</wsa:To><wsa:Action SOAP-ENV:mustUnderstand=\"true\">http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe</wsa:Action></SOAP-ENV:Header><SOAP-ENV:Body><wsdd:Probe><wsdd:Types></wsdd:Types></wsdd:Probe></SOAP-ENV:Body></SOAP-ENV:Envelope>";

@interface UDPSender()
{
    GCDAsyncUdpSocket   *udpSock_ez,*udpSock_iC,*udpSock_NVR265;
    NSString            *strTypes;
    Device              *iC_DVR;
}
- (void)parseData:(NSData *)data;
- (NSString *)convertAddress:(unsigned char *)strAry;

- (NSInteger)checkProduct:(NSInteger)mode name:(NSString *)devName;
- (NSInteger)checkProduct:(NSString *)category;
- (NSInteger)checkModel:(NSInteger)productType name:(NSString *)devName;
- (NSInteger)checkStreamType:(NSInteger)mode product:(NSInteger)type;
- (NSInteger)checkStreamType:(NSString *)protocol;
- (NSInteger)checkIPType:(NSString *)protocol;
- (BOOL)checkValidType:(NSString *)devName ip:(NSString *)address;
@end

@implementation UDPSender

@synthesize deviceList,blnStop,productFilter;

#pragma mark - Action

- (id)init
{
    deviceList = [[NSMutableArray alloc] init];
    blnStop = YES;
    
    MMLog(@"[UDP] Init %@",self);
    return self;
}

- (void)dealloc
{
    MMLog(@"[UDP] dealloc %@",self);
    NSLog(@"[UDP] dealloc %@",self);
    
    [deviceList removeAllObjects];
    SAVE_FREE(deviceList);
    SAVE_FREE(udpSock);
    SAVE_FREE(udpSock_ez);
    SAVE_FREE(udpSock_iC);
    SAVE_FREE(udpSock_NVR265);
    SAVE_FREE(currentElementName);
    
    [super dealloc];
}

- (void)start
{
    if (!blnStop)   return;
    
    udpSock = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    udpSock_ez = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    udpSock_iC = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    udpSock_NVR265 = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    uuid_t uuid;
    char uuid_buf[128];
    NSError *error;
    
    //EF Protocol
    CMD_HEADER pkEF = {0};
    pkEF.magic = NSSwapBigIntToHost(EF_MAGIC);
    pkEF.command = NSSwapBigShortToHost(ect_devRequest);
    for (int n=0; n<sizeof(pkEF.rxMac); n++)
        pkEF.rxMac[n] = 0xFF;
    pkEF.pktType = ept_devXML;
    
    NSData *data = [NSData dataWithBytes:&pkEF length:sizeof(pkEF)];
    
    [udpSock bindToPort:RX_PORT error:&error];
    [udpSock enableBroadcast:YES error:&error];
    [udpSock sendData:data toHost:BROADCAST_ADDR port:TX_PORT withTimeout:TIMEOUT tag:1];
    [udpSock beginReceiving:&error];
    
    //Afreey Protocol
    SEARCH_HEAD pkEz;
    pkEz.magic_number = NSSwapBigIntToHost(TX_MAGIC_EZ);
    pkEz.type = NSSwapBigIntToHost(0x01);
    NSData *data_ez = [NSData dataWithBytes:&pkEz length:sizeof(pkEz)];
    
    [udpSock_ez bindToPort:RX_PORT_EZ error:&error];
    [udpSock_ez enableBroadcast:YES error:&error];
    [udpSock_ez sendData:data_ez toHost:BROADCAST_ADDR port:TX_PORT_EZ withTimeout:TIMEOUT tag:1];
    [udpSock_ez beginReceiving:&error];
    
    //iCatch Protocol
    uuid_generate_time(uuid);
    uuid_unparse(uuid, uuid_buf);
    NSString *stringData3 = [NSString stringWithFormat:discovery_probe_str_idvr,
                             [NSString stringWithCString:uuid_buf encoding:NSUTF8StringEncoding]];
    NSData *data3 = [stringData3 dataUsingEncoding:NSUTF8StringEncoding];
    
    [udpSock_iC bindToPort:WS_DISCOVERY_PORT error:&error];
    [udpSock_iC enableBroadcast:YES error:&error];
    [udpSock_iC sendData:data3 toHost:WS_DISCOVERY_ADDR port:WS_DISCOVERY_PORT withTimeout:TIMEOUT tag:1];
    [udpSock_iC beginReceiving:&error];
    
    //NVR265 Protocol
    int NVR265_MAGIC_PACK[4] = {0,0x0B,0,0x04};
    NSData *data4 = [NSData dataWithBytes:&NVR265_MAGIC_PACK length:sizeof(NVR265_MAGIC_PACK)];
    
    [udpSock_NVR265 bindToPort:NVR265_LISTEN_PORT error:&error];
    [udpSock_NVR265 enableBroadcast:YES error:&error];
    [udpSock_NVR265 sendData:data4 toHost:BROADCAST_ADDR port:NVR265_SEND_PORT withTimeout:TIMEOUT tag:1];
    [udpSock_NVR265 beginReceiving:&error];
    
    blnStop = NO;
    
    NSLog(@"[UDP] Start Brocasting");
}

- (void)stop
{
    if (blnStop)    return;
    
    blnStop = YES;
    [udpSock close];
    [udpSock_ez close];
    [udpSock_iC close];
    [udpSock_NVR265 close];
    
    SAVE_FREE(udpSock);
    SAVE_FREE(udpSock_ez);
    SAVE_FREE(udpSock_iC);
    SAVE_FREE(udpSock_NVR265);
    NSLog(@"[UDP] Stop Brocasting");
}

#pragma mark - Common converter

- (NSString *)convertAddress:(unsigned char *)strAry
{
    return [NSString stringWithFormat:@"%u.%u.%u.%u",strAry[0],strAry[1],strAry[2],strAry[3]];
}

#pragma mark - Device checking

- (void)parseData:(NSData *)data
{
    CMD_HEADER *cmdHdr = (CMD_HEADER *)[data bytes];
    //NSSwapHostIntToBig(var) need to use here
    if (NSSwapHostIntToBig(cmdHdr->magic)==EF_MAGIC && [data length] == (sizeof(CMD_HEADER)+sizeof(DEV_INFO))) {
        
        DEV_INFO *tmpInfo = (DEV_INFO *)([data bytes] + sizeof(CMD_HEADER));
        NSString *devName = [[NSString alloc] initWithBytes:tmpInfo->name length:sizeof(tmpInfo->name) encoding:NSUTF8StringEncoding];
        //NSLog(@"[UDP EF] DeviceNames : %@",devName);
        NSArray *infoAry = nil;
        if ([devName rangeOfString:@"\0"].location != NSNotFound) {
            infoAry = [devName componentsSeparatedByString:@"\0"];
            SAVE_FREE(devName);
            devName = [[[NSString alloc] initWithString:[infoAry objectAtIndex:0]] autorelease];
        }
        
        Device *curDev = [[Device alloc] initWithProduct:[self checkProduct:(tmpInfo->protocol&(0xF0>>4)) name:devName]];
        
        curDev.ip = [self convertAddress:tmpInfo->ipAddr];
        curDev.maskAddr = [self convertAddress:tmpInfo->maskAddr];
        curDev.gateway = [self convertAddress:tmpInfo->gateWay];
        curDev.dns = [NSString stringWithFormat:@"%@:%@",[self convertAddress:tmpInfo->DNS1],[self convertAddress:tmpInfo->DNS2]];
        curDev.port = NSSwapBigShortToHost(tmpInfo->httpPort);
        if ([devName isEqualToString:@""]) {
            
            NSString *cmd;
            NSString *response;
            NSInteger iErrorCode = 0;
            blnGetP2DevName = YES;
            cmdSender = [[CommandSender alloc] initWithHost:[NSString stringWithFormat:@"%@:%ld",curDev.ip,(long)curDev.port] delegate:self];
            cmd = [NSString stringWithFormat:@"cgi-bin/Info.xml"];
            [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
            
            if( errorDesc != nil)
            {
                iErrorCode = -1;
            }
            // parse dvr info response
            response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            NSLog(@"[UDP] response:%@",response);
            [cmdSender parseXmlResponse:response];
            [response release];
            blnGetP2DevName = NO;
            curDev.name = oldDevName;
        }
        else
            curDev.name = devName;
        curDev.streamType = [self checkStreamType:(tmpInfo->protocol&(0xF0>>4)) product:curDev.product];
        curDev.mac = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X"
                      ,cmdHdr->txMac[0],cmdHdr->txMac[1],cmdHdr->txMac[2],cmdHdr->txMac[3],cmdHdr->txMac[4],cmdHdr->txMac[5]];
        curDev.blnStatic = (tmpInfo->ipType == eit_static);
        //NSLog(@"[UDP] PSIA Name:%@, IP:%@",curDev.name, curDev.ip);
        BOOL blnExist = NO;
        for (Device *refInfo in deviceList) {               //check if the device is already in list(bad method)
            
            blnExist = [refInfo.ip isEqualToString:curDev.ip];
            if (blnExist)   break;
        }
        if (!blnExist && [self checkValidType:curDev.name ip:curDev.ip] && curDev.product==productFilter) {
            curDev.type = [self checkModel:curDev.product name:curDev.name];
            [deviceList addObject:curDev];
            //NSLog(@"[UDP] DeviceCount:%d",deviceList.count);
        }
        
        SAVE_FREE(curDev);
        return;
    }
    
    SEARCH_SOCK_REP *tmpEz = (SEARCH_SOCK_REP *)[data bytes];
    //NSSwapHostIntToBig(var) need to use here
    if (NSSwapHostIntToBig(tmpEz->head.magic_number)==RX_MAGIC_EZ && [data length]==EZ_LEN) {
        
        NSString *devName = [[NSString alloc] initWithBytes:tmpEz->data.string length:sizeof(tmpEz->data.string) encoding:NSUTF8StringEncoding];
        NSArray *infoAry = nil;
        if ([devName rangeOfString:@"\0"].location != NSNotFound) {
            infoAry = [devName componentsSeparatedByString:@"\0"];
            SAVE_FREE(devName);
        }
        
        Device *ezDev = [[Device alloc] initWithProduct:DEV_IPCAM];
        NSUInteger uIP = tmpEz->data.ip;
        ezDev.ip = [NSString stringWithFormat:@"%ld.%ld.%ld.%ld",(long)(uIP>>0)&0xFF,(long)(uIP>>8)&0xFF,(long)(uIP>>16)&0xFF,(long)(uIP>>24)&0xFF];
        ezDev.port = NSSwapHostIntToBig(tmpEz->data.web_port);
        ezDev.name = [NSString stringWithFormat:@"%@(%@)",[infoAry objectAtIndex:0],NSLocalizedString(@"MsgLoginRequired", nil)];
        ezDev.streamType = STREAM_AFREEY;
        ezDev.mac = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",tmpEz->data.mac[0],tmpEz->data.mac[1],tmpEz->data.mac[2],tmpEz->data.mac[3],tmpEz->data.mac[4],tmpEz->data.mac[5]];
        ezDev.rtspPort = NSSwapHostIntToBig(tmpEz->data.s_port);
        //NSLog(@"[UDP] Afreey Name:%@, IP:%@",ezDev.name, ezDev.ip);
        BOOL blnExist = NO;
        for (Device *refInfo in deviceList) {               //check if the device is already in list(bad method)
            
            blnExist = [refInfo.ip isEqualToString:ezDev.ip];
            if (blnExist)   break;
        }
        if (!blnExist && [self checkValidType:ezDev.name ip:ezDev.ip] && ezDev.product==productFilter) {
            ezDev.type = [self checkModel:ezDev.product name:ezDev.name];
            //ezDev.type = ETN2160_2260_2560;
            [deviceList addObject:ezDev];
            //NSLog(@"[UDP] DeviceCount:%d",deviceList.count);
        }
        
        SAVE_FREE(ezDev);
        return;
    }
    
    //20161213 added by Ray Lin, for NVR265 response data
    NSString *STR_NVR265 = [[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding];
    if (STR_NVR265 != NULL && [data length] == NVR265_LEN) {
        Device *tmpDev = [[Device alloc] initWithProduct:DEV_DVR];
        tmpDev.streamType = STREAM_NVR265;
        tmpDev.type = NVR_Pro;
        
        //get ip
        NSRange RANGE_IPADDR = NSMakeRange(0x10, 16);
        NSData *DATA_IPADDR = [data subdataWithRange:RANGE_IPADDR];
        NSString *STR_IPADDR = [[NSString alloc] initWithData:DATA_IPADDR encoding:NSUTF8StringEncoding];
        STR_IPADDR = [STR_IPADDR stringByReplacingOccurrencesOfString:@"\0" withString:@""];
        tmpDev.ip = [NSString stringWithFormat:@"%@",STR_IPADDR];
        
        //get port
        NSRange RANGE_PORT = NSMakeRange(0x88, 4);
        NSData *DATA_PORT = [data subdataWithRange:RANGE_PORT];
        NSString *STR_PORT = [[NSString alloc] initWithData:DATA_PORT encoding:NSUTF8StringEncoding];
        STR_PORT = [STR_PORT stringByReplacingOccurrencesOfString:@"\0" withString:@""];
        
        //get mask
        NSRange RANGE_MASK = NSMakeRange(0x44, 16);
        NSData *DATA_MASK = [data subdataWithRange:RANGE_MASK];
        NSString *STR_MASK = [[NSString alloc] initWithData:DATA_MASK encoding:NSUTF8StringEncoding];
        STR_MASK = [STR_MASK stringByReplacingOccurrencesOfString:@"\0" withString:@""];
        tmpDev.maskAddr = [NSString stringWithFormat:@"%@",STR_MASK];
        
        //get devname
        NSRange RANGE_DEVNAME = NSMakeRange(0xDC, 16);
        NSData *DATA_DEVNAME = [data subdataWithRange:RANGE_DEVNAME];
        NSString *STR_DEVNAME = [[NSString alloc] initWithData:DATA_DEVNAME encoding:NSUTF8StringEncoding];
        STR_DEVNAME = [STR_DEVNAME stringByReplacingOccurrencesOfString:@"\0" withString:@""];
        tmpDev.name = [NSString stringWithFormat:@"%@",STR_DEVNAME];
        
        BOOL blnExist = NO;
        for (Device *refInfo in deviceList) {
            
            blnExist = [refInfo.ip isEqualToString:tmpDev.ip];
            if (blnExist)   break;
        }
        if (!blnExist && [self checkValidType:tmpDev.name ip:tmpDev.ip] && tmpDev.product==productFilter) {
            [deviceList addObject:tmpDev];
        }
        
        SAVE_FREE(tmpDev);
        return;
    }
    
    NSString *xmlString = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
    xmlString = [xmlString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    xmlString = [xmlString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    xmlString = [xmlString stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    xmlString = [xmlString stringByReplacingOccurrencesOfString:@"&apos;" withString:@"\'"];
    if (xmlString != NULL) {
        NSLog(@"\n[UDP] %@",xmlString);
        
        NSXMLParser *parser = [[[NSXMLParser alloc] initWithData:[xmlString dataUsingEncoding:NSUTF8StringEncoding]] autorelease];
        //NSXMLParser *parser = [[[NSXMLParser alloc] initWithData:data] autorelease];
        [parser setDelegate:self];
        [parser parse];
    }
}

- (NSInteger)checkProduct:(NSInteger)mode name:(NSString *)devName
{
    NSInteger product = DEV_DVR;
    
    if (mode != DEV_DVR) {
        
        BOOL ret = ([devName rangeOfString:@"Paragon"].location == NSNotFound);
        ret &= ([devName rangeOfString:@"NVR"].location == NSNotFound);
        ret &= ([devName rangeOfString:@"TUTIS+"].location == NSNotFound);
        ret &= ([devName rangeOfString:@"EPHD"].location == NSNotFound);
        ret &= ([devName rangeOfString:@"ECOR"].location == NSNotFound);
        ret &= ([devName rangeOfString:@"EMV"].location == NSNotFound);
        ret &= ([devName rangeOfString:@"EMX"].location == NSNotFound);
        
        if (ret)    product = DEV_IPCAM;
    }
    return product;
}

- (NSInteger)checkModel:(NSInteger)productType name:(NSString *)devName
{
    NSInteger model = 99;
    if (productType == DEV_DVR) {
        
        model = ECOR264_16X1;
        if ([devName rangeOfString:@"ECOR264"].location != NSNotFound) {
            if ([devName rangeOfString:@"4D"].location != NSNotFound) {
                if ([devName rangeOfString:@"1"].location != NSNotFound) {
                    model = ECOR264_4D1;
                }
                else
                    model = ECOR264_4D2;
            }
            else if ([devName rangeOfString:@"8D"].location != NSNotFound) {
                if ([devName rangeOfString:@"1"].location != NSNotFound) {
                    model = ECOR264_8D1;
                }
                else
                    model = ECOR264_8D2;
            }
        }
        else if ([devName rangeOfString:@"Paragon" options:NSCaseInsensitiveSearch].location != NSNotFound) {          //20160329 added by Ray Lin, put Paragon Series together
            if ([devName rangeOfString:@"FHD" options:NSCaseInsensitiveSearch].location != NSNotFound)
                model = PARAGON_FHD;
            else if ([devName rangeOfString:@"264"].location != NSNotFound) {
                if ([devName rangeOfString:@"x1" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                    model = PARAGON264_16X1;
                }
                else if ([devName rangeOfString:@"x2" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                    model = PARAGON264_16X2;
                }
                else
                    model = PARAGON264_16X4;
            }
            else if ([devName rangeOfString:@"960"].location != NSNotFound) {
                if ([devName rangeOfString:@"x1" options:NSCaseInsensitiveSearch].location != NSNotFound)
                    model = PARAGON960_16X1;
                else
                    model = PARAGON960_16X4;
            }
            else
                model = EPARA264_32;
        }
        else if ([devName rangeOfString:@"EMV"].location != NSNotFound) {
            if ([devName rangeOfString:@"1201"].location != NSNotFound)
                model = EMV1201;
            else if ([devName rangeOfString:@"01"].location != NSNotFound)
                model = EMV_401_801_1601;
            else if ([devName rangeOfString:@"400S" options:NSCaseInsensitiveSearch].location != NSNotFound)
                model = EMV400S_FHD;
            else if ([devName rangeOfString:@"FHD"].location != NSNotFound)
                model = EMV_FHD_SERIES;
            else if ([devName rangeOfString:@"HD"].location != NSNotFound)
                model = EMV_400_800_1200_HD;
            else
                model = EMV_400_800_1200;
        }
        else if ([devName rangeOfString:@"EMX"].location != NSNotFound)
            model = EMX32;
        
        else if ([devName rangeOfString:@"EVS"].location != NSNotFound)
            model = EVS410_FHD;
        
        else if ([devName rangeOfString:@"ESN"].location != NSNotFound)
            model = ESN41_41E_FHD;
        
        else if ([devName rangeOfString:@"960"].location != NSNotFound) {
            if ([devName rangeOfString:@"ECOR"].location != NSNotFound) {
                if ([devName rangeOfString:@"x1" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                    model = ECOR960_16X1;
                }
                else
                    model = ECOR960_4_8_16F2;
                
            }
        }
        else if ([devName rangeOfString:@"EPHD"].location != NSNotFound) {
            if ([devName rangeOfString:@"08+"].location != NSNotFound)
                model = EPHD08_PLUS;
            else if ([devName rangeOfString:@"16+"].location != NSNotFound)
                model = EPHD16U;
            else
                model = EPHD_04;
        }
        else if([devName rangeOfString:@"ECORHD"].location != NSNotFound) {
            if ([devName rangeOfString:@"F"].location != NSNotFound) {
                model = ECORHD_4_8_16F;
            }
            else
                model = ECORHD_16X1;
        }
        else if([devName rangeOfString:@"ECORFHD"].location != NSNotFound) {
            model = ECOR_FHD_SERIES;
        }
        else if([devName rangeOfString:@"ELUX"].location != NSNotFound) {
            model = ELUX_SERIES;
        }
        else if ([devName rangeOfString:@"NVR"].location != NSNotFound)
        {
            if ([devName rangeOfString:@"8304"].location != NSNotFound)
                model = ENVR8304D_E_X;
            else if ([devName rangeOfString:@"8316"].location != NSNotFound)
                model = ENVR8316E;
            else
                model = NVR8004X;
        }
        else if ([devName rangeOfString:@"EDR HD"].location != NSNotFound)
            model = EDR_HD_4H4;
        else if ([devName rangeOfString:@"ENDEAVOR"].location != NSNotFound)
            model = ENDEAVOR264X4;
        else if ([devName rangeOfString:@"TUTIS"].location != NSNotFound)
            model = TUTIS_4_8_16F3;
        else if ([devName rangeOfString:@"XMS"].location != NSNotFound
                ||[devName rangeOfString:@"Commander"].location != NSNotFound
                ||[devName rangeOfString:@"Elite"].location != NSNotFound)              
            model = XMS_SERIES;
    }
    else if (productType == DEV_IPCAM) {
        
        model = EAN3200;
        if([devName rangeOfString:@"EAN3"].location != NSNotFound) {
            if([devName rangeOfString:@"3200"].location != NSNotFound)
                model = EAN3200;
            else
                model = EAN3300;
        }
        else if([devName rangeOfString:@"EAN7"].location != NSNotFound) {
            
            if([devName rangeOfString:@"221"].location != NSNotFound || [devName rangeOfString:@"260"].location != NSNotFound || [devName rangeOfString:@"360"].location != NSNotFound)
                model = EAN7221_7260_7360;
            else if([devName rangeOfString:@"220"].location != NSNotFound)
                model = EAN7220;
            else
                model = EAN7200;
        }
        else if([devName rangeOfString:@"EAN8"].location != NSNotFound || [devName rangeOfString:@"EAN9"].location != NSNotFound)
            model = EAN800A_AW;
        else if([devName rangeOfString:@"EBN2"].location != NSNotFound || [devName rangeOfString:@"EZN2"].location != NSNotFound)
            model = EBN268_V;
        else if([devName rangeOfString:@"EBN3"].location != NSNotFound || [devName rangeOfString:@"EZN3"].location != NSNotFound || [devName rangeOfString:@"EDN3"].location != NSNotFound)
            model = EBN368_V;
        else if([devName rangeOfString:@"EDN1"].location != NSNotFound)
            model = EDN1120_1220_1320;
        else if([devName rangeOfString:@"EDN228"].location != NSNotFound)
            model = EDN228;
        else if([devName rangeOfString:@"EDN3240"].location != NSNotFound)
            model = EDN3240;
        else if([devName rangeOfString:@"EDN368"].location != NSNotFound)
            model = EDN368M;
        else if([devName rangeOfString:@"EDN3"].location != NSNotFound)
            model = EDN3160;
        else if([devName rangeOfString:@"EDN8"].location != NSNotFound)
            model = EDN800;
        else if([devName rangeOfString:@"EDN"].location != NSNotFound)
            model = EDN2160_2260_2560;
        else if([devName rangeOfString:@"EFN3"].location != NSNotFound) {
            if ([devName rangeOfString:@"c"].location != NSNotFound) {
                model = EFN3320c_3321c;
            }
            else
                model = EFN3320_3321;
        }
        else if([devName rangeOfString:@"EHN"].location != NSNotFound) {
            if([devName rangeOfString:@"EHN1"].location != NSNotFound)
                model = EHN1120_1220_1320;
            else if([devName rangeOfString:@"EHN7"].location != NSNotFound)
                model = EHN7221_7260_7360;
            else if ([devName rangeOfString:@"61"].location != NSNotFound) {
                model = EHN3261_3361;
            }
            else
                model = EHN3160;
        }
        else if([devName rangeOfString:@"EMN2"].location != NSNotFound)
            model = EMN2120_2220_2320;
        else if([devName rangeOfString:@"EPN5"].location != NSNotFound) {
            if ([devName rangeOfString:@"210"].location != NSNotFound) {
                model = EPN5210;
            }
            else if ([devName rangeOfString:@"230"].location != NSNotFound) {
                model = EPN5230;
            }
        }
        else if([devName rangeOfString:@"EPN4"].location != NSNotFound) {
            if ([devName rangeOfString:@"0d"].location != NSNotFound) {
                model = EPN4220_4230_D;
            }
            else if ([devName rangeOfString:@"230"].location != NSNotFound) {
                model = EPN4230_P;
            }
            else
                model = EPN4122_4220;
        }
        else if([devName rangeOfString:@"EPN3"].location != NSNotFound)
            model = EPN3100;
        else if([devName rangeOfString:@"ETN"].location != NSNotFound)
            model = ETN2160_2260_2560;
        else if([devName rangeOfString:@"EVS2"].location != NSNotFound)
            model = EVS200A_AW;
        else if([devName rangeOfString:@"EVS4"].location != NSNotFound)
            model = EVS410;
        else if([devName rangeOfString:@"EZN8"].location != NSNotFound)
            model = EZN850;
        else if([devName rangeOfString:@"EZN368"].location != NSNotFound)
            model = EZN368_V_M;
        else if([devName rangeOfString:@"EZN2"].location != NSNotFound)
            model = EZN268_V;
        else if([devName rangeOfString:@"EZN1"].location != NSNotFound)
            model = EZN1160_1260_1360;
        else if([devName rangeOfString:@"EZN7"].location != NSNotFound)
            model = EZN7221_7260_7360;
        else if([devName rangeOfString:@"EZN"].location != NSNotFound) {
            if ([devName rangeOfString:@"61"].location != NSNotFound) {
                model = EZN3261_3361;
            }
            else
                model = EZN3160;
        }
        else if ([devName rangeOfString:@"EQN"].location != NSNotFound) {
            if ([devName rangeOfString:@"22"].location != NSNotFound)
                model = EQN2200_2201;
            else if ([devName rangeOfString:@"21"].location != NSNotFound)
                model = EQN2101_2171;
        }
    }
    
    return model;
}

- (NSInteger)checkStreamType:(NSInteger)mode product:(NSInteger)type
{
    NSInteger streamType = STREAM_P2;
    
    if (type != DEV_DVR) {
        if (mode & 1) {
            streamType = STREAM_NEVIO;
        }else {
            streamType = STREAM_HDIP;
        }
    }
    
    return streamType;
}

- (NSInteger)checkStreamType:(NSString *)protocol
{
    NSInteger stream = STREAM_HDIP;
    
    if ([protocol isEqualToString:@"NEVIO"])
        stream = STREAM_NEVIO;
    else if ([protocol isEqualToString:@"PSIA"])
        stream = STREAM_HDIP;
    else if ([protocol isEqualToString:@"EFP2"] || [protocol isEqualToString:@"EFP3"])
        stream = STREAM_P2;
    
    return stream;
}

- (BOOL)checkValidType:(NSString *)devName ip:(NSString *)address
{
    BOOL ret = YES;
    
    if ([devName rangeOfString:@"Series"].location != NSNotFound)  //filter ODM product
        ret = NO;
    
    if ([address isEqualToString:@"0.0.0.0"])
        ret = NO;

    return ret;
}

- (NSInteger)checkIPType:(NSString *)protocol
{
    NSInteger ipType = eit_dhcp;
    if ([protocol compare:@"DHCP" options:NSCaseInsensitiveSearch|NSNumericSearch] == NSOrderedSame)
        ipType = eit_dhcp;
    else if ([protocol compare:@"STATIC" options:NSCaseInsensitiveSearch|NSNumericSearch] == NSOrderedSame)
        ipType = eit_static;
    else if ([protocol compare:@"PPPOE" options:NSCaseInsensitiveSearch|NSNumericSearch] == NSOrderedSame)
        ipType = eit_pppoe;
    
    return ipType;
}

- (NSInteger)checkProduct:(NSString *)category
{
    NSInteger product = DEV_IPCAM;
    
    if ([category isEqualToString:@"DVR"] || [category isEqualToString:@"MDVR"]
        || [category isEqualToString:@"NVR"] || [category isEqualToString:@"XMS"])
        product = DEV_DVR;
    
    return product;
}

#pragma mark - XML Parser Delegate

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    //[currentElementName release];
    currentElementName = nil;
    currentElementName = [elementName copy];
    
    if ([currentElementName isEqualToString:@"DeviceInfo"]) {
        Device *dev = [[Device alloc] init];
        [deviceList addObject:dev];
        [dev release];
    }
    
    else if ([currentElementName isEqualToString:@"Ipv4"])
        blnIPv4 = YES;
    
    else if ([currentElementName isEqualToString:@"Platform"]) {
        NSString *modelName = [attributeDict objectForKey:@"Model"];
        iC_DVR.type = [self checkModel:DEV_DVR name:modelName];
        NSLog(@"[UDP] curDeviceModel : %@",modelName);
    }
    
    else if ([currentElementName isEqualToString:@"Host"]) {
        NSString *devName = [attributeDict objectForKey:@"Name"];
        NSData *decodeData = [[NSData alloc] initWithBase64EncodedString:devName options:0];
        NSString *decodeStr = [[NSString alloc] initWithData:decodeData encoding:NSUTF8StringEncoding];
        iC_DVR.name = [decodeStr stringByReplacingOccurrencesOfString:@"\0" withString:@""];
        NSLog(@"[UDP] curDeviceName : %@ , curDeviceIP : %@",iC_DVR.name,iC_DVR.ip);
        iC_DVR.streamType = STREAM_ICATCH;
        [deviceList addObject:iC_DVR];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    
    if ([currentElementName isEqualToString:@"Protocol"]) {
        Device *curDev = [deviceList lastObject];
        curDev.streamType = [self checkStreamType:string];
    }
    if ([currentElementName isEqualToString:@"IpType"]) {
        Device *curDev = [deviceList lastObject];
        curDev.blnStatic = (eit_static==[self checkIPType:string]);
    }
    if ([currentElementName isEqualToString:@"Addr"]) {
        if (blnIPv4) {
            Device *curDev = [deviceList lastObject];
            curDev.ip = [string copy];
        }
    }
    if ([currentElementName isEqualToString:@"Mask"]) {
        if (blnIPv4) {
            Device *curDev = [deviceList lastObject];
            curDev.maskAddr = [string copy];
        }
    }
    if ([currentElementName isEqualToString:@"Gateway"]) {
        if (blnIPv4) {
            Device *curDev = [deviceList lastObject];
            curDev.gateway = [string copy];
        }
    }
    if ([currentElementName isEqualToString:@"DNS1"] ||
        [currentElementName isEqualToString:@"DNS2"]) {
        if (blnIPv4) {
            Device *curDev = [deviceList lastObject];
            if (!curDev.dns)
                curDev.dns = [string copy];
            else
                curDev.dns = [curDev.dns stringByAppendingString:[NSString stringWithFormat:@":%@",string]];
        }
    }
    if ([currentElementName isEqualToString:@"DeviceName"]) {
        //NSLog(@"[UDP XML] DeviceName : %@",string);
        Device *curDev = [deviceList lastObject];
        if (!curDev.name) {
            curDev.name = [string copy];
        }
        else
            curDev.name = [curDev.name stringByAppendingString:string];
    }
    
    if (blnGetP2DevName) {
        if ([currentElementName isEqualToString:@"Model"]) {
            oldDevName = [string copy];
        }
    }
    
    if ([currentElementName isEqualToString:@"HttpPort"]) {
        Device *curDev = [deviceList lastObject];
        curDev.port = [string integerValue];
    }
    if ([currentElementName isEqualToString:@"MacAddr"]) {
        Device *curDev = [deviceList lastObject];
        curDev.mac = [string copy];
    }
    if ([currentElementName isEqualToString:@"ModelName"]) {
        SAVE_FREE(currentModelname);
        currentModelname = [string copy];
    }
    if ([currentElementName isEqualToString:@"Category"]) {
        Device *curDev = [deviceList lastObject];
        curDev.product = [self checkProduct:string];
        curDev.type = [self checkModel:curDev.product name:currentModelname];
        // avoid stupid firmware.......XXXX
        if (curDev.product == DEV_DVR)
            curDev.streamType = (curDev.type == XMS_SERIES) ? STREAM_XMS : STREAM_P2;
    }
    if ([currentElementName rangeOfString:@"Types"].location != NSNotFound) {
        strTypes = [string copy];
    }
    if ([strTypes isEqualToString:@"i:DVR"] && productFilter == DEV_DVR) {
        if ([currentElementName rangeOfString:@"XAddrs"].location != NSNotFound) {
            NSLog(@"[UDP] %@",string);
            SAVE_FREE(iC_DVR);
            iC_DVR = [[Device alloc] init];
            NSString *strHost = [string copy];
            NSArray *hostAry = [strHost componentsSeparatedByString:@"//"];
            NSArray *strArray = [[hostAry objectAtIndex:1] componentsSeparatedByString:@":"];
            iC_DVR.ip = [strArray objectAtIndex:0];
            iC_DVR.port = [[strArray objectAtIndex:1] integerValue];
            
            dispatch_queue_t reentrantAvoidanceQueue = dispatch_queue_create("reentrantAvoidanceQueue", DISPATCH_QUEUE_SERIAL);
            dispatch_async(reentrantAvoidanceQueue, ^{
                NSInteger iErrorCode = 0;
                NSString *cmd = [NSString stringWithFormat:@"dvr/brief.xml"];
                cmdSender = [[CommandSender alloc] initWithHost:[hostAry objectAtIndex:1] delegate:self];
                [cmdSender getData:cmd connection:&requestConnection synchronously:YES random:NO];
                
                // connection failed
                if (errorDesc!=nil ) {
                    iErrorCode = -1;
                }
                else {
                    
                    NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                    NSLog(@"[UDP] response:%@",response);
                    [cmdSender parseXmlResponse:response];
                    [response release];
                }
            });
        }
    }
}

- (void)parser:(NSXMLParser *)parser
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"DeviceInfo"]) {
        Device *curDev = [deviceList lastObject];
        if (curDev.product != productFilter) {
            [deviceList removeObject:curDev];
            return;
        }
        BOOL blnExist = NO;
        for (Device *refInfo in deviceList) {               //check if the device is already in list(bad method)
            if ([refInfo isEqual:curDev]) break;
            blnExist = [refInfo.ip isEqualToString:curDev.ip];
            if (blnExist)
                break;
        }
        if (blnExist)
            [deviceList removeObject:curDev];
    }
    
    if ([elementName isEqualToString:@"Ipv4"])
        blnIPv4 = NO;
}

#pragma mark - UDP Socket Delegate

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
	// You could add checks here
    NSLog(@"[UDP] Send");
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
	// You could add checks here
    NSLog(@"[UDP] Send Error:%@",error);
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotConnect:(NSError *)error
{
    NSLog(@"[UDP] Connect Failed");
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext
{
    NSLog(@"[UDP] Received");
    [self parseData:data];
}

- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error
{
    NSLog(@"[UDP] Closed");
}

#pragma mark Connection Delegate

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
// A delegate method called by the NSURLConnection as data arrives.
// it's a kind of callback function
//when call CommandSender.getData will set self to be delegate
//then in CommandSender.getData will set to become NSURLConnection's delegate
//after NSURLConnection receive data will trigger this method
{
    //NSLog(@"Received: %d", [data length]);
    if ( theConnection==requestConnection ) {
        
        [responseData appendData:data];
    }
}

- (void)connection:(NSURLConnection *)theConnection didReceiveResponse:(NSURLResponse *)response
// A delegate method called by the NSURLConnection when the request/response
// exchange is complete.
{
    NSHTTPURLResponse * httpResponse;
    httpResponse = (NSHTTPURLResponse *) response;
    //NSLog(@"Response:%@",httpResponse);
    
    if ( theConnection==requestConnection ) {
        
        SAVE_FREE(responseData);
        responseData = [NSMutableData alloc];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)theConnection
// A delegate method called by the NSURLConnection when the connection has been done successfully.
{
    if ( theConnection==requestConnection ) {
        
        requestConnection = nil;
    }
    
    NSLog(@"[SR] Connection finished");
}
- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error
// A delegate method called by the NSURLConnection if the connection fails.
{
    if ( theConnection==requestConnection ) {
        
        requestConnection = nil;
    }
    
    NSLog(@"[SR] Connection failed with error: %@", [error localizedDescription]);
}

@end
