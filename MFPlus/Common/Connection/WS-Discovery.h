//
//  WS-Discovery.h
//  EFViewerHD
//
//  Created by Steven Chang on 2015/3/19.
//  Copyright (c) 2015年 EF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CFNetwork/CFNetwork.h>
#import "GCDAsyncUdpSocket.h"
#import "Device.h"
#import "NetDefine.h"


@interface WS_Discovery : NSObject<GCDAsyncUdpSocketDelegate,NSXMLParserDelegate>
{
    GCDAsyncUdpSocket   *udpSock_ws;
    
    NSString            *currentElementName;
    Device              *currentDevice;
}

+ (NSInteger)checkModelByName:(NSString *)devName;

@property(nonatomic, retain) NSMutableArray     *deviceList;
@property(nonatomic)         BOOL               blnStop;

- (id)init;
- (void)start;
- (void)stop;

@end