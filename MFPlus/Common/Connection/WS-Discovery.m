//
//  UDPServer.m
//  EFViewerHD
//
//  Created by James Lee on 13/1/17.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#include <sys/socket.h>
#include <netinet/in.h>
#import "WS-Discovery.h"

NSString *discovery_probe_str_tds = @"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<Envelope xmlns:tds=\"http://www.onvif.org/ver10/device/wsdl\" xmlns=\"http://www.w3.org/2003/05/soap-envelope\"><Header><wsa:MessageID xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/08/addressing\">uuid:%@</wsa:MessageID><wsa:To xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/08/addressing\">urn:schemas-xmlsoap-org:ws:2005:04:discovery</wsa:To><wsa:Action xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/08/addressing\">http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe</wsa:Action></Header><Body><Probe xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://schemas.xmlsoap.org/ws/2005/04/discovery\"><Types>tds:Device</Types><Scopes /></Probe></Body></Envelope>";

NSString *discovery_probe_str_dn = @"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
"<Envelope xmlns:dn=\"http://www.onvif.org/ver10/network/wsdl\" xmlns=\"http://www.w3.org/2003/05/soap-envelope\"><Header><wsa:MessageID xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/08/addressing\">uuid:%@</wsa:MessageID><wsa:To xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/08/addressing\">urn:schemas-xmlsoap-org:ws:2005:04:discovery</wsa:To><wsa:Action xmlns:wsa=\"http://schemas.xmlsoap.org/ws/2004/08/addressing\">http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe</wsa:Action></Header><Body><Probe xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://schemas.xmlsoap.org/ws/2005/04/discovery\"><Types>dn:NetworkVideoTransmitter</Types><Scopes /></Probe></Body></Envelope>";

//const char uuid_buf[] = "8c9731a4-bcc8-22e4-ae13-00408cdbef64";

@interface WS_Discovery()
{
}
- (void)parseData:(NSData *)data fromAddress:(NSData *)address;
@end

@implementation WS_Discovery

@synthesize deviceList,blnStop;

#pragma mark - Action

- (id)init
{
    deviceList = [[NSMutableArray alloc] init];
    blnStop = YES;
    
    MMLog(@"[WS] Init %@",self);
    return self;
}

- (void)dealloc
{
    MMLog(@"[WS] dealloc %@",self);
    NSLog(@"[WS] dealloc %@",self);
    
    [deviceList removeAllObjects];
    SAVE_FREE(deviceList);
    
    SAVE_FREE(udpSock_ws);
    SAVE_FREE(currentElementName);
    
    [super dealloc];
}

- (void)start
{
    if (!blnStop)   return;
    
    uuid_t uuid;
    char uuid_buf[128];
    
    udpSock_ws = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    NSError *error;
    
    uuid_generate_time(uuid);
    uuid_unparse(uuid, uuid_buf);
    NSString *stringData1 = [NSString stringWithFormat:discovery_probe_str_tds,
                             [NSString stringWithCString:uuid_buf encoding:NSUTF8StringEncoding]];
    NSData *data1 = [stringData1 dataUsingEncoding:NSUTF8StringEncoding];
    
    uuid_generate_time(uuid);
    uuid_unparse(uuid, uuid_buf);
    NSString *stringData2 = [NSString stringWithFormat:discovery_probe_str_dn,
                             [NSString stringWithCString:uuid_buf encoding:NSUTF8StringEncoding]];
    NSData *data2 = [stringData2 dataUsingEncoding:NSUTF8StringEncoding];
    
    [udpSock_ws bindToPort:WS_DISCOVERY_PORT error:&error];
    [udpSock_ws enableBroadcast:YES error:&error];
    [udpSock_ws sendData:data1 toHost:WS_DISCOVERY_ADDR port:WS_DISCOVERY_PORT withTimeout:TIMEOUT tag:1];
    [udpSock_ws sendData:data2 toHost:WS_DISCOVERY_ADDR port:WS_DISCOVERY_PORT withTimeout:TIMEOUT tag:1];
    [udpSock_ws beginReceiving:&error];
    
    blnStop = NO;
    
    NSLog(@"[WS] Start Brocasting");
}

- (void)stop
{
    if (blnStop)    return;
    
    blnStop = YES;
    [udpSock_ws close];
    
    SAVE_FREE(udpSock_ws);
    NSLog(@"[WS] Stop Brocasting");
}

#pragma mark - Device checking

+ (NSInteger)checkModelByName:(NSString *)devName
{
    NSInteger model = 99;
    model = EAN3200;
    if([devName rangeOfString:@"EAN3"].location != NSNotFound) {
        if([devName rangeOfString:@"3200"].location != NSNotFound)
            model = EAN3200;
        else
            model = EAN3300;
    }
    else if([devName rangeOfString:@"EAN7"].location != NSNotFound) {
        
        if([devName rangeOfString:@"221"].location != NSNotFound || [devName rangeOfString:@"260"].location != NSNotFound || [devName rangeOfString:@"360"].location != NSNotFound)
            model = EAN7221_7260_7360;
        else if([devName rangeOfString:@"220"].location != NSNotFound)
            model = EAN7220;
        else
            model = EAN7200;
    }
    else if([devName rangeOfString:@"EAN8"].location != NSNotFound || [devName rangeOfString:@"EAN9"].location != NSNotFound)
        model = EAN800A_AW;
    else if([devName rangeOfString:@"EBN2"].location != NSNotFound)
        model = EBN268_V;
    else if([devName rangeOfString:@"EBN3"].location != NSNotFound)
        model = EBN368_V;
    else if([devName rangeOfString:@"EDN1"].location != NSNotFound)
        model = EDN1120_1220_1320;
    else if([devName rangeOfString:@"EDN228"].location != NSNotFound)
        model = EDN228;
    else if([devName rangeOfString:@"EDN3240"].location != NSNotFound)
        model = EDN3240;
    else if([devName rangeOfString:@"EDN368"].location != NSNotFound)
        model = EDN368M;
    else if([devName rangeOfString:@"EDN3"].location != NSNotFound)
        model = EDN3160;
    else if([devName rangeOfString:@"EDN8"].location != NSNotFound)
        model = EDN800;
    else if([devName rangeOfString:@"EDN"].location != NSNotFound)
        model = EDN2160_2260_2560;
    else if([devName rangeOfString:@"EFN3"].location != NSNotFound) {
        if ([devName rangeOfString:@"c"].location != NSNotFound) {
            model = EFN3320c_3321c;
        }
        else
            model = EFN3320_3321;
    }
    else if([devName rangeOfString:@"EHN"].location != NSNotFound) {
        if([devName rangeOfString:@"EHN1"].location != NSNotFound)
            model = EHN1120_1220_1320;
        else if([devName rangeOfString:@"EHN7"].location != NSNotFound)
            model = EHN7221_7260_7360;
        else if ([devName rangeOfString:@"61"].location != NSNotFound) {
            model = EHN3261_3361;
        }
        else
            model = EHN3160;
    }
    else if([devName rangeOfString:@"EMN2"].location != NSNotFound)
        model = EMN2120_2220_2320;
    else if([devName rangeOfString:@"EPN5"].location != NSNotFound) {
        if ([devName rangeOfString:@"210"].location != NSNotFound) {
            model = EPN5210;
        }
        else if ([devName rangeOfString:@"230"].location != NSNotFound) {
            model = EPN5230;
        }
    }
    else if([devName rangeOfString:@"EPN4"].location != NSNotFound) {
        if ([devName rangeOfString:@"0d"].location != NSNotFound) {
            model = EPN4220_4230_D;
        }
        else if ([devName rangeOfString:@"230"].location != NSNotFound) {
            model = EPN4230_P;
        }
        else
            model = EPN4122_4220;
    }
    else if([devName rangeOfString:@"EPN3"].location != NSNotFound)
        model = EPN3100;
    else if([devName rangeOfString:@"ETN"].location != NSNotFound)
        model = ETN2160_2260_2560;
    else if([devName rangeOfString:@"EVS2"].location != NSNotFound)
        model = EVS200A_AW;
    else if([devName rangeOfString:@"EVS4"].location != NSNotFound)
        model = EVS410;
    else if([devName rangeOfString:@"EZN8"].location != NSNotFound)
        model = EZN850;
    else if([devName rangeOfString:@"EZN368"].location != NSNotFound)
        model = EZN368_V_M;
    else if([devName rangeOfString:@"EZN2"].location != NSNotFound)
        model = EZN268_V;
    else if([devName rangeOfString:@"EZN1"].location != NSNotFound)
        model = EZN1160_1260_1360;
    else if([devName rangeOfString:@"EZN7"].location != NSNotFound)
        model = EZN7221_7260_7360;
    else if([devName rangeOfString:@"EZN"].location != NSNotFound) {
        if ([devName rangeOfString:@"61"].location != NSNotFound) {
            model = EZN3261_3361;
        }
        else
            model = EZN3160;
    }
    else if ([devName rangeOfString:@"EQN"].location != NSNotFound) {
        if ([devName rangeOfString:@"22"].location != NSNotFound)
            model = EQN2200_2201;
        else if ([devName rangeOfString:@"21"].location != NSNotFound)
            model = EQN2101_2171;
    }
    else
        model = ONVIF;

    return model;
}

- (void)parseData:(NSData *)data fromAddress:(NSData *)address
{
    NSString *xmlString = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
    NSLog(@"[WS] %@",xmlString);
    xmlString = [xmlString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    xmlString = [xmlString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    xmlString = [xmlString stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    xmlString = [xmlString stringByReplacingOccurrencesOfString:@"&apos;" withString:@"\'"];
    if (xmlString != NULL) {
        SAVE_FREE(currentDevice);
        currentDevice = [[Device alloc] init];
        NSString *hostip = nil;
        uint16_t port = 0;
        [GCDAsyncUdpSocket getHost:&hostip port:&port fromAddress:address];
        currentDevice.ip = hostip;
        NSXMLParser *parser = [[[NSXMLParser alloc] initWithData:[xmlString dataUsingEncoding:NSUTF8StringEncoding]] autorelease];
        [parser setDelegate:self];
        [parser parse];
    }
}

#pragma mark - XML Parser Delegate

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    [currentElementName release];
    currentElementName = [elementName copy];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if ([currentElementName rangeOfString:@"Scopes"].location != NSNotFound) {
        NSArray *items = [string componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" \r\n"]];
        for (NSString *item in items)
        {
            NSRange range = [item rangeOfString:@"org/name"];
            if (range.location == NSNotFound)
                continue;
            range.location = range.location+9;
            range.length = [item length] - range.location;
            NSString *subString = [item substringWithRange:range];
            NSString *decodeString = [subString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            if (!currentDevice.name) {
                currentDevice.name = decodeString;
            }
            else
                currentDevice.name = [currentDevice.name stringByAppendingFormat:@"_%@",decodeString];
            
            //20151113 modified by Ray Lin, after search cannot get device type.
            currentDevice.type = [WS_Discovery checkModelByName:currentDevice.name];
            
            //NSLog(@"[WS] currentDevice.name : %@",currentDevice.name);
        }
    }
    if ([currentElementName rangeOfString:@"XAddrs"].location != NSNotFound) {
        if ([string rangeOfString:@" "].location != NSNotFound)
        {
            NSArray *addrs = [string componentsSeparatedByString:@" "];
            for (int i = 0; i < [addrs count]; i++)
            {
                NSString *device_service = [addrs objectAtIndex:i];
                if ([device_service rangeOfString:@"169.254"].location != NSNotFound) {
                    //do nothing
                }
                //if ([string rangeOfString:@"http://"].location != NSNotFound)
                else if ([device_service rangeOfString:@"http://["].location != NSNotFound) {
                    //do nothing
                }
                else
                {
                    if (![device_service isEqualToString:@""]) {
                        NSArray *arry = [device_service componentsSeparatedByString:@"http://"];
                        NSString *ipstr = [arry objectAtIndex:1];
                        if ([ipstr rangeOfString:@":"].location != NSNotFound)
                        {
                            NSArray *iparry = [ipstr componentsSeparatedByString:@":"];
                            currentDevice.ip = [iparry objectAtIndex:0];
                            currentDevice.port = [[[[iparry objectAtIndex:1] componentsSeparatedByString:@"/"] objectAtIndex:0] integerValue];
                        }
                        else
                        {
                            NSArray *iparry = [ipstr componentsSeparatedByString:@"/"];
                            currentDevice.ip = [iparry objectAtIndex:0];
                            currentDevice.port = 80;
                        }
                        currentDevice.device_service = [device_service copy];
                    }
                }
            }
            if (currentDevice.device_service == nil)
                [addrs objectAtIndex:0];
        }
        else
        {
            if ([string rangeOfString:@"http://"].location != NSNotFound)
            {
                NSArray *arry = [string componentsSeparatedByString:@"http://"];
                NSString *ipstr = [arry objectAtIndex:1];
                if ([ipstr rangeOfString:@":"].location != NSNotFound)
                {
                    NSArray *iparry = [ipstr componentsSeparatedByString:@":"];
                    currentDevice.ip = [iparry objectAtIndex:0];
                    currentDevice.port = [[[[iparry objectAtIndex:1] componentsSeparatedByString:@"/"] objectAtIndex:0] integerValue];
                }
                else
                {
                    NSArray *iparry = [ipstr componentsSeparatedByString:@"/"];
                    currentDevice.ip = [iparry objectAtIndex:0];
                    currentDevice.port = 80;
                }
                currentDevice.device_service = [string copy];
            }
        }
    }
}

- (void)parser:(NSXMLParser *)parser
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
{
    if ([elementName rangeOfString:@"Envelope"].location != NSNotFound)
    {
        if ((currentDevice.name == NULL))
        {
            return;
        }
        //currentDevice.streamType = STREAM_ONVIF;
        currentDevice.streamType = [Device getStreamType:currentDevice.type product:1];
        currentDevice.product = DEV_IPCAM;
        BOOL blnExist = NO;
        for (Device *refInfo in deviceList)     //check if the device is already in list(bad method)
        {
            blnExist = [refInfo.ip isEqualToString:currentDevice.ip];
            if (blnExist)   break;
        }
        if (!blnExist)
        {
            [deviceList addObject:currentDevice];
        }
    }
}

#pragma mark - UDP Socket Delegate

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
    // You could add checks here
    NSLog(@"[WS] Send");
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
    // You could add checks here
    NSLog(@"[WS] Send Error:%@",error);
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotConnect:(NSError *)error
{
    NSLog(@"[WS] Connect Failed");
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext
{
    NSLog(@"[WS] Received");
    [self parseData:data fromAddress:address];
}

- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error
{
    NSLog(@"[WS] Closed");
}

@end
