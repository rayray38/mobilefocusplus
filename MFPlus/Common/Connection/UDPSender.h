//
//  UDPServer.h
//  EFViewerHD
//
//  Created by James Lee on 13/1/17.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CFNetwork/CFNetwork.h>
#import "GCDAsyncUdpSocket.h"
#import "Device.h"
#import "NetDefine.h"
#import "CommandSender.h"


@interface UDPSender : NSObject<GCDAsyncUdpSocketDelegate,NSXMLParserDelegate>
{
    GCDAsyncUdpSocket   *udpSock;
    CommandSender       *cmdSender;
    
    NSString            *currentElementName;
    NSString            *currentModelname;
    NSString			*errorDesc;
    NSString            *oldDevName;
    NSString            *iCatch_ipAddr;
    NSURLConnection		*requestConnection;
    NSMutableData		*responseData;
    
    BOOL                *blnIPv4;
    BOOL                *blnGetP2DevName;
    BOOL                *blnGetICatchDevName;
}

@property(nonatomic, retain) NSMutableArray     *deviceList;
@property(nonatomic)         BOOL               blnStop;
@property(nonatomic)         NSInteger          productFilter;

- (id)init;
- (void)start;
- (void)stop;

@end