//
//  NetDefine.h
//  EFSideKick
//
//  Created by Nobel on 2015/1/14.
//  Copyright (c) 2015年 EverFocus. All rights reserved.
//

#ifndef EFSideKick_NetDefine_h
#define EFSideKick_NetDefine_h

#define TIMEOUT     5000

#define TX_PORT     31501
#define RX_PORT     31500
#define EF_MAGIC    0x26982334
#define BROADCAST_ADDR @"255.255.255.255"

#define WS_DISCOVERY_PORT 3702
#define WS_DISCOVERY_ADDR @"239.255.255.250"

#define NVR265_SEND_PORT    9333
#define NVR265_LISTEN_PORT  57078
#define NVR265_LEN          464

/**
* EverFocus Packet Structure
*
*/
#pragma pack(push,1)

typedef enum : int
{
    ect_devRequest = 0x401,
    ect_devResponse = 0x402,
    
    ect_devConfig = 0x403,
    ect_devConfigResponse = 0x602,
    
    ect_authRequest = 0x404,
    ect_authResponse = 0x405,
    
    ect_macConfig = 0x501,
    ect_macResponse = 0x603,
    
    ect_fwResponse = 0x601,
}ECommandType;

typedef enum : int
{
    ept_devStruct = 0,
    ept_devXML,
    ept_fwUpdateXML,
}EPacketType;

typedef struct _CMD_HEADER
{
    int             magic;      //[0-3] EF_MAGIC
    short           command;    //[4-5] ECommandType
    unsigned char   txMac[6];   //[6-11]
    unsigned char   rxMac[6];   //[12-17]
    unsigned char   pktType;    //[18] EPacketType
    unsigned char   resv;       //[19]
    short           pktSize;    //[20-21] size of the packet after CMD_HEADER
}CMD_HEADER; //22 bytes

typedef enum : int
{
    edp_nevio = 1,
    edp_psia = 2,
    edp_onvif = 4,
}EDeviceProtocol;

typedef enum : int
{
    eit_dhcp = 0x01,
    eit_static,
    eit_pppoe
}EIpType;

typedef struct _DEV_INFO
{
    unsigned char   instrument[3];  //[0-2]
    unsigned char   protocol;       //[3] EDeviceProtocol
    unsigned char   ipType;         //[4] EIpType
    unsigned char   ipAddr[4];      //[5-8]
    unsigned char   maskAddr[4];    //[9-12]
    unsigned char   gateWay[4];     //[13-16]
    unsigned char   DNS1[4];        //[17-20]
    unsigned char   DNS2[4];        //[21-24]
    unsigned char   name[78];       //[25-102]
    short           httpPort;       //[103-104]
}DEV_INFO; //105 bytes

typedef struct _AUTH_INFO
{
    unsigned char   uid[21];    //[0-20]
    unsigned char   pwd[21];    //[21-41]
}AUTH_INFO; //42 bytes



/**
 * AFreey Packet Structure
 *
 */

#define TX_PORT_EZ  49160
#define RX_PORT_EZ  49160
#define TX_MAGIC_EZ 16177572
#define RX_MAGIC_EZ 18180614
#define EZ_LEN      284

typedef struct _SEARCH_HEAD
{
    unsigned int magic_number;          //[0-3] magic_number is 16177572 for request type, 18180614 for reply type
    unsigned int type;                  //[4-7] type:request 0x01 or reply 0x02
    
}SEARCH_HEAD;//8 Bytes

typedef struct _SEARCH_DATA_
{
    unsigned char mac[8];               //[0-7]
    unsigned int  ip;                   //[8-11]
    unsigned int  web_port;             //[12-15]
    unsigned char string[256];          //[16-271]
    unsigned int  s_port;               //[272-275]
}SEARCH_DATA;//276 Bytes

typedef struct _SEARCH_SOCK_REP_
{
    SEARCH_HEAD head;
    SEARCH_DATA data;
    
}SEARCH_SOCK_REP;//284 Bytes

#pragma pack(pop)

#endif
