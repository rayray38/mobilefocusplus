//
//  RecordControl.h
//  EFViewerHD
//
//  Created by James Lee on 2014/1/15.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "EFViewerAppDelegate.h"
#include "Mp4Converter.h"

#define VO_TYPE_NONE	0
#define VO_TYPE_IFRAME	1
#define VO_TYPE_PFRAME	2

#define MAX_RECORD_TIME 600 //10 minutes

#define REC_START   @"REC_START"
#define REC_STOP    @"REC_STOP"

typedef struct _VIDEO_PACKAGE {
	
	unsigned int	codec;
	unsigned char	type;
	unsigned long	sec;
	unsigned short	ms;
	unsigned long	seq;
	unsigned long	size;
	unsigned short	width;  // don't care
	unsigned short	height; // don't care
	void			*data;
    unsigned long   channel;
	
} VIDEO_PACKAGE;

@protocol RecordDelegate

@required - (void)recordStateCallback:(NSString *)msg;

@end

@interface RecordControl : NSObject
{
    AVFormatContext *pFmtCtx;
    
    NSURL           *recPath;
    int64_t         pts_init, dts_init;
    
    BOOL            blnRecording;
    BOOL            blnFirstI;
    
    EFMediaTool     *mediaLibrary;
    NSTimer         *recTimer;
    NSInteger       recCount;
}

- (BOOL)startRecordingWithView:(NSInteger)vIdx vCtx:(AVCodecContext *)pVCtx aCtx:(AVCodecContext *)pACtx FPS:(NSInteger)fps;
- (void)writeVideoFrame:(VIDEO_PACKAGE *)frame;
- (void)writeAudioFrame:(uint8_t *)data size:(NSInteger)length;
- (void)stopRecording;
- (void)recWatchDog;
- (uint64_t)getFreeDiskspace;

@property(nonatomic)        BOOL           blnRecording;
@property(nonatomic)        NSInteger      recView;
@property(nonatomic,assign) id<RecordDelegate>  recDelegate;

@end
