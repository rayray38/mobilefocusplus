//
//  RecordControl.m
//  EFViewerHD
//
//  Created by James Lee on 2014/1/15.
//  Copyright (c) 2014年 EF. All rights reserved.
//

#import "RecordControl.h"

@implementation RecordControl

@synthesize recView,blnRecording,recDelegate;

#pragma mark - LifeCycle

- (id)init
{
    if (![super init])
    return nil;
    
    blnRecording = NO;
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    mediaLibrary = appDelegate.mediaLibrary;
    
    return self;
}

- (void)dealloc
{
    if (recPath) {
        [recPath release];
        recPath = nil;
    }
    
    mediaLibrary = nil;
    
    [super dealloc];
}

#pragma mark - Recording Control

- (BOOL)startRecordingWithView:(NSInteger)vIdx vCtx:(AVCodecContext *)pVCtx aCtx:(AVCodecContext *)pACtx FPS:(NSInteger)fps
{
    if (blnRecording)
        return NO;
    
    if (pVCtx == NULL && pACtx == NULL)
        return NO;

    recView = vIdx;
    recPath = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@tmp.mp4",NSTemporaryDirectory()]];
    [recPath retain];
    
    pFmtCtx = avformat_alloc_context();
    pts_init = 0;
    dts_init = 0;
    
    if(!MP4FileCreate([recPath path].UTF8String, pFmtCtx, pVCtx, pACtx, fps, 0, 0)) {
        NSLog(@"[RC] MP4FileCreate Failed");
        return NO;
    }
    
    recCount = 0;
    recTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f
                                       target:self
                                     selector:@selector(recWatchDog)
                                     userInfo:Nil
                                      repeats:YES];
    NSLog(@"[RC] Start Recording");
    blnRecording = YES;
    blnFirstI = NO;

    return YES;
}

- (void)recWatchDog
{
    recCount++;
    if (recCount >= (MAX_RECORD_TIME/5) || ([self getFreeDiskspace] < 50)) {
        
        [recDelegate recordStateCallback:REC_STOP];
        [recTimer invalidate];
    }
}

- (void)stopRecording
{
    if (!blnRecording)
    return;
    
    blnRecording = NO;
    [recTimer invalidate];
    MP4FileClose(pFmtCtx);
    
    NSLog(@"[RC] Stop Recording");
    NSString *path = [NSString stringWithString:recPath.path];
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *albumName = [NSString stringWithFormat:@"%@",appDelegate._AppName];
    [mediaLibrary saveVideoFileAtPath:path toAlbum:albumName];
    
//	[mediaLibrary saveVideoFileAtPath:recPath toAlbum:albumName withCompletionBlock:^(NSError *error)
//     {
//         if (error != nil)
//             NSLog(@"[RC] Error %@",error.debugDescription);
//         else
//             NSLog(@"[RC] writeToAlbum Done");
//         
//         
//         NSFileManager *fileManager = [NSFileManager defaultManager];
//         if ([fileManager fileExistsAtPath:path]) {
//             NSError *error;
//             BOOL success = [fileManager removeItemAtPath:path error:&error];
//             if (!success)
//                 NSLog(@"[RC] RemoveFile Error:%@",error);
//         }
//     }];
}

- (void)writeVideoFrame:(VIDEO_PACKAGE *)frame
{
    AVPacket avpkt;
    av_init_packet(&avpkt);
    avpkt.size = frame->size;
    avpkt.data = frame->data;
    avpkt.flags |= AV_PKT_FLAG_KEY;
    
    if (frame->type == VO_TYPE_IFRAME)
        blnFirstI = YES;
    
    if (!blnFirstI)
        return;
    
    if (pts_init==0 && dts_init==0) {
        pts_init = avpkt.pts;
        dts_init = avpkt.dts;
    }
    
    //NSLog(@"Size:%d dts:%llx pts:%llx",avpkt.size, avpkt.dts-dts_init,avpkt.pts-pts_init);
    
    MP4FileWriteFrame(pFmtCtx, avpkt.stream_index, avpkt.data, avpkt.size, avpkt.dts-dts_init, avpkt.pts-pts_init);
}

- (void)writeAudioFrame:(uint8_t *)data size:(NSInteger)length
{
    
}

- (uint64_t)getFreeDiskspace
{
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = ([fileSystemSizeInBytes unsignedLongLongValue]/1024)/1024;
        totalFreeSpace = ([freeFileSystemSizeInBytes unsignedLongLongValue]/1024)/1024;
        NSLog(@"[RC] Memory Capacity of %llu MiB with %llu MiB Free memory available.", totalSpace, totalFreeSpace);
    } else {
        NSLog(@"[RC] Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return totalFreeSpace;
}

@end
