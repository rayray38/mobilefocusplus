//
//  Mp4Converter.h
//  EFViewerHD
//
//  Created by James Lee on 14/1/15
//  Copyright (c) 2013年 EF. All rights reserved.
//

#ifndef MP4CONVERTER_H
#define MP4CONVERTER_H

#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"

extern int  MP4FileCreate(const char *pFilePath, AVFormatContext *fc, AVCodecContext *pCodecCtx,AVCodecContext *pAudioCodecCtx, double fps, void *p, int len );
extern void MP4FileClose(AVFormatContext *fc);
extern void MP4FileWriteFrame(AVFormatContext *fc, int vStreamId, const void* p, int len, int64_t dts, int64_t pts);
extern void MP4FileWriteAudioFrame(AVFormatContext *fc, AVCodecContext  *pAudioCodecContext,int vStreamIdx, const void* p, int len, int64_t dts, int64_t pts );

extern int MoveMP4MoovToHeader(char *pSrc, char *pDst);

#endif
