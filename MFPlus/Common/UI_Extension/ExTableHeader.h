//
//  ExTableHeader.h
//  EFViewerPlus
//
//  Created by James Lee on 2014/6/17.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CameraInfo.h"

@interface ExTableHeader : UITableViewHeaderFooterView
{
    IBOutlet UILabel        *titleLabel;
    NSInteger               group_id;
}

@property(nonatomic, retain) IBOutlet UIButton       *badgeBtn;
@property(nonatomic, retain) IBOutlet UIButton       *detailBtn;
@property(nonatomic, retain) IBOutlet UIButton       *deleteBtn;
@property(nonatomic, retain) IBOutlet UIButton       *bgButton;

- (void)setGroupID:(NSInteger)_ID;
- (NSInteger)getGroupID;
- (void)setTitle:(NSString *)title;
- (void)setEditing:(BOOL)blnEditing;
- (void)setSize:(CGSize)_size;

@end

@class ExTableViewCell;
@class CameraInfo;

@protocol ExTableViewCellDelegate  <NSObject>

- (void)ExTableViewCell:(ExTableViewCell *)cell didTapIconWithTreeItem:(CameraInfo *)treeItem;

@end

@interface ExTableViewCell : UITableViewCell

@property(nonatomic, strong) UIButton    *iconButton;
@property(nonatomic, strong) UILabel     *titleTextLabel;
@property(nonatomic, strong) CameraInfo  *cameraItem;
@property(nonatomic)         BOOL        blnIsExpanded;

- (void)setLevel:(NSInteger)pixels;
- (void)setTitle:(NSString *)title;

@end
