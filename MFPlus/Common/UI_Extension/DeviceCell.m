//
//  DeviceCell.m
//  EFViewerHD
//
//  Created by James Lee on 13/1/21.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import "DeviceCell.h"

@implementation DeviceCell

@synthesize devName,devModel,devHost,addBtn,popBtn;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
