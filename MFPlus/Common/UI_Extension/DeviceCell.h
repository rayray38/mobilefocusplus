//
//  DeviceCell.h
//  EFViewerHD
//
//  Created by James Lee on 13/1/21.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceCell : UITableViewCell
{
    IBOutlet UILabel        *devName;
    IBOutlet UILabel        *devModel;
    IBOutlet UILabel        *devHost;
    IBOutlet UIButton       *addBtn;
    IBOutlet UIButton       *popBtn;
}

@property(nonatomic, retain) IBOutlet UILabel        *devName;
@property(nonatomic, retain) IBOutlet UILabel        *devModel;
@property(nonatomic, retain) IBOutlet UILabel        *devHost;
@property(nonatomic, retain) IBOutlet UIButton       *addBtn;
@property(nonatomic, retain) IBOutlet UIButton       *popBtn;

@end
