//
//  ExTableHeader.m
//  EFViewerPlus
//
//  Created by James Lee on 2014/6/17.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import "ExTableHeader.h"

@implementation ExTableHeader

@synthesize badgeBtn,detailBtn,deleteBtn,bgButton;

#pragma mark - Action

- (void)setTitle:(NSString *)title
{
    [titleLabel setText:title];
}

- (void)setGroupID:(NSInteger)_ID
{
    group_id = _ID;
    [detailBtn setTag:_ID];
    [deleteBtn setTag:_ID];
    [bgButton setTag:_ID];
}

- (NSInteger)getGroupID
{
    return group_id;
}

- (void)setEditing:(BOOL)blnEditing
{
    [detailBtn setHidden:blnEditing];
    [deleteBtn setHidden:!blnEditing];
}

- (void)setSize:(CGSize)_size
{
    [super setFrame:CGRectMake(0, 0, _size.width, _size.height)];
    CGSize _frame = self.frame.size;
    CGSize _btnFrame = detailBtn.frame.size;
    CGPoint newPos = CGPointMake(_frame.width - 43, 17);
    [detailBtn setFrame:CGRectMake(newPos.x, newPos.y, _btnFrame.width, _btnFrame.height)];
    [deleteBtn setFrame:CGRectMake(newPos.x, newPos.y, _btnFrame.width, _btnFrame.height)];
}

@end

@implementation ExTableViewCell

@synthesize titleTextLabel,iconButton,cameraItem,blnIsExpanded;

#pragma mark - Action

- (void)layoutSubviews
{
    //The default implement of the layoutSubviews
    [super layoutSubviews];
    
    CGRect textLabelFrame = titleTextLabel.frame;
    self.textLabel.frame = textLabelFrame;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        iconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        [iconButton setSelected:NO];
        [iconButton setImage:[UIImage imageNamed:@"collapse"] forState:UIControlStateNormal];
        [iconButton setImage:[UIImage imageNamed:@"expand"] forState:UIControlStateSelected];
        
        [self.accessoryView setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin];
        
        titleTextLabel = [[UILabel alloc] init];
        [titleTextLabel setBackgroundColor:[UIColor clearColor]];
        [titleTextLabel setTextColor:[UIColor lightTextColor]];
        [titleTextLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [titleTextLabel setNumberOfLines:0];
        [titleTextLabel setFrame:CGRectMake(15, 0, self.frame.size.width-120, self.frame.size.height)];
        
        [self.contentView addSubview:titleTextLabel];
        [self.layer setMasksToBounds:YES];
        
        cameraItem.selected = NO;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setLevel:(NSInteger)level
{
    CGRect rect;
    
    rect = titleTextLabel.frame;
    rect.origin.x = 15 + 10 * level;
    titleTextLabel.frame = rect;
}

- (void)setTitle:(NSString *)title
{
    [titleTextLabel setText:title];
}

@end
