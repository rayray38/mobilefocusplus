//
//  AudioOutput.h
//  MobileFocus
//
//  Created by James Lee on 2011/3/4.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <AudioToolbox/AudioToolbox.h>

#import "AudioCodec.h"
#import "G723Codec.h"

#define AQ_BUFFER_NUMBER	2    // dont set too large, or it would dealy too long time
#define AQ_BUFFER_SIZE		0x1000
#define AUDIO_BPS           2
#define AUDIO_BYTE          8

@interface AudioOutput : NSObject {

	// codec
	AudioCodec* audioDecoder;   //FFMpeg
    G723Codec*  g723Decoder;    //G723
	Byte*		pOutAudio;
	
	// buffer
	void*	readPos;
	void*	writePos;
	void*	dataBuffer;
	long		lpcmBufSize;
	long		dalayLength;
	int		readFlag;
	int		writeFlag;
	
	// player
	AudioStreamBasicDescription		audioDesc;
	AudioQueueRef					queue;
	AudioQueueBufferRef				aqbuf[AQ_BUFFER_NUMBER];
	
	BOOL	isDecoding;
	BOOL	isStop;
}

// codec
- (id)initWithCodecId:(int)cid srate:(int)srate bps:(int)bps balign:(int)balign fsize:(int)fsize channel:(int)channel;
- (id)initWithG723Codec;
- (void)playAudio:(Byte *)pInAudio length:(long)length;
- (BOOL)start;
- (void)stop;
- (void)close;
- (AVCodecContext *)getCodecCtx;

// player
- (void)mute;

@property(nonatomic) NSInteger	codecId;
@property(nonatomic) NSInteger  amFlag;

@end
