//
//  AudioInput.m
//  EFViewer
//
//  Created by James Lee on 2011/3/9.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import "AudioInput.h"

static void CASoundAQInputCallback(
                                   void *									inUserData,
                                   AudioQueueRef							inAQ,
                                   AudioQueueBufferRef					inBuffer,
                                   const AudioTimeStamp *					inStartTime,
                                   UInt32									inNumPackets,
                                   const AudioStreamPacketDescription*	inPacketDesc);

@interface AudioInput (private)

- (void)writeData:(int)writeNumBytes buffer:(void *)buffer;
- (void*)nextBlockRef; // point to next block address if have
- (void)encodeAndPostProcess; // encode and call the callback function

@end

@implementation AudioInput

@synthesize codecId;

- (id)initWithCodecId:(int)cid srate:(int)srate bps:(int)bps balign:(int)balign fsize:(int)fsize {
	
	if (!(self=[super init])) return nil;
	
	blockAlign	= balign;
	frameSize	= fsize;
	lpcmBufSize = fsize*2*50;
	
	// init audio codec
	audioEncoder = [[AudioCodec alloc] initEncoderWithCodecId:cid srate:srate bps:bps balign:balign fsize:fsize];
    
	// buffer
	dataBuffer	= malloc(lpcmBufSize);
	readPos		= dataBuffer;
	writePos	= dataBuffer;
	readFlag	= 0;
	writeFlag	= 0;
	
	// initial AQ parameters
	audioDesc.mSampleRate		= srate;
	audioDesc.mFormatID			= kAudioFormatLinearPCM;
	audioDesc.mFormatFlags		= kAudioFormatFlagIsSignedInteger|kAudioFormatFlagIsPacked;
	audioDesc.mBytesPerPacket	= 2;
	audioDesc.mFramesPerPacket	= 1;
	audioDesc.mBytesPerFrame	= 2;
	audioDesc.mChannelsPerFrame	= 1;
	audioDesc.mBitsPerChannel	= 16;
	audioDesc.mReserved			= 0;
    
    codecId = cid;
	
    NSLog(@"[AI] init CID:%d",cid);
	return self;
	
initError:
	[self release];
	return nil;
}

- (void)setPostProcessAction:(id)delegate action:(SEL)action {
	
	actionDelegate = delegate;
	postProcessAction = action;
}

- (BOOL)start {
	
	if (queue!=nil) return YES;
	
	// open AQ
	OSStatus error;
	error = AudioQueueNewInput( &audioDesc, CASoundAQInputCallback, self, NULL, NULL ,0 , &queue);
	if (error) {
		NSLog(@"[AI] AudioQueueNewInput: OS error %li\n", (long)error);
		return NO;
	}
	
	// alloc AQ buffer
	for (UInt32 i = 0; i < AQ_BUFFER_NUMBER_IN; ++i) {
		
		error = AudioQueueAllocateBuffer(queue, AQ_BUFFER_SIZE_IN, aqbuf+i);
		if (error) {
			NSLog(@"[AI] AudioQueueAllocateBuffer: OS error %li\n", (long)error);
			[self stop];
			return NO;
		}
		
		CASoundAQInputCallback(self, queue, aqbuf[i], NULL, 0, NULL);
	}
	
	// start AQ
	error = AudioQueueStart(queue, NULL);
	if (error) {
		NSLog(@"[AI] AudioQueueStart: OS error %li\n", (long)error);
		[self stop];
		return NO;
	}
	
	NSLog(@"[AI] start");
	return YES;
}

- (void)stop {
	
	if (queue==nil) return;
	
	// stop AQ
	OSStatus error = AudioQueueStop(queue, true);
	if (error) {
		NSLog(@"[AI] AudioQueueStop: OS error %li\n", (long)error);
	}
	
	// dispose AQ
	AudioQueueDispose(queue, true);
	queue = nil;
	
	NSLog(@"[AI] stop");
}

- (void)writeData:(int)writeNumBytes buffer:(void *)buffer {
	
	int bytesToFill = writeNumBytes;
	void* fillPtr = buffer;
	int bytesFilled = 0;
	
	while (true) {
		
		long ioNumBytes = 0;
		long bufRemnantSize = dataBuffer+lpcmBufSize-writePos;
		
		if(bytesToFill<bufRemnantSize) {
			ioNumBytes = bytesToFill;
		}
		else {
			ioNumBytes = bufRemnantSize;
		}
		
		memcpy(writePos, fillPtr, ioNumBytes);
		
		writePos += ioNumBytes;
		if (writePos>=(dataBuffer+lpcmBufSize)) {
			
			writePos = dataBuffer;
			writeFlag++;
		}
		
		fillPtr += ioNumBytes;
		bytesFilled += ioNumBytes;
		bytesToFill -= ioNumBytes;
		
		if (bytesToFill == 0) {
			break;
		}
	}
}

- (void*)nextBlockRef {
	
	void* pBlock = NULL;
	int frameLen = frameSize*2;
	
	if (writeFlag > readFlag ||
	    writePos > (readPos+frameLen)) {
        
		pBlock = readPos;
		readPos = readPos+frameLen;
	}
    
	if (readPos >=(dataBuffer+lpcmBufSize)) {
		
		readPos = dataBuffer;
		readFlag++;
	}
	
	return pBlock;
}

- (void)encodeAndPostProcess {
    
	void* buffer = NULL;
	
	while ((buffer = [self nextBlockRef])) {
		
		void* pOutAudio = malloc(blockAlign);
		
		// encode
		[audioEncoder encodeFrame:buffer pOutAudio:pOutAudio bufSize:blockAlign];
		
		// custom callback
		if(actionDelegate!=nil && postProcessAction!=nil &&
		   [actionDelegate respondsToSelector:postProcessAction]) {
			
			NSData *audioData = [[NSData alloc] initWithBytesNoCopy:pOutAudio length:blockAlign];
			[actionDelegate performSelector:postProcessAction withObject:audioData];
		}
	}
}

- (void)close   //without release self, keep the handle&mem until detailView release
{
    [self stop];
	
	// Close the codec
    if (audioEncoder) {
        
        [audioEncoder release];
        audioEncoder = nil;
    }
	
	// free data buffer
    if (dataBuffer) {
        free(dataBuffer);
        dataBuffer = nil;
    }
    
    NSLog(@"[AI] close");
}

- (void)dealloc {
	
	[self stop];
	
	// Close the codec
    if (audioEncoder) {
        
        [audioEncoder release];
        audioEncoder = nil;
    }
	
	// free data buffer
    if (dataBuffer) {
        free(dataBuffer);
    }
	
    NSLog(@"[AI] release");
	[super dealloc];
}

@end

static void CASoundAQInputCallback(
								   void *								inUserData,
								   AudioQueueRef						inAQ,
								   AudioQueueBufferRef					inBuffer,
								   const AudioTimeStamp *				inStartTime,
								   UInt32								inNumPackets,
								   const AudioStreamPacketDescription*	inPacketDesc)
{
	//NSLog(@"Input bytes: %d", inBuffer->mAudioDataByteSize);
	
	AudioInput* audioInput = (AudioInput*)inUserData;
	
	if (inBuffer->mAudioDataByteSize) {
		
		// write data to buffer
		[audioInput writeData:inBuffer->mAudioDataByteSize buffer:inBuffer->mAudioData];
		
		// process the data, only work when a block complete
		[audioInput encodeAndPostProcess];
	}
    
	// re-enqueue the buffe so that it gets filled again
	AudioQueueEnqueueBuffer(inAQ, inBuffer, 0, NULL);
}
