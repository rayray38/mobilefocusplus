//
//  G723Codec.h
//  SEONViewer
//
//  Created by James Lee on 12/11/23.
//  Copyright (c) 2012年 SEON. All rights reserved.
//

#import <Foundation/Foundation.h>

#define _AUDIO_LITTLE_ENDIAN
#define	AUDIO_ENCODING_LINEAR	(3)	/* PCM 2's-complement (0-center) */

struct g72x_state {
	long yl;	/* Locked or steady state step size multiplier. */
	short yu;	/* Unlocked or non-steady state step size multiplier. */
	short dms;	/* Short term energy estimate. */
	short dml;	/* Long term energy estimate. */
	short ap;	/* Linear weighting coefficient of 'yl' and 'yu'. */
    
	short a[2];	/* Coefficients of pole portion of prediction filter. */
	short b[6];	/* Coefficients of zero portion of prediction filter. */
	short pk[2];	/*
                     * Signs of previous two samples of a partially
                     * reconstructed signal.
                     */
	short dq[6];	/*
                     * Previous 6 samples of the quantized difference
                     * signal represented in an internal floating point
                     * format.
                     */
	short sr[2];	/*
                     * Previous 2 samples of the quantized difference
                     * signal represented in an internal floating point
                     * format.
                     */
	char td;	/* delayed tone detect, new in 1988 version */
};

@interface G723Codec : NSObject
{
    struct g72x_state m_encState;
	struct g72x_state m_decState;
}

- (id)initWithG723Codec;
- (int)EncodeData:(short *)bufOrgData by:(int)lenOrgData to:(Byte *)bufEncCode with:(int)lenEncData;
- (BOOL)DecodeData:(Byte *)bufEncCode by:(int)lenEncCode to:(short *)bufDecData with:(int)lenDecData;

@end
