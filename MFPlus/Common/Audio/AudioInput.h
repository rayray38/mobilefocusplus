//
//  AudioInput.h
//  EFViewer
//
//  Created by James Lee on 2011/3/9.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <AudioToolbox/AudioToolbox.h>

#import "AudioCodec.h"

#include "libavformat/avformat.h"
#include "libswscale/swscale.h"

#define AQ_BUFFER_NUMBER_IN     10
#define AQ_BUFFER_SIZE_IN		1000

@interface AudioInput : NSObject <UIApplicationDelegate> {
    
	// codec
	AudioCodec* audioEncoder;
	
	// buffer
	int		blockAlign;
	int		frameSize;
	int		lpcmBufSize;
	void*	readPos;
	void*	writePos;
	void*	dataBuffer;
	int		readFlag;
	int		writeFlag;
	
	// AQ
	AudioStreamBasicDescription	audioDesc;
	AudioQueueRef				queue;
	AudioQueueBufferRef			aqbuf[AQ_BUFFER_NUMBER_IN];
	
	id		actionDelegate;
	SEL		postProcessAction;
}

- (id)initWithCodecId:(int)cid srate:(int)srate bps:(int)bps balign:(int)balign fsize:(int)fsize;
- (void)setPostProcessAction:(id)delegate action:(SEL)action;
- (BOOL)start;
- (void)stop;
- (void)close;

@property(nonatomic) NSInteger	codecId;
@end
