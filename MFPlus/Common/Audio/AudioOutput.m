//
//  AudioOutput.m
//  MobileFocus
//
//  Created by James Lee on 2011/3/4.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import "AudioOutput.h"
#import "libkern/OSAtomic.h"

static void CASoundAQOutputCallback(
									void *                  inUserData,
									AudioQueueRef           inAQ,
									AudioQueueBufferRef     inBuffer);

@interface AudioOutput (private)

// buffer
- (void)writeData:(long)writeNumBytes buffer:(void *)buffer;
- (int)readData:(int)needNumBytes buffer:(void *)buffer bytesFilled:(int)bytesFilled; // return actural output bytes number

// player
- (BOOL)prepareForPlay;
- (void)queue: (AudioQueueRef)inAQ buffer: (AudioQueueBufferRef)inBuffer;

@end

@implementation AudioOutput
{
    NSData *audioData;
}

@synthesize codecId,amFlag;


- (id)initWithCodecId:(int)cid srate:(int)srate bps:(int)bps balign:(int)balign fsize:(int)fsize channel:(int)channel{
	
	if (!(self=[super init])) return nil;
	
	// init audio codec
	audioDecoder = [[AudioCodec alloc] initDecoderWithCodecId:cid srate:srate bps:bps balign:balign fsize:fsize];
    
	// allocate output audio buffer
	pOutAudio = malloc(AVCODEC_MAX_AUDIO_FRAME_SIZE);
	
	// buffer for AQ
	lpcmBufSize = srate*2*3; // 3sec
	dalayLength = srate; // 0.5sec
	dataBuffer	= malloc(lpcmBufSize);
	memset(dataBuffer, 0, lpcmBufSize);
	readPos		= dataBuffer;
	writePos	= dataBuffer+dalayLength;
	readFlag	= 0;
	writeFlag	= 0;
	
	// player
	audioDesc.mSampleRate		= channel * srate;
	audioDesc.mFormatID			= kAudioFormatLinearPCM;
	audioDesc.mFormatFlags		= kAudioFormatFlagIsSignedInteger|kAudioFormatFlagIsPacked;
	audioDesc.mBytesPerPacket	= AUDIO_BPS;
	audioDesc.mFramesPerPacket	= 1;
	audioDesc.mBytesPerFrame	= AUDIO_BPS;
	audioDesc.mChannelsPerFrame	= 1;
	audioDesc.mBitsPerChannel	= AUDIO_BPS * AUDIO_BYTE;
	audioDesc.mReserved			= 0;

	queue = nil;

	
	isDecoding = NO;
	isStop = YES;
    
    codecId = cid;
    audioData = nil;
	
	return self;
}

- (id)initWithG723Codec
{
    if (!(self=[super init])) return nil;
    
    if (g723Decoder) {
        [g723Decoder release];
        g723Decoder = nil;
    }
    
    //init G723 Codec
    g723Decoder = [[G723Codec alloc] initWithG723Codec];
    
    // allocate output audio buffer
	pOutAudio = malloc(AVCODEC_MAX_AUDIO_FRAME_SIZE);
	
	// buffer for AQ
    NSInteger srate = 8000;
	lpcmBufSize = srate*2*3; // 3sec
	dalayLength = srate; // 0.5sec
	dataBuffer	= malloc(lpcmBufSize);
	memset(dataBuffer, 0, lpcmBufSize);
	readPos		= dataBuffer;
	writePos	= dataBuffer+dalayLength;
	readFlag	= 0;
	writeFlag	= 0;
	
	// player
	audioDesc.mSampleRate		= srate;//+20;
	audioDesc.mFormatID			= kAudioFormatLinearPCM;
	audioDesc.mFormatFlags		= kAudioFormatFlagIsSignedInteger|kAudioFormatFlagIsPacked;
	audioDesc.mBytesPerPacket	= 2;
	audioDesc.mFramesPerPacket	= 1;
	audioDesc.mBytesPerFrame	= 2;
	audioDesc.mChannelsPerFrame	= 1;
	audioDesc.mBitsPerChannel	= 16;
	audioDesc.mReserved			= 0;
    
	queue = nil;
	
	isDecoding = NO;
	isStop = YES;
    
    codecId = AV_CODEC_ID_G723_1;
    audioData = nil;
    
    return self;
}


- (void)audioPlayingThread
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init]; // thread need an autorelease pool in it
    
    while (!isStop) {
        @synchronized(self)
        {
            if (audioData != nil && queue!=nil) {
                
                isDecoding = YES;
                
                // decode audio
                unsigned long outSize = 0;
                BOOL ret = NO;
                if (codecId == AV_CODEC_ID_G723_1) {
                    
                    outSize = [audioData length] *8 /3 *2;
                    //NSLog(@"[AO] G723 length:%d",outSize);
                    ret = [g723Decoder DecodeData:(Byte *)[audioData bytes] by:[audioData length] to:(short *)pOutAudio with:outSize];
                }else {
                    //NSLog(@"[AO] audioData length:%lu, outSize:%lu",(unsigned long)[audioData length],outSize);
                    ret = [audioDecoder decodeFrame:(Byte *)[audioData bytes] inSize:[audioData length] pOutAudio:pOutAudio pOutSize:&outSize];
                }
                
                if (ret) {
                    
                    // write to buffer
                    [self writeData:(int)outSize buffer:(void *)pOutAudio];
                    //NSLog(@"AudioSize:%d",outSize);
                }
                
                isDecoding = NO;
                [audioData release];
                audioData = nil;
            }
            else
                [NSThread sleepForTimeInterval:0.1];
        }
    }
    
    [pool drain];
}

- (void)playAudio:(Byte *)pInAudio length:(long)length {
    
    @synchronized(self)
    {
        if (audioData != nil)
            [audioData release];
        audioData = [[NSData alloc] initWithBytes:pInAudio length:length];
    }
	
	//[NSThread detachNewThreadSelector:@selector(decodeAndPutToBuffer:)
	//						 toTarget:self
	//					   withObject:audioData];
}

- (BOOL) start {

	if (queue!=nil) return YES;
		
	// create audio queue
	if ([self prepareForPlay]) {
		
		// audio start
		AudioQueueSetParameter(queue, kAudioQueueParam_Volume, 1.0f);
		OSStatus error = AudioQueueStart(queue, NULL);
		if (error)
		{
			NSLog(@"[AO] AudioQueueStart: OS error %li\n", (long)error);
			
			[self stop];
			return NO;
		}
        
        isStop = NO;
        
        [NSThread detachNewThreadSelector:@selector(audioPlayingThread) toTarget:self withObject:nil];
		
		NSLog(@"[AO] Audio output start.");
		return YES;
	}
	else {
		[self stop];
		return NO;
	}
}

- (void)stop {
	
	if (queue==nil) return;
		
	isStop = YES;
	
	AudioQueueStop(queue, true);
	
	// dispose audio queue
	AudioQueueDispose(queue, true);
	queue = nil;
	
	while ( isDecoding ) {
		
		[NSThread sleepForTimeInterval:0.001];
	}
	
	NSLog(@"[AO] Audio output stop.");
}

- (void)close       //without release self, keep the handle&mem until detailView release
{
    [self stop];
    
    // Close the codec
    if (audioDecoder) {
        [audioDecoder release];
        audioDecoder = nil;
    }
    if (g723Decoder) {
        [g723Decoder release];
        g723Decoder = nil;
    }
    // free data buffer
    if (dataBuffer) {
        free(dataBuffer);
        dataBuffer = nil;
    }
    
    NSLog(@"[AO] Close");
}

- (AVCodecContext *)getCodecCtx
{
    if (!audioDecoder)
        return nil;
    
    return [audioDecoder getCodecCtx];
}

- (void)dealloc {
	
	[self stop];
	
	// Close the codec
    if (audioDecoder) {
        [audioDecoder release];
        audioDecoder = nil;
    }
    
    if (g723Decoder) {
        [g723Decoder release];
        g723Decoder = nil;
    }
	
	// free output buffer
	free(pOutAudio);
	
	// free data buffer
	free(dataBuffer);		
	
	[super dealloc];
}

- (void)writeData:(long)writeNumBytes buffer:(void *)buffer {
	
	long bytesToFill = writeNumBytes;
	void* fillPtr = buffer;
	int bytesFilled = 0;
	
	while (true) {
		
		long ioNumBytes = 0;
		long bufRemnantSize = dataBuffer+lpcmBufSize-writePos;
		
		if(bytesToFill<bufRemnantSize) {
			ioNumBytes = bytesToFill;
		}
		else {
			ioNumBytes = bufRemnantSize;
		}

		memcpy(writePos, fillPtr, ioNumBytes);
		
		writePos += ioNumBytes;
		
		if (writePos>=(dataBuffer+lpcmBufSize)) {
		
			writePos = dataBuffer;
			writeFlag++;
			//NSLog(@"=========================write flag=%d",writeFlag);
		}
		
		if (writeFlag>readFlag && writePos>=readPos) {
			
			readFlag = writeFlag;
			readPos = ((writePos-dalayLength)<dataBuffer)?dataBuffer:(writePos-dalayLength);
			NSLog(@"[AO] Skip audio for 1 buffer length");
		}
		
		fillPtr += ioNumBytes;
		bytesFilled += ioNumBytes;
		bytesToFill -= ioNumBytes;
		
		if (bytesToFill == 0) {
			break;
		}
	}
}

- (int)readData:(int)needNumBytes buffer:(void *)buffer bytesFilled:(int)bytesFilled{
	
	int bytesToFill = needNumBytes;
	void* fillPtr = buffer;
	//int bytesFilled = 0;
	
	while (true) {
		
		long ioNumBytes = 0;
		long bufAvailanleSize = 0;
		
		if(writeFlag>readFlag) {
			bufAvailanleSize = dataBuffer+lpcmBufSize-readPos;
		}
		else if (writePos>readPos) {
			bufAvailanleSize = writePos-readPos;
		}
		
		if (bufAvailanleSize>=bytesToFill) {
			ioNumBytes = bytesToFill;
		}
		else {
			ioNumBytes = bufAvailanleSize;
		}

		
		if (ioNumBytes) {

			memcpy(fillPtr, readPos, ioNumBytes);
			
			readPos += ioNumBytes;
			if (readPos>=(dataBuffer+lpcmBufSize)) {
				
				readPos = dataBuffer;
				readFlag++;
				//NSLog(@"=========================read flag=%d",readFlag);				
			}
			
			fillPtr += ioNumBytes;
			bytesFilled += ioNumBytes;
			bytesToFill -= ioNumBytes;
            
			if (bytesToFill == 0) {
				break;
			}
		}
		else { 
			if (isStop) {
				break;
			}
			else {
				// no data to read, fill null data to buffer
				NSLog(@"[AO] Audio buffering...");
				void* tmp = malloc(dalayLength);
				memset(tmp, 0, dalayLength);
				[self writeData:dalayLength buffer:tmp];
				free(tmp);
			}
		}
	}
    
	return bytesFilled;
}

- (BOOL)prepareForPlay
{
	OSStatus error;
	
	// create a new audio queue output
	error = AudioQueueNewOutput(&audioDesc, CASoundAQOutputCallback, self, NULL, NULL, 0, &queue);
	if (error)
	{
		NSLog(@"[AO] AudioQueueNewOutput: OS error %li\n", (long)error);
		return NO;
	}
	
	for (UInt32 i = 0; i < AQ_BUFFER_NUMBER; ++i) {
		
		error = AudioQueueAllocateBuffer(queue, AQ_BUFFER_SIZE, &aqbuf[i]);
		if (error)
		{
			NSLog(@"[AO] AudioQueueAllocateBuffer: OS error %li\n", (long)error);
			return NO;
		}
		
		CASoundAQOutputCallback(self, queue, aqbuf[i]);
	}
	
	return YES;
}

- (void)queue: (AudioQueueRef)inAQ buffer: (AudioQueueBufferRef)inBuffer
{	
	//if (isStop) return;
	
	UInt32 bytesToFill = inBuffer->mAudioDataBytesCapacity;
	UInt8* fillPtr = (UInt8*)inBuffer->mAudioData;
	UInt32 bytesFilled = 0;
	
	// get a data from array
    bytesFilled = [self readData:bytesToFill buffer:fillPtr bytesFilled:bytesFilled];

    inBuffer->mAudioDataByteSize = bytesFilled;
	
	//if (bytesFilled) {
		AudioQueueEnqueueBuffer(inAQ, inBuffer, 0, NULL);
	//}
	
	//NSLog(@"Audio queue need: %d get: %d", bytesToFill, bytesFilled);
}

- (void)mute {
	AudioQueueSetParameter(queue, kAudioQueueParam_Volume, 0.0);
}

@end

static void CASoundAQOutputCallback(
									void *                  inUserData,
									AudioQueueRef           inAQ,
									AudioQueueBufferRef     inBuffer)
{
	//NSLog(@"callback: %x", inBuffer);
	AudioOutput* sound = (AudioOutput*)inUserData;
	[sound queue:inAQ buffer:inBuffer];
}
