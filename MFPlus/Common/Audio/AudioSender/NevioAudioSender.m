//
//  NevioAudioSender.m
//  EFViewer
//
//  Created by James Lee on 2011/5/18.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import "NevioAudioSender.h"


@implementation NevioAudioSender

#pragma mark -
#pragma mark Send Audio Data

- (void)start {
	
	if ( (sock = tcp_connect(szAddr, port)) < 0 )
	{
		return;
	}
    
    NSString *strAuth = [NSString stringWithFormat:@"%s:%s",szUser,szPasswd];
    NSString *strAuth64 = [Encryptor base64:strAuth];
	
	char szHeader[1024];
	char *szRequestMethod;
	char *szContenType; 
	char szQueryString[256];
	
	// Nevio protocal
	szRequestMethod = "POST";
	sprintf(szQueryString, "/cgi-bin/cgiStream?sessionid=%s&video=7&data=", szSid); 
	szContenType = "everfocus/audioin";
	
	sprintf(szHeader, 
			"%s %s HTTP/1.1\r\n"
			"Content-Type: %s\r\n"
			"Content-Length: 15436800\r\n"	
			"User-Agent: MobileFocus\r\n"
			"Host: %s\r\n"
			"Authorization: Basic %s\r\n\r\n"
			, szRequestMethod
			, szQueryString
			, szContenType		
			, szAddr
			, strAuth64.UTF8String);
	
	NSLog(@"[NEVIO] Socket send: %s", szHeader);
	
	if( send( sock, szHeader, strlen(szHeader), 0 ) < 0 )
	{
		NSLog(@"[NEVIO] send header failed!");
		return;
	}
	
	isConnect = YES;
	
	NSLog(@"[NEVIO] Audio sender start");
}

- (void)stop {
	
	if (!isConnect) return;
	
	sequence = 0;
	SAFE_CLOSE(sock);
	
	isConnect = NO;
	
	NSLog(@"[NEVIO] Audio sender stop");
}

- (void)sendData:(NSData*)data {
	
	if (!isConnect) return;
	
	sequence++;
	
	// set audio data
	UInt8 *audioPkt = malloc(12+[data length]);
	
	UInt32* pMagic		= (UInt32*)&(audioPkt[0]);
	UInt16* pSeq		= (UInt16*)&(audioPkt[4]);
	UInt16* pLength		= (UInt16*)&(audioPkt[6]);
	UInt8*  pPayload	= (UInt8*)&(audioPkt[8]);
	UInt32* pTail		= (UInt32*)&(audioPkt[8+[data length]]);
	
	*pMagic		= ntohl(0XA74F80E6);
	*pSeq		= sequence;
	*pLength	= [data length];
	memcpy(pPayload, [data bytes], [data length]);
	*pTail		= ntohl(0XABCD8765);
	
	if( send( sock, audioPkt, 12+[data length], 0 ) < 0 )
	{
		NSLog(@"[NEVIO] send audio data failed!");
		[self stop];
	}
	
	free(audioPkt);
	[data release];
}

@end
