//
//  AudioSender.m
//  EFViewer
//
//  Created by James Lee on 2011/3/13.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import "AudioSender.h"

@implementation AudioSender

@synthesize blnSupportNCB,auFlag,auChannel;

#pragma mark -
#pragma mark Initialization Command

- (id)initWithHost:(NSString *)host user:(NSString*)user passwd:(NSString*)passwd sid:(NSString *)sid
{
	if (self = [super init]) {
		
		NSArray *items = [host componentsSeparatedByString:@":"];
		[[items objectAtIndex:0] getCString:szAddr maxLength:sizeof(szAddr) encoding:NSASCIIStringEncoding];
        port = [[items objectAtIndex:1] intValue];
		
		[user getCString:szUser maxLength:sizeof(szUser) encoding:NSASCIIStringEncoding];
		[passwd getCString:szPasswd maxLength:sizeof(szPasswd) encoding:NSASCIIStringEncoding];
		[sid getCString:szSid maxLength:sizeof(szSid) encoding:NSASCIIStringEncoding];
		
		sequence = 0;
		isConnect = NO;
        blnSupportNCB = NO;
	}
	
	return self;
}

- (void)dealloc {
	
	[self stop];
	[super dealloc];
}

#pragma mark -
#pragma mark Send Audio Data

- (void)start {
	
	// no-op
}

- (void)stop {
	
	// no-op
}

- (void)sendData:(NSData*)data {
	
	// no-op
}

///// socket /////
#pragma mark -
#pragma mark Socket

int tcp_connect(char *ip, unsigned short port)
{
	int sock;
	struct sockaddr_in socketAddr;
	//int nTimeout = 15;
	
	if ( (sock = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
	{
		NSLog(@"[AS] TCP open socket failed.");
		return -1;
	}
    
	bzero(&socketAddr, sizeof(struct sockaddr_in));
	socketAddr.sin_family = AF_INET;
	socketAddr.sin_port = htons(port);
	socketAddr.sin_addr.s_addr = inet_addr(ip);
	
	if (connect(sock, (struct sockaddr*)&socketAddr, sizeof(struct sockaddr_in)) < 0)
	{
		NSLog(@"[AS] TCP connect failed.");
		SAFE_CLOSE(sock);
		return -1;
	}
	
    fd_set          fdwrite;
    struct timeval  tvSelect;
    
    FD_ZERO(&fdwrite);
    
    FD_SET(sock, &fdwrite);
    
    tvSelect.tv_sec = 2;
    
    tvSelect.tv_usec = 0;
    
    int retval = select(sock + 1,NULL, &fdwrite, NULL, &tvSelect);
    
    if(retval < 0) {
        
        if ( errno == EINTR ) {
            
            NSLog(@"[AS] Socket Select Error");
        } else {
            
            NSLog(@"[AS] Error");
            close(sock);
        }
    } else if(retval == 0) {
        
        NSLog(@"[AS] Socket Select Timeout");
    }
    
    signal(SIGPIPE, SIG_IGN);
	return sock;
}

///// socket end /////

@end
