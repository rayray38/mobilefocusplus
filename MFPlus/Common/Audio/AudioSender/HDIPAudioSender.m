//
//  HDIPAudioSender.m
//  EFViewer
//
//  Created by James Lee on 2011/5/18.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import "HDIPAudioSender.h"


@implementation HDIPAudioSender

#pragma mark -
#pragma mark Send Audio Data

- (void)start {
	
	if ( (sock = tcp_connect(szAddr, port)) < 0 )
	{
		return;
	}
    
    NSString *strAuth = [NSString stringWithFormat:@"%s:%s",szUser,szPasswd];
    NSString *strAuth64 = [Encryptor base64:strAuth];
    
	char szHeader[1024];
	char *szRequestMethod;
	char *szContenType; 
	char szQueryString[256];
	
	// Nevio protocal
	szRequestMethod = "PUT";
	sprintf(szQueryString, "/PSIA/Custom/EverFocus/uploadAudio"); 
	szContenType = "application/octet-stream";
	
	sprintf(szHeader, 
			"%s %s HTTP/1.1\r\n"
			"Content-Type: %s\r\n"
			"Content-Length: 15436800\r\n"	
			"User-Agent: MobileFocus\r\n"
			"Host: %s\r\n"
			"Authorization: Basic %s\r\n\r\n"
			, szRequestMethod
			, szQueryString
			, szContenType		
			, szAddr
			, strAuth64.UTF8String);
	
	NSLog(@"[HDIP] Socket send: %s", szHeader);
	
	if( send( sock, szHeader, strlen(szHeader), 0 ) < 0 )
	{
		NSLog(@"[HDIP] send header failed!");
		return;
	}
	
	isConnect = YES;
	
	NSLog(@"[HDIP] Audio sender start");
}

- (void)stop {
	
	if (!isConnect) return;
	
	sequence = 0;
	SAFE_CLOSE(sock);
	
	isConnect = NO;
	
	NSLog(@"[HDIP] Audio sender stop");
}

- (void)sendData:(NSData*)data {
	
	if (!isConnect) return;
	
	// set audio data	
	if( send( sock, [data bytes], [data length], 0 ) < 0 )
	{
		NSLog(@"[HDIP] send audio data failed!");
		[self stop];
	}
	
	[data release];
}

@end
