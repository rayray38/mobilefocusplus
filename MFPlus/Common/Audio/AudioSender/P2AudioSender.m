//
//  P2AudioSender.m
//  EFViewerHD
//
//  Created by James Lee on 12/11/7.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import "P2AudioSender.h"

@implementation P2AudioSender

#pragma mark -
#pragma mark Send Audio Data

- (void)start {
    
	char szHeader[1024];
	char *szRequestMethod;
	char szQueryString[256];
	
    // Create audio stream socket
    if ( (sock = tcp_connect(szAddr, port)) < 0 )
    {
        return;
    }
    
	// P2 protocal
	szRequestMethod = "POST";
    
    if (self.blnSupportNCB) {
        
        // Ask for Digest information
        if ( (sock401 = tcp_connect(szAddr, port)) < 0 )
        {
            return;
        }
        sprintf(szQueryString, "/cgi-bin/Audio.cgi");
        sprintf(szHeader,
                "%s %s HTTP/1.1\r\n"
                "Accept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, */*\r\n"
                "Accept-Language: zh-tw\r\n"
                "Accept-Encoding: gzip, deflate\r\n"
                "If-Modified-Since: Sun, 21 Sep 2008 17:07:28 GMT\r\n"
                "User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; .NET CLR 2.0.50727; .NET CLR 1.1.4322)\r\n"
                "Host: %s\r\n"
                "Connection: Keep-Alive\r\n\r\n"
                , szRequestMethod
                , szQueryString
                , szAddr);
        
        NSLog(@"[AS] Socket send(%zd): %s",strlen(szHeader), szHeader);
        if( send( sock401, szHeader, strlen(szHeader), 0 ) < 0 )
        {
            NSLog(@"[AS] send header failed!");
            SAFE_CLOSE(sock401);
            return;
        }
        
        char recvBuf[512] = {0};
        while (recv(sock401, recvBuf, sizeof(recvBuf), 0) < 0) {
        }
        
        NSLog(@"[AS] Socket recV:%@",[NSString stringWithUTF8String:recvBuf]);
        [self parseAuthenticate:[NSString stringWithUTF8String:recvBuf]];
        
        //MD5
        char cpCnonce[] = "0f1ed110bbfc627214c29d2046bcb83d";
        char cpNc[] = "00000001";
        char *szContenType = "application/octet-stream";
        
        NSString *strTmp1 = [Encryptor md5:[NSString stringWithFormat:@"%s:%s:%s",szUser,szRealM,szPasswd]];
        NSString *strTmp2 = [Encryptor md5:[NSString stringWithFormat:@"%s:%s",szRequestMethod,szQueryString]];
        NSString *strReturn = [Encryptor md5:[NSString stringWithFormat:@"%@:%s:%s:%s:auth:%@",strTmp1,szNonce,cpNc,cpCnonce,strTmp2]];
        
        sprintf(szHeader,
                "%s %s HTTP/1.1\r\n"
                "Content-Type: %s\r\n"
                "Content-Length: 15436800\r\n"
                "User-Agent: AudioBroadcast\r\n"
                "Host: %s\r\n"
                "Cache-Control: no-cache\r\n"
                "Authorization: Digest username=\"%s\",realm=\"%s\",nonce=\"%s\",uri=\"/cgi-bin/Audio.cgi\",cnonce=\"%s\",nc=%s,response=\"%s\",qop=\"auth\"\r\n\r\n"
                , szRequestMethod
                , szQueryString
                , szContenType
                , szAddr
                , szUser
                , szRealM
                , szNonce
                , cpCnonce
                , cpNc
                , strReturn.UTF8String);
    }else {
        
        sprintf(szQueryString, "/Audio.cgi");
        sprintf(szHeader,
                "%s %s HTTP/1.1\r\n"
                "Accept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, */*\r\n"
                "Accept-Language: zh-tw\r\n"
                "Accept-Encoding: gzip, deflate\r\n"
                "If-Modified-Since: Sun, 21 Sep 2008 17:07:28 GMT\r\n"
                "User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; .NET CLR 2.0.50727; .NET CLR 1.1.4322)\r\n"
                "Host: %s\r\n"
                "Connection: Keep-Alive\r\n\r\n"
                , szRequestMethod
                , szQueryString
                , szAddr);
    }
	
    // Send Streaming Header
	NSLog(@"[AS] Socket send(%zd): %s",strlen(szHeader), szHeader);
	
	if( send( sock, szHeader, strlen(szHeader), 0 ) < 0 )
	{
		NSLog(@"[AS] send header failed!");
		return;
	}
    
    SAFE_CLOSE(sock401);
	
	isConnect = YES;
	
	NSLog(@"[AS] Audio sender start");
}

- (void)stop {
	
	if (!isConnect) return;
	
	sequence = 0;
	SAFE_CLOSE(sock);
	
	isConnect = NO;
	
	NSLog(@"[P2] Audio sender stop");
}

- (void)sendData:(NSData*)data {
	
	if (!isConnect) return;
    
	// set audio data
	UInt8 *audioPkt = malloc(TOTAL_HEADER_SIZE+[data length]);
    
    // set header
    P2_PACKET_HEADER packetHeader;
    P2_AM_HEADER     amHeader;
    unsigned char    hisiHeader[4];
    memset(&packetHeader, 0, sizeof(P2_PACKET_HEADER));
    memset(&amHeader, 0, sizeof(P2_AM_HEADER));
    packetHeader.audio_len = htons([data length]+4+sizeof(P2_AM_HEADER));
    amHeader.ch = self.auChannel;
    amHeader.flag = self.auFlag;
    hisiHeader[0] = 0;
    hisiHeader[1] = 1;
    hisiHeader[2] = [data length]/2;
    hisiHeader[3] = 0;
    memcpy(audioPkt, &packetHeader, sizeof(P2_PACKET_HEADER));
    memcpy(audioPkt+sizeof(P2_PACKET_HEADER), &amHeader, sizeof(P2_AM_HEADER));
    memcpy(audioPkt+sizeof(P2_PACKET_HEADER)+sizeof(P2_AM_HEADER), &hisiHeader, sizeof(HISI_AU_HEADER));
    memcpy(audioPkt+sizeof(P2_PACKET_HEADER)+sizeof(P2_AM_HEADER)+sizeof(hisiHeader), [data bytes], [data length]);
    
//    NSMutableString * result = [[NSMutableString alloc] init];
//    size_t len = strlen((unsigned char)audioPkt);
//    NSLog(@"%zu",len);
//    for (int i=0; i < len; i++) {
//        [result appendString:[NSString stringWithFormat:@"%02x",audioPkt[i]]];
//    }
//    
//    NSLog(@"[p2] audiopkt:%@",result);
    
    if (!sock) {
        free(audioPkt);
        [data release];
        return;
    }
    
	if( send( sock, audioPkt, TOTAL_HEADER_SIZE+[data length], 0 ) < 0 ){
		NSLog(@"[P2] send audio data failed!");
        
		[self stop];
	}
	
	free(audioPkt);
	[data release];
}

- (void)parseAuthenticate:(NSString *)authPkg
{
    NSArray *lines = [authPkg componentsSeparatedByString:@"\r\n"];
    NSString  *lineString, *echoString=nil;
    NSEnumerator *nse = [lines objectEnumerator];
    
    //Get line of WWW-Authenticate
    NSString  *tmp = [NSString stringWithFormat:@"WWW-Authenticate: "];
    while (lineString = [nse nextObject]) {
        NSScanner *scanner = [NSScanner scannerWithString:lineString];
        [scanner scanUpToString:tmp intoString:nil];
        [scanner scanString:tmp intoString:nil];
        [scanner scanUpToString:@" Digest " intoString:&echoString];
        if (echoString)
            break;
    }
    
    //Get value of realm & nonce
    NSArray *objs = [echoString componentsSeparatedByString:@", "];
    for (NSString *string in objs) {
        
        NSRange range1=[string rangeOfString:@"="];
        NSRange range2=[string rangeOfString:@"realm"];
        NSRange range3=[string rangeOfString:@"nonce"];
        
        string =[string substringFromIndex:range1.location+2];
        
        NSUInteger length=[string length];
        
        string=[string substringToIndex:length-1];
        
        if(range2.location!=NSNotFound) {
            [string getCString:szRealM maxLength:sizeof(szRealM) encoding:NSASCIIStringEncoding];
        }else if(range3.location!=NSNotFound){
            [string getCString:szNonce maxLength:sizeof(szNonce) encoding:NSASCIIStringEncoding];
        }
    }
    
    NSLog(@"[AS] realm:%s, nonce:%s",szRealM,szNonce);
}

@end
