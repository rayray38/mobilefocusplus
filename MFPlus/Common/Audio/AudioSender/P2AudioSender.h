//
//  P2AudioSender.h
//  EFViewerHD
//
//  Created by James Lee on 12/11/7.
//  Copyright (c) 2012年 EF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AudioSender.h"
#import "P2Net.h"

#define TOTAL_HEADER_SIZE   16

@interface P2AudioSender : AudioSender {
    
    int     sock401;
}

- (void)parseAuthenticate:(NSString *)authPkg;

@end
