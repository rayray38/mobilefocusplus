//
//  GTAudioSender.m
//  EFViewerHD
//
//  Created by James Lee on 13/3/21.
//  Copyright (c) 2013年 EF. All rights reserved.
//

#import "GTAudioSender.h"

@implementation GTAudioSender

#pragma mark -
#pragma mark Send Audio Data

- (void)start {
	
	if ( (sock = tcp_connect(szAddr, port)) < 0 )
	{
		return;
	}
    
    NSString *strAuth = [NSString stringWithFormat:@"%s:%s",szUser,szPasswd];
    NSString *strAuth64 = [Encryptor base64:strAuth];
	
	char szHeader[1024];
	char *szRequestMethod;
	char *szContenType;
	char szQueryString[256];
	
	// Nevio protocal
	szRequestMethod = "POST";
	sprintf(szQueryString, "/cgi-bin/instream.cgi");
	szContenType = "audio/basic";
	
	sprintf(szHeader,
			"%s %s HTTP/1.1\r\n"
			"Content-Type: %s\r\n"
            "Cache-Control: no-cache\r\n"
			"User-Agent: Mozilla/4.0 (compatible; )\r\n"
			"Host: %s\r\n"
			"Content-Length: 30000000\r\n"
            "Connection: Keep-Alive\r\n"
			"Authorization: Basic %s\r\n\r\n"
			, szRequestMethod
			, szQueryString
			, szContenType
			, szAddr
			, strAuth64.UTF8String);
	
	NSLog(@"[GT] Socket send: %s", szHeader);
	
	if( send( sock, szHeader, strlen(szHeader), 0 ) < 0 )
	{
		NSLog(@"[GT] send header failed!");
		return;
	}
	
	isConnect = YES;
	
	NSLog(@"[GT] Audio sender start");
}

- (void)stop {
	
	if (!isConnect) return;
	
	sequence = 0;
	SAFE_CLOSE(sock);
	
	isConnect = NO;
	
	NSLog(@"[GT] Audio sender stop");
}

- (void)sendData:(NSData*)data {
	
	if (!isConnect) return;
	
	sequence++;
	
	// set audio data
	UInt8 *audioPkt = malloc([data length]);
    
	memcpy(audioPkt, [data bytes], [data length]);
	
	if( send( sock, audioPkt, [data length], 0 ) < 0 )
	{
		NSLog(@"[GT] send audio data failed!");
		[self stop];
	}
	
	free(audioPkt);
	[data release];
}
@end
