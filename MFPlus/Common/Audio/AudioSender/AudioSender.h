//
//  AudioSender.h
//  EFViewer
//
//  Created by James Lee on 2011/3/13.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#import "Encrypt.h"

#define SAFE_CLOSE(x) { if (x) shutdown(x, 2); x = 0; }

@interface AudioSender : NSObject <NSStreamDelegate> {
    
	char	szAddr[64];
	int		port;
	char	szUser[128];
	char	szPasswd[128];
	char	szSid[128]; // Nevio
    char    szRealM[128];
    char    szNonce[128];
	BOOL	isConnect;
	
	int		sock;
	int		sequence; // Nevio
}

// initiate
- (id)initWithHost:(NSString *)host user:(NSString*)user passwd:(NSString*)passwd sid:(NSString *)sid;

// send audio data
- (void)start;
- (void)stop;
- (void)sendData:(NSData*)data; // Nevio:256 HDIP:240/320

// socket
int tcp_connect(char *ip, unsigned short port);

@property(nonatomic)    BOOL        blnSupportNCB;
@property(nonatomic)    NSInteger   auFlag;
@property(nonatomic)    NSInteger   auChannel;

@end
