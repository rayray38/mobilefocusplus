//
//  AudioCodec.m
//  EFViewer
//
//  Created by James Lee on 2011/5/13.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import "AudioCodec.h"

@implementation AudioCodec

@synthesize codecId;

- (id)initDecoderWithCodecId:(int)cid srate:(int)srate bps:(int)bps balign:(int)balign fsize:(int)fsize{
	
	if (!(self=[super init])) return nil;
	
	AVCodec *pCodec;
	
	// Register all formats and codecs
	av_register_all();
    
    
	// Find the decoder
	pCodec = avcodec_find_decoder(cid);
	if(pCodec==NULL)
		goto initError; // Codec not found
	
	// Allocate codec context
	pCodecCtx = avcodec_alloc_context3(pCodec);
	pCodecCtx->sample_fmt	= AV_SAMPLE_FMT_S16;
	pCodecCtx->sample_rate	= srate;
	pCodecCtx->bits_per_coded_sample = bps;
	pCodecCtx->block_align	= balign;
	pCodecCtx->channels		= 1;
	pCodecCtx->frame_size	= fsize;
    
	
	// Open codec
	if(avcodec_open2(pCodecCtx, pCodec, nil)<0)
		goto initError; // Could not open codec
    
	codecId = pCodecCtx->codec_id;
	
	return self;
	
initError:
	[self release];
	return nil;
}

- (id)initEncoderWithCodecId:(int)cid srate:(int)srate bps:(int)bps balign:(int)balign fsize:(int)fsize; {
	
	// initial codec
	
	AVCodec *pCodec;
	
	// Register all formats and codecs
	av_register_all();
    
	// Find the encoder
	pCodec = avcodec_find_encoder(cid);
	if(pCodec==NULL)
		goto initError; // Codec not found
	
	// Allocate codec context
	pCodecCtx = avcodec_alloc_context3(pCodec);
    
	// it would be over write by ffmpeg, so we set them again later
	pCodecCtx->sample_fmt	= AV_SAMPLE_FMT_S16;
	pCodecCtx->sample_rate	= srate;
	pCodecCtx->bits_per_coded_sample = bps;
	pCodecCtx->block_align	= balign;
	pCodecCtx->channels		= 1;
	pCodecCtx->frame_size	= fsize;
	
	
	// Open codec
	if(avcodec_open2(pCodecCtx, pCodec,nil)<0)
		goto initError; // Could not open codec
	
	// set codec parameters again
	pCodecCtx->sample_fmt	= AV_SAMPLE_FMT_S16;
	pCodecCtx->sample_rate	= srate;
	pCodecCtx->bits_per_coded_sample = bps;
	pCodecCtx->block_align	= balign;
	pCodecCtx->channels		= 1;
	pCodecCtx->frame_size	= fsize;
	
	codecId = pCodecCtx->codec_id;
	
	return self;
	
initError:
	[self release];
	return nil;
}

- (BOOL)decodeFrame:(Byte *)pInAudio inSize:(int)inSize pOutAudio:(Byte *)pOutAudio pOutSize:(int *)pOutSize {
    
	// decode audio
	av_init_packet(&avpkt);
    
	avpkt.size = inSize;
	avpkt.data = (uint8_t *)pInAudio;
	//avpkt.flags |= AV_PKT_FLAG_KEY;
    
    if (!decodeFrame) {
        //if(!(decodeFrame = avcodec_alloc_frame()))      return NO;     //avcodec_alloc_frame() is deprecated
        if(!(decodeFrame = av_frame_alloc()))      return NO;
            
    }else {
        //avcodec_get_frame_defaults(decodeFrame);        // avcodec_get_frame_defaults is deprecated
        av_frame_unref(decodeFrame);
    }
    
    *pOutSize = AVCODEC_MAX_AUDIO_FRAME_SIZE;
    //NSLog(@"[AU] OutSize:%d",*pOutSize);
    int used = avcodec_decode_audio4(pCodecCtx, decodeFrame, pOutSize, &avpkt);
    
    if (used < 0) {
        NSLog(@"[AU] Decode failed");
        return NO;
    }
    if (pOutSize) {
        int dataSize = av_samples_get_buffer_size(NULL, pCodecCtx->channels, decodeFrame->nb_samples, pCodecCtx->sample_fmt, 1);
        if (dataSize > 0) {
            memcpy(pOutAudio,decodeFrame->data[0],dataSize);
            *pOutSize = dataSize;
            //NSLog(@"[AU] used:%d, OutSize:%d",used,*pOutSize);
        }
    }
    
    av_free_packet(&avpkt);
    
	return (used>0);
}

- (BOOL)encodeFrame:(Byte *)pInAudio pOutAudio:(Byte *)pOutAudio bufSize:(int)bufSize
{
    //int got_frame = 0;
    int outsize = avcodec_encode_audio(pCodecCtx, pOutAudio, bufSize, (const short*)pInAudio);          //avcodec_encode_audio is deprecated
//    int outsize = avcodec_encode_audio2(pCodecCtx, &avpkt, encodeFrame, 1);

    //NSLog(@"outSize:%d, bufSize:%d",outsize,bufSize);
    return (outsize>0);
}

- (void)dealloc {
	
	// Close the codec
	if (pCodecCtx) avcodec_close(pCodecCtx);
	
	[super dealloc];
}

- (AVCodecContext *)getCodecCtx
{
    return pCodecCtx;
}

@end
