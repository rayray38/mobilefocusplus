//
//  AudioCodec.h
//  EFViewer
//
//  Created by James Lee on 2011/5/13.
//  Copyright 2011 EverFocus. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "libavformat/avformat.h"

#define AVCODEC_MAX_AUDIO_FRAME_SIZE    1920000

@interface AudioCodec : NSObject {

	// codec
	NSInteger      codecId;
    AVPacket       avpkt;
	AVCodecContext *pCodecCtx;
    AVFrame        *decodeFrame;
    AVFrame        *encodeFrame;
}

- (id)initDecoderWithCodecId:(int)cid srate:(int)srate bps:(int)bps balign:(int)balign fsize:(int)fsize;
- (id)initEncoderWithCodecId:(int)cid srate:(int)srate bps:(int)bps balign:(int)balign fsize:(int)fsize;
- (BOOL)decodeFrame:(Byte *)pInAudio inSize:(int)inSize pOutAudio:(Byte *)pOutAudio pOutSize:(int *)pOutSize;
- (BOOL)encodeFrame:(Byte *)pInAudio pOutAudio:(Byte *)pOutAudio bufSize:(int)bufSize;
- (AVCodecContext *)getCodecCtx;

@property(nonatomic) NSInteger		codecId;

@end