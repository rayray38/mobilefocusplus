//
//  LiveViewController.m
//  EFViewer
//
//  Created by Nobel Hsu on 2010/5/24.
//  Copyright 2010 Everfocus. All rights reserved.
//

#import <QuartzCore/QuartzCore.h> // for anchor point
#import "LiveViewController.h"
#import "EFViewerAppDelegate.h"

#import "P2StreamReceiver.h"
#import "iCStreamReceiver.h"
#import "NVRStreamReceiver.h"
#import "EzAccessoryView.h"

@interface LiveViewController()
- (void)backToPreviousPage;
- (void)eventHandleProcess:(NSString *)msg;
@end

@implementation LiveViewController

@synthesize device,devArray,event;
@synthesize searchView;
@synthesize blnChSelector,blnEventMode,blnEventMode_pb,blnOpenSearch,blnWillOpenSearch,blnPlaybackMode;

#pragma mark - ViewController Management

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //NavigationBar Initiate
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backBtn.png"]
                                                                style:UIBarButtonItemStyleBordered
                                                               target:self
                                                               action:@selector(backToPreviousPage)];
    
    [self.navigationItem setLeftBarButtonItem:backBtn];
    SAVE_FREE(backBtn);
    
    navRightItems = [[NSArray alloc] initWithObjects:m_btnSnap, m_btnRecord, nil];
    self.navigationItem.rightBarButtonItems = navRightItems;
    
    //ControlBar Initiate
    CGRect selfFrame = self.view.bounds;
    CGRect barFrame = CGRectMake(0, selfFrame.size.height-TOOLBAR_HEIGHT_V, selfFrame.size.width, TOOLBAR_HEIGHT_V);
    [controlBar setFrame:barFrame];
    [controlBar setNeedsDisplay];
    [self.view addSubview:controlBar];
    
    dvrLiveItems = [[NSArray alloc] initWithObjects:m_btnCh, space, m_btnPtz, space, m_btnSound, space, m_btnSpeak, space, m_btnSearch,nil];
    dvrPbItems   = [[NSArray alloc] initWithObjects:m_btnCh, space, m_btnSound, space,m_btnPbControl, space, m_btnSearch, nil];
    
    //LayoutBar Initiate
    layoutItems = [[NSArray alloc] initWithObjects:btn1Div,btn4Div,btnSequence,prevBtn,nextBtn,textSpeed, nil];
    [self.view addSubview:layoutPanel];
    
    //PBControl Panel
    [self.view addSubview:pbControlView];
    [pbControlView.layer setCornerRadius:10.0];
    [pbControlView setHidden:YES];
    
    //PTZ Panel Initiate
    [self.view addSubview:ptzPanel];
    ptzCtrlBtn[0] = [self buttonWithTitle:@"Focus" target:self downAction:@selector(passPtzCmd:) upAction:nil fontSize:14 tag:PTZ_FOCUS];
    ptzCtrlBtn[1] = [self buttonWithTitle:@"Iris+" target:self downAction:@selector(passPtzCmd:) upAction:nil fontSize:14 tag:PTZ_IRISON];
    ptzCtrlBtn[2] = [self buttonWithTitle:@"Iris-" target:self downAction:@selector(passPtzCmd:) upAction:nil fontSize:14 tag:PTZ_IRISOFF];
    ptzCtrlBtn[3] = [self buttonWithTitle:@"Preset" target:self downAction:@selector(passPtzCmd:) upAction:nil fontSize:14 tag:PTZ_PRESET];
    ptzCtrlBtn[4] = [self buttonWithTitle:@"AutoPan" target:self downAction:@selector(passPtzCmd:) upAction:nil fontSize:14 tag:PTZ_AUTOPAN];
    ptzCtrlBtn[5] = [self buttonWithTitle:@"Pattern" target:self downAction:@selector(passPtzCmd:) upAction:nil fontSize:14 tag:PTZ_PATTERN];
    ptzCtrlBtn[6] = [self buttonWithTitle:@"Tour" target:self downAction:@selector(passPtzCmd:) upAction:nil fontSize:14 tag:PTZ_TOUR];
    ptzCtrlBtn[7] = [self buttonWithTitle:@"Menu" target:self downAction:@selector(passPtzCmd:) upAction:nil fontSize:14 tag:PTZ_OSD];
    
    for (NSInteger i=0; i<PTZ_BTN_NUMBER; i++) {
        [scrollPtzBar addSubview:ptzCtrlBtn[i]];
    }
    
    [self.view addSubview:presetBar];
    [presetBar setHidden:YES];
    
    UITapGestureRecognizer   *ptzTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnPTZ:)];
    UIPinchGestureRecognizer *ptzPinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchPTZ:)];
    UISwipeGestureRecognizer *ptzSwipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipePTZ:)];
    UISwipeGestureRecognizer *ptzSwipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipePTZ:)];
    UISwipeGestureRecognizer *ptzSwipeGestureUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipePTZ:)];
    UISwipeGestureRecognizer *ptzSwipeGestureDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipePTZ:)];
    [ptzSwipeGestureLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [ptzSwipeGestureRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [ptzSwipeGestureUp setDirection:UISwipeGestureRecognizerDirectionUp];
    [ptzSwipeGestureDown setDirection:UISwipeGestureRecognizerDirectionDown];
    [ptzTapGesture setDelegate:self];
    [ptzSwipeGestureLeft setDelegate:self];
    [ptzSwipeGestureRight setDelegate:self];
    [ptzSwipeGestureUp setDelegate:self];
    [ptzSwipeGestureDown setDelegate:self];
    [ptzTapGesture setDelegate:self];
    [ptzPanel addGestureRecognizer:ptzTapGesture];
    [ptzPanel addGestureRecognizer:ptzPinchGesture];
    [ptzPanel addGestureRecognizer:ptzSwipeGestureLeft];
    [ptzPanel addGestureRecognizer:ptzSwipeGestureRight];
    [ptzPanel addGestureRecognizer:ptzSwipeGestureUp];
    [ptzPanel addGestureRecognizer:ptzSwipeGestureDown];
    SAVE_FREE(ptzTapGesture);
    SAVE_FREE(ptzPinchGesture);
    SAVE_FREE(ptzSwipeGestureLeft);
    SAVE_FREE(ptzSwipeGestureRight);
    SAVE_FREE(ptzSwipeGestureUp);
    SAVE_FREE(ptzSwipeGestureDown);
    
    //RenderViews Initiate
    renderViews[0] = [[VideoDisplayView alloc] initWithView:renderView0];
    renderViews[1] = [[VideoDisplayView alloc] initWithView:renderView1];
    renderViews[2] = [[VideoDisplayView alloc] initWithView:renderView2];
    renderViews[3] = [[VideoDisplayView alloc] initWithView:renderView3];
    
    if (!renderArray)
        renderArray = [[NSMutableArray alloc] init];
    
    for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
        [renderViews[i] showView:YES];
        [renderViews[i].mainView.layer setBorderWidth:1.0f];
        [renderViews[i].mainView.layer setBorderColor:[[UIColor grayColor] CGColor]];
        [renderArray addObject:renderViews[i]];
        [renderViews[i] release];
        
        UITapGestureRecognizer *mainTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnMain:)];
        [mainTapGesture setNumberOfTapsRequired:1];
        renderViews[i].mainView.userInteractionEnabled = YES;
        [renderViews[i].mainView addGestureRecognizer:mainTapGesture];
        SAVE_FREE(mainTapGesture);
        
        UILongPressGestureRecognizer *mainLongGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressOnMain:)];
        [mainLongGesture setMinimumPressDuration:0.5f];
        [renderViews[i].mainView addGestureRecognizer:mainLongGesture];
        SAVE_FREE(mainLongGesture);
        
        UISwipeGestureRecognizer *mainSwipeLeftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeOnMain:)];
        UISwipeGestureRecognizer *mainSwipeRightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeOnMain:)];
        [mainSwipeLeftGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
        [mainSwipeRightGesture setDirection:UISwipeGestureRecognizerDirectionRight];
        [mainSwipeLeftGesture setDelegate:self];
        [mainSwipeRightGesture setDelegate:self];
        [mainSwipeLeftGesture setDelaysTouchesEnded:YES];
        [renderViews[i].mainView addGestureRecognizer:mainSwipeLeftGesture];
        [renderViews[i].mainView addGestureRecognizer:mainSwipeRightGesture];
        
        UIPinchGestureRecognizer *mainPinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchOnMain:)];
        UIPanGestureRecognizer *mainPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanOnMain:)];
        [mainPanGesture setMaximumNumberOfTouches:2];
        [mainPinchGesture setDelegate:self];
        [mainPanGesture setDelegate:self];
        [renderViews[i].mainView addGestureRecognizer:mainPinchGesture];
        [renderViews[i].mainView addGestureRecognizer:mainPanGesture];
        SAVE_FREE(mainPinchGesture);
        SAVE_FREE(mainPanGesture);
        SAVE_FREE(mainSwipeLeftGesture);
        SAVE_FREE(mainSwipeRightGesture);
    }
    
    if (!recordControl) {
        recordControl = [[RecordControl alloc] init];
    }
    
    if (!videoControl) {
        videoControl = [[VideoControl alloc] initWithVideoDisplayViews:renderArray];
        videoControl.recCtrl = recordControl;
    }
    videoControl.outputMask = 0;
    
    if (!audioControl) {
        audioControl = [AudioOutput alloc];
    }
    
    if (!speakControl) {
        speakControl = [AudioInput alloc];
    }
    
    //Channel Panel Initiate
    [self.view addSubview:devTable];
    [devTable setHidden:YES];
    
    [self.view addSubview:channelPanel];
    //Initial Channel Button Array
    for (NSInteger i=0; i<MAX_CH_NUMBER; i++) {
        channelBtn[i] = [self buttonWithTitle:[NSString stringWithFormat:@"%ld",i+1] target:self downAction:nil upAction:@selector(passChCmd:) fontSize:16 tag:i];
        [scrollChBar addSubview:channelBtn[i]];
    }
    
    if (!devArray)  devArray = [[NSMutableArray alloc] init];
    if (!playingIndexList)  playingIndexList = [[NSMutableArray alloc] init];
    
    ///// Audio session /////
	OSStatus error = AudioSessionInitialize(NULL, NULL, NULL, self);
    if (error) {
        NSLog(@"[LV] AudioSession Initial ERROR");
    }else {
        error = AudioSessionSetActive(TRUE);
        if (error) {
            NSLog(@"[LV] AudioSession Active FAILED");
        }
    }
    
    //EFMediaTool
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    mediaLibrary = appDelegate.mediaLibrary;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // get rotation information from delegate
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    blnFullScreen = appDelegate.blnFullScreen;
    if(blnFullScreen == NO)
        [self setPortraitLayout];
    else
        [self setLandscapeLayout];
    
    [controlBar setHidden:NO];
    
    //get smooth mode config
    blnSmoothMode = [EZAccessoryView getDetailSettingBy:KEY_SMOOTHMODE];
    NSLog(@"[LV] SmoothMode:%d",blnSmoothMode);
    
    // RenderViews Layout Initiate
    m_intDivide = 1;
    [self resetLiveImageViewWithAnimated:YES mode:m_intDivide];
    [self selectView:0];
    
    [layoutPanel setHidden:NO];
    [textSpeed setHidden:YES];
    [channelPanel setHidden:YES];
    
    // set ControlBar Items
    blnPlaybackMode = streamReceiver.m_blnPlaybackMode;
    NSArray *barItems = blnPlaybackMode ? dvrPbItems:dvrLiveItems;
    [controlBar setItems:barItems];
    self.navigationItem.rightBarButtonItems = nil; //Double confirm....
    
#ifndef UILAYOUTMODE
    // Initiate StreamReceiver and Get Device Info
    if (device != nil && streamReceiver==nil) {
       
        // Initial
        blnChPanelOn = NO;
        blnPtzPanelOn = NO;
        blnStop = NO;
        blnGetInfo = NO;
        blnFirstConnect = YES;
        blnSeqOn = NO;
        ptzStatus = 99;
        channelBarPage = 4;
        currentChPage = 0;
        totalChannel = 0;
        currentChMask = 0;
        [ptzPanel setHidden:YES];
        [focusView setHidden:YES];
        
        //Check NetworkStatus
        if (![self checkNetworkStatus:device]) {
            
            [self showAlarmAndBack:NSLocalizedString(@"MsgTimeOut", nil)];
            return;
        }
        
        switch (device.streamType) {
            case STREAM_P2:
                streamReceiver = [[P2StreamReceiver alloc] initWithDevice:device];
                NSLog(@"[LV] Initiate P2 streaming receiver %p",streamReceiver);
                break;
            case STREAM_ICATCH:
                streamReceiver = [[iCStreamReceiver alloc] initWithDevice:device];
                NSLog(@"[LV] Initiate iC streaming receiver %p",streamReceiver);
                break;
            case STREAM_NVR265:
                streamReceiver = [[NVRStreamReceiver alloc] initWithDevice:device];
                NSLog(@"[LV] Initiate NVR streaming receiver %p",streamReceiver);
                break;
            default:
                [self showAlarmAndBack:NSLocalizedString(@"MsgDeviceErr", nil)];
                break;
        }
        [streamReceiver setDelegate:self];
        
        for (UIBarButtonItem *cBtn in controlBar.items)
            [cBtn setEnabled:NO];
        
        [self checkLayoutItems:NO];
    }
#endif
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [devTable setHidden:NO];
    [loadingAct setCenter:self.view.center];
    [self.view addSubview:loadingAct];
    [self resetLiveImageViewWithAnimated:YES mode:m_intDivide];
    
    if (blnOpenSearch) {
        
        blnOpenSearch = NO;
        blnPlaybackPause = NO;
        [pausePlayBtn setImage:IMG_PAUSE forState:UIControlStateNormal];
        if (!streamReceiver.m_blnPlaybackMode) {
            
            if (m_intDivide==1) {
                [videoControl resumeByCH:selectWindow];
            }else {
                [videoControl resume];
            }
        }else {
            
            NSLog(@"[LV] Live -> Playback");
            controlBar.items = dvrPbItems;
            [textSpeed setHidden:NO];
            [self checkLayoutItems:NO];
            [self checkLiveChannelButton];
            
            if (streamReceiver.m_blnEventSearchRS) {
                currentChannel = streamReceiver.eventSearchCH;
                streamReceiver.m_blnEventSearchRS = NO;
            }
            
            [self checkValidButtonItems:currentChannel];
            
            pbSpeed = 1;
            [textSpeed setText:[NSString stringWithFormat:@"%ldX",(long)pbSpeed]];
            [NSThread detachNewThreadSelector:@selector(startStreaming)
                                     toTarget:self
                                   withObject:nil];
        }
    }else {
        
        [NSThread detachNewThreadSelector:@selector(getDeviceInfomation)
                                 toTarget:self
                               withObject:nil];
    }
    self.navigationItem.rightBarButtonItems = navRightItems; //Double confirm....
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (blnChPanelOn) { // close channel panel
        [self openChPanel:chBtn];
    }
    if (blnPtzPanelOn) { // close PTZ panel
        [self openPtzPanel:ptzBtn];
    }
    if (blnSeqOn)
        [self passChCmd:btnSequence];
    
    if (blnSoundOn)
        [self openSound];
    
    if (blnSpeakOn)
        [self openSpeak];
    
    if (blnOpenSearch) {
        [videoControl pause];
        return;
    }
    
    if (blnRecording) {
        [self changeRecordMode:recordBtn];
    }
    
    if (blnPbControl) {
        [self openPBControlPanel];
    }
    
    blnStop = YES;
    if (streamReceiver)
        [streamReceiver stopStreaming];
    
    device = nil;
    
    while (blnInfoThread) 
        [NSThread sleepForTimeInterval:0.01f];
    
    [devTable setHidden:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (blnOpenSearch) {
        return;
    }
    
    //while (blnStreamThread)
        //[NSThread sleepForTimeInterval:0.01f];
    
    if (self.searchView) {                  // avoid searchView's pointer not released
        searchView.streamReceiver = nil;
    }
    
//    devArray = nil;
    [playingIndexList removeAllObjects];
    
    SAVE_FREE(streamReceiver);
    device = nil;
}

- (void)backToPreviousPage
{
    if (blnPtzPanelOn)  // close PTZ panel
        [self openPtzPanel:ptzBtn];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    SAVE_FREE(searchView);
    SAVE_FREE(layoutItems);
    SAVE_FREE(dvrLiveItems);
    SAVE_FREE(dvrPbItems);
    SAVE_FREE(navRightItems);
    
    [controlBar removeFromSuperview];
    
    [playingIndexList removeAllObjects];
    SAVE_FREE(playingIndexList);
    SAVE_FREE(devTable);
    
    [renderArray removeAllObjects];
    
    SAVE_FREE(videoControl);
    SAVE_FREE(audioControl);
    SAVE_FREE(speakControl);
    mediaLibrary = nil;
    
    [super dealloc];
}

#pragma mark - View Control

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation!=UIInterfaceOrientationPortraitUpsideDown);
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
	{
        blnFullScreen = NO;
		[self.navigationController setNavigationBarHidden:NO animated:YES];
        
        if (blnPtzPanelOn) {
            [scrollPtzBar setHidden:NO];
            if (ptzStatus == PTZ_FOCUS) {
                [focusView setHidden:NO];
            }
        }
        
        [layoutPanel setHidden:blnPtzPanelOn];
		[controlBar setHidden:NO];
        
        if (blnPbControl)
            [pbControlView setHidden:NO];
	}
	else
	{
        blnFullScreen = YES;
        
        if (!blnChPanelOn && !blnPtzPanelOn && !blnPbControl) {
            
            [self.navigationController setNavigationBarHidden:YES animated:YES];
            [layoutPanel setHidden:YES];
            [controlBar setHidden:YES];
            [pbControlView setHidden:YES];
        }
	}
    
    [devTable setHidden:YES];
    BOOL blnOpenCH = blnChPanelOn;
    if (blnOpenCH) {
        
        [self openChPanel:chBtn];
        blnChPanelOn = blnOpenCH;
    }
    
    for (VideoDisplayView *tmpView in renderArray) {
        [tmpView.mainView setHidden:YES];
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [loadingAct setCenter:self.view.center];
    
    if(blnFullScreen == NO) {
        
        [self setPortraitLayout];
    }else {
     
        [self setLandscapeLayout];
    }
    
    [self resetLiveImageViewWithAnimated:YES mode:m_intDivide];
    CGRect selfFrame = self.view.frame;
    CGRect barFrame = CGRectMake(0, selfFrame.size.height - TOOLBAR_HEIGHT_V, selfFrame.size.width, TOOLBAR_HEIGHT_V);
    [controlBar setFrame:barFrame];
    [controlBar setNeedsDisplay];
    
    if (blnChPanelOn) {
        blnChPanelOn = NO;
        [self openChPanel:chBtn];
    }
    [devTable setHidden:NO];
}

- (void)setPortraitLayout
{
    CGRect selfFrame = self.view.frame;
    CGRect layoutFrame = CGRectMake(0, TOOLBAR_HEIGHT_V+10, selfFrame.size.width, TOOLBAR_HEIGHT_V);
    [textSpeed setFrame:CGRectMake(selfFrame.size.width-70, 0, 60, TOOLBAR_HEIGHT_V)];
    [layoutPanel setFrame:layoutFrame];
    
    //Channel Panel
    CGRect tbFrame = selfFrame;
    tbFrame.origin.x -= CH_LIST_WIDTH;
    tbFrame.size.width = CH_LIST_WIDTH;
    [devTable setFrame:tbFrame];
    
    CGRect channelFrame = CGRectMake(0, selfFrame.size.height-TOOLBAR_HEIGHT_V-CH_PANEL_HEIGHT, selfFrame.size.width, CH_PANEL_HEIGHT);
    [channelPanel setFrame:channelFrame];
    [scrollChBar setFrame:CGRectMake(0, 0, selfFrame.size.width, CH_PANEL_HEIGHT)];
    [scrollChBar setContentOffset:CGPointMake(0, 0)];
    [self setChannelPanelLayout];
    [self setScrollChBarLayout];
    
    //PTZ Panel
    CGRect ptzFrame = CGRectMake(0, TOOLBAR_HEIGHT_V, selfFrame.size.width, selfFrame.size.height-2*TOOLBAR_HEIGHT_V);
    [ptzPanel setFrame:ptzFrame];
    [scrollPtzBar setFrame:CGRectMake(0, 15, selfFrame.size.width, PTZ_BTNPANEL_HEIGHT)];
    [scrollPtzBar setContentSize:CGSizeMake(scrollPtzBar.frame.size.width*2, PTZ_BTNPANEL_HEIGHT)];
    CGFloat curOffset_x = scrollPtzBar.contentOffset.x - ((int)scrollPtzBar.contentOffset.x)%((int)scrollPtzBar.frame.size.width);
    CGPoint newOffset = CGPointMake(curOffset_x, 0);
    [scrollPtzBar setContentOffset:newOffset animated:YES];
    [focusView setFrame:CGRectMake(0, ptzPanel.frame.size.height-PTZ_FOCUS_HEIGHT, ptzPanel.frame.size.width, PTZ_FOCUS_HEIGHT)];
    [presetBar setFrame:CGRectMake(0, selfFrame.size.height-KEYBOARD_HEIGHT_V-TOOLBAR_HEIGHT_V, ptzPanel.frame.size.width, TOOLBAR_HEIGHT_V)];
    [self setPtzPanelLayout];
    
    //PBControl Panel
    CGRect pbFrame = pbControlView.frame;
    pbFrame.origin.x = (selfFrame.size.width - pbFrame.size.width)/2;
    pbFrame.origin.y = selfFrame.size.height - TOOLBAR_HEIGHT_V - pbFrame.size.height - 5;
    [pbControlView setFrame:pbFrame];
    
    //Navigation bar item need to resize
    [snapBtn setFrame:CGRectMake(0, 0, 44, 44)];
    [recordBtn setFrame:CGRectMake(0, 0, 44, 44)];
}

- (void)setLandscapeLayout
{
    CGRect selfFrame = self.view.frame;
    CGRect layoutFrame = CGRectMake(0, TOOLBAR_HEIGHT_H+10, selfFrame.size.width, TOOLBAR_HEIGHT_V);
    [textSpeed setFrame:CGRectMake(selfFrame.size.width-70, 0, 60, TOOLBAR_HEIGHT_V)];
    [layoutPanel setFrame:layoutFrame];
    
    //Channel Panel
    CGRect tbFrame = selfFrame;
    tbFrame.origin.x -= CH_LIST_WIDTH;
    tbFrame.size.width = CH_LIST_WIDTH;
    [devTable setFrame:tbFrame];
    
    //Channel Panel
    CGRect channelFrame = CGRectMake(0, selfFrame.size.height-TOOLBAR_HEIGHT_V-CH_PANEL_HEIGHT, selfFrame.size.width, CH_PANEL_HEIGHT);
    [channelPanel setFrame:channelFrame];
    [scrollChBar setFrame:CGRectMake(0, 0, selfFrame.size.width, CH_PANEL_HEIGHT)];
    [scrollChBar setContentOffset:CGPointMake(0, 0)];
    [self setChannelPanelLayout];
    [self setScrollChBarLayout];
    
    //PTZ Panel
    CGRect ptzFrame = CGRectMake(0, TOOLBAR_HEIGHT_H, selfFrame.size.width, selfFrame.size.height-TOOLBAR_HEIGHT_H-TOOLBAR_HEIGHT_V);
    [ptzPanel setFrame:ptzFrame];
    [scrollPtzBar setFrame:CGRectMake(0, 15, selfFrame.size.width, PTZ_BTNPANEL_HEIGHT)];
    [scrollPtzBar setContentSize:CGSizeMake(scrollPtzBar.frame.size.width*2, PTZ_BTNPANEL_HEIGHT)];
    CGFloat curOffset_x = scrollPtzBar.contentOffset.x - ((int)scrollPtzBar.contentOffset.x)%((int)scrollPtzBar.frame.size.width);
    CGPoint newOffset = CGPointMake(curOffset_x, 0);
    [scrollPtzBar setContentOffset:newOffset animated:YES];
    [focusView setFrame:CGRectMake(0, ptzPanel.frame.size.height-PTZ_FOCUS_HEIGHT, ptzPanel.frame.size.width, PTZ_FOCUS_HEIGHT)];
    [presetBar setFrame:CGRectMake(0, selfFrame.size.height-KEYBOARD_HEIGHT_H-TOOLBAR_HEIGHT_H, ptzPanel.frame.size.width, TOOLBAR_HEIGHT_H)];
    [self setPtzPanelLayout];
    
    //PBControl Panel
    CGRect pbFrame = pbControlView.frame;
    pbFrame.origin.x = (selfFrame.size.width - pbFrame.size.width)/2;
    pbFrame.origin.y = selfFrame.size.height - TOOLBAR_HEIGHT_H - pbFrame.size.height - 10;
    [pbControlView setFrame:pbFrame];
    
    //Navigation bar item need to resize
    [snapBtn setFrame:CGRectMake(0, 0, 32, 32)];
    [recordBtn setFrame:CGRectMake(0, 0, 32, 32)];
}

- (void)resetLiveImageViewWithAnimated:(BOOL)animated mode:(NSInteger)n_divide
{
    CGFloat width = self.view.frame.size.width;
    CGFloat height = blnFullScreen? self.view.frame.size.height:(width*0.625);
    CGFloat center = self.view.center.y;
    CGFloat zeroPoint = center - height/2;
    NSInteger iwidth = width / n_divide;
    NSInteger iHeight = height / n_divide;
    NSInteger index = 0;
    [videoControl pause];
    
    if (n_divide == 1) {
        
        index = selectWindow;
        [videoControl resetByCH:index];
        
        [renderViews[index].mainView setTransform:CGAffineTransformIdentity];
        [renderViews[index].mainView setFrame:CGRectMake(0, zeroPoint, iwidth, iHeight)];
        [renderViews[index] resetView:YES];
        [videoControl setResolution:(blnSmoothMode?erm_d1:erm_hd)];
        
        for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
            if (i == index) {
                [renderViews[i] showView:YES];
                [videoControl resumeByCH:i];
            }else {
                [renderViews[i] showView:NO];
            }
        }
        [btn1Div setImage:IMG_1DIVOVER forState:UIControlStateNormal];
        [btn4Div setImage:IMG_4DIV forState:UIControlStateNormal];
        
    }else {
        for (NSInteger i=0; i<n_divide; i++) {
            for (NSInteger j=0; j<n_divide; j++) {
                index = i + j*n_divide;
                
                if (!(0x1<<index & videoControl.outputMask)
                    && device.product == DEV_DVR
                    && device.streamType != STREAM_NVR265
                    && !blnXmsIPcam) {
                    [videoControl startByCH:index];
                }
                [renderViews[index].mainView setTransform:CGAffineTransformIdentity];
                [renderViews[index].mainView setFrame:CGRectMake(i*iwidth, zeroPoint+j*iHeight, iwidth, iHeight)];
                [renderViews[index] resetView:YES];
            }
        }
        [videoControl setResolution:erm_cif];
        
        for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
            if (i <= index) {
                [renderViews[i] showView:YES];
                [videoControl resumeByCH:i];
            }else {
                [renderViews[i] showView:NO];
            }
        }
        [btn1Div setImage:IMG_1DIV forState:UIControlStateNormal];
        [btn4Div setImage:IMG_4DIVOVER forState:UIControlStateNormal];
    }
    
    [videoControl reset];
    curZoomScale = minZoomScale = 1.0;
    maxZoomScale = 4.0;
    m_intDivide = n_divide;
    
    if (animated) {
        [UIView commitAnimations];
    }
}

- (IBAction)changeViewMode:(id)sender
{
    UIButton *tmpBtn = (UIButton *)sender;
    
    if (tmpBtn.tag == m_intDivide) return;
    
    currentChannel = renderViews[selectWindow].playingChannel;
    currentChMask = (unsigned)0xF << (currentChannel/4)*4;
    
    [self resetLiveImageViewWithAnimated:YES mode:tmpBtn.tag];
    if (tmpBtn.tag == 1) {
        
        [videoControl pauseByCH:selectWindow];
        [self outputListReset];
        [streamReceiver.outputList replaceObjectAtIndex:currentChannel withObject:[NSString stringWithFormat:@"%ld",(long)selectWindow]];
        streamReceiver.currentPlayCH = (unsigned)0x1<<currentChannel;
        [streamReceiver changeVideoMask:(unsigned)0x1<<currentChannel];
        [videoControl resumeByCH:selectWindow];
        [self checkValidButtonItems:currentChannel];
    }else {
        
        if (device.streamType == STREAM_NVR265) {
            [self autoFillMask];
        }
        else {
            [videoControl pause];
            currentChannel = currentChannel - currentChannel%4;
            streamReceiver.currentPlayCH = (unsigned)0x1<<currentChannel;
            [self outputListReset];
            for (NSInteger i=0; i<4; i++) {
                [streamReceiver.outputList replaceObjectAtIndex:currentChannel+i withObject:[NSString stringWithFormat:@"%ld",(long)i]];
            }
            [streamReceiver changeVideoMask:currentChMask];
            [self checkValidButtonItems:currentChannel+selectWindow];
            [videoControl resume];
        }
        [ptzBtn setEnabled:NO];
    }
    
    [self checkLiveChannelButton];
}

#pragma mark - Streaming Control

- (void)getDeviceInfomation
{
    NSLog(@"[LV] GetDeviceInfo Start");
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    blnInfoThread = YES;
    
    if (!blnGetInfo) {
        
        //20160613 modified by Ray Lin, because loading activity indicator didn't show in performSelectorOnMainThread funtion
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        dispatch_async(mainQueue, ^(){
            [self showMaskView:YES];
        });
        
        // Get Device Information
        if (![streamReceiver getDeviceInfo])
        {
            if (!streamReceiver.errorDesc) {
                streamReceiver.errorDesc = NSLocalizedString(@"MsgInfoErr", nil);
            }
            blnInfoThread = NO;
            [self showAlarmAndBack:streamReceiver.errorDesc];
            return;
        }
        
        blnGetInfo = YES;
        
        if (streamReceiver.m_DiskGMT) {
            device.devTimeZone = [streamReceiver.m_DiskGMT copy];
            NSLog(@"[XMS] Device TIMEZONE : %@",device.devTimeZone);
        }
        
        // Update Channel Panel
        totalChannel = streamReceiver.iMaxSupportChannel;
        channelBarPage = ceil((float)totalChannel/8);
        [self setValidChannelButton];
        
        currentChannel = ((CameraInfo *)[streamReceiver.cameraList firstObject]).index-1;
    }
    
    // Start Streaming
    if (!blnStop && streamReceiver.validChannel>0) {
        
        if (device.streamType == STREAM_NVR265) {
            [playingIndexList addObject:[streamReceiver.cameraList firstObject]];
            [self addStreaming:0 viewIdx:0];
        }
        else {
            [NSThread detachNewThreadSelector:@selector(startStreaming)
                                     toTarget:self
                                   withObject:nil];
        }
    }
    for (UIBarButtonItem *cBtn in controlBar.items) {
        [cBtn setEnabled:YES];
    }
    [recordBtn setEnabled:YES];
    
    if (device.streamType == STREAM_DYNA
        || device.streamType == STREAM_AFREEY
        || device.streamType == STREAM_ICATCH) {
        [speakBtn setEnabled:NO];
    }
    
    [self showMaskView:NO];
    [self checkLiveChannelButton];
    [self checkValidButtonItems:currentChannel];
    [self setScrollChBarLayout];
    
    blnInfoThread = NO;
    [pool release];
    NSLog(@"[LV] GetDeviceInfo Stop");
}

- (void)startStreaming
{
    NSLog(@"[LV] Streaming Thread Start");
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    blnStreamThread = YES;
    
    if (streamReceiver.m_blnPlaybackMode) {
        
        if (videoControl!=nil) {
            streamReceiver.videoControl = videoControl;
            [streamReceiver.videoControl startByCH:selectWindow];
        }else {
            NSLog(@"[LV] VideoControl Loss");
            return;
        }
        
        [self outputListReset];
        [streamReceiver.outputList replaceObjectAtIndex:currentChannel withObject:[NSString stringWithFormat:@"%ld",(long)selectWindow]];
        streamReceiver.currentPlayCH = (unsigned)0x1<<currentChannel;
        
        if ( ![streamReceiver startPlaybackStreaming:(unsigned)0x1<<currentChannel] ) {
            [self showAlarmAndBack:[streamReceiver errorDesc]];
        }
        [videoControl stop];
    }
    else {
        if (device.streamType == STREAM_NVR265) {
//            streamReceiver.currentPlayCH = (unsigned)0x1<<currentChannel;
            if ( ![streamReceiver startLiveStreaming:currentChannel] ) {
                [self showAlarm:[streamReceiver errorDesc]];
            }
        }
        else {
            
            if (videoControl!=nil) {
                streamReceiver.videoControl = videoControl;
                [streamReceiver.videoControl startByCH:selectWindow];
            }
            else {
                NSLog(@"[LV] VideoControl Loss");
                return;
            }
            
            [self outputListReset];
            [streamReceiver.outputList replaceObjectAtIndex:currentChannel withObject:[NSString stringWithFormat:@"%ld",(long)selectWindow]];
            streamReceiver.currentPlayCH = (unsigned)0x1<<currentChannel;
            if ( ![streamReceiver startLiveStreaming:(unsigned)0x1<<currentChannel] ) {
                [self showAlarmAndBack:[streamReceiver errorDesc]];
            }
            [videoControl stop];
        }
    }
    
    blnStreamThread = NO;
    [pool release];
    NSLog(@"[LV] Streaming Thread Stop");
}

- (void)addStreaming:(NSInteger)chIdx viewIdx:(NSInteger)vIdx
{
    DBGLog(@"[XMS] Add CH:%ld View:%ld",(long)chIdx,(long)vIdx);
    [streamReceiver.outputList replaceObjectAtIndex:0 withObject:[NSString stringWithFormat:@"%ld",(long)vIdx]];
//    [streamReceiver.outputList replaceObjectAtIndex:chIdx withObject:[NSString stringWithFormat:@"%ld",(long)vIdx]];
    currentChannel = chIdx;
    
    if (videoControl!=nil) {
        streamReceiver.videoControl = videoControl;
        [streamReceiver.videoControl startByCH:vIdx];
    }else {
        NSLog(@"[LV] VideoControl Loss");
        return;
    }
    
    [NSThread detachNewThreadSelector:@selector(startStreaming)
                             toTarget:self
                           withObject:nil];
    
    [streamReceiver.videoControl setPlayingChannelByCH:vIdx Playing:chIdx];
}

- (void)autoFillMask
{
    for (CameraInfo *tmpInfo in streamReceiver.cameraList)
    {
        if ([playingIndexList indexOfObject:tmpInfo] == NSNotFound) {  //沒有在播放的camera
            
            if (playingIndexList.count+1 > pow(m_intDivide,2)) //現有view已點滿了
                return;
            
            //找個空的view來add
            for (VideoDisplayView *tmpView in streamReceiver.videoControl.aryDisplayViews)
            {
                if (!tmpView.blnIsPlay) {
                    
                    [self selectView:[streamReceiver.videoControl.aryDisplayViews indexOfObject:tmpView]];
                    [playingIndexList addObject:tmpInfo];
                    [NSThread sleepForTimeInterval:0.1f];
                    [self addStreaming:tmpInfo.index-1 viewIdx:selectWindow];
                    break;
                }
            }
        }
    }
}

- (IBAction)changeRecordMode:(id)sender
{
    if (streamReceiver.blnIsMJPEG == YES) {
        [self showAlarm:@"No Support MJPEG Recording"];
        NSLog(@"[LV] No Support for the MJPEG mode");
        return;
    }
    
    if (!blnRecording) {
        //Check the window is playing or not
        if (!renderViews[selectWindow].blnIsPlay) {
            NSLog(@"[DV] Inactive Window");
            return;
        }
        
        //start record
        if (recordControl) {
            BOOL ret = [recordControl startRecordingWithView:selectWindow vCtx:[videoControl getCodecCtxByCH:selectWindow] aCtx:NULL FPS:[videoControl getFpsByCH:selectWindow]];
            if (!ret) {
                [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
                return;
            }
        }
        
        [recordBtn setImage:[UIImage imageNamed:@"recordingover.png"] forState:UIControlStateNormal];
        renderViews[selectWindow].blnIsRec = YES;
        blnRecording = YES;
    }else {
        
        [recordControl stopRecording];
        [recordBtn setImage:[UIImage imageNamed:@"recording.png"] forState:UIControlStateNormal];
        renderViews[recordControl.recView].blnIsRec = NO;
        [self showAlarm:NSLocalizedString(@"MsgMp4OK", nil)];
        blnRecording = NO;
    }
}

- (void)setAudioSessionProp
{
	
	// set audio session property
	UInt32 category;
	if (blnSoundOn && blnSpeakOn) {
		category = kAudioSessionCategory_PlayAndRecord;
	}
	else if (blnSpeakOn) {
		category = kAudioSessionCategory_RecordAudio;
	}
	else {
		category = kAudioSessionCategory_LiveAudio;
	}
	
	OSStatus error = AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(category), &category);
	if (error) printf("Couldn't set audio category!");
}

- (void)openSound
{
    blnSoundOn = !blnSoundOn;
    if (!blnSoundOn) {
		
        [self setAudioSessionProp];
		[streamReceiver closeSound];
        [soundBtn setImage:IMG_SOUND forState:UIControlStateNormal];
	}else {
        
		if (audioControl && !streamReceiver.audioControl) {
            streamReceiver.audioControl = audioControl;
        }
        
        [self setAudioSessionProp];
		[streamReceiver openSound];
        [soundBtn setImage:IMG_SOUND_OVER forState:UIControlStateNormal];
	}
}

- (IBAction)openSpeak
{
    blnSpeakOn = !blnSpeakOn;
    if (!blnSpeakOn) {
		
        [self setAudioSessionProp];
        [speakControl stop];
		[streamReceiver closeSpeak];
        [speakControl close];
        
        [speakBtn setImage:IMG_SPEAK forState:UIControlStateNormal];
	}else {
        
        if (speakControl!=nil) {
            
            [self setAudioSessionProp];
            [speakControl close];
            switch (streamReceiver.audioCodec) {
                case P2_AUDIO_TYPE_G711:
                case P3_AUDIO_TYPE_G711:
                    speakControl = [speakControl initWithCodecId:AV_CODEC_ID_PCM_MULAW srate:16000 bps:4 balign:2560 fsize:2560];
                    break;
                case P2_AUDIO_TYPE_ADPCM:
                case P3_AUDIO_TYPE_ADPCM:
                    speakControl = [speakControl initWithCodecId:AV_CODEC_ID_ADPCM_IMA_WAV srate:8000 bps:4 balign:164 fsize:321];
                    break;
                    
                case P3_AUDIO_TYPE_NVRG711:
                    speakControl = [speakControl initWithCodecId:AV_CODEC_ID_PCM_MULAW srate:8000 bps:8 balign:320 fsize:320];
                    break;
            }
            
            streamReceiver.audioSender.auFlag = streamReceiver.audioCodec;
            streamReceiver.audioSender.auChannel = log2(streamReceiver.currentPlayCH);
            [speakControl setPostProcessAction:self action:@selector(sendingData:)];
            [speakControl start];
            [streamReceiver openSpeak];
            
            [speakBtn setImage:IMG_SPEAK_OVER forState:UIControlStateNormal];
        }
	}
}

- (void)sendingData:(NSData *)data
{
    [streamReceiver.audioSender sendData:data];
}

- (IBAction)changePlaybackMode:(id)sender
{
    NSInteger btnId = ((UIButton *)sender).tag;
    NSInteger mode;
    
    if (btnId == PLAYBACK_EXIT) {
        
        NSLog(@"[LV] Playback -> Live");
        [self openPBControlPanel];

        [streamReceiver stopStreaming];
        while(blnStreamThread) {                   //Wait for StreamThread Stop
            [NSThread sleepForTimeInterval:0.01f];
        }
        streamReceiver.m_blnPlaybackMode = NO;
        blnFirstConnect = YES;
        [self viewWillAppear:YES];                  //Set Layout
        blnStop = NO;
        [self viewDidAppear:YES];                   //Start Streaming
        
        return;
    }
    
    switch (btnId) {
            
        case PLAYBACK_PAUSE:
            if (blnPlaybackPause) {
                [pausePlayBtn setImage:IMG_PAUSE forState:UIControlStateNormal];
                mode = PLAYBACK_FORWARD;
                [streamReceiver.videoControl resume];
                pbSpeed = 1;
            }else {
                [pausePlayBtn setImage:IMG_PLAY forState:UIControlStateNormal];
                [fastFwBtn setImage:IMG_FF forState:UIControlStateNormal];
                [fastBwBtn setImage:IMG_FB forState:UIControlStateNormal];
                mode = PLAYBACK_PAUSE;
                [streamReceiver.videoControl pause];
                pbSpeed = 0;
            }
            blnPlaybackPause = !blnPlaybackPause;
            break;
            
        case PLAYBACK_FAST_FORWARD:
        case PLAYBACK_FAST_BACKWARD:
            if (blnPlaybackPause) {
                [pausePlayBtn setImage:IMG_PAUSE forState:UIControlStateNormal];
                [streamReceiver.videoControl resume];
                blnPlaybackPause = NO;
            }
            
            if (btnId == PLAYBACK_FAST_FORWARD) {
                [fastFwBtn setImage:IMG_FF_OVER forState:UIControlStateNormal];
                [fastBwBtn setImage:IMG_FB forState:UIControlStateNormal];
                if (pbSpeed <= 0) {
                    pbSpeed = 1;
                }else {
                    if (pbSpeed < 32) {
                        pbSpeed = pbSpeed*2;
                    }
                }
            }else {
                [fastFwBtn setImage:IMG_FF forState:UIControlStateNormal];
                [fastBwBtn setImage:IMG_FB_OVER forState:UIControlStateNormal];
                if (pbSpeed >= 0) {
                    pbSpeed = -1;
                }else {
                    if (pbSpeed > -32) {
                        pbSpeed = pbSpeed*2;
                    }
                }
            }
            mode = btnId;
            break;
    }
    
    [streamReceiver changePlaybackMode:mode withValue:labs(pbSpeed)];
    [textSpeed setText:[NSString stringWithFormat:@"%ldX",(long)pbSpeed]];
}

#pragma mark - Stream Delegate

- (void)EventCallback:(NSString *)msg
{
    NSLog(@"[LV] %@",msg);
    [self performSelectorOnMainThread:@selector(eventHandleProcess:)
                           withObject:msg
                        waitUntilDone:NO];
}

- (void)eventHandleProcess:(NSString *)msg
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    NSArray *msgArray = [msg componentsSeparatedByString:@":"];
    if ([[msgArray firstObject] isEqualToString:@"VIDEO_OK"])
        [self checkLayoutItems:YES];
    else if ([[msgArray firstObject] isEqualToString:@"PLAYBACK_NO"])
        [searchBtn setEnabled:NO];
    else if ([[msgArray firstObject] isEqualToString:@"PLAYBACK_YES"])
        [searchBtn setEnabled:YES];
    else if ([[msgArray firstObject] isEqualToString:@"RECORD_NO"])
        [recordBtn setEnabled:NO];
    else if ([[msgArray firstObject] isEqualToString:@"BPS"])
        [self setTitle:[msgArray objectAtIndex:1]];
    
    [pool release];
}

- (void)xmsEventStatusCallback:(NSDictionary *)data
{
    
}

#pragma mark - Channel Panel Management

- (IBAction)openChPanel:(id)sender
{
    CGRect navFrame = self.navigationController.navigationBar.frame;
    CGRect tbFrame = devTable.frame;
    CGRect barFrame = controlBar.frame;
    
    if (blnChPanelOn) {
        [chBtn setImage:IMG_CH forState:UIControlStateNormal];
        if (blnChSelector) {
            [channelPanel setHidden:YES];
        }else {
            navFrame.origin.x -= tbFrame.size.width;
            barFrame.origin.x -= tbFrame.size.width;
            tbFrame.origin.x -= tbFrame.size.width;
            //[videoControl resume];
        }
    }else {
        [chBtn setImage:IMG_CH_OVER forState:UIControlStateNormal];
        if (blnChSelector) {
            [channelPanel setHidden:NO];
        }else {
            navFrame.origin.x += tbFrame.size.width;
            barFrame.origin.x += tbFrame.size.width;
            tbFrame.origin.x += tbFrame.size.width;
            [self.view bringSubviewToFront:devTable];
            //[videoControl pause];
        }
        
        if (blnPtzPanelOn) {
            [self openPtzPanel:ptzBtn];
        }
    }
    
    if (blnChSelector) {
        
    }else {
        [UIView animateWithDuration:0.5
                         animations:^{
                             //[self.view setFrame:selfFrame];
                             [self.navigationController.navigationBar setFrame:navFrame];
                             [controlBar setFrame:barFrame];
                             [devTable setFrame:tbFrame];
                         }];
        [maskView setHidden:blnChPanelOn];
    }
    blnChPanelOn = !blnChPanelOn;
}

- (void)setChannelPanelLayout
{
    CGRect bounds = [scrollChBar bounds];
	CGFloat oriX = (CGRectGetMinX(bounds)+CGRectGetMaxX(bounds))/2-4*(CH_BTN_WIDTH+CH_BTN_GAP);
    CGFloat stepX = CH_BTN_WIDTH+CH_BTN_GAP;
    CGFloat shiftX = bounds.size.width;
    
    if (blnFullScreen) {
        oriX = oriX - 15;
        stepX = stepX + CH_BTN_GAP;
    }
    
    for (int i=0; i<8; i++) {
		[channelBtn[i] setFrame:CGRectMake(oriX+stepX*i,5,CH_BTN_WIDTH,CH_BTN_HEIGHT)];
	}
	for (int i=8; i<16; i++) {
		[channelBtn[i] setFrame:CGRectMake(shiftX+oriX+stepX*(i-8),5,CH_BTN_WIDTH,CH_BTN_HEIGHT)];
	}
    for (int i=16; i<24; i++) {
		[channelBtn[i] setFrame:CGRectMake(2*shiftX+oriX+stepX*(i-8*2),5,CH_BTN_WIDTH,CH_BTN_HEIGHT)];
	}
    for (int i=24; i<32; i++) {
		[channelBtn[i] setFrame:CGRectMake(3*shiftX+oriX+stepX*(i-8*3),5,CH_BTN_WIDTH,CH_BTN_HEIGHT)];
	}
}

- (void)setScrollChBarLayout
{
    [scrollChBar setContentSize:CGSizeMake(scrollChBar.frame.size.width*channelBarPage, CH_PANEL_HEIGHT)];
    CGFloat curOffset_x = currentChannel/8 * scrollChBar.frame.size.width;
    CGPoint newOffset = CGPointMake(curOffset_x, 0);
    [scrollChBar setContentOffset:newOffset animated:NO];
}

- (BOOL)checkValidChannel:(NSInteger)chIdx
{
    NSInteger ret = (0x1<<chIdx)&streamReceiver.validChannel;
    
    return ret!=0;
}

- (void)checkValidButtonItems:(NSInteger)chIdx
{
    if (device.streamType == STREAM_HDIP
        || device.streamType == STREAM_AFREEY
        || device.streamType == STREAM_NEVIO
        || device.streamType == STREAM_GEMTEK
        || device.streamType == STREAM_DYNA
        || device.streamType == STREAM_XMS
        || device.streamType == STREAM_ONVIF
        || device.streamType == STREAM_NVR265) {
        return;
    }
    
    //check currentCH's live is valid or not, if invalid, disable all the button.
    BOOL valid =  [self checkValidChannel:chIdx];
    if (!valid) {
        [recordBtn setEnabled:valid];
        [ptzBtn setEnabled:valid];
        [searchBtn setEnabled:valid];
        [speakBtn setEnabled:valid];
        [soundBtn setEnabled:valid];
        [snapBtn setEnabled:valid];
    }
    else {
        [recordBtn setEnabled:valid];
        [speakBtn setEnabled:valid];
        [soundBtn setEnabled:valid];
        [snapBtn setEnabled:valid];
        
        //check valid ptzbtn
        NSInteger ret = (0x1<<chIdx)&streamReceiver.validPtzChannel;
        
        if (ret!=0) {
            [ptzBtn setEnabled:YES];
        }
        else
            [ptzBtn setEnabled:NO];
        
        //check valid searchbtn
        NSInteger ret2 = (0x1<<chIdx)&streamReceiver.validPlaybackCh;
        
        if (ret2!=0) {
            [searchBtn setEnabled:YES];
        }
        else
            [searchBtn setEnabled:NO];
    }
}

- (void)setValidChannelButton
{
   	NSLog(@"[LV] valid channel:%lu",(unsigned long)streamReceiver.validChannel);
	for (int i=0; i<MAX_CH_NUMBER; i++) {
        
        if (i < streamReceiver.iMaxSupportChannel) {
            [channelBtn[i] setEnabled:YES];
        }
        else {
            [channelBtn[i] setEnabled:NO];
        }
	}
}

- (void)checkLiveChannelButton
{
    UIImage *bgImage = [IMG_UNSELECT stretchableImageWithLeftCapWidth:8.0 topCapHeight:0.0];
    UIImage *bgImage_pressed = [IMG_SELECT stretchableImageWithLeftCapWidth:8.0 topCapHeight:0.0];
    NSUInteger mask = m_intDivide==1 ? (unsigned)0x1<<currentChannel : currentChMask;
    
    for (int i=0; i<MAX_CH_NUMBER; i++) {
        
        if (0x1<<i & mask) {
            [channelBtn[i] setBackgroundImage:bgImage_pressed forState:UIControlStateNormal];
        }else {
            [channelBtn[i] setBackgroundImage:bgImage forState:UIControlStateNormal];
        }
	}
    //NSLog(@"[LV] Mask:%d, Channel:%d, Page:%d",currentChMask,currentChannel,currentChPage);
}

////////Channel List///////
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return streamReceiver.cameraList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setTextColor:[UIColor lightTextColor]];
        [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
        cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
    }
    
    // Configure the cell...
    CameraInfo *currentEntry = [streamReceiver.cameraList objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@",currentEntry.title];
    return cell;
}

#pragma mark - Channel Control

- (IBAction)passChCmd:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self switchChannel:btn.tag];
}

- (void)switchChannel:(NSInteger)Idx
{
    [self resetLiveImageViewWithAnimated:YES mode:m_intDivide];
	if (Idx<32) {
        
        if (m_intDivide != 1) {
            [self changeViewMode:btn1Div];
            [videoControl pause];
        }
        
        currentChannel = Idx ;
        currentChPage = floor(currentChannel/4);
        
        if (device.streamType == STREAM_NVR265) {
            [streamReceiver stopStreamingByView:selectWindow];
//            [self outputListReset];
            [self addStreaming:currentChannel viewIdx:selectWindow];
        }
        else {
            [videoControl pauseByCH:selectWindow];
            [self outputListReset];
            [streamReceiver.outputList replaceObjectAtIndex:currentChannel withObject:[NSString stringWithFormat:@"%ld",(long)selectWindow]];
            
            streamReceiver.currentPlayCH = (unsigned)0x1<<currentChannel;
            [streamReceiver changeVideoMask:(unsigned)0x1<<currentChannel];
            streamReceiver.audioSender.auChannel = log2(streamReceiver.currentPlayCH);
            [videoControl resumeByCH:selectWindow];
            [videoControl reset];
        }
        
        [self checkLiveChannelButton];
        [self checkValidButtonItems:currentChannel];
	}
	else if (Idx==PREV_CH) { // previous channel
		
        if (m_intDivide == 1) {
            
            NSInteger index = (currentChannel+totalChannel-1)%totalChannel;
            
            if (device.streamType != STREAM_NVR265) {
                while ( ![self checkValidChannel:index] ) {
                    
                    index = (index+totalChannel-1)%totalChannel;
                }
            }
            [self switchChannel:index];
        }else {
            
            currentChPage = currentChPage - 1;
            if (currentChPage == -1 )
            {
                if (totalChannel%4 == 1) {
                    currentChPage = (totalChannel/4);
                }
                else
                    currentChPage = (totalChannel/4) - 1;
            }
            
            currentChMask = (unsigned)0xF<<(currentChPage*4);
            currentChannel = currentChPage*4;
            [videoControl pause];
            
            [streamReceiver changeVideoMask:currentChMask];
            [self outputListReset];
            for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
                [streamReceiver.outputList replaceObjectAtIndex:currentChannel+i withObject:[NSString stringWithFormat:@"%ld",(long)i]];
                [videoControl resetByCH:i];
                if (currentChannel+i >= totalChannel) {
                    [videoControl stopByCH:i];
                }
            }
            [videoControl resume];
            [self checkLiveChannelButton];
        }
	}
	else if (Idx==NEXT_CH) { // next channel
		
        if (m_intDivide == 1) {
            
            NSInteger index = (currentChannel+1)%totalChannel;
            if(currentChannel + 1 == [streamReceiver iMaxSupportChannel])
            {
                index = 0;
            }
            if (device.streamType != STREAM_NVR265) {
                while ( ![self checkValidChannel:index] ) {
                    
                    index = (index+1)%totalChannel;
                }
            }
            
            [self switchChannel:index];
            
        }else {
            
            currentChPage = currentChPage + 1;
            if (currentChPage == totalChannel/4) {
                if (totalChannel%4 != 1) {
                    currentChPage = 0;
                }
            }
            if (currentChPage == totalChannel/4 + 1) {
                currentChPage = 0;
            }
           
            currentChMask = (unsigned)0xF<<(currentChPage*4);
            currentChannel = currentChPage*4;
            [videoControl pause];
            
            [streamReceiver changeVideoMask:currentChMask];
            [self outputListReset];
            for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
                [streamReceiver.outputList replaceObjectAtIndex:currentChannel+i withObject:[NSString stringWithFormat:@"%ld",(long)i]];
                [videoControl resetByCH:i];
                if (currentChannel+i >= totalChannel) {
                    [videoControl stopByCH:i];
                }
            }
            [videoControl resume];
            [self checkLiveChannelButton];
        }
	}
	else if (Idx==SEQUENCE) { // channel sequence
		
		if (!blnSeqOn) { // run sequence
			seqTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(sequenceRun) userInfo:nil repeats:YES];
			[btnSequence setImage:[UIImage imageNamed:@"seq_pressed.png"] forState:UIControlStateNormal];
		}
		else { // stop sequence
			[btnSequence setImage:[UIImage imageNamed:@"seq.png"] forState:UIControlStateNormal];
			[seqTimer invalidate];
		}
		
		blnSeqOn = !blnSeqOn;
	}
}

- (void)sequenceRun
{
    static int counter = 0;
    counter++;
    
    if (counter>5) {
        
        [self switchChannel:NEXT_CH];
        counter=0;
    }
}

- (void)outputListReset
{
    for (NSInteger i=0; i<totalChannel; i++) {
        [streamReceiver.outputList replaceObjectAtIndex:i withObject:[NSString stringWithFormat:@"99"]];
    }
}

#pragma mark - PTZ Panel Management

- (IBAction)openPtzPanel:(id)sender
{
    if (blnPtzPanelOn) {
        [ptzPanel setHidden:YES];
        [layoutPanel setHidden:NO];
        [ptzBtn setImage:IMG_PTZ forState:UIControlStateNormal];
        
        if (ptzStatus==PTZ_PRESET)
            [self sendPtzCmd:PTZ_PRESET];
    }
    else {
        [ptzPanel setHidden:NO];
        [layoutPanel setHidden:YES];
        [ptzBtn setImage:IMG_PTZ_OVER forState:UIControlStateNormal];
        
        if (blnChPanelOn) {
            [self openChPanel:chBtn];
        }
    }
    blnPtzPanelOn = !blnPtzPanelOn;
}

- (void)setPtzPanelLayout
{
    CGRect  bounds = [ptzPanel frame];
    CGFloat centerX = CGRectGetMinX(bounds)+CGRectGetMaxX(bounds)/2;
	CGFloat oriX;
    CGFloat stepX = PTZ_BTN_WIDTH+PTZ_BTN_GAP;
    CGFloat shiftX = bounds.size.width;

    oriX = centerX-2*(PTZ_BTN_WIDTH+PTZ_BTN_GAP);
    if (blnFullScreen) {
        oriX = centerX-2*(PTZ_BTN_WIDTH+2*PTZ_BTN_GAP);
        stepX = PTZ_BTN_WIDTH+2*PTZ_BTN_GAP;
    }
    for (int i=0; i<4; i++) {
        [ptzCtrlBtn[i] setFrame:CGRectMake(oriX+stepX*i,7,PTZ_BTN_WIDTH,PTZ_BTN_HEIGHT)];
    }
    for (int i=4; i<8; i++) {
        [ptzCtrlBtn[i] setFrame:CGRectMake(shiftX+oriX+stepX*(i-4),7,PTZ_BTN_WIDTH,PTZ_BTN_HEIGHT)];
    }
    [focusIn setFrame:CGRectMake(5, 5, 30, 30)];
    [focusOut setFrame:CGRectMake(bounds.size.width-35, 5, 30, 30)];
    [focusBar setFrame:CGRectMake(40, 5, bounds.size.width-2*40, 23)];
    
    CGRect inputFrame = presetInput.frame;
    inputFrame.size.width = presetBar.frame.size.width / 2;
    [presetInput setFrame:inputFrame];
}

- (void)checkPtzButton
{
    ///////////////////////////
    //ptzStatus :
    //          0 - Focus
    //          1 - Iris+
    //          2 - Iris-
    //          3 - AutoPan
    //          4 - Preset
    //          5 - Pattern
    //          6 - Tour
    //          7 - Menu
    //         99 - None
    ///////////////////////////
    
    for (NSInteger i=0; i<PTZ_BTN_NUMBER; i++) {
        
        if (i==1 || i==2) {
            continue;
        }
        if (i == ptzStatus) {
            [ptzCtrlBtn[i] setBackgroundImage:IMG_SELECT forState:UIControlStateNormal];
        }else {
            [ptzCtrlBtn[i] setBackgroundImage:IMG_UNSELECT forState:UIControlStateNormal];
        }
        if (ptzStatus>2 && !focusView.hidden) {
            [focusView setHidden:YES];
        }
    }
}

#pragma mark - PTZ Control

- (void)passPtzCmd:(id)sender
{
    UIButton *tmpBtn = (UIButton *)sender;
    [self sendPtzCmd:tmpBtn.tag];
}

- (void)sendPtzCmd:(NSInteger)Idx
{
    NSInteger channel = renderViews[selectWindow].playingChannel;
    if (device.streamType == STREAM_ICATCH) {
        CameraInfo *curInfo = [streamReceiver.cameraList objectAtIndex:channel];
        channel = curInfo.ptzID;
        [streamReceiver ptzController].baudrate = curInfo.baudrate;
        [streamReceiver ptzController].ptzType = curInfo.ptzType;
        //NSLog(@"[LV] Set IC PTZ Type:%@, BR:%d",curInfo.ptzType,curInfo.baudrate);
    }
    else if (device.streamType == STREAM_XMS) {
        CameraInfo *curInfo = [playingIndexList firstObject];
        if (curInfo.deviceType == XMS_IPCAM || curInfo.deviceType == XMS_RTSP) {
            [[streamReceiver ptzController] setSessionId:[NSString stringWithFormat:@"%ld",(long)curInfo.ptzID]];
        }
    }
    
	switch (Idx) {
        case PTZ_FOCUS:
            if (ptzStatus==PTZ_OSD || ptzStatus==PTZ_PRESET) break;
            if (ptzStatus==Idx) {
                [focusView setHidden:YES];
                ptzStatus = PTZ_NONE;
            }else {
                [focusView setHidden:NO];
                ptzStatus = Idx;
            }
            break;
        case PTZ_IRISON:
			[[streamReceiver ptzController] ptzIrisOpen:channel];
			break;
		case PTZ_IRISOFF:
			[[streamReceiver ptzController] ptzIrisClose:channel];
			break;
        case PTZ_PRESET:
            if (ptzStatus==PTZ_OSD) break;
            if (ptzStatus==Idx) {
                [presetInput resignFirstResponder];
                [presetBar setHidden:YES];
                ptzStatus = PTZ_NONE;
            }else {
                [presetBar setHidden:NO];
                [presetInput setText:@""];
                [presetInput becomeFirstResponder];
                ptzStatus = Idx;
            }
			break;
		case PTZ_AUTOPAN:
            if (ptzStatus==PTZ_OSD || ptzStatus==PTZ_PRESET) break;
			if (ptzStatus==Idx) {
				[[streamReceiver ptzController] ptzStop:channel];
				ptzStatus = PTZ_NONE;
			}
			else {
				[[streamReceiver ptzController] ptzAutoPanRun:channel];
				ptzStatus = Idx;
			}
			break;
        case PTZ_PATTERN:
            if (ptzStatus==PTZ_OSD || ptzStatus==PTZ_PRESET) break;
			if (ptzStatus==Idx) {
				[[streamReceiver ptzController] ptzStop:channel];
				ptzStatus = PTZ_NONE;
			}
			else {
				[[streamReceiver ptzController] ptzPattern:channel value:1];
				ptzStatus = Idx;
			}
			break;
		case PTZ_TOUR:
            if (ptzStatus==PTZ_OSD || ptzStatus==PTZ_PRESET) break;
			if (ptzStatus==Idx) {
				[[streamReceiver ptzController] ptzStop:channel];
				ptzStatus = PTZ_NONE;
			}
			else {
				[[streamReceiver ptzController] ptzTour:channel value:1];
				ptzStatus = Idx;
			}
			break;
        case PTZ_OSD:
            if (ptzStatus==PTZ_PRESET) break;
			if (ptzStatus==Idx) {
				[[streamReceiver ptzController] ptzOsdExit:channel];
				ptzStatus = PTZ_NONE;
			}
			else {
				[[streamReceiver ptzController] ptzOsdOpen:channel];
				ptzStatus = Idx;
			}
			break;
		case PTZ_UP:
			[[streamReceiver ptzController] ptzTiltUp:channel];
			break;
		case PTZ_RIGHT:
			[[streamReceiver ptzController] ptzPanRight:channel];
			break;
		case PTZ_DOWN:
			[[streamReceiver ptzController] ptzTiltDown:channel];
			break;
		case PTZ_LEFT:
			[[streamReceiver ptzController] ptzPanLeft:channel];
			break;
		case PTZ_ZOOMIN:
			[[streamReceiver ptzController] ptzZoomIn:channel];
			break;
		case PTZ_ZOOMOUT:
			[[streamReceiver ptzController] ptzZoomOut:channel];
			break;
		case PTZ_FFAR:
			[[streamReceiver ptzController] ptzFocusFar:channel];
			break;
		case PTZ_FNEAR:
			[[streamReceiver ptzController] ptzFocusNear:channel];
			break;
        case PTZ_GO:
            [[streamReceiver ptzController] ptzPresetGo:channel value:presetInput.text.integerValue];
            [self sendPtzCmd:PTZ_PRESET];
            break;
        case PTZ_SET:
            [[streamReceiver ptzController] ptzPresetSet:channel value:presetInput.text.integerValue];
            [self sendPtzCmd:PTZ_PRESET];
            break;
        case PTZ_DELETE:
            [[streamReceiver ptzController] ptzPresetCancel:channel value:presetInput.text.integerValue];
            [self sendPtzCmd:PTZ_PRESET];
            break;
    }
    [self checkPtzButton];
}

- (void)sendPtzStop
{
    NSInteger channel = renderViews[selectWindow].playingChannel;
    if (device.streamType == STREAM_ICATCH) {
        CameraInfo *curInfo = [streamReceiver.cameraList objectAtIndex:channel];
        channel = curInfo.ptzID;
        [streamReceiver ptzController].baudrate = curInfo.baudrate;
        [streamReceiver ptzController].ptzType = curInfo.ptzType;
    }
    [[streamReceiver ptzController] ptzStop:channel];
}

#pragma mark - UI Object Control

- (UIButton *)buttonWithTitle:(NSString *)title
					   target:(id)target
				   downAction:(SEL)downAction
					 upAction:(SEL)upAction
					 fontSize:(CGFloat)fontSize
						  tag:(NSInteger)tag
{
	UIButton *button = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
	
	button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
	
	[button setTitle:title forState:UIControlStateNormal];
	[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	button.titleLabel.font = [UIFont boldSystemFontOfSize:fontSize];
	
	UIImage *bgImage = [IMG_UNSELECT stretchableImageWithLeftCapWidth:8.0 topCapHeight:0.0];
	[button setBackgroundImage:bgImage forState:UIControlStateNormal];
	[button addTarget:target action:downAction forControlEvents:UIControlEventTouchDown];
	[button addTarget:target action:upAction   forControlEvents:UIControlEventTouchUpInside];
	button.backgroundColor = [UIColor clearColor];
	button.tag = tag;
	
	return button;
}

- (void)snapShot
{
    if (m_intDivide == 1) {
        
        UIImage *image = [[renderViews[selectWindow] videoImage] retain];
        //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
        NSString *albumName = [NSString stringWithFormat:@"%@",appDelegate._AppName];
        [mediaLibrary saveImage:image toAlbum:albumName withCompletionBlock:^(NSError *error) {
            if (error!=nil) {
                [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
            }else {
                [self showAlarm:NSLocalizedString(@"MsgSnapOK", nil)];
            }
        }];
        SAVE_FREE(image);
    }else {
        
        UIImage *image;
        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
        NSString *albumName = [NSString stringWithFormat:@"%@",appDelegate._AppName];
        [mediaLibrary saveImage:image toAlbum:albumName withCompletionBlock:^(NSError *error) {
            if (error!=nil) {
                [self showAlarm:NSLocalizedString(@"MsgSaveErr", nil)];
            }else {
                [self showAlarm:NSLocalizedString(@"MsgSnapOK", nil)];
            }
        }];
        
    }
}

- (IBAction)openSearch:(id)sender
{
    if (!self.searchView) {     //Load Search View
        SearchViewController *viewController = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:[NSBundle mainBundle]];
        self.searchView = viewController;
        SAVE_FREE(viewController);
    }
    
    if (streamReceiver.m_intDiskStartTime == 0) {
        [self showAlarm:@"No Video Recording"];
        return;
    }
    searchView.streamReceiver = streamReceiver;
    searchView.device = device;
    searchView.sourceView = self;
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.blnFullScreen = blnFullScreen;
    blnOpenSearch = YES;
    
    [self.navigationController pushViewController:self.searchView animated:YES];
}

- (void)selectView:(NSInteger)vIdx
{
    selectWindow = vIdx;
    videoControl.selectWindow = vIdx;
    streamReceiver.currentPlayCH = 0x1<<selectWindow;
    [self checkValidButtonItems:renderViews[selectWindow].playingChannel];
    
    for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
        if (i == selectWindow) {
            [renderViews[i].mainView.layer setBorderWidth:2.0f];
            [renderViews[i].mainView.layer setBorderColor:[[UIColor EFCOLOR] CGColor]];
        }else {
            [renderViews[i].mainView.layer setBorderWidth:1.0f];
            [renderViews[i].mainView.layer setBorderColor:[[UIColor grayColor] CGColor]];
        }
    }
    if (blnSoundOn) {
        [streamReceiver changeAudioChannel];
    }
    if (m_intDivide==2) {
        [ptzBtn setEnabled:NO];
    }
}

- (IBAction)openPBControlPanel
{
    if (!blnPbControl) {
        [pbControlBtn setImage:[UIImage imageNamed:@"playbackD2.png"] forState:UIControlStateNormal];
    }
    else {
        [pbControlBtn setImage:[UIImage imageNamed:@"playbackD.png"] forState:UIControlStateNormal];
    }
    
    [pbControlView setHidden:blnPbControl];
    
    blnPbControl = !blnPbControl;
}

- (void)checkLayoutItems:(BOOL)isEnable
{
    for (UIButton *lBtn in layoutItems)
        [lBtn setEnabled:isEnable];
    
    if (blnEventMode && blnOpenSearch) {
        [searchBtn setEnabled:NO];
    }
    
    if (device.streamType == STREAM_NVR265) {
//        [btn4Div setEnabled:NO];
        [ptzBtn setEnabled:NO];
        [soundBtn setEnabled:NO];
        [speakBtn setEnabled:NO];
    }
    
//only free version need to limit
//    if (isEnable)
//        [btn4Div setEnabled:!blnPlaybackMode];
}

- (void)showMaskView:(BOOL)visible
{
    [maskView setHidden:!visible];
    visible ? [loadingAct startAnimating] : [loadingAct stopAnimating];
}

#pragma mark - GestureRecognizer Control

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch  //Ignore tap gesture on button or bar
{
    if ([touch.view isKindOfClass:[UISlider class]] ||
        [touch.view isDescendantOfView:scrollPtzBar]) {
        return NO;
    }
    
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    // if the gesture recognizers are on different views, don't allow simultaneous recognition
    if (gestureRecognizer.view != otherGestureRecognizer.view)
        return NO;
    
    // if either of the gesture recognizers is the long press, don't allow simultaneous recognition
    if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] ||
		[otherGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
        return NO;
    
    return YES;
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan) {
        UIView *piece = gesture.view;
        CGPoint locationInView = [gesture locationInView:piece];
        CGPoint locationInSuperview = [gesture locationInView:piece.superview];
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}

- (void)handleTapOnMain:(UITapGestureRecognizer *)gesture
{
    UIView *tapView = [gesture.view.subviews objectAtIndex:0];
    
    if(selectWindow==tapView.tag) {
        
        if (renderViews[selectWindow].lblTitle.hidden) {
            [renderViews[selectWindow].lblTitle setHidden:NO];
            [renderViews[selectWindow].infoLabel setHidden:NO];
        }
        else {
            [renderViews[selectWindow].lblTitle setHidden:YES];
            [renderViews[selectWindow].infoLabel setHidden:YES];
        }
        
        if (blnFullScreen) {
            if (blnPtzPanelOn)
                return;
            
            if (controlBar.hidden) {
                
                if (blnPbControl)
                    [pbControlView setHidden:NO];
                if (blnChPanelOn)
                    [channelPanel setHidden:NO];
                
                [controlBar setHidden:NO];
                [layoutPanel setHidden:NO];
                [self.navigationController setNavigationBarHidden:NO animated:YES];
                
                [self performSelector:@selector(setLayoutHidden) withObject:nil afterDelay:3];
            }else {
                
                if (blnPbControl)
                    [pbControlView setHidden:YES];
                if (blnChPanelOn)
                    [channelPanel setHidden:YES];
                
                [controlBar setHidden:YES];
                [layoutPanel setHidden:YES];
                [self.navigationController setNavigationBarHidden:YES animated:YES];
            }
        }
    }
    
    [self selectView:tapView.tag];
}

- (void)handleLongPressOnMain:(UILongPressGestureRecognizer *)gesture
{
    UIView *tapView = [gesture.view.subviews objectAtIndex:0];
    
    if (gesture.state == UIGestureRecognizerStateBegan)
        blnLongPressEnd = NO;
    
    if (blnLongPressEnd)
        return;
    
    if (renderViews[tapView.tag].playingChannel == EMPTY_CHANNEL)
        return;
    
    if (tapView.tag != selectWindow) {
        
        [self selectView:tapView.tag];
        
        streamReceiver.currentPlayCH = 0x1<<renderViews[selectWindow].playingChannel;
        if (blnSoundOn)     [streamReceiver changeAudioChannel];
    }
    
    if (m_intDivide == 1)
        [self changeViewMode:btn4Div];
    else
        [self changeViewMode:btn1Div];
    
    blnLongPressEnd = YES;
}

- (void)handlePinchOnMain:(UIPinchGestureRecognizer *)gesture
{
    if (m_intDivide == 1) {
        
        CGFloat scale = gesture.scale;
        UIView *piece = gesture.view;
        
        if ([gesture state] == UIGestureRecognizerStateBegan ||
            [gesture state] == UIGestureRecognizerStateChanged) {
            
            // size restriction
            if ( curZoomScale<minZoomScale || curZoomScale>maxZoomScale ) {
                scale = (scale-1)/10 + 1;
            }
            
            // resize image view
            piece.transform = CGAffineTransformScale([piece transform], scale, scale);
            
            curZoomScale = curZoomScale * scale;
        }
        
        if ([gesture state] == UIGestureRecognizerStateEnded ) {
            
            // if the piece size too large or small -> resize it
            
            if ( curZoomScale<minZoomScale ) {
                
                scale = minZoomScale/curZoomScale;
                curZoomScale = minZoomScale;
            }
            else if ( curZoomScale>maxZoomScale ) {
                
                scale = maxZoomScale/curZoomScale;
                curZoomScale = maxZoomScale;
            }
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.1];
            piece.transform = CGAffineTransformScale([piece transform], scale, scale);
            [UIView commitAnimations];
            
            // correct the piece locaton
            
            CGRect newFrame = CGRectMake(piece.frame.origin.x, piece.frame.origin.y, piece.frame.size.width, piece.frame.size.height);
            
            if ( newFrame.size.height <= [piece superview].frame.size.height ) {
                
                newFrame.origin.y = ([piece superview].frame.size.height - newFrame.size.height) / 2;
            }
            if ( newFrame.size.width <= [piece superview].frame.size.width ) {
                
                newFrame.origin.x = ([piece superview].frame.size.width - newFrame.size.width) / 2;
            }
            
            if ( piece.frame.size.width >= [piece superview].frame.size.width ) {
                
                if ( piece.frame.origin.x > 0 ) {
                    newFrame.origin.x = 0;
                }
                else if ( piece.frame.origin.x < [piece superview].frame.size.width - piece.frame.size.width ) {
                    newFrame.origin.x = [piece superview].frame.size.width - piece.frame.size.width;
                }
            }
            if ( piece.frame.size.height >= [piece superview].frame.size.height ) {
                
                if ( piece.frame.origin.y > 0 ) {
                    newFrame.origin.y = 0;
                }
                else if ( piece.frame.origin.y < [piece superview].frame.size.height - piece.frame.size.height ) {
                    newFrame.origin.y = [piece superview].frame.size.height - piece.frame.size.height;
                }
            }
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.1];
            [piece setFrame:newFrame];
            [UIView commitAnimations];
        }
        gesture.scale = 1;
    }
}

- (void)handlePanOnMain:(UIPanGestureRecognizer *)gesture
{
    UIView *piece = gesture.view;
    
    [self adjustAnchorPointForGestureRecognizer:gesture];
    
    if ([gesture state] == UIGestureRecognizerStateBegan ||
        [gesture state] == UIGestureRecognizerStateChanged) {
        
        CGPoint translation = [gesture translationInView:[piece superview]];
        
        // if the piece height < super view height -> keep image at vertial center
        // if the piece width  < super view width  -> keep image at horizontal center (full screen)
        // if the piece not fit the bounds -> reduce the translation
        if ( piece.frame.size.height <= [piece superview].frame.size.height ) {
            
            translation.y = 0;
        }
        else if ( piece.frame.origin.y > 0 || piece.frame.origin.y < [piece superview].frame.size.height - piece.frame.size.height ) {
            
            translation.y = translation.y/4;
        }
        
        if ( piece.frame.size.width <= [piece superview].frame.size.width ) {
            
            translation.x = 0;
        }
        else if ( piece.frame.origin.x > 0 || piece.frame.origin.x < [piece superview].frame.size.width - piece.frame.size.width ) {
            
            translation.x = translation.x/4;
        }
        
        // let the piece to follow the finger
        [piece setCenter:CGPointMake([piece center].x + translation.x, [piece center].y + translation.y)];
    }
    
    if ([gesture state] == UIGestureRecognizerStateEnded ) {
        
        // if the piece not fit the bounds -> relocate the piece to fit the bounds
        
        CGPoint newOrigin = CGPointMake(piece.frame.origin.x, piece.frame.origin.y);
        if ( piece.frame.size.width >= [piece superview].frame.size.width ) {
            
            if ( piece.frame.origin.x > 0 ) {
                newOrigin.x = 0;
            }
            else if ( piece.frame.origin.x < [piece superview].frame.size.width - piece.frame.size.width ) {
                newOrigin.x = [piece superview].frame.size.width - piece.frame.size.width;
            }
        }
        if ( piece.frame.size.height >= [piece superview].frame.size.height ) {
            
            if ( piece.frame.origin.y > 0 ) {
                newOrigin.y = 0;
            }
            else if ( piece.frame.origin.y < [piece superview].frame.size.height - piece.frame.size.height ) {
                newOrigin.y = [piece superview].frame.size.height - piece.frame.size.height;
            }
        }
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.1];
        [piece setFrame:CGRectMake(newOrigin.x, newOrigin.y, piece.frame.size.width, piece.frame.size.height)];
        [UIView commitAnimations];
    }
    
    [gesture setTranslation:CGPointZero inView:[piece superview]];
}

- (void)handleSwipeOnMain:(UISwipeGestureRecognizer *)gesture
{
    if (curZoomScale != 1.0f)
        return;
    
    if (gesture.direction == UISwipeGestureRecognizerDirectionLeft) {
        
        [self switchChannel:NEXT_CH];
	}
	else if (gesture.direction == UISwipeGestureRecognizerDirectionRight) {
        
        [self switchChannel:PREV_CH];
	}
}

- (void)handleTapOnPTZ:(UITapGestureRecognizer *)gesture
{
    
    if(blnFullScreen) {
        
        if (controlBar.hidden) {
            [controlBar setHidden:NO];
            [scrollPtzBar setHidden:NO];
            if (ptzStatus == PTZ_FOCUS) {
                [focusView setHidden:NO];
            }
            [self.navigationController setNavigationBarHidden:NO];
        }else {
            
            [scrollPtzBar setHidden:YES];
            [controlBar setHidden:YES];
            if (ptzStatus == PTZ_FOCUS) {
                [focusView setHidden:YES];
            }
            [self.navigationController setNavigationBarHidden:YES];
        }
    }
}

- (void)handlePinchPTZ:(UIPinchGestureRecognizer *)gesture
{
    CGFloat scale = [gesture scale];
    
    if (scale>1.0 && blnPtzZoomIn==NO) {
        [self sendPtzCmd:PTZ_ZOOMIN];
        blnPtzZoomIn = YES;
        blnPtzZoomOut = NO;
    }
    else if (scale<1.0 && blnPtzZoomOut==NO) {
        [self sendPtzCmd:PTZ_ZOOMOUT];
        blnPtzZoomIn = NO;
        blnPtzZoomOut = YES;
    }
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        
        [self sendPtzStop];
        blnPtzZoomIn = NO;
        blnPtzZoomOut = NO;
    }
}

- (void)handleSwipePTZ:(UISwipeGestureRecognizer *)gesture
{
    if (gesture.direction == UISwipeGestureRecognizerDirectionUp) {
		
		[self sendPtzCmd:PTZ_UP];
	}
	else if (gesture.direction == UISwipeGestureRecognizerDirectionDown) {
		
		[self sendPtzCmd:PTZ_DOWN];
	}
	else if (gesture.direction == UISwipeGestureRecognizerDirectionLeft) {
		
		[self sendPtzCmd:PTZ_LEFT];
	}
	else if (gesture.direction == UISwipeGestureRecognizerDirectionRight) {
		
		[self sendPtzCmd:PTZ_RIGHT];
	}
    
    //20151109 modified by Ray Lin, 因為發現P2/P3 DVR/NVR的ptz太早stop，所以把時間加長
    NSTimeInterval duration = 0.0f;
    if (device.streamType == STREAM_XMS) {
        CameraInfo *curInfo = [streamReceiver.cameraList objectAtIndex:currentIndex];
        if (curInfo.deviceType == XMS_IPCAM) {
            duration = 0.2f;
        }
        else
            duration = 0.5f;
    }
    else
        duration = 0.5f;
    
    [NSTimer scheduledTimerWithTimeInterval:duration
                                     target:self
                                   selector:@selector(sendPtzStop)
                                   userInfo:nil
                                    repeats:NO];
}

- (void)setLayoutHidden
{
    [controlBar setHidden:YES];
    [layoutPanel setHidden:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark - Slider Delegate

- (IBAction)sliderTouchDown:(id)sender
{
    blnSliderTouch = YES;
}

- (IBAction)sliderDragUp:(id)sender
{
    blnSliderTouch = NO;
    focusBar.value = 0;
    [self sendPtzStop];
}

- (IBAction)sliderChange:(id)sender
{
    if (blnSliderTouch) {
        if (fabs(focusBar.value) >= 1.0) {
            if ((focusBar.value-focusNum)>0) {
                
                [self sendPtzCmd:PTZ_FFAR];
            }
            else {
                
                [self sendPtzCmd:PTZ_FNEAR];
            }
            blnSliderTouch = NO;
        }
    }
}

#pragma mark - RecordControl Delegate

- (void)recordStateCallback:(NSString *)msg
{
    if ([msg isEqualToString:REC_STOP]) {
        [self changeRecordMode:recordBtn];
    }
}

#pragma mark - Alert View Delegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	// use "buttonIndex" to decide your action
	switch (buttonIndex) {
		case 0:
			[self.navigationController popViewControllerAnimated:YES];
			break;
		default:
			break;
	}
}

- (void)showAlarm:(NSString *)message
{
    if ( message!=nil ) {
		
		// open an alert with just an OK button, touch "OK" will do nothing
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
	}
}

- (void)showAlarmAndBack:(NSString *)message
{
	if ( message!=nil ) {
		
		// open an alert with just an OK button
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MobileFocus" message:message
													   delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
		
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        [alert performSelectorOnMainThread:@selector(release) withObject:nil waitUntilDone:YES];
	}
}

#pragma mark - Network Detector

- (BOOL)checkNetworkStatus:(Device *)device
{
    BOOL ret = NO;
    
    Reachability *deviceReach = [Reachability reachabilityWithHostName:self.device.ip];
    NetworkStatus netStatus = [deviceReach currentReachabilityStatus];
    
    if (netStatus != NotReachable) {
        ret = YES;
    }
    
    return ret;
}

@end
