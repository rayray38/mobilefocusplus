//
//  XmsVC.m
//  EFViewerPlus
//
//  Created by Nobel on 2015/1/16.
//  Copyright (c) 2015年 EverFocus. All rights reserved.
//

#import "XMSViewController.h"
#import "XMSStreamReceiver.h"
#import "P2StreamReceiver.h"
#import "EzAccessoryView.h"
#import "ExTableHeader.h"

@interface XMSViewController ()
- (void)addStreaming:(NSInteger)chIdx viewIdx:(NSInteger)vIdx;
- (void)deleteStreaming:(NSInteger)chIdx viewIdx:(NSInteger)vIdx;
- (void)disconnectAll;
@end

@implementation XMSViewController
{
    BOOL blnVehSection;     //20151126 add by Ray Lin, 用來判斷是否為Vehicle Section
    BOOL blnSupportTreeView;
}

#pragma mark - View LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Init cameraList
    streamArray = [[NSMutableArray alloc] init];//20160620 ++ by Ray Lin
    ipcamArray = [[NSMutableArray alloc] init];
    dvrArray = [[NSMutableArray alloc] init];
    rtspArray = [[NSMutableArray alloc] init];
    vehicleArray = [[NSMutableArray alloc] init];//20151120 added by Ray Lin, for xms support vehicle tree view test
    channelTreeItems = [[NSMutableArray alloc] init];//20151120 added by Ray Lin, for xms support vehicle tree view test
}

- (void)viewWillAppear:(BOOL)animated
{
    // get rotation information from delegate
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    blnFullScreen = appDelegate.blnFullScreen;
    if(blnFullScreen == NO)
        [self setPortraitLayout];
    else
        [self setLandscapeLayout];
    
    [controlBar setHidden:NO];
    
    //get smooth mode config
    blnSmoothMode = [EZAccessoryView getDetailSettingBy:KEY_SMOOTHMODE];
    DBGLog(@"[LV] SmoothMode:%d",blnSmoothMode);
    
    // RenderViews Layout Initiate
    m_intDivide = 1;
    [self selectView:0];
    
    [layoutPanel setHidden:NO];
    [textSpeed setHidden:YES];
    
    // set ControlBar Items
    NSArray *barItems = dvrLiveItems;
    [controlBar setItems:barItems];
    
    [devTable reloadData];
    
    // Initial
    blnChPanelOn = NO;
    blnPtzPanelOn = NO;
    blnStop = NO;
    blnGetInfo = NO;
    blnSeqOn = NO;
    ptzStatus = 99;
    currentChMask = 0;
    channelBarPage = 4;
    currentChPage = 0;
    totalChannel = 0;
    currentChMask = 0;
    [channelPanel setHidden:YES];
    [ptzPanel setHidden:YES];
    [focusView setHidden:YES];
    tmp2RemoveAry = [[NSMutableArray alloc] init];
    tmpSelectDVR = [[NSMutableArray alloc] init];
    
    // Initiate StreamReceiver and Get Device Info
    if (device && !streamReceiver) {
        
        //Check NetworkStatus
        if (![self checkNetworkStatus:device]) {
            
            [self showAlarmAndBack:NSLocalizedString(@"MsgTimeOut", nil)];
            return;
        }
        
        streamReceiver = [[XMSStreamReceiver alloc] initWithDevice:device];
        DBGLog(@"[LV] Initiate XMS streaming receiver %p",streamReceiver);
        [streamArray addObject:streamReceiver];//201606210 ++ by Ray Lin
        [streamReceiver setDelegate:self];
        
        for (UIBarButtonItem *cBtn in controlBar.items)
            [cBtn setEnabled:NO];
        
        [self checkLayoutItems:NO];
    }
    
    if (self.blnEventMode) {
        if (self.event.dev_type == DEV_IPCAM) {
            [btnSequence setHidden:YES];
            [prevBtn setHidden:YES];
            [nextBtn setHidden:YES];
        }
        
        if (blnOpenSearch && self.blnPlaybackMode) {
            streamReceiver.m_blnPlaybackMode = YES;
            if (!self.blnEventMode_pb) {
                NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
                [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
                [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
                NSString *dateString = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.event.event_time - 5]];   //從event發生的前5秒開始回放
                NSDate *currentDate = [dateFormatter dateFromString:dateString];
                streamReceiver.m_intPlaybackStartTime = [currentDate timeIntervalSince1970];
                //DBGLog(@"[XMS] Event Playback Time : %lu\n[XMS] Event Playback Date : %@\n[XMS] Event Playback String : %@",(unsigned long)streamReceiver.m_intPlaybackStartTime,currentDate,dateString);
            }
        }
    }
    blnPlaybackMode = streamReceiver.m_blnPlaybackMode;
}

- (void)viewDidAppear:(BOOL)animated
{
    [devTable setHidden:NO];
    [loadingAct setCenter:self.view.center];
    [loadingAct setHidden:YES];
    
    // RenderViews Layout Initiate
    [self resetLiveImageViewWithAnimated:YES mode:m_intDivide];
    
    if (blnOpenSearch) {
        
        blnOpenSearch = NO;
        blnPlaybackPause = NO;
        if (!streamReceiver.m_blnPlaybackMode) {
            
            if (m_intDivide==1) {
                [videoControl resumeByCH:selectWindow];
            }else {
                [videoControl resume];
            }
        }else {
            
            DBGLog(@"[XMS] Live -> Playback");
            [self disconnectAll];
            controlBar.items = dvrPbItems;
            [textSpeed setHidden:NO];
            [self checkLayoutItems:NO];
            blnFirstConnect = YES;
            pbSpeed = 1;
            [textSpeed setText:[NSString stringWithFormat:@"%ldX",(long)pbSpeed]];
            
            if (!self.blnEventMode) {
                [playingIndexList removeAllObjects];
                NSIndexPath *targetPath = [NSIndexPath indexPathForRow:intSelectRow inSection:intSelectSection];
                [[devTable delegate] tableView:devTable didSelectRowAtIndexPath:targetPath];
            }
            else {
                if (self.blnEventMode_pb) {
                    self.blnEventMode_pb = NO;
                    [playingIndexList removeAllObjects];
                    NSIndexPath *targetPath = [NSIndexPath indexPathForRow:0 inSection:intSelectSection];
                    [[devTable delegate] tableView:devTable didSelectRowAtIndexPath:targetPath];
                }
                else
                    [NSThread detachNewThreadSelector:@selector(getDeviceInfomation)
                                             toTarget:self
                                           withObject:nil];
            }
        }
    }else {
        
        [NSThread detachNewThreadSelector:@selector(getDeviceInfomation)
                                 toTarget:self
                               withObject:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (blnChPanelOn) { // close channel panel
        [self openChPanel:chBtn];
    }
    if (blnPtzPanelOn) { // close PTZ panel
        [self openPtzPanel:ptzBtn];
    }
    if (blnSeqOn)
        [self passChCmd:btnSequence];
    
    if (blnSoundOn)
        [self openSound];
    
    if (blnSpeakOn)
        [self openSpeak];
    
    if (blnOpenSearch) {
        [videoControl pause];
        return;
    }
    
    if (blnRecording) {
        [self changeRecordMode:recordBtn];
    }
    
    if (blnPbControl) {
        [self openPBControlPanel];
    }
    
    [self disconnectAll];
    [streamReceiver stopStreaming];
    
    blnStop = YES;
    device = nil;
    [tmp2RemoveAry removeAllObjects];
    SAVE_FREE(tmp2RemoveAry);
    [tmpSelectDVR removeAllObjects];
    SAVE_FREE(tmpSelectDVR);
    [streamArray removeObject:streamReceiver];//201606210 ++ by Ray Lin
    [devArray removeAllObjects];
    
    while (blnInfoThread) {
        [NSThread sleepForTimeInterval:0.01f];
    }
    
    [devTable setHidden:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)dealloc
{
    SAVE_FREE(ipcamArray);
    SAVE_FREE(dvrArray);
    SAVE_FREE(rtspArray);
    SAVE_FREE(vehicleArray);
    SAVE_FREE(streamArray);
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View Control

- (IBAction)openSearch:(id)sender
{
    if (!self.blnWillOpenSearch) {
        
        if (streamReceiver.m_intDiskStartTime == 0) {
            [self showAlarm:@"No Video Recording"];
            return;
        }
        
        if (!self.searchView) {     //Load Search View
            SearchViewController *viewController = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:[NSBundle mainBundle]];
            self.searchView = viewController;
            SAVE_FREE(viewController);
        }
        
        searchView.streamReceiver = streamReceiver;
        searchView.device = device;
        searchView.sourceView = self;
        EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.blnFullScreen = blnFullScreen;
        blnOpenSearch = YES;
        if (self.blnEventMode) {
            self.blnEventMode_pb = YES;
        }
        
        [self.navigationController pushViewController:self.searchView animated:YES];
    }
    else {
        DBGLog(@"[XMS] Live -> Playback");
        [self disconnectAll];
        controlBar.items = dvrPbItems;
        [textSpeed setHidden:NO];
        [self checkLayoutItems:NO];
        pbSpeed = 1;
        [textSpeed setText:[NSString stringWithFormat:@"%ldX",(long)pbSpeed]];
        
        [streamReceiver stopStreaming];
        while(blnStreamThread) {                   //Wait for StreamThread Stop
            [NSThread sleepForTimeInterval:0.01f];
        }
        
        [self changeViewMode:btn1Div];
        streamReceiver.m_blnPlaybackMode = YES;
        streamReceiver.m_intPlaybackStartTime = self.event.event_time - 5;    //從event發生的前5秒開始回放
        self.blnWillOpenSearch = NO;
        NSIndexPath *targetPath = [NSIndexPath indexPathForRow:intSelectRow inSection:intSelectSection];
        [[devTable delegate] tableView:devTable didSelectRowAtIndexPath:targetPath];
//        [NSThread detachNewThreadSelector:@selector(startStreaming)
//                                 toTarget:self
//                               withObject:nil];
    }
}

- (void)selectView:(NSInteger)vIdx
{
    [super selectView:vIdx];
    [streamReceiver changeLiveChannel:vIdx];
}

- (IBAction)changeViewMode:(id)sender
{
    UIButton *tmpBtn = (UIButton *)sender;
    
    if (tmpBtn.tag == m_intDivide) return;
    if (currentChannel == 99 || currentChannel == 96) {
        currentChannel = 0;
    }
    
    CameraInfo *curEntry;
    if (streamReceiver.blnDvrChannel || streamReceiver.blnVehChannel) {
        curEntry = [streamReceiver.currentCHList objectAtIndex:currentChannel];
    }
    else
        curEntry = [streamReceiver.cameraList objectAtIndex:currentChannel];
    
    currentChannel = renderViews[selectWindow].playingChannel;
    currentChMask = (unsigned)0xF << (currentChannel/4)*4;
    
    [self resetLiveImageViewWithAnimated:YES mode:tmpBtn.tag];
    if (tmpBtn.tag == 1) {
        if (curEntry.deviceType == XMS_IPCAM || curEntry.deviceType == XMS_RTSP || curEntry.deviceType == XMS_VEHICLE_CH) {
            
            if (renderViews[selectWindow].playingChannel != 99 && playingIndexList.count > 1) {
                
                [self disconnectStreamingByView:curEntry.index-1 with:selectWindow];
            }
            [ptzBtn setEnabled:YES];
        }
        else
        {
            [videoControl pauseByCH:selectWindow];
            [self outputListReset];
            [streamReceiver.outputList replaceObjectAtIndex:currentChannel withObject:[NSString stringWithFormat:@"%ld",(long)selectWindow]];
            streamReceiver.currentPlayCH = (unsigned)0x1<<currentChannel;
            [streamReceiver changeVideoMask:(unsigned)0x1<<currentChannel];
            [videoControl resumeByCH:selectWindow];
            
            [ptzBtn setEnabled:YES];
            [self resetPlayingIndexList];
        }
        
    }else {
        if (curEntry.deviceType == XMS_IPCAM || curEntry.deviceType == XMS_RTSP || curEntry.deviceType == XMS_VEHICLE_CH) {
            [self autoFillMask];
            [ptzBtn setEnabled:NO];
        }
        else
        {
            [videoControl pause];
            currentChannel = currentChannel - currentChannel%4;
            streamReceiver.currentPlayCH = (unsigned)0x1<<currentChannel;
            [self outputListReset];
            for (NSInteger i=0; i<4; i++) {
                [streamReceiver.outputList replaceObjectAtIndex:currentChannel+i withObject:[NSString stringWithFormat:@"%ld",(long)i]];
            }
            [streamReceiver changeVideoMask:currentChMask];
            [videoControl resume];
            
            [ptzBtn setEnabled:NO];
            [self resetPlayingIndexList];
        }
    }
}

- (IBAction)changePlaybackMode:(id)sender
{
    NSInteger btnId = ((UIButton *)sender).tag;
    NSInteger mode;
    
    if (btnId == PLAYBACK_EXIT) {
        
        DBGLog(@"[LV] Playback -> Live");
        
        [self openPBControlPanel];
        if (blnXmsIPcam) {
            [self disconnectAll];
        }
        else
            [streamReceiver stopStreaming];
        
        while(blnStreamThread) {                   //Wait for StreamThread Stop
            [NSThread sleepForTimeInterval:0.01f];
        }

        controlBar.items = dvrLiveItems;
        [textSpeed setHidden:YES];
        [ptzBtn setEnabled:YES];
        [speakBtn setEnabled:YES];
        [self checkLayoutItems:NO];
        [self changeViewMode:btn1Div];
        blnPlaybackMode = streamReceiver.m_blnPlaybackMode = NO;
        blnFirstConnect = YES;
        
        //NSInteger chIdx = streamReceiver.currentCHMask;
        [playingIndexList removeAllObjects];
        NSIndexPath *targetPath = [NSIndexPath indexPathForRow:intSelectRow inSection:intSelectSection];
        [[devTable delegate] tableView:devTable didSelectRowAtIndexPath:targetPath];
        
        return;
    }
    
    switch (btnId) {
            
        case PLAYBACK_PAUSE:
            if (blnPlaybackPause) {
                [pausePlayBtn setImage:IMG_PAUSE forState:UIControlStateNormal];
                mode = PLAYBACK_FORWARD;
                [streamReceiver.videoControl resume];
                pbSpeed = 1;
            }else {
                [pausePlayBtn setImage:IMG_PLAY forState:UIControlStateNormal];
                [fastFwBtn setImage:IMG_FF forState:UIControlStateNormal];
                [fastBwBtn setImage:IMG_FB forState:UIControlStateNormal];
                mode = PLAYBACK_PAUSE;
                [streamReceiver.videoControl pause];
                pbSpeed = 0;
            }
            blnPlaybackPause = !blnPlaybackPause;
            break;
            
        case PLAYBACK_FAST_FORWARD:
        case PLAYBACK_FAST_BACKWARD:
            if (blnPlaybackPause) {
                [pausePlayBtn setImage:IMG_PAUSE forState:UIControlStateNormal];
                [streamReceiver.videoControl resume];
                blnPlaybackPause = NO;
            }
            
            if (btnId == PLAYBACK_FAST_FORWARD) {
                [fastFwBtn setImage:IMG_FF_OVER forState:UIControlStateNormal];
                [fastBwBtn setImage:IMG_FB forState:UIControlStateNormal];
                if (pbSpeed <= 0) {
                    pbSpeed = 1;
                }else {
                    if (pbSpeed < 32) {
                        pbSpeed = pbSpeed*2;
                    }
                }
            }else {
                [fastFwBtn setImage:IMG_FF forState:UIControlStateNormal];
                [fastBwBtn setImage:IMG_FB_OVER forState:UIControlStateNormal];
                if (pbSpeed >= 0) {
                    pbSpeed = -1;
                }else {
                    if (pbSpeed > -32) {
                        pbSpeed = pbSpeed*2;
                    }
                }
            }
            mode = btnId;
            break;
    }
    
    [streamReceiver changePlaybackMode:mode withValue:(log2(labs(pbSpeed))+1)];
    [textSpeed setText:[NSString stringWithFormat:@"%ldX",(long)pbSpeed]];
}

#pragma mark - Channel Control

- (IBAction)passChCmd:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self switchChannel:btn.tag];
}

- (void)switchChannel:(NSInteger)Idx
{
    [self resetLiveImageViewWithAnimated:YES mode:m_intDivide];
    if (Idx<32) {
        
        if (m_intDivide != 1) {
            [self changeViewMode:btn1Div];
            [videoControl pause];
        }
        
        currentChannel = Idx ;
        currentChPage = floor(currentChannel/4);
        
        [videoControl pauseByCH:selectWindow];
        [self outputListReset];
        [streamReceiver.outputList replaceObjectAtIndex:currentChannel withObject:[NSString stringWithFormat:@"%ld",(long)selectWindow]];
        streamReceiver.currentPlayCH = (unsigned)0x1<<currentChannel;
        [streamReceiver changeVideoMask:(unsigned)0x1<<currentChannel];
        streamReceiver.audioSender.auChannel = log2(streamReceiver.currentPlayCH);
        [videoControl resumeByCH:selectWindow];
        [videoControl reset];
        [self resetPlayingIndexList];
    }
    else if (Idx==PREV_CH) { // previous channel
        
        if (m_intDivide == 1) {
            
            NSInteger index = (currentChannel+totalChannel-1)%totalChannel;
            
            while ( ![self checkValidChannel:index] ) {
                
                index = (index+totalChannel-1)%totalChannel;
            }
            [self switchChannel:index];
        }else {
            
            currentChPage = currentChPage - 1;
            if (currentChPage == -1 )
            {
                if (totalChannel%4 == 1) {
                    currentChPage = (totalChannel/4);
                }
                else
                    currentChPage = (totalChannel/4) - 1;
            }
            
            currentChMask = (unsigned)0xF<<(currentChPage*4);
            currentChannel = currentChPage*4;
            [videoControl pause];
            
            [streamReceiver changeVideoMask:currentChMask];
            [self outputListReset];
            for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
                [streamReceiver.outputList replaceObjectAtIndex:currentChannel+i withObject:[NSString stringWithFormat:@"%ld",(long)i]];
                [videoControl resetByCH:i];
            }
            [videoControl resume];
            [self resetPlayingIndexList];
        }
    }
    else if (Idx==NEXT_CH) { // next channel
        
        if (m_intDivide == 1) {
            
            NSInteger index = (currentChannel+1)%totalChannel;
            if(currentChannel + 1 == [streamReceiver iMaxSupportChannel])
            {
                index = 0;
            }
            while ( ![self checkValidChannel:index] ) {
                
                index = (index+1)%totalChannel;
            }
            [self switchChannel:index];
            
        }else {
            
            currentChPage = currentChPage + 1;
            if (currentChPage == totalChannel/4) {
                if (totalChannel%4 != 1) {
                    currentChPage = 0;
                }
            }
            if (currentChPage == totalChannel/4 + 1) {
                currentChPage = 0;
            }
            
            currentChMask = (unsigned)0xF<<(currentChPage*4);
            currentChannel = currentChPage*4;
            [videoControl pause];
            
            [streamReceiver changeVideoMask:currentChMask];
            [self outputListReset];
            for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
                [streamReceiver.outputList replaceObjectAtIndex:currentChannel+i withObject:[NSString stringWithFormat:@"%ld",(long)i]];
                [videoControl resetByCH:i];
            }
            [videoControl resume];
            [self resetPlayingIndexList];
        }
    }
    else if (Idx==SEQUENCE) { // channel sequence
        
        if (!blnSeqOn) { // run sequence
            seqTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(sequenceRun) userInfo:nil repeats:YES];
            [btnSequence setImage:[UIImage imageNamed:@"seq_pressed.png"] forState:UIControlStateNormal];
        }
        else { // stop sequence
            [btnSequence setImage:[UIImage imageNamed:@"seq.png"] forState:UIControlStateNormal];
            [seqTimer invalidate];
        }
        
        blnSeqOn = !blnSeqOn;
    }
}

- (void)sequenceRun
{
    static int counter = 0;
    counter++;
    
    if (counter>5) {
        
        [self switchChannel:NEXT_CH];
        counter=0;
    }
}

- (void)outputListReset
{
    for (NSInteger i=0; i<totalChannel; i++) {
        [streamReceiver.outputList replaceObjectAtIndex:i withObject:[NSString stringWithFormat:@"99"]];
    }
}

- (BOOL)checkValidChannel:(NSInteger)chIdx
{
    NSInteger ret = (0x1<<chIdx)&streamReceiver.validChannel;
    
    return ret!=0;
}

- (void)autoFillMask
{
//    if (self.blnEventMode && !blnFirstConnect)
//        return;
    
    CameraInfo *currentInfo = [playingIndexList lastObject];
    NSMutableArray *currArray = [NSMutableArray new];
    
    switch (currentInfo.deviceType) {
        case XMS_IPCAM:
            currArray = ipcamArray;
            break;
            
        case XMS_RTSP:
            currArray = rtspArray;
            break;
            
        case XMS_VEHICLE_CH:
            currArray = streamReceiver.currentCHList;
            break;
            
        default:
            currArray = streamReceiver.cameraList;
            break;
    }
    
    NSArray *_ary = [NSArray arrayWithObjects:devArray, currentInfo, nil];
    [self autoFillMask:_ary];
    [devTable reloadData];
}

- (void)autoFillMask:(NSArray *)tmpArray
{
    NSMutableArray *chArray = [tmpArray objectAtIndex:0];
    
    for (CameraInfo *tmpInfo in chArray)
    {
        if ([playingIndexList indexOfObject:tmpInfo] == NSNotFound) {  //沒有在播放的camera
            
            if (playingIndexList.count+1 > pow(m_intDivide,2)) //現有view已點滿了
                return;
            
            //找個空的view來add
            for (VideoDisplayView *tmpView in streamReceiver.videoControl.aryDisplayViews)
            {
                if (!tmpView.blnIsPlay) {
                    
                    [self selectView:[streamReceiver.videoControl.aryDisplayViews indexOfObject:tmpView]];
                    [playingIndexList addObject:tmpInfo];
                    [NSThread sleepForTimeInterval:0.1f];
                    [self addStreaming:tmpInfo.index-1 viewIdx:selectWindow];
                    break;
                }
            }
        }
    }
}

#pragma mark - Streaming Control

- (void)getDeviceInfomation
{
    DBGLog(@"[XMS] GetDeviceInfo Start");
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    CameraInfo *currentEntry = [[CameraInfo alloc] init];
    blnInfoThread = YES;
    
    if (!blnGetInfo) {
        
        //20160613 modified by Ray Lin, because loading activity indicator didn't show in performSelectorOnMainThread funtion
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        dispatch_async(mainQueue, ^(){
            [self showMaskView:YES];
        });
        
        // Get Device Information
        if (![streamReceiver getDeviceInfo])
        {
            if (!streamReceiver.errorDesc) {
                streamReceiver.errorDesc = NSLocalizedString(@"MsgInfoErr", nil);
            }
            blnInfoThread = NO;
            [self showAlarmAndBack:streamReceiver.errorDesc];
            return;
        }
        
        blnGetInfo = YES;
        blnFirstConnect = blnXms1stConnect = YES;
        
        if (streamReceiver.m_DiskGMT) {
            device.devTimeZone = [streamReceiver.m_DiskGMT copy];
            DBGLog(@"[XMS] Device TIMEZONE : %@",device.devTimeZone);
        }
        
        //update channel parameter
        CameraInfo *camInfo = [streamReceiver.cameraList firstObject];
        currentChannel = camInfo.index-1;
        
        //update cameraList
        [ipcamArray removeAllObjects];
        [dvrArray removeAllObjects];
        [rtspArray removeAllObjects];
        [vehicleArray removeAllObjects];
        [channelTreeItems removeAllObjects];
        
        for (CameraInfo *tmpEntry in streamReceiver.cameraList) {
            
            switch (tmpEntry.deviceType) {
                    
                case XMS_IPCAM:
                    [ipcamArray addObject:tmpEntry];
                    break;
                    
                case XMS_DVR:
                case XMS_NVR:
                    [dvrArray addObject:tmpEntry];
                    break;
                    
                case XMS_RTSP:
                    [rtspArray addObject:tmpEntry];
                    break;
                    
                case XMS_VEHICLE:
                    [vehicleArray addObject:tmpEntry];
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    for (UIBarButtonItem *cBtn in controlBar.items) {
        [cBtn setEnabled:YES];
    }
    [recordBtn setEnabled:YES];
    
    if (!self.blnEventMode) {
        //update channel parameter
        NSIndexPath *targetPath;
        if (ipcamArray.count) {
            targetPath = [NSIndexPath indexPathForRow:0 inSection:XMS_SECTION_IPCAM];
        }
        else if (dvrArray.count) {
            targetPath = [NSIndexPath indexPathForRow:0 inSection:XMS_SECTION_DVR];
        }
        else if (rtspArray.count) {
            targetPath = [NSIndexPath indexPathForRow:0 inSection:XMS_SECTION_RTSP];
        }
        else {  //vehicle array
            targetPath = [NSIndexPath indexPathForRow:0 inSection:XMS_SECTION_VEHICLE];
        }
        
        [[devTable delegate] tableView:devTable didSelectRowAtIndexPath:targetPath];
    }
    else {
        if (self.blnOpenSearch) {
            //connect playback streaming
            blnPlaybackPause = NO;
            pbSpeed = 1;
        }
        
        blnFirstConnect = YES;
        blnXms1stConnect = YES;
        
        switch (self.event.dev_type) {
                
            case DEV_IPCAM:
                intSelectSection = 0;
                for (int idx=0; idx<ipcamArray.count; idx++) {
                    CameraInfo *tmpDev = [ipcamArray objectAtIndex:idx];
                    if ([tmpDev.title isEqualToString:self.device.name]) {
                        intSelectRow = idx;
                        break;
                    }
                }
                break;
                
            case DEV_DVR:
                intSelectSection = 1;
                for (int idx=0; idx<streamReceiver.channelList.count; idx++) {
                    CameraInfo *tmpDev = [streamReceiver.channelList objectAtIndex:idx];
                    if (tmpDev.ptzID == self.event.dev_eid) {
                        intSelectRow = idx;
                        break;
                    }
                }
                break;
                
            default:
                break;
        }
        
        NSIndexPath *targetPath = [NSIndexPath indexPathForRow:intSelectRow inSection:intSelectSection];
        [[devTable delegate] tableView:devTable didSelectRowAtIndexPath:targetPath];
    }
    
    [devTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    
    dispatch_queue_t mainQueue = dispatch_get_main_queue();
    dispatch_async(mainQueue, ^(){
        [self showMaskView:NO];
    });
    
    blnInfoThread = NO;
    [currentEntry release];
    [pool release];
    DBGLog(@"[XMS] GetDeviceInfo Stop");
}

- (void)startStreaming
{
    DBGLog(@"[XMS] Streaming thread start");
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    blnStreamThread = YES;
    
    if (streamReceiver.m_blnPlaybackMode) {
        
        if ( ![streamReceiver startPlaybackStreaming:currentIndex] ) {
            [self showAlarm:[streamReceiver errorDesc]];
            streamReceiver.errorDesc = nil;
        }
    }else {
        if ( ![streamReceiver startLiveStreaming:currentIndex] ) {
            [self showAlarm:[streamReceiver errorDesc]];
            streamReceiver.errorDesc = nil;
        }
    }
    
    blnStreamThread = NO;
    [pool release];
    DBGLog(@"[XMS] Streaming thread stop");
}

- (void)addStreaming:(NSInteger)chIdx viewIdx:(NSInteger)vIdx
{
    [streamReceiver.outputList replaceObjectAtIndex:chIdx withObject:[NSString stringWithFormat:@"%ld",(long)vIdx]];
    DBGLog(@"[XMS] Add CH:%ld View:%ld",(long)chIdx,(long)vIdx);
    
    if (streamReceiver.blnDvrChannel) {
        if (blnXms1stConnect) {
            
            currentIndex = 0;
            currentIndex |= 0x1<<chIdx;
            //currentPlayingMask = chIdx;
            if (videoControl != nil ) {
                
                streamReceiver.videoControl = videoControl;
                [streamReceiver.videoControl startByCH:vIdx];
                [streamReceiver.videoControl setBlnPSLimit:NO];
                
                streamReceiver.blnEventMode = self.blnEventMode;
                [streamReceiver.event release];
                streamReceiver.event = [self.event retain];
            }else {
                
                DBGLog(@"[XMS] VideoControl loss");
                return;
            }
            
            blnXms1stConnect = blnFirstConnect = NO;
            [NSThread detachNewThreadSelector:@selector(startStreaming)
                                     toTarget:self
                                   withObject:nil];
        } else {
            
            while (!streamReceiver.blnReceivingStream) {
                if (streamReceiver.blnConnectionClear) {
                    [self showAlarm:@"Connection Failed"];
                    return;
                }
                [NSThread sleepForTimeInterval:0.01f];
            }
            
            currentIndex |= 0x1<<chIdx;
            [streamReceiver.videoControl startByCH:vIdx];
            [streamReceiver changeVideoMask:currentIndex];
        }
    }
    else {
        currentIndex = chIdx;
        blnXms1stConnect = blnFirstConnect = NO;
        
        if (videoControl != nil ) {
            
            streamReceiver.videoControl = videoControl;
            [streamReceiver.videoControl startByCH:vIdx];
            [streamReceiver.videoControl setBlnPSLimit:NO];
            
            streamReceiver.blnEventMode = self.blnEventMode;
            [streamReceiver.event release];
            streamReceiver.event = [self.event retain];
        }else {
            
            DBGLog(@"[XMS] VideoControl loss");
            return;
        }
        
        [NSThread detachNewThreadSelector:@selector(startStreaming)
                                 toTarget:self
                               withObject:nil];
    }
    
    [streamReceiver.videoControl setPlayingChannelByCH:vIdx Playing:chIdx];
}

- (void)deleteStreaming:(NSInteger)chIdx viewIdx:(NSInteger)vIdx
{
    DBGLog(@"[XMS] deleteStreaming CH:%ld View:%ld",(long)chIdx,(long)vIdx);
    
    CameraInfo *tmpEntry = [playingIndexList lastObject];
    if (tmpEntry.deviceType  != XMS_CHANNEL) {
        [streamReceiver stopStreamingByView:vIdx];
        [streamReceiver.videoControl stopByCH:vIdx];
    }
    else
    {
        if (blnXms1stConnect) {
            [streamReceiver stopStreaming];
        }
        else {
            currentIndex ^= 0x1<<chIdx;
            
            if (streamReceiver.blnReceivingStream) {
                [streamReceiver changeVideoMask:currentIndex];
            }
            
            [streamReceiver.videoControl stopByCH:vIdx];
        }
    }
    
    [streamReceiver.outputList replaceObjectAtIndex:chIdx withObject:[NSString stringWithFormat:@"99"]];
    
    [devTable reloadData];
}

- (void)closeView:(NSInteger)chIdx with:(NSInteger)vIdx
{
    DBGLog(@"[XMS] Disconnect CH:%ld View:%ld",(long)chIdx,(long)vIdx);
    for (CameraInfo *tmpEntry in playingIndexList)
    {
        //DBGLog(@"[CV] PlayingIndex[%d] CH:%d",i,currentEntry.index-1);
        if (tmpEntry.index-1 == chIdx) {
            [tmpEntry retain];
            //DBGLog(@"[CV] RemoveFromPlayList");
            tmpEntry.status = 0;    //reset status when delete streaming
            [self deleteStreaming:chIdx viewIdx:vIdx];
            [playingIndexList removeObject:tmpEntry];
            break;
        }
    }
}

//20160415 added by Ray Lin, close the background streaming when change view mode from multiview to single view
- (void)disconnectStreamingByView:(NSInteger)chIdx with:(NSInteger)vIdx
{
    for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
        
        if (i != vIdx) {
            if (renderViews[i].playingChannel != 99) {
                [self closeView:renderViews[i].playingChannel with:i];
            }
        }
    }
}

- (IBAction)openSpeak
{
    blnSpeakOn = !blnSpeakOn;
    [self setAudioSessionProp];
    
    if (blnSpeakOn) {
        for (StreamReceiver *tmpStreamReceiver in streamArray) {
            if ([[tmpStreamReceiver.outputList firstObject] integerValue] == selectWindow) {
                
                if (speakControl != nil) {
                    
                    [speakControl close];
                    switch (tmpStreamReceiver.streamType) {
                        case STREAM_NEVIO:
                            speakControl = [speakControl initWithCodecId:AV_CODEC_ID_ADPCM_IMA_WAV srate:8000 bps:4 balign:256 fsize:505];
                            DBGLog(@"[IV] Initiate NEVIO audio sender");
                            break;
                        case STREAM_HDIP:
                        case STREAM_GEMTEK:
                            speakControl = [speakControl initWithCodecId:AV_CODEC_ID_PCM_MULAW srate:8000 bps:8 balign:320 fsize:320];
                            DBGLog(@"[IV] Initiate HDIP audio sender");
                            break;
                    }
                }
                [speakControl setPostProcessAction:self action:@selector(sendingData:)];
                [speakControl start];
                [tmpStreamReceiver openSpeak];
                
                blnSpeakOn = YES;
            }
            [speakBtn setImage:IMG_SPEAK_OVER forState:UIControlStateNormal];
        }
    }else {
        [self closeSpeak];
        [speakBtn setImage:IMG_SPEAK forState:UIControlStateNormal];
    }

}

- (void)closeSpeak
{
    blnSpeakOn = NO;
    [speakControl stop];
    for (StreamReceiver *tmpStreamReceiver in streamArray)  [tmpStreamReceiver closeSpeak];
}

- (void)disconnectAll
{
    for (NSInteger n=0; n<MAX_SUPPORT_DISPLAY; n++) {
        
        if (0x1<<n & videoControl.outputMask)
            [self closeView:renderViews[n].playingChannel with:n];
    }
}


#pragma mark - Tree View Actions

- (NSMutableArray *)listItemsWithCamera:(CameraInfo *)camInfo
{
    NSMutableArray *tmpVehicleArry = [[[NSMutableArray alloc] init] autorelease];
    
    if (camInfo.deviceType == XMS_VEHICLE) {
        
        NSMutableArray *tmpchannelArray = [[[NSMutableArray alloc] init] autorelease];
        for (CameraInfo *channelInfo in streamReceiver.v_channelList) {
            if ([channelInfo.parent isEqualToString:camInfo.child]) {
                [tmpchannelArray addObject:channelInfo];
            }
        }
        for (CameraInfo *tmpInfo in streamReceiver.vehicleList) {
            if ([tmpInfo.path isEqualToString:camInfo.child]) {
                [tmpInfo setSubmersionLevel:1];
                [tmpInfo setParentSelectingItem:camInfo];
                [tmpInfo setAncestorSelectingItems:tmpchannelArray];
                [tmpInfo setNumberOfSubitems:tmpchannelArray.count];
                [tmpVehicleArry addObject:tmpInfo];
            }
        }
    }
    else if (camInfo.deviceType == XMS_DVR || camInfo.deviceType == XMS_NVR || camInfo.deviceType == XMS_VEHICLE_DVR) {
        if (blnVehSection) {
            for (CameraInfo *tmpInfo in streamReceiver.v_channelList) {
                if ([tmpInfo.parent isEqualToString:camInfo.path]) {
                    [tmpInfo setSubmersionLevel:2];
                    [tmpInfo setParentSelectingItem:camInfo];
                    [tmpInfo setAncestorSelectingItems:[NSMutableArray array]];
                    [tmpInfo setNumberOfSubitems:0];
                    [tmpVehicleArry addObject:tmpInfo];
                }
            }
        }
        else {
            for (CameraInfo *tmpInfo in streamReceiver.channelList) {
                if ([camInfo.relations containsObject:tmpInfo.path]) {
                    [tmpInfo setSubmersionLevel:2];
                    [tmpInfo setParentSelectingItem:camInfo];
                    [tmpInfo setAncestorSelectingItems:[NSMutableArray array]];
                    [tmpInfo setNumberOfSubitems:0];
                    [tmpVehicleArry addObject:tmpInfo];
                }
            }
        }
    }
    else {
        
    }
    
    return tmpVehicleArry;
}

- (void)iconButtonAction:(ExTableViewCell *)cell
{
    blnFirstConnect = NO;
    [cell.iconButton setSelected:!cell.iconButton.selected];
    cell.cameraItem.selected = cell.iconButton.selected;
}

- (void)selectingItemsToDelete:(CameraInfo *)selItems saveToArray:(NSMutableArray *)deleteSelectingItems
{
    for (CameraInfo *obj in selItems.ancestorSelectingItems) {
        [self selectingItemsToDelete:obj saveToArray:deleteSelectingItems];
    }
    
    [deleteSelectingItems addObject:selItems];
}

- (NSMutableArray *)removeIndexPathForTreeItems:(NSMutableArray *)treeItemsToRemove inSection:(NSInteger)section
{
    NSMutableArray *result = [NSMutableArray array];
    
    for (NSInteger i = 0; i < [devTable numberOfRowsInSection:section]; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        CameraInfo *info = [tmpTreeArray_copy objectAtIndex:indexPath.row];
        
        for (CameraInfo *tmpCamItem in treeItemsToRemove) {
            if ([info isEqualToSelectingItem:tmpCamItem]) {
                tmpCamItem.selected = NO;
                [result addObject:indexPath];
            }
        }
    }
    
    return result;
}

- (void)toolBarControl
{
    BOOL hidden = blnXmsIPcam;
    //[channelPanel setHidden:hidden];
    [btnSequence setHidden:hidden];
    [prevBtn setHidden:hidden];
    [nextBtn setHidden:hidden];
}

- (void)resetPlayingIndexList
{
    NSUInteger mask = m_intDivide==1 ? (unsigned)0x1<<currentChannel : currentChMask;
    NSMutableArray *playingAry = [NSMutableArray array];
    
    for (int i=0; i<streamReceiver.currentCHList.count; i++) {
        if (0x1<<i & mask) {
            
            [playingAry addObject:[streamReceiver.currentCHList objectAtIndex:i]];
        }
    }
    
    playingIndexList = [[NSMutableArray arrayWithArray:playingAry] retain];
    [devTable reloadData];
}

#pragma mark - GestureRecognizer Control

- (void)handleSwipeOnMain:(UISwipeGestureRecognizer *)gesture
{
    //20160607 added by Ray Lin, for DVR/NVR swipe gesture in XMS
    CameraInfo *curDevice = [playingIndexList lastObject];
    if (curDevice.deviceType == XMS_CHANNEL) {
        if (gesture.direction == UISwipeGestureRecognizerDirectionLeft) {
            
            [self switchChannel:NEXT_CH];
        }
        else if (gesture.direction == UISwipeGestureRecognizerDirectionRight) {
            
            [self switchChannel:PREV_CH];
        };
    }
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 20;
    switch (section) {
        case XMS_SECTION_IPCAM:
            if ([ipcamArray count] == 0) {
                height = 0;
            }
            break;
        case XMS_SECTION_DVR:
            if ([dvrArray count] == 0) {
                height = 0;
            }
            break;
        case XMS_SECTION_RTSP:
            if ([rtspArray count] == 0) {
                height = 0;
            }
            break;
        case XMS_SECTION_VEHICLE:
            if ([vehicleArray count] == 0) {
                height = 0;
            }
            break;
            
        default:
            break;
    }
    
    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 20)] autorelease];
    [headerView setBackgroundColor:[UIColor BACOLOR]];
    UILabel *lbTitle = [[[UILabel alloc] initWithFrame:headerView.frame] autorelease];
    [lbTitle setBackgroundColor:[UIColor clearColor]];
    [lbTitle setTextColor:[UIColor whiteColor]];
    [lbTitle setFont:[UIFont systemFontOfSize:15]];
    
    switch (section) {
        case XMS_SECTION_IPCAM:
            [lbTitle setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"IPCAM", nil)]];
            break;
        case XMS_SECTION_DVR:
            [lbTitle setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"DVR/NVR", nil)]];
            break;
        case XMS_SECTION_RTSP:
            [lbTitle setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"RTSP", nil)]];
            break;
        case XMS_SECTION_VEHICLE:
            [lbTitle setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"VEHICLE", nil)]];
            break;
            
        default:
            break;
    }
    
    [headerView addSubview:lbTitle];
    
    return headerView;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    switch (section) {
        case XMS_SECTION_IPCAM:
            rows = [ipcamArray count];
            break;
        case XMS_SECTION_DVR:
            rows = [dvrArray count];
            break;
        case XMS_SECTION_RTSP:
            rows = [rtspArray count];
            break;
        case XMS_SECTION_VEHICLE:
            rows = [vehicleArray count];
            break;
            
        default:
            break;
    }
    return rows;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell...
    NSMutableArray *subarry = [[[NSMutableArray alloc] init] autorelease];
    switch (indexPath.section) {
        case XMS_SECTION_IPCAM:
            for (int i=0; i<ipcamArray.count; i++) {
                CameraInfo *currentEntry = [ipcamArray objectAtIndex:i];
                [subarry addObject:currentEntry];
                blnSupportTreeView = NO;
            }
            break;
        case XMS_SECTION_DVR:
            for (int i=0; i<dvrArray.count; i++) {
                CameraInfo *currentEntry = [dvrArray objectAtIndex:i];
                [subarry addObject:currentEntry];
                blnSupportTreeView = YES;
            }
            break;
        case XMS_SECTION_RTSP:
            for (int i=0; i<rtspArray.count; i++) {
                CameraInfo *currentEntry = [rtspArray objectAtIndex:i];
                [subarry addObject:currentEntry];
                blnSupportTreeView = NO;
            }
            break;
        case XMS_SECTION_VEHICLE:
            for (int i=0; i<vehicleArray.count; i++) {
                CameraInfo *currentEntry = [vehicleArray objectAtIndex:i];
                [subarry addObject:currentEntry];
                blnSupportTreeView = YES;
            }
            break;
            
        default:
            break;
    }
    
    CameraInfo *subEntry = [subarry objectAtIndex:indexPath.row];
    
    if (!blnSupportTreeView) {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell setBackgroundColor:[UIColor clearColor]];
            [cell.textLabel setBackgroundColor:[UIColor clearColor]];
            [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
            [cell.textLabel setNumberOfLines:0];
            [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
            [cell.textLabel setTextColor:[UIColor lightTextColor]];
            cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
            
            UIImageView *checkBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addDev1.png"]];
            [checkBtn setFrame:CGRectMake(0, 0, 25, 25)];
            [cell setAccessoryView:checkBtn];
            [checkBtn release];
            [cell.accessoryView setHidden:YES];
        }
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[subEntry.title stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        BOOL bVisible = ([playingIndexList indexOfObject:subEntry] != NSNotFound);
        [cell.accessoryView setHidden:!bVisible];
        
        return  cell;
    }
    else {
        static NSString *CellIdentifier = @"ExTableCell";
        ExTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[[ExTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell setBackgroundColor:[UIColor clearColor]];
            cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
        }
        
        cell.cameraItem = [subEntry retain];
        if (cell.cameraItem.submersionLevel != 2) {
            
            if (blnFirstConnect) {
                cell.iconButton.selected = NO;
            }
            else {
                if (cell.cameraItem.selected) {
                    cell.iconButton.selected = YES;
                }
                else
                    cell.iconButton.selected = NO;
            }
            
            [cell setAccessoryView:cell.iconButton];
            [cell.accessoryView setHidden:NO];
        }
        else {
            
            UIImageView *checkBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addDev1.png"]];
            [checkBtn setFrame:CGRectMake(0, 0, 23, 23)];
            [cell setAccessoryView:checkBtn];
            [checkBtn release];
            
            BOOL bVisible = ([playingIndexList indexOfObject:subEntry] != NSNotFound);
            [cell.accessoryView setHidden:!bVisible];
        }
        
        [cell setLevel:[subEntry submersionLevel]];
        [cell setTitle:[NSString stringWithFormat:@"%@",[subEntry.title stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (streamReceiver.cameraList.count == 0) {
        [self showAlarmAndBack:NSLocalizedString(@"MsgNoDevice", nil)];
        return;
    }
    
    if (blnSeqOn)
        [self passChCmd:btnSequence];
    
    ExTableViewCell *cell = nil;
    cell = (ExTableViewCell *)[devTable cellForRowAtIndexPath:indexPath];
    
    intSelectSection = indexPath.section;
    intSelectRow = indexPath.row;
    
    CameraInfo *currentEntry = [[[CameraInfo alloc] init] autorelease];
    NSMutableArray *tmpTreeArray = [[NSMutableArray alloc] init];
    //CameraInfo *currentEntry = [streamReceiver.cameraList objectAtIndex:indexPath.row];
    
    switch (intSelectSection) {
        case XMS_SECTION_IPCAM:
            
            blnXmsIPcam = YES;
            blnVehSection = NO;
            blnSupportTreeView = NO;
            
            if (ipcamArray.count > 0) {
                
                if ((self.blnEventMode || self.blnEventMode_pb) && blnFirstConnect) {
                    BOOL ret = NO;
                    for (CameraInfo *tmpCam in streamReceiver.cameraList) {
                        if ([tmpCam.title isEqualToString:self.device.name]) {
                            currentEntry = tmpCam;
                            ret = YES;
                        }
                    }
                    
                    if (!ret) {
                        [self showAlarmAndBack:NSLocalizedString(@"MsgDeviceErr", nil)];
                        DBGLog(@"[XMS] Device not found in XMS camera list");
                        return;
                    }
                }
                else
                    currentEntry = [ipcamArray objectAtIndex:intSelectRow];
            }
            else
            {
                [self showAlarm:@"No IP camera in camera list"];
                [devTable reloadData];
                return;
            }
            
            break;
        case XMS_SECTION_DVR:
            
            tmpTreeArray = dvrArray;
            blnXmsIPcam = NO;
            blnVehSection = NO;
            blnSupportTreeView = YES;
            
            if ((self.blnEventMode || self.blnEventMode_pb) && blnFirstConnect) {
                BOOL ret = NO;
                for (CameraInfo *tmpCam in streamReceiver.channelList) {
                    if (tmpCam.ptzID == self.event.dev_eid) {
                        currentEntry = tmpCam;
                        ret = YES;
                    }
                }
                
                if (!ret) {
                    [self showAlarmAndBack:NSLocalizedString(@"MsgDeviceErr", nil)];
                    DBGLog(@"[XMS] Device not found in XMS camera list");
                    return;
                }
            }
            else if (cell != nil && cell.cameraItem.submersionLevel == 0) {
                currentEntry = [dvrArray objectAtIndex:intSelectRow];
            }
            else if (channelTreeItems.count>0 && cell != nil) {
                currentEntry = [channelTreeItems objectAtIndex:[channelTreeItems indexOfObject:cell.cameraItem]];
            }
            else
                currentEntry = [streamReceiver.channelList objectAtIndex:intSelectRow];
            
            break;
        case XMS_SECTION_RTSP:
            
            blnXmsIPcam = YES;
            blnVehSection = NO;
            blnSupportTreeView = NO;
            
            if ((self.blnEventMode || self.blnEventMode_pb) && blnFirstConnect) {
                BOOL ret = NO;
                for (CameraInfo *tmpCam in streamReceiver.cameraList) {
                    if ([tmpCam.title isEqualToString:self.device.name]) {
                        currentEntry = tmpCam;
                        ret = YES;
                    }
                }
                
                if (!ret) {
                    [self showAlarmAndBack:NSLocalizedString(@"MsgDeviceErr", nil)];
                    DBGLog(@"[XMS] Device not found in XMS camera list");
                    return;
                }
            }
            else
                currentEntry = [rtspArray objectAtIndex:intSelectRow];
            
            break;
        case XMS_SECTION_VEHICLE:
            
            tmpTreeArray = vehicleArray;
            blnXmsIPcam = YES;
            blnVehSection = YES;
            blnSupportTreeView = YES;
            
            if ((self.blnEventMode || self.blnEventMode_pb) && blnFirstConnect) {
                BOOL ret = NO;
                for (CameraInfo *tmpCam in streamReceiver.cameraList) {
                    if ([tmpCam.title isEqualToString:self.device.name]) {
                        currentEntry = tmpCam;
                        ret = YES;
                    }
                }
                
                if (!ret) {
                    [self showAlarmAndBack:NSLocalizedString(@"MsgDeviceErr", nil)];
                    DBGLog(@"[XMS] Device not found in XMS camera list");
                    return;
                }
            }
            else
                currentEntry = [vehicleArray objectAtIndex:intSelectRow];
            
            break;
            
        default:
            break;
    }
    
    //設定TreeView的展開或是收縮
    if (blnSupportTreeView && !blnInfoThread && (cell != nil)) {
        
        NSMutableArray *insertIndexPaths = [NSMutableArray array];
        NSMutableArray *insertselectingItems = [self listItemsWithCamera:cell.cameraItem];
        tmpTreeArray_copy = [[[NSMutableArray alloc] initWithArray:tmpTreeArray] autorelease];
        NSMutableArray *removeIndexPaths = [NSMutableArray array];
        NSMutableArray *treeItemsToRemove = [NSMutableArray array];
        
        //20160605 added by Ray Lin, close the channels, avoid expanding two DVR/Vehicle's channels at the same time.
        for (CameraInfo *tmpDVR in tmpSelectDVR) {
            
            if (intSelectSection != lastSelectSection) {
                [self showAlarm:@"Please collapse the current selected DVR/Vehicle before expanding another one."];
                return;
            }
            
            if (![tmpDVR isEqualToSelectingItem:currentEntry] && currentEntry.submersionLevel == 0) {
                
                removeIndexPaths = [self removeIndexPathForTreeItems:tmp2RemoveAry inSection:lastSelectSection];
                
                for (CameraInfo *rmTreeItem in tmp2RemoveAry) {
                    [tmpTreeArray removeObject:rmTreeItem];
                    
                    for (CameraInfo *tmp3TreeItem in channelTreeItems) {
                        if ([tmp3TreeItem isEqualToSelectingItem:rmTreeItem]) {
                            //DBGLog(@"[XMS] Remove Cell:%@", tmp3TreeItem.title);
                            [channelTreeItems removeObject:rmTreeItem];
                            break;
                        }
                    }
                }
                
                NSIndexPath *indexPth = [NSIndexPath indexPathForRow:lastSelectRow inSection:lastSelectSection];
                ExTableViewCell *lastSelectCell = (ExTableViewCell *)[devTable cellForRowAtIndexPath:indexPth];
                tmpDVR.selected = NO;
                lastSelectCell.iconButton.selected = tmpDVR.selected;
                [devTable deleteRowsAtIndexPaths:removeIndexPaths withRowAnimation:UITableViewRowAnimationFade];
                [tmpSelectDVR removeAllObjects];
                [tmp2RemoveAry removeAllObjects];
                [treeItemsToRemove removeAllObjects];
                [removeIndexPaths removeAllObjects];
            }
        }
        
        NSInteger insertTreeItemIndex = [tmpTreeArray indexOfObject:cell.cameraItem];
        
        if (insertTreeItemIndex != NSNotFound) {
            
            for (CameraInfo *tmpTreeItem in insertselectingItems) {
                
                [tmpTreeItem setParentSelectingItem:cell.cameraItem];
                
                [cell.cameraItem.ancestorSelectingItems removeAllObjects];
                [cell.cameraItem.ancestorSelectingItems addObjectsFromArray:insertselectingItems];
                
                insertTreeItemIndex++;
                
                BOOL contains = NO;
                
                for (CameraInfo *tmp2TreeItem in tmpTreeArray) {
                    
                    if ([tmp2TreeItem isEqualToSelectingItem:tmpTreeItem]) {
                        contains = YES;
                        
                        [self selectingItemsToDelete:tmp2TreeItem saveToArray:treeItemsToRemove];
                        
                        removeIndexPaths = [self removeIndexPathForTreeItems:(NSMutableArray *)treeItemsToRemove inSection:intSelectSection];
                    }
                }
                
                for (CameraInfo *tmp2TreeItem in treeItemsToRemove) {
                    [tmpTreeArray removeObject:tmp2TreeItem];
                    
                    for (CameraInfo *tmp3TreeItem in channelTreeItems) {
                        if ([tmp3TreeItem isEqualToSelectingItem:tmp2TreeItem]) {
                            //DBGLog(@"[XMS] Remove Cell:%@", tmp3TreeItem.title);
                            [channelTreeItems removeObject:tmp2TreeItem];
                            [tmp2RemoveAry removeObject:tmp2TreeItem];
                            break;
                        }
                    }
                }
                
                if (!contains) {
                    [tmpTreeItem setSubmersionLevel:tmpTreeItem.submersionLevel];
                    
                    [tmpTreeArray insertObject:tmpTreeItem atIndex:insertTreeItemIndex];
                    
                    if (tmpTreeItem.deviceType == XMS_CHANNEL || tmpTreeItem.deviceType == XMS_VEHICLE_CH) {
                        [channelTreeItems addObject:tmpTreeItem];
                    }
                    
                    [tmp2RemoveAry addObject:tmpTreeItem];
                    
                    NSIndexPath *indexPth = [NSIndexPath indexPathForRow:insertTreeItemIndex inSection:intSelectSection];
                    [insertIndexPaths addObject:indexPth];
                }
            }
            
            if (cell.cameraItem.submersionLevel != 2) {
                [self iconButtonAction:cell];
            }
            
            if (currentEntry.submersionLevel != 2 && [insertIndexPaths count]==0 && [removeIndexPaths count]==0) {
                return;
            }
            
            if ([insertIndexPaths count]) {
                
                if (currentEntry.submersionLevel == 0) {
                    
                    lastSelectSection = intSelectSection;
                    lastSelectRow = intSelectRow;
                    [tmpSelectDVR addObject:currentEntry];
                }
                [devTable insertRowsAtIndexPaths:insertIndexPaths withRowAnimation:UITableViewRowAnimationFade];
                return;
            }
            
            if ([removeIndexPaths count]) {
                
                if (currentEntry.submersionLevel == 0) {
                    [tmpSelectDVR removeObject:currentEntry];
                }
                [devTable deleteRowsAtIndexPaths:removeIndexPaths withRowAnimation:UITableViewRowAnimationFade];
                return;
            }
        }
    }
    
    //把tmpInfo.relations設給streamReceiver.currentCHList
    if (currentEntry.deviceType == XMS_CHANNEL || currentEntry.deviceType == XMS_DVR || currentEntry.deviceType == XMS_NVR) {
        if (channelTreeItems.count == 0) {
            NSMutableArray *tmpChAry = [[NSMutableArray alloc] init];
            for (CameraInfo *chInfo in streamReceiver.channelList) {
                if ([currentEntry.relations containsObject:chInfo.path]) {
                    [tmpChAry addObject:chInfo];
                }
            }
            streamReceiver.currentCHList = [NSMutableArray arrayWithArray:tmpChAry];
            [tmpChAry release];
        }
        else
            streamReceiver.currentCHList = [NSMutableArray arrayWithArray:channelTreeItems];
    }
    else if (currentEntry.deviceType == XMS_VEHICLE) {
        NSMutableArray *tmpChAry = [[NSMutableArray alloc] init];
        for (CameraInfo *v_chInfo in streamReceiver.v_channelList) {
            if ([v_chInfo.parent isEqual:currentEntry.child]) {
                [tmpChAry addObject:v_chInfo];
            }
        }
        streamReceiver.currentCHList = [NSMutableArray arrayWithArray:tmpChAry];
        [tmpChAry release];
    }
    else if (currentEntry.deviceType == XMS_VEHICLE_CH) {
        streamReceiver.currentCHList = [NSMutableArray arrayWithArray:channelTreeItems];
    }
    
    [self toolBarControl];
    
    currentChannel = currentIndex = currentEntry.index-1;
    totalChannel = currentEntry.totalChannel;
    
    if (playingIndexList.count == 0) {
        
        if (currentEntry.deviceType == XMS_DVR || currentEntry.deviceType == XMS_NVR || currentEntry.deviceType == XMS_VEHICLE_DVR) {
            currentEntry = [streamReceiver.currentCHList firstObject];
        }
        streamReceiver.blnVehChannel = (blnVehSection ? YES : NO);
        streamReceiver.blnDvrChannel = (blnXmsIPcam ? NO : YES);
        blnXms1stConnect = YES;
        [playingIndexList addObject:currentEntry];
        [devArray addObject:currentEntry];
        [self addStreaming:currentIndex viewIdx:selectWindow];
    }
    else
    {
        CameraInfo *tmpInfo = [[playingIndexList lastObject] retain];
        
        for(CameraInfo *tmpEntry in playingIndexList)
        {
            if ([tmpEntry isEqual:currentEntry]) {
                
                DBGLog(@"[XMS] CH:%ld is Playing",(long)currentIndex);
                return;
            }
        }
        
        if (0x1<<selectWindow & streamReceiver.videoControl.outputMask) {
            
            VideoDisplayView *tmpView = [streamReceiver.videoControl.aryDisplayViews objectAtIndex:selectWindow];
            if (tmpView.playingChannel == EMPTY_CHANNEL) {
                DBGLog(@"[XMS] View:%ld is empty",(long)selectWindow);
                return;
            }
            else if (!blnXmsIPcam && [tmpInfo.ipAddr isEqualToString:currentEntry.ipAddr] && m_intDivide == 2 && tmpView.blnIsPlay) {
                NSString *errDesc = nil;
                errDesc =  NSLocalizedString(@"MsgNowPlaying", nil);
                
                [self showAlarm:errDesc];
                [devTable reloadData];
                return;
            }
            
            [self closeView:tmpView.playingChannel with:selectWindow];
            [devArray removeObject:tmpInfo];
        }
        
        if (playingIndexList.count<pow(m_intDivide,2) || m_intDivide==1) {
            
            if (tmpInfo.deviceType != currentEntry.deviceType || (!blnXmsIPcam && ![tmpInfo.ipAddr isEqualToString:currentEntry.ipAddr]))
            {
                [self disconnectAll];
                [self resetLiveImageViewWithAnimated:YES mode:1];
                blnXms1stConnect = YES;
                [ptzBtn setEnabled:YES];
                for (NSInteger i=0; i<256; i++) {
                    [streamReceiver.outputList replaceObjectAtIndex:i withObject:[NSString stringWithFormat:@"99"]];
                }
                [devArray removeAllObjects];
                [playingIndexList removeAllObjects];
            }
            
            streamReceiver.blnVehChannel = (blnVehSection ? YES : NO);
            streamReceiver.blnDvrChannel = (blnXmsIPcam ? NO : YES);
            
            [playingIndexList addObject:currentEntry];
            [devArray addObject:currentEntry];
            [self addStreaming:currentEntry.index-1 viewIdx:selectWindow];
        }
        
        [tmpInfo release];
    }
    
    [currentEntry release];
    [devTable reloadData];
    
    if (blnChPanelOn)
        [self openChPanel:chBtn];
}

@end
