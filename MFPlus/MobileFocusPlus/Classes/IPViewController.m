//
//  IPViewController.m
//  EFViewerPlus
//
//  Created by James Lee on 2014/1/6.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import "IPViewController.h"
#import "NevioStreamReceiver.h"
#import "CGIIPStreamReceiver.h"
#import "HDIPStreamReceiver.h"
#import "DynaStreamReceiver.h"
#import "GTStreamReceiver.h"
#import "PTStreamReceiver.h"
#import "OnvifStreamReceiver.h"
#import "EzAccessoryView.h"

@implementation IPViewController

#pragma mark - ViewController Management

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)getDeviceInfomatiionNotify:(NSString *)error withDev:(Device *)dev
{
    if (dev.main_stream_uri != nil || dev.streamType == STREAM_ONVIF) {
        [self initStreamingWithDevice:dev];
        [self addStreamingWithViewId:selectWindow];
    }
    else
    {
        [self showAlarmAndBack:error];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    ipLiveItems = [[NSArray alloc] initWithObjects:m_btnCh, space, m_btnPtz, space, m_btnSound, space, m_btnSpeak, space, m_btnClose, nil];
    streamArray = [[NSMutableArray alloc] init];
    [btnSequence setHidden:YES];
    [prevBtn setHidden:YES];
    [nextBtn setHidden:YES];
    [maskView setHidden:YES];
    [channelPanel setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    // get rotation information from delegate
    EFViewerAppDelegate *appDelegate = (EFViewerAppDelegate *)[[UIApplication sharedApplication] delegate];
    blnFullScreen = appDelegate.blnFullScreen;
    if(blnFullScreen == NO) {
        
        [self setPortraitLayout];
    }else {
        
        [self setLandscapeLayout];
    }
    [controlBar setHidden:NO];
    
    //get smooth mode config
    blnSmoothMode = [EZAccessoryView getDetailSettingBy:KEY_SMOOTHMODE];
    NSLog(@"[IV] SmoothMode:%d",blnSmoothMode);
    
    // RenderViews Layout Initiate
    m_intDivide = 1;
    [self selectView:0];
    
    [layoutPanel setHidden:NO];
    [textSpeed setHidden:YES];
    
    // set ControlBar Items
    NSArray *barItems = ipLiveItems;
    [controlBar setItems:barItems];
    
    [devTable reloadData];
    
    // Initial
    blnChPanelOn = NO;
    blnPtzPanelOn = NO;
    blnStop = NO;
    blnGetInfo = NO;
    blnFirstConnect = YES;
    blnSeqOn = NO;
    ptzStatus = 99;
    currentChMask = 0;
    [ptzPanel setHidden:YES];
    [focusView setHidden:YES];
    
    [loadingAct setCenter:self.view.center];
    [loadingAct setHidden:YES];
    
    //[ptzBtn setEnabled:((device.streamType == STREAM_ONVIF) ? NO : YES)];
    [soundBtn setEnabled:((device.streamType == STREAM_ONVIF) ? NO : YES)];
    [speakBtn setEnabled:((device.streamType == STREAM_ONVIF) ? NO : YES)];
}

- (void)viewDidAppear:(BOOL)animated
{
    [devTable setHidden:NO];
    
    // RenderViews Layout Initiate
    [self resetLiveImageViewWithAnimated:YES mode:m_intDivide];
    
#ifndef UILAYOUTMODE
    NSInteger idx = 0;
    for (idx=0; idx<devArray.count; idx++)
    {
        Device *dev = [devArray objectAtIndex:idx];
        if (dev.rowID == device.rowID)
            break;
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:idx inSection:0];
    [[devTable delegate] tableView:devTable didSelectRowAtIndexPath:path];
#endif
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (streamArray.count == 0) {
        device.blnIsPlaying = NO;
    }
    else if (videoControl.outputMask == 0) {   //20151112 modified by Ray Lin, RTSP connection error後返回，畫面卡住的問題
        [self disconnectView];
    }
    else
        [self disconnectAll];
    
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)dealloc
{
    SAVE_FREE(ipLiveItems);
    
    [streamArray removeAllObjects];
    SAVE_FREE(streamArray);
    
    [super dealloc];
}

#pragma mark - View Control

- (IBAction)changeViewMode:(id)sender
{
    UIButton *tmpBtn = (UIButton *)sender;
    
    if (tmpBtn.tag == m_intDivide) return;
    
    [self resetLiveImageViewWithAnimated:YES mode:tmpBtn.tag];
    
    if (tmpBtn.tag == 1) {
        
        if (renderViews[selectWindow].playingChannel != 99) {
            
            [self disconnectStreamingByView:selectWindow];
        }
        //[ptzBtn setEnabled:((device.streamType == STREAM_ONVIF) ? NO : YES)];
    }
    else {
        
        [ptzBtn setEnabled:NO];
    }
}

#pragma mark - Streaming Control

- (BOOL)initStreamingWithDevice:(Device *)dev
{
    BOOL ret = NO;
    StreamReceiver  *tmpStreamReceiver;
    
    switch (dev.streamType) {
            
        case STREAM_NEVIO:
            tmpStreamReceiver = [[NevioStreamReceiver alloc] initWithDevice:dev];
            NSLog(@"[IV] Initiate Nevio streaming receiver");
            break;
        case STREAM_HDIP:
//            tmpStreamReceiver = [[HDIPStreamReceiver alloc] initWithDevice:device];
            tmpStreamReceiver = [[CGIIPStreamReceiver alloc] initWithDevice:dev];
            NSLog(@"[IV] Initiate HDIP streaming receiver");
            break;
        case STREAM_DYNA:
            tmpStreamReceiver = [[DynaStreamReceiver alloc] initWithDevice:dev];
            NSLog(@"[IV] Initiate DYNA streaming receiver");
            break;
        case STREAM_GEMTEK:
            tmpStreamReceiver = [[GTStreamReceiver alloc] initWithDevice:dev];
            NSLog(@"[IV] Initiate GT streaming receiver");
            break;
        case STREAM_AFREEY:
            tmpStreamReceiver = [[PTStreamReceiver alloc] initWithDevice:dev];
            NSLog(@"[IV] Initiate PT streaming receiver");
            break;
        case STREAM_ONVIF:
            tmpStreamReceiver = [[OnvifStreamReceiver alloc] initWithDevice:dev];
            NSLog(@"[IV] Initiate ONVIF streaming receiver");
            break;

    }
    
    if (tmpStreamReceiver != nil) {
        
        [streamArray addObject:tmpStreamReceiver];
        ret = YES;
    }
    return ret;
}

- (void)addStreamingWithViewId:(NSInteger)vIdx
{
    NSInteger tmpCount = streamArray.count;
    StreamReceiver *tmpStreamReceiver = [streamArray objectAtIndex:tmpCount-1];
    VideoDisplayView *tmpView = [videoControl.aryDisplayViews objectAtIndex:selectWindow];
    tmpView.playingChannel = tmpStreamReceiver.deviceId;
    
    if (videoControl != nil) {
        
        tmpStreamReceiver.videoControl = videoControl;
        [tmpStreamReceiver.videoControl startByCH:vIdx];
        [tmpStreamReceiver.outputList replaceObjectAtIndex:0 withObject:[NSString stringWithFormat:@"%ld",(long)selectWindow]];
    }
    
    [NSThread detachNewThreadSelector:@selector(startStreaming)
                             toTarget:self
                           withObject:nil];
}

- (void)startStreaming
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    blnStreamThread = YES;
    NSInteger tmpCount = streamArray.count;
    NSLog(@"[IV] Streaming[%ld] Thread Start",(long)tmpCount-1);
    StreamReceiver *tmpStreamReceiver = [streamArray objectAtIndex:tmpCount-1];
    NSInteger vIdx = [[tmpStreamReceiver.outputList objectAtIndex:0] integerValue];
    [devTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    
    if (blnSoundOn) {
        [self closeSound];
        tmpStreamReceiver.audioControl = audioControl;
        [tmpStreamReceiver openSound];
        blnSoundOn = YES;
    }
    
    if (![tmpStreamReceiver startLiveStreaming:0])
    {
        [tmpStreamReceiver.videoControl stopByCH:vIdx];

        if (streamArray.count > 1)
            [self showAlarm:tmpStreamReceiver.errorDesc];
        else
            [self showAlarmAndBack:tmpStreamReceiver.errorDesc];
    }
    
    for (Device *dev in devArray) {
        
        if (dev.rowID == tmpStreamReceiver.deviceId) {
            dev.blnIsPlaying = NO;
            NSLog(@"[IV] Device %ld disconnect",(long)dev.rowID);
            [playingIndexList removeObject:dev];
            
            [devTable performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
            while (!tmpStreamReceiver.blnConnectionClear) {
                [NSThread sleepForTimeInterval:0.001];
            }
            break;
        }
    }
    
    [streamArray removeObject:tmpStreamReceiver];

    //SAVE_FREE(tmpStreamReceiver);
    if (streamArray.count == 0)
        blnStreamThread = NO;
    
    NSLog(@"[IV] Streaming[%ld] Thread Stop",(long)tmpCount-1);
    [pool release];
}

- (void)closeView:(NSInteger)vIdx
{
    for (StreamReceiver *tmpStreamReceiver in streamArray) {
        
        if ([[tmpStreamReceiver.outputList objectAtIndex:0] integerValue] == vIdx) {
            
            if (streamArray.count == 1) {
                
                if (blnSpeakOn) {
                    [self closeSpeak];
                }
                if (blnSoundOn) {
                    [self closeSound];
                }
            }
            [tmpStreamReceiver stopStreamingByView:vIdx];
            
            break;
        }
    }
}

- (IBAction)disconnectView
{
    NSInteger vIdx = selectWindow;
    [self closeView:vIdx];
}

- (void)disconnectAll
{
    for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
        
        if (0x1<<i & videoControl.outputMask)
            [self closeView:i];
    }
}

//20160415 added by Ray Lin, close the background streaming when change view mode from multiview to single view
- (void)disconnectStreamingByView:(NSInteger)vIdx
{
    for (NSInteger i=0; i<MAX_SUPPORT_DISPLAY; i++) {
        
        if (i != vIdx) {
            if (renderViews[i].playingChannel != 99) {
                
//                [videoControl pauseByCH:i];
                [self closeView:i];
            }
        }
    }
}

- (void)changeChannel:(NSInteger)cIdx
{
    for (StreamReceiver *tmpStreamReceiver in streamArray) {
        if ([[tmpStreamReceiver.outputList objectAtIndex:0] integerValue] == selectWindow) {
            
            if (blnSoundOn && !tmpStreamReceiver.audioOn) {
                
                [self closeSound];
                [self openSound];
            }
            else if (blnSpeakOn && !tmpStreamReceiver.speakOn) {
                
                [self closeSpeak];
                [self openSpeak];
            }
        }
    }
}

- (void)channelSwitch:(NSInteger)cmd
{
}

- (IBAction)openSound
{
    blnSoundOn = !blnSoundOn;
    [self setAudioSessionProp];
    
    if (blnSoundOn) {
        
        for (StreamReceiver *tmpStreamReceiver in streamArray) {
            if ([[tmpStreamReceiver.outputList objectAtIndex:0] integerValue] == selectWindow) {
                
                //NSLog(@"[IV] Open -- dv.AuCtrl:%p, streamRcvr.AuCtrl:%p",detailView.audioControl,tmpStreamReceiver.audioControl);
                if (audioControl!=nil && tmpStreamReceiver.audioControl==nil) {
                    tmpStreamReceiver.audioControl = audioControl;
                }
                [tmpStreamReceiver openSound];
                blnSoundOn = YES;
            }
        }
        [soundBtn setImage:IMG_SOUND_OVER forState:UIControlStateNormal];
    }else {
        
        [self closeSound];
        [soundBtn setImage:IMG_SOUND forState:UIControlStateNormal];
    }
}

- (void)closeSound
{
    for (StreamReceiver *tmpStreamReceiver in streamArray) {
        
        [tmpStreamReceiver closeSound];
        //NSLog(@"[IV] Close -- streamRcvr.AuCtrl:%p",tmpStreamReceiver.audioControl);
    }
    blnSoundOn = NO;
}

- (IBAction)openSpeak
{
    blnSpeakOn = !blnSpeakOn;
    [self setAudioSessionProp];
    
    if (blnSpeakOn) {
        for (StreamReceiver *tmpStreamReceiver in streamArray) {
            if ([[tmpStreamReceiver.outputList objectAtIndex:0] integerValue] == selectWindow) {
                
                if (speakControl != nil) {
                    
                    [speakControl close];
                    switch (tmpStreamReceiver.streamType) {
                        case STREAM_NEVIO:
                            speakControl = [speakControl initWithCodecId:AV_CODEC_ID_ADPCM_IMA_WAV srate:8000 bps:4 balign:256 fsize:505];
                            NSLog(@"[IV] Initiate NEVIO audio sender");
                            break;
                        case STREAM_HDIP:
                        case STREAM_GEMTEK:
                            speakControl = [speakControl initWithCodecId:AV_CODEC_ID_PCM_MULAW srate:8000 bps:8 balign:320 fsize:320];
                            NSLog(@"[IV] Initiate HDIP audio sender");
                            break;
                    }
                }
                [speakControl setPostProcessAction:self action:@selector(sendingData:)];
                [speakControl start];
                [tmpStreamReceiver openSpeak];
                
                blnSpeakOn = YES;
            }
            [speakBtn setImage:IMG_SPEAK_OVER forState:UIControlStateNormal];
        }
    }else {
        [self closeSpeak];
        [speakBtn setImage:IMG_SPEAK forState:UIControlStateNormal];
    }
    
}

- (void)closeSpeak
{
    blnSpeakOn = NO;
    [speakControl stop];
    for (StreamReceiver *tmpStreamReceiver in streamArray)  [tmpStreamReceiver closeSpeak];
}

- (void)sendingData:(NSData *)data
{
    for (StreamReceiver *tmpStreamReceiver in streamArray) {
        if ([[tmpStreamReceiver.outputList objectAtIndex:0] integerValue] == selectWindow)
            [tmpStreamReceiver.audioSender sendData:data];
    }
}

#pragma mark - PTZ Control

- (void)sendPtzCmd:(NSInteger)Idx
{
    NSInteger channel = 0;
    for (StreamReceiver *tmpStreamReceiver in streamArray) {
        if ([[tmpStreamReceiver.outputList objectAtIndex:0] integerValue] == selectWindow)
        {
            switch (Idx) {
                case PTZ_FOCUS:
                    if (ptzStatus==PTZ_OSD || ptzStatus==PTZ_PRESET) break;
                    if (ptzStatus==Idx) {
                        [focusView setHidden:YES];
                        ptzStatus = PTZ_NONE;
                    }else {
                        [focusView setHidden:NO];
                        ptzStatus = Idx;
                    }
                    break;
                case PTZ_IRISON:
                    [[tmpStreamReceiver ptzController] ptzIrisOpen:channel];
                    break;
                case PTZ_IRISOFF:
                    [[tmpStreamReceiver ptzController] ptzIrisClose:channel];
                    break;
                case PTZ_PRESET:
                    if (ptzStatus==PTZ_OSD) break;
                    if (ptzStatus==Idx) {
                        [presetInput resignFirstResponder];
                        [presetBar setHidden:YES];
                        ptzStatus = PTZ_NONE;
                    }else {
                        [presetBar setHidden:NO];
                        [presetInput setText:@""];
                        [presetInput becomeFirstResponder];
                        ptzStatus = Idx;
                    }
                    break;
                case PTZ_AUTOPAN:
                    if (ptzStatus==PTZ_OSD || ptzStatus==PTZ_PRESET) break;
                    if (ptzStatus==Idx) {
                        [[tmpStreamReceiver ptzController] ptzStop:channel];
                        ptzStatus = PTZ_NONE;
                    }
                    else {
                        [[tmpStreamReceiver ptzController] ptzAutoPanRun:channel];
                        ptzStatus = Idx;
                    }
                    break;
                case PTZ_PATTERN:
                    if (ptzStatus==PTZ_OSD || ptzStatus==PTZ_PRESET) break;
                    if (ptzStatus==Idx) {
                        [[tmpStreamReceiver ptzController] ptzStop:channel];
                        ptzStatus = PTZ_NONE;
                    }
                    else {
                        [[tmpStreamReceiver ptzController] ptzPattern:channel value:1];
                        ptzStatus = Idx;
                    }
                    break;
                case PTZ_TOUR:
                    if (ptzStatus==PTZ_OSD || ptzStatus==PTZ_PRESET) break;
                    if (ptzStatus==Idx) {
                        [[tmpStreamReceiver ptzController] ptzStop:channel];
                        ptzStatus = PTZ_NONE;
                    }
                    else {
                        [[tmpStreamReceiver ptzController] ptzTour:channel value:1];
                        ptzStatus = Idx;
                    }
                    break;
                case PTZ_OSD:
                    if (ptzStatus==PTZ_PRESET) break;
                    if (ptzStatus==Idx) {
                        [[tmpStreamReceiver ptzController] ptzOsdExit:channel];
                        ptzStatus = PTZ_NONE;
                    }
                    else {
                        [[tmpStreamReceiver ptzController] ptzOsdOpen:channel];
                        ptzStatus = Idx;
                    }
                    break;
                case PTZ_UP:
                    [[tmpStreamReceiver ptzController] ptzTiltUp:channel];
                    break;
                case PTZ_RIGHT:
                    [[tmpStreamReceiver ptzController] ptzPanRight:channel];
                    break;
                case PTZ_DOWN:
                    [[tmpStreamReceiver ptzController] ptzTiltDown:channel];
                    break;
                case PTZ_LEFT:
                    [[tmpStreamReceiver ptzController] ptzPanLeft:channel];
                    break;
                case PTZ_ZOOMIN:
                    [[tmpStreamReceiver ptzController] ptzZoomIn:channel];
                    break;
                case PTZ_ZOOMOUT:
                    [[tmpStreamReceiver ptzController] ptzZoomOut:channel];
                    break;
                case PTZ_FFAR:
                    [[tmpStreamReceiver ptzController] ptzFocusFar:channel];
                    break;
                case PTZ_FNEAR:
                    [[tmpStreamReceiver ptzController] ptzFocusNear:channel];
                    break;
                case PTZ_GO:
                    [[tmpStreamReceiver ptzController] ptzPresetGo:channel value:presetInput.text.integerValue];
                    [self sendPtzCmd:PTZ_PRESET];
                    break;
                case PTZ_SET:
                    [[tmpStreamReceiver ptzController] ptzPresetSet:channel value:presetInput.text.integerValue];
                    [self sendPtzCmd:PTZ_PRESET];
                    break;
                case PTZ_DELETE:
                    [[tmpStreamReceiver ptzController] ptzPresetCancel:channel value:presetInput.text.integerValue];
                    [self sendPtzCmd:PTZ_PRESET];
                    break;
            }
        }
    }
	
    [self checkPtzButton];
}

- (void)sendPtzStop
{
    NSInteger channel = 0;
    for (StreamReceiver *tmpStreamReceiver in streamArray) {
        if ([[tmpStreamReceiver.outputList objectAtIndex:0] integerValue] == selectWindow) {
            [[tmpStreamReceiver ptzController] ptzStop:channel];
        }
    }
}

#pragma mark - GestureRecognizer Control

- (void)handleSwipeOnMain:(UISwipeGestureRecognizer *)gesture
{
    //Do nothing
}

#pragma mark - Table View Data Source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"CameraList", nil);
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return devArray.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.detailTextLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setTextColor:[UIColor lightTextColor]];
		cell.backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell.png"]] autorelease];
        
        UIImageView *checkBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addDev1.png"]];
        [checkBtn setFrame:CGRectMake(0, 0, 27, 27)];
        [cell setAccessoryView:checkBtn];
        [cell.accessoryView setHidden:YES];
        [checkBtn release];
    }
    
    // Configure the cell...
    Device *dev = [devArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[dev.name stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

    if (dev.rowID != indexPath.row) {
        dev.rowID = indexPath.row;
    }
    
    if (dev.blnIsPlaying) {
        [cell.accessoryView setHidden:NO];
    }else {
        [cell.accessoryView setHidden:YES];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // set background
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor lightTextColor];
    cell.detailTextLabel.textColor = [UIColor lightTextColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *errDesc = nil;
    
    Device *dev = (Device *)[devArray objectAtIndex:indexPath.row];
    if (dev.blnIsPlaying) {
        errDesc =  NSLocalizedString(@"MsgNowPlaying2", nil);
        
        [self showAlarm:errDesc];
        [devTable reloadData];
        return;
    }
    if (0x1<<selectWindow & videoControl.outputMask) {
        
        //20151118 modified by Ray Lin, let user can watch different ipcam's live stream in quad view
        VideoDisplayView *tmpView = [streamReceiver.videoControl.aryDisplayViews objectAtIndex:selectWindow];
        if (tmpView.playingChannel == EMPTY_CHANNEL) {
            NSLog(@"[LV] View:%ld is empty",(long)selectWindow);
            return;
        }
        
        [self disconnectView];
    }
    
    BOOL ret = [self checkNetworkStatus:dev];
    if (!ret) {
        errDesc = NSLocalizedString(@"MsgNoInternet", nil);
        
        [self showAlarm:errDesc];
        [devTable reloadData];
        return;
    }
    
    if (dev.streamType == STREAM_ONVIF)
    {
        OnvifDefine *onvif_Req = [[[OnvifDefine alloc] init] autorelease];
        
        if (dev.type == RTSP) {
            dev.device_service = [dev.ip copy];
            ret = [self initStreamingWithDevice:dev];
            if (!ret) {
                errDesc = NSLocalizedString(@"MsgDeviceErr", nil);
                [self showAlarm:errDesc];
                [devTable reloadData];
                return;
            }
            
            [self addStreamingWithViewId:selectWindow];
        }
        else {
            NSString *str_service = [NSString stringWithFormat:@"http://%@:%ld/%@",dev.ip, (long)dev.port, STR_ONVIF_DEVICE_SERVICE];
            dev.device_service = [str_service copy];
            onvif_Req.dev = dev;
            [onvif_Req onvif_GetDeviceInfomation:self];
        }
    }
    else
    {
        ret = [self initStreamingWithDevice:dev];         // init device
        if (!ret) {
            errDesc = NSLocalizedString(@"MsgDeviceErr", nil);
            
            [self showAlarm:errDesc];
            [devTable reloadData];
            return;
        }
        
        [self addStreamingWithViewId:selectWindow];
    }
    dev.blnIsPlaying = YES;
    
    if (blnChPanelOn)
        [self openChPanel:chBtn];
}

@end
