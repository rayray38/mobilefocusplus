//
//  IPViewController.h
//  EFViewerPlus
//
//  Created by James on 2014/1/6.
//  Copyright (c) 2014年 EverFocus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveViewController.h"
#import "OnvifDefine.h"

@interface IPViewController : LiveViewController
{
    NSArray              *ipLiveItems; //ch, ptz, sound, speak
    NSMutableArray       *streamArray; //save the streamReceivers
}

- (BOOL)initStreamingWithDevice:(Device *)dev;
- (void)addStreamingWithViewId:(NSInteger)vIdx;
- (void)startStreaming;
- (void)closeView:(NSInteger)vIdx;
- (void)closeSound;
- (void)closeSpeak;

@end
