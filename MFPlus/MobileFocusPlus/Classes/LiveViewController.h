//
//  LiveViewController.h
//  EFViewer
//
//  Created by Nobel Hsu on 2010/5/24.
//  Copyright 2010 Everfocus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
#import "EFViewerAppDelegate.h"
#import "StreamReceiver.h"
#import "VideoControl.h"
#import "AudioInput.h"
#import "SearchViewController.h"  
#import "Reachability.h"


#define TOOLBAR_HEIGHT_V    44
#define TOOLBAR_HEIGHT_H	32
#define STATUSBAR_HEIGHT    20
#define CH_PANEL_HEIGHT     48
#define CH_BTN_WIDTH		30
#define CH_BTN_HEIGHT		30
#define CH_BTN_GAP			5
#define PTZ_BTN_NUMBER      8
#define PTZ_BTNPANEL_HEIGHT 48
#define PTZ_BTN_WIDTH       60
#define PTZ_BTN_GAP         10
#define PTZ_FOCUS_HEIGHT    40
#define PTZ_BTN_HEIGHT		30
#define CH_LIST_WIDTH       240

#define IMG_1DIV            [UIImage imageNamed:@"singlech.png"]
#define IMG_1DIVOVER        [UIImage imageNamed:@"singlech_pressed.png"]
#define IMG_4DIV            [UIImage imageNamed:@"multich.png"]
#define IMG_4DIVOVER        [UIImage imageNamed:@"multich_pressed.png"]
#define IMG_UNSELECT        [UIImage imageNamed:@"button.png"]
#define IMG_SELECT          [UIImage imageNamed:@"button_pressed.png"]
#define IMG_CH              [UIImage imageNamed:@"chlist.png"]
#define IMG_CH_OVER         [UIImage imageNamed:@"chlistover.png"]
#define IMG_PTZ             [UIImage imageNamed:@"ptz.png"]
#define IMG_PTZ_OVER        [UIImage imageNamed:@"ptzover.png"]
#define IMG_SOUND           [UIImage imageNamed:@"audio.png"]
#define IMG_SOUND_OVER      [UIImage imageNamed:@"audioover.png"]
#define IMG_SPEAK           [UIImage imageNamed:@"speak.png"]
#define IMG_SPEAK_OVER      [UIImage imageNamed:@"speakover.png"]
#define IMG_PAUSE           [UIImage imageNamed:@"playbackII.png"]
#define IMG_PLAY            [UIImage imageNamed:@"playbackD.png"]
#define IMG_FF              [UIImage imageNamed:@"playbackFF.png"]
#define IMG_FF_OVER         [UIImage imageNamed:@"playbackFF2.png"]
#define IMG_FB              [UIImage imageNamed:@"playbackFB.png"]
#define IMG_FB_OVER         [UIImage imageNamed:@"playbackFB2.png"]
#define IMG_STOP            [UIImage imageNamed:@"playbackO.png"]

#define CH_BTN_INDEX        0
#define PTZ_BTN_INDEX       2
#define FB_BTN_INDEX        4
#define STOP_BTN_INDEX      6
#define PAUSE_BTN_INDEX     8
#define FF_BTN_INDEX        10

#define NEXT_CH             97
#define PREV_CH             98
#define SEQUENCE            99

@interface LiveViewController : UIViewController <UITableViewDataSource,UITableViewDelegate
                                                ,UIGestureRecognizerDelegate, UIScrollViewDelegate
                                                ,StreamDelegate>
{
    //Control Bar 
    IBOutlet UIToolbar          *controlBar;
    IBOutlet UIButton           *chBtn;
    IBOutlet UIButton           *ptzBtn;
    IBOutlet UIButton           *soundBtn;
    IBOutlet UIButton           *speakBtn;
    IBOutlet UIButton           *snapBtn;
    IBOutlet UIButton           *searchBtn;
    IBOutlet UIButton           *recordBtn;
    IBOutlet UIButton           *pbControlBtn;
    IBOutlet UIButton           *fastBwBtn;
    IBOutlet UIButton           *stopBtn;
    IBOutlet UIButton           *pausePlayBtn;
    IBOutlet UIButton           *fastFwBtn;
    IBOutlet UIButton           *closeBtn;
    IBOutlet UIBarButtonItem    *space;
    IBOutlet UIBarButtonItem    *m_btnCh;
    IBOutlet UIBarButtonItem    *m_btnPtz;
    IBOutlet UIBarButtonItem    *m_btnSound;
    IBOutlet UIBarButtonItem    *m_btnSpeak;
    IBOutlet UIBarButtonItem    *m_btnSnap;
    IBOutlet UIBarButtonItem    *m_btnRecord;
    IBOutlet UIBarButtonItem    *m_btnSearch;
    IBOutlet UIBarButtonItem    *m_btnClose;
    IBOutlet UIBarButtonItem    *m_btnPbControl;
    NSArray                     *dvrLiveItems; //ch, ptz, sound, speak, search
    NSArray                     *dvrPbItems;   //ch, fbw, stop, pause, ffw
    NSArray                     *navRightItems; //record, snapshot
    IBOutlet UIView             *maskView;
    IBOutlet UIActivityIndicatorView    *loadingAct;
    IBOutlet UIView             *pbControlView;
    
    //Layout Bar
    IBOutlet UIView             *layoutPanel;
    IBOutlet UIButton           *btn1Div;
    IBOutlet UIButton           *btn4Div;
    IBOutlet UIButton           *btnSequence;
    IBOutlet UIButton           *nextBtn;
    IBOutlet UIButton           *prevBtn;
    IBOutlet UILabel            *textSpeed;
    NSArray                     *layoutItems;
    
    //Channel Panel
    IBOutlet UITableView        *devTable;
    IBOutlet UIView             *channelPanel;
    UIButton                    *channelBtn[MAX_CH_NUMBER];
    IBOutlet UIScrollView       *scrollChBar;
    
    //PTZ Panel
    IBOutlet UIView             *ptzPanel;
    IBOutlet UIScrollView       *scrollPtzBar;
    IBOutlet UIView             *focusView;
    UIButton                    *ptzCtrlBtn[PTZ_BTN_NUMBER];
    IBOutlet UISlider           *focusBar;
    IBOutlet UIImageView        *focusIn;
    IBOutlet UIImageView        *focusOut;
    IBOutlet UIToolbar          *presetBar;
    IBOutlet UITextField        *presetInput;
    
    //VideoDisplayView
    IBOutlet UIImageView        *renderView0;
    IBOutlet UIImageView        *renderView1;
    IBOutlet UIImageView        *renderView2;
    IBOutlet UIImageView        *renderView3;
    
    VideoDisplayView            *renderViews[MAX_SUPPORT_DISPLAY];
    NSMutableArray              *renderArray;
    VideoControl                *videoControl;
    
    RecordControl               *recordControl;
    
    //AudioControl
    AudioOutput                 *audioControl;
    AudioInput                  *speakControl;
    
    //SearchView
    SearchViewController        *searchView;
    
    //Control valuable
    BOOL                        blnFullScreen;
    BOOL                        blnPlaybackMode;
    BOOL                        blnChPanelOn;
    BOOL                        blnPtzPanelOn;
    BOOL                        blnSoundOn;
    BOOL                        blnSpeakOn;
    BOOL                        blnSliderTouch;
    BOOL                        blnGoPreset;
    BOOL                        blnStreamThread;
    BOOL                        blnInfoThread;
    BOOL                        blnGetInfo;
    BOOL                        blnStop;
    BOOL                        blnPtzZoomIn;
    BOOL                        blnPtzZoomOut;
    BOOL                        blnOpenSearch;
    BOOL                        blnPlaybackPause;
    BOOL                        blnFirstConnect;
    BOOL                        blnXms1stConnect;
    BOOL                        blnSeqOn;
    BOOL                        blnLongPressEnd;
    BOOL                        blnRecording;
    BOOL                        blnPbControl;
    BOOL                        blnSmoothMode;
    BOOL                        blnXmsIPcam;
    NSInteger                   selectWindow;
    CGFloat                     curZoomScale;
    CGFloat                     minZoomScale;
    CGFloat                     maxZoomScale;
    NSInteger                   m_intDivide;
    NSInteger                   totalChannel;
    NSUInteger                  currentChannel;
    NSUInteger                  currentChMask;
    NSInteger                   currentChPage;
    NSInteger                   currentIndex;
    NSInteger                   channelBarPage;
    CGFloat                     focusNum;
    NSInteger                   ptzStatus;
    NSInteger                   pbSpeed;
    
    NSTimer                     *seqTimer;
    
    //Device management
    Device                      *device;
    StreamReceiver              *streamReceiver;
    NSMutableArray              *devArray;
    NSMutableArray              *playingIndexList;
    
    //Media library
    EFMediaTool                 *mediaLibrary;
}

@property(nonatomic, assign)  Device                *device;
@property(nonatomic, assign)  EventTable            *event;
@property(nonatomic, assign)  NSMutableArray        *devArray;
@property(nonatomic, retain)  SearchViewController  *searchView;
@property(nonatomic)          BOOL                  blnChSelector;
@property(nonatomic)          BOOL                  blnEventMode;
@property(nonatomic)          BOOL                  blnEventMode_pb;
@property(nonatomic)          BOOL                  blnOpenSearch;
@property(nonatomic)          BOOL                  blnWillOpenSearch;
@property(nonatomic)          BOOL                  blnPlaybackMode;

- (void)setPortraitLayout;
- (void)setLandscapeLayout;
- (IBAction)changeViewMode:(id)sender;
- (void)resetLiveImageViewWithAnimated:(BOOL)animated mode:(NSInteger)n_divide;

- (UIButton *)buttonWithTitle:(NSString *)title
					   target:(id)target
				   downAction:(SEL)downAction
					 upAction:(SEL)upAction
					 fontSize:(CGFloat)fontSize
						  tag:(NSInteger)tag;
- (IBAction)snapShot;

- (IBAction)openChPanel:(id)sender;
- (void)setChannelPanelLayout;
- (void)setScrollChBarLayout;
- (BOOL)checkValidChannel:(NSInteger)chIdx;
- (void)setValidChannelButton;
- (void)checkValidButtonItems:(NSInteger)chIdx;
- (void)checkLiveChannelButton;

- (void)sequenceRun;
- (IBAction)passChCmd:(id)sender;
- (void)switchChannel:(NSInteger)Idx;
- (void)outputListReset;

- (IBAction)openPtzPanel:(id)sender;
- (void)setPtzPanelLayout;
- (IBAction)sliderTouchDown:(id)sender;
- (IBAction)sliderDragUp:(id)sender;
- (IBAction)sliderChange:(id)sender;
- (void)sendPtzCmd:(NSInteger)Idx;
- (IBAction)passPtzCmd:(id)sender;
- (void)sendPtzStop;
- (void)checkPtzButton;

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gesture;
- (void)handleTapOnMain:(UITapGestureRecognizer *)gesture;
- (void)handleLongPressOnMain:(UILongPressGestureRecognizer *)gesture;
- (void)handlePinchOnMain:(UIPinchGestureRecognizer *)gesture;
- (void)handlePanOnMain:(UIPanGestureRecognizer *)gesture;
- (void)handleSwipeOnMain:(UISwipeGestureRecognizer *)gesture;
- (void)handleTapOnPTZ:(UITapGestureRecognizer *)gesture;
- (void)handlePinchPTZ:(UIPinchGestureRecognizer *)gesture;
- (void)handleSwipePTZ:(UISwipeGestureRecognizer *)gesture;
- (void)selectView:(NSInteger)vIdx;

- (void)getDeviceInfomation;
- (void)startStreaming;

- (void)setAudioSessionProp;
- (IBAction)openSound;
- (IBAction)openSpeak;
- (void)sendingData:(NSData *)data;
- (IBAction)changeRecordMode:(id)sender;
- (IBAction)openPBControlPanel;

- (IBAction)openSearch:(id)sender;
- (void)recordStateCallback:(NSString *)msg;

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;
- (void)showAlarm:(NSString *)message;
- (void)showAlarmAndBack:(NSString *)message;

- (BOOL)checkNetworkStatus:(Device *)device;
- (void)EventCallback:(NSString *)msg;
- (void)checkLayoutItems:(BOOL)isEnable;

- (void)showMaskView:(BOOL)visible;

@end
