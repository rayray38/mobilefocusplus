//
//  EFViewerAppDelegate.m
//  EFViewer
//
//  Created by Nobel Hsu on 2010/5/7.
//  Copyright EverFocus 2010. All rights reserved.
//

#import "EFViewerAppDelegate.h"
#import "RootViewController.h"
#import "XMSViewController.h"

#pragma GCC diagnostic ignored "-Wwrite-strings"

@interface EFViewerAppDelegate (Private)

- (void)createEditableCopyOfDatabaseIfNeeded;
- (void)initializeDatabase;
- (BOOL)checkGroupExist;
- (BOOL)checkEventExist;
- (void)customizeAppearance;
- (void)initLagTextField;
- (void)initProperty;
- (void)resetBadgeNumber;
- (void)dismissAlert:(UIAlertView *)alert;
- (void)APNsRegistration;
- (void)sendLocalNotification:(NSDictionary *)payload;
- (void)updateDeviceTokenIfNeeded;

@end


@implementation EFViewerAppDelegate{
    DBControl *dbControl;
    BOOL      blnNotify_lock;
    NSString  *alert_body;
}

@synthesize window;
@synthesize navigationController;
@synthesize dvrs,ipcams,groups,events;
@synthesize blnFullScreen,os_version,version;
@synthesize mediaLibrary,_DeviceToken,_AppName;

+ (EFViewerAppDelegate *)sharedAppDelegate
{
    return (EFViewerAppDelegate *) [UIApplication sharedApplication].delegate;
}

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after app launch
    
    [self APNsRegistration];
	[self createEditableCopyOfDatabaseIfNeeded];
	[self initializeDatabase];
    [self customizeAppearance];
    [self initProperty];
    [self resetBadgeNumber];
    blnNotify_lock = NO;
    
    mediaLibrary = [[EFMediaTool alloc] init];
    
    //20160204 added by Ray Lin, get app Info
    self._AppName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    self.version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    window.rootViewController = navigationController;
    
    [window makeKeyAndVisible];
    [UIApplication sharedApplication].idleTimerDisabled = YES;          // 20120307 add by Nobel Hsu, avoid the device go to sleep
    
    /* 20160118 added by Ray Lin, 處理透過Local Notify而啟動的App事件 */
    if (launchOptions != nil) {
//        RELog(@"======= [EFAD] Received push at didFinishLaunchingWithOptions ========");
//        NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
//        [self application:application didReceiveRemoteNotification:notification];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;          // 20120307 add by Nobel Hsu, avoid the device go to sleep
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;          // 20120307 add by Nobel Hsu, avoid the device go to sleep
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;          // 20120307 add by Nobel Hsu, avoid the device go to sleep
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    //return UIInterfaceOrientationMaskPortrait;
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

#pragma mark -
#pragma mark Memory management

- (void)dealloc {
    
    SAVE_FREE(window);
    SAVE_FREE(navigationController);
    SAVE_FREE(dvrs);
    SAVE_FREE(ipcams);
    SAVE_FREE(groups);
    SAVE_FREE(events);
    SAVE_FREE(dbControl);
    SAVE_FREE(mediaLibrary);
    SAVE_FREE(version);
    SAVE_FREE(_DeviceToken);
    SAVE_FREE(_AppName);
	
	[super dealloc];
}

#pragma mark - UIAppearance

- (void)customizeAppearance
{
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"bar.png"] forBarMetrics:UIBarMetricsDefault];
    [[UIToolbar appearance] setBackgroundImage:[UIImage imageNamed:@"bar.png"] forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setTintColor:[UIColor lightGrayColor]];
    [[UISwitch appearance] setOnTintColor:[UIColor lightGrayColor]];
    [[UISegmentedControl appearance] setTintColor:[UIColor lightGrayColor]];
    
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor lightGrayColor],UITextAttributeTextColor,
                                               [UIColor clearColor],      UITextAttributeTextShadowColor,
                                               [NSValue valueWithUIOffset:UIOffsetMake(-1, 0)], UITextAttributeTextShadowOffset, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    
    os_version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (os_version >= 7.0) {
        
        self.window.tintColor = [UIColor lightTextColor];
        [[UINavigationBar appearance] setBackgroundColor:[UIColor BGCOLOR]];
        [[UIPickerView appearance] setBackgroundColor:[UIColor whiteColor]];
        [[UIActionSheet appearance] setBackgroundColor:[UIColor clearColor]];
        [[UIAlertView appearance] setTintColor:[UIColor EFCOLOR]];
        [[UISwitch appearance] setThumbTintColor:[UIColor blackColor]];
    }else {
        
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:[UIImage imageNamed:@"clearbg.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [[UIBarButtonItem appearance] setBackgroundImage:[UIImage imageNamed:@"clearbg.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [[UIBarButtonItem appearance] setTintColor:[UIColor BGCOLOR]];
        [[UIActionSheet appearance] setActionSheetStyle:UIActionSheetStyleBlackOpaque];
        
        if (os_version >= 6.0)
            [[UISwitch appearance] setThumbTintColor:[UIColor blackColor]];
    }
    //[self initLagTextField];     20150515  hidden by Ray Lin(螢幕旋轉會導致keyboard移位)
}

- (void)initLagTextField
{
    UITextField *lagTxtField = [[UITextField alloc] init];
    [self.window addSubview:lagTxtField];
    [lagTxtField becomeFirstResponder];
    [lagTxtField resignFirstResponder];
    [lagTxtField removeFromSuperview];
}

- (void)resetBadgeNumber
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    DBGLog(@"[EFAD] Reset Badge Number");
}

- (void)dismissAlert:(UIAlertView *)alert
{
    if (alert) {
        [alert dismissWithClickedButtonIndex:[alert cancelButtonIndex] animated:YES];
        [alert release];
    }
}

#pragma mark - Property Init

- (void)initProperty
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    // KEY_SMOOTHMODE
    if (![userDefault objectForKey:KEY_SMOOTHMODE])
        [userDefault setBool:YES forKey:KEY_SMOOTHMODE];
}

#pragma mark - HandleRemoteNotification

// Register for APNs notifications
- (void)APNsRegistration
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if( granted ){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)    //若iOS為較新的版本(支援registerUserNotificationSettings方法)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:
                                                                             UIRemoteNotificationTypeSound |
                                                                             UIRemoteNotificationTypeAlert |
                                                                             UIRemoteNotificationTypeBadge
                                                                             categories:nil]];
        
        //Register to APNs
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else   //若iOS為較舊的版本
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    //Convert Device Token from NSData to NSString
    _DeviceToken = [[[[[deviceToken description]
                       stringByReplacingOccurrencesOfString: @"<" withString: @""]
                      stringByReplacingOccurrencesOfString: @">" withString: @""]
                     stringByReplacingOccurrencesOfString: @" " withString: @""] retain];
    
    //20160607 added by Ray Lin, check device token change or not
    [self updateDeviceTokenIfNeeded];
    
    DBGLog(@"[EFAD] Device Token:%@",_DeviceToken);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    //Error message...
    DBGLog(@"[EFAD] Remote notification error:%@", [error localizedDescription]);
}

//Receive remote notifications, while app is running
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo  // for iOS 6 and before
{
    RELog(@"[EFAD] %@",userInfo);
    
    if (blnNotify_lock) {
        return;
    }
    
    switch (application.applicationState) {
            
        case UIApplicationStateBackground:
            
            RELog(@"[EFAD] Background Received Notification");
            [self handleRemoteNotification:userInfo];
            break;
            
        case UIApplicationStateInactive:
            
            RELog(@"[EFAD] InActive Received Notification");
            [self handleRemoteNotification:userInfo];
            [self startLiveAndPlayBack];
            break;
            
        case UIApplicationStateActive:
            
            RELog(@"[EFAD] Active Received Notification");
            [self handleRemoteNotification:userInfo];
            break;
            
        default:
            break;
    }
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler  // for iOS 7 and later
{
    RELog(@"[EFAD] %@",userInfo);
    
    if (blnNotify_lock) {
        return;
    }

    switch (application.applicationState) {
            
        case UIApplicationStateBackground:
            
            RELog(@"[EFAD] Background Received Notification");
            [self handleRemoteNotification:userInfo];
            break;
            
        case UIApplicationStateInactive:
            
            RELog(@"[EFAD] InActive Received Notification");
            [self startLiveAndPlayBack];
            break;
            
        case UIApplicationStateActive:
            
            RELog(@"[EFAD] Active Received Notification");
            [self handleRemoteNotification:userInfo];
            break;
            
        default:
            break;
    }
    
    //處理完消息，最後一定要調用這個代碼
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)handleRemoteNotification:(NSDictionary *)payload
{
    @synchronized (self) {
        
        SAVE_FREE(evtable);
        evtable = [[EventTable alloc] initWithEvent];
        
        [self parseJSonData:payload];
        
        //收到remote notification後先用event.uuid去資料庫裡比對是否有對應的device.uuid, 有的話存進eventTable
        NSMutableArray *tmpdvrArray = [[[NSMutableArray alloc] init] autorelease];
        [dbControl getDevicesToArray:tmpdvrArray type:DEV_DVR];
        
        for (Device *tmpDev in tmpdvrArray) {
            
            if ([tmpDev.uuid isEqualToString:evtable.source_uuid]) {
                
                evtable.source_ip = tmpDev.ip;
                evtable.source_name = tmpDev.name;
                
                if ([dbControl addEventToTable:evtable table:@"event"]) {
                    [[self.events firstObject] addObject:evtable];
                    NSInteger badge = 1;
                    NSInteger currentBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber;
                    [UIApplication sharedApplication].applicationIconBadgeNumber = badge + currentBadgeNumber;
                    
                    dispatch_queue_t mainQueue = dispatch_get_main_queue();
                    dispatch_async(mainQueue, ^(){
                        [self sendLocalNotification:payload];
                    });
                }
            }
        }
    }
}

- (void)startLiveAndPlayBack
{
    [self resetBadgeNumber];
    blnNotify_lock = YES;

    Device *dev = [[Device alloc] initWithProduct:evtable.dev_type];
    NSMutableArray *tmpdvr = [[[NSMutableArray alloc] init] autorelease];
    [dbControl getDevicesToArray:tmpdvr type:DEV_DVR];
    
    BOOL ret = NO;
    for (Device *tmpDvr in tmpdvr) {
        if ([tmpDvr.ip isEqualToString:evtable.source_ip]) {
            dev.ip = evtable.source_ip;
            dev.name = evtable.dev_name;
            dev.type = XMS_SERIES;
            dev.streamType = STREAM_XMS;
            dev.user = tmpDvr.user;
            dev.password = tmpDvr.password;
            ret = YES;
        }
    }
    
    if (!ret) {
        DBGLog(@"[EFAD] Device not found");
        return;
    }
    
    XMSViewController *viewController = [[XMSViewController alloc] initWithNibName:@"LiveViewController" bundle:[NSBundle mainBundle]];
    
    evtable.blnRead = EF_EVENT_READ;   //已讀
    [dbControl updateEventFromDatabase:evtable table:@"event"];
    
    if (viewController.event != nil) {
        [viewController.event release];
    }
    if (viewController.device != nil) {
        [viewController.device release];
    }
    viewController.event = [evtable retain];
    viewController.device = [dev retain];
    viewController.blnChSelector = NO;
    viewController.blnEventMode = YES;
    viewController.blnWillOpenSearch = YES;
    [navigationController pushViewController:viewController animated:YES];
    SAVE_FREE(viewController);
    blnNotify_lock = NO;
}

#pragma mark - UNUserNotificationCenterDelegate

//iOS10新增：處理前台收到通知的代理方法
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    if ([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        RELog(@"--ios10 官方-前台收到push通知：%@",userInfo);
        [self handleRemoteNotification:userInfo];
    }
    else {
        RELog(@"--ios10 官方-前台收到本地通知:{\n body：%@,\n title:%@,subtitle:%@,badge:%@,\nsound:%@,\nuserinfo:%@\n}",notification.request.content.body,notification.request.content.title,notification.request.content.subtitle, notification.request.content.badge,notification.request.content.sound,userInfo);
    }
    
    completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert);
}

//iOS10新增：處理後台點擊通知的代理方法
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler
{
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    
    if ([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        RELog(@"--ios10 官方-後台收到push通知：%@",userInfo);
        [self handleRemoteNotification:userInfo];
        [self startLiveAndPlayBack];
        
    }
    else {
        RELog(@"--ios10 官方-後台收到本地通知:{\n body：%@,\n title:%@,subtitle:%@,badge:%@,\nsound:%@,\nuserinfo:%@\n}",response.notification.request.content.body,response.notification.request.content.title,response.notification.request.content.subtitle, response.notification.request.content.badge,response.notification.request.content.sound,userInfo);
    }
    
    completionHandler();
}

#pragma mark - NSNotification Center

- (void)sendLocalNotification:(NSDictionary *)payload
{
    RELog(@"[EFAD] Local NSNotify Send");
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"DidReceivedRemoteNotify" //Notification以一個字串(Name)下去辨別
     object:self
     userInfo:payload];
}

#pragma mark - JSON Parser

- (void)parseJSonData:(NSDictionary *)_json
{
    if (!_json) {
        return;
    }
    
    @try {
        NSDictionary *apsInfo = [_json objectForKey:@"aps"];
        if ([apsInfo objectForKey:@"alert"] != NULL)
        {
            alert_body = [[[apsInfo objectForKey:@"alert"] objectForKey:@"body"] copy];
        }
        
        NSDictionary *dataInfo = [_json objectForKey:@"data"];
        if ([dataInfo objectForKey:@"devData"] != NULL)
        {
            evtable.channel = [[[dataInfo objectForKey:@"devData"] objectForKey:@"ch"] integerValue];
            evtable.dev_eid = [[[dataInfo objectForKey:@"devData"] objectForKey:@"eid"] integerValue];
            evtable.dev_ip = [[dataInfo objectForKey:@"devData"] objectForKey:@"ip"];
        }
        if ([dataInfo objectForKey:@"eventInfo"] != NULL)
        {
            NSString *encodeString = [[dataInfo objectForKey:@"eventInfo"] objectForKey:@"devName"];
            NSData *decodeData=[[NSData alloc] initWithBase64Encoding:encodeString];
            //NSData *decodeData = [[NSData alloc] initWithBase64EncodedString:encodeString options:0];
            evtable.dev_name = [[NSString alloc] initWithData:decodeData encoding:NSUTF8StringEncoding];
            
            if ([[[dataInfo objectForKey:@"eventInfo"] objectForKey:@"devType"] isEqualToString:@"DVR"]) {
                evtable.dev_type = DEV_DVR;
            }
            else
                evtable.dev_type = DEV_IPCAM;
            
            NSString *strdate = [[dataInfo objectForKey:@"eventInfo"] objectForKey:@"eventDate"];
            evtable.event_time = [strdate integerValue];
            
            evtable.event_type = [[dataInfo objectForKey:@"eventInfo"] objectForKey:@"eventType"];
        }
        if ([dataInfo objectForKey:@"xmsInfo"] != NULL)
        {
            evtable.source_ip = [[dataInfo objectForKey:@"xmsInfo"] objectForKey:@"ip"];
            evtable.source_port = [[[dataInfo objectForKey:@"xmsInfo"] objectForKey:@"port"] integerValue];
            evtable.source_uuid = [[dataInfo objectForKey:@"xmsInfo"] objectForKey:@"uuid"];
        }
    }
    @catch (NSException *exception) {
        RELog(@"[EFAD] Parse JSON data exception");
    }
}

#pragma mark -
#pragma mark Database

- (void)createEditableCopyOfDatabaseIfNeeded
{
	NSFileManager* fileManager = [NSFileManager defaultManager];
	
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString* documentsDirectory = [paths objectAtIndex:0];
	NSString* writableDBPath = [documentsDirectory stringByAppendingPathComponent:SQL_FILE_NAME];
    
	BOOL exist = [fileManager fileExistsAtPath:writableDBPath];
    
    dbControl = [[DBControl alloc] initByPath:writableDBPath];
    if (dbControl == NULL) {
        DBGLog(@"[EFAD] DBControl initial failed");
        return;
    }
    
    if (exist)
    {
        DBGLog(@"[EFAD] DB exist");
        //return;
    }
    
    BOOL result = YES;
    //create dvr table
    if ([dbControl addTableByName:@"dvr"]) {
        
        //add columns
        //result |= [dbControl addColumnToTable:@"dvr" column:@"pk" type:@"INTEGER PRIMARY KEY AUTOINCREMENT"];
        result |= [dbControl addColumnToTable:@"dvr" column:@"product"        type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"dvr" column:@"name"           type:@"TEXT"];
        result |= [dbControl addColumnToTable:@"dvr" column:@"type"           type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"dvr" column:@"ip"             type:@"TEXT"];
        result |= [dbControl addColumnToTable:@"dvr" column:@"port"           type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"dvr" column:@"user"           type:@"TEXT"];
        result |= [dbControl addColumnToTable:@"dvr" column:@"password"       type:@"TEXT"];
        result |= [dbControl addColumnToTable:@"dvr" column:@"streamtype"     type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"dvr" column:@"dualstream"     type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"dvr" column:@"group_id"       type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"dvr" column:@"blnCloseNotify" type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"dvr" column:@"uuid"           type:@"TEXT"];
        
        if (!result) {
            NSAssert1(0, @"%@",[dbControl getErrorMessage]);
            return;
        }
        
        if (!exist) {
            
            //add demo site
            Device *dev = [[Device alloc] initWithProduct:DEV_DVR];
            NSArray *demoAryDvr = [NSArray arrayWithObjects:SQL_DEMO_DVR_1,SQL_DEMO_DVR_2,SQL_DEMO_DVR_3, nil];
            for(NSString *tmpStr in demoAryDvr)
            {
                NSArray *strArray = [tmpStr componentsSeparatedByString:@","];
                dev.name = [strArray objectAtIndex:0];
                dev.type = [[strArray objectAtIndex:1] integerValue];
                dev.streamType = [Device getStreamType:dev.type product:dev.product];  //20161028 ++ by Ray Lin
                dev.ip = [strArray objectAtIndex:2];
                dev.port = [[strArray objectAtIndex:3] integerValue];
                dev.user = [strArray objectAtIndex:4];
                dev.password = [strArray objectAtIndex:5];
                result |= [dbControl addDeviceToTable:dev table:@"dvr"];
            }
            
            if (!result) {
                NSAssert1(0, @"%@",[dbControl getErrorMessage]);
                return;
            }
        }
    }
    
    //create ipcam table
    if ([dbControl addTableByName:@"ipcam"]) {
        
        //add columns
        //result |= [dbControl addColumnToTable:@"ipcam" column:@"pk" type:@"INTEGER PRIMARY KEY AUTOINCREMENT"];
        result |= [dbControl addColumnToTable:@"ipcam" column:@"product"        type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"ipcam" column:@"name"           type:@"TEXT"];
        result |= [dbControl addColumnToTable:@"ipcam" column:@"type"           type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"ipcam" column:@"ip"             type:@"TEXT"];
        result |= [dbControl addColumnToTable:@"ipcam" column:@"port"           type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"ipcam" column:@"user"           type:@"TEXT"];
        result |= [dbControl addColumnToTable:@"ipcam" column:@"password"       type:@"TEXT"];
        result |= [dbControl addColumnToTable:@"ipcam" column:@"streamtype"     type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"ipcam" column:@"dualstream"     type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"ipcam" column:@"rtspport"       type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"ipcam" column:@"group_id"       type:@"INTEGER"];
        result |= [dbControl addColumnToTable:@"ipcam" column:@"device_service" type:@"TEXT"];//20150703 added by Ray Lin for ipcam onvif
        
        if (!result) {
            NSAssert1(0, @"%@",[dbControl getErrorMessage]);
            return;
        }
        
        if (!exist) {
            
            //add demo site
            Device *dev = [[Device alloc] initWithProduct:DEV_IPCAM];
            NSArray *demoAryIp = [NSArray arrayWithObjects:SQL_DEMO_IP_1,SQL_DEMO_IP_2,SQL_DEMO_IP_3,SQL_DEMO_IP_4, nil];
            
            for(NSString *tmpStr in demoAryIp)
            {
                NSArray *strArray = [tmpStr componentsSeparatedByString:@","];
                dev.name = [strArray objectAtIndex:0];
                dev.type = [[strArray objectAtIndex:1] integerValue];
                dev.streamType = [Device getStreamType:dev.type product:dev.product];  //20161028 ++ by Ray Lin
                dev.ip = [strArray objectAtIndex:2];
                dev.port = [[strArray objectAtIndex:3] integerValue];
                dev.user = [strArray objectAtIndex:4];
                dev.password = [strArray objectAtIndex:5];
                dev.streamType = [[strArray objectAtIndex:6] integerValue];
                dev.rtspPort = [[strArray objectAtIndex:7] integerValue];
                result |= [dbControl addDeviceToTable:dev table:@"ipcam"];
            }
            
            if (!result) {
                NSAssert1(0, @"%@",[dbControl getErrorMessage]);
                return;
            }
        }
    }
    
    DBGLog(@"[EFAD] Add Demo site success");
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)initializeDatabase
{
	NSMutableArray *dvrArray = [[NSMutableArray alloc] init];
	self.dvrs = dvrArray;
    SAVE_FREE(dvrArray);
	
	NSMutableArray *ipcamArray = [[NSMutableArray alloc] init];
	self.ipcams = ipcamArray;
    SAVE_FREE(ipcamArray);
    
    NSMutableArray *groupArray = [[NSMutableArray alloc] init];
    self.groups = groupArray;
    SAVE_FREE(groupArray);
    
    NSMutableArray *eventArray = [[NSMutableArray alloc] init];
    self.events = eventArray;
    SAVE_FREE(eventArray);
    
    // The dayabase is stored in the application bundle.
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:SQL_FILE_NAME];
    
    if (!dbControl) {
        dbControl = [[DBControl alloc] initByPath:path];
        if (dbControl == NULL) {
            DBGLog(@"[EFAD] DBControl initial failed");
            return;
        }
    }
    
    //check group is exist
    if(![self checkGroupExist])
        return;
    
    //Get Group list
    [dbControl getGroupsToArray:self.groups];
    
    //check event table is exist
    if(![self checkEventExist])
        return;
    
    //Get Event List
    NSMutableArray *tmpEvents = [[NSMutableArray alloc] init];
    if ([dbControl getEventsToArray:tmpEvents]) {
        [self.events addObject:[tmpEvents retain]];
    }
    
    //Get Dvr items
    NSMutableArray *tmpDvrs = [[NSMutableArray alloc] init];
    if ([dbControl getDevicesToArray:tmpDvrs type:DEV_DVR]) {
        
        for (DeviceGroup *dvrGroup in [self.groups objectAtIndex:DEV_DVR])
        {
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[tmpDvrs filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.group == %d",dvrGroup.group_id]]];
            if (tmpArray.count)
                [self.dvrs addObject:[tmpArray retain]];
        }
        
        //Unclassified device
        NSMutableArray *unDvrs = [NSMutableArray arrayWithArray:[tmpDvrs filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.group == 0"]]];
        [self.dvrs addObject:[unDvrs retain]];
    }
    
    //Get IPCam items
    NSMutableArray *tmpIPCams = [[NSMutableArray alloc] init];
    if ([dbControl getDevicesToArray:tmpIPCams type:DEV_IPCAM]) {
        
        for (DeviceGroup *ipGroup in [self.groups objectAtIndex:DEV_IPCAM])
        {
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[tmpIPCams filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.group == %d",ipGroup.group_id]]];
            if (tmpArray.count)
                [self.ipcams addObject:[tmpArray retain]];
        }
        
        //Unclassified device
        NSMutableArray *unIPcams = [NSMutableArray arrayWithArray:[tmpIPCams filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.group == 0"]]];
        [self.ipcams addObject:[unIPcams retain]];
    }
    
    DBGLog(@"[EFAD] DBControl Initail Success - DVR:%lu/%lu IPCAM:%lu/%lu",(unsigned long)self.dvrs.count,(unsigned long)tmpDvrs.count,(unsigned long)self.ipcams.count,(unsigned long)tmpIPCams.count);
    SAVE_FREE(tmpDvrs);
    SAVE_FREE(tmpIPCams);
    SAVE_FREE(tmpEvents);
}

- (BOOL)checkGroupExist
{
    BOOL ret = YES;
    
    // Check group table is exist, if not, create it
    if ([dbControl addTableByName:@"grouptable"]) {
        
        if (![dbControl checkColumnByName:@"name" table:@"grouptable"]) {
            
            if (![dbControl addColumnToTable:@"grouptable" column:@"name" type:@"TEXT"])
                ret = NO;
        }
        
        if (![dbControl checkColumnByName:@"type" table:@"grouptable"]) {
            
            if (![dbControl addColumnToTable:@"grouptable" column:@"type" type:@"INTEGER"]) {
                ret = NO;
            }
        }
        
        if (![dbControl checkColumnByName:@"group_id" table:@"grouptable"]) {
            
            if (![dbControl addColumnToTable:@"grouptable" column:@"group_id" type:@"INTEGER"])
                ret = NO;
        }
        // Check group column is exist
        if (![dbControl checkColumnByName:@"group_id" table:@"dvr"]) {
            
            if (![dbControl addColumnToTable:@"dvr" column:@"group_id" type:@"INTEGER"])
                ret = NO;
        }
        
        if (![dbControl checkColumnByName:@"group_id" table:@"ipcam"]) {
            
            if (![dbControl addColumnToTable:@"ipcam" column:@"group_id" type:@"INTEGER"])
                ret = NO;
        }
        
        // Check devcie_service column is exist, if not, create it
        if (![dbControl checkColumnByName:@"device_service" table:@"ipcam"]) {
            
            if (![dbControl addColumnToTable:@"ipcam" column:@"device_service" type:@"TEXT"])
                ret = NO;
        }
        
    } else
        ret = NO;
    
    
    return ret;
}

- (BOOL)checkEventExist
{
    BOOL result = YES;
    
    // Check event table is exist, if not, create it
    if ([dbControl addTableByName:@"event"]) {
        //add columns
        
        if (![dbControl checkColumnByName:@"source_name" table:@"event"]) {
            
            if (![dbControl addColumnToTable:@"event" column:@"source_name" type:@"TEXT"])
                result = NO;
        }
        if (![dbControl checkColumnByName:@"source_ip" table:@"event"]) {
            
            if (![dbControl addColumnToTable:@"event" column:@"source_ip" type:@"TEXT"])
                result = NO;
        }
        if (![dbControl checkColumnByName:@"source_port" table:@"event"]) {
            
            if (![dbControl addColumnToTable:@"event" column:@"source_port" type:@"INTEGER"])
                result = NO;
        }
        if (![dbControl checkColumnByName:@"source_uuid" table:@"event"]) {
            
            if (![dbControl addColumnToTable:@"event" column:@"source_uuid" type:@"TEXT"])
                result = NO;
        }
        if (![dbControl checkColumnByName:@"dev_Type" table:@"event"]) {
            
            if (![dbControl addColumnToTable:@"event" column:@"dev_Type" type:@"INTEGER"])
                result = NO;
        }
        if (![dbControl checkColumnByName:@"dev_name" table:@"event"]) {
            
            if (![dbControl addColumnToTable:@"event" column:@"dev_name" type:@"TEXT"])
                result = NO;
        }
        if (![dbControl checkColumnByName:@"dev_ip" table:@"event"]) {
            
            if (![dbControl addColumnToTable:@"event" column:@"dev_ip" type:@"INTEGER"])
                result = NO;
        }
        if (![dbControl checkColumnByName:@"dev_eid" table:@"event"]) {
            
            if (![dbControl addColumnToTable:@"event" column:@"dev_eid" type:@"INTEGER"])
                result = NO;
        }
        if (![dbControl checkColumnByName:@"channel" table:@"event"]) {
            
            if (![dbControl addColumnToTable:@"event" column:@"channel" type:@"INTEGER"])
                result = NO;
        }
        if (![dbControl checkColumnByName:@"event_type" table:@"event"]) {
            
            if (![dbControl addColumnToTable:@"event" column:@"event_type" type:@"TEXT"])
                result = NO;
        }
        if (![dbControl checkColumnByName:@"event_time" table:@"event"]) {
            
            if (![dbControl addColumnToTable:@"event" column:@"event_time" type:@"INTEGER"])
                result = NO;
        }
        if (![dbControl checkColumnByName:@"event_seq" table:@"event"]) {
            
            if (![dbControl addColumnToTable:@"event" column:@"event_seq" type:@"INTEGER"])
                result = NO;
        }
        if (![dbControl checkColumnByName:@"blnRead" table:@"event"]) {
            
            if (![dbControl addColumnToTable:@"event" column:@"blnRead" type:@"INTEGER"])
                result = NO;
        }
        
        if (!result) {
            NSAssert1(0, @"%@",[dbControl getErrorMessage]);
            return result;
        }
    }
    return result;
}

- (BOOL)refreshDVRsIntoDatabase
{
    BOOL ret = YES;
    
	// delete all devices from database
	if (![dbControl deleteAllItemsFromTable:@"dvr"])
        ret = NO;
	
	// insert all devices in the array into database
    NSInteger devCount = 0;
	for (NSMutableArray *tmpArray in self.dvrs)
    {
        for (Device *dev in tmpArray)
        {
            dev.rowID = devCount++;
            ret |= [dbControl addDeviceToTable:dev table:@"dvr"];
        }
	}
    
    return ret;
}

- (BOOL)refreshIPCamsIntoDatabase
{
	BOOL ret = YES;
    
    // delete all devices from database
    NSInteger devCount = 0;
	if (![dbControl deleteAllItemsFromTable:@"ipcam"])
        ret = NO;
    
	// insert all devices in the array into database
	for (NSMutableArray *tmpArray in self.ipcams)
    {
        for (Device *dev in tmpArray)
        {
            dev.rowID = devCount++;
            ret |= [dbControl addDeviceToTable:dev table:@"ipcam"];
        }
	}
    
    return ret;
}

- (BOOL)refreshGroupIntoDatabase
{
	BOOL ret = YES;
    
    // delete all group from database
	if (![dbControl deleteAllItemsFromTable:@"grouptable"])
        ret = NO;
	
	// insert all group in the array into database
    for (NSMutableArray *tmpArray in self.groups)
    {
        for (DeviceGroup *group in tmpArray)
            ret |= [dbControl addGroupToTable:group table:@"grouptable"];
    }
    
    return ret;
}

- (BOOL)deleteAllEventItemsFromTable:(NSString *)_uuid
{
    BOOL ret = YES;
    
    // delete all events from database
    if (![dbControl deleteAllEventsFromTable:_uuid table:@"event"])
        ret = NO;
    
    return ret;
}

- (void)updateDeviceTokenIfNeeded
{
    if (!_DeviceToken) {
        return;
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *strToken = [NSString new];
    
    if (![userDefault objectForKey:KEY_DEVTOKEN]) {
        [userDefault setValue:_DeviceToken forKey:KEY_DEVTOKEN];
        [userDefault synchronize];
        return;
    }
    else
        strToken = [userDefault stringForKey:KEY_DEVTOKEN];
    
    if (![strToken isEqualToString:_DeviceToken]) {
        [userDefault setValue:_DeviceToken forKey:KEY_DEVTOKEN];
        [userDefault synchronize];
        
        // Update device token here
        [self updateByMobile];
    }
}

- (void)updateByMobile
{
    NSMutableArray *tmpdvr = [[[NSMutableArray alloc] init] autorelease];
    [dbControl getDevicesToArray:tmpdvr type:DEV_DVR];
    
    for (Device *tmpDev in tmpdvr) {
        if (tmpDev.type == XMS_SERIES) {
            if (tmpDev.blnCloseNotify == 0) {
                StreamReceiver *streamReceiver = [[StreamReceiver alloc] initWithDevice:tmpDev];
                NSArray *ary = [NSArray arrayWithObjects:_DeviceToken, tmpDev.name, nil];
                
                //update in background
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
                    [streamReceiver updateDevTokenToServer:ary];
                });
            }
        }
    }
}

@end

