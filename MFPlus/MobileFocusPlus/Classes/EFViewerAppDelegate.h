//
//  EFViewerAppDelegate.h
//  EFViewer
//
//  Created by Nobel Hsu on 2010/5/7.
//  Copyright EverFocus 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import "Device.h"
#import "NavViewController.h"
#import "EFMediaTool.h"
#import "DBControl.h"

#define HELPPDF @"MobileFocus_iOS_UM_V2.4.10_O_EN.pdf"
#define HELPURL @"http://old.everfocus.com.tw/Storage/HQ/Manual/Mobile%20Focus/MobileFocus_iOS_UM.pdf"
#define MAX_SUPPORT_DISPLAY 4
 
@interface EFViewerAppDelegate : NSObject <UIApplicationDelegate,UNUserNotificationCenterDelegate>{
    EventTable      *evtable;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet NavViewController *navigationController;
@property (nonatomic, retain) NSMutableArray *dvrs;
@property (nonatomic, retain) NSMutableArray *ipcams;
@property (nonatomic, retain) NSMutableArray *groups;
@property (nonatomic, retain) NSMutableArray *events;
@property (nonatomic) BOOL blnFullScreen;    //20120416 add by James Lee, use this to keep the status in different viewcontrollers
@property (nonatomic) NSInteger os_version;
@property (nonatomic, retain) NSString       *version;
@property (nonatomic, retain) EFMediaTool    *mediaLibrary;
@property (nonatomic, retain) NSString       *_DeviceToken;  //20150730 added by Ray Lin, use this to save the device token for APNs
@property (nonatomic, retain) NSString       *_AppName;  //20160204 added by Ray Lin, get app name

+ (EFViewerAppDelegate *)sharedAppDelegate;

- (BOOL)refreshDVRsIntoDatabase;
- (BOOL)refreshIPCamsIntoDatabase;
- (BOOL)refreshGroupIntoDatabase;
- (BOOL)deleteAllEventItemsFromTable:(NSString *)_uuid;
- (void)resetBadgeNumber;
- (void)startLiveAndPlayBack;
- (void)handleRemoteNotification:(NSDictionary *)payload;

@end

