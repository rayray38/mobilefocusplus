#!/bin/bash

# 使用者參數
PROJECT_NAME="EFViewerPlus"
PROFILE="MobileFocus_PlusAd.mobileprovision"
PROVISIONING_PROFILE_NAME="MobileFocus PlusAd"
INFOPLIST_FILE="EFViewer-Info.plist"

function NSLog(){
    clear
    MESSAGE=$1
    echo "$(tput setaf 220)\n ${MESSAGE} \n$(tput sgr 0)"
}


## 確認參數
NSLog "** CHECK PARAMETERS **"

# 沒給ProjectName則用FolderName
if [[(-z "$PROJECT_NAME")]]
then
    PROJECT_NAME=${PWD##*/}
fi

# 沒給ProvisioningProfile從檔案拿
if [[(-z "${PROFILE}")]]
then
    echo "\n$(tput setaf 1)  缺少ProvisioningProfile\n"
    exit 0
fi

# Get App_Version
NSLog "** Get App Version Number **"
VERSIONNUM=$(/usr/libexec/PlistBuddy -c "Print CFBundleShortVersionString" "${INFOPLIST_FILE}")
if  [[  "${VERSIONNUM}"  ==  ""  ]] ;  then
    echo  "No version number in $INFOPLIST_FILE"
    exit 0
fi

# Get App_Build_Number
NSLog "** Get App Build Number **"
BUILDNUM=$(/usr/libexec/PlistBuddy -c "Print CFBundleVersion" "${INFOPLIST_FILE}")
if  [[  "${VERSIONNUM}"  ==  ""  ]] ;  then
    echo  "No build number in $INFOPLIST_FILE"
    exit 0
fi

#NEWSUBVERSION=`echo $VERSIONNUM | awk -F "." '{print $4}'`
#NEWSUBVERSION=$(($NEWSUBVERSION + 1))
#NEWVERSIONSTRING=`echo $VERSIONNUM | awk -F "." '{print $1 "." $2 "." $3 ".'$NEWSUBVERSION'" }'`
#/usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString $NEWVERSIONSTRING" "${INFOPLIST_FILE}"

## 系統參數
BUILD_PATH="MobileFocus_Build"
IPA_FILE="${PROJECT_NAME}(${VERSIONNUM}.${BUILDNUM}_Internal).ipa"
ARCHIVE_FILE="${PROJECT_NAME}.xcarchive"

# Error Log檔案
BUILD_ERROR_FILE=build_errors.log

ARCHIVE_PATH="./${BUILD_PATH}/${ARCHIVE_FILE}"
EXPORT_PATH="./${BUILD_PATH}/${IPA_FILE}"

# Provisioning Profile
PROFILE_DATA=$(security cms -D -i ${PROFILE})
#PROVISIONING_PROFILE_NAME=$(/usr/libexec/PlistBuddy -c 'Print :Name' /dev/stdin <<< $PROFILE_DATA)
UUID=$(/usr/libexec/PlistBuddy -c 'Print :UUID' /dev/stdin <<< $PROFILE_DATA)
APP_ID_PREFIX=$(/usr/libexec/PlistBuddy -c 'Print :ApplicationIdentifierPrefix:0' /dev/stdin <<< $PROFILE_DATA)
CODE_SIGN_IDENTITY=$(/usr/libexec/PlistBuddy -c 'Print :Entitlements:application-identifier' /dev/stdin <<< $PROFILE_DATA)

# Copy 來源的 Provisioning Profile 至 OS
#cp -rf $PROFILE ~/Library/MobileDevice/Provisioning\ Profiles/$UUID.mobileprovision

echo "PROJECT_NAME: ${PROJECT_NAME}"
echo "PROVISIONING_PROFILE: ${PROFILE}"
echo "PROFILE_NAME: ${PROVISIONING_PROFILE_NAME}"
echo "UUID: ${UUID}"
echo "CODE_SIGN_IDENTITY: ${CODE_SIGN_IDENTITY}"
echo "APP_ID_PREFIX: ${APP_ID_PREFIX}"
echo ""

## 建置前清除
NSLog "** CLEAN BEFORE BUILD **"
xcodebuild -alltargets clean
rm -rf ${BUILD_PATH}
rm $BUILD_ERROR_FILE
mkdir ${BUILD_PATH}

## 建置與封裝
NSLog "** BUILD & ARCHIVE **"

## Unlock Keychain
security unlock-keychain -p Bs20160107 /Users/buildcode/Library/Keychains/login.keychain

# 可換成xcodebuild
xcodebuild \
-scheme $PROJECT_NAME \
archive \
PROVISIONING_PROFILE="$UUID" \
CODE_SIGN_IDENTITY="$CODE_SIGN_IDENTITY" \
-archivePath ${ARCHIVE_PATH} \
CONFIGURATION_BUILD_DIR=${BUILD_PATH} \
2>${BUILD_ERROR_FILE} \


## 檢查建置錯誤
NSLog "** CHECK BUILD ERROR ** "

errors=`grep -wc "The following build commands failed" ${BUILD_ERROR_FILE}`
if [ "$errors" != "0" ]
then
    echo "$(tput setaf 1)BUILD FAILED. Error Log:"
    cat ${BUILD_ERROR_FILE}
    echo "$(tput sgr 0)"
    exit 0
else
    rm $BUILD_ERROR_FILE
fi

# ... continue


## 輸出IPA檔案
NSLog "** EXPORT IPA FILE **"

xcodebuild \
-exportArchive \
-exportFormat IPA \
-archivePath ${ARCHIVE_PATH} \
-exportPath ${EXPORT_PATH} \
-exportProvisioningProfile "$PROVISIONING_PROFILE_NAME" \


##移除多餘檔案
NSLog "** REMOVE UNUSED FILES **"

rm "./${BUILD_PATH}/${PROJECT_NAME}.app"
rm -rf "./${BUILD_PATH}/${PROJECT_NAME}.app.dSYM"
rm -rf ${ARCHIVE_PATH}

# ... finish all
